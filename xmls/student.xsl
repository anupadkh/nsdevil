<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Student - Details</title></head>
<body>
<table border="1">
    <caption>My Details</caption>
    <tr>
        <th>Firstname</th><th>Lastname</th><th>Email</th>
    </tr>
    <xsl:for-each select="students/student/studentdata">
    <tr>
        <td><xsl:value-of select="firstname"/></td>
        <td><xsl:value-of select="lastname"/></td>
        <td><xsl:value-of select="email"/></td>
    </tr>
    </xsl:for-each>
    <tr></tr>
</table>

    <table border="1">
    <caption>My Electives</caption>
    <xsl:for-each select="students/courses">
    <tr>
        <td><xsl:value-of select="elective1"/></td>
        <td><xsl:value-of select="elective2"/></td>
        <td><xsl:value-of select="elective3"/></td>
        <td><xsl:value-of select="elective4"/></td>
    </tr>
    </xsl:for-each>
    </table>
</body>
 </html>
</xsl:template>
</xsl:transform>
