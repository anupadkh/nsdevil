<%@ include file="../include/header.jsp" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type="text/javascript">
$(document).ready(function(e) {
	
});

//ajax function example
function userList(page) {
	$.ajax({
        type: "POST",
        url: "../ajax/userList",
        data: {
        },
        dataType: "json",
        success: function(data, status) {
        	var html ="<tr><td><span class=\"checkBt\"></td>"
		    		+	"<td>user ID</td>"
		    		+	"<td>user name</td>"
		    		+	"<td>user contact</td>"
		    		+	"<td>user Email</td>"
		    		+	"<td>2018-01-30 11:11:11</td>"
		    		+	"<td><span class='sbtn'>"+$.i18n.prop("examList.seeDet")+"</span><span class='sbtn'>"+$.i18n.prop("examList.delete")+"</span></td>"
		    		+"</tr>";
            $("#user_list").html(html);
        },
        error: function(xhr, textStatus) {
            alert("An error has occurred");
        }
    });	
}

</script>

<div class="wrap">

<%@ include file="../include/top.jsp" %>
     
            
	<div class="contents"> 
    
      <div class="nav">
        <a href="../main">Home</a> / <spring:message code="main.assign"/> / <em><spring:message code="main.applicants"/></em>
      </div>    
      
      <div class="list">
      		  
              <table width="100%" cellpadding="0" cellspacing="0" border="0" class="listtable">
              <colgroup>
 	            <col width="5%" />
                <col width="10%" />
                <col width="20%" />
                <col width="15%" />
                <col width="20%" />
                <col width="15%" />
                <col width="20%" />
              </colgroup>
                <tr>
				<th><a href="javascript:checkAll()"><span id="checkAll" class="checkBt" ></span></a></th>
                  <th class="thleft"><spring:message code="userList.Id"/></th>
                  <th><spring:message code="examinee.name"/></th>
                  <th><spring:message code="examinee.contact"/></th>
                  <th><spring:message code="examinee.email"/></th>
                  <th><spring:message code="examList.regDate"/></th>
                  <th>...</th>
                </tr>
                <tbody id="user_list">
                </tbody>
              </table>
              <input type="hidden" id="page" name="page">
              <input type="hidden" id="userType">
              <p class="page" id="list_page"></p>
       </div><!--//list-->            
	</div><!--//contents--> 
	

</div><!--//wrap-->

<%@ include file="../include/footer.jsp" %>