package com.nsdevil.tms;

import java.util.HashMap;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nsdevil.tms.service.MainService;

import net.sf.json.JSONObject;

@Controller
public class MainController {
	
	@Autowired
	private MainService mainService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Locale locale, Model model, HttpSession session) {
		return "redirect:/login";
	}
	
	/**
	 * page mapping sample
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String main(Locale locale, Model model) {
		return "/login";
	}
	
	/**
	 * ajax sample
	 * @param searchParam
	 * @return
	 */
	@RequestMapping(value = "/ajax/userList", produces="application/json", method = RequestMethod.POST)
	public @ResponseBody String userList(@RequestParam HashMap<String, Object> param) {
		
		JSONObject json = new JSONObject();
		
		// TODO call service function and fill result hashmap
		HashMap<String, Object> result = null;
		
		json.accumulateAll(result);
		
		return json.toString();
	}
}
