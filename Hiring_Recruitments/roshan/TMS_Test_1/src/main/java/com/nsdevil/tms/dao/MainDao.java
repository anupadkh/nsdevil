package com.nsdevil.tms.dao;

import java.util.HashMap;

import com.nsdevil.tms.domain.UserInfoVo;

public interface MainDao {
	public UserInfoVo getUserList(HashMap<String, Object> param);
}
