package com.nsdevil.tms.service;

import java.util.ArrayList;
import java.util.HashMap;

import com.nsdevil.tms.domain.UserInfoVo;

public interface MainService {
	public ArrayList<UserInfoVo> getUserList(HashMap<String, Object> param);
}
