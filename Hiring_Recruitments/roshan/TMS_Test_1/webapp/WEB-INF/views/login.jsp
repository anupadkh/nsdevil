<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><spring:message code="html_title"/></title>
<link rel="stylesheet" href="<c:url value="/css/login.css"/>" type="text/css" />
<script type="text/javascript" src="<c:url value="/js/jquery/jquery-1.11.1.min.js"/>"></script>
</head>

<body>

<div class="top">
	<div class="logo"><img src="img/logo.gif"/></div>
    <p class="gnb"><a href=""><spring:message code="login.customerCenter"/></a> | <a href=""><spring:message code="login.addDesc"/></a></p>
</div>

<div class="con">

	<form onSubmit="javascript:return false;">

    <div class="input">
    	<ul class="box">
        	<li class="id"><input id="id" name="id" type="text" /></li>
        	<li class="pw"><input id="password" name="password" type="password" /></li>            
        </ul>
    	<div class="btn"><input type="image" src="img/log_btn.gif" onClick="javascript:void(0);" /></div>  
    	<p class="save"><input name="id_save" type="checkbox" value="아이디 저장" /><spring:message code="login.saveId"/></p>                
     </div>
 
   	<div class="text"><img src="img/log_text.gif" /></div>
   	
   	</form>
   	               
</div>
         
<div class="copy">COPYRIGHT (C) 2014 Powered by UBT ALL RIGHT RESERVED</div>  

</body>
</html>