<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="top">
        <div class="t_logo">
        <a href="../main"><img src="../img/logo.gif" /></a>
        </div>
        <p class="gnb">
        	<span>Tester <spring:message code="main.name"/></span>
        	<a href="<c:url value="/logout"/>"><span class="log"><img src="../img/icon_check.png" /><spring:message code="main.logout"/></span></a> 
        	l 
        	<a href="javascript:void(0);"><span class="mypage"><spring:message code="main.myinfo"/></span></a>
        </p>
</div>
