<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><spring:message code="html_title"/></title>
<link rel="stylesheet" href="<c:url value="/css/navi.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/css/common.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/css/layer.css"/>"  type="text/css" />
<link rel="stylesheet" href="<c:url value="/css/dev.css"/>" type="text/css" />
<script type="text/javascript" src="<c:url value="/js/jquery/jquery-1.11.1.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery/jquery.i18n.properties-min-1.0.9.js"/>"></script>
</head>

<script type="text/javascript">

//메세지 프로퍼티
$.i18n.properties({
	name:"message",
	path:"<c:url value="/messages"/>/",
	mode:"both",
	language:"${pageContext.response.locale}",
	callback: function() {
	}
});

</script>
<body>
