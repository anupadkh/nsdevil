import pandas as pd

file='translation.xlsx'
xl = pd.ExcelFile(file)
df = pd.DataFrame()
# df.columns=['korean','english','indonesian']
df = pd.read_excel(file,header=[0],sheet_name = xl.sheet_names[0])
df = df.iloc[:,range(3)].copy()
df.columns = ['korean','english','indonesian']
for i in range(1,len(xl.sheet_names)):
    tempdf = pd.read_excel(file,header=[0],sheet_name = xl.sheet_names[i])
    tempdf = tempdf.iloc[:,range(3)].copy()
    tempdf.columns = ['korean','english','indonesian']
    df = df.append(tempdf,ignore_index=True)

df = df.iloc[df['korean'].dropna().index,:]
df = df.sort_values(by='korean', ascending=False)
df = df.reset_index(drop=True)
df.to_json('translation.json')
