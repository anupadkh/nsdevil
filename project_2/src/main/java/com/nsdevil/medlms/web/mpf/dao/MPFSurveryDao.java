package com.nsdevil.medlms.web.mpf.dao;

import java.util.List;
import java.util.HashMap;


public interface MPFSurveryDao {

	public List<HashMap<String, Object>> getLessonPlanSFSurveyCS(HashMap<String, Object> param) throws Exception;
	
}
