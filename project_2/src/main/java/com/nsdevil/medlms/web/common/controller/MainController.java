package com.nsdevil.medlms.web.common.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.common.util.FileUploader;
import com.nsdevil.medlms.web.admin.service.AcaScheduleService;
import com.nsdevil.medlms.web.admin.service.AcademicService;
import com.nsdevil.medlms.web.common.service.CommonService;

@Controller
public class MainController extends ExceptionController {
	
	@Value("#{config['config.rootPath']}")
	private String ROOT_PATH;
	
	@Value("#{config['config.resPath']}")
	private String RES_PATH;
	
	@Autowired
	private CommonService commonService;

	@Autowired
	private AcademicService academicService;
	
	@Autowired
	private AcaScheduleService acaScheduleService;
	
	@RequestMapping(value = "/setChangeLanguage", method = RequestMethod.GET)
	public String chageLanguage(@RequestParam(required = false) String locale, HttpServletRequest request, HttpServletResponse response, Model model) {
		String status = "success";
		HttpSession session = request.getSession();
		Locale locales = null;
		if (locale.matches("en")) {
			locales = Locale.ENGLISH;
		} else {
			locales = Locale.KOREA;
		}
		session.setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, locales);
		session.setAttribute("locales", locales);
		model.addAttribute("status", status);
		return JSON_VIEW;
	}
	
	@RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
	public String index(HttpServletRequest request, Locale locale, Model model, Device device, HttpServletResponse response) throws Exception {
		String viewName = "index";
		
		if(request.getSession().getAttribute("S_DEVICE") == null) {
			if(device.isNormal()) {
				request.getSession().setAttribute("S_DEVICE", "PC");
			}else if(device.isMobile() || device.isTablet()) {
				viewName = "redirect:/mobile";
				request.getSession().setAttribute("S_DEVICE", "MOBILE");
			}
		}else {
			String s_device = request.getSession().getAttribute("S_DEVICE").toString();
			if(s_device.equals("MOBILE"))
				viewName = "redirect:/mobile";
				
		}		
		
		//사용자별 메인화면
		if (request.getSession().getAttribute("S_USER_LEVEL") != null) {
			int userLevel = (int) request.getSession().getAttribute("S_USER_LEVEL");
			if (userLevel == 1 || userLevel == 2) {	
				model.addAllAttributes(academicService.getAcademicYearList());
				viewName = "adminIndex";
			} else if (userLevel == 3) {
				viewName = "pfIndex";
			} else if (userLevel == 4) {
				viewName = "stIndex";
			}
 		}
		
		//중복 로그인 처리
		String overlapLogin = (String)request.getParameter("overlapLogin");
		if (overlapLogin != null) {
			if (overlapLogin.equals("Y")) {
				model.addAttribute("overlapLogin", "Y");
			}
		}
		
		return viewName;
	}
	
	@RequestMapping(value = "/aca/academicM", method = RequestMethod.GET)
	public String academicMView(HttpServletRequest request, Model model) throws Exception {
		model.addAttribute("lacaList", acaScheduleService.getLacaList());
		return "aca/acaSchedule";
	}
	
	//게시판 목록 첨부파일 다운로드
	@RequestMapping(value = "/common/board/download", method = RequestMethod.POST)
	public String boardDownload(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model, RedirectAttributes redirectAttribute) throws Exception {
		String returnUrl = String.valueOf(param.get("return_url"));
		HashMap<String, Object> map = commonService.getBoardAttachFileInfo(param);
		@SuppressWarnings("unchecked")
		List<HashMap<String,Object>> fileList =  (ArrayList<HashMap<String, Object>>) map.get("fileList");
		if (fileList == null || fileList.isEmpty()) {
			return "redirect:"+returnUrl;
		} else if (fileList.size() > 1) {
			model.addAllAttributes(map);
			return "zipDownloadView";
		} else {
			map = fileList.get(0);
			File file = new File(ROOT_PATH + RES_PATH + (String)map.get("file_path"));
			model.addAttribute("downloadFile", file);
			model.addAttribute("orignalName", (String)map.get("file_name"));
			return "downloadView";
		}
	}
	
		
	@RequestMapping(value = "/ajax/common/smartEdiotorImgUpload", method = RequestMethod.POST)
	public String smartEdiotorImgUpload(MultipartHttpServletRequest request) {
		
		if (request.getSession().getAttribute("S_CURRICULUM_SEQ") == null) {
			MultipartFile Filedata = request.getFile("Filedata");
			String callback=request.getParameter("callback"); 
			String callbackFunc=request.getParameter("callback_func"); 
			String file_result = ""; 
			try { 
				//파일 존재 유무 확인
				if(Filedata != null && Filedata.getOriginalFilename() != null && !Filedata.getOriginalFilename().equals("")) { 
					String original_name = Filedata.getOriginalFilename();
					String defaultPath = ROOT_PATH + RES_PATH + "/upload/editorPicture";
					File file = new File(defaultPath);
					
					if(!file.exists())
						file.mkdirs();
					
					String realname = FileUploader.uploadFile(defaultPath, Filedata);
					
					file_result = "&bNewLine=true&sFileName="+original_name+"&sFileURL="+RES_PATH+"/upload/editorPicture/"+"/"+realname;
				}else { 
					file_result += "&errstr=error"; 				
				} 
				
			} catch (Exception e) { 
				e.printStackTrace(); 			
			} 
			return "redirect:" + callback + "?callback_func="+callbackFunc+file_result;
		} else {
			String curr_seq = request.getSession().getAttribute("S_CURRICULUM_SEQ").toString(); //교육과정 시퀀스
			String year = request.getSession().getAttribute("S_CURRENT_ACADEMIC_YEAR").toString(); //년도
			
			MultipartFile Filedata = request.getFile("Filedata");
			String callback=request.getParameter("callback"); 
			String callbackFunc=request.getParameter("callback_func"); 
			String file_result = ""; 
			try { 
				//파일 존재 유무 확인
				if(Filedata != null && Filedata.getOriginalFilename() != null && !Filedata.getOriginalFilename().equals("")) { 
					String original_name = Filedata.getOriginalFilename();
					String defaultPath = ROOT_PATH + RES_PATH + "/upload/"+year+"/"+curr_seq+"/";
					File file = new File(defaultPath);
					
					if(!file.exists())
						file.mkdirs();
					
					String realname = FileUploader.uploadFile(defaultPath, Filedata);
					
					file_result = "&bNewLine=true&sFileName="+original_name+"&sFileURL="+RES_PATH+"/upload/"+year+"/"+curr_seq+"/"+realname;
				}else { 
					file_result += "&errstr=error"; 				
				} 
				
			} catch (Exception e) { 
				e.printStackTrace(); 			
			} 
			return "redirect:" + callback + "?callback_func="+callbackFunc+file_result;
		}
	}	
	
	@RequestMapping(value = "/ajax/common/getAcademicStageInfo", method = RequestMethod.POST)
	public String getAcademicStageInfo(HttpServletRequest request, Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		model.addAllAttributes(commonService.getAcademicStageInfo(param));
		
		return JSON_VIEW;
	}	
}
