package com.nsdevil.medlms.web.mpf.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.exception.LogicException;
import com.nsdevil.medlms.web.mpf.dao.MPFScheduleDao;
import com.nsdevil.medlms.web.pf.dao.PFLessonDao;

@Service
public class MPFScheduleService {

	@Autowired
	private MPFScheduleDao mpfScheduleDao;
	
	@Autowired
	private PFLessonDao pfLessonDao;

	public HashMap<String, Object> getSchedule(HashMap<String, Object> param) throws Exception {
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//교육과정 기본 정보
		result.put("basicInfo", pfLessonDao.getCurriculumBasicInfo(param));
		
		//시간표
		result.put("schedule", mpfScheduleDao.getSchedule(param));
		
		//메모
		//result.put("memo", scheduleDao.getMemo(param));
		
		return result;
	}
	
	public List<HashMap<String, Object>> getSelectedDaySchedule(HashMap<String, Object> param) throws Exception {
		
		//선택한 날짜가 없으면 오늘날짜로 설정
		if (((String)param.get("lesson_date")).length() == 0) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			param.put("lesson_date", sdf.format(new Date()));
		}
		
		return mpfScheduleDao.getSelectedDaySchedule(param);
	}
	
	@Transactional
	public ResultMap uploadScheduleXls(MultipartFile uploadFile, HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		if (uploadFile != null) {
			Workbook workbook = new XSSFWorkbook(uploadFile.getInputStream());
			
			//첫번째 Sheet 취득
			Sheet sheet = workbook.getSheetAt(0);
			int rows = sheet.getPhysicalNumberOfRows(); // 총 Row 취득
						
			for (int rowIdx = 1; rowIdx < rows; rowIdx++) {
				Row row = sheet.getRow(rowIdx);

				HashMap<String, Object> schedule = new HashMap<String, Object>();
				if(row.getCell(0) == null
						|| row == null)
					break;
									
				schedule.put("curr_seq", param.get("curr_seq"));						//교육과정 시퀀스(*)
				schedule.put("period_seq", String.valueOf(rowIdx));						//차시(*)
				schedule.put("lesson_date", getDateCellValue(row.getCell(0)));			//날짜(*)
				schedule.put("start_time", getTimeCellValue(row.getCell(1)));			//시작시간
				schedule.put("end_time", getTimeCellValue(row.getCell(2)));				//종료시간
				String period = getCellValue(row.getCell(3));
				
				//앞뒤 콤마 삭제
				period = period.replaceAll("(^(\\s*?\\,+)+\\s?)|(^\\s+)|(\\s+$)|((\\s*?\\,+)+\\s?$)", "");
				String[] periodList = period.split(",");
				
				schedule.put("period", String.join(",", periodList));					//교시
				schedule.put("period_cnt", periodList.length);							//시수
				schedule.put("pf_name", getCellValue(row.getCell(4)));					//교수명(*)
				schedule.put("professor_id", getCellValue(row.getCell(5)));				//사번(*)
				schedule.put("classroom", getCellValue(row.getCell(6)));				//장소(*)
				schedule.put("lesson_subject", getCellValue(row.getCell(7)));			//수업명(*)
				schedule.put("learning_outcome", getCellValue(row.getCell(8)));			//학습성과
				
				String lectureYN = "Y";
				if (getCellValue(row.getCell(9)).equals("비강의")) {
					lectureYN = "N";
				}
				
				schedule.put("lecture_yn", lectureYN);									//학습방법
				String lessonMethodCode = getCellValue(row.getCell(10));				//학습방법코드
				schedule.put("fe_flag", getCellValue(row.getCell(11)));					//형성평가
				schedule.put("lesson_code", getCellValue(row.getCell(12)));				//수업코드
				schedule.put("domain_code", getCellValue(row.getCell(13)));				//영역코드
				schedule.put("level_code", getCellValue(row.getCell(14)));				//수준코드
				schedule.put("evaluation_method_code", getCellValue(row.getCell(15)));	//평가방법코드
				schedule.put("reg_user_seq", param.get("reg_user_seq"));
				
				//필수체크
				if (((String)schedule.get("period_seq")).length() > 0 &&
						((String)schedule.get("lesson_date")).length() > 0 &&
						((String)schedule.get("classroom")).length() > 0 &&
						((String)schedule.get("lesson_subject")).length() > 0) {
					mpfScheduleDao.insertLessonPlan(schedule);
					
					if (lessonMethodCode.length() > 0) {
						String[] lessonMethodCodeList = lessonMethodCode.split(",");
						for (String lessonMethod : lessonMethodCodeList) {
							schedule.put("lesson_method_code", lessonMethod);
							mpfScheduleDao.insertLessonMethod(schedule);
						}
					}
				}
			}
		}
		return resultMap;
	}
	
	/**
	 * 일정 상세
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, Object> getScheduleDetail(HashMap<String, Object> param) throws Exception {
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		List<HashMap<String, Object>> resultList = mpfScheduleDao.getSchedule(param);
		
		if (0 < resultList.size()) {
			result = resultList.get(0);
		}
		return result;
	}
	
	/**
	 * 일정 수정
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ResultMap saveSchedule(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		mpfScheduleDao.saveSchedule(param);
		return resultMap;
	}
	
	/**
	 * 해당 교육과정의 모든 일정 삭제
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, Object> deleteAllSchedule(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		mpfScheduleDao.deleteAllSchedule(param);
		return resultMap;
	}
	
	/**
	 * 교수목록
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, Object> getPfList(HashMap<String, Object> param) throws Exception {
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		result.put("pfList", mpfScheduleDao.getPfList(param));
		
		return result;
	}
	
	
	
	/**
	 * fullCalendar 일정 날짜 변경
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public ResultMap changeEventDate(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		if (((String)param.get("event_type")).equals("memo")) {
			mpfScheduleDao.changeMemoDate(param);
		} else {
			//교수일 경우 수정권한이 있는지 확인
			if (((Integer)param.get("userLevel")) == 3) {
				param.put("lp_seq", ((String)param.get("event_seq")));
				HashMap<String, Object> scheduleDetail = getScheduleDetail(param);
				if ((int)param.get("reg_user_seq") != (int)scheduleDetail.get("reg_user_seq")) {
					throw new LogicException("변경할 권한이 없습니다.");
				}
			}
			mpfScheduleDao.changeLessonPlanDate(param);
		}
		return resultMap;
	}
	
	
	/**
	 * 일정 엑셀 다운로드
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<HashMap<String, Object>> scheduleExcelDown(HashMap<String, Object> param) throws Exception {
		return mpfScheduleDao.getSchedule(param);
	}
	
	/**
	 * yyyy-MM-dd 형식 날짜 취득
	 * @param cell
	 * @return
	 */
	private String getDateCellValue(Cell cell) {
		String value = getDateCellValue(cell, "yyyy-MM-dd");
		return value;
	}
	
	/**
	 * HH:mm:ss 형식 시간 취득
	 * @param cell
	 * @return
	 */
	private String getTimeCellValue(Cell cell) {
		String value = getDateCellValue(cell, "HH:mm:ss");
		return value;
	}
	
	/**
	 * 날짜 포맷 데이터 취득
	 * @param cell
	 * @param format
	 * @return
	 */
	private String getDateCellValue(Cell cell, String format) {
		String value = "";
		if (cell != null) {
			if (HSSFDateUtil.isCellDateFormatted(cell)) {
				SimpleDateFormat dtFormat = new SimpleDateFormat(format);
				value = dtFormat.format(cell.getDateCellValue()).toString();
			}
		}
		return value;
	}
	
	/**
	 * Cell 문자열 값 취득
	 * @param cell
	 * @return
	 */
	private String getCellValue(Cell cell) {
		String value = "";
		if (cell != null) {
			DataFormatter formatter = new DataFormatter();
			value = formatter.formatCellValue(cell);
		}
		return value;
	}
	
	public List<HashMap<String, Object>> getAcademicList(HashMap<String, Object> param) throws Exception {
		return mpfScheduleDao.getAcademicList(param);
	}

	public List<HashMap<String, Object>> getCurrPfList(HashMap<String, Object> param) throws Exception {
		
		return mpfScheduleDao.getCurrPfList(param);
	}
}