package com.nsdevil.medlms.web.mpf.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.web.mpf.dao.MPFLessonDao;
import com.nsdevil.medlms.web.pf.dao.PFLessonDao;

@Service
public class MPFLessonService {

	@Autowired
	private MPFLessonDao mpfLessonDao;

	@Autowired
	private PFLessonDao pfLessonDao;
	
	public List<HashMap<String, Object>> getprofessor(HashMap<String, Object> param) throws Exception {
		
		return mpfLessonDao.getprofessor(param);
	}

	public ResultMap getLessonPlanRegCs(HashMap<String, Object> param) throws Exception{
		ResultMap resultMap = new ResultMap();
		resultMap.put("currInfo", pfLessonDao.getCurriculumBasicInfo(param));
		resultMap.put("lpList", mpfLessonDao.getLessonPlanRegCs(param));
		return resultMap;
	}

	public ResultMap getLessonPlan(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("lessonPlanBasicInfo", pfLessonDao.getLessonPlanBasicInfo(param)); //수업계획서 정보

		resultMap.put("lessonCoreClinicList", pfLessonDao.getLessonCoreClinicList(param)); //핵심 임상표현
		resultMap.put("lessonClinicList", pfLessonDao.getLessonClinicList(param)); //관련 임상표현
		resultMap.put("lessonDiagnosisList", pfLessonDao.getLessonDiagnosisList(param)); //진단명
		resultMap.put("lessonOsceList", pfLessonDao.getLessonOsceList(param)); //임상술기
		resultMap.put("lessonFinishCapabilityList", pfLessonDao.getLessonFinishCapabilityList(param)); //졸업역량
		resultMap.put("formationEvaluationList", pfLessonDao.formationEvaluationCodeList(param)); //형성평가
		resultMap.put("lessonMethodList", pfLessonDao.lessonMethodCodeList(param)); //비강의
		resultMap.put("tloInfo", mpfLessonDao.getLessonPlanTlo(param));
		resultMap.put("eloInfo", mpfLessonDao.getLessonPlanElo(param));
		resultMap.put("preNextInfo", mpfLessonDao.preNextLpSeq(param));
		return resultMap;
	}

	public ResultMap getAbsenceSTList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("stList", mpfLessonDao.getAbsenceSTList(param));
		resultMap.put("lpInfo", pfLessonDao.getLessonPlanBasicInfo(param)); //수업계획서 정보
		return resultMap;
	}
}