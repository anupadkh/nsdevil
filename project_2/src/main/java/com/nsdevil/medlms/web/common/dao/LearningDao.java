package com.nsdevil.medlms.web.common.dao;

import java.util.HashMap;
import java.util.List;


public interface LearningDao {
	public List<HashMap<String, Object>> getAcademicYearList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getCurriculumList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getLessonDataLst(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getLessonDataAttachList(HashMap<String, Object> param) throws Exception;
	
	public int getOSCEContentsCount(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getOSCEContentsList(HashMap<String, Object> param) throws Exception;
	
	public int getCPXContentsCount(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getCPXContentsList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getOSCECodeList() throws Exception;
	public List<HashMap<String, Object>> getCPXCodeList() throws Exception;
	
	public HashMap<String, Object> getOSCEDetailInfo(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getCPXDetailInfo(HashMap<String, Object> param) throws Exception;
	
	public List<HashMap<String, Object>> getPBLAttachFileList(HashMap<String, Object> param) throws Exception;
	public int getPBLContentsCount(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getPBLContentsList(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getPBLDetailInfo(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getNextPBLInfo(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getPrevPBLInfo(HashMap<String, Object> param) throws Exception;
}
