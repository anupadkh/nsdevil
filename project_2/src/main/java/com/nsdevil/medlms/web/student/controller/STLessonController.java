package com.nsdevil.medlms.web.student.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.mobileweb.student.service.MobileStudentService;
import com.nsdevil.medlms.web.pf.service.PFLessonService;
import com.nsdevil.medlms.web.student.service.STLessonService;

@Controller
public class STLessonController extends ExceptionController {
	
	@Autowired
	private STLessonService stLessonservice;
	
	@Autowired
	private PFLessonService pfLessonservice;
	
	@Autowired
	private MobileStudentService mobileStudentService;
	
	//수업관리
	@RequestMapping(value = "/st/lesson", method = RequestMethod.GET)
	public String lessonView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		String viewName = "st/lesson/curriculumView";
		//교육과정 시퀀스, 년도, 수업계획서 시퀀스 셋팅 한다.
		model.addAllAttributes(stLessonservice.setCurrAndLpSeq(request));
		
					
		model.addAttribute("rightMenuOpenYn", "N");//default : 왼쪽메뉴 오픈상태
		return viewName;
	}
	
	
	@RequestMapping(value="/ajax/st/lesson/menu/left/list", method = RequestMethod.GET)
	public String lessonLeftMenuList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(stLessonservice.getLessonLeftMenuList(param, request));
		
		return JSON_VIEW;
	}
	
	@RequestMapping(value="/ajax/st/lesson/menu/right/list", method = RequestMethod.GET)
	public String lessonRightMenuList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		//TODO
		return JSON_VIEW;
	}	
	
	@RequestMapping(value="/ajax/st/lesson/menu/left/lessonPlanList", method = RequestMethod.POST)
	public String lessonLeftMenulessonPlanList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		request.getSession().setAttribute("S_CURRICULUM_SEQ", param.get("curr_seq"));
		model.addAllAttributes(stLessonservice.getLessonPlanLeftMenuList(param));
		return JSON_VIEW;
	}
	
	//교육과정계획서
	@RequestMapping(value = "/st/lesson/curriculum", method = RequestMethod.POST)
	public String curriculumView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		return "st/lesson/curriculumView";
	}
	
	@RequestMapping(value="/ajax/st/lesson/lessonPlanTitle", method = RequestMethod.POST)
	public String lessonPlanTitle(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		if(request.getSession().getAttribute("S_LP_SEQ") != null) {
			param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());
			model.addAllAttributes(stLessonservice.getLessonPlanTitle(param));
		}
		return JSON_VIEW;
	}	
		
	//수업계획서
	@RequestMapping(value = "/st/lesson/lessonPlan", method = RequestMethod.POST)
	public String lessonPlanView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		request.getSession().setAttribute("S_LP_SEQ", param.get("lpSeq").toString());					
		return "st/lesson/lessonPlanView";
	}
	
	//수업자료
	@RequestMapping(value = "/st/lesson/lessonData", method = RequestMethod.GET)
	public String lessonDataView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		if(request.getSession().getAttribute("S_LP_SEQ") == null)
			param.put("lp_seq", "");
		else
			param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());
		int lesson_data_seq = pfLessonservice.getLessonDataSeq(param);		
		model.addAttribute("lesson_data_seq", lesson_data_seq);						
		return "st/lesson/lessonDataView";
	}
		
	@RequestMapping(value = "/ajax/st/lesson/lessonData/multiFileDownload", method = RequestMethod.POST)
	public String lessonDataMultiFileDownload(@RequestParam HashMap<String, Object> param, String originalFilename, Model model) throws Exception {
		Util.requiredCheck(param, new String[]{"file_path", "file_name"});
		
		model.addAllAttributes(stLessonservice.lessonDataMultiFileDown(param));		
		return JSON_VIEW;

	}

	//형성평가
	@RequestMapping(value = "/st/lesson/formationEvaluation", method = RequestMethod.GET)
	public String formationEvaluationView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
							
		return "st/lesson/formationEvaluationView";
	}
	
	@RequestMapping(value="/ajax/st/lesson/formationEvaluation/list", method = RequestMethod.POST)
	public String formationEvaluationList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		if(request.getSession().getAttribute("S_CURRICULUM_SEQ") == null)
			param.put("curr_seq", "");
		else
			param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());	
		
		model.addAllAttributes(stLessonservice.STFormationEvaluationList(param));
		return JSON_VIEW;
	}	
	
	//과제
	@RequestMapping(value = "/st/lesson/assignMent", method = RequestMethod.GET)
	public String assignMentView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		return "st/lesson/assignMent";
	}
	
	@RequestMapping(value="/ajax/st/lesson/assignMent/list", method = RequestMethod.POST)
	public String assignMentList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Object lp_seq = request.getSession().getAttribute("S_LP_SEQ");
		
		if(lp_seq == null)
			param.put("lp_seq", "");
		else {
			if("2".equals(param.get("asgmt_type").toString()))
				param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());
		}

		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());	
				
		model.addAllAttributes(stLessonservice.STAssignMentList(param));
		return JSON_VIEW;
	}	
	
	@RequestMapping(value="/ajax/st/lesson/assignMent/Onelist", method = RequestMethod.POST)
	public String assignMentOneList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());	
		
		model.addAllAttributes(stLessonservice.STAssignMentOneList(param));
		return JSON_VIEW;
	}	
	
	//과제 제출	
	@RequestMapping(value = "/ajax/st/lesson/assignMent/create", method = RequestMethod.POST)
	public String STassignMentCreate(MultipartHttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		
		model.addAllAttributes(stLessonservice.assignMentSubmitInsert(param, request));
		
		
		return JSON_VIEW;
	}
	
	//과제 제출	
	@RequestMapping(value = "/ajax/st/lesson/assignMent/submitFileDel", method = RequestMethod.POST)
	public String STassignMentsubmitFileDel(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());	
		model.addAllAttributes(stLessonservice.assignMentSubmitFileDel(param));
				
		return JSON_VIEW;
	}
	
	//알림 페이지
	@RequestMapping(value = "/st/lesson/stNotice/view", method = RequestMethod.GET)
	public String stNoticeView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		return "st/lesson/stNotice";
	}

	//단원관리 페이지
	@RequestMapping(value = "/st/lesson/unit", method = RequestMethod.GET)
	public String unitView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		return "st/lesson/unitView";
	}	

	//종합성적 페이지
	@RequestMapping(value = "/st/lesson/currGrade", method = RequestMethod.GET)
	public String currGradeView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		
		model.addAllAttributes(stLessonservice.getAcaState(param));
		return "st/lesson/currGrade";
	}

	//수시성적 페이지
	@RequestMapping(value = "/st/lesson/soosiGrade", method = RequestMethod.GET)
	public String soosiGradeView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		return "st/lesson/soosiGrade";
	}

	//형성평가 페이지
	@RequestMapping(value = "/st/lesson/feScore", method = RequestMethod.GET)
	public String feScoreView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		return "st/lesson/feScore";
	}
	
	//종합성적 가져오기	
	@RequestMapping(value = "/ajax/st/lesson/currGrade/list", method = RequestMethod.POST)
	public String getCurrGradeList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());	
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		
		model.addAllAttributes(stLessonservice.getCurrGradeList(param));
				
		return JSON_VIEW;
	}	

	//수시성적 가져오기	
	@RequestMapping(value = "/ajax/st/lesson/soosiGrade/list", method = RequestMethod.POST)
	public String getSoosiGradeList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());	
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		
		model.addAllAttributes(stLessonservice.getSoosiGradeList(param));
				
		return JSON_VIEW;
	}

	//수시성적 가져오기	
	@RequestMapping(value = "/ajax/st/lesson/feScore/list", method = RequestMethod.POST)
	public String getFeScoreList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());	
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		
		model.addAllAttributes(stLessonservice.getFeScoreList(param));
				
		return JSON_VIEW;
	}
	
	//종합과제 페이지
	@RequestMapping(value = "/st/lesson/currAsgmt", method = RequestMethod.GET)
	public String currAsgmtView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		return "st/lesson/currAssignMent";
	}
	
	//수업만족도 조사 페이지
	@RequestMapping(value = "/st/lesson/sfResearch", method = RequestMethod.GET)
	public String sfResearchView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		param.put("s_lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());
		param.put("sr_type", "1");
		model.addAttribute("end_chk", stLessonservice.getLessonEndChk(param));
		
		model.addAttribute("lp_seq", param.get("s_lp_seq").toString());	
		model.addAttribute("curr_seq", param.get("curr_seq").toString());
		model.addAttribute("sr_type", param.get("sr_type").toString());
		model.addAllAttributes(mobileStudentService.getLessonPlanTitleInfo(param));	
		return "st/lesson/satisfactionResearchCreate";
	}
	
	//과정만족도 조사 페이지
	@RequestMapping(value = "/st/lesson/sfCurrResearch", method = RequestMethod.GET)
	public String sfCurrResearchView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		param.put("s_lp_seq", "0");
		param.put("sr_type", "2");
		model.addAttribute("end_chk", stLessonservice.getCurrEndChk(param));
				
		model.addAttribute("curr_seq", param.get("curr_seq").toString());
		model.addAttribute("lp_seq", param.get("s_lp_seq").toString());	
		model.addAttribute("sr_type", param.get("sr_type").toString());
		model.addAllAttributes(mobileStudentService.getLessonPlanTitleInfo(param));	
		return "st/lesson/satisfactionCurrResearchCreate";
	}
}
