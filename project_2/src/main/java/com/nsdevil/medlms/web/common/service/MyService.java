package com.nsdevil.medlms.web.common.service;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.exception.LogicException;
import com.nsdevil.medlms.common.exception.ResourceNotFoundException;
import com.nsdevil.medlms.common.util.FileUploader;
import com.nsdevil.medlms.common.util.PagingHelper;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.mobileweb.student.dao.MobileStudentDao;
import com.nsdevil.medlms.web.common.dao.MyDao;

@Service
public class MyService {

	@Autowired
	private MyDao myDao;
	
	@Autowired
	private MobileStudentDao mobileStudentDao;

	@Value("#{config['config.rootPath']}")
	private String ROOT_PATH;

	@Value("#{config['config.resPath']}")
	private String RES_PATH;

	@Value("#{config['config.profilePath']}")
	private String PROFILE_PATH;

	public ResultMap getProfileDetail(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		HashMap<String, Object> profileInfo = new HashMap<String, Object>();
		profileInfo = myDao.getProfileInfo(param);
		if (profileInfo == null || profileInfo.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_USER_PROFILE_INFO", "007");
		}
		resultMap.putAll(profileInfo);
		return resultMap;
	}

	public ResultMap profileModify(HttpServletRequest request, HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		boolean isPictureChange = false;
		boolean isPwdChagne = false;
		String profileImgUploadPath = ROOT_PATH + RES_PATH + PROFILE_PATH;
		// 비밀번호 변경시 비밀번호 체크 로직
		String nowPwd = (String) param.get("nowPwd");
		if (nowPwd != null && !nowPwd.equals("")) {
			param.put("pwd", Util.getSHA256(nowPwd));
			int userExistCnt = myDao.getUserExistCnt(param);
			if (userExistCnt != 1) {
				throw new ResourceNotFoundException("NOT_FOUND_USER_INFO", "101");
			}
			isPwdChagne = true;
		}

		// 파일업로드
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		MultipartFile profileImgFile = mRequest.getFile("uploadFile");
		if (profileImgFile != null && profileImgFile.getSize() > 0) {
			String realName = FileUploader.uploadFile(profileImgUploadPath, profileImgFile);
			if (realName == null || realName.equals("")) {
				throw new LogicException("FILE_UPLOAD_FAIL [ profile image ]", "003");
			}
			profileImgUploadPath = PROFILE_PATH + "/" + realName;
			isPictureChange = true;
		}

		// 정보 업데이트
		HashMap<String, Object> userInfo = new HashMap<String, Object>();
		userInfo.put("name", param.get("name"));
		userInfo.put("email", param.get("email"));
		userInfo.put("tel", param.get("tel"));
		userInfo.put("upt_user_seq", param.get("upt_user_seq"));

		if (isPwdChagne) {
			userInfo.put("pwd", Util.getSHA256(param.get("newPwd1").toString()));
		}

		if (isPictureChange) {
			userInfo.put("picture_path", profileImgUploadPath);
			userInfo.put("picture_name", profileImgFile.getOriginalFilename());
		}

		if (myDao.updateUserInfo(userInfo) != 1) {
			throw new LogicException("QUERY_FAIL [ users update query fail ]", "004");
		}

		request.getSession().setAttribute("S_USER_PICTURE", profileImgUploadPath);
		return resultMap;
	}

	public HashMap<String, Object> getMyLessonData(HashMap<String, Object> param) throws Exception{
		int totalCount = myDao.myLessonDataCount(param);
		PagingHelper pagingHelper = new PagingHelper(5, 10);
		List<HashMap<String, Object>> list = myDao.myLessonData(pagingHelper.getPagingParam(param, totalCount));
		return pagingHelper.getPageList(list, "getMyLessonDataList", totalCount);
	}

	public HashMap<String, Object> getAcaInfo(HashMap<String, Object> param) throws Exception{
		
		return myDao.getAcaInfo(param);
	}

	public ResultMap getFeScore(HashMap<String, Object> param) throws Exception{
		ResultMap resultMap = new ResultMap();
		resultMap.put("feList", myDao.getMyFeScoreList(param));
		return resultMap;
	}

	public ResultMap getCurrGrade(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("currList", myDao.getMyCurrGradeList(param));
		return resultMap;
	}

	public ResultMap getSoosiGrade(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("soosiList", myDao.getMySoosiGradeList(param));
		return resultMap;
	}
	
	public ResultMap getMyNewsList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		param.put("researchYN" , "N");
		param.put("minusPoint_flag" , "N");
		int lpResearchCnt = mobileStudentDao.getSatisfactionResearchList(param).size();//수업만족도조사
		int currResearchCnt = mobileStudentDao.getSatisfactionCurrResearchList(param).size();//과정만족도조사
		int asgmtCnt = myDao.getMyAsgmtCount(param); //과제
		int reportCardCnt = myDao.getReportCardCount(param);//성적
		
		resultMap.put("lpResearchCnt", lpResearchCnt);
		resultMap.put("currResearchCnt", currResearchCnt);
		resultMap.put("asgmtCnt", asgmtCnt);
		resultMap.put("reportCardCnt", reportCardCnt);
		
		return resultMap;
	}

}
