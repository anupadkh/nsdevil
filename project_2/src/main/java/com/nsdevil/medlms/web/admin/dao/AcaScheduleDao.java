package com.nsdevil.medlms.web.admin.dao;

import java.util.HashMap;
import java.util.List;


public interface AcaScheduleDao {

	public List<HashMap<String, Object>> getAcaLList() throws Exception;

	public List<HashMap<String, Object>> getAcaMSList(HashMap<String, Object> param) throws Exception;

	public int insertAcaSchedule(HashMap<String, Object> param) throws Exception;

	public int updateAcaSchedule(HashMap<String, Object> param) throws Exception;

	public int updateUseFlagMpTarget(HashMap<String, Object> param) throws Exception;

	public int insertUseFlagMpTarget(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getAcaSchedule(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getAcaScheduleHeader(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getAcaScheduleItem(HashMap<String, Object> param) throws Exception;

	public int getAcaScheduleModify(HashMap<String, Object> param) throws Exception;

	public int updateUseFlagAcaSchedule(HashMap<String, Object> param) throws Exception;
	
}
