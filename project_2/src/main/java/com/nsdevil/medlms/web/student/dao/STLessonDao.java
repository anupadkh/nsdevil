package com.nsdevil.medlms.web.student.dao;

import java.util.HashMap;
import java.util.List;


public interface STLessonDao {
	public List<HashMap<String, Object>> getLeftMenuAcademicYearList(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getLeftMenuCurriculumList(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getLeftMenuLessonPlanList(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getLessonPlanOne(HashMap<String, Object> param) throws Exception;

	public int getLessonPlanDefaultCount(HashMap<String, Object> param) throws Exception;

	public String getAttendance(HashMap<String, Object> param) throws Exception;

	public int getAttendanceCount(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getSTFormationEvaluationList(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getAssignMentSTList(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getAssignMentSubmitST(HashMap<String, Object> param) throws Exception;

	public int getAssignMentLpseq(HashMap<String, Object> param) throws Exception;

	public int insertAssignMentSubmit(HashMap<String, Object> param) throws Exception;

	public int updateAssignMentSubmit(HashMap<String, Object> param) throws Exception;

	public int getAssignMentFeedbackYN(HashMap<String, Object> param) throws Exception;

	public int updateAssignMentSubmitFileDel(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getLessonPlanDefault(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getAssignMentGroupStList(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getLessonCurrGradeList(HashMap<String, Object> param) throws Exception;

	public String getCurrAcaState(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getLessonSoosiGradeList(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getLessonFeScoreList(HashMap<String, Object> param) throws Exception;

	public int getLeftMenuLessonPlanListCount(HashMap<String, Object> param) throws Exception;

	public String getCurrEndChk(HashMap<String, Object> param) throws Exception;

	public String getLessonEndChk(HashMap<String, Object> param) throws Exception;

	public int getTodayLessonPeriodSeq(HashMap<String, Object> param) throws Exception;
	
}
