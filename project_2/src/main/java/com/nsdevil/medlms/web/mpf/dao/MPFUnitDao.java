package com.nsdevil.medlms.web.mpf.dao;

import java.util.List;
import java.util.HashMap;


public interface MPFUnitDao {
	public List<HashMap<String, Object>> getlessonScheduleList(HashMap<String, Object> param) throws Exception ;

	public int insertUnit(HashMap<String, Object> param) throws Exception ;

	public int updateUnit(HashMap<String, Object> param) throws Exception ;

	public void insertMpUnitLesson(HashMap<String, Object> param) throws Exception ;

	public int insertTLO(HashMap<String, Object> param) throws Exception ;

	public int updateTLO(HashMap<String, Object> param) throws Exception ;

	public int insertELO(HashMap<String, Object> param) throws Exception ;

	public int updateELO(HashMap<String, Object> param) throws Exception ;

	public List<HashMap<String, Object>> getUnitList(HashMap<String, Object> param) throws Exception ;

	public int updateUnitALLUseflag(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getLessonAddPC(HashMap<String, Object> param) throws Exception ;

	public List<HashMap<String, Object>> getUnitCode(HashMap<String, Object> param) throws Exception; 


}
