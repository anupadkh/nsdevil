package com.nsdevil.medlms.web.admin.dao;

import java.util.HashMap;
import java.util.List;


public interface BoardDao {
	public int deleteBoardAttach(HashMap<String, Object> param) throws Exception;
	public int deleteMpBoardShowTarget(HashMap<String, Object> param) throws Exception;
	public int deleteBoard(HashMap<String, Object> param) throws Exception;
	public int getNoticeContentsCount(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getNoticeContentsList(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getAcademicSystemInfo(String aca_seq) throws Exception;
	public HashMap<String, Object> getNoticeDetailInfo(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getNextNoticeInfo(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getPrevNoticeInfo(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getNoticeAttachFileList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getAcademicSystemList(HashMap<String, Object> param) throws Exception;
	public int insertBoard(HashMap<String, Object> param) throws Exception;
	public int insertMpBoardShowTarget(HashMap<String, Object> param) throws Exception;
	public int insertBoardAttach(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getBoardShowTargetList(HashMap<String, Object> param) throws Exception;
	public int updateBoard(HashMap<String, Object> param) throws Exception;
	public int deleteBoardAttachForBoardAttachSeq(String boardAttachSeq) throws Exception;
	public int getOSCEContentsCount(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getOSCEContentsList(HashMap<String, Object> param) throws Exception;
	public int getCPXContentsCount(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getCPXContentsList(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getOSCEDetailInfo(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getCPXDetailInfo(HashMap<String, Object> param) throws Exception;
	public int getPBLContentsCount(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getPBLContentsList(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getPBLDetailInfo(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getNextPBLInfo(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getPrevPBLInfo(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getBoardList(HashMap<String, Object> param) throws Exception;
	public int updateBoardShowFlag(HashMap<String, Object> param) throws Exception;
}
