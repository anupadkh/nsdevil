package com.nsdevil.medlms.web.common.dao;

import java.util.HashMap;
import java.util.List;


public interface SchoolLifeDao {
	public int getQNAContentsCount(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getQNAContentsList(HashMap<String, Object> param) throws Exception;
	public int insertAnswer(HashMap<String, Object> param) throws Exception;
	public int getExistQuestionCount(HashMap<String, Object> param) throws Exception;
	public int getExistAnswerCount(HashMap<String, Object> param) throws Exception;
	public int updateAnswer(HashMap<String, Object> param) throws Exception;
	public int deleteAnswer(HashMap<String, Object> param) throws Exception;
	public int insertQuestion(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getNoticeAttachFileList(HashMap<String, Object> param) throws Exception;
	public int getNoticeContentsCount(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getNoticeContentsList(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getNoticeDetailInfo(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getNextNoticeInfo(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getPrevNoticeInfo(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getAnswerUserLevel(HashMap<String, Object> param) throws Exception;
}
