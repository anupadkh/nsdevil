package com.nsdevil.medlms.web.admin.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nsdevil.medlms.common.constants.Constants;
import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.admin.service.BoardService;
import com.nsdevil.medlms.web.common.service.CommonService;

@Controller
public class BoardController extends ExceptionController {
	
	@Autowired
	private BoardService service;
	
	@Autowired
	private CommonService commonService;
	
	/************************************************************************************************/
	/*                                        전체 게시판 관리                                                                           */
	/************************************************************************************************/
	@RequestMapping(value="admin/board/list", method = RequestMethod.GET)
	public String boardListView() throws Exception {
		return "admin/board/boardList";
	}
	
	//전체게시판 목록
	@RequestMapping(value = "/ajax/admin/board/list", method = RequestMethod.GET)
	public String boardList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_menu_code", param.get("searchBoardMenuCode"));
		param.put("board_name", param.get("searchText"));
		model.addAllAttributes(service.getBoardList(param));
		return JSON_VIEW;
	}
	
	//게시판 on/off 관리
	@RequestMapping(value = "/ajax/admin/board/show/modify", method = RequestMethod.POST)
	public String boardShowModify(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		param.put("board_m_seq", param.get("boardMSeq"));
		param.put("show_flag", param.get("showFlag"));
		model.addAllAttributes(service.boardShowModify(param));
		return JSON_VIEW;
	}
	
	
	@RequestMapping(value = "/ajax/admin/board/menu/list", method = RequestMethod.GET)
	public String boardMenuList(Model model) throws Exception {
		model.addAttribute("menuCodeList", commonService.getCodeList("board_menu_code"));
		return JSON_VIEW;
	}
	
	
	
	
	/************************************************************************************************/
	/*                                        공지사항                                                                                    */
	/************************************************************************************************/
	
	//공지사항 목록 화면
	@RequestMapping(value = "admin/board/notice/list", method = RequestMethod.GET)
	public String noticeListView() throws Exception {
		return "admin/board/notice/noticeList";
	}
	
	//공지사항 목록
	@RequestMapping(value = "/ajax/admin/board/notice/list", method = RequestMethod.GET)
	public String noticeList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.NOTICE_BOARD_CODE);
		param.put("listFunctionName", "noticeListView");
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		model.addAllAttributes(service.getNoticeList(param));
		return JSON_VIEW;
	}
	
	//공지사항 상세화면
	@RequestMapping(value = "/admin/board/notice/detail", method = RequestMethod.GET)
	public String noticeDetailView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "admin/board/notice/noticeDetail";
	}
	
	//공지사항 등록화면
	@RequestMapping(value = "/admin/board/notice/create", method = RequestMethod.GET)
	public String noticeCreateView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		return "admin/board/notice/noticeCreate";
	}
	
	//공지사항 등록
	@RequestMapping(value = "/ajax/admin/board/notice/create", method = RequestMethod.POST)
	public String noticeCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.NOTICE_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.noticeCreate(param, request));
		return JSON_VIEW;
	}
	
	//공지사항 학사 체계 가져오기
	@RequestMapping(value = "/ajax/admin/board/academic/list", method = RequestMethod.GET)
	public String academicList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(service.getAcademicList(param));
		return JSON_VIEW;
	}
	
	//공지사항 상세
	@RequestMapping(value = "/ajax/admin/board/notice/detail", method = RequestMethod.GET)
	public String noticeDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.NOTICE_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(service.getNoticeDetail(param));
		return JSON_VIEW;
	}
	
	//게시판 글 삭제
	@RequestMapping(value = "/ajax/admin/board/remove", method = RequestMethod.POST)
	public String removeBoard(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("remove_board_seqs", param.get("removeBoardSeqs"));
		Util.requiredCheck(param, new String[] {"s_user_seq", "remove_board_seqs"});
		model.addAllAttributes(service.removeBoard(param));
		return JSON_VIEW;
	}
	
	//공지사항 수정화면
	@RequestMapping(value = "/admin/board/notice/modify", method = RequestMethod.POST)
	public String noticeModifyView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "/admin/board/notice/noticeModify";
	}
	
	//공지사항 수정상세
	@RequestMapping(value = "/ajax/admin/board/notice/modify/detail", method = RequestMethod.GET)
	public String noticeModifyDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.NOTICE_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(service.getNoticeDetail(param));
		return JSON_VIEW;
	}
	
		
	//공지사항 수정
	@RequestMapping(value = "/ajax/admin/board/notice/modify", method = RequestMethod.POST)
	public String noticeModify(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.NOTICE_BOARD_CODE);
		param.put("board_seq", param.get("boardSeq"));
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.noticeModify(param, request));
		return JSON_VIEW;
	}
	
	
	/************************************************************************************************/
	/*                                        학습자료실                                                                                  */
	/************************************************************************************************/
	
	//학습자료실 목록 화면
	@RequestMapping(value = "admin/board/learning/list", method = RequestMethod.GET)
	public String learningListView() throws Exception {
		return "admin/board/learning/learningList";
	}
	
	//학습자료실 목록
	@RequestMapping(value = "/ajax/admin/board/learning/list", method = RequestMethod.GET)
	public String learningList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.LEARNING_BOARD_CODE);
		param.put("listFunctionName", "boardListView");
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		model.addAllAttributes(commonService.getBoardList(param));
		return JSON_VIEW;
	}
	
	
	//학습자료실 등록화면
	@RequestMapping(value = "/admin/board/learning/create", method = RequestMethod.GET)
	public String learningCreateView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		return "admin/board/learning/learningCreate";
	}
	
	//학습자료실 등록
	@RequestMapping(value = "/ajax/admin/board/learning/create", method = RequestMethod.POST)
	public String learningCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.LEARNING_BOARD_CODE);
		param.put("board_cate_code", param.get("boardCateCode"));
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(commonService.boardCreate(param, request));
		return JSON_VIEW;
	}
	
	//학습자료실 상세화면
	@RequestMapping(value = "/admin/board/learning/detail", method = RequestMethod.GET)
	public String learningDetailView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "admin/board/learning/learningDetail";
	}
	
	//학습자료실 상세
	@RequestMapping(value = "/ajax/admin/board/learning/detail", method = RequestMethod.GET)
	public String learningDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.LEARNING_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(commonService.getBoardDetail(param));
		return JSON_VIEW;
	}
	
	//학습자료실 수정화면
	@RequestMapping(value = "/admin/board/learning/modify", method = RequestMethod.POST)
	public String learningModifyView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "/admin/board/learning/learningModify";
	}
	
	//학습자료실 수정상세
	@RequestMapping(value = "/ajax/admin/board/learning/modify/detail", method = RequestMethod.GET)
	public String learningModifyDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.LEARNING_BOARD_CODE);
		param.put("board_cate_code", param.get("boardCateCode"));
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(commonService.getBoardDetail(param));
		return JSON_VIEW;
	}
	
		
	//학습자료실 수정
	@RequestMapping(value = "/ajax/admin/board/learning/modify", method = RequestMethod.POST)
	public String learningModify(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.NOTICE_BOARD_CODE);
		param.put("board_seq", param.get("boardSeq"));
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(commonService.boardModify(param, request));
		return JSON_VIEW;
	}
	
	/************************************************************************************************/
	/*                                         핫뉴스                                                                                     */
	/************************************************************************************************/
	
	//핫뉴스 목록 화면
	@RequestMapping(value = "admin/board/hotnews/list", method = RequestMethod.GET)
	public String hotnewsListView() throws Exception {
		return "admin/board/hotnews/hotnewsList";
	}
	
	//핫뉴스 목록
	@RequestMapping(value = "/ajax/admin/board/hotnews/list", method = RequestMethod.GET)
	public String hotnewsList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.HOTNEWS_BOARD_CODE);
		param.put("listFunctionName", "boardListView");
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		model.addAllAttributes(commonService.getBoardList(param));
		return JSON_VIEW;
	}
	
	//핫뉴스 등록화면
	@RequestMapping(value = "/admin/board/hotnews/create", method = RequestMethod.GET)
	public String hotnewsCreateView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		return "admin/board/hotnews/hotnewsCreate";
	}

	//핫뉴스 등록
	@RequestMapping(value = "/ajax/admin/board/hotnews/create", method = RequestMethod.POST)
	public String hotnewsCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.HOTNEWS_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(commonService.boardCreate(param, request));
		return JSON_VIEW;
	}
	
	//핫뉴스 상세화면
	@RequestMapping(value = "/admin/board/hotnews/detail", method = RequestMethod.GET)
	public String hotnewsDetailView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "admin/board/hotnews/hotnewsDetail";
	}
	
	//핫뉴스 상세
	@RequestMapping(value = "/ajax/admin/board/hotnews/detail", method = RequestMethod.GET)
	public String hotnewsDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.HOTNEWS_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(commonService.getBoardDetail(param));
		return JSON_VIEW;
	}
	
	//핫뉴스 수정화면
	@RequestMapping(value = "/admin/board/hotnews/modify", method = RequestMethod.POST)
	public String hotnewsModifyView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "/admin/board/hotnews/hotnewsModify";
	}
	
	//핫뉴스 수정상세
	@RequestMapping(value = "/ajax/admin/board/hotnews/modify/detail", method = RequestMethod.GET)
	public String hotnewsModifyDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.HOTNEWS_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(commonService.getBoardDetail(param));
		return JSON_VIEW;
	}
		
	//핫뉴스 수정
	@RequestMapping(value = "/ajax/admin/board/hotnews/modify", method = RequestMethod.POST)
	public String hotnewsModify(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.HOTNEWS_BOARD_CODE);
		param.put("board_seq", param.get("boardSeq"));
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(commonService.boardModify(param, request));
		return JSON_VIEW;
	}
	
	
	/************************************************************************************************/
	/*                                         FAQ                                                  */
	/************************************************************************************************/
	
	//FAQ 목록 화면
	@RequestMapping(value = "admin/board/faq/list", method = RequestMethod.GET)
	public String faqListView() throws Exception {
		return "admin/board/faq/faqList";
	}
	
	//FAQ 목록
	@RequestMapping(value = "/ajax/admin/board/faq/list", method = RequestMethod.GET)
	public String faqList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.FAQ_BOARD_CODE);
		param.put("listFunctionName", "boardListView");
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		model.addAllAttributes(commonService.getBoardList(param));
		return JSON_VIEW;
	}
	
	//FAQ 등록화면
	@RequestMapping(value = "/admin/board/faq/create", method = RequestMethod.GET)
	public String faqCreateView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		return "admin/board/faq/faqCreate";
	}

	//FAQ 등록
	@RequestMapping(value = "/ajax/admin/board/faq/create", method = RequestMethod.POST)
	public String faqCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.FAQ_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(commonService.boardCreate(param, request));
		return JSON_VIEW;
	}
	
	//FAQ 상세화면
	@RequestMapping(value = "/admin/board/faq/detail", method = RequestMethod.GET)
	public String faqDetailView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "admin/board/faq/faqDetail";
	}
	
	//FAQ 상세
	@RequestMapping(value = "/ajax/admin/board/faq/detail", method = RequestMethod.GET)
	public String faqDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.FAQ_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(commonService.getBoardDetail(param));
		return JSON_VIEW;
	}
	
	//FAQ 수정화면
	@RequestMapping(value = "/admin/board/faq/modify", method = RequestMethod.POST)
	public String faqModifyView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "/admin/board/faq/faqModify";
	}
	
	//FAQ 수정상세
	@RequestMapping(value = "/ajax/admin/board/faq/modify/detail", method = RequestMethod.GET)
	public String faqModifyDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.FAQ_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(commonService.getBoardDetail(param));
		return JSON_VIEW;
	}
		
	//FAQ 수정
	@RequestMapping(value = "/ajax/admin/board/faq/modify", method = RequestMethod.POST)
	public String faqModify(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.FAQ_BOARD_CODE);
		param.put("board_seq", param.get("boardSeq"));
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(commonService.boardModify(param, request));
		return JSON_VIEW;
	}
	
	
	/************************************************************************************************/
	/*                                         임상술기(OSCE) 동영상                                                               */
	/************************************************************************************************/
	
	//OSCE 목록 화면
	@RequestMapping(value = "admin/board/osce/list", method = RequestMethod.GET)
	public String osceListView() throws Exception {
		return "admin/board/osce/osceList";
	}
	
	//OSCE 목록
	@RequestMapping(value = "/ajax/admin/board/osce/list", method = RequestMethod.GET)
	public String osceList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.OSCE_BOARD_CODE);
		param.put("search_osce_code", param.get("searchOSCECode"));
		param.put("listFunctionName", "boardListView");
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		model.addAllAttributes(service.getOSCEList(param));
		return JSON_VIEW;
	}
	
	//OSCE 등록화면
	@RequestMapping(value = "/admin/board/osce/create", method = RequestMethod.GET)
	public String osceCreateView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAttribute("code_name", commonService.getCodeName("osce_code_name"));
		return "admin/board/osce/osceCreate";
	}

	//OSCE 등록
	@RequestMapping(value = "/ajax/admin/board/osce/create", method = RequestMethod.POST)
	public String osceCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.OSCE_BOARD_CODE);
		param.put("board_cate_code", param.get("boardCateCode"));
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(commonService.boardCreate(param, request));
		return JSON_VIEW;
	}
	
	//OSCE 상세화면
	@RequestMapping(value = "/admin/board/osce/detail", method = RequestMethod.GET)
	public String osceDetailView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "admin/board/osce/osceDetail";
	}
	
	//OSCE 상세
	@RequestMapping(value = "/ajax/admin/board/osce/detail", method = RequestMethod.GET)
	public String osceDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.OSCE_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(service.getOSCEDetail(param));
		return JSON_VIEW;
	}
	
	//OSCE 수정화면
	@RequestMapping(value = "/admin/board/osce/modify", method = RequestMethod.POST)
	public String osceModifyView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		model.addAttribute("code_name", commonService.getCodeName("osce_code_name"));
		return "/admin/board/osce/osceModify";
	}
	
	//OSCE 수정상세
	@RequestMapping(value = "/ajax/admin/board/osce/modify/detail", method = RequestMethod.GET)
	public String osceModifyDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.OSCE_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(service.getOSCEDetail(param));
		return JSON_VIEW;
	}
		
	//OSCE 수정
	@RequestMapping(value = "/ajax/admin/board/osce/modify", method = RequestMethod.POST)
	public String osceModify(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.CPX_BOARD_CODE);
		param.put("board_cate_code", param.get("boardCateCode"));
		param.put("board_seq", param.get("boardSeq"));
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(commonService.boardModify(param, request));
		return JSON_VIEW;
	}
	
	
	/************************************************************************************************/
	/*                                         임상표현(CPX) 동영상                                                                 */
	/************************************************************************************************/
	
	//CPX 목록 화면
	@RequestMapping(value = "admin/board/cpx/list", method = RequestMethod.GET)
	public String cpxListView() throws Exception {
		return "admin/board/cpx/cpxList";
	}
	
	//CPX 목록
	@RequestMapping(value = "/ajax/admin/board/cpx/list", method = RequestMethod.GET)
	public String cpxList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.CPX_BOARD_CODE);
		param.put("search_cpx_code", param.get("searchCPXCode"));
		param.put("listFunctionName", "boardListView");
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		model.addAllAttributes(service.getCPXList(param));
		return JSON_VIEW;
	}
	
	//CPX 등록화면
	@RequestMapping(value = "/admin/board/cpx/create", method = RequestMethod.GET)
	public String cpxCreateView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAttribute("code_name", commonService.getCodeName("cpx_code_name"));
		return "admin/board/cpx/cpxCreate";
	}

	//CPX 등록
	@RequestMapping(value = "/ajax/admin/board/cpx/create", method = RequestMethod.POST)
	public String cpxCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.CPX_BOARD_CODE);
		param.put("board_cate_code", param.get("boardCateCode"));
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(commonService.boardCreate(param, request));
		return JSON_VIEW;
	}
	
	//CPX 상세화면
	@RequestMapping(value = "/admin/board/cpx/detail", method = RequestMethod.GET)
	public String cpxDetailView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "admin/board/cpx/cpxDetail";
	}
	
	//CPX 상세
	@RequestMapping(value = "/ajax/admin/board/cpx/detail", method = RequestMethod.GET)
	public String cpxDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.CPX_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(service.getCPXDetail(param));
		return JSON_VIEW;
	}
	
	//CPX 수정화면
	@RequestMapping(value = "/admin/board/cpx/modify", method = RequestMethod.POST)
	public String cpxModifyView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		model.addAttribute("code_name", commonService.getCodeName("cpx_code_name"));
		return "/admin/board/cpx/cpxModify";
	}
	
	//CPX 수정상세
	@RequestMapping(value = "/ajax/admin/board/cpx/modify/detail", method = RequestMethod.GET)
	public String cpxModifyDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.CPX_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(service.getCPXDetail(param));
		return JSON_VIEW;
	}
		
	//CPX 수정
	@RequestMapping(value = "/ajax/admin/board/cpx/modify", method = RequestMethod.POST)
	public String cpxModify(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.CPX_BOARD_CODE);
		param.put("board_cate_code", param.get("boardCateCode"));
		param.put("board_seq", param.get("boardSeq"));
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(commonService.boardModify(param, request));
		return JSON_VIEW;
	}
	
	
	/************************************************************************************************/
	/*                                        PBL												    */
	/************************************************************************************************/
	
	//PBL 목록 화면
	@RequestMapping(value = "admin/board/pbl/list", method = RequestMethod.GET)
	public String pblListView() throws Exception {
		return "admin/board/pbl/pblList";
	}
	
	//PBL 목록
	@RequestMapping(value = "/ajax/admin/board/pbl/list", method = RequestMethod.GET)
	public String pblList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.PBL_BOARD_CODE);
		param.put("listFunctionName", "pblListView");
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		model.addAllAttributes(service.getPBLList(param));
		return JSON_VIEW;
	}
	
	//PBL 상세화면
	@RequestMapping(value = "/admin/board/pbl/detail", method = RequestMethod.GET)
	public String pblDetailView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "admin/board/pbl/pblDetail";
	}
	
	//PBL 등록화면
	@RequestMapping(value = "/admin/board/pbl/create", method = RequestMethod.GET)
	public String pblCreateView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		return "admin/board/pbl/pblCreate";
	}
	
	//PBL 등록
	@RequestMapping(value = "/ajax/admin/board/pbl/create", method = RequestMethod.POST)
	public String pblCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.PBL_BOARD_CODE);
		param.put("start_date", param.get("startDateTime"));
		param.put("end_date", param.get("endDateTime"));
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.pblCreate(param, request));
		return JSON_VIEW;
	}
	
	//PBL 상세
	@RequestMapping(value = "/ajax/admin/board/pbl/detail", method = RequestMethod.GET)
	public String pblDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.PBL_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(service.getPBLDetail(param));
		return JSON_VIEW;
	}
	
	//PBL 수정화면
	@RequestMapping(value = "/admin/board/pbl/modify", method = RequestMethod.POST)
	public String pblModifyView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "/admin/board/pbl/pblModify";
	}
	
	//PBL 수정상세
	@RequestMapping(value = "/ajax/admin/board/pbl/modify/detail", method = RequestMethod.GET)
	public String pblModifyDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.PBL_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(service.getPBLDetail(param));
		return JSON_VIEW;
	}
	
		
	//PBL 수정
	@RequestMapping(value = "/ajax/admin/board/pbl/modify", method = RequestMethod.POST)
	public String pblModify(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.PBL_BOARD_CODE);
		param.put("board_seq", param.get("boardSeq"));
		param.put("start_date", param.get("startDateTime"));
		param.put("end_date", param.get("endDateTime"));
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.pblModify(param, request));
		return JSON_VIEW;
	}
}

