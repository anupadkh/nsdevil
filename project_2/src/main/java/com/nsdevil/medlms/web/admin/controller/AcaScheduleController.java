package com.nsdevil.medlms.web.admin.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.admin.service.AcaScheduleService;

@Controller
public class AcaScheduleController extends ExceptionController {
	
	@Autowired
	private AcaScheduleService acaScheduleService;
	
	//학사일정 관리 매핑
	@RequestMapping(value = "/admin/acaSchedule/scheduleManagement", method = RequestMethod.GET)
	public String scheduleManagementView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		
		model.addAttribute("lacaList", acaScheduleService.getLacaList());
		
		return "admin/acaSchedule/acaSchedule";
	}		
	
	//학사쳬게 중소분류 가져오기
	@RequestMapping(value = "/ajax/admin/acaSchedule/scheduleManagement/msAcaList", method = RequestMethod.POST)
	public String acasystemMSList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"aca_system_seq"});
		model.addAllAttributes(acaScheduleService.getAcaMSList(param));
		return JSON_VIEW;
	}
	
	//학사일정 저장 수정
	@RequestMapping(value = "/ajax/admin/acaSchedule/scheduleManagement/insert", method = RequestMethod.POST)
	public String acaScheduleInsert(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(acaScheduleService.insertAcaSchedule(param, request));
		return JSON_VIEW;
	}	

	//학사일정 리스트 가져오기
	@RequestMapping(value = "/ajax/admin/acaSchedule/scheduleManagement/list", method = RequestMethod.POST)
	public String acaScheduleList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		model.addAllAttributes(acaScheduleService.getAcaSchedule(param));
		return JSON_VIEW;
	}
	
	//학사일정 수정 가져오기
	@RequestMapping(value = "/ajax/admin/acaSchedule/scheduleManagement/oneList", method = RequestMethod.POST)
	public String acaScheduleoneList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		model.addAllAttributes(acaScheduleService.getAcaScheduleOneList(param));
		return JSON_VIEW;
	}
	
	//학사일정 수정 가져오기
	@RequestMapping(value = "/ajax/admin/acaSchedule/scheduleManagement/modify", method = RequestMethod.POST)
	public String acaScheduleModify(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"as_seq"});
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(acaScheduleService.getAcaScheduleModify(param));
		return JSON_VIEW;
	}
	
	//학사일정 삭제하기
	@RequestMapping(value = "/ajax/admin/acaSchedule/scheduleManagement/delete", method = RequestMethod.POST)
	public String acaScheduleDelte(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"as_seq"});
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(acaScheduleService.deleteAcaSchedule(param));
		return JSON_VIEW;
	}
	
	//개인일정 관리 매핑
	@RequestMapping(value = "/admin/acaSchedule/MYscheduleMemo", method = RequestMethod.GET)
	public String MYscheduleMemoView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		
		return "admin/acaSchedule/MYscheduleMemo";
	}	
}
