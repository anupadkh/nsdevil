package com.nsdevil.medlms.web.common.dao;

import java.util.HashMap;

import org.springframework.stereotype.Repository;
@Repository
public interface CarteDao {
	public HashMap<String, Object> getCarteInfo() throws Exception; 
}
