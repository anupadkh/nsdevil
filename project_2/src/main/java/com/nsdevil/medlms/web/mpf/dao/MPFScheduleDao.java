package com.nsdevil.medlms.web.mpf.dao;

import java.util.List;
import java.util.HashMap;


public interface MPFScheduleDao {
	public List<HashMap<String, Object>> getSchedule(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getSelectedDaySchedule(HashMap<String, Object> param) throws Exception;
	public int insertLessonPlan(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getPfList(HashMap<String, Object> param) throws Exception;
	public void saveSchedule(HashMap<String, Object> param) throws Exception;
	public void insertLessonMethod(HashMap<String, Object> param) throws Exception;
	public void deleteAllSchedule(HashMap<String, Object> param) throws Exception;
	public void changeMemoDate(HashMap<String, Object> param) throws Exception;
	public void changeLessonPlanDate(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getAcademicList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getCurrPfList(HashMap<String, Object> param) throws Exception;
}
