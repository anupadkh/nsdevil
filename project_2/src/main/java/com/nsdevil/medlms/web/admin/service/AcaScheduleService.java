package com.nsdevil.medlms.web.admin.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.exception.RuntimeLogicException;
import com.nsdevil.medlms.common.util.FileUploader;
import com.nsdevil.medlms.common.util.PagingHelper;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.admin.dao.AcaScheduleDao;
import com.nsdevil.medlms.web.admin.dao.AcademicDao;


@Service
public class AcaScheduleService {
	
	@Autowired
	private AcaScheduleDao acaScheduleDao;

	public List<HashMap<String, Object>> getLacaList() throws Exception {
		
		return acaScheduleDao.getAcaLList();
	}

	public ResultMap getAcaMSList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("acaMSList", acaScheduleDao.getAcaMSList(param));
		
		return resultMap;
	}

	@Transactional
	public ResultMap insertAcaSchedule(HashMap<String, Object> param, HttpServletRequest request) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		String[] aca_system_seq = request.getParameterValues("chk");
		
		if(request.getParameter("as_seq").toString().isEmpty()) {			
			if(acaScheduleDao.insertAcaSchedule(param) < 1)
				throw new RuntimeLogicException("QUERY_FAIL [academic_schedule insert query fail ]", "004");
		}else {
			if(acaScheduleDao.updateAcaSchedule(param) < 1)
				throw new RuntimeLogicException("QUERY_FAIL [academic_schedule update query fail ]", "004");
		}
		
		acaScheduleDao.updateUseFlagMpTarget(param);
		
		if(aca_system_seq != null) {
			for(int i=0; i<aca_system_seq.length; i++) {
				param.put("aca_system_seq", aca_system_seq[i]);
				if(acaScheduleDao.insertUseFlagMpTarget(param) < 1)
					throw new RuntimeLogicException("QUERY_FAIL [mp_academic_schedule_target insert query fail ]", "004");
			}
		}
				
		return resultMap;
	}

	public ResultMap getAcaSchedule(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("list", acaScheduleDao.getAcaSchedule(param));
				
		return resultMap;
	}

	public ResultMap getAcaScheduleOneList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("listH", acaScheduleDao.getAcaScheduleHeader(param));
		resultMap.put("listI", acaScheduleDao.getAcaScheduleItem(param));
				
		return resultMap;
	}

	public ResultMap getAcaScheduleModify(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		if(acaScheduleDao.getAcaScheduleModify(param) < 1)
			throw new RuntimeLogicException("QUERY_FAIL [academic_schedule date update query fail ]", "004");
				
		return resultMap;
	}

	@Transactional
	public ResultMap deleteAcaSchedule(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		if(acaScheduleDao.updateUseFlagAcaSchedule(param) < 1)
			throw new RuntimeLogicException("QUERY_FAIL [academic_schedule delete update query fail ]", "004");
			
		acaScheduleDao.updateUseFlagMpTarget(param); 
		
		return resultMap;
	}	
}
