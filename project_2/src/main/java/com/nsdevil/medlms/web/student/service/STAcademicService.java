package com.nsdevil.medlms.web.student.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.web.student.dao.STAcademicDao;

@Service
public class STAcademicService {

	@Autowired
	private STAcademicDao stAcademicDao;
	
	public ResultMap getCourseApplicationList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("caList", stAcademicDao.getCourseApplicationList(param));
		
		return resultMap;
	}

	public ResultMap getCourseApplicationDetail(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();

		resultMap.put("caInfo", stAcademicDao.getCourseApplicationDetail(param));
		resultMap.put("caAttach", stAcademicDao.getCourseApplicationAttach(param));
		resultMap.put("refundInfo", stAcademicDao.getRefund(param));
		resultMap.put("notesInfo", stAcademicDao.getNotes(param));
		
		return resultMap;
	}

	public ResultMap insertCa(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		int cnt = stAcademicDao.getCaStCount(param);
		
		HashMap<String, Object> list = stAcademicDao.getCourseApplicationDetail(param);
		
		if(list.get("end_chk").toString().equals("Y")) {
			resultMap.put("status", "201");
		}else if(cnt > 0)
			resultMap.put("status", "202");
		else{
			stAcademicDao.insertCaSt(param);
		}
		
		return resultMap;
	}
}