package com.nsdevil.medlms.web.admin.dao;

import java.util.HashMap;
import java.util.List;


public interface UserDao {
	public int insertProfessorInfo(HashMap<String, Object> param) throws Exception;
	public int getPFContentsCount(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getPFContentsList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getYearAcaList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getAcaList(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getPFDetail(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getCurriculumList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getLessonPlanList(HashMap<String, Object> param) throws Exception;
	public int updateUserPwdInit(HashMap<String, Object> param) throws Exception; 
	public int getUserPwdCount(HashMap<String, Object> param) throws Exception;
	public int getExistProfessorIdCount(HashMap<String, Object> param) throws Exception;
	public int getExistUserEmailCount(HashMap<String, Object> param) throws Exception;
	public int getExistUserIdCount(HashMap<String, Object> param) throws Exception;
	public int updateProfessorInfo(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getCodeCountList(HashMap<String, Object> param) throws Exception;
	public int getNoCodePFCount(HashMap<String, Object> param) throws Exception;
	public int deleteCode(HashMap<String, Object> param) throws Exception;
	public String getMaxCode(HashMap<String, Object> param) throws Exception;
	public int insertCode(HashMap<String, Object> param) throws Exception;
	public int updateCode(HashMap<String, Object> param) throws Exception;
	public int insertStaffInfo(HashMap<String, Object> param) throws Exception;
	public int getStaffContentsCount(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getStaffContentsList(HashMap<String, Object> param) throws Exception;
	public int deleteUsersCode(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getAttendCountList(HashMap<String, Object> param) throws Exception;
	public int getNoCodeCount(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getStaffDetail(HashMap<String, Object> param) throws Exception;
	public int updateStaffInfo(HashMap<String, Object> param) throws Exception;
}

