package com.nsdevil.medlms.web.common.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.exception.RuntimeLogicException;
import com.nsdevil.medlms.web.common.dao.RoomReserveDao;

@Service
public class RoomReserveService {

	@Autowired
	private RoomReserveDao roomReserveDao;		
	
	public ResultMap getReserveList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("list", roomReserveDao.getReserveList(param));
		return resultMap;
	}

	public ResultMap insertReserve(HashMap<String, Object> param)  throws Exception{
		ResultMap resultMap = new ResultMap();
		if(roomReserveDao.insertReserve(param) == 0)
			throw new RuntimeLogicException("QUERY_FAIL [ reserve insert or update query fail ]", "004");
		return resultMap;
	}

	public ResultMap getFacilityCode(HashMap<String, Object> param) throws Exception{
		ResultMap resultMap = new ResultMap();
		resultMap.put("facilityList", roomReserveDao.getFacilityCode(param));
		return resultMap;
	}

	public ResultMap getReserveDetailInfo(HashMap<String, Object> param) throws Exception{
		ResultMap resultMap = new ResultMap();
		resultMap.put("reserveInfo", roomReserveDao.getReserveDetail(param));
		return resultMap;
	}
}
