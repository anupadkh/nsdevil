package com.nsdevil.medlms.web.common.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.web.common.service.CarteService;

@Controller
public class CarteController extends ExceptionController {
	
	@Autowired
	private CarteService service;
	
	//식단
	@RequestMapping(value = {"/common/SLife/carte/detail", "/common/SLife/carte"}, method = RequestMethod.GET)
	public String carteDetailView(HttpServletRequest request) throws Exception {
		return "common/SLife/carte/carteDetail";
	}
	
	@RequestMapping(value ="/ajax/common/SLife/carte/detail", method = RequestMethod.GET)
	public String getCarteDetail(HttpServletRequest request, Model model) throws Exception {
		model.addAllAttributes(service.getCarteDetail());
		return JSON_VIEW;
	}
}
