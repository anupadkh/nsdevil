package com.nsdevil.medlms.web.mpf.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.exception.RuntimeLogicException;
import com.nsdevil.medlms.web.mpf.dao.MPFUnitDao;
import com.nsdevil.medlms.web.pf.dao.PFLessonDao;

@Service
public class MPFUnitService {

	@Autowired
	private MPFUnitDao unitDao;

	@Autowired
	private PFLessonDao pfLessonDao;
	
	public List<HashMap<String, Object>> lessonScheduleList(HashMap<String, Object> param) throws Exception {
		// TODO Auto-generated method stub
		return unitDao.getlessonScheduleList(param);
	}

	public ResultMap unitCreate(HashMap<String, Object> param) {
		ResultMap resultMap = new ResultMap();
		return resultMap;
	}

	@Transactional
	public ResultMap createUnit(String reg_user_seq, 
			String curr_seq, String[] unit_seq, 
			String[] unit_name, //단원 명 
			String[] lp_seq,  //수업 시퀀스 ex) 1,2 콤마 구분으로 여러개 들어올 수 있음
			String[] skill, //기술
			String[] teaching_method, //수업방법 
			String[] evaluation_method, //평가방법
			String[] domain_code, //영역
			String[] level_code,  //수준
			String[] content,  //내용
			String[] tlo_seq,  //TLO 시퀀스
			String[] elo_seq   //ELO 시퀀스
			)  throws Exception {
		
		ResultMap resultMap = new ResultMap();
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("reg_user_seq", reg_user_seq);
		param.put("curr_seq", curr_seq);
		
		unitDao.updateUnitALLUseflag(param);
		
		if(curr_seq.isEmpty() || curr_seq == null)
			throw new RuntimeLogicException("잘못된 접근입니다.");
		
		if(unit_seq != null) {
			//단원 수만큼 반복문 돈다...
			for(int i=0; i<unit_seq.length;i++){
				//new_yn 값 수정 = Y, 등록 = N
				String new_yn = unit_seq[i].split("_")[0];
				String this_unit_seq = unit_seq[i].split("_")[1];
				List<String> lp_seqList = new ArrayList<String>();
	
				//수업계획서 추가한거 반복문 돈다.. 한단원에 여러건 수업 추가 됨. 그리고 수업 여러개 묶어서 하나로 추가도 됨..
				for(int lp_index=0;lp_index<lp_seq.length;lp_index++){
					String lp_value[] = lp_seq[lp_index].split("_");
					//unit 시퀀스 같은거만 LIST에 추가함.
					if(lp_value[1].equals(this_unit_seq)){
						lp_seqList.add(lp_value[2]);
					}
				}	
				
				param.put("unit_name", unit_name[i]);
				param.put("unit_seq", this_unit_seq);
				if(new_yn.equals("N")){				
					unitDao.insertUnit(param);
				}else{
					unitDao.updateUnit(param);
				}
	
				if(lp_seqList != null) {				
					for(int lp_index=0;lp_index<lp_seqList.size();lp_index++){
						String lp_seqs[] = lp_seqList.get(lp_index).split(",");
						
						//단원 수업 맵핑 테이블 insert
						for(int lp_count=0;lp_count<lp_seqs.length;lp_count++){
							param.put("lp_seq",lp_seqs[lp_count]);
							param.put("unit_group_num", (lp_index+1));
							unitDao.insertMpUnitLesson(param);
						}
		
						
						
						int tlo_order_num = 1;
						
						if(tlo_seq != null) {
							//TLO 테이블 insert
							for(int tlo_index=0;tlo_index<tlo_seq.length;tlo_index++){
								
								String tlo_value[] = tlo_seq[tlo_index].split("_");
								if(tlo_value[1].equals(this_unit_seq) && tlo_value[2].equals(lp_seqList.get(lp_index))){
									param.put("tlo_seq", tlo_value[3]);
									param.put("skill", skill[tlo_index]);
									param.put("teaching_method", teaching_method[tlo_index]);
									param.put("evaluation_method", evaluation_method[tlo_index]);
									param.put("order_num", tlo_order_num);
									if(tlo_value[0].equals("N"))
										unitDao.insertTLO(param);
									else
										unitDao.updateTLO(param);
									tlo_order_num++;
									
									int elo_order_num = 1;
			
									if(elo_seq != null) {
										for(int elo_index=0;elo_index<elo_seq.length;elo_index++){							
											String elo_value[] = elo_seq[elo_index].split("_");
											if(elo_value[1].equals(this_unit_seq) && elo_value[2].equals(lp_seqList.get(lp_index)) && elo_value[3].equals(tlo_value[3])){
												param.put("elo_seq", elo_value[4]);
												param.put("domain_code", domain_code[elo_index]);
												param.put("level_code", level_code[elo_index]);
												param.put("content", content[elo_index]);
												param.put("order_num", elo_order_num);
												if(elo_value[0].equals("N"))
													unitDao.insertELO(param);
												else
													unitDao.updateELO(param);
												elo_order_num++;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return resultMap;
	}

	public ResultMap unitList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("basicInfo", pfLessonDao.getCurriculumBasicInfo(param));
		resultMap.put("unitList", unitDao.getUnitList(param));
		resultMap.put("domainCode", unitDao.getUnitCode(param));
		
		return resultMap;
	}

	public HashMap<String, Object> lessonAddPCList(HashMap<String, Object> param) throws Exception {
		// TODO Auto-generated method stub
		return unitDao.getLessonAddPC(param);
	}

	public ResultMap unitCode(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("unitCode", unitDao.getUnitCode(param));
		return resultMap;
	}

}
