package com.nsdevil.medlms.web.common.dao;

import java.util.HashMap;
import java.util.List;


public interface MyDao {
	public HashMap<String, Object> getProfileInfo(HashMap<String, Object> param) throws Exception;
	public int getUserExistCnt(HashMap<String, Object> param) throws Exception;
	public int updateUserInfo(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> myLessonData(HashMap<String, Object> param) throws Exception;
	public int myLessonDataCount(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getAcaInfo(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getMyFeScoreList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getMyCurrGradeList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getMySoosiGradeList(HashMap<String, Object> param) throws Exception;
	public int getMyAsgmtCount(HashMap<String, Object> param) throws Exception;
	public int getReportCardCount(HashMap<String, Object> param) throws Exception;
	
}
