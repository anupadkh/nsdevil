package com.nsdevil.medlms.web.admin.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
 
import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.web.admin.service.SurveyService;

@Controller
public class SurveyController extends ExceptionController {
	
	@Autowired
	private SurveyService surveyService;

	//수업만족도 설문 매핑
	@RequestMapping(value = {"/admin/survey/lessonSatisfaction", "/admin/survey"}, method = RequestMethod.GET)
	public String lessonSatisfactionView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
				
		return "admin/survey/lessonSatisfaction";
	}
	
	//수업만족도 설문 리스트
	@RequestMapping(value = "/ajax/admin/survey/lessonSatisfaction/list", method = RequestMethod.POST)
	public String lessonSatisfactionList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		model.addAllAttributes(surveyService.getSFResearchList(param));	
		return JSON_VIEW;
	}
	
	//수업만족도설문 등록 매핑
	@RequestMapping(value = {"/admin/survey/lessonSatisfaction/create"}, method = RequestMethod.POST)
	public String lessonSatisfactionCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
				
		model.addAttribute("sr_seq", request.getParameter("sr_seq"));
		return "admin/survey/lessonSatisfactionCreate";
	}
	
	//수업만족도 설문 등록
	@RequestMapping(value = "/ajax/admin/survey/lessonSatisfaction/insert", method = RequestMethod.POST)
	public String lessonSatisfactionInsert(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception{		
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		model.addAllAttributes(surveyService.insertSFResearch(param));	
		
		return JSON_VIEW;
	}
	
	//수업만족도 기본 설문 설정
	@RequestMapping(value = "/ajax/admin/survey/lessonSatisfaction/basicResearch/insert", method = RequestMethod.POST)
	public String lessonSatisfactionBasicResearchInsert(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception{		
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		
		model.addAllAttributes(surveyService.insertSFResearchBasic(param));	
		
		return JSON_VIEW;
	}
	
	//수업만족도 설문 등록 리스트
	@RequestMapping(value = "/ajax/admin/survey/lessonSatisfaction/create/list", method = RequestMethod.POST)
	public String lessonSatisfactionResearchQuestionList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception{		
		model.addAllAttributes(surveyService.getSFResearchQuestionList(param));	
		return JSON_VIEW;
	}
	
	//과정만족도 설문 매핑
	@RequestMapping(value = "/admin/survey/currSatisfaction", method = RequestMethod.GET)
	public String currSatisfactionView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
				
		return "admin/survey/currSatisfaction";
	}
	

	//과정만족도설문 등록 매핑
	@RequestMapping(value = {"/admin/survey/currSatisfaction/create"}, method = RequestMethod.POST)
	public String currSatisfactionCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
				
		model.addAttribute("sr_seq", request.getParameter("sr_seq"));
		return "admin/survey/currSatisfactionCreate";
	}
}
