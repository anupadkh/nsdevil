package com.nsdevil.medlms.web.pf.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.common.exception.RuntimeLogicException;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.admin.service.AcademicService;
import com.nsdevil.medlms.web.common.service.CommonService;
import com.nsdevil.medlms.web.pf.service.PFLessonService;

@Controller
public class PFLessonController extends ExceptionController {
	
	@Autowired
	private PFLessonService pfLessonservice;
	
	@Autowired
	private CommonService commonService;

	@Autowired
	private AcademicService academicService;

	//수업관리
	@RequestMapping(value = "/pf/lesson", method = RequestMethod.GET)
	public String lessonView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		String viewName = "pf/lesson/curriculumModify";
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		
		//TODO 교육과정계획서 첫번째것 or 현재 진행중인 정보
		pfLessonservice.setCurrAndLp(param, request);
		
		//TODO 교수/책임교수/에 따라 리턴 페이지 분기처리.
		if(request.getSession().getAttribute("S_CURRICULUM_SEQ") != null) {
			param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
			String pflevel = pfLessonservice.PfLevelChk(param);		
			model.addAllAttributes(commonService.getAcaState(param));
			model.addAllAttributes(commonService.getCurrFlagInfo(param));
			if(pflevel.equals("3"))
				viewName = "pf/lesson/curriculumView";
		}
			
		model.addAttribute("rightMenuOpenYn", "N");//default : 왼쪽메뉴 오픈상태
		return viewName;
	}
	
	
	@RequestMapping(value="/ajax/pf/lesson/menu/left/list", method = RequestMethod.GET)
	public String lessonLeftMenuList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(pfLessonservice.getLessonLeftMenuList(param, request));
		return JSON_VIEW;
	}
	
	@RequestMapping(value="/ajax/pf/lesson/menu/left/lessonPlanList", method = RequestMethod.POST)
	public String leftLessonPlanList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		
		model.addAllAttributes(pfLessonservice.getLeftLessonPlanList(param));
		return JSON_VIEW;
	}
	
	@RequestMapping(value="/ajax/pf/lesson/menu/right/list", method = RequestMethod.GET)
	public String lessonRightMenuList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		//TODO
		return JSON_VIEW;
	}
	
	@RequestMapping(value="/ajax/pf/lesson/curriculum/exist/check", method = RequestMethod.GET)
	public String curriculumExistCheck(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		Util.requiredCheck(param, new String[] {"s_user_seq"});
		model.addAllAttributes(pfLessonservice.getCurriculumExistCheck(param));
		return JSON_VIEW;
	}
	
	//교육과정계획서
	@RequestMapping(value = "/pf/lesson/curriculum", method = RequestMethod.POST)
	public String curriculumView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		request.getSession().setAttribute("S_CURRICULUM_SEQ", param.get("currSeq"));
		request.getSession().setAttribute("S_CURRENT_ACADEMIC_YEAR", param.get("academicYear"));
				
		param.put("curr_seq", param.get("currSeq").toString());
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		
		model.addAllAttributes(commonService.getAcaState(param));
		model.addAllAttributes(commonService.getCurrFlagInfo(param));
		
		String professorLevel = pfLessonservice.PfLevelChk(param);
		String viewName = "pf/lesson/curriculumModify";
		if(professorLevel.equals("3"))
			viewName = "pf/lesson/curriculumView";
		
		return viewName;
	}
	
	@RequestMapping(value = "/ajax/pf/lesson/curriculum", method = RequestMethod.GET)
	public String curriculumDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ"));
		if(request.getSession().getAttribute("S_CURRICULUM_SEQ") != null)
			model.addAllAttributes(pfLessonservice.getCurriculumDetail(param, request));
				
		return JSON_VIEW;
	}	
	
	@RequestMapping(value = "/ajax/pf/lesson/preCurriculum", method = RequestMethod.GET)
	public String preCurriculumDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ"));		
		model.addAllAttributes(pfLessonservice.getPreCurr(param));						
		return JSON_VIEW;
	}	

	@RequestMapping(value = "/ajax/pf/lesson/curriculumInfo", method = RequestMethod.GET)
	public String curriculumInfo(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ"));
		model.addAllAttributes(pfLessonservice.getCurriculumInfo(param));
				
		return JSON_VIEW;
	}	
	
	@RequestMapping(value = "/ajax/pf/lesson/curriculum/lessonPlanInfo", method = RequestMethod.POST)
	public String lessonPlanInfo(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ"));
		model.addAllAttributes(pfLessonservice.getLessonPlanPeriodInfo(param));
				
		return JSON_VIEW;
	}
	
	
	@RequestMapping(value = "/ajax/pf/lesson/curriculum/summativeEvaluationCodeList", method = RequestMethod.POST)
	public String summativeEvaluationCodeList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAttribute("summativeEvaluationList", pfLessonservice.summativeEvaluationList(param));
				
		return JSON_VIEW;
	}
	
	@RequestMapping(value = "/ajax/pf/lesson/graduationCapabilityList", method = RequestMethod.POST)
	public String graduationCapabilityList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		//졸업역량 가져오기
		model.addAttribute("graduationCapabilityList", pfLessonservice.graduationCapabilityList(param));
		return JSON_VIEW;
	}
	
	//교육과정 저장
	@RequestMapping(value = "/ajax/pf/lesson/curriculum/modify", method = RequestMethod.POST)
	public String curriculumModify(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		String pf_user_seq[] = request.getParameterValues("pf_user_seq"); //교수
		String pf_level[] = request.getParameterValues("pf_level"); //교수 구분
		
		//총괄평가 기준
		String eval_method_code[] = request.getParameterValues("eval_method_option"); //평가방법
		String eval_domain_code[] = request.getParameterValues("eval_domain_code"); //평가영역
		String importance[] = request.getParameterValues("importance"); //비중
		String eval_method_text[] = request.getParameterValues("eval_method_text"); //평가방법 및 기준
		String eval_etc[] = request.getParameterValues("eval_etc"); //비고
		String se_seq[] = request.getParameterValues("se_seq");
		
		//졸업역량
		String fc_seq[] = request.getParameterValues("fc_seq");
		String process_result[] = request.getParameterValues("process_result");
		String teaching_method[] = request.getParameterValues("teaching_method");
		String evaluation_method[] = request.getParameterValues("evaluation_method");
		
		//학점기준
		String grade_standard_code[] = request.getParameterValues("grade_standard_code");
		String grade_importance[] = request.getParameterValues("grade_importance");
	
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		
		model.addAllAttributes(pfLessonservice.curriculumModify(param, pf_user_seq, pf_level, eval_method_code, 
				eval_domain_code, importance, eval_method_text, eval_etc, fc_seq, process_result, teaching_method, evaluation_method, se_seq, grade_standard_code ,grade_importance));
		return JSON_VIEW;
	}
	
	//비강의 수업 리스트 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/lessonMethodSujectList", method = RequestMethod.POST)
	public String lessonMethodSujectList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		//졸업역량 가져오기		
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		model.addAttribute("lessonMethodSujectList", pfLessonservice.lessonMethodSujectList(param));
		return JSON_VIEW;
	}
	
	//TODO
	//수업계획서
	@RequestMapping(value = "/pf/lesson/lessonPlan", method = RequestMethod.POST)
	public String lessonPlanView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		//TODO 교수/책임교수/에 따라 리턴 페이지 분기처리 필요함.
		//TODO 교육과정계획서 첫번째것 or 현재 진행중인 정보
		request.getSession().setAttribute("S_CURRICULUM_SEQ", param.get("currSeq"));
		request.getSession().setAttribute("S_LP_SEQ", request.getParameter("lpSeq").toString());
						
		model.addAttribute("lp_seq", request.getParameter("lpSeq").toString());
		model.addAttribute("rightMenuOpenYn", "N");//default : 왼쪽메뉴 오픈상태
		
		param.put("level", "1");
		model.addAttribute("domainCodeList", commonService.getDomainCodeList(param)); //영역코드
		model.addAttribute("classRoom", commonService.getCodeList("class_room")); //강의실
		model.addAttribute("clinicCode", commonService.getCpxCode()); //임상표현
		model.addAttribute("osceCode", commonService.getOsceCode()); //임상술기
		model.addAttribute("diaCode", commonService.getDiaCode()); //진단명
		model.addAttribute("formationEvalution", commonService.getCodeList("formation_evaluation")); //형성평가
		model.addAttribute("evaluationCode", commonService.getCodeList("evaluation_code")); //평가방법
		model.addAttribute("lessonMethodCode", commonService.getCodeList("lesson_method_code")); //평가방법
		
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAttribute("yearList", pfLessonservice.getAcademicYear(param));
		
		param.put("lp_seq", request.getParameter("lpSeq").toString());
		param.put("curr_seq", request.getParameter("currSeq").toString());
		model.addAttribute("currFinishCapability", pfLessonservice.CurrFinishCapabilityList(param));//졸업역량
		
		param.put("code_cate", "osce_code_name");
		model.addAttribute("code1", academicService.getCodeMenuName(param));
		param.put("code_cate", "cpx_code_name");
		model.addAttribute("code2", academicService.getCodeMenuName(param));
		param.put("code_cate", "dia_code_name");
		model.addAttribute("code3", academicService.getCodeMenuName(param));
		return "pf/lesson/lessonPlan";
	}
	
	//수업계획서 기본정보 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/lessonPlanBasicInfo", method = RequestMethod.POST)
	public String lessonPlanBasicInfo(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {	
		if(request.getSession().getAttribute("S_LP_SEQ") != null) {
			param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());
			model.addAllAttributes(pfLessonservice.lessonPlanBasicInfo(param)); //수업계획서 정보
			model.addAllAttributes(pfLessonservice.lessonPlanSummativeInfo(param)); //수업계획서 부가정보 정보
		}
		return JSON_VIEW;
	}
	
	//수업계획서 저장
	@RequestMapping(value = "/ajax/pf/lesson/lessonPlan/modify", method = RequestMethod.POST)
	public String lessonPlanModify(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		String coreClinic_code[] = request.getParameterValues("coreClinic_code"); //핵심 임상표현
		String clinic_code[] = request.getParameterValues("clinic_code"); //관련 임상표현
		String osce_code[] = request.getParameterValues("osce_code"); //임상 술기
		String dia_code[] = request.getParameterValues("dia_code"); //진단명		
		String fe_code[] = request.getParameterValues("fe_code"); //형성평가 유형 코드
		String fe_seq[] = request.getParameterValues("fe_seq"); //형성평가 시퀀스
		String fc_seq[] = request.getParameterValues("fc_seq"); //졸업역량
		String lesson_method_code[] = request.getParameterValues("lesson_method_code");//비강의
		//비강의 추가해야된다..
				
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		model.addAllAttributes(pfLessonservice.lessonPlanModify(param, coreClinic_code, clinic_code, osce_code, dia_code, fe_code, fc_seq, fe_seq, lesson_method_code));
		return JSON_VIEW;
	}
	
	//이전 수업계획서 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/lessonPlan/preLsessonPlanList", method = RequestMethod.POST)
	public String preLessonPlanList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		param.put("this_lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());

		model.addAttribute("lessonList", pfLessonservice.preLessonPlanList(param));
		return JSON_VIEW;
	}
		
	//수업계획서에 타이틀 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/titleInfo", method = RequestMethod.POST)
	public String getLessonTitleInfo(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());
		model.addAllAttributes(pfLessonservice.lessonPlanBasicInfo(param)); //수업계획서 정보
		return JSON_VIEW;
	}
	
	
	//수업계획서 개별 형성평가 삭제 시 형성평가 상태값 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/lessonPlan/feDelChk", method = RequestMethod.POST)
	public String feDelChk(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		
		model.addAllAttributes(pfLessonservice.formationEvaluationState(param)); //형성평가 상태값 가져온다.
		return JSON_VIEW;
	}

	//수업계획서 아니오 선택시 전체 형성평가 상태값 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/lessonPlan/feDelChkAll", method = RequestMethod.POST)
	public String feDelChkAll(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());
		
		model.addAllAttributes(pfLessonservice.formationEvaluationState(param)); //형성평가 상태값 가져온다.
		return JSON_VIEW;
	}
	
	//수업계획서 진단명 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/lessonPlan/diacode/list", method = RequestMethod.POST)
	public String getDiaCodeList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());
		
		model.addAllAttributes(pfLessonservice.getDiaCodePageList(param)); //형성평가 상태값 가져온다.
		return JSON_VIEW;
	}
	
	/*********************************************************************************************
	 * 형성평가_ST
	 *********************************************************************************************/
	
	//형성평가 목록 화면
	@RequestMapping(value = "/pf/lesson/formationEvaluation", method = RequestMethod.GET)
	public String formationEvaluationView(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		return "pf/lesson/formationEvaluationList";
	}
	
	//형성평가 목록
	@RequestMapping(value = "/ajax/pf/lesson/formationEvaluation/list", method = RequestMethod.GET)
	public String formationEvaluationList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		
		param.put("s_lp_seq", request.getSession().getAttribute("S_LP_SEQ"));
		Util.requiredCheck(param, new String[] {"s_lp_seq"});
		model.addAllAttributes(pfLessonservice.getFormationEvaluationList(param));
		return JSON_VIEW;
	}
	
	//형성평가 삭제
	@RequestMapping(value = "/ajax/pf/lesson/formationEvaluation/remove", method = RequestMethod.POST)
	public String formationEvaluationRemove(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		Util.requiredCheck(param, new String[] {"s_user_seq", "fe_seq"});
		model.addAllAttributes(pfLessonservice.formationEvaluationRemove(param));
		return JSON_VIEW;
	}
	
	//형성 평가명 저장
	@RequestMapping(value = "/ajax/pf/lesson/formationEvaluation/title/modify", method = RequestMethod.POST)
	public String formationEvaluationTitleModify(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		Util.requiredCheck(param, new String[] {"fe_name", "fe_seq", "s_user_seq"});
		model.addAllAttributes(pfLessonservice.formationEvaluationTitleModify(param));
		return JSON_VIEW;
	}
	
	//퀴즈목록
	@RequestMapping(value = "/ajax/pf/lesson/formationEvaluation/quiz/list", method = RequestMethod.GET)
	public String quizList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		Util.requiredCheck(param, new String[] {"fe_seq"});
		model.addAllAttributes(pfLessonservice.getQuizList(param));
		return JSON_VIEW;
	}
	
	
	//문제 등록
	@RequestMapping(value = "/ajax/pf/lesson/formationEvaluation/quiz/create", method = RequestMethod.POST)
	public String quizCreate(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ"));
		
		int answerViewCnt = Integer.parseInt(String.valueOf(param.get("answerViewCnt")));
		if (answerViewCnt < 2) { //답가지 개수 부족 최소 2개이상
			throw new RuntimeLogicException("MISSING_REQUIRED_VALUE : [answerViewCnt lack]", "001");
		}
		
		for (int i = 1; i <= answerViewCnt; i++) { //답가지 텍스트 입력 확인
			String answerViewText = String.valueOf(param.get("answerContent" + i));
			if (Util.isNull(answerViewText)) {
				throw new RuntimeLogicException("MISSING_REQUIRED_VALUE : [answerContent]", "001");
			}
		}
		
		Util.requiredCheck(param, new String[] {"quizQuestion", "answerText", "answerViewCnt", "quizOrder", "feSeq", "s_user_seq", "s_curr_seq"});
		model.addAllAttributes(pfLessonservice.quizCreate(param, request));
		return JSON_VIEW;
	}
	
	//문제 순서 변경
	@RequestMapping(value = "/ajax/pf/lesson/formationEvaluation/quiz/order/modify", method = RequestMethod.POST)
	public String quizOrderModify(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		Util.requiredCheck(param, new String[] {"quiz_seq", "order_up_flag", "fe_seq", "s_user_seq"});
		model.addAllAttributes(pfLessonservice.quizOrderModify(param));
		return JSON_VIEW;
	}
	
	//문제 삭제
	@RequestMapping(value = "/ajax/pf/lesson/formationEvaluation/quiz/remove", method = RequestMethod.POST)
	public String quizRemove(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		Util.requiredCheck(param, new String[] {"fe_seq", "quiz_seq_str", "s_user_seq"});
		model.addAllAttributes(pfLessonservice.quizRemove(param));
		return JSON_VIEW;
	}
	
	//문제 미리보기 첫페이지
	@RequestMapping(value = "/ajax/pf/lesson/formationEvaluation/quiz/preview", method = RequestMethod.POST)
	public String quizPreview(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("s_curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ"));
		Util.requiredCheck(param, new String[] {"fe_seq", "s_curr_seq"});
		model.addAllAttributes(pfLessonservice.quizPreview(param));
		return JSON_VIEW;
	}
	
	//문제 상세보기
	@RequestMapping(value = "/ajax/pf/lesson/formationEvaluation/quiz/detail", method = RequestMethod.POST)
	public String quizDetail(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("s_curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ"));
		param.put("s_lp_seq", request.getSession().getAttribute("S_LP_SEQ"));
		Util.requiredCheck(param, new String[] {"quiz_seq", "s_curr_seq", "s_lp_seq"});
		model.addAllAttributes(pfLessonservice.getQuizDetail(param));
		return JSON_VIEW;
	}
	
	//형성평가 썸네일 업로드
	@RequestMapping(value = "/ajax/pf/lesson/formationEvaluation/thumbnail/upload", method = RequestMethod.POST)
	public String formationEvaluationThumbnailUpload(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("fe_seq", param.get("feSeq"));
		Util.requiredCheck(param, new String[] {"s_user_seq", "fe_seq"});
		model.addAllAttributes(pfLessonservice.thumbnailUpload(param, request));
		return JSON_VIEW;
	}
	
	
	//퀴즈 수정
	@RequestMapping(value = "/ajax/pf/lesson/formationEvaluation/quiz/modify", method = RequestMethod.POST)
	public String quizModify(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ"));
		
		int answerViewCnt = Integer.parseInt(String.valueOf(param.get("answerViewCnt")));
		if (answerViewCnt < 2) { //답가지 개수 부족 최소 2개이상
			throw new RuntimeLogicException("MISSING_REQUIRED_VALUE : [answerViewCnt lack]", "001");
		}
		
		for (int i = 1; i <= answerViewCnt; i++) { //답가지 텍스트 입력 확인
			String answerViewText = String.valueOf(param.get("answerContent" + i));
			if (Util.isNull(answerViewText)) {
				throw new RuntimeLogicException("MISSING_REQUIRED_VALUE : [answerContent]", "001");
			}
		}
		
		Util.requiredCheck(param, new String[] {"quizSeq", "quizQuestion", "answerText", "answerViewCnt", "quizOrder", "feSeq", "s_user_seq", "s_curr_seq"});
		model.addAllAttributes(pfLessonservice.quizModify(param, request));
		return JSON_VIEW;
	}
	
	//지난 수업 퀴즈 목록
	@RequestMapping(value = "/ajax/pf/lesson/formationEvaluation/past/list", method = RequestMethod.POST)
	public String getPastFormationEvaluationList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ"));
		
		param.put("search_fe_name", param.get("searchText"));
		param.put("search_aca_year", param.get("searchYear"));
		
		Util.requiredCheck(param, new String[] {"s_user_seq", "s_curr_seq"});
		model.addAllAttributes(pfLessonservice.getPastFormationEvaluationList(param));
		return JSON_VIEW;
	}
	
	
	//지난 수업 퀴즈 불러오기
	@RequestMapping(value = "/ajax/pf/lesson/formationEvaluation/quiz/copy", method = RequestMethod.POST)
	public String pastQuizCopy(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		Util.requiredCheck(param, new String[] {"s_user_seq", "fe_seq", "curr_fe_seq"});
		model.addAllAttributes(pfLessonservice.pastQuizCopy(param));
		return JSON_VIEW;
	}
	
	//퀴즈 엑셀 일괄 업로드
	@RequestMapping(value = "/ajax/pf/lesson/formationEvaluation/quiz/excel/create", method = RequestMethod.POST)
	public String quizExcelCreate(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("fe_seq", param.get("currFeSeq"));
		
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		MultipartFile excelFile = mRequest.getFile("excelFile");
		if (excelFile == null) {
			throw new RuntimeLogicException("MISSING_REQUIRED_VALUE [ excelFile ]", "001");
		}
		
		Util.requiredCheck(param, new String[] {"s_user_seq", "fe_seq"});
		model.addAllAttributes(pfLessonservice.quizExcelCreate(param, request));
		return JSON_VIEW;
	}
	
	//결과 엑셀 다운로드
	@RequestMapping(value = "/pf/lesson/formationEvaluation/result/excel/download", method = RequestMethod.POST)
	public String resultExcelDownload(HttpServletRequest request, HttpServletResponse response, Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("fe_seq", param.get("feSeq"));
		Util.requiredCheck(param, new String[] {"s_user_seq", "fe_seq"});
		ResultMap resultMap = pfLessonservice.getFormationEvaluationResult(param);
		model.addAllAttributes(resultMap);
		String fileName = String.valueOf(resultMap.get("file_name"));

		model.addAttribute("excel_view_type", "formation_evaluation");
		response.setHeader("Content-disposition", "attachment; filename=" + new String((fileName).getBytes("KSC5601"), "8859_1")+".xlsx"); //target명을 파일명으로 작성
		return "excelDownloadView";
	}
	
	//TODO 작업중
	//학생별 점수 확인
	@RequestMapping(value = "/ajax/pf/lesson/formationEvaluation/result/list", method = RequestMethod.POST)
	public String formationEvaluationResultList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_lp_seq", request.getSession().getAttribute("S_LP_SEQ"));
		param.put("fe_seq", param.get("feSeq"));
		param.put("absence_flag", param.get("absenceFlag"));
		Util.requiredCheck(param, new String[] {"s_user_seq", "fe_seq", "absence_flag", "s_lp_seq"});
		model.addAllAttributes(pfLessonservice.getFormationEvaluationResultList(param));
		return JSON_VIEW;
	}
	
	
	/*********************************************************************************************
	 * 형성평가_END
	 *********************************************************************************************/
	
	
	/*********************************************************************************************
	 * 과제 
	 *********************************************************************************************/
	
	//수업자료
	@RequestMapping(value = "/pf/lesson/lessonData", method = RequestMethod.GET)
	public String lessonData(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		String viewName = "pf/lesson/lessonData";
		if(request.getSession().getAttribute("S_LP_SEQ") != null) {
			param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());
			model.addAttribute("menuTitle", "lessonData");
			int lesson_data_seq = pfLessonservice.getLessonDataSeq(param);
			
			model.addAttribute("lesson_data_seq", lesson_data_seq);
		}else{
			viewName = "redirect:/pf/lesson";
		}
			
		return viewName;
	}
	
	//수업자료 등록 카운트 체크
	@RequestMapping(value = "/ajax/pf/lesson/lessonData/lessonDataSeq", method = RequestMethod.POST)
	public String getLessonDataSeq(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());
		int lesson_data_seq = pfLessonservice.getLessonDataSeq(param);		
		model.addAttribute("lesson_data_seq", lesson_data_seq);
		return JSON_VIEW;
	}
	
	//수업자료
	@RequestMapping(value = "/pf/lesson/lessonData/view", method = RequestMethod.GET)
	public String lessonDataView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());
		int lesson_data_seq = pfLessonservice.getLessonDataSeq(param);		
		model.addAttribute("lesson_data_seq", lesson_data_seq);			
		return "pf/lesson/lessonDataView";
	}
	
	//수업자료 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/lessonData/list", method = RequestMethod.POST)
	public String lessonDataList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		
		model.addAllAttributes(pfLessonservice.getLessonDataList(param));
		return JSON_VIEW;
	}
	
	//수업자료 저장
	@RequestMapping(value = "/ajax/pf/lesson/lessonData/create", method = RequestMethod.POST)
	public String lessonDataCreate(MultipartHttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		model.addAllAttributes(pfLessonservice.lessonDataInsert(request, param));
		/*try {
			model.addAllAttributes(pfLessonservice.lessonDataInsert(request, param));
		}catch(Exception e) {
			model.addAttribute("status",e.getMessage());
		}	*/
		return JSON_VIEW;
	}

	//수업자료 코멘트 저장
	@RequestMapping(value = "/ajax/pf/lesson/lessonData/comment/insert", method = RequestMethod.POST)
	public String commentInsert(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		Util.requiredCheck(param, new String[] {"lesson_attach_seq", "explan"});
		model.addAllAttributes(pfLessonservice.lessonDataCommentInsert(param));		
		
		return JSON_VIEW;
	}
	
	//과제 등록 매핍
	@RequestMapping(value = "/pf/lesson/assignMent", method = RequestMethod.GET)
	public String assignMentMapping(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		String viewName = "pf/lesson/assignMentCreate";
		if(request.getSession().getAttribute("S_LP_SEQ") != null) {
			param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());
			param.put("asgmt_type", "2");	
			int asgmt_cnt = pfLessonservice.getAssignMentCount(param);
			model.addAttribute("asgmt_cnt", asgmt_cnt);
			/*if(asgmt_cnt > 0)
				model.addAttribute("asgmt_seq", pfLessonservice.getAssignMentSeq(param));*/
			
			//등록된 과제 있는지 체크. 있으면 과제등록현황으로 간다.
			if(asgmt_cnt > 0)
				viewName="pf/lesson/assignMentView";
			
		}
		else
			viewName = "redirect:/pf/lesson";
		
		return viewName;
	}
	
	//과제 등록 수정 페이지
	@RequestMapping(value = "/pf/lesson/assignMent/create", method = RequestMethod.GET)
	public String assignMentCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		String viewName = "pf/lesson/assignMentCreate";
		/*if(request.getSession().getAttribute("S_LP_SEQ") != null) {
			param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());	
			param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
			param.put("asgmt_type", "1");	
			model.addAttribute("asgmt_cnt", pfLessonservice.getAssignMentCount(param));			
		}
		else
			viewName = "redirect:/pf/lesson";*/
		return viewName;
	}
	
	//과제 제출현황 페이지
	@RequestMapping(value = "/pf/lesson/assignMent/view", method = RequestMethod.GET)
	public String assignMentView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		String viewName = "pf/lesson/assignMentView";
		if(request.getSession().getAttribute("S_LP_SEQ") == null)
			viewName = "redirect:/pf/lesson";
		return viewName;
	}
	
	//과제 저장
	@RequestMapping(value = "/ajax/pf/lesson/assignMent/insert", method = RequestMethod.POST)
	public String assignMentCreate(MultipartHttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param
			, @RequestParam(value="asgmt_attach_seq",required=false) String[] asgmt_attach_seq) throws Exception {
				
		model.addAllAttributes(pfLessonservice.assignMentInsert(request, param, asgmt_attach_seq));
		
		
		return JSON_VIEW;
	}
	
	//과제 하나만 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/assignMent/oneList", method = RequestMethod.POST)
	public String assignMentOneList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		model.addAllAttributes(pfLessonservice.getAssignMentOne(param));
		return JSON_VIEW;
	}
	
	//과제 리스트 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/assignMent/List", method = RequestMethod.POST)
	public String assignMentList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		if(param.get("asgmt_type").toString().equals("2"))
			param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		param.put("userLevel", request.getSession().getAttribute("S_USER_LEVEL"));
		
		model.addAllAttributes(pfLessonservice.getAssignMentList(param));
		return JSON_VIEW;
	}
	

	//과제 등록 리스트 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/assignMent/SubmitList", method = RequestMethod.POST)
	public String assignMentSubmitList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		if(request.getSession().getAttribute("S_LP_SEQ") != null) {
			param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());
		}
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		model.addAllAttributes(pfLessonservice.getAssignMentSubmitList(param));
		return JSON_VIEW;
	}
	
	//피드백 저장
	@RequestMapping(value = "/ajax/pf/lesson/assignMent/feedback/insert", method = RequestMethod.POST)
	public String assignMentFeedbackInsert(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		model.addAllAttributes(pfLessonservice.assignMentFeedBackInsert(request, param));
		return JSON_VIEW;
	}
	
	//과제 하나 팝업 상세보기
	@RequestMapping(value = "/ajax/pf/lesson/assignMent/detailView", method = RequestMethod.POST)
	public String assignMentdetailView(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		model.addAllAttributes(pfLessonservice.getAssignMentDetail(param));
		return JSON_VIEW;
	}	
	
	//과제 삭제하기
	@RequestMapping(value = "/ajax/pf/lesson/assignMent/delete", method = RequestMethod.POST)
	public String assignMentDelete(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		Util.requiredCheck(param, new String[] {"user_seq", "asgmt_seq"});
		model.addAllAttributes(pfLessonservice.deleteAssignMent(param));
		return JSON_VIEW;
	}	
	/**
	 * 과제 전체 파일 다운로드
	 */
	@RequestMapping(value = "/ajax/pf/lesson/assignMent/AllFileDown", produces="application/json", method=RequestMethod.POST)
	public String assignMentAllFileDown(HttpServletRequest request, @RequestParam HashMap<String, Object> param,  Model model) throws Exception {
		
		model.addAllAttributes(pfLessonservice.assignMentAllFileDown(param));
			
		return JSON_VIEW;
	}
	
	//출석 페이지
	@RequestMapping(value = "/pf/lesson/attendance/view", method = RequestMethod.GET)
	public String attendanceView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		String viewName = "pf/lesson/attendance";
		if(request.getSession().getAttribute("S_LP_SEQ") == null)
			viewName = "redirect:/pf/lesson";
		return viewName;
	}
	
	//출석 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/attendance/list", method = RequestMethod.POST)
	public String attendanceList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		if(request.getSession().getAttribute("S_LP_SEQ") != null) {
			param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());
			param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		}
		model.addAllAttributes(pfLessonservice.attendanceList(param));
		return JSON_VIEW;
	}	
		
	//알림 페이지
	@RequestMapping(value = "/pf/lesson/pfNotice/view", method = RequestMethod.GET)
	public String pfNoticeView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		return "pf/lesson/pfNotice";
	}	
	
	//과제 등록 매핍
	@RequestMapping(value = "/pf/lesson/currAssignMent", method = RequestMethod.GET)
	public String currAssignMentMapping(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		String viewName = "pf/lesson/currAssignMentCreate";
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		param.put("asgmt_type", "1");
		param.put("lp_seq", "");
		int asgmt_cnt = pfLessonservice.getAssignMentCount(param);
		model.addAttribute("asgmt_cnt", asgmt_cnt);

		//등록된 과제 있는지 체크. 있으면 과제등록현황으로 간다.
		if(asgmt_cnt > 0)
			viewName="pf/lesson/currAssignMentView";
		
		return viewName;
	}
	
	//과제 등록 수정 페이지
	@RequestMapping(value = "/pf/lesson/currAssignMent/create", method = RequestMethod.GET)
	public String currAssignMentCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		return "pf/lesson/currAssignMentCreate";
	}
	
	//과제 제출현황 페이지
	@RequestMapping(value = "/pf/lesson/currAssignMent/view", method = RequestMethod.GET)
	public String currAssignMentView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		String viewName = "pf/lesson/currAssignMentView";
		return viewName;
	}
	
	//전체 수업과제 페이지
	@RequestMapping(value = "/pf/lesson/currAssignMent/lpAll", method = RequestMethod.GET)
	public String currAssignMentLpAllView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		return "pf/lesson/currAssignMentLpAllView";
	}
	
	//과정 전체 수업과제 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/currAssignMent/lpAll/list", method = RequestMethod.POST)
	public String getCurrAssignMentLpAllList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		model.addAllAttributes(pfLessonservice.getAssignMentAllList(param));
		return JSON_VIEW;
	}
	
	//등록된 과제 수 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/currAssignMent/getAssignCount", method = RequestMethod.POST)
	public String getAssignCount(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		model.addAttribute("asgmt_cnt", pfLessonservice.getAssignMentCount(param));
		return JSON_VIEW;
	}

	//운영보고서 페이지
	@RequestMapping(value = "/pf/lesson/report", method = RequestMethod.GET)
	public String reportMentCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		return "pf/lesson/currReport";
	}
	
	//운영보고서 정보 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/report/list", method = RequestMethod.POST)
	public String getReportInfo(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		model.addAllAttributes(pfLessonservice.getReportInfo(param));
		return JSON_VIEW;
	}
	
	//팝업 수업 리스트 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/report/popup/list", method = RequestMethod.POST)
	public String getReportPopupList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		model.addAllAttributes(pfLessonservice.getPopupLessonList(param));
		return JSON_VIEW;
	}	

	//일반교수 단원관리 페이지
	@RequestMapping(value = "/pf/lesson/unitView", method = RequestMethod.GET)
	public String unitView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		return "pf/lesson/unitView";
	}

	//일반교수 수업시간표 페이지
	@RequestMapping(value = "/pf/lesson/lessonPlanCs", method = RequestMethod.GET)
	public String lessonPlanCsView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		return "pf/lesson/lessonPlanRegCs";
	}
}
