package com.nsdevil.medlms.web.admin.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.admin.service.AcademicService;
import com.nsdevil.medlms.web.admin.service.LessonService;
import com.nsdevil.medlms.web.common.service.CommonService;
import com.nsdevil.medlms.web.mpf.service.MPFGradeService;
import com.nsdevil.medlms.web.pf.service.PFLessonService;

@Controller
public class LessonController extends ExceptionController {
	
	@Autowired
	private LessonService lessonService;

	@Autowired
	private PFLessonService pfLessonservice;	

	@Autowired
	private CommonService commonService;

	@Autowired
	private MPFGradeService mpGradeService;

	@Autowired
	private AcademicService academicService;
	
	@RequestMapping(value = "/admin/lesson", method = RequestMethod.GET)
	public String monitoringCurriculum (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		return "admin/monitoring/curriculum";
	}
	
	//교육과정모니터링
	@RequestMapping(value = "/ajax/admin/lesson/monitoring/curriculum", method = RequestMethod.POST)
	public String monitoringCurr (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(lessonService.monitoringCurrList(param));
		return JSON_VIEW;
	}
	
	@RequestMapping(value = "/admin/lesson/monitoring/schedule", method = RequestMethod.GET)
	public String monitoringSchedule (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		return "admin/monitoring/schedule";
	}
	
	@RequestMapping(value = "/admin/lesson/monitoring/unit", method = RequestMethod.GET)
	public String monitoringUnit (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		return "admin/monitoring/unit";
	}
	
	@RequestMapping(value = "/admin/lesson/monitoring/unitSchedule", method = RequestMethod.GET)
	public String monitoringUnitSchedule (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		return "admin/monitoring/unitSchedule";
	}
	
	@RequestMapping(value = "/admin/lesson/monitoring/survey", method = RequestMethod.GET)
	public String monitoringSurvey (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		return "admin/monitoring/survey";
	}
	
	@RequestMapping(value = "/admin/lesson/monitoring/soosiGrade", method = RequestMethod.GET)
	public String monitoringSoosiGrade (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		return "admin/monitoring/soosiGrade";
	}
	
	@RequestMapping(value = "/admin/lesson/monitoring/grade", method = RequestMethod.GET)
	public String monitoringGrade (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		return "admin/monitoring/grade";
	} 
	
	@RequestMapping(value = "/admin/lesson/monitoring/asgmt", method = RequestMethod.GET)
	public String monitoringAsgmt (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		return "admin/monitoring/asgmt";
	}
	
	@RequestMapping(value = "/admin/lesson/monitoring/report", method = RequestMethod.GET)
	public String monitoringReport (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		return "admin/monitoring/report";
	}
	
	@RequestMapping(value = "/admin/lesson/curriculum", method = RequestMethod.POST)
	public String curriculumView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"curr_seq"});
		model.addAllAttributes(commonService.getAcaState(param));
		model.addAllAttributes(commonService.getCurrFlagInfo(param));	
		request.getSession().setAttribute("S_CURRICULUM_SEQ", param.get("curr_seq"));
		request.getSession().setAttribute("S_CURRENT_ACADEMIC_YEAR", param.get("year"));
		return "admin/curr/curriculumCreate";
	}
	
	//교육과정 확정/확정취소
	@RequestMapping(value = "/ajax/admin/lesson/curriculum/confirm", method = RequestMethod.POST)
	public String currConfirm (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		model.addAllAttributes(lessonService.confirmCurriculum(param));
		return JSON_VIEW;
	}
	
	//학사년도 가져오기
	@RequestMapping(value = "/ajax/admin/lesson/yearList", method = RequestMethod.POST)
	public String getAcaYearList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		model.addAllAttributes(lessonService.getAcaYear());
		return JSON_VIEW;
	}
	
	//학사리스트 가져오기
	@RequestMapping(value = "/ajax/admin/lesson/acaList", method = RequestMethod.POST)
	public String getAcademicList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"year"});
		model.addAllAttributes(lessonService.getAcaList(param));
		return JSON_VIEW;
	}
	
	//학사리스트 가져오기
	@RequestMapping(value = "/ajax/admin/lesson/leftCurrList", method = RequestMethod.POST)
	public String getLeftCurrList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"year"});
		model.addAllAttributes(lessonService.getLeftCurrList(param));
		return JSON_VIEW;
	}
	
	//수업 리스트 가져오기
	@RequestMapping(value = "/ajax/admin/lesson/leftCurrList/lpList", method = RequestMethod.POST)
	public String getLeftCurrListLpList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"curr_seq","page"});
		model.addAllAttributes(lessonService.getLeftLpList(param));
		return JSON_VIEW;
	}
	
	@RequestMapping(value = "/admin/lesson/schedule", method = RequestMethod.GET)
	public String scheduleView(HttpServletRequest request) throws Exception {
		return "admin/curr/schedule";
	}
	
	@RequestMapping(value = "/admin/lesson/scheduleM", method = RequestMethod.GET)
	public String scheduleMonthView(HttpServletRequest request) throws Exception {
		return "admin/curr/scheduleM";
	}
	
	@RequestMapping(value = "/admin/lesson/scheduleMod", method = RequestMethod.GET)
	public String scheduleModView(HttpServletRequest request) throws Exception {
		return "admin/curr/scheduleModify";
	}
	
	@RequestMapping(value = "/admin/lesson/scheduleSearch", method = RequestMethod.GET)
	public String scheduleSearchView(HttpServletRequest request) throws Exception {
		return "admin/curr/scheduleSearch";
	}	
	
	@RequestMapping(value = "/admin/lesson/survey", method = RequestMethod.GET)
	public String surveyView(HttpServletRequest request) throws Exception {
		return "admin/curr/currSFSurveyCS";
	}	
	
	@RequestMapping(value = "/admin/lesson/lessonPlanCs", method = RequestMethod.GET)
	public String lessonPlanCsView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		return "admin/curr/lessonPlanRegCs";
	}	

	@RequestMapping(value = "/admin/lesson/unit", method = RequestMethod.GET)
	public String unitView(HttpServletRequest request) throws Exception {
		return "admin/curr/unit";
	}
	
	@RequestMapping(value = "/admin/lesson/soosiGrade", method = RequestMethod.GET)
	public String soosiGradeView(HttpServletRequest request, Model model) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		model.addAllAttributes(commonService.getAcaState(param));
		model.addAttribute("soosiList", mpGradeService.getSoosiList(param));
		return "admin/curr/soosiGrade";
	}	
	
	@RequestMapping(value = "/admin/lesson/grade", method = RequestMethod.GET)
	public String gradeView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());		
		model.addAllAttributes(commonService.getAcaState(param));	
		model.addAllAttributes(commonService.getCurrFlagInfo(param));
		return "admin/curr/grade";
	}
	
	@RequestMapping(value = "/admin/lesson/report", method = RequestMethod.GET)
	public String reportView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		return "admin/curr/currReport";
	}
	
	//과제 등록 매핍
	@RequestMapping(value = "/admin/lesson/currAssignMent", method = RequestMethod.GET)
	public String currAssignMentMapping(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		String viewName = "admin/curr/currAssignMentCreate";
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		param.put("asgmt_type", "1");
		param.put("lp_seq", "");
		int asgmt_cnt = pfLessonservice.getAssignMentCount(param);
		model.addAttribute("asgmt_cnt", asgmt_cnt);

		//등록된 과제 있는지 체크. 있으면 과제등록현황으로 간다.
		if(asgmt_cnt > 0)
			viewName="admin/curr/currAssignMentView";
		
		return viewName;
	}
	
	//과제 등록 수정 페이지
	@RequestMapping(value = "/admin/lesson/currAssignMent/create", method = RequestMethod.GET)
	public String currAssignMentCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		return "admin/curr/currAssignMentCreate";
	}
	
	//과제 제출현황 페이지
	@RequestMapping(value = "/admin/lesson/currAssignMent/view", method = RequestMethod.GET)
	public String currAssignMentView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		String viewName = "admin/curr/currAssignMentView";
		return viewName;
	}
	
	//전체 수업과제 페이지
	@RequestMapping(value = "/admin/lesson/currAssignMent/lpAll", method = RequestMethod.GET)
	public String currAssignMentLpAllView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		return "admin/curr/currAssignMentLpAllView";
	}
	
	/*                        수업 관리                                                    */
	
	//수업계획서
	@RequestMapping(value = "/admin/lesson/lessonPlan", method = RequestMethod.POST)
	public String lessonPlanView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		request.getSession().setAttribute("S_CURRICULUM_SEQ", param.get("curr_seq").toString());
		request.getSession().setAttribute("S_LP_SEQ", request.getParameter("lp_seq").toString());
		request.getSession().setAttribute("S_CURRENT_ACADEMIC_YEAR", param.get("year"));
						
		model.addAttribute("lp_seq", request.getParameter("lp_seq").toString());
		
		param.put("level", "1");
		model.addAttribute("domainCodeList", commonService.getDomainCodeList(param)); //영역코드
		model.addAttribute("classRoom", commonService.getCodeList("class_room")); //강의실
		model.addAttribute("clinicCode", commonService.getCpxCode()); //임상표현
		model.addAttribute("osceCode", commonService.getOsceCode()); //임상술기
		model.addAttribute("diaCode", commonService.getDiaCode()); //진단명
		model.addAttribute("formationEvalution", commonService.getCodeList("formation_evaluation")); //형성평가
		model.addAttribute("evaluationCode", commonService.getCodeList("evaluation_code")); //평가방법
		model.addAttribute("lessonMethodCode", commonService.getCodeList("lesson_method_code")); //평가방법
		
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));	
		model.addAllAttributes(lessonService.getAcaYear());
		
		param.put("lp_seq", request.getParameter("lp_seq").toString());
		param.put("curr_seq", request.getParameter("curr_seq").toString());
		model.addAttribute("currFinishCapability", pfLessonservice.CurrFinishCapabilityList(param));//졸업역량
		
		param.put("code_cate", "osce_code_name");
		model.addAttribute("code1", academicService.getCodeMenuName(param));
		param.put("code_cate", "cpx_code_name");
		model.addAttribute("code2", academicService.getCodeMenuName(param));
		param.put("code_cate", "dia_code_name");
		model.addAttribute("code3", academicService.getCodeMenuName(param));
		return "admin/lesson/lessonPlan";
	}
	
	//수업자료
	@RequestMapping(value = "/admin/lesson/lessonData", method = RequestMethod.GET)
	public String lessonData(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		String viewName = "admin/lesson/lessonData";
		if(request.getSession().getAttribute("S_LP_SEQ") != null) {
			param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());
			model.addAttribute("menuTitle", "lessonData");
			int lesson_data_seq = pfLessonservice.getLessonDataSeq(param);
			
			model.addAttribute("lesson_data_seq", lesson_data_seq);
		}else{
			viewName = "redirect:/admin/lesson";
		}
			
		return viewName;
	}
	
	//수업자료
	@RequestMapping(value = "/admin/lesson/lessonData/view", method = RequestMethod.GET)
	public String lessonDataView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());
		int lesson_data_seq = pfLessonservice.getLessonDataSeq(param);		
		model.addAttribute("lesson_data_seq", lesson_data_seq);			
		return "admin/lesson/lessonDataView";
	}
	
	//형성평가 목록 화면
	@RequestMapping(value = "/admin/lesson/formationEvaluation", method = RequestMethod.GET)
	public String formationEvaluationView(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		return "admin/lesson/formationEvaluationList";
	}
	
	//과제 등록 매핍
	@RequestMapping(value = "/admin/lesson/assignMent", method = RequestMethod.GET)
	public String assignMentMapping(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		String viewName = "admin/lesson/assignMentCreate";
		if(request.getSession().getAttribute("S_LP_SEQ") != null) {
			param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());
			param.put("asgmt_type", "2");	
			int asgmt_cnt = pfLessonservice.getAssignMentCount(param);
			model.addAttribute("asgmt_cnt", asgmt_cnt);
			/*if(asgmt_cnt > 0)
				model.addAttribute("asgmt_seq", pfLessonservice.getAssignMentSeq(param));*/
			
			//등록된 과제 있는지 체크. 있으면 과제등록현황으로 간다.
			if(asgmt_cnt > 0)
				viewName="admin/lesson/assignMentView";
			
		}
		else
			viewName = "redirect:/admin/lesson";
		
		return viewName;
	}
	
	//과제 등록 수정 페이지
	@RequestMapping(value = "/admin/lesson/assignMent/create", method = RequestMethod.GET)
	public String assignMentCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		String viewName = "admin/lesson/assignMentCreate";
		/*if(request.getSession().getAttribute("S_LP_SEQ") != null) {
			param.put("lp_seq", request.getSession().getAttribute("S_LP_SEQ").toString());	
			param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
			param.put("asgmt_type", "1");	
			model.addAttribute("asgmt_cnt", pfLessonservice.getAssignMentCount(param));			
		}
		else
			viewName = "redirect:/pf/lesson";*/
		return viewName;
	}
	
	//과제 제출현황 페이지
	@RequestMapping(value = "/admin/lesson/assignMent/view", method = RequestMethod.GET)
	public String assignMentView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		String viewName = "admin/lesson/assignMentView";
		if(request.getSession().getAttribute("S_LP_SEQ") == null)
			viewName = "redirect:/admin/lesson";
		return viewName;
	}
	
	//출석 페이지
	@RequestMapping(value = "/admin/lesson/attendance/view", method = RequestMethod.GET)
	public String attendanceView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		String viewName = "admin/lesson/attendance";
		if(request.getSession().getAttribute("S_LP_SEQ") == null)
			viewName = "redirect:/admin/lesson";
		return viewName;
	}
	
	//만족도 조사 현황 리스트 가져오기
	@RequestMapping(value = "/ajax/admin/lesson/SFSurvey/list", method = RequestMethod.POST)
	public String SFSurveyList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ"));
		model.addAllAttributes(academicService.getSFSurveyCSList(param));	
		return JSON_VIEW;
	}
}
