package com.nsdevil.medlms.web.admin.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.exception.ResourceNotFoundException;
import com.nsdevil.medlms.common.exception.RuntimeLogicException;
import com.nsdevil.medlms.common.util.FileUploader;
import com.nsdevil.medlms.common.util.PagingHelper;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.admin.dao.UserDao;

@Service
public class UserService {
	
	@Value("#{config['config.rootPath']}")
	private String ROOT_PATH;

	@Value("#{config['config.resPath']}")
	private String RES_PATH;

	@Value("#{config['config.profilePath']}")
	private String PROFILE_PATH;

	@Value("#{config['config.initPassword']}")
	private String INIT_PASSWORD;
	
	@Autowired
	private UserDao dao;
	
	

	@Transactional
	public ResultMap pfExcelCreate(HttpServletRequest request, HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		List<HashMap<String, Object>> pfInfoList = getPfListForExcel(request);
		int successCnt = 0;
		for (HashMap<String, Object> map : pfInfoList) {
			successCnt++;
			int cnt = 0;
			//사번 중복검사
			String professorId = String.valueOf(map.get("professor_id"));
			if (professorId != null && !professorId.equals("")) {
				cnt = dao.getExistProfessorIdCount(param);
				if (cnt > 0) {
					throw new RuntimeLogicException(String.valueOf(successCnt), "301");
				}
			}
			
			//이메일 중복검사
			String email = String.valueOf(map.get("email"));
			if (email != null && !email.equals("")) {
				cnt = dao.getExistUserEmailCount(map);
				if (cnt > 0) {
					throw new RuntimeLogicException(String.valueOf(successCnt), "302");
				}
			}
			
			//아이디 중복검사 : 입력된 아이디가 잇으면 아이디로 입력된 아이디가 없으면 이메일로
			String id = String.valueOf(map.get("id"));
			if (id.equals("null")) {
				id="";
			}
			cnt = dao.getExistUserIdCount(map);
			if (cnt > 0) {
				throw new RuntimeLogicException(String.valueOf(successCnt), "303");
			}
			
			HashMap<String, Object> userParam = new HashMap<String, Object>();
			userParam.put("name", map.get("name")); //이름 (필수)
			userParam.put("professor_id", map.get("professor_id")); //사번
			userParam.put("department_code", map.get("department_code")); //직위
			userParam.put("position_code", map.get("position_code")); //소속
			userParam.put("specialty_code", map.get("specialty_code")); //세부전공
			userParam.put("tel", map.get("tel")); //연락처
			userParam.put("email", map.get("email")); //이메일 (필수)
			userParam.put("s_user_seq", param.get("s_user_seq")); //이메일 (필수)
			if (id != null && !id.equals("")) {
				userParam.put("id", id); //아이디
			} else {
				userParam.put("id", map.get("email")); //아이디 값이 없으면 이메일 주소로 아이디 등록
			}
			userParam.put("pwd", Util.getSHA256(INIT_PASSWORD)); //초기비밀번호
			if (dao.insertProfessorInfo(userParam) < 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ users insert query fail]", "004");
			}
		}
		resultMap.put("success_cnt", successCnt);
		return resultMap;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<HashMap<String, Object>> getPfListForExcel(HttpServletRequest request) throws Exception {
		List<HashMap<String, Object>> pfInfoList = new ArrayList<HashMap<String, Object>>();
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		MultipartFile uploadFile = mRequest.getFile("uploadFile");
		if (uploadFile != null) {
			@SuppressWarnings("resource")
			XSSFWorkbook workBook = new XSSFWorkbook(uploadFile.getInputStream());
			XSSFSheet curSheet = null;
			XSSFRow curRow = null;
			XSSFCell curCell = null;
			DataFormatter formatter = new DataFormatter();
			HashMap<String, Object> pfInfo = null;
			//현재 sheet 반환
			curSheet = workBook.getSheetAt(0);
			//row 탐색 for문
			for (int rowIndex = 0; rowIndex < curSheet.getPhysicalNumberOfRows(); rowIndex++) {
				//0번째 row는 Header 정보이기 때문에 pass
				if (rowIndex != 0) {
					//현재 row 반환
					curRow = curSheet.getRow(rowIndex);
					pfInfo = new HashMap<String, Object>();
					for (int cellIndex = 0; cellIndex < 10; cellIndex++) {
						curCell = curRow.getCell(cellIndex);
						if (curCell != null) {
							String value = formatter.formatCellValue(curCell);
							switch (cellIndex) {	
								case 1:
									pfInfo.put("id", value);
									break;
								case 2:
									pfInfo.put("name", value);
									break;
								case 3:
									pfInfo.put("professor_id", value);
									break;
								case 4:
									pfInfo.put("position_code", value);
									break;
								case 5:
									pfInfo.put("department_code", value);
									break;
								case 6:
									pfInfo.put("specialty_code", value);
									break;
								case 7:
									pfInfo.put("tel", value);
									break;
								case 8:
									pfInfo.put("email", value);
									break;
								default:
									break;
							}
						}
					}
					String name = String.valueOf(pfInfo.get("name"));
					String email = String.valueOf(pfInfo.get("email"));
					if (!name.equals("") && !email.equals("")) {
						pfInfoList.add((HashMap<String, Object>)pfInfo.clone());
					}
				}
			}
		}
		return pfInfoList;
	}
	
	public ResultMap pfCreate(HttpServletRequest request, HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		String profileImgUploadPath = ROOT_PATH + RES_PATH + PROFILE_PATH;
		boolean isPictureChange = false;
		param.put("professor_id", param.get("professorId"));
		param.put("email", param.get("userEmail"));
		param.put("id", param.get("userId"));
		
		int cnt = 0;
		
		//사번 중복검사
		String professorId = String.valueOf(param.get("professor_id"));
		if (professorId != null && !professorId.equals("")) {
			cnt = dao.getExistProfessorIdCount(param);
			if (cnt > 0) {
				throw new RuntimeLogicException("PROFESSOR_ID OVERLAP", "301");
			}
		}
		
		//이메일 중복검사
		String email = String.valueOf(param.get("email"));
		if (email != null && !email.equals("")) {
			cnt = dao.getExistUserEmailCount(param);
			if (cnt > 0) {
				throw new RuntimeLogicException("EMAIL OVERLAP", "302");
			}
		}
		
		//아이디 중복검사 : 입력된 아이디가 잇으면 아이디로 입력된 아이디가 없으면 이메일로
		String id = String.valueOf(param.get("id"));
		cnt = dao.getExistUserIdCount(param);
		if (cnt > 0) {
			throw new RuntimeLogicException("ID OVERLAP", "303");
		}
		
		HashMap<String, Object> userParam = new HashMap<String, Object>();
		userParam.put("name", param.get("userName")); //이름 (필수)
		userParam.put("professor_id", param.get("professor_id")); //사번
		userParam.put("department_code", param.get("departmentCode")); //소속
		userParam.put("position_code", param.get("positionCode")); //직위
		userParam.put("specialty_code", param.get("specialtyCode")); //세부전공
		userParam.put("tel", param.get("tel")); //연락처
		userParam.put("email", param.get("email")); //이메일 (필수)
		userParam.put("s_user_seq", param.get("s_user_seq")); //이메일 (필수)
		
		if (id != null && !id.equals("")) {
			userParam.put("id", id); //아이디
		} else {
			userParam.put("id", param.get("email")); //아이디 값이 없으면 이메일 주소로 아이디 등록
		}
		userParam.put("pwd", Util.getSHA256(INIT_PASSWORD)); //초기비밀번호

		// 파일업로드
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		MultipartFile profileImgFile = mRequest.getFile("uploadFile");
		if (profileImgFile != null && profileImgFile.getSize() > 0) {
			String realName = FileUploader.uploadFile(profileImgUploadPath, profileImgFile);
			if (realName == null || realName.equals("")) {
				throw new RuntimeLogicException("FILE_UPLOAD_FAIL [ profile image ]", "003");
			}
			profileImgUploadPath = PROFILE_PATH + "/" + realName;
			isPictureChange = true;
		}
		
		if (isPictureChange) {
			userParam.put("picture_path", profileImgUploadPath);
			userParam.put("picture_name", profileImgFile.getOriginalFilename());
		}
		
		if (dao.insertProfessorInfo(userParam) < 1) {
			throw new RuntimeLogicException("QUERY_FAIL [ users insert query fail]", "004");
		}
		
		return resultMap;
	}
	
	
	public ResultMap getPFList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		int totalCnt = dao.getPFContentsCount(param);
		PagingHelper pagingHelper = new PagingHelper(20, 10);
		List<HashMap<String, Object>> contentsList = dao.getPFContentsList(pagingHelper.getPagingParam(param, totalCnt));
		resultMap.putAll(pagingHelper.getPageList(contentsList, (String)param.get("listFunctionName"), totalCnt));
		return resultMap;
	}
	
	public ResultMap getAcademicList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		HashMap<String, Object> acaParam = new HashMap<String, Object>();
		//년도
		List<HashMap<String, Object>> yearList = new ArrayList<HashMap<String, Object>>();
		yearList = dao.getYearAcaList(param);
		
		//대분류
		List<HashMap<String, Object>> lAcaList = new ArrayList<HashMap<String, Object>>();
		lAcaList = dao.getAcaList(acaParam);
		
		//중분류
		List<HashMap<String, Object>> mAcaList = new ArrayList<HashMap<String, Object>>();
		String lSeq = String.valueOf(param.get("l_seq"));
		if (!lSeq.equals("")) {
			acaParam.put("l_seq", param.get("l_seq"));
			mAcaList = dao.getAcaList(acaParam);
		}
		
		//소분류
		List<HashMap<String, Object>> sAcaList = new ArrayList<HashMap<String, Object>>();
		String mSeq = String.valueOf(param.get("m_seq"));
		if (!mSeq.equals("")) {
			acaParam.put("m_seq", param.get("m_seq"));
			sAcaList = dao.getAcaList(acaParam);
		}
		
		//기간
		List<HashMap<String, Object>> pAcaList = new ArrayList<HashMap<String, Object>>();
		String sSeq = String.valueOf(param.get("s_seq"));
		if (!sSeq.equals("")) {
			acaParam.put("s_seq", param.get("s_seq"));
			pAcaList = dao.getAcaList(acaParam);
		}
		
		resultMap.put("year_list", yearList);
		resultMap.put("l_aca_list", lAcaList);
		resultMap.put("m_aca_list", mAcaList);
		resultMap.put("s_aca_list", sAcaList);
		resultMap.put("p_aca_list", pAcaList);
		
		return resultMap;
	}
	
	public ResultMap getPFDetail(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		HashMap<String, Object> userInfo = new HashMap<String, Object>();
		userInfo = dao.getPFDetail(param);
		if (userInfo == null || userInfo.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_PF_USER_INFO", "007");
		}
		//교육과정 목록
		List<HashMap<String, Object>> currList = new ArrayList<HashMap<String, Object>>();
		currList = dao.getCurriculumList(userInfo);
		for (HashMap<String, Object> currMap : currList) {
			List<HashMap<String, Object>> lpList = new ArrayList<HashMap<String, Object>>();
			currMap.put("user_seq", userInfo.get("user_seq"));
			lpList = dao.getLessonPlanList(currMap);
			currMap.put("lp_list", lpList);
		}
		resultMap.put("user_info", userInfo);
		resultMap.put("curr_list", currList);
		return resultMap;
	}
	
	public ResultMap userPwdInitModify(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		String pwd = Util.getSHA256(INIT_PASSWORD);
		param.put("pwd", pwd);
		int cnt = dao.getUserPwdCount(param);
		if (cnt < 1) { // 비밀번호가 다를때만 업데이트
			if (dao.updateUserPwdInit(param) < 1) {
				throw new RuntimeLogicException("QUERY_FAIL [users update pwd fail ]", "004");
			}
		}
		return resultMap;
	}
	
	public ResultMap pfModify(HttpServletRequest request, HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		String profileImgUploadPath = ROOT_PATH + RES_PATH + PROFILE_PATH;
		boolean isPictureChange = false;
		
		//수정 정보
		HashMap<String, Object> userParam = new HashMap<String, Object>();
		userParam.put("user_seq", param.get("userSeq"));
		userParam.put("name", param.get("userName"));
		userParam.put("attend_code", param.get("attendCode"));
		userParam.put("department_code", param.get("departmentCode"));
		userParam.put("position_code", param.get("positionCode"));
		userParam.put("specialty_code", param.get("specialtyCode"));
		userParam.put("tel", param.get("tel"));
		userParam.put("professor_id", param.get("professorId"));
		userParam.put("email", param.get("userEmail"));
		userParam.put("id", param.get("userId"));
		userParam.put("account_use_state", param.get("accountUseState"));
		
		//기존 정보
		int cnt = 0;
		
		//사번
		String professorId = String.valueOf(userParam.get("professor_id"));
		if (professorId != null && !professorId.equals("")) {
			cnt = dao.getExistProfessorIdCount(param);
			if (cnt > 0) {
				throw new RuntimeLogicException("PROFESSOR_ID OVERLAP", "301");
			}
		}
		
		//이메일
		String userEmail = String.valueOf(userParam.get("email"));
		if (userEmail != null && !userEmail.equals("")) {
			cnt = dao.getExistUserEmailCount(userParam);
			if (cnt > 0) {
				throw new RuntimeLogicException("EMAIL OVERLAP", "302");
			}
		}
		
		//아이디
		String userId = String.valueOf(userParam.get("id"));
		cnt = dao.getExistUserIdCount(userParam);
		if (cnt > 0) {
			throw new RuntimeLogicException("ID OVERLAP", "303");
		}
		
		if (userId == null || userId.equals("")) {
			userParam.put("id", userParam.get("email"));
		}

		// 파일업로드
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		MultipartFile profileImgFile = mRequest.getFile("uploadFile");
		if (profileImgFile != null && profileImgFile.getSize() > 0) {
			String realName = FileUploader.uploadFile(profileImgUploadPath, profileImgFile);
			if (realName == null || realName.equals("")) {
				throw new RuntimeLogicException("FILE_UPLOAD_FAIL [ profile image ]", "003");
			}
			profileImgUploadPath = PROFILE_PATH + "/" + realName;
			isPictureChange = true;
		}
		
		if (isPictureChange) {
			userParam.put("picture_path", profileImgUploadPath);
			userParam.put("picture_name", profileImgFile.getOriginalFilename());
		} else {
			userParam.put("picture_path", null);
			userParam.put("picture_name", null);
		}
		
		
		if (dao.updateProfessorInfo(userParam) < 0) {
			throw new RuntimeLogicException("QUERY_FAIL [ users update query fail ]", "004");
		}
		
		return resultMap;
	}
	
	public ResultMap getAttendCountList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		List<HashMap<String, Object>> codePFCountList = new ArrayList<HashMap<String, Object>>();
		codePFCountList = dao.getAttendCountList(param);
		resultMap.put("code_cnt_list", codePFCountList);
		param.put("user_level", 3);
		resultMap.put("no_code_pf_cnt", dao.getNoCodeCount(param)); //code가 없는 교수 수
		param.put("user_level", 2);
		resultMap.put("no_code_staff_cnt", dao.getNoCodeCount(param)); //code가 없는 직원 수
		return resultMap;
	}
	
	public ResultMap getCodeCountList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		List<HashMap<String, Object>> codePFCountList = new ArrayList<HashMap<String, Object>>();
		codePFCountList = dao.getCodeCountList(param);
		resultMap.put("code_pf_cnt_list", codePFCountList);
		resultMap.put("no_code_pf_cnt", dao.getNoCodePFCount(param)); //code가 없는 교수 수
		return resultMap;
	}
	
	
	@Transactional
	public ResultMap codeListModify(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		JSONParser jsonParser = new JSONParser();
		JSONArray codeArray = (JSONArray) jsonParser.parse(String.valueOf(param.get("codeListStr")));
		JSONArray removeCodeArray = (JSONArray) jsonParser.parse(String.valueOf(param.get("removeCodeListStr")));
		
		List<HashMap<String, Object>> codeList = Util.fromJson(codeArray);
		List<HashMap<String, Object>> removeCodeList = Util.fromJson(removeCodeArray);

		//1. 삭제
		for (HashMap<String, Object> removeCodeParam : removeCodeList) {
			removeCodeParam.put("code_cate", param.get("code_cate"));
			removeCodeParam.put("remove_code_column", param.get("remove_code_column"));
			removeCodeParam.put("remove_user_level", param.get("remove_user_level"));
			removeCodeParam.put("s_user_seq", param.get("s_user_seq"));
			if (dao.deleteCode(removeCodeParam) < 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ code update query fail ]", "004");
			}
			
			dao.deleteUsersCode(removeCodeParam);
			int removeUserLevel = (int)param.get("remove_user_level");
			if (removeUserLevel == 2) {//재직상태는 교수:3, 직원:2 두번 지워야함
				removeCodeParam.put("remove_user_level", 3);
				dao.deleteUsersCode(removeCodeParam);
			}
		}
		//2. 수정 or 삽입
		for (HashMap<String, Object> codeParam : codeList) {
			codeParam.put("code_cate", param.get("code_cate"));
			String code = String.valueOf(codeParam.get("code"));
			if (code.equals("")) { // insert
				code = getMaxCode(param);
				codeParam.put("code", code);
				if (dao.insertCode(codeParam) < 1) {
					throw new RuntimeLogicException("QUERY_FAIL [ code insert query fail ]", "004");
				}
			} else { // update
				if (dao.updateCode(codeParam) < 1) {
					throw new RuntimeLogicException("QUERY_FAIL [ code update query fail ]", "004");
				}
			}
		}
		return resultMap;
	}
	
	
	public String getMaxCode(HashMap<String, Object> param) throws Exception {
		String returnCode = dao.getMaxCode(param);
		if (returnCode == null || returnCode.equals("")) {
			returnCode = "1";
		} else {
			int maxCode = Integer.parseInt(returnCode);
			returnCode = String.valueOf(maxCode + 1);
		}
		return returnCode;
	}
	
	
	public ResultMap staffCreate(HttpServletRequest request, HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		String profileImgUploadPath = ROOT_PATH + RES_PATH + PROFILE_PATH;
		boolean isPictureChange = false;
		param.put("professor_id", param.get("professorId"));
		param.put("email", param.get("userEmail"));
		param.put("id", param.get("userId"));
		
		int cnt = 0;
		
		//사번 중복검사
		String professorId = String.valueOf(param.get("professor_id"));
		if (professorId != null && !professorId.equals("")) {
			cnt = dao.getExistProfessorIdCount(param);
			if (cnt > 0) {
				throw new RuntimeLogicException("PROFESSOR_ID OVERLAP", "301");
			}
		}
		
		//이메일 중복검사
		String email = String.valueOf(param.get("email"));
		if (email != null && !email.equals("")) {
			cnt = dao.getExistUserEmailCount(param);
			if (cnt > 0) {
				throw new RuntimeLogicException("EMAIL OVERLAP", "302");
			}
		}
		
		//아이디 중복검사 : 입력된 아이디가 잇으면 아이디로 입력된 아이디가 없으면 이메일로
		String id = String.valueOf(param.get("id"));
		cnt = dao.getExistUserIdCount(param);
		if (cnt > 0) {
			throw new RuntimeLogicException("ID OVERLAP", "303");
		}
		
		HashMap<String, Object> userParam = new HashMap<String, Object>();
		userParam.put("name", param.get("userName")); //이름 (필수)
		userParam.put("professor_id", param.get("professor_id")); //사번
		userParam.put("department_code", param.get("departmentCode")); //소속
		userParam.put("tel", param.get("tel")); //연락처
		userParam.put("email", param.get("email")); //이메일 (필수)
		userParam.put("s_user_seq", param.get("s_user_seq")); //이메일 (필수)
		if (id != null && !id.equals("")) {
			userParam.put("id", id); //아이디
		} else {
			userParam.put("id", param.get("email")); //아이디 값이 없으면 이메일 주소로 아이디 등록
		}
		userParam.put("pwd", Util.getSHA256(INIT_PASSWORD)); //초기비밀번호

		// 파일업로드
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		MultipartFile profileImgFile = mRequest.getFile("uploadFile");
		if (profileImgFile != null && profileImgFile.getSize() > 0) {
			String realName = FileUploader.uploadFile(profileImgUploadPath, profileImgFile);
			if (realName == null || realName.equals("")) {
				throw new RuntimeLogicException("FILE_UPLOAD_FAIL [ profile image ]", "003");
			}
			profileImgUploadPath = PROFILE_PATH + "/" + realName;
			isPictureChange = true;
		}
		
		if (isPictureChange) {
			userParam.put("picture_path", profileImgUploadPath);
			userParam.put("picture_name", profileImgFile.getOriginalFilename());
		}
		
		if (dao.insertStaffInfo(userParam) < 1) {
			throw new RuntimeLogicException("QUERY_FAIL [ users insert query fail]", "004");
		}
		
		return resultMap;
	}
	
	
	public ResultMap getStaffList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		int totalCnt = dao.getStaffContentsCount(param);
		PagingHelper pagingHelper = new PagingHelper(20, 10);
		List<HashMap<String, Object>> contentsList = dao.getStaffContentsList(pagingHelper.getPagingParam(param, totalCnt));
		resultMap.putAll(pagingHelper.getPageList(contentsList, (String)param.get("listFunctionName"), totalCnt));
		return resultMap;
	}
	
	
	public ResultMap getStaffDetail(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		HashMap<String, Object> userInfo = new HashMap<String, Object>();
		userInfo = dao.getStaffDetail(param);
		if (userInfo == null || userInfo.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_STAFF_USER_INFO", "007");
		}
		resultMap.put("user_info", userInfo);
		return resultMap;
	}
	
	public ResultMap staffModify(HttpServletRequest request, HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		String profileImgUploadPath = ROOT_PATH + RES_PATH + PROFILE_PATH;
		boolean isPictureChange = false;
		
		//수정 정보
		HashMap<String, Object> userParam = new HashMap<String, Object>();
		userParam.put("user_seq", param.get("userSeq"));
		userParam.put("name", param.get("userName"));
		userParam.put("attend_code", param.get("attendCode"));
		userParam.put("department_code", param.get("departmentCode"));
		userParam.put("tel", param.get("tel"));
		userParam.put("professor_id", param.get("professorId"));
		userParam.put("email", param.get("userEmail"));
		userParam.put("id", param.get("userId"));
		userParam.put("account_use_state", param.get("accountUseState"));
		
		//기존 정보
		int cnt = 0;
		
		//사번
		String professorId = String.valueOf(userParam.get("professor_id"));
		if (professorId != null && !professorId.equals("")) {
			cnt = dao.getExistProfessorIdCount(param);
			if (cnt > 0) {
				throw new RuntimeLogicException("PROFESSOR_ID OVERLAP", "301");
			}
		}
		
		//이메일
		String userEmail = String.valueOf(userParam.get("email"));
		if (userEmail != null && !userEmail.equals("")) {
			cnt = dao.getExistUserEmailCount(userParam);
			if (cnt > 0) {
				throw new RuntimeLogicException("EMAIL OVERLAP", "302");
			}
		}
		
		//아이디
		String userId = String.valueOf(userParam.get("id"));
		cnt = dao.getExistUserIdCount(userParam);
		if (cnt > 0) {
			throw new RuntimeLogicException("ID OVERLAP", "303");
		}
		
		if (userId == null || userId.equals("")) {
			userParam.put("id", userParam.get("email"));
		}

		// 파일업로드
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		MultipartFile profileImgFile = mRequest.getFile("uploadFile");
		if (profileImgFile != null && profileImgFile.getSize() > 0) {
			String realName = FileUploader.uploadFile(profileImgUploadPath, profileImgFile);
			if (realName == null || realName.equals("")) {
				throw new RuntimeLogicException("FILE_UPLOAD_FAIL [ profile image ]", "003");
			}
			profileImgUploadPath = PROFILE_PATH + "/" + realName;
			isPictureChange = true;
		}
		
		if (isPictureChange) {
			userParam.put("picture_path", profileImgUploadPath);
			userParam.put("picture_name", profileImgFile.getOriginalFilename());
		} else {
			userParam.put("picture_path", null);
			userParam.put("picture_name", null);
		}
		
		
		if (dao.updateStaffInfo(userParam) < 0) {
			throw new RuntimeLogicException("QUERY_FAIL [ users update query fail ]", "004");
		}
		
		return resultMap;
	}
	
}
 