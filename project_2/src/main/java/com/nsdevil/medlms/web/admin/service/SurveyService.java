package com.nsdevil.medlms.web.admin.service;

import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.exception.RuntimeLogicException;
import com.nsdevil.medlms.web.admin.dao.SurveyDao;

@Service
public class SurveyService {
	
	@Autowired
	private SurveyDao surveyDao;

	public ResultMap getSFResearchList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("sfResearchList", surveyDao.getSFResearchList(param));
		return resultMap;
	}

	@Transactional
	public ResultMap insertSFResearch(HashMap<String, Object> param) throws Exception{
		ResultMap resultMap = new ResultMap();
		
		JSONParser jsonParser = new JSONParser();
		
		JSONObject jsonObject = (JSONObject) jsonParser.parse(param.get("sfResearchList").toString());
		
		JSONArray researchList = (JSONArray) jsonObject.get("list");
				
		if(param.get("sr_seq").toString().isEmpty()) {			
			if(surveyDao.insertSFResearch(param) != 1)
				throw new RuntimeLogicException("QUERY_FAIL [ satisfaction_research insert query fail ]", "004");
		}else {
			surveyDao.updateSFResearchAll(param);
			
			if(surveyDao.updateSFResearch(param) != 1)
				throw new RuntimeLogicException("QUERY_FAIL [ satisfaction_research update query fail ]", "004");
		}
				
		for(int i=0; i<researchList.size(); i++) {	
			JSONObject research = (JSONObject) researchList.get(i);
			param.put("srh_seq", research.get("srh_seq"));
			param.put("srh_order", (i+1));
			param.put("srh_subject", research.get("srh_subject"));
			param.put("srh_explan", research.get("srh_explan"));
			param.put("srh_type", research.get("srh_type"));
			param.put("srh_detail_type", research.get("srh_detail_type"));
			param.put("srh_num", research.get("srh_num"));
			param.put("essential_flag", research.get("essential_flag"));
			if(param.get("srh_seq").toString().isEmpty()) {			
				if(surveyDao.insertSFResearchHeader(param) != 1)
					throw new RuntimeLogicException("QUERY_FAIL [ satisfaction_research_header insert query fail ]", "004");
			}else {
				if(surveyDao.updateSFResearchHeader(param) != 1)
					throw new RuntimeLogicException("QUERY_FAIL [ satisfaction_research_header update query fail ]", "004");
			}			
			
			if(research.containsKey("sri_item_explan")) {
				JSONArray itemSeqList = (JSONArray) research.get("sri_seq");
				JSONArray itemExplanList = (JSONArray) research.get("sri_item_explan");
				
				for(int j=0; j<itemExplanList.size() ; j++) {
					if(itemSeqList.get(j).toString().isEmpty()) {
						param.put("sri_seq", "");
						param.put("sri_item_explan", itemExplanList.get(j));
						param.put("sri_item_no", (j+1));
						
						if(surveyDao.insertSFResearchItem(param) != 1)
							throw new RuntimeLogicException("QUERY_FAIL [ satisfaction_research_item insert query fail ]", "004");
					}else {
						param.put("sri_seq", itemSeqList.get(j));
						param.put("sri_item_explan", itemExplanList.get(j));
						param.put("sri_item_no", (j+1));
						
						if(surveyDao.updateSFResearchItem(param) != 1)
							throw new RuntimeLogicException("QUERY_FAIL [ satisfaction_research_item update query fail ]", "004");
					}
				}
			}
		}
		
		return resultMap;
	}

	public ResultMap insertSFResearchBasic(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		if(surveyDao.insertSFResearchVersion(param) != 1)
			throw new RuntimeLogicException("QUERY_FAIL [ satisfaction_research_basic insert query fail ]", "004");
		return resultMap;
	}

	public ResultMap getSFResearchQuestionList(HashMap<String, Object> param) {
		ResultMap resultMap = new ResultMap();
		resultMap.put("questionList", surveyDao.getSFResearchQuestionList(param));
		return resultMap;
	}
	
}
