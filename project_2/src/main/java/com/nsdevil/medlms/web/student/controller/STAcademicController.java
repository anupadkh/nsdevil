package com.nsdevil.medlms.web.student.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.web.student.service.STAcademicService;

@Controller
public class STAcademicController extends ExceptionController {
	
	@Autowired
	private STAcademicService stAcademicService;
		
	//수강신청 리스트
	@RequestMapping(value = "/st/aca/caList", method = RequestMethod.GET)
	public String caView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
				
		return "st/academic/courseApplication";
	}
	
	
	@RequestMapping(value="/ajax/st/aca/caList", method = RequestMethod.POST)
	public String caList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(stAcademicService.getCourseApplicationList(param));
		
		return JSON_VIEW;
	}	
	
	//수강신청 상세
	@RequestMapping(value = "/st/aca/caDetail", method = RequestMethod.POST)
	public String caDetailView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAttribute("ca_seq", param.get("ca_seq"));
		return "st/academic/courseApplicationDetail";
	}
	
	@RequestMapping(value="/ajax/st/aca/caDetail", method = RequestMethod.POST)
	public String caDetailInfo(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(stAcademicService.getCourseApplicationDetail(param));
		
		return JSON_VIEW;
	}	
	
	@RequestMapping(value="/ajax/st/aca/ca/insert", method = RequestMethod.POST)
	public String caInsert(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(stAcademicService.insertCa(param));
		
		return JSON_VIEW;
	}
}
