package com.nsdevil.medlms.web.admin.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.admin.service.AcademicService;
import com.nsdevil.medlms.web.common.service.CommonService;
import com.nsdevil.medlms.web.mpf.service.MPFLessonService;
import com.nsdevil.medlms.web.pf.service.PFLessonService;

@Controller
public class AcademicController extends ExceptionController {
	
	@Autowired
	private AcademicService service;

	@Autowired
	private CommonService commonService;
	
	@Autowired
	private MPFLessonService MPFservice;
		
	@Autowired
	private PFLessonService pfLessonservice;
		
	//학사체계
	@RequestMapping(value = {"/admin/academic/academicSystem/modify", "/admin/academic/academicSystem"}, method = RequestMethod.GET)
	public String academicSystemModifyView(HttpServletRequest request, @RequestParam HashMap<String, Object> param)  throws Exception {
		return "admin/academic/academicSystemModify";
	}
	
	//학사체계 저장
	@RequestMapping(value = "/ajax/admin/academic/academicSystem/create", method = RequestMethod.POST)
	public String academicSystemCreate(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		Util.requiredCheck(param, new String[] {"lv","aca_system_name"});
		if(param.get("l_seq") == null)
			param.remove("l_seq");
		else
			param.put("l_seq", Integer.parseInt(param.get("l_seq").toString()));
		
		if(param.get("m_seq") == null)
			param.remove("m_seq");
		else
			param.put("m_seq", Integer.parseInt(param.get("m_seq").toString()));
		
		if(param.get("s_seq") == null)
			param.remove("s_seq");
		else
			param.put("s_seq", Integer.parseInt(param.get("s_seq").toString()));
		
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("level", Integer.parseInt(param.get("lv").toString().replaceAll("lv", "")));
		
		model.addAllAttributes(service.academicSystemCreate(param));
		return JSON_VIEW;
	}
	
	//학사체계 가져오기
	@RequestMapping(value = "/ajax/admin/academic/academicSystem/list", method = RequestMethod.GET)
	public String academicSystemList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		List<HashMap<String, Object>> hashmap = service.academicSystemListNoAdd(param);
		model.addAttribute("list", hashmap);
		return JSON_VIEW;
	}
	
	//학사체계 분류명 수정
	@RequestMapping(value = "/ajax/admin/academic/academicSystem/name/modify", method = RequestMethod.POST)
	public String academicSystemNameModify(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		Util.requiredCheck(param, new String[] {"aca_system_seq","aca_system_name"});		
		param.put("upt_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		
		model.addAllAttributes(service.academicSystemAcaNameModify(param));
		return JSON_VIEW;
	}	

	//학사체계 순서 수정
	@RequestMapping(value = "/ajax/admin/academic/academicSystem/order/modify", method = RequestMethod.POST)
	public String academicSystemAcaOrderModify(HttpServletRequest request,  Model model, @RequestParam(value="seq_order") String[] seq_order) throws Exception {
		//Util.requiredCheck(param, new String[] {"seq_order"});	
		HashMap<String, Object> hashmap = new HashMap<String, Object>();
		hashmap.put("upt_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		for(int i=0; i<seq_order.length ;i++){
			if(seq_order[i].split("_")[0].equals("0"))
				continue;
			hashmap.put("aca_system_seq", seq_order[i].split("_")[0]);
			hashmap.put("aca_system_order", seq_order[i].split("_")[1]);
			
			model.addAllAttributes(service.academicSystemAcaOrderModify(hashmap));
		}
		return JSON_VIEW;
	}	

	//학사체계 삭제
	@RequestMapping(value = "/ajax/admin/academic/academicSystem/delete", method = RequestMethod.POST)
	public String academicSystemDelete(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		Util.requiredCheck(param, new String[] {"aca_system_seq"});	
		param.put("upt_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.academicSystemDelete(param));
		return JSON_VIEW;
	}	
	
	
	
	/*********************************************************************************************************************************
	 
	 교육과정 관리
	 
	 *********************************************************************************************************************************/
	

	
	/* 학사체계 단계별 가져오기
	 * param level, l_seq, m_seq, s_seq
	 */
	@RequestMapping(value = "/ajax/admin/academic/academicSystem/acaStageOfList", method = RequestMethod.POST)
	public String academicSystemStageOfList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		Util.requiredCheck(param, new String[] {"level"});
		model.addAttribute("list", service.academicSystemSategeOfList(param));
				
		return JSON_VIEW;
	}

	//교육과정 조회 맵핑
	@RequestMapping(value = {"/admin/academic/curriculum/list", "/admin/academic/curriculum"}, method = RequestMethod.GET)
	public String curriculumListView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		model.addAttribute("completeCodeList", commonService.getCodeList("complete")); //이수구분코드
		model.addAttribute("administerCodeList", commonService.getCodeList("administer")); //관리구분코드
		return "admin/academic/curriculumView";
	}
	
	//교육과정 리스트 가져오기
	@RequestMapping(value = "/ajax/admin/academic/curriculum/list", method = RequestMethod.POST)
	public String curriculumList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		model.addAllAttributes(service.curriculumList(param));
		
		return JSON_VIEW;
	}
	//카운트 취득
			
	//교육과정 등록 맵핑
	@RequestMapping(value = "/admin/academic/curriculum/create", method = RequestMethod.POST)
	public String curriculumListCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {

		model.addAttribute("completeCodeList", commonService.getCodeList("complete")); //이수구분코드
		model.addAttribute("administerCodeList", commonService.getCodeList("administer")); //관리구분코드
		model.addAttribute("specialtyCodeList", commonService.getCodeList("specialty")); //세부전공
		param.put("level", 1);		
		model.addAttribute("lseqList", service.academicSystemSategeOfList(param));
		
		if(request.getParameter("curr_seq") != null){
			param.put("curr_seq", request.getParameter("curr_seq"));
			
			HashMap<String, Object> hashmap = service.curriculumOneList(param);		
			model.addAttribute("currList", hashmap);
			
			param.put("level", 2);
			param.put("l_seq",hashmap.get("l_seq"));
			model.addAttribute("mseqList", service.academicSystemSategeOfList(param));
			
			param.put("level", 3);
			param.put("l_seq",hashmap.get("l_seq"));
			param.put("m_seq",hashmap.get("m_seq"));
			model.addAttribute("sseqList", service.academicSystemSategeOfList(param));

			//등록된 책임교수 가져오기
			param.put("professor_level", 1);			
			model.addAttribute("mpfList", service.curriculumMpAssignPfList(param));
			
			//부책임교수 가져오기
			param.put("professor_level", 2);
			model.addAttribute("pfList", service.curriculumMpAssignPfList(param));
		}
		
		return "admin/academic/curriculumCreate";
	}
	
	//교육과정 등록에서 보기 맵핑
	@RequestMapping(value = "/admin/academic/curriculum/create/view", method = RequestMethod.POST)
	public String curriculumView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAttribute("curr_seq", param.get("curr_seq"));
		
		return "admin/academic/curriculumCreateView";
	}
	
	//교육과정 보기에서 가져오기
	@RequestMapping(value = "/ajax/admin/academic/curriculum/view/info", method = RequestMethod.POST)
	public String getCurriculumInfo(HttpServletRequest request,  Model model,@RequestParam HashMap<String, Object> param) throws Exception {
		Util.requiredCheck(param, new String[] {"curr_seq"});
		
		HashMap<String, Object> hashmap = service.curriculumOneList(param);		
		model.addAttribute("currList", hashmap);

		//등록된 책임교수 가져오기
		param.put("professor_level", 1);			
		model.addAttribute("mpfList", service.curriculumMpAssignPfList(param));
		
		//부책임교수 가져오기
		param.put("professor_level", 2);
		model.addAttribute("pfList", service.curriculumMpAssignPfList(param));
		
		return JSON_VIEW;
	}
	
	//교육과정 저장 && 수정
	@RequestMapping(value = "/ajax/admin/academic/curriculum/create", method = RequestMethod.POST)
	public String curriculumCreate(HttpServletRequest request,  Model model,@RequestParam HashMap<String, Object> param) throws Exception {
		Util.requiredCheck(param, new String[] {"auto_flag","curr_name","aca_system_seq"});

		
		String[] mpf_user_seq = request.getParameterValues("mpf_user_seq");
		String[] pf_user_seq = request.getParameterValues("pf_user_seq");
				
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));	
		
		model.addAllAttributes(service.curriculumInsert(param, mpf_user_seq, pf_user_seq));
		
		return JSON_VIEW;
	}
		
	//교육과정 한개 가져오기
	@RequestMapping(value = "/ajax/admin/academic/curriculum/one/list", method = RequestMethod.POST)
	public String curriculumOneList(HttpServletRequest request,  Model model, 
			@RequestParam HashMap<String, Object> param) throws Exception {
		
		model.addAttribute("curr_data", service.curriculumOneList(param));
		
		return JSON_VIEW;
	}
	
	//교육과정 코드 카운트 중복 체크용
	@RequestMapping(value = "/ajax/admin/academic/curriculum/currcode/count", method = RequestMethod.POST)
	public String curriculumCodeCheck(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		Util.requiredCheck(param, new String[] {"curr_code"});
		String chk = "N";
		if(service.curriculumCodeCount(param) != 0)
			chk = "Y";
		
		model.addAttribute("chk", chk);
		
		return JSON_VIEW;
	}
	
	//교육과정 삭제
	@RequestMapping(value = "/ajax/admin/academic/curriculum/delete", method = RequestMethod.POST)
	public String curriculumDelete(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		Util.requiredCheck(param, new String[] {"curr_seqs"});	
		param.put("upt_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		try {
			model.addAllAttributes(service.curriculumDelete(param));
		}catch(Exception e) {
			model.addAttribute("status", "201");
			model.addAttribute("msg", e.getMessage());
		}
		return JSON_VIEW;
	}	
	

	/**
	 * 엑셀 양식 출력
	 */
	@RequestMapping(value = "/admin/academic/curriculum/templateXlxsDown", method = RequestMethod.GET)
	public String curriculumTemplateExcelDownload(HttpServletRequest req, Model model,@RequestParam HashMap<String, Object> param, HttpServletResponse response)  throws Exception {
		
		//엑셀 제목 크기
		int[] sheetWidth = {1500, 4000, 6000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000};
		int[] sheet2Width = {5000, 6000};
		int[] sheet3Width = {1500, 3000, 4000, 6000, 6000};

		//제목
		String[] sheetTitle = {"No","교육과정코드","교육과정명","학사체계분류코드","이수구분코드","학점코드","학점","관리구분코드","대상코드","신청비용","책임교수코드","부책임교수코드"};
		String[] sheet3Title = {"No","과","사번","이름","책임교수코드"};

		String curDate= (new SimpleDateFormat("yyyy-MM-dd")).format( new Date() );
		
		model.addAttribute("excel_view_type", "curriculumXlsxTemplate");
		model.addAttribute("filename","curriculumXlsx_"+curDate);
		
		model.addAttribute("sheetName","sheet1");
		model.addAttribute("sheetTitle", sheetTitle);
		model.addAttribute("academicSystemList", service.academicSystemTemplateList()); //학사체계코드
		model.addAttribute("completeCodeList", commonService.getCodeList("complete")); //이수구분코드
		model.addAttribute("administerCodeList", commonService.getCodeList("administer")); //관리구분코드
		model.addAttribute("pfList", MPFservice.getprofessor(param)); //책임교수
		model.addAttribute("sheetWidth",sheetWidth);
		model.addAttribute("sheet2Width",sheet2Width);
		model.addAttribute("sheet3Width",sheet3Width);
		
		model.addAttribute("sheet3Title", sheet3Title);

		response.setHeader("Content-disposition", "attachment; filename=" + "curriculumXlsx_"+curDate+".xlsx"); 
		return "excelDownloadView";
	}
	
	/**
	 * 교육과정관리 엑셀 출력
	 */
	@RequestMapping(value = "/admin/academic/curriculum/curriculumListXlxsDown", method = RequestMethod.GET)
	public String curriculumListExcelDownload(HttpServletRequest req, Model model,@RequestParam HashMap<String, Object> param, HttpServletResponse response)  throws Exception {
		
		//엑셀 제목 크기
		int[] sheetWidth = {1500, 4000, 3000, 3500, 5000, 4000, 5000, 6000, 4000, 3500, 3000, 3500, 3500};

		//제목
		String[] sheetTitle = {"No","대분류","중분류","소분류","교육과정코드","등록일","관리코드","교육과정명","책임교수","이수구분","학점", "관리구분", "대상"};

		String curDate= (new SimpleDateFormat("yyyy-MM-dd")).format( new Date() );
		
		model.addAttribute("excel_view_type", "curriculumListXlxsDown");
		model.addAttribute("filename","curriculumListXlxsDown_"+curDate);
		
		model.addAttribute("sheetName","sheet1");
		model.addAttribute("sheetTitle", sheetTitle);
		model.addAttribute("sheetWidth",sheetWidth);
		model.addAttribute("curriculumList", service.curriculumListExcelDown(param));

		response.setHeader("Content-disposition", "attachment; filename=" + "curriculumListXlxsDown_"+curDate+".xlsx"); 
		
		return "excelDownloadView";
	}
	
	//교육과정 엑셀 업로드
	@RequestMapping(value = "/ajax/admin/curriculum/excel/create", method = RequestMethod.POST)
	public String curriculumExcelCreate (MultipartHttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"xlsFile"});

		MultipartFile uploadFile = request.getFile("xlsFile");
		
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));

		try{
			model.addAllAttributes(service.curriculumExcelCreate(uploadFile, param));
		}catch(Exception e){
			model.addAttribute("status",e.getMessage());
		}
		return JSON_VIEW;
	}
	
	

	/*********************************************************************************************************************************
	 
	 학사등록 관리
	 
	 *********************************************************************************************************************************/
	
	//교육과정 조회 맵핑
	@RequestMapping(value = {"/admin/academic/academicReg/list", "/admin/academic/academicReg"}, method = RequestMethod.GET)
	public String academicListView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {		
		
		model.addAllAttributes(service.getAcademicYearList());
		
		return "admin/academic/academicView";
	}
	
	//교육과정 리스트 가져오기
	@RequestMapping(value = "/ajax/admin/academic/academicReg/list", method = RequestMethod.POST)
	public String academicList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
						
		model.addAllAttributes(service.academicList(param));
		
		return JSON_VIEW;
	}
	
	//교육과정 엑셀 다운로드
	@RequestMapping(value = "/admin/academic/academicReg/excelDown", method = RequestMethod.GET)
	public String academicExcelDown(HttpServletRequest request,  Model model
			, @RequestParam HashMap<String, Object> param
			, HttpServletResponse response) throws Exception {
		
		ResultMap resultMap = service.academicExcelList(param);
		//엑셀 제목 크기
		int[] sheetWidth = {1500, 2500, 4000, 3000, 3000, 4000, 7000, 8000, 6000, 3500, 3500};

		//제목
		String[] sheetTitle = {"No","년도","대분류","중분류","소분류","기간","학사기간","학사명","학사상태","교육과정배정","교육과정계획서등록"};

		String curDate= (new SimpleDateFormat("yyyy-MM-dd")).format( new Date() );
		
		model.addAttribute("excel_view_type", "academicListXlxsDown");
		model.addAttribute("filename","academicListXlxsDown_"+curDate);
		
		model.addAttribute("sheetName","sheet1");
		model.addAttribute("sheetTitle", sheetTitle);
		model.addAttribute("sheetWidth",sheetWidth);
		model.addAttribute("academicList", resultMap.get("list"));

		response.setHeader("Content-disposition", "attachment; filename=" + "academicListXlxsDown_"+curDate+".xlsx"); 
		
		return "excelDownloadView";
	}
	
	//학사등록 맵핑
	@RequestMapping(value = {"/admin/academic/academicReg/academicRegBasicInfo/create"}, method = RequestMethod.POST)
	public String academicCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {		
		Calendar cal = Calendar.getInstance();
		DateFormat df = new SimpleDateFormat("yyyy");
		cal.setTime(new Date());
		model.addAttribute("thisYear", df.format(cal.getTime()));
		cal.add(Calendar.YEAR, 1);
		model.addAttribute("nextYear", df.format(cal.getTime()));		
		request.getSession().setAttribute("S_ACA_SEQ", request.getParameter("aca_seq"));
		return "admin/academic/academicRegBasicInfo";
	}
	
	//학사체계 학생 수 가져오기
	@RequestMapping(value = "/ajax/admin/academic/academicReg/academicRegBasicInfo/studentCount", method = RequestMethod.POST)
	public String academicStudentCount(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		
		model.addAllAttributes(service.academicStudentCount(param));
		
		return JSON_VIEW;
	}

	//학사 등록 
	@RequestMapping(value = "/ajax/admin/academic/academicReg/academicRegBasicInfo/insert", method = RequestMethod.POST)
	public String academicInsert(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		model.addAllAttributes(service.academicInsert(param));
		
		//저장 후 반환값 있을 때 , 중복체크 걸리면 aca_seq 반환 값 없음.
		if(param.containsKey("aca_seq")) {
			request.getSession().setAttribute("S_ACA_SEQ", param.get("aca_seq").toString());
		}
		
		return JSON_VIEW;
	}	

	//학사 삭제 
	@RequestMapping(value = "/ajax/admin/academic/academicReg/academicRegBasicInfo/delete", method = RequestMethod.POST)
	public String academicDelete(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		model.addAllAttributes(service.academicDelete(param));
				
		return JSON_VIEW;
	}	
	
	//학사 상태 변경 
	@RequestMapping(value = "/ajax/admin/academic/academicReg/academicRegBasicInfo/stateUpdate", method = RequestMethod.POST)
	public String academicStateUpdate(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		Util.requiredCheck(param, new String[] {"user_seq","aca_seq","aca_state"});
		model.addAllAttributes(service.academicStateUpdate(param));
		
		return JSON_VIEW;
	}
	
	//학사 확정 
	@RequestMapping(value = "/ajax/admin/academic/academicReg/academicRegBasicInfo/confirm", method = RequestMethod.POST)
	public String academicAssign(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		Util.requiredCheck(param, new String[] {"aca_seq","confirm_name","flag"});
		model.addAllAttributes(service.academicConfirm(param));
		
		return JSON_VIEW;
	}
	
	//학사 상세 가져오기 
	@RequestMapping(value = "/ajax/admin/academic/academicReg/academicRegBasicInfo/list", method = RequestMethod.POST)
	public String academicGetBaseInfo(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {		
		model.addAllAttributes(service.academicGetBaseInfoList(param));
		
		return JSON_VIEW;
	}
	
	//교육과정배정 맵핑
	@RequestMapping(value = {"/admin/academic/academicReg/curriculumAssign"}, method = RequestMethod.GET)
	public String curriculumAssignView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ").toString());
		model.addAllAttributes(service.getAcaConfirmInfo(param));
	
		return "admin/academic/academicCurriCulumAssign";
	}
	
	//학사 타이틀 정보 가져오기 
	@RequestMapping(value = "/ajax/admin/academic/academicReg/titleInfo", method = RequestMethod.POST)
	public String academicTitleInfo(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {	
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ").toString());
		Util.requiredCheck(param, new String[] {"aca_seq"});
		model.addAllAttributes(service.academicGetBaseInfoList(param));
		return JSON_VIEW;
	}
	
	//학사 타이틀 정보 가져오기 
	@RequestMapping(value = "/ajax/admin/academic/academicReg/curriculumAssign/list", method = RequestMethod.POST)
	public String curriculumAssignList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {	
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ").toString());
		Util.requiredCheck(param, new String[] {"aca_seq"});
		model.addAllAttributes(service.curriculumAssignList(param));
		return JSON_VIEW;
	}
	
	//교육과정 배정 저장 
	@RequestMapping(value = "/ajax/admin/academic/academicReg/curriculumAssign/assignInsert", method = RequestMethod.POST)
	public String curriculumAssignInsert(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {	
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ").toString());
		Util.requiredCheck(param, new String[] {"aca_seq"});
		String[] curr_seq = request.getParameterValues("curr_seq");
		model.addAllAttributes(service.curriculumAssignInsert(param, curr_seq));
		return JSON_VIEW;
	}
	
	//학사관리 시간표 맵핑
	@RequestMapping(value = {"/admin/academic/academicReg/schedule"}, method = RequestMethod.GET)
	public String scheduleView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {	
		
	
		return "admin/academic/academicScheduleList";
	}
	
	//시간표 교육과정 리스트 가져오기	 
	@RequestMapping(value = "/ajax/admin/academic/academicReg/schedule/currList", method = RequestMethod.POST)
	public String academicSchdCurrList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {	
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ").toString());
		Util.requiredCheck(param, new String[] {"aca_seq"});
		model.addAllAttributes(service.getSchdCurrList(param));
		return JSON_VIEW;
	}
	
	//학사시퀀스 세션에 등록	 
	@RequestMapping(value = "/ajax/admin/academic/academicReg/setAcaSeq", method = RequestMethod.POST)
	public String setAcaSeq(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {			
		Util.requiredCheck(param, new String[] {"aca_seq"});
		request.getSession().setAttribute("S_ACA_SEQ", param.get("aca_seq").toString());
		return JSON_VIEW;
	}
	
	//종합성적 맵핑
	@RequestMapping(value = {"/admin/academic/academicReg/currScore"}, method = RequestMethod.GET)
	public String currScoreView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {	
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ"));
		model.addAllAttributes(service.getAcaCurrList(param));
		return "admin/academic/academicCurrGrade";
	}
	
	//종합성적 가져오기
	@RequestMapping(value = {"/ajax/admin/academic/academicReg/currScore/list"}, method = RequestMethod.POST)
	public String currScoreList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {	
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ"));
		Util.requiredCheck(param, new String[] {"aca_seq"});
		model.addAllAttributes(service.getGradeScoreList(param));
		return JSON_VIEW;
	}
	
	//종합성적 등록 맵핑
	@RequestMapping(value = {"/admin/academic/academicReg/currScore/create"}, method = RequestMethod.POST)
	public String currScoreCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {	
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ"));
		model.addAllAttributes(commonService.getAcaState(param));	
		model.addAllAttributes(commonService.getCurrFlagInfo(param));
		model.addAttribute("curr_seq", param.get("curr_seq").toString());
		model.addAttribute("curr_name", param.get("curr_name").toString());
		param.remove("curr_seq");
		model.addAllAttributes(service.getAcaCurrList(param));
		return "admin/academic/academicCurrGradCreate";
	}
	
	//종합성적 엑셀 다운로드
	@RequestMapping(value = "/admin/academic/academicReg/currScore/excelDown", method = RequestMethod.POST)
	public String currScoreExcelDown(HttpServletRequest request,  Model model
			, @RequestParam HashMap<String, Object> param
			, HttpServletResponse response) throws Exception {
		
		JSONParser jsonParser = new JSONParser();
		
		JSONObject jsonObject = (JSONObject) jsonParser.parse(param.get("stList").toString());
		
		JSONArray scoreList = (JSONArray) jsonObject.get("list");
				
		List<HashMap<String,Object>> map = new ArrayList<HashMap<String, Object>>();
		
		if(scoreList != null) {
			int size = scoreList.size();
			for(int i=0; i<size; i++) {
				JSONObject detailList = (JSONObject) scoreList.get(i);
				HashMap<String, Object> detailMap = new ObjectMapper().readValue(detailList.toJSONString(), HashMap.class);
				map.add(detailMap);
			}
		}
		
		//엑셀 제목 크기
		int[] sheetWidth = {3000, 2500, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000};

		String curDate= (new SimpleDateFormat("yyyy-MM-dd")).format( new Date() );
		
		model.addAttribute("excel_view_type", "currScoreXlxsDown");
		model.addAttribute("filename","currScoreXlxsDown_"+curDate);
				
		model.addAttribute("sheetName","sheet1");
		model.addAttribute("sheetWidth",sheetWidth);
		model.addAttribute("scoreList", map);
		
		model.addAttribute("no", param.get("no").toString());
		model.addAttribute("curr_name", param.get("curr_name").toString());
		model.addAttribute("gradeInfo", param.get("gradeInfo").toString());

		response.setHeader("Content-disposition", "attachment; filename=" + "currScoreXlxsDown_"+curDate+".xlsx"); 
		
		return "excelDownloadView";
	}
	
	
	//수시성적 맵핑
	@RequestMapping(value = {"/admin/academic/academicReg/soosiScore"}, method = RequestMethod.GET)
	public String soosiScoreView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {	
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ"));
		model.addAllAttributes(service.getAcaCurrList(param));
		return "admin/academic/academicCurrSooSiGrade";
	}
	
	//수시성적 가져오기
	@RequestMapping(value = {"/ajax/admin/academic/academicReg/soosiScore/list"}, method = RequestMethod.POST)
	public String soosiScoreList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {	
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ"));
		
		Util.requiredCheck(param, new String[] {"aca_seq"});
		model.addAllAttributes(service.getSooSiGradeScoreList(param));
		return JSON_VIEW;
	}
	
	//수시성적 등록 맵핑
	@RequestMapping(value = {"/admin/academic/academicReg/soosiScore/create"}, method = RequestMethod.POST)
	public String soosiScoreCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {	
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ"));
		model.addAttribute("curr_seq", param.get("curr_seq").toString());
		model.addAttribute("curr_name", param.get("curr_name").toString());
		param.remove("curr_seq");
		model.addAllAttributes(service.getAcaCurrList(param));
		return "admin/academic/academicCurrSooSiGradCreate";
	}
	
	//수시성적 양식 엑셀 다운로드
	@RequestMapping(value = "/admin/academic/academicReg/soosiScore/TmplExcelDown", method = RequestMethod.GET)
	public String TmplExcelDown(HttpServletRequest request,  Model model
			, @RequestParam HashMap<String, Object> param
			, HttpServletResponse response) throws Exception {
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ"));		
		//엑셀 제목 크기
		int[] sheetWidth = {3000, 5000, 4000, 4000, 4000};

		String curDate= (new SimpleDateFormat("yyyy-MM-dd")).format( new Date() );
		
		model.addAttribute("excel_view_type", "currScoreTmplDown");
		model.addAttribute("filename","currScoreTmplDown_"+curDate);
				
		model.addAttribute("sheetName","sheet1");
		model.addAttribute("sheetWidth",sheetWidth);
		model.addAttribute("stList", service.getGradeStList(param));

		response.setHeader("Content-disposition", "attachment; filename=" + "currScoreTmplDown_"+curDate+".xlsx"); 
		
		return "excelDownloadView";
	}	

	//수시성적 엑셀 업로드
	@RequestMapping(value = "/ajax/admin/academicReg/soosiScore/excel/create", method = RequestMethod.POST)
	public String soosiScoreExcelCreate (MultipartHttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		

		MultipartFile uploadFile = request.getFile("xlsFile");
		
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));

		try{
			model.addAllAttributes(service.sooSiScoreExcelCreate(uploadFile, param));
		}catch(Exception e){
			model.addAttribute("status",e.getMessage());
		}
		return JSON_VIEW;
	}

	//수시성적 가져오기
	@RequestMapping(value = "/ajax/admin/academicReg/soosiScore/create/list", method = RequestMethod.POST)
	public String sooSiScoreList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		

		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ"));	

		model.addAllAttributes(service.getSooSiScore(param));

		return JSON_VIEW;
	}	

	//수시성적 저장하기
	@RequestMapping(value = "/ajax/admin/academicReg/soosiScore/create/insert", method = RequestMethod.POST)
	public String sooSiScoreInsert (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));	

		model.addAllAttributes(service.insertSooSiScore(param));

		return JSON_VIEW;
	}
	
	//수시성적 교육과정 전체 엑셀 다운로드
	@RequestMapping(value = "/admin/academic/academicReg/soosiScore/excelDown", method = RequestMethod.POST)
	public String soosiScoreExcelDown(HttpServletRequest request,  Model model
			, @RequestParam HashMap<String, Object> param
			, HttpServletResponse response) throws Exception {
		
		JSONParser jsonParser = new JSONParser();
		
		JSONObject jsonObject = (JSONObject) jsonParser.parse(param.get("stList").toString());
		
		JSONArray scoreList = (JSONArray) jsonObject.get("list");
				
		List<HashMap<String,Object>> map = new ArrayList<HashMap<String, Object>>();
		
		if(scoreList != null) {
			int size = scoreList.size();
			for(int i=0; i<size; i++) {
				JSONObject detailList = (JSONObject) scoreList.get(i);
				HashMap<String, Object> detailMap = new ObjectMapper().readValue(detailList.toJSONString(), HashMap.class);
				map.add(detailMap);
			}
		}
		
		//엑셀 제목 크기
		int[] sheetWidth = {3000, 2500, 2000, 2000, 4000, 4000, 4000, 4000, 4000, 4000, 4000};

		String curDate= (new SimpleDateFormat("yyyy-MM-dd")).format( new Date() );
		
		model.addAttribute("excel_view_type", "sooSiScoreXlxsDown");
		model.addAttribute("filename","currScoreXlxsDown_"+curDate);
				
		model.addAttribute("sheetName","sheet1");
		model.addAttribute("sheetWidth",sheetWidth);
		model.addAttribute("scoreList", map);
		
		model.addAttribute("no", param.get("no").toString());
		model.addAttribute("curr_name", param.get("curr_name").toString());
		model.addAttribute("srcCnt", param.get("srcCnt").toString());

		response.setHeader("Content-disposition", "attachment; filename=" + "currScoreXlxsDown_"+curDate+".xlsx"); 
		
		return "excelDownloadView";
	}
	
	//수시성적 교육과정 하나 엑셀 다운로드
	@RequestMapping(value = "/admin/academic/academicReg/soosiScore/create/excelDown", method = RequestMethod.POST)
	public String currSoosiScoreExcelDown(HttpServletRequest request,  Model model
			, @RequestParam HashMap<String, Object> param
			, HttpServletResponse response) throws Exception {
		
		JSONParser jsonParser = new JSONParser();
		
		JSONObject jsonObject = (JSONObject) jsonParser.parse(param.get("stList").toString());
		
		JSONArray scoreList = (JSONArray) jsonObject.get("list");
				
		List<HashMap<String,Object>> map = new ArrayList<HashMap<String, Object>>();
		
		if(scoreList != null) {
			int size = scoreList.size();
			for(int i=0; i<size; i++) {
				JSONObject detailList = (JSONObject) scoreList.get(i);
				HashMap<String, Object> detailMap = new ObjectMapper().readValue(detailList.toJSONString(), HashMap.class);
				map.add(detailMap);
			}
		}
		
		//엑셀 제목 크기
		int[] sheetWidth = {1500, 3000, 2500, 1500};

		String curDate= (new SimpleDateFormat("yyyy-MM-dd")).format( new Date() );
		
		model.addAttribute("excel_view_type", "currSooSiScoreXlxsDown");
				
		model.addAttribute("sheetName","sheet1");
		model.addAttribute("sheetWidth",sheetWidth);
		model.addAttribute("scoreList", map);
		
		model.addAttribute("no", param.get("no").toString());
		model.addAttribute("curr_name", param.get("curr_name").toString());
		model.addAttribute("srcDate", param.get("srcDate").toString());

		response.setHeader("Content-disposition", "attachment; filename=" + "currSooSiScoreXlxsDown_"+curDate+".xlsx"); 
		
		return "excelDownloadView";
	}
		
	//형성평가 맵핑
	@RequestMapping(value = {"/admin/academic/academicReg/feScore"}, method = RequestMethod.GET)
	public String feScoreView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {	
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ"));
		model.addAllAttributes(service.getAcaCurrList(param));
		return "admin/academic/academicCurrFeGrade";
	}
	
	//형성평가 점수 가져오기
	@RequestMapping(value = "/ajax/admin/academicReg/feScore/list", method = RequestMethod.POST)
	public String FeScoreList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		

		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ"));	

		model.addAllAttributes(service.getFeScore(param));

		return JSON_VIEW;
	}	

	//학생개별 종합 성적 가져오기(팝업 창)
	@RequestMapping(value = {"/ajax/admin/academic/academicReg/currScore/stScore"}, method = RequestMethod.POST)
	public String currStScore(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {	
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ"));
		Util.requiredCheck(param, new String[] {"aca_seq"});
		model.addAllAttributes(service.getCurrGradeStScore(param));
		return JSON_VIEW;
	}
	
	//학생개별 수시 성적 가져오기(팝업 창)
	@RequestMapping(value = {"/ajax/admin/academic/academicReg/soosiScore/stScore"}, method = RequestMethod.POST)
	public String soosiStScore(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {	
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ"));
		Util.requiredCheck(param, new String[] {"aca_seq"});
		model.addAllAttributes(service.getSoosiGradeStScore(param));
		return JSON_VIEW;
	}
	
	//운영보고서 맵핑
	@RequestMapping(value = {"/admin/academic/academicReg/currReport"}, method = RequestMethod.GET)
	public String currReportView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		return "admin/academic/academicCurrReportList";
	}
	
	//운영보고서 정보 가져오기
	@RequestMapping(value = "/ajax/admin/academic/academicReg/currReport/list", method = RequestMethod.POST)
	public String getReportInfo(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {			
		model.addAllAttributes(service.getReportInfo(param));
		return JSON_VIEW;
	}
	
	//팝업 수업 리스트 가져오기
	@RequestMapping(value = "/ajax/admin/academic/academicReg/currReport/popup/list", method = RequestMethod.POST)
	public String getReportPopupList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		model.addAllAttributes(pfLessonservice.getPopupLessonList(param));
		return JSON_VIEW;
	}
	/*********************************************************************************************************************************
	 
	졸업역량 관리
	 
	 *********************************************************************************************************************************/
	
	
	//졸업역량 매핑
	@RequestMapping(value = {"/admin/academic/graduationCapability/modify", "/admin/academic/graduationCapability"}, method = RequestMethod.GET)
	public String graduationCapabilityModifyView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		
		
		return "admin/academic/graduationCapability";
	}
	

	//졸업역량 리스트 가져오기
	@RequestMapping(value = "/ajax/admin/academic/graduationCapability/list", method = RequestMethod.POST)
	public String graduationCapabilityList(HttpServletRequest request,  Model model, @RequestParam HashMap<String, Object> param) throws Exception {
						
		model.addAttribute("list", service.graduationCapabilityList(param));
		
		return JSON_VIEW;
	}
	
	//졸업역량 버전 리스트 가져오기
	@RequestMapping(value = "/ajax/admin/academic/graduationCapability/versionList", method = RequestMethod.POST)
	public String graduationCapabilityVersionList(HttpServletRequest request,  Model model) throws Exception {
		
		model.addAttribute("list", service.gradCapabilityVersionList());
		model.addAttribute("last_version", service.gradCapabilityLastVersion());
		return JSON_VIEW;
	}
		
	
	//졸업역량 엑셀 업로드
	@RequestMapping(value = "/ajax/admin/graduationCapability/excel/create", method = RequestMethod.POST)
	public String graduationCapabilityExcelCreate (MultipartHttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		Util.requiredCheck(param, new String[] {"xlsFile"});

		MultipartFile uploadFile = request.getFile("xlsFile");
		
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.graduationCapabilityExcelCreate(uploadFile, param));	
		return JSON_VIEW;
	}

	//수업만족도 조사 현황 매핑
	@RequestMapping(value = {"/admin/academic/SFSurvey/list", "/admin/academic/graduationCapability"}, method = RequestMethod.GET)
	public String SFSurveyView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ").toString());
		
		model.addAllAttributes(service.getCurrNameList(param));
		
		return "admin/academic/academicSFSurveyCS";
	}
	
	//수업만족도 조사 현황 리스트 가져오기
	@RequestMapping(value = "/ajax/admin/academic/SFSurvey/list", method = RequestMethod.POST)
	public String SFSurveyList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ").toString());
		Util.requiredCheck(param, new String[] {"aca_seq"});
		model.addAllAttributes(service.getSFSurveyCSList(param));	
		return JSON_VIEW;
	}
	
	//수업리스트 가져오기
	@RequestMapping(value = "/ajax/admin/academic/SFSurvey/lessonList", method = RequestMethod.POST)
	public String lessonList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"curr_seq"});
		model.addAllAttributes(service.getLessonList(param));	
		return JSON_VIEW;
	}
	
	//교수목록 가져오기
	@RequestMapping(value = "/ajax/admin/academic/SFSurvey/pfList", method = RequestMethod.POST)
	public String pfList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"aca_seq"});
		model.addAllAttributes(service.getPFList(param));	
		return JSON_VIEW;
	}
	
	//수업만족도 미제출 학생 가져오기
	@RequestMapping(value = "/ajax/admin/academic/SFSurvey/stList", method = RequestMethod.POST)
	public String UnsubmissionSTList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"curr_seq"});
		model.addAllAttributes(service.getUnsubmissionSTList(param));	
		return JSON_VIEW;
	}
	
	//과정만족도 미제출 학생 가져오기
	@RequestMapping(value = "/ajax/admin/academic/SFSurvey/curStList", method = RequestMethod.POST)
	public String UnsubmissionCurrSTList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"curr_seq"});
		model.addAllAttributes(service.getUnsubmissionCurrSTList(param));	
		return JSON_VIEW;
	}
	
	//수업만족도 결과 가져오기
	@RequestMapping(value = "/ajax/admin/academic/SFSurvey/resultList", method = RequestMethod.POST)
	public String SFSurveyResultList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		Util.requiredCheck(param, new String[] {"lp_seq"});
		model.addAllAttributes(service.getSRSurveyResult(param));	
		return JSON_VIEW;
	}
	
	//교육과정만족도 결과 가져오기
	@RequestMapping(value = "/ajax/admin/academic/SFSurvey/currResultList", method = RequestMethod.POST)
	public String SFSurveyCurrResultList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		Util.requiredCheck(param, new String[] {"curr_seq"});
		model.addAllAttributes(service.getSRSurveyCurrResult(param));	
		return JSON_VIEW;
	}
		
	//수업만족도 현황 엑셀 다운로드
	@RequestMapping(value = "/admin/academic/SFSurvey/excelDown", method = RequestMethod.GET)
	public String SFSurveyExcelDown(HttpServletRequest request,  Model model
			, @RequestParam HashMap<String, Object> param
			, HttpServletResponse response) throws Exception {
		//param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ").toString());
		ResultMap resultMap = service.getSFSurveyCSList(param);
		//엑셀 제목 크기
		int[] sheetWidth = {1500, 3000, 6000, 3000, 3000, 2500, 3000, 3000, 6000};

		//제목
		String[] sheetTitle = {"No","교육과정코드","수업명","수업일","교수명","평균점","수업만족도여부","제출상태","미제출학생"};

		String curDate= (new SimpleDateFormat("yyyy-MM-dd")).format( new Date() );
		
		model.addAttribute("excel_view_type", "SFSurveyXlxsDown");
		model.addAttribute("filename","SFSurveyXlxsDown_"+curDate);
		
		model.addAttribute("sheetName","sheet1");
		model.addAttribute("sheetTitle", sheetTitle);
		model.addAttribute("sheetWidth",sheetWidth);
		model.addAttribute("surveyList", resultMap.get("surveyList"));

		response.setHeader("Content-disposition", "attachment; filename=" + "SFSurveyXlxsDown_"+curDate+".xlsx"); 
		
		return "excelDownloadView";
	}
		
	//학사 학생배정
	@RequestMapping(value = {"/admin/academic/academicReg/studentAssign"}, method = RequestMethod.GET)
	public String studentAssign(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ").toString());
		model.addAllAttributes(service.getAcademicYearList());
		model.addAllAttributes(service.getAcaConfirmInfo(param));
		return "admin/academic/academicStudentAssign";
	}
	
	//학생 배정된 수
	@RequestMapping(value = "/ajax/admin/academic/academicReg/studentAssign/assignStCnt", method = RequestMethod.POST)
	public String getAcaAssignStCnt (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ").toString());
		model.addAllAttributes(service.getAcaAssignStCnt(param));
		return JSON_VIEW;
	}
	
	//학생 리스트 가져오기
	@RequestMapping(value = "/ajax/admin/academic/academicReg/studentAssign/stlist", method = RequestMethod.POST)
	public String getStudentList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ").toString());
		model.addAllAttributes(service.getStudentList(param));
		return JSON_VIEW;
	}

	//학생 배정하기
	@RequestMapping(value = "/ajax/admin/academic/academicReg/studentAssign/insert", method = RequestMethod.POST)
	public String insertAssignSt (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ").toString());
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.insertAcaAssignSt(param));
		return JSON_VIEW;
	}

	//학생 배정 취소
	@RequestMapping(value = "/ajax/admin/academic/academicReg/studentAssign/cancel", method = RequestMethod.POST)
	public String cancelAssignSt (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ").toString());
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.cancelAcaAssignSt(param));
		return JSON_VIEW;
	}
	
	//학생 배정 확정
	@RequestMapping(value = "/ajax/admin/academic/academicReg/studentAssign/confirm", method = RequestMethod.POST)
	public String assignStConfirm (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ").toString());
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.assignStConfirm(param));
		return JSON_VIEW;
	}
	
	//학생 배정 확정 취소
	@RequestMapping(value = "/ajax/admin/academic/academicReg/studentAssign/cancelConfirm", method = RequestMethod.POST)
	public String assignStConfirmCancel (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("aca_seq", request.getSession().getAttribute("S_ACA_SEQ").toString());
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.assignStConfirmCancel(param));
		return JSON_VIEW;
	}
	/*********************************************************************************************************************************
	 
	학사코드 관리
	 
	 *********************************************************************************************************************************/
	
	//학사 코드관리 매핑 - 이수구분
	@RequestMapping(value = {"/admin/academic/codeManagement/completeCode"}, method = RequestMethod.GET)
	public String completeCodetView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		
		return "admin/academic/code/completeCode";
	}
	
	//코드테이블에 있는거는 리스트
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/codeList", method = RequestMethod.POST)
	public String getCodeList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		Util.requiredCheck(param, new String[] {"code_cate"});
		
		model.addAttribute("codeList", commonService.getCodeList(param.get("code_cate").toString())); //이수구분코드
		return JSON_VIEW;
	}
	
	//코드테이블에 삭제
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/deleteCode", method = RequestMethod.POST)
	public String deleteCode (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		Util.requiredCheck(param, new String[] {"code_cate", "code"});
		
		model.addAllAttributes(service.deleteCode(param));
		return JSON_VIEW;
	}
	
	//코드테이블에 있는거는 리스트
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/insert", method = RequestMethod.POST)
	public String insertCode (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		model.addAllAttributes(service.insertCode(request, param));
		return JSON_VIEW;
	}

	//학사 코드관리 매핑 - 형성평가
	@RequestMapping(value = {"/admin/academic/codeManagement/feCode"}, method = RequestMethod.GET)
	public String feCodetView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		
		return "admin/academic/code/feCode";
	}
	
	//학사 코드관리 매핑 - 관리구분
	@RequestMapping(value = {"/admin/academic/codeManagement/administerCode"}, method = RequestMethod.GET)
	public String administerCodetView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		
		return "admin/academic/code/administerCode";
	}

	//학사 코드관리 매핑 - 비강의유형
	@RequestMapping(value = {"/admin/academic/codeManagement/lessonMethodCode"}, method = RequestMethod.GET)
	public String lessonMethodCodeView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		
		return "admin/academic/code/lessonMethodCode";
	}
	
	//학사 코드관리 매핑 - 총괄평가기준
	@RequestMapping(value = {"/admin/academic/codeManagement/seCode"}, method = RequestMethod.GET)
	public String seCodeView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		
		return "admin/academic/code/seCode";
	}
	
	//총괄평가기준 가져오기
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/seCodeList", method = RequestMethod.POST)
	public String getSeCodeList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(service.getSeCodeList());
		return JSON_VIEW;
	}
	
	//총괄평가기준 삭제
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/deleteSeCode", method = RequestMethod.POST)
	public String deleteSeCode (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		Util.requiredCheck(param, new String[] {"l_se_code"});
		model.addAllAttributes(service.deleteSeCode(param));
		return JSON_VIEW;
	}
		
	//총괄평가기준 저장
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/seCodeInsert", method = RequestMethod.POST)
	public String getSeCodeInsert (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(service.seCodeInsert(param));
		return JSON_VIEW;
	}
	
	//학사 코드관리 매핑 - 수업영역/수준
	@RequestMapping(value = {"/admin/academic/codeManagement/unitCode"}, method = RequestMethod.GET)
	public String unitCodeView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		
		return "admin/academic/code/unitCode";
	}
	
	//수업영역/수준 가져오기
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/unitCodeList", method = RequestMethod.POST)
	public String getUnitCodeList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(service.getUnitCodeList());
		return JSON_VIEW;
	}
	
	//수업영역/수준 저장
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/unitCodeInsert", method = RequestMethod.POST)
	public String getUnitCodeInsert (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(service.unitCodeInsert(param));
		return JSON_VIEW;
	}
	

	//수업영역/수준 삭제
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/deleteUnitCode", method = RequestMethod.POST)
	public String deleteUnitCode (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		Util.requiredCheck(param, new String[] {"l_uc_code"});
		model.addAllAttributes(service.deleteUnitCode(param));
		return JSON_VIEW;
	}

	//학사 코드관리 매핑 - 학점기준
	@RequestMapping(value = {"/admin/academic/codeManagement/gradeCode"}, method = RequestMethod.GET)
	public String gradeCodeView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		
		return "admin/academic/code/gradeCode";
	}
	
	//학점기준 가져오기
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/gradeCodeList", method = RequestMethod.POST)
	public String getGradeCodeList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(service.getGradeCodeList());
		return JSON_VIEW;
	}
	
	//학점기준 저장
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/gradeCodeInsert", method = RequestMethod.POST)
	public String GradeCodeInsert (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.gradeCodeInsert(param, request));
		return JSON_VIEW;
	}	

	//학점기준 삭제
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/deleteGradeCode", method = RequestMethod.POST)
	public String deleteGradeCode (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		Util.requiredCheck(param, new String[] {"gs_seq"});
		model.addAllAttributes(service.deleteGradeCode(param));
		return JSON_VIEW;
	}
	
	//코드1 맵핑
	@RequestMapping(value = {"/admin/academic/codeManagement/code1"}, method = RequestMethod.GET)
	public String code1View(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		param.put("code_cate", "osce_code_name");
		model.addAllAttributes(service.getCodeMenuName(param));
		return "admin/academic/code/code1";
	}

	//코드1 코드명으로 항목 가져오기
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/code1/search", method = RequestMethod.POST)
	public String code1Search (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(service.getCode1SearchList(param));
		return JSON_VIEW;
	}
	
	//코드1 가져오기
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/code1/list", method = RequestMethod.POST)
	public String code1List (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(service.getCode1List(param));
		return JSON_VIEW;
	}

	//코드1 삭제
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/code1/delete", method = RequestMethod.POST)
	public String code1Delete (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		model.addAllAttributes(service.deleteCode1(param));
		return JSON_VIEW;
	}
	
	//코드1 수정
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/code1/update", method = RequestMethod.POST)
	public String code1Update (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		model.addAllAttributes(service.updateCode1(param));
		return JSON_VIEW;
	}	

	//코드1 등록 맵핑
	@RequestMapping(value = {"/admin/academic/codeManagement/code1/reg"}, method = RequestMethod.GET)
	public String code1Reg(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		param.put("code_cate", "osce_code_name");
		model.addAllAttributes(service.getCodeMenuName(param));
		return "admin/academic/code/code1Reg";
	}
	
	//코드1 등록
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/code1/insert", method = RequestMethod.POST)
	public String code1Insert (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		model.addAllAttributes(service.insertCode1(param));
		return JSON_VIEW;
	}	
	
	//코드1 엑셀 다운로드
	@RequestMapping(value = "/admin/academic/codeManagement/code1/excelDown", method = RequestMethod.GET)
	public String code1ExcelDown(HttpServletRequest request,  Model model
			, @RequestParam HashMap<String, Object> param
			, HttpServletResponse response) throws Exception {
		
		List<HashMap<String, Object>> list = service.getCode1ExcelList();
		//엑셀 제목 크기
		int[] sheetWidth = {1500,7000};

		//제목
		String[] sheetTitle = {"No", "항목명"};

		String curDate= (new SimpleDateFormat("yyyy-MM-dd")).format( new Date() );
		
		model.addAttribute("excel_view_type", "code1");
		model.addAttribute("filename","code1_"+curDate);
		
		model.addAttribute("sheetName","sheet1");
		model.addAttribute("sheetTitle", sheetTitle);
		model.addAttribute("sheetWidth",sheetWidth);
		model.addAttribute("codeList", list);

		response.setHeader("Content-disposition", "attachment; filename=" + "code1"+curDate+".xlsx"); 
		
		return "excelDownloadView";
	}
	
	/* 코드 2	*/	
	
	//코드2 맵핑
	@RequestMapping(value = {"/admin/academic/codeManagement/code2"}, method = RequestMethod.GET)
	public String code2View(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		param.put("code_cate", "cpx_code_name");
		model.addAllAttributes(service.getCodeMenuName(param));
		return "admin/academic/code/code2";
	}

	//코드2 코드명으로 항목 가져오기
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/code2/search", method = RequestMethod.POST)
	public String code2Search (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(service.getCode2SearchList(param));
		return JSON_VIEW;
	}
	
	//코드2 가져오기
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/code2/list", method = RequestMethod.POST)
	public String code2List (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(service.getCode2List(param));
		return JSON_VIEW;
	}

	//코드2 삭제
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/code2/delete", method = RequestMethod.POST)
	public String code2Delete (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		model.addAllAttributes(service.deleteCode2(param));
		return JSON_VIEW;
	}
	
	//코드2 수정
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/code2/update", method = RequestMethod.POST)
	public String code2Update (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		model.addAllAttributes(service.updateCode2(param));
		return JSON_VIEW;
	}	

	//코드2 등록 맵핑
	@RequestMapping(value = {"/admin/academic/codeManagement/code2/reg"}, method = RequestMethod.GET)
	public String code2Reg(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		param.put("code_cate", "cpx_code_name");
		model.addAllAttributes(service.getCodeMenuName(param));
		return "admin/academic/code/code2Reg";
	}
	
	//코드2 등록
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/code2/insert", method = RequestMethod.POST)
	public String code2Insert (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		model.addAllAttributes(service.insertCode2(param));
		return JSON_VIEW;
	}	
	
	//코드1 엑셀 다운로드
	@RequestMapping(value = "/admin/academic/codeManagement/code2/excelDown", method = RequestMethod.GET)
	public String code2ExcelDown(HttpServletRequest request,  Model model
			, @RequestParam HashMap<String, Object> param
			, HttpServletResponse response) throws Exception {

		List<HashMap<String, Object>> list = service.getCode2ExcelList();
		//엑셀 제목 크기
		int[] sheetWidth = {1500,7000};

		//제목
		String[] sheetTitle = {"NO", "항목명"};

		String curDate= (new SimpleDateFormat("yyyy-MM-dd")).format( new Date() );
		
		model.addAttribute("excel_view_type", "code2");
		model.addAttribute("filename","code2_"+curDate);
		
		model.addAttribute("sheetName","sheet1");
		model.addAttribute("sheetTitle", sheetTitle);
		model.addAttribute("sheetWidth",sheetWidth);
		model.addAttribute("codeList", list);

		response.setHeader("Content-disposition", "attachment; filename=" + "code2"+curDate+".xlsx"); 
		
		return "excelDownloadView";
	}
		
	/* 코드 3	*/	
	
	//코드3 맵핑
	@RequestMapping(value = {"/admin/academic/codeManagement/code3"}, method = RequestMethod.GET)
	public String code3View(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		param.put("code_cate", "dia_code_name");
		model.addAllAttributes(service.getCodeMenuName(param));
		return "admin/academic/code/code3";
	}

	//코드3 코드명으로 항목 가져오기
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/code3/search", method = RequestMethod.POST)
	public String code3Search (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(service.getCode3SearchList(param));
		return JSON_VIEW;
	}
	
	//코드3 가져오기
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/code3/list", method = RequestMethod.POST)
	public String code3List (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(service.getCode3List(param));
		return JSON_VIEW;
	}

	//코드3 삭제
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/code3/delete", method = RequestMethod.POST)
	public String code3Delete (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		model.addAllAttributes(service.deleteCode3(param));
		return JSON_VIEW;
	}
	
	//코드3 수정
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/code3/update", method = RequestMethod.POST)
	public String code3Update (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		model.addAllAttributes(service.updateCode3(param));
		return JSON_VIEW;
	}	

	//코드3 등록 맵핑
	@RequestMapping(value = {"/admin/academic/codeManagement/code3/reg"}, method = RequestMethod.GET)
	public String code3Reg(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		param.put("code_cate", "dia_code_name");
		model.addAllAttributes(service.getCodeMenuName(param));
		return "admin/academic/code/code3Reg";
	}
	
	//코드3 등록
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/code3/insert", method = RequestMethod.POST)
	public String code3Insert (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		model.addAllAttributes(service.insertCode3(param));
		return JSON_VIEW;
	}	
	
	
	//코드3 엑셀 다운로드
	@RequestMapping(value = "/admin/academic/codeManagement/code3/excelDown", method = RequestMethod.GET)
	public String code3ExcelDown(HttpServletRequest request,  Model model
			, @RequestParam HashMap<String, Object> param
			, HttpServletResponse response) throws Exception {
		
		List<HashMap<String, Object>> list = service.getCode3ExcelList();
		//엑셀 제목 크기
		int[] sheetWidth = {1500,7000,7000};

		//제목
		String[] sheetTitle = {"No", "항목명(국문)", "항목명(영문)"};

		String curDate= (new SimpleDateFormat("yyyy-MM-dd")).format( new Date() );
		
		model.addAttribute("excel_view_type", "code3");
		model.addAttribute("filename","code3_"+curDate);
		
		model.addAttribute("sheetName","sheet1");
		model.addAttribute("sheetTitle", sheetTitle);
		model.addAttribute("sheetWidth",sheetWidth);
		model.addAttribute("codeList", list);

		response.setHeader("Content-disposition", "attachment; filename=" + "code3"+curDate+".xlsx"); 
		
		return "excelDownloadView";
	}
	
	//코드 메뉴 맵핑
	@RequestMapping(value = {"/admin/academic/codeManagement/code/nameChange"}, method = RequestMethod.POST)
	public String codeNameChange(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		Util.requiredCheck(param, new String[] {"code_cate"});
		
		model.addAttribute("code_cate", param.get("code_cate").toString());
		model.addAllAttributes(service.getCodeMenuName(param));
		
		return "admin/academic/code/codeMenuName";
	}
	
	//코드 메뉴명 변경
	@RequestMapping(value = "/ajax/admin/academic/codeManagement/code/nameChange", method = RequestMethod.POST)
	public String codeMenuNameChange (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"code_cate", "name"});
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		model.addAllAttributes(service.updateCodeMenuName(param));
		return JSON_VIEW;
	}	
	
	/************************************************** 수강 신청 ************************************************************/
	
	
	//수강신청 리스트 맵핑
	@RequestMapping(value = {"/admin/academic/courseApplication/list"}, method = RequestMethod.GET)
	public String courseApplicationList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {

		model.addAllAttributes(service.getAcademicYearList());
		
		return "admin/academic/courseApplication/courseApplication";
	}	

	//수강신청 저장하기
	@RequestMapping(value = "/ajax/admin/academic/courseApplication/list", method = RequestMethod.POST)
	public String getCourseApplicationList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(service.getApplicationList(param));
		
		return JSON_VIEW;
	}	
	
	//수강신청 등록 맵핑
	@RequestMapping(value = {"/admin/academic/courseApplication/insert"}, method = RequestMethod.POST)
	public String courseApplicationInsert(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		param.put("level", 1);		
		model.addAttribute("lseqList", service.academicSystemSategeOfList(param));

		if(request.getParameter("ca_seq") != null) {
			model.addAllAttributes(service.getApplicationInfo(param));
		}
		return "admin/academic/courseApplication/courseApplicationReg";
	}
	

	//수강신청 등록 조회 맵핑
	@RequestMapping(value = {"/admin/academic/courseApplication/view"}, method = RequestMethod.POST)
	public String courseApplicationView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {		
		model.addAllAttributes(service.getApplicationViewInfo(param));
		model.addAttribute("ca_seq", param.get("ca_seq"));
		return "admin/academic/courseApplication/courseApplicationView";
	}
	
	//수강신청한 학생들 가져오기
	@RequestMapping(value = "/ajax/admin/academic/courseApplication/stlist", method = RequestMethod.POST)
	public String getApplicationStlist (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"ca_seq"});
		model.addAllAttributes(service.getApplicationStList(param));
		
		return JSON_VIEW;
	}
	
	//학사체계로 학사 리스트 가져오기
	@RequestMapping(value = "/ajax/admin/academic/courseApplication/acaList", method = RequestMethod.POST)
	public String getAcaList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"aca_system_seq"});
		model.addAllAttributes(service.getApplicationAcaList(param));
		return JSON_VIEW;
	}	
	
	//학사로 교육과정 리스트 가져오기
	@RequestMapping(value = "/ajax/admin/academic/courseApplication/currList", method = RequestMethod.POST)
	public String getCurrList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"aca_seq"});
		model.addAllAttributes(service.getApplicationCurrList(param));
		return JSON_VIEW;
	}	
	
	//교육과정 정보 가져오기
	@RequestMapping(value = "/ajax/admin/academic/courseApplication/currInfo", method = RequestMethod.POST)
	public String getCurrInfo (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"curr_seq"});
		
		model.addAllAttributes(service.getApplicationCurrInfo(param));
		
		return JSON_VIEW;
	}	
	
	//수강신청 저장하기
	@RequestMapping(value = "/ajax/admin/academic/courseApplication/insert", method = RequestMethod.POST)
	public String applicationInsert (MultipartHttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"curr_seq"});
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.applicationInsert(param, request));
		
		return JSON_VIEW;
	}	

	//환불규정 등록 맵핑
	@RequestMapping(value = {"/admin/academic/courseApplication/refund"}, method = RequestMethod.GET)
	public String courseApplicationRefund(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		String viewName = "admin/academic/courseApplication/refundView";
		
		if(service.getRefundCount() == 0)
			viewName = "admin/academic/courseApplication/refundReg";			
			
		return viewName;
	}
	
	//환불규정 등록 맵핑
	@RequestMapping(value = {"/admin/academic/courseApplication/refund/reg"}, method = RequestMethod.GET)
	public String courseApplicationRefundReg(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		String viewName = "admin/academic/courseApplication/refundReg";			
			
		return viewName;
	}
	
	//환불규정 가져오기
	@RequestMapping(value = "/ajax/admin/academic/courseApplication/refund/list", method = RequestMethod.POST)
	public String refundInfo (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		model.addAllAttributes(service.getRefundInfo());
		
		return JSON_VIEW;
	}	
	
	//환불규정 저장/수정
	@RequestMapping(value = "/ajax/admin/academic/courseApplication/refund/insert", method = RequestMethod.POST)
	public String refundInsert(MultipartHttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		model.addAllAttributes(service.insertRefund(request, param));
		
		return JSON_VIEW;
	}	
	
	//유의사항 등록 맵핑
	@RequestMapping(value = {"/admin/academic/courseApplication/notes"}, method = RequestMethod.GET)
	public String notes(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		String viewName = "admin/academic/courseApplication/notesView";
		
		if(service.getNotesCount() == 0)
			viewName = "admin/academic/courseApplication/notesReg";			
			
		return viewName;
	}
	
	//유의사항 등록 맵핑
	@RequestMapping(value = {"/admin/academic/courseApplication/notes/reg"}, method = RequestMethod.GET)
	public String notesReg(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model)  throws Exception {
		String viewName = "admin/academic/courseApplication/notesReg";			
			
		return viewName;
	}
	
	//유의사항 가져오기
	@RequestMapping(value = "/ajax/admin/academic/courseApplication/notes/list", method = RequestMethod.POST)
	public String notesInfo (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		model.addAllAttributes(service.getNotesInfo());
		
		return JSON_VIEW;
	}	
	
	//유의사항 저장/수정
	@RequestMapping(value = "/ajax/admin/academic/courseApplication/notes/insert", method = RequestMethod.POST)
	public String notesInsert(MultipartHttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		model.addAllAttributes(service.insertNotes(request, param));
		
		return JSON_VIEW;
	}	

	//입금확인 상태 변경
	@RequestMapping(value = "/ajax/admin/academic/courseApplication/updateDeposit", method = RequestMethod.POST)
	public String updateDeposit(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"cas_seq"});
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.updateDeposit(param));
		
		return JSON_VIEW;
	}	
	
	//승인 상태 변경
	@RequestMapping(value = "/ajax/admin/academic/courseApplication/updateCasState", method = RequestMethod.POST)
	public String updateCasState(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"cas_seq"});
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.updateCasState(param));
		
		return JSON_VIEW;
	}	
	
	//승인 취소 상태 변경
	@RequestMapping(value = "/ajax/admin/academic/courseApplication/updateCasCancelState", method = RequestMethod.POST)
	public String updateCasCancelState(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"cas_seq"});
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.updateCasCancelState(param));
		
		return JSON_VIEW;
	}	
	
}
