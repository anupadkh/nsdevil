package com.nsdevil.medlms.web.admin.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.exception.LogicException;
import com.nsdevil.medlms.common.exception.ResourceNotFoundException;
import com.nsdevil.medlms.common.exception.RuntimeLogicException;
import com.nsdevil.medlms.common.util.FileUploader;
import com.nsdevil.medlms.common.util.PagingHelper;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.admin.dao.BoardDao;
import com.nsdevil.medlms.web.common.dao.CommonDao;

@Service
public class BoardService {
	
	@Value("#{config['config.rootPath']}")
	private String ROOT_PATH;
	
	@Value("#{config['config.resPath']}")
	private String RES_PATH;
	
	@Value("#{config['config.boardPath']}")
	private String BOARD_PATH;
	
	@Autowired
	private BoardDao dao;
	

	@Autowired
	private CommonDao commonDao;
	
	@Transactional
	public ResultMap removeBoard(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		String boardSeqsStr = (String) param.get("remove_board_seqs");
		if (boardSeqsStr != null && !boardSeqsStr.equals("")) {
			String[] boardSeqsArr = boardSeqsStr.split(",");
			for (String boardSeq : boardSeqsArr) {
				HashMap<String, Object> paramBoard = new HashMap<String, Object>();
				paramBoard.put("board_seq", boardSeq);
				paramBoard.put("s_user_seq", param.get("s_user_seq"));
				dao.deleteBoardAttach(paramBoard);
				dao.deleteMpBoardShowTarget(paramBoard);
				if (dao.deleteBoard(paramBoard) < 1) {
					throw new RuntimeLogicException("QUERY_FAIL [ board delete query fail ]", "004");
				}
			}
		}
		return resultMap;
	}
	
	@SuppressWarnings("unchecked")
	public ResultMap getNoticeList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		int totalCnt = dao.getNoticeContentsCount(param);
		PagingHelper pagingHelper = new PagingHelper();
		List<HashMap<String, Object>> contentsList = dao.getNoticeContentsList(pagingHelper.getPagingParam(param, totalCnt));
		for (HashMap<String, Object> map : contentsList) {
			List<HashMap<String, Object>> targetList = new ArrayList<HashMap<String, Object>>();
			//공지 대상이 학생(02)이면서 학생 전체가 아닌 학년 별일때
			if (map.get("aca_seq_str") != null) {
				String acaSeqStr = String.valueOf(map.get("aca_seq_str"));
				if (!acaSeqStr.equals("")) {
					String[] acaSeqArr = acaSeqStr.split("\\|\\|");
					
					if (acaSeqArr.length > 1) { //정렬
						Arrays.sort(acaSeqArr);
					}
					
					for (String acaSeq : acaSeqArr) {
						HashMap<String, Object> tmp = new HashMap<String, Object>();
						tmp = dao.getAcademicSystemInfo(acaSeq);
						if (tmp != null && !tmp.isEmpty()) {
							String targetName = "";
							String lAcaName = Util.nvl(String.valueOf(tmp.get("l_aca_name")));
							String mAcaName = Util.nvl(String.valueOf(tmp.get("m_aca_name")));
							String currAcaName = Util.nvl(String.valueOf(tmp.get("curr_aca_name")));
							int level = (int) tmp.get("level"); //1:대, 2:중, 3:소, 4:기간
							if (level == 1) {
								targetName = currAcaName;
							} else if (level == 2) {
								targetName = lAcaName + " " +currAcaName;
							} else if (level == 3) {
								targetName = mAcaName + " " + currAcaName;
							}
							
							if (!targetName.equals("")) {
								tmp.put("target_name", targetName);
								targetList.add((HashMap<String, Object>)tmp.clone());
							}
						}
					}
				}
			}
			map.put("target_list", targetList);
		}
		
		resultMap.putAll(pagingHelper.getPageList(contentsList, (String)param.get("listFunctionName"), totalCnt));
		return resultMap;
	}
	
	
	@SuppressWarnings("unchecked")
	public ResultMap getNoticeDetail(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		HashMap<String, Object> noticeDetailInfo = new HashMap<String, Object>();
		noticeDetailInfo = dao.getNoticeDetailInfo(param);
		if (noticeDetailInfo == null || noticeDetailInfo.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_NOTICE_INFO", "007");
		}
		
		List<HashMap<String, Object>> targetList = new ArrayList<HashMap<String, Object>>();
		//공지 대상이 학생(03)이면서 학생 전체가 아닌 학년 별일때
		if (noticeDetailInfo.get("aca_seq_str") != null) {
			String acaSeqStr = String.valueOf(noticeDetailInfo.get("aca_seq_str"));
			if (!acaSeqStr.equals("")) {
				String[] acaSeqArr = acaSeqStr.split("\\|\\|");
				
				if (acaSeqArr.length > 1) { //정렬
					Arrays.sort(acaSeqArr);
				}
				
				for (String acaSeq : acaSeqArr) {
					HashMap<String, Object> tmp = new HashMap<String, Object>();
					tmp = dao.getAcademicSystemInfo(acaSeq);
					if (tmp != null && !tmp.isEmpty()) {
						String targetName = "";
						String lAcaName = Util.nvl(String.valueOf(tmp.get("l_aca_name")));
						String mAcaName = Util.nvl(String.valueOf(tmp.get("m_aca_name")));
						String currAcaName = Util.nvl(String.valueOf(tmp.get("curr_aca_name")));
						int level = (int) tmp.get("level"); //1:대, 2:중, 3:소, 4:기간
						if (level == 1) {
							targetName = currAcaName;
						} else if (level == 2) {
							targetName = lAcaName + " " +currAcaName;
						} else if (level == 3) {
							targetName = mAcaName + " " + currAcaName;
						}
						
						if (!targetName.equals("")) {
							tmp.put("target_name", targetName);
							targetList.add((HashMap<String, Object>)tmp.clone());
						}
					}
				}
			}
		}
		
		List<HashMap<String, Object>> attachList = new ArrayList<HashMap<String, Object>>();
		attachList = dao.getNoticeAttachFileList(param);
		for (HashMap<String, Object> map : attachList) {
			map.put("file_path", RES_PATH + map.get("file_path"));
		}
		
		HashMap<String, Object> nextNoticeInfo = new HashMap<String, Object>();
		nextNoticeInfo = dao.getNextNoticeInfo(param);
		HashMap<String, Object> prevNoticeInfo = new HashMap<String, Object>();
		prevNoticeInfo = dao.getPrevNoticeInfo(param);
		
		resultMap.put("notice_info", noticeDetailInfo);
		resultMap.put("attach_list", attachList);
		resultMap.put("next_notice_info", nextNoticeInfo);
		resultMap.put("prev_notice_info", prevNoticeInfo);
		resultMap.put("target_list", targetList);
		return resultMap;
	}
	
	
	public ResultMap getAcademicList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		HashMap<String, Object> acaParam = new HashMap<String, Object>();
		List<HashMap<String, Object>> lAcaList = new ArrayList<HashMap<String, Object>>();
		lAcaList = dao.getAcademicSystemList(acaParam);
		
		List<HashMap<String, Object>> mAcaList = new ArrayList<HashMap<String, Object>>();
		String lSeq = String.valueOf(param.get("l_seq"));
		if (!lSeq.equals("")) {
			acaParam.put("l_seq", param.get("l_seq"));
			mAcaList = dao.getAcademicSystemList(acaParam);
		}
		
		List<HashMap<String, Object>> sAcaList = new ArrayList<HashMap<String, Object>>();
		String mSeq = String.valueOf(param.get("m_seq"));
		if (!mSeq.equals("")) {
			acaParam.put("m_seq", param.get("m_seq"));
			sAcaList = dao.getAcademicSystemList(acaParam);
		}
		
		resultMap.put("l_aca_list", lAcaList);
		resultMap.put("m_aca_list", mAcaList);
		resultMap.put("s_aca_list", sAcaList);
		return resultMap;
	}
	
	@Transactional
	public ResultMap noticeCreate(HashMap<String, Object> param, HttpServletRequest request) throws Exception {
		ResultMap resultMap = new ResultMap();
		String boardCateCode = String.valueOf(param.get("boardCateCode"));
		if (boardCateCode.equals("")) {
			boardCateCode = null;
		}
		String showTarget = String.valueOf(param.get("showTarget"));
		
		HashMap<String, Object> noticeParam = new HashMap<String, Object>();
		noticeParam.put("board_code", param.get("board_code"));
		noticeParam.put("board_cate_code", boardCateCode);
		noticeParam.put("show_target", showTarget);
		noticeParam.put("title", param.get("title"));
		noticeParam.put("content", param.get("content"));
		noticeParam.put("s_user_seq", param.get("s_user_seq"));
		
		String periodDateValue = String.valueOf(param.get("periodDateValue"));
		if (periodDateValue.equals("Y")) {
			String startDate = String.valueOf(param.get("startDate"));
			String endDate = String.valueOf(param.get("endDate"));
			noticeParam.put("start_date", startDate + " 00:00:00");
			noticeParam.put("end_date", endDate + " 23:59:59");
		}
		
		if (dao.insertBoard(noticeParam) < 1) {
			throw new RuntimeLogicException("QUERY_FAIL [notice board insert query fail ]", "004");
		}
		String boardSeq = String.valueOf(noticeParam.get("board_seq"));
		if (showTarget.equals("03")) {
			JSONParser jsonParser = new JSONParser();
			JSONArray jsonArray = (JSONArray) jsonParser.parse(String.valueOf(param.get("academicJsonArrayStr")));
			List<HashMap<String, Object>> boardShowTargetList = Util.fromJson(jsonArray);
			for (HashMap<String, Object> map : boardShowTargetList) {
				map.put("board_seq", boardSeq);
				map.put("s_user_seq", param.get("s_user_seq"));
				map.put("l_seq", ((String) map.get("l_seq")).equals("")?null:Integer.parseInt((String) map.get("l_seq")));
				map.put("m_seq", ((String) map.get("m_seq")).equals("")?null:Integer.parseInt((String) map.get("m_seq")));
				map.put("s_seq", ((String) map.get("s_seq")).equals("")?null:Integer.parseInt((String) map.get("s_seq")));
				map.put("aca_system_seq", Integer.parseInt((String) map.get("curr_seq")));
				if (dao.insertMpBoardShowTarget(map) < 1) {
					throw new RuntimeLogicException("QUERY_FAIL [bp_board_show_target insert query fail ]", "004");
				}
			}
		}
		
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		List<MultipartFile> attachFileList = mRequest.getFiles("uploadFile");
		String uploadPath = ROOT_PATH + RES_PATH + BOARD_PATH + "/" + boardSeq;
		String filePath = BOARD_PATH + "/" + boardSeq;
		for (MultipartFile attachFile : attachFileList) {
			HashMap<String, Object> boardAttachParam = new HashMap<String, Object>();
			String realName = FileUploader.uploadFile(uploadPath, attachFile);
			if (realName == null || realName.equals("not")) {
				Util.deleteFolder(new File(uploadPath));
				throw new LogicException("FILE_UPLOAD_FAIL", "003");
			}
			boardAttachParam.put("board_seq", boardSeq);
			boardAttachParam.put("file_path", filePath+"/"+realName);
			boardAttachParam.put("file_name", attachFile.getOriginalFilename());
			boardAttachParam.put("s_user_seq", param.get("s_user_seq"));
			if (dao.insertBoardAttach(boardAttachParam) < 1) {
				Util.deleteFolder(new File(uploadPath));
				throw new RuntimeLogicException("QUERY_FAIL [ board_attach insert fail ]", "004");
			}
		}
		return resultMap;
	}
	
	@Transactional
	public ResultMap noticeModify(HashMap<String, Object> param, HttpServletRequest request) throws Exception {
		ResultMap resultMap = new ResultMap();
		String boardCateCode = String.valueOf(param.get("boardCateCode"));
		if (boardCateCode.equals("")) {
			boardCateCode = null;
		}
		String showTarget = String.valueOf(param.get("showTarget"));
		
		HashMap<String, Object> noticeParam = new HashMap<String, Object>();
		noticeParam.put("board_seq", param.get("board_seq"));
		noticeParam.put("board_code", param.get("board_code"));
		noticeParam.put("board_cate_code", boardCateCode);
		noticeParam.put("show_target", showTarget);
		noticeParam.put("title", param.get("title"));
		noticeParam.put("content", param.get("content"));
		noticeParam.put("s_user_seq", param.get("s_user_seq"));
		
		String periodDateValue = String.valueOf(param.get("periodDateValue"));
		if (periodDateValue.equals("Y")) {
			String startDate = String.valueOf(param.get("startDate"));
			String endDate = String.valueOf(param.get("endDate"));
			noticeParam.put("start_date", startDate + " 00:00:00");
			noticeParam.put("end_date", endDate + " 23:59:59");
		} else {
			noticeParam.put("start_date", null);
			noticeParam.put("end_date", null);
		}
		
		if (dao.updateBoard(noticeParam) < 1) {
			throw new RuntimeLogicException("QUERY_FAIL [notice board update query fail ]", "004");
		}
		
		String boardSeq = String.valueOf(noticeParam.get("board_seq"));
		if (showTarget.equals("03")) {
			dao.deleteMpBoardShowTarget(noticeParam);
			JSONParser jsonParser = new JSONParser();
			JSONArray jsonArray = (JSONArray) jsonParser.parse(String.valueOf(param.get("academicJsonArrayStr")));
			List<HashMap<String, Object>> boardShowTargetList = Util.fromJson(jsonArray);
			for (HashMap<String, Object> map : boardShowTargetList) {
				map.put("board_seq", boardSeq);
				map.put("s_user_seq", param.get("s_user_seq"));
				map.put("l_seq", ((String) map.get("l_seq")).equals("")?null:Integer.parseInt((String) map.get("l_seq")));
				map.put("m_seq", ((String) map.get("m_seq")).equals("")?null:Integer.parseInt((String) map.get("m_seq")));
				map.put("s_seq", ((String) map.get("s_seq")).equals("")?null:Integer.parseInt((String) map.get("s_seq")));
				map.put("aca_system_seq", Integer.parseInt((String) map.get("curr_seq")));
				if (dao.insertMpBoardShowTarget(map) < 1) {
					throw new RuntimeLogicException("QUERY_FAIL [bp_board_show_target insert query fail ]", "004");
				}
			}
		}
		
		String removeAttachSeqStr = (String)param.get("removeAttachSeqStr");
		if (removeAttachSeqStr != null && !removeAttachSeqStr.equals("")) {
			String [] removeAttachSeqArr = removeAttachSeqStr.split(",");
			for(String boardAttachSeq : removeAttachSeqArr) {
				if (dao.deleteBoardAttachForBoardAttachSeq(boardAttachSeq) < 1) {
					throw new RuntimeLogicException("QUERY_FAIL [board_attach delete query fail ]", "004");
				}
			}
		}
		
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		List<MultipartFile> attachFileList = mRequest.getFiles("uploadFile");
		String uploadPath = ROOT_PATH + RES_PATH + BOARD_PATH + "/" + boardSeq;
		String filePath = BOARD_PATH + "/" + boardSeq;
		for (MultipartFile attachFile : attachFileList) {
			HashMap<String, Object> boardAttachParam = new HashMap<String, Object>();
			String realName = FileUploader.uploadFile(uploadPath, attachFile);
			if (realName == null || realName.equals("not")) {
				throw new LogicException("FILE_UPLOAD_FAIL", "003");
			}
			boardAttachParam.put("board_seq", boardSeq);
			boardAttachParam.put("file_path", filePath+"/"+realName);
			boardAttachParam.put("file_name", attachFile.getOriginalFilename());
			boardAttachParam.put("s_user_seq", param.get("s_user_seq"));
			if (dao.insertBoardAttach(boardAttachParam) < 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ board_attach insert fail ]", "004");
			}
		}
		return resultMap;
	}
	
	public ResultMap getOSCEList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		int totalCnt = dao.getOSCEContentsCount(param);
		PagingHelper pagingHelper = new PagingHelper(6 ,10);
		List<HashMap<String, Object>> ContentsList = dao.getOSCEContentsList(pagingHelper.getPagingParam(param, totalCnt));
		resultMap.putAll(pagingHelper.getPageList(ContentsList, (String)param.get("listFunctionName"), totalCnt));
		return resultMap;
	}
	
	public ResultMap getOSCEDetail(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		HashMap<String, Object> boardDetailInfo = new HashMap<String, Object>();
		boardDetailInfo = dao.getOSCEDetailInfo(param);
		if (boardDetailInfo == null || boardDetailInfo.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_NOTICE_INFO", "007");
		}
		List<HashMap<String, Object>> attachList = new ArrayList<HashMap<String, Object>>();
		attachList = commonDao.getBoardAttachFileList(param);
		HashMap<String, Object> nextBoardInfo = new HashMap<String, Object>();
		nextBoardInfo = commonDao.getNextBoardInfo(param);
		HashMap<String, Object> prevBoardInfo = new HashMap<String, Object>();
		prevBoardInfo = commonDao.getPrevBoardInfo(param);
		resultMap.put("board_info", boardDetailInfo);
		resultMap.put("attach_list", attachList);
		resultMap.put("next_board_info", nextBoardInfo);
		resultMap.put("prev_board_info", prevBoardInfo);
		return resultMap;
	}
	
	public ResultMap getCPXList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		int totalCnt = dao.getCPXContentsCount(param);
		PagingHelper pagingHelper = new PagingHelper(6 ,10);
		List<HashMap<String, Object>> ContentsList = dao.getCPXContentsList(pagingHelper.getPagingParam(param, totalCnt));
		resultMap.putAll(pagingHelper.getPageList(ContentsList, (String)param.get("listFunctionName"), totalCnt));
		return resultMap;
	}
	
	public ResultMap getCPXDetail(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		HashMap<String, Object> boardDetailInfo = new HashMap<String, Object>();
		boardDetailInfo = dao.getCPXDetailInfo(param);
		if (boardDetailInfo == null || boardDetailInfo.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_NOTICE_INFO", "007");
		}
		
		List<HashMap<String, Object>> attachList = new ArrayList<HashMap<String, Object>>();
		attachList = commonDao.getBoardAttachFileList(param);
		HashMap<String, Object> nextBoardInfo = new HashMap<String, Object>();
		nextBoardInfo = commonDao.getNextBoardInfo(param);
		HashMap<String, Object> prevBoardInfo = new HashMap<String, Object>();
		prevBoardInfo = commonDao.getPrevBoardInfo(param);
		resultMap.put("board_info", boardDetailInfo);
		resultMap.put("attach_list", attachList);
		resultMap.put("next_board_info", nextBoardInfo);
		resultMap.put("prev_board_info", prevBoardInfo);
		return resultMap;
	}
	
	
	@SuppressWarnings("unchecked")
	public ResultMap getPBLList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		int totalCnt = dao.getPBLContentsCount(param);
		PagingHelper pagingHelper = new PagingHelper();
		List<HashMap<String, Object>> contentsList = dao.getPBLContentsList(pagingHelper.getPagingParam(param, totalCnt));
		for (HashMap<String, Object> map : contentsList) {
			List<HashMap<String, Object>> targetList = new ArrayList<HashMap<String, Object>>();
			//공지 대상이 학생(02)이면서 학생 전체가 아닌 학년 별일때
			if (map.get("aca_seq_str") != null) {
				String acaSeqStr = String.valueOf(map.get("aca_seq_str"));
				if (!acaSeqStr.equals("")) {
					String[] acaSeqArr = acaSeqStr.split("\\|\\|");
					
					if (acaSeqArr.length > 1) { //정렬
						Arrays.sort(acaSeqArr);
					}
					
					for (String acaSeq : acaSeqArr) {
						HashMap<String, Object> tmp = new HashMap<String, Object>();
						tmp = dao.getAcademicSystemInfo(acaSeq);
						if (tmp != null && !tmp.isEmpty()) {
							String targetName = "";
							String lAcaName = Util.nvl(String.valueOf(tmp.get("l_aca_name")));
							String mAcaName = Util.nvl(String.valueOf(tmp.get("m_aca_name")));
							String currAcaName = Util.nvl(String.valueOf(tmp.get("curr_aca_name")));
							int level = (int) tmp.get("level"); //1:대, 2:중, 3:소, 4:기간
							if (level == 1) {
								targetName = currAcaName;
							} else if (level == 2) {
								targetName = lAcaName + " " +currAcaName;
							} else if (level == 3) {
								targetName = mAcaName + " " + currAcaName;
							}
							
							if (!targetName.equals("")) {
								tmp.put("target_name", targetName);
								targetList.add((HashMap<String, Object>)tmp.clone());
							}
						}
					}
				}
			}
			map.put("target_list", targetList);
		}
		
		resultMap.putAll(pagingHelper.getPageList(contentsList, (String)param.get("listFunctionName"), totalCnt));
		return resultMap;
	}
	
	
	@Transactional
	public ResultMap pblCreate(HashMap<String, Object> param, HttpServletRequest request) throws Exception {
		ResultMap resultMap = new ResultMap();
		String boardCateCode = String.valueOf(param.get("boardCateCode"));
		if (boardCateCode.equals("")) {
			boardCateCode = null;
		}
		String showTarget = String.valueOf(param.get("showTarget"));
		
		HashMap<String, Object> pblParam = new HashMap<String, Object>();
		pblParam.put("board_code", param.get("board_code"));
		pblParam.put("board_cate_code", boardCateCode);
		pblParam.put("show_target", showTarget);
		pblParam.put("title", param.get("title"));
		pblParam.put("content", param.get("content"));
		pblParam.put("s_user_seq", param.get("s_user_seq"));
		pblParam.put("start_date", param.get("start_date"));
		pblParam.put("end_date", param.get("end_date"));
		
		if (dao.insertBoard(pblParam) < 1) {
			throw new RuntimeLogicException("QUERY_FAIL [board insert query fail ]", "004");
		}
		String boardSeq = String.valueOf(pblParam.get("board_seq"));
		if (showTarget.equals("03")) {
			JSONParser jsonParser = new JSONParser();
			JSONArray jsonArray = (JSONArray) jsonParser.parse(String.valueOf(param.get("academicJsonArrayStr")));
			List<HashMap<String, Object>> boardShowTargetList = Util.fromJson(jsonArray);
			for (HashMap<String, Object> map : boardShowTargetList) {
				map.put("board_seq", boardSeq);
				map.put("s_user_seq", param.get("s_user_seq"));
				map.put("l_seq", ((String) map.get("l_seq")).equals("")?null:Integer.parseInt((String) map.get("l_seq")));
				map.put("m_seq", ((String) map.get("m_seq")).equals("")?null:Integer.parseInt((String) map.get("m_seq")));
				map.put("s_seq", ((String) map.get("s_seq")).equals("")?null:Integer.parseInt((String) map.get("s_seq")));
				map.put("aca_system_seq", Integer.parseInt((String) map.get("curr_seq")));
				if (dao.insertMpBoardShowTarget(map) < 1) {
					throw new RuntimeLogicException("QUERY_FAIL [bp_board_show_target insert query fail ]", "004");
				}
			}
		}
		
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		List<MultipartFile> attachFileList = mRequest.getFiles("uploadFile");
		String uploadPath = ROOT_PATH + RES_PATH + BOARD_PATH + "/" + boardSeq;
		String filePath = BOARD_PATH + "/" + boardSeq;
		for (MultipartFile attachFile : attachFileList) {
			HashMap<String, Object> boardAttachParam = new HashMap<String, Object>();
			String realName = FileUploader.uploadFile(uploadPath, attachFile);
			if (realName == null || realName.equals("not")) {
				Util.deleteFolder(new File(uploadPath));
				throw new LogicException("FILE_UPLOAD_FAIL", "003");
			}
			boardAttachParam.put("board_seq", boardSeq);
			boardAttachParam.put("file_path", filePath+"/"+realName);
			boardAttachParam.put("file_name", attachFile.getOriginalFilename());
			boardAttachParam.put("s_user_seq", param.get("s_user_seq"));
			if (dao.insertBoardAttach(boardAttachParam) < 1) {
				Util.deleteFolder(new File(uploadPath));
				throw new RuntimeLogicException("QUERY_FAIL [ board_attach insert fail ]", "004");
			}
		}
		return resultMap;
	}
	
	
	@SuppressWarnings("unchecked")
	public ResultMap getPBLDetail(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		HashMap<String, Object> pblDetailInfo = new HashMap<String, Object>();
		pblDetailInfo = dao.getPBLDetailInfo(param);
		if (pblDetailInfo == null || pblDetailInfo.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_NOTICE_INFO", "007");
		}
		
		List<HashMap<String, Object>> targetList = new ArrayList<HashMap<String, Object>>();
		//공지 대상이 학생(03)이면서 학생 전체가 아닌 학년 별일때
		if (pblDetailInfo.get("aca_seq_str") != null) {
			String acaSeqStr = String.valueOf(pblDetailInfo.get("aca_seq_str"));
			if (!acaSeqStr.equals("")) {
				String[] acaSeqArr = acaSeqStr.split("\\|\\|");
				
				if (acaSeqArr.length > 1) { //정렬
					Arrays.sort(acaSeqArr);
				}
				
				for (String acaSeq : acaSeqArr) {
					HashMap<String, Object> tmp = new HashMap<String, Object>();
					tmp = dao.getAcademicSystemInfo(acaSeq);
					if (tmp != null && !tmp.isEmpty()) {
						String targetName = "";
						String lAcaName = Util.nvl(String.valueOf(tmp.get("l_aca_name")));
						String mAcaName = Util.nvl(String.valueOf(tmp.get("m_aca_name")));
						String currAcaName = Util.nvl(String.valueOf(tmp.get("curr_aca_name")));
						int level = (int) tmp.get("level"); //1:대, 2:중, 3:소, 4:기간
						if (level == 1) {
							targetName = currAcaName;
						} else if (level == 2) {
							targetName = lAcaName + " " +currAcaName;
						} else if (level == 3) {
							targetName = mAcaName + " " + currAcaName;
						}
						
						if (!targetName.equals("")) {
							tmp.put("target_name", targetName);
							targetList.add((HashMap<String, Object>)tmp.clone());
						}
					}
				}
			}
		}
		
		List<HashMap<String, Object>> attachList = new ArrayList<HashMap<String, Object>>();
		attachList = dao.getNoticeAttachFileList(param);
		for (HashMap<String, Object> map : attachList) {
			map.put("file_path", RES_PATH + map.get("file_path"));
		}
		
		HashMap<String, Object> nextPBLInfo = new HashMap<String, Object>();
		nextPBLInfo = dao.getNextPBLInfo(param);
		HashMap<String, Object> prevPBLInfo = new HashMap<String, Object>();
		prevPBLInfo = dao.getPrevPBLInfo(param);
		
		resultMap.put("pbl_info", pblDetailInfo);
		resultMap.put("attach_list", attachList);
		resultMap.put("next_pbl_info", nextPBLInfo);
		resultMap.put("prev_pbl_info", prevPBLInfo);
		resultMap.put("target_list", targetList);
		return resultMap;
	}
	
	@Transactional
	public ResultMap pblModify(HashMap<String, Object> param, HttpServletRequest request) throws Exception {
		ResultMap resultMap = new ResultMap();
		String boardCateCode = String.valueOf(param.get("boardCateCode"));
		if (boardCateCode.equals("")) {
			boardCateCode = null;
		}
		String showTarget = String.valueOf(param.get("showTarget"));
		
		HashMap<String, Object> pblParam = new HashMap<String, Object>();
		pblParam.put("board_seq", param.get("board_seq"));
		pblParam.put("board_code", param.get("board_code"));
		pblParam.put("board_cate_code", boardCateCode);
		pblParam.put("show_target", showTarget);
		pblParam.put("title", param.get("title"));
		pblParam.put("content", param.get("content"));
		pblParam.put("start_date", param.get("start_date"));
		pblParam.put("end_date", param.get("end_date"));
		pblParam.put("s_user_seq", param.get("s_user_seq"));
		
		if (dao.updateBoard(pblParam) < 1) {
			throw new RuntimeLogicException("QUERY_FAIL [pbl board update query fail ]", "004");
		}
		
		String boardSeq = String.valueOf(pblParam.get("board_seq"));
		if (showTarget.equals("03")) {
			dao.deleteMpBoardShowTarget(pblParam);
			JSONParser jsonParser = new JSONParser();
			JSONArray jsonArray = (JSONArray) jsonParser.parse(String.valueOf(param.get("academicJsonArrayStr")));
			List<HashMap<String, Object>> boardShowTargetList = Util.fromJson(jsonArray);
			for (HashMap<String, Object> map : boardShowTargetList) {
				map.put("board_seq", boardSeq);
				map.put("s_user_seq", param.get("s_user_seq"));
				map.put("l_seq", ((String) map.get("l_seq")).equals("")?null:Integer.parseInt((String) map.get("l_seq")));
				map.put("m_seq", ((String) map.get("m_seq")).equals("")?null:Integer.parseInt((String) map.get("m_seq")));
				map.put("s_seq", ((String) map.get("s_seq")).equals("")?null:Integer.parseInt((String) map.get("s_seq")));
				map.put("aca_system_seq", Integer.parseInt((String) map.get("curr_seq")));
				if (dao.insertMpBoardShowTarget(map) < 1) {
					throw new RuntimeLogicException("QUERY_FAIL [bp_board_show_target insert query fail ]", "004");
				}
			}
		}
		
		String removeAttachSeqStr = (String)param.get("removeAttachSeqStr");
		if (removeAttachSeqStr != null && !removeAttachSeqStr.equals("")) {
			String [] removeAttachSeqArr = removeAttachSeqStr.split(",");
			for(String boardAttachSeq : removeAttachSeqArr) {
				if (dao.deleteBoardAttachForBoardAttachSeq(boardAttachSeq) < 1) {
					throw new RuntimeLogicException("QUERY_FAIL [board_attach delete query fail ]", "004");
				}
			}
		}
		
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		List<MultipartFile> attachFileList = mRequest.getFiles("uploadFile");
		String uploadPath = ROOT_PATH + RES_PATH + BOARD_PATH + "/" + boardSeq;
		String filePath = BOARD_PATH + "/" + boardSeq;
		for (MultipartFile attachFile : attachFileList) {
			HashMap<String, Object> boardAttachParam = new HashMap<String, Object>();
			String realName = FileUploader.uploadFile(uploadPath, attachFile);
			if (realName == null || realName.equals("not")) {
				throw new LogicException("FILE_UPLOAD_FAIL", "003");
			}
			boardAttachParam.put("board_seq", boardSeq);
			boardAttachParam.put("file_path", filePath+"/"+realName);
			boardAttachParam.put("file_name", attachFile.getOriginalFilename());
			boardAttachParam.put("s_user_seq", param.get("s_user_seq"));
			if (dao.insertBoardAttach(boardAttachParam) < 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ board_attach insert fail ]", "004");
			}
		}
		return resultMap;
	}
	
	public ResultMap getBoardList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		List<HashMap<String, Object>> boardList = new ArrayList<HashMap<String, Object>>();
		boardList = dao.getBoardList(param);
		resultMap.put("list", boardList);
		return resultMap;
	}
	
	
	public ResultMap boardShowModify(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		if (dao.updateBoardShowFlag(param) < 1) {
			throw new RuntimeLogicException("QUERY_FAIL [ board_management update fail ]", "004");
		}
		return resultMap;
	}
	
}
