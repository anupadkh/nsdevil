package com.nsdevil.medlms.web.common.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.common.service.CommonService;
import com.nsdevil.medlms.web.common.service.ScheduleService;

@Controller
public class ScheduleController extends ExceptionController {
	
	@Autowired
	private ScheduleService scheduleService;
	
	@Autowired
	private CommonService commonService;
	
	//달력보기
	@RequestMapping(value = "/aca/MYscheduleM", method = RequestMethod.GET)
	public String MYscheduleM(HttpServletRequest request, Model model) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		model.addAllAttributes(commonService.getAssignCurrList(param));
		return "common/schedule/MYscheduleM";
	}
	
	//엑셀보기
	@RequestMapping(value = "/aca/MYschedule", method = RequestMethod.POST)
	public String MYschedule(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAttribute("curr_seq",param.get("curr_seq").toString());
		
		return "common/schedule/MYschedule";
	}
	
	//검색보기
	@RequestMapping(value = "/aca/MYscheduleSearch", method = RequestMethod.POST)
	public String MYscheduleSearch(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAttribute("curr_seq",param.get("curr_seq").toString());
		return "common/schedule/MYscheduleSearch";
	}
	
	//개인일정
	@RequestMapping(value = "/aca/MYscheduleMemo", method = RequestMethod.GET)
	public String MYscheduleMemo(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		return "common/schedule/MYscheduleMemo";
	}
	
	@RequestMapping(value = "/ajax/pf/lesson/getMYSchedule", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String getMYSchedule(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		model.addAllAttributes(scheduleService.getMYSchedule(param));
		return JSON_VIEW;
	}

	@RequestMapping(value = "/ajax/pf/lesson/getMYScheduleMemo", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String getMYScheduleMemo(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(scheduleService.getMYScheduleMemo(param));
		return JSON_VIEW;
	}
	
	@RequestMapping(value = "/ajax/pf/lesson/addMemo", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String addMemo(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		Util.requiredCheck(param, new String[]{"public_flag", "memo_name", "memo_date","memo_edate","all_day"});
		HashMap<String, Object> map = scheduleService.addMemo(param);
		model.addAllAttributes(map);
		return JSON_VIEW;
	}
	
	@RequestMapping(value = "/ajax/pf/lesson/deleteMemo", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String deleteMemo(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		Util.requiredCheck(param, new String[]{"memo_seq"});
		HashMap<String, Object> map = scheduleService.deleteMemo(param);
		model.addAllAttributes(map);
		return JSON_VIEW;
	}
	
	@RequestMapping(value = "/ajax/pf/lesson/getMemo", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String getMemo(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		Util.requiredCheck(param, new String[]{"memo_seq"});
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		HashMap<String, Object> map = scheduleService.getMemo(param);
		model.addAllAttributes(map);
		return JSON_VIEW;
	}	

	@RequestMapping(value = "/ajax/st/lesson/getMYSchedule", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String getMYScheduleST(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(scheduleService.getMYScheduleST(param));
		return JSON_VIEW;
	}
	
	//달력보기
	@RequestMapping(value = "/aca/MYscheduleMST", method = RequestMethod.GET)
	public String MYscheduleMST(HttpServletRequest request, Model model) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		model.addAllAttributes(commonService.getSTAssignCurrList(param));
		return "common/schedule/MYscheduleM_ST";
	}
	
}
