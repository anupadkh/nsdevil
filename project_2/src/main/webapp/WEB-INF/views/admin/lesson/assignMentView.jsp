<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
	<html>
	<head>
		<style>
			.tabcontent5 .mlms_tb1_1 th .dw{display:inline-block;width: 25px;height: 25px;font-size: 0px;color: rgba(255, 255, 255, 1);padding: 0;margin: 3px 3px;text-align: center;text-indent: 0px;background: url('${IMG}/ic02_ex_d1.png') no-repeat 50% 49% rgba(117, 161, 216, 1);border-radius: 3px;box-shadow: 1px 1px 1px rgba(239, 235, 255, 1);}
		</style> 
		<script type="text/javascript">
		   			
			$(document).ready(function() {	 
			      
				tabChange("tab2");
                //getAssignSubmitList();
                
                var mo2 = document.getElementById('m_pop2');
                var button = document.getElementsByClassName('close2')[0];
                var b = document.getElementsByClassName('open2');
                var i;
                    for (i = 0; i < b.length; i++){
                    
                b[i].onclick = function() {
                	if($("input[name=chk_box]:checked").length != 0){
                		$("textarea[name=popupFeedBack]").val("");
	                	getSelectStudent();
	                    mo2.style.display = "block";
                	}else{
                		alert("학생을 선택해주세요");
                	}
                }

                button.onclick = function() {
                    mo2.style.display = "none";
                }

                window.onclick = function(event) {
                    if (event.target == mo2) {
                        mo2.style.display = "none";
                    }
                }
                }
                //수업계획서 타이틀 가져오기
                getLessonTitleInfo();
			});	
				        
			function getLessonTitleInfo(){
	            
	            $.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/pf/lesson/lessonPlanBasicInfo",
	                data: {                  
	                },
	                dataType: "json",
	                success: function(data, status) {
	                	if(data.status == "200"){
		                    var lesson_date = data.lessonPlanBasicInfo.lesson_date.split("/");
		                    var aca_name = data.lessonPlanBasicInfo.year + " " + data.lessonPlanBasicInfo.l_aca_system_name + " " + data.lessonPlanBasicInfo.m_aca_system_name + " " + data.lessonPlanBasicInfo.aca_system_name;

		                    $("span[data-name=lesson_subject]").text("["+data.lessonPlanBasicInfo.lesson_date + " - " +data.lessonPlanBasicInfo.period+"교시] "+data.lessonPlanBasicInfo.lesson_subject);                	
		                	$("span[data-name=curr_name]").text(" ("+data.lessonPlanBasicInfo.curr_name +" / "+ aca_name+")");                 
	                	}
	                },
	                error: function(xhr, textStatus) {
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                }
	            }); 
	        }
			
			function getAssignMentList(){
				$.ajax({
                    type: "POST",
                    url: "${HOME}/ajax/pf/lesson/assignMent/List",
                    data: {
                        "lp_seq_yn" : "N",
                        "asgmt_type" : "2"
                     
                    },
                    dataType: "json",
                    success: function(data, status) {
                        if(data.status=="200"){
                            $("#assignMentListAdd table").remove();
                            var htmls = '';                            
                            $.each(data.assignMentList, function(){
                                var state = "", day = "", group_YN="";
                                var start_date = new Date(this.start_date);
                                var end_date = new Date(this.end_date);
                                
                                if(this.group_flag=="Y") group_YN="그룹과제";
                                else group_YN="개별과제";
                                
                                if(this.assignment_state == "1")
                                    state = "대기중";
                                else if(this.assignment_state == "2")
                                    state = "진행중";
                                else if(this.assignment_state == "3")
                                    state = "마감";

                                switch(this.day){
                                    case 0 : day="일";break;
                                    case 1 : day="월";break;
                                    case 2 : day="화";break;
                                    case 3 : day="수";break;
                                    case 4 : day="목";break;
                                    case 5 : day="금";break;
                                    case 6 : day="토";break;
                                }
                                
                                var period = this.period.split(",");
                                if(period.length > 1)
                                	period= this.lesson_date+" ["+period[0]+"~"+period[period.length-1]+"교시] "+this.lesson_subject;
                                else
                                	period= this.lesson_date+" ["+period+"교시] "+this.lesson_subject;
                                
                                
                                
                                htmls='<table class="tab_table ttb1">';
                                htmls+='<tbody>';    
                                htmls+='<tr class="tr01">';
                                htmls+='<td class="w1">';
                                htmls+='<div class="in_wrap">';
                                htmls+='<span class="tt_2_'+this.assignment_state+'">[ '+state+' ]</span>';
                                htmls+='<span class="tt_1">'+pad(start_date.getMonth()+1)+'/'+start_date.getDate()+'~'+pad(end_date.getMonth()+1)+'/'+end_date.getDate()+'</span>';
                                htmls+='</div>';
                                htmls+='</td>';
                                htmls+='<td class="w6">';
                                htmls+='<span class="tt_3_1">'+group_YN+' : '+this.submit_cnt+' / '+this.st_cnt+'</span>';
                                if(this.user_chk == "Y" || "${S_USER_LEVEL}" == "1")
                                	htmls+='<div class="wrap_tt" onClick="getAssignSubmitList('+this.asgmt_seq+');" style="cursor:pointer;">';
                               	else
                               		htmls+='<div class="wrap_tt" style="cursor:pointer;">';
                                htmls+='<span class="sp1">'+this.asgmt_name+'</span>';
                                htmls+='<span class="sp2">'+period+'</span>';
                                htmls+='</div>';                                            
                                htmls+='<span class="tt_4"><span class="sp_t">마감</span><span>'+pad(end_date.getMonth()+1)+'/'+end_date.getDate()+' ('+day+')</span></span>';
                                htmls+='</td>';
                                htmls+='</tr>  ';
                                htmls+='</tbody>';
                                htmls+='</table>';     
                                $("#assignMentListAdd").append(htmls);                   
                            });
                        }else{
                            alert("실패");
                        }
                        
                    },
                    error: function(xhr, textStatus) {
                        //alert("오류가 발생했습니다.");
                        document.write(xhr.responseText);
                    }
                }); 
			}
			
	        //등록과제 리스트
	        function getAssignSubmitList(asgmt_seq){
	        	
	        	$("input[name=asgmt_seq]").val(asgmt_seq);
	        	
	        	$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/pf/lesson/assignMent/SubmitList",
	                data: {
	                	"asgmt_seq" : asgmt_seq,
	                	"asgmt_type" : "2"
	                },
	                dataType: "json",
	                success: function(data, status) {
	                    if(data.status=="200"){
	                    	var group = "(개별과제)";
	                    	if(data.assignMent.group_flag == "Y")
	                    		group = "(그룹과제)";
	                    	
	                    	$("div[data-name=asgmt_name]").html('<span class="sp_s">'+group+'</span>'+data.assignMent.asgmt_name);
	                    	
	                    	tabChange("tab1");
	                        $("#feedBackListAdd tr").remove();
	                        var htmls = '';
	                        $.each(data.assignMentSubmitList, function(index){
	                            var group = "";
	                            var time = "";
	                            var readonly_yn = "readonly";
	                            var picture = "${IMG}/ph_2.png";
	                            if(!isEmpty(this.picture_path))
	                            	picture = '${RES_PATH}'+this.picture_path+'/'+this.picture_name;
	                            
	                        	if(this.group_leader_flag=="Y")
	                        		group = this.group_number+"조-조장";
	                        	else 
	                        		group = this.group_number+"조";
	                        	
	                        	if(this.submit_chk == "3") time = "time3";
	                        	else if(this.submit_chk == "2") time = "time2";
	                        	else if(this.submit_chk == "1") time = "";
	                        	
	                        	if(!isEmpty(this.asgmt_submit_seq))
	                        		readonly_yn="";
	                        	
	                            htmls='<tr>';
	                            htmls+='<td class="td_1">';
	                            
	                            if(this.submit_chk != "3")
	                            	   htmls+='<input type="checkbox" name="chk_box" class="ip_c1" value="'+index+'">';	                            
	                            htmls+='</td><td class="td_1">'+this.id+'<input type="hidden" name="asgmt_submit_seq" value="'+this.asgmt_submit_seq+'"></td>';
	                            htmls+='<td class="td_1t_l"><span class="a_mp"><span class="pt01"><img src="'+picture+'" class="pt_img"></span><span class="ssp1" name="names">'+this.name+'</span></span></td>';
	                            htmls+='<td class="td_1 time1">'+group+'</td>';
	                            htmls+='<td class="td_1 '+(this.submit_chk=="1" ? "time1" : "")+'">'+(this.submit_chk=="1" ? "O" : "")+'</td>';
	                            htmls+='<td class="td_1 '+(this.submit_chk=="2" ? "time2" : "")+'">'+(this.submit_chk=="2" ? "O" : "")+'</td>';
	                            htmls+='<td class="td_1 '+time+'">'+(this.submit_chk=="3" ? "O" : "")+'</td>';
	                            htmls+='<td class="td_1 free_textarea '+time+'">';
	                            htmls+='<textarea name="content" rows="1" class="tarea1" readonly style="height:36px;">'+this.content+'</textarea></td>';
	                            htmls+='<td class="td_1 free_textarea '+time+'">';
	                            htmls+='<textarea name="feedback" rows="1" '+readonly_yn+' class="tarea1" style="height:36px;">'+this.feedback+'</textarea></td>';
	                            htmls+='<td class="td_1 p_n '+time+'"><input type="text" '+readonly_yn+' class="ip_t1" name="score" value="'+this.score+'" onkeyup="onlyNumCheck(this);"></td>';
	                            htmls+='<td class="td_1 '+time+'">';
	                            if(!isEmpty(this.file_name))
	                            	   htmls+='<button class="dw" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');" title="'+this.file_name+'">다운로드</button>';
	                            htmls+='</td></tr>';     
	                            $("#feedBackListAdd").append(htmls);                   
	                        });
	                    }else{
	                        alert("실패");
	                    }
	                    
	                },
	                error: function(xhr, textStatus) {
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                }
	            }); 
	        }
	        
	        //전체 선택 해제
	        function AllChk(obj){
                if(obj.checked == true){
                    $("input[name=chk_box]").prop("checked",true);
                }
                else{
                    $("input[name=chk_box]").prop("checked",false);
                }
            }
            
	        //선택한 학생 팝업 피드백 리스트에 추가
            function getSelectStudent(){
            	var htmls = '';
            	
            	$("#selectStudentAdd span").remove();
            	
            	$("span[name=studentCount]").text($("input[name=chk_box]:checked").length + "명");
            	
                $.each($("input[name=chk_box]:checked"), function(){
                    htmls+= '<span class="a_mp"><span class="pt01"><img src="'+$(this).closest("tr").find("img").attr("src")+'" class="pt_img"></span>';
                    htmls+= '<span class="ssp1">'+$(this).closest("tr").find("span[name=names]").text()+'</span>';
                    htmls+= '<input type="hidden" name="asgmt_submit_seq" value="'+$(this).closest("tr").find("input[name=asgmt_submit_seq]").val()+'"/></span>';
                });             
                
                $("#selectStudentAdd").append(htmls);
            }
            
	        //선택한 학생 팝업 피드백 값으로 넣어주기
            function selectStdFeedbackSubmit(){
            	if(isEmpty($("textarea[name=popupFeedBack]").val())){
            		alert("피드백을 입력해주세요");
            		$("textarea[name=popupFeedBack]").focus();
            		return false;
            	}
            	var feedback = $("textarea[name=popupFeedBack]").val();
            	$.each($("#selectStudentAdd input[name=asgmt_submit_seq]"), function(){
            		$("#feedBackListAdd input[name=asgmt_submit_seq][value="+$(this).val()+"]").closest("tr").find("textarea[name=feedback]").val(feedback);
            	});
            	
            	$(".close2").click();
            }
            
            function pad(num) {
                num = num + '';
                return num.length < 2 ? '0' + num : num;
            }
            
            function tabChange(tabName){
            	if(tabName=="tab1"){
            		$("#tab1").show();
            		$("#tab2").hide();    
            	}else if(tabName=="tab2"){
                    $("#tab1").hide();
                    $("#tab2").show();  
                    getAssignMentList();            		
            	}
            }
            
            function asgmtSubmit(){
            	var chk = false;
            	$.each($("#feedBackListAdd textarea[name=feedback]"), function(){
            		if(!isEmpty($(this).val()))
            			chk = true;
            	});
            	
            	if(chk==false){
            		alert("입력된 피드백이 없습니다.\n피드백을 입력해주세요");
            		return false;
            	}
            	
            	if(!confirm("저장 하시겠습니까?"))
            		return false;
            	
            	$("#assignMentSubmitForm").ajaxForm({
                    type: "POST",
                    url: "${HOME}/ajax/pf/lesson/assignMent/feedback/insert",
                    dataType: "json",
                    success: function(data, status){
                        if (data.status == "200") {
                        	getAssignSubmitList($("input[name=asgmt_seq]").val());
                            alert("저장 성공하였습니다.");                            
                        } else {
                            alert("저장 실패하였습니다.");
                            $.unblockUI();                          
                        }
                        
                    },
                    error: function(xhr, textStatus) {
                        document.write(xhr.responseText); 
                        $.unblockUI();                      
                    },beforeSend:function() {
                        $.blockUI();                        
                    },complete:function() {
                        $.unblockUI();                      
                    }                       
                });     
                $("#assignMentSubmitForm").submit();      
            }
            
            function AllAssignFileDown(){
            	if($("#feedBackListAdd button").length == 0){
            		alert("다운받을 파일이 없습니다.");
            		return false;
            	}
            	$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/pf/lesson/assignMent/AllFileDown",
	                data: {
	                	"asgmt_seq" : $("input[name=asgmt_seq]").val()
	                },
	                dataType: "json",
	                success: function(data, status) {
	                    if(data.status=="200"){
	                    	location.href="${RES_PATH}"+data.filePath;
	                    }else{
	                        alert("실패");
	                    }
	                    
	                },
	                error: function(xhr, textStatus) {
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                }
	            });             	
            }
		</script>
	</head>
	<body>
			<div class="main_con">
			<!-- s_메뉴 접기 버튼 -->	
			<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
			<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
			<!-- e_메뉴 접기 버튼 -->	
				
			<!-- s_tt_wrap -->                
			<div class="tt_wrap">
			    <h3 class="am_tt">
			    	<span class="tt" data-name="lesson_subject"></span>
			    	<span class="tt_s" data-name="curr_name"></span>
			    </h3>                    
			</div>
			<!-- e_tt_wrap -->
			    
			<!-- s_tab_wrap_cc --> 
			<div class="tab_wrap_cc">
			<!-- s_해당 탭 페이지 class에 active 추가 --> 
				<button class="tab01 tablinks" onclick="pageMoveLessonPlanView(${S_LP_SEQ},${S_CURRICULUM_SEQ});">수업계획서</button>
			<button class="tab01 tablinks" onclick="pageMoveAdminLessonData('${HOME}');">수업자료</button>
			<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/formationEvaluation'">형성평가</button>
			<button class="tab01 tablinks active" onclick="location.href='${HOME}/admin/lesson/assignMent'">과제</button>
			<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/attendance/view'">출석</button>
			<!-- e_해당 탭 페이지 class에 active 추가 -->
			</div>
			<!-- e_tab_wrap_cc --> 
			
						
        <div id="tab001" class="tabcontent tabcontent5 rl">  
            <div class="stab_title">
	            <button onClick="location.href='${HOME}/admin/lesson/assignMent/create'" class="h_tt2" name="tabBtn1">과제 리스트</button>
	            <button class="h_tt3 on" onClick="" name="tabBtn2">과제제출내역조회</button>
            </div>          
            
            
            <div id="tab1">    
				<!-- s_hw_wrap-->
				<div class="hw_wrap_l">         
				    <div class="tt_t" data-name="asgmt_name"></div>        
				    <button class="list_v" onclick="tabChange('tab2');">과제 등록 내역 조회</button>    
				    <button class="fback open2">선택 학생 피드백 남기기</button>
				</div>
				<!-- e_hw_wrap-->
				<form id="assignMentSubmitForm" onSubmit="return false;">
					<input type="hidden" name="asgmt_seq" value="">
				<table class="mlms_tb1_1">
                   <thead>
                       <tr>
                           <th class="th01 bd01 w1">No.<br><input type="checkbox" class="ip_c1" onclick="AllChk(this);"></th>
                           <th class="th01 bd01 w3">학번</th>
                           <th class="th01 bd01 w4">학생</th>
                           <th class="th01 bd01 w3">조</th>
                           <th class="th01 bd01 w2">기한 내</th>
                           <th class="th01 bd01 w2">마감 후</th>
                           <th class="th01 bd01 w2">제출<br>안함</th>
                           <th class="th01 bd01 w5">제출<br>메모</th>
                           <th class="th01 bd01 w4">피드백</th>
                           <th class="th01 bd01 w2">점수</th>
                           <th class="th01 bd01 w1" onClick=""><button class="dw" onclick="AllAssignFileDown();" title="전체다운로드">다운로드</button></th>
                       </tr>
                   </thead>
                    
				        <tbody id="feedBackListAdd">  
					
				        </tbody>
			        
			    </table>
				</form>
				<div class="bt_wrap">
				    <button class="bt_2" onclick="asgmtSubmit();">저장</button>
				    <button class="bt_3" onclick="tabChange('tab2');">취소</button>
				</div>
		    </div>
		    
		    <div id="tab2" style="display:none;">    
                
	                <div class="hw_wrap_v" id="assignMentListAdd">                 
	                </div>                 
                
            </div>
        </div> 
        </div>
        <!-- s_ 피드백 남기기 팝업 -->
		<div id="m_pop2" class="pop_up_h pop_up_h_2 mo2">
            <div class="pop_wrap">
                <p class="popup_title">피드백 남기기</p>
                <button class="pop_close close2" type="button">X</button>               
				<div class="wrap2">
				    <span class="tt_1">선택한 학생</span><span class="tt_2" name="studentCount"></span>                  
				</div>
		                    
				<div class="con_wrap"> 
					<div class="con_s2" id="selectStudentAdd"> 
					
					</div>
				</div>
		
				<div class="wrap3">
				    <span class="tt_1">피드백</span>             
				</div>
						                                                    
				<div class="edit">
					<table class="tab_table edit">
						<tbody>
							<tr class="edit_wrap">
								<td class="tarea free_textarea">
								    <textarea class="tarea01" style="height:20px;" name="popupFeedBack" placeholder="피드백 내용을 입력하세요."></textarea>
								</td>
							</tr>
						</tbody>
					</table>
				</div>         
				                     
				<!-- s_t_dd --> 
				<div class="t_dd">
				   
					<div class="pop_btn_wrap2">
						<button type="button" class="btn01" onclick="selectStdFeedbackSubmit();">등록</button>
						<button type="button" class="btn02" onclick="$('.close2').click();">취소</button>                    
					</div>
				
				</div>
				<!-- e_t_dd -->                  
             
		     </div>                 
		</div>
		<!-- e_ 피드백 남기기 팝업 -->       
	</body>
</html>