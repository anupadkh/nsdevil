<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<link rel="stylesheet" href="${RES}/fullcalendar-v3.6.2/css/calendar1.css">
<link rel="stylesheet" href="${RES}/fullcalendar-v3.6.2/css/fullcalendar.print.css" media="print">
<script src="${RES}/fullcalendar-v3.6.2/js/moment.min.js"></script>
<script src="${RES}/fullcalendar-v3.6.2/js/fullcalendar.js"></script>
<script src="${RES}/fullcalendar-v3.6.2/js/locale-all.js"></script>
<script src="${JS}/schedule.js"></script>
<script type="text/javascript">
	
	$(document).ready(function() {
		$("body").addClass("full_schd");
		$('.free_textarea').on( 'keyup', 'textarea', function (e){
			$(this).css('height', 'auto' );
			$(this).height( this.scrollHeight );
		});
		$('.free_textarea').find( 'textarea' ).keyup();
		
		$("#open_01").hide();
        $("#fold_01").click(function(){
	        $("#top_pop").hide();
			$("#open_01").show();
	        $("#fold_01").hide();
	    });
        $("#open_01").click(function(){
	        $("#top_pop").show();
			$("#open_01").hide();
	        $("#fold_01").show();
	    });
        
        
        $('#calendar1').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaTwoWeek,agendaDay,listWeek'
			},
			navLinks: true,
			editable: true,
			locale: "ko",
			eventLimit: true,
			fixedWeekCount: true,	//true: 6주로 고정
			viewRender : function (view, element) {
				//년도가 변경되었을때만 로드 
  				var calendarYear = $('#calendar1').fullCalendar('getDate').format("YYYY");
				if ($(":input[name='calendarYear']").val() != calendarYear) {
					getScheduleMonth("${S_CURRICULUM_SEQ}");
				}
			},
			eventDrop: function(event, delta, revertFunc) {
				if (event.event_type == "lesson" && "${S_USER_LEVEL}" == "3" && "${S_USER_SEQ}" != event.reg_user_seq) {
					alert("변경할 권한이 없습니다.");
					revertFunc();
					return;
				}
				if (confirm("\"" + event.title + "\" 일정을 " + event.start.format("YYYY-MM-DD") + " " + event.start.format("a hh:mm") + "로 변경하시겠습니까?")) {
		        	changeEventDate(event.event_seq, event.event_type, event.start.format("YYYY-MM-DD"), event.start.format("HH:mm"), event.event_minute);
		        } else {
		        	revertFunc();
		        }
		    },
		    eventResize: function(event, delta, revertFunc) {
		    	revertFunc();
		    },
			views: {
				agendaTwoWeek: {
					type: 'basic',
					duration: { weeks: 2 },
					buttonText: '2주'
				}
			}
		});
        
        getCurriculum();
	});
	
	//교육과정 가져오기
	function getCurriculum() {
		$.ajax({
			type : "GET",
			url : "${HOME}/ajax/pf/lesson/curriculumInfo",
			data : {},
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {

					$("span[data-name=curr_name]").text(data.basicInfo.curr_name);
					$("span[data-name=academic_name]").text(": " + data.basicInfo.academic_name);
                    
				} else {

				}
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}
	
	//시간표 목록
	function getScheduleMonth(curr_seq) {
		
		if (curr_seq == "") {
			alert("교육과정을 선택해 주세요.");
			history.back(-1);
			return;
		}
		
		var dateStart = $('#calendar1').fullCalendar('getView').start.format("YYYY-MM-DD");
		var dateEnd = $('#calendar1').fullCalendar('getView').end.format("YYYY-MM-DD");
		var calendarYear = $('#calendar1').fullCalendar('getDate').format("YYYY");
		
        $.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/getSchedule",
            data: {
            	"curr_seq": curr_seq,
            	"dateStart" : dateStart,
            	"dateEnd" : dateEnd,
            	"calendarYear": calendarYear
            },
            dataType: "json",
            success: function(data, status) {
            	
            	$(":input[name='calendarYear']").val(calendarYear);
            	
            	var html = "";
            	
            	$("span[data-name='curr_name']").html(data.basicInfo.curr_name);
            	$("span[data-name='aca_system_name']").html("("+ data.basicInfo.aca_system_name + ")");
            	$("#curr_week").html(data.basicInfo.curr_week);
            	$("#curr_period_cnt").html(data.basicInfo.cnt);
            	$("#curr_start_date").html(data.basicInfo.curr_start_date);
            	$("#curr_end_date").html(data.basicInfo.curr_end_date);
            	
            	var eventList = [{
            		event_type: "lesson",
            		title: data.basicInfo.curr_name+"["+data.basicInfo.curr_start_date_mmdd+"~"+data.basicInfo.curr_end_date_mmdd+"]",
					start: data.basicInfo.curr_start_date,
					end: data.basicInfo.curr_end_date,
					editable: false
            	}];
            	
                $.each(data.schedule, function() {
                	var startDate = this.lesson_date;
                	var endDate = this.lesson_date;
                	
                	if (this.start_time != null && this.end_time != null) {
                		startDate += "T" + this.start_time;
                		endDate += "T" + this.end_time;
                	}
                	
                	var eventMinute = ampmTimeToMinute(this.end_time) - ampmTimeToMinute(this.start_time);
                	
                	var period = this.period.split(",");
                	
                	if(period.length > 1)
                		period = "["+period[0]+"~"+period[period.length-1]+"]";
                	else
                		period = "["+this.period+"]";
                	
                	var newEvent = {
                		event_type: "lesson",
                		event_seq : this.lp_seq,
                		title: period+this.lesson_subject,
                		reg_user_seq: this.reg_user_seq,
						url: 'javascript:openEditSchedulePopup(' + this.lp_seq + ',"${S_CURRICULUM_SEQ}");',
						start: startDate,
						end: endDate,
						event_minute: eventMinute
					}
					eventList.push(newEvent);
                });
                /* 
                $.each(data.memo, function() {
                	var startDate = this.memo_date;
                	var endDate = this.memo_edate;
                	var eventMinute = "";
                	if (this.start_time != null && this.end_time != null) {
                		startDate += "T" + this.start_time;
                		endDate += "T" + this.end_time;
                		eventMinute = ampmTimeToMinute(this.end_time) - ampmTimeToMinute(this.start_time);
                	}
                	
                	var newEvent = {
                		event_type: "memo",
                		event_seq : this.memo_seq,
                		title: "[개인일정]"+this.memo_name,
						url: 'javascript:openEditMemoPopup(' + this.memo_seq + ');',
						start: startDate,
						end: endDate,
						event_minute: eventMinute,
						textColor: "#FFFFFF",
						backgroundColor: "#EAA361"
					}
                	
                	$('.fc-event').css('font-size', '0.9em');
                	
					eventList.push(newEvent);
                });
                 */
                $('#calendar1').fullCalendar('removeEventSources');
                $('#calendar1').fullCalendar('addEventSource', eventList);
                
                bindPopupEvent("#m_pop1", ".open1");
            },
            error: function(xhr, textStatus) {
                alert("오류가 발생했습니다.");
                //document.write(xhr.responseText);
            }
        });
    }
	
	function changeEventDate(event_seq, event_type, event_date, start_time, event_minute) {
		//종료시간 계산
		var endMinute = (Number(start_time.substring(0, 2)) * 60) + Number(start_time.substring(3, 5)) + event_minute;
		var end_time = Math.floor(endMinute / 60) + ":" + endMinute % 60;
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/changeEventDate",
            data: {
            	"event_seq" : event_seq,
            	"event_type" : event_type,
            	"event_date" : event_date,
            	"start_time" : start_time,
            	"end_time" : end_time
            },
            dataType: "json",
            success: function(data, status) {
            	if (data.status == "200") {
            		alert("변경 되었습니다.");
            		getScheduleMonth("${S_CURRICULUM_SEQ}");
            	} else {
					alert(data.msg);
				}
            },
            error: function(xhr, textStatus) {
                alert("오류가 발생했습니다.");
                //document.write(xhr.responseText);
            }
        });
	}
</script>
</head>

<body class="full_schd">
 
<!-- s_main_con -->
<div class="main_con adm_ccschd">
<!-- s_메뉴 접기 버튼 -->	
<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
<!-- e_메뉴 접기 버튼 -->	
	
<!-- s_tt_wrap -->                
<div class="tt_wrap">
			    <h3 class="am_tt">
			    	<span class="tt" data-name="curr_name"></span>
			    	<span class="tt_s" data-name="academic_name"></span>
			    </h3>                    
			</div>
<!-- e_tt_wrap -->

<!-- s_tab_wrap_cc --> 
<div class="tab_wrap_cc">
<!-- s_해당 탭 페이지 class에 active 추가 --> 
	<button class="tab01 tablinks" onclick="pageMoveCurriculumView('${S_CURRICULUM_SEQ}');">교육과정계획서</button>	         
	<button class="tab01 tablinks active" onclick="location.href='${HOME}/admin/lesson/schedule'">시간표관리</button>	
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/unit'">단원관리</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/lessonPlanCs'">단위 수업계획서</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/survey'">만족도 조사</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/currAssignMent'">종합 과제</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/soosiGrade'">수시 성적</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/grade'">종합 성적</button>	
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/report'">운영보고서</button>
<!-- e_해당 탭 페이지 class에 active 추가 -->
</div>
 
<!-- s_mpf_tabcontent2 --> 
 <div class="mpf_tabcontent2">
<!-- s_tt_wrap -->                
<div class="tt_wrap">
<!-- s_sch_wrap -->
<div class="sch_wrap">
<!-- s_wrap_p1_uselectbox -->
<!--
                     <div class="wrap_p1_uselectbox">
		                        <div class="uselectbox">
		                                <span class="uselected">전체 교육과정</span>
		                                <span class="uarrow">▼</span>
			
		                            <div class="uoptions">
		                                    <span class="uoption opt1 firstseleted">전체 교육과정</span>
		                                    <span class="uoption opt2 ">인체의 구조 Ⅰ</span>
		                                    <span class="uoption opt3 ">호흡기학</span>
		                                    <span class="uoption opt4 ">소화기학 Ⅱ</span>
		                                    <span class="uoption opt5 ">교육과정</span>
		                                    <span class="uoption opt6 ">교육과정</span>
		                                    <span class="uoption opt7 ">교육과정</span>
		                                    <span class="uoption opt8 ">교육과정</span>
		                            </div>
		                        </div>  								
                       </div>
-->
<!-- e_wrap_p1_uselectbox -->    

<button onclick="createLesson();" class="btn_view full">수업 등록</button>
<button onclick="location.href='./schedule'" class="btn_view full">엑셀 보기</button>
<button onclick="location.href='./scheduleSearch'" class="btn_search1" title="검색"></button>           
                      
</div>
<!-- e_sch_wrap -->                       
</div>
<!-- e_tt_wrap -->
    
<!-- s_calendar1 -->      
<div id="calendar1">
<input type="hidden" name="calendarYear">
</div>
<!-- e_calendar1 -->  

</div>
</div>
<!-- e_mpf_tabcontent2 -->
<jsp:include page="schedulePopup.jsp">
	<jsp:param name="pageName" value="scheduleM" />
</jsp:include>
</body>
</html>