<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<style>
	.main .main_con .mpf_tabcontent3 .wrap2_lms_uselectbox div.uoptions{width: 200px;}	
</style>
<script>
	var popupType = "";
	var parentUnitSeq = "";
	var domain_code_html = '<span class="uoption u2open" onClick="getUnitCode(2,0, this);"><input type="hidden" value="">선택</span>';
	
	$(document).ready(function() {
		var mo1 = document.getElementById('m_pop1');
		var button = document.getElementsByClassName('close1')[0];
		var b = document.getElementsByClassName('open1');
		var i;
		for (i = 0; i < b.length; i++) {

			b[i].onclick = function() {
				mo1.style.display = "block";
			}

			button.onclick = function() {
				mo1.style.display = "none";
			}

			window.onclick = function(event) {
				if (event.target == mo1) {
					mo1.style.display = "none";
				}
			}
		}
		var mo2 = document.getElementById('m_pop2');
		var button = document.getElementsByClassName('close2')[0];
		var b = document.getElementsByClassName('open2');
		var i;
		for (i = 0; i < b.length; i++) {
			button.onclick = function() {
				lessonPopupClear();
				mo2.style.display = "none";
			}

			window.onclick = function(event) {
				if (event.target == mo2) {
					lessonPopupClear();
					mo2.style.display = "none";
				}
			}
		}
		
		//팝업 열기 클릭 시
		$(document).on("click",".open2", function(){
			getScheduleList();
			popupType = this.name;
			var input_value = $(this).closest("tbody").find("input[name=unit_seq]").val();
			parentUnitSeq = input_value ? input_value.split("_").slice(-1)[0] : "";
            mo2.style.display = "block";
		});
		
		//수업 추가 선택 체크, 해제 이벤트
	    $("#unit_list_add").on("change","input[name='chk']",function(){
	        var lp_seq = $("#unit_list_add tr:eq("+$(this).val()+")").attr("id");
	        if($(this).prop("checked")){
	            var lesson_count = $("#unit_list_add tr:eq("+$(this).val()+") td[name='lesson_count']").text();
	            var lesson_subject = $("#unit_list_add tr:eq("+$(this).val()+") td[name='lesson_subject']").text(); 
	            var pf_name = $("#unit_list_add tr:eq("+$(this).val()+") td[name='pf_name']").text(); 
	            var period_cnt = $("#unit_list_add tr:eq("+$(this).val()+") td[name='period_cnt']").val();
	            
	            var html = '<div class="a_mp" name="lp_seq_'+lp_seq+'">';
	            html +='<span class="ts1">'+lesson_count+'.</span> <span class="ts2" name="lesson_subject">'+lesson_subject+'</span>';
	            html +='<span class="sign">( </span> <span class="ts3" name="pf_name">'+pf_name+'</span>';
	            html +='<span class="sign">,</span> <span class="ts4" name="period_cnt">'+period_cnt+'</span>';
	            html +='<span class="">시수</span><span class="sign">)</span>';
	            html +='<button type="button" class="btn_c" title="삭제하기" onClick="delLesson('+lp_seq+','+$(this).val()+', this);">X</button></div>';
	            
	            $("#select_lesson_add").append(html);
	        }else{
	            $("#select_lesson_add div[name='lp_seq_"+lp_seq+"']").remove();
	        }
	        $("#lesson_count").text($("#select_lesson_add div").length);
	    });

		//단원 리스트 가져온다.
	    getUnitList();
		//수업추가현황 가져온다.
        lessaonAddPC();
		
        getCurriculum();
	});
	   
	//교육과정 가져오기
	function getCurriculum() {
		$.ajax({
			type : "GET",
			url : "${HOME}/ajax/pf/lesson/curriculumInfo",
			data : {},
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {

					$("span[data-name=curr_name]").text(data.basicInfo.curr_name);
					$("span[data-name=academic_name]").text(": " + data.basicInfo.academic_name);
                    
				} else {

				}
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}
	
	
	//선택된 수업 삭제 X 클릭
    function delLesson(user_seq, index, obj){
        $(obj).parent("div").remove();
        //$("#select_pf div[name='user_seq_"+user_seq+"']").remove();
        $("#unit_list_add input[name='chk']").eq(index).prop("checked",false);
        $("#lesson_count").text($("#select_lesson_add div").length);
    }

	//팝업 초기화
	function lessonPopupClear(){
        $("#unit_list_add tr").remove();
        $("#select_lesson_add").empty();
        $("#lesson_count").text("");
        $("#searchCount").text("");
        popupType="";
	}
	
	//수업계획서 가져온다.
	function getScheduleList(){		
		$.ajax({
            type : "POST",
            url : "${HOME}/ajax/mpf/lesson/unit/lessonScheduleList",
            data : {
                "lessonNameOrPfName" : $("#lessonNameOrPfName").val()
            },
            dataType : "json",
            success : function(data, status) {
            	$("#unit_list_add tr").remove();
                var htmls = '';
                var count = 0;
                $.each(data.lessonList, function(index) {
					htmls += '<tr class="tr01" id="'+this.lp_seq+'"><td class="w1 ta_c" name="lesson_count"></td>';
					htmls += '<td class="w2 ta_c" name="lesson_subject">'+this.lesson_subject+'</td>';
					htmls += '<td class="w3 ta_c" name="pf_name">'+this.pf_name+'</td>';
					htmls += '<td class="w4 ta_c" name="period_cnt">'+this.period_cnt+'</td>';
					var thisLp_seq = this.lp_seq;
					var chk = true;
					var lp_seq;
					
					//이미 등록된 값 있는지 찾는다.
					$.each($("#iptab input[name='lp_seq']"), function(index){
					    
					    lp_seq = $(this).val().split("_")[2].split(",");
					    
					    for(var i=0;i<lp_seq.length;i++){
					    	if(lp_seq[i]==thisLp_seq){
					    		chk=false;		
					    		return false;
					    	}
					    }
					    
					    //값이 있으면 each 빠져나간다.
					    if(chk==false)
					    	return false;
					});
							
					//이미 등록되었는지 체크
					if(this.unit_yn=='X' && chk == true )
					    htmls += '<td class="w5 ta_c">X</td>';
				    else
				    	htmls += '<td class="w5 ta_c">O</td>';	
                    htmls += '<td class="ta_c w7">';
                    
                    //이미 등록되었는지 체크                                      
                    if(this.unit_yn=='X' && chk == true){
                        htmls += '<label class="chk01">';
                        //옆에 리스트에 추가했는지 체크하여 체크박스 체크
                        if($('#select_lesson_add div[name="lp_seq_'+this.lp_seq+'"]').length == 0)
                        	htmls += '<input name="chk" type="checkbox" value="'+index+'">';
                       	else 
                       		htmls += '<input name="chk" type="checkbox" checked value="'+index+'">';
                                                        
                        htmls +='<span class="slider round">선택</span></label>';
                    }
                    htmls += '</td></tr>';
                    count++;
                });
                $("#unit_list_add").append(htmls);
                $("#searchCount").text(count);
            },
            error : function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            },
            beforeSend : function() {
            },
            complete : function() {
            }
        });
	}
	
	//선택한 수업 추가 한다.
	function selectLessonAdd(){
		if($("#select_lesson_add div").length == 0){
			alert("등록할 수업을 선택하세요");
			return false;
		}
			
		var lpSeqs = '';
        var lessonSubjectHtmls = '';
        var period_cnt = 0;
        var unit_seq = 0;
        if($("#iptab tbody").length == 0)
        	unit_seq = 1;
        else{
        	var unit_values = $("input[name=unit_seq]").last().val().split("_");
        	unit_seq = parseInt(unit_values[1]) + 1;        	
        }
        	
		$.each($("#select_lesson_add div"), function(index){
			var lp_seq = $(this).attr("name");
			period_cnt += $(this).find("span[name='period_cnt']").text();			
			if($("#select_lesson_add div").length != (index+1))
			    lpSeqs += lp_seq.replace("lp_seq_","") + ",";
			else
				lpSeqs += lp_seq.replace("lp_seq_","");
			lessonSubjectHtmls += $(this).find("span[name='lesson_subject']").text()+"("+$(this).find("span[name='period_cnt']").text()+")"+"\n";			
		});
				
		var htmls = '';
		if(popupType == "unit"){
			htmls += '<tbody class="lv1" name="'+unit_seq+'"><tr class="tr02 lv2" name="'+unit_seq+'_'+lpSeqs+'"><td class="pm0 free_textarea">';
			htmls += '<input type="hidden" name="unit_seq" value="N_'+unit_seq+'"/>';
			htmls += '<textarea class="tarea01" rows="1" placeholder="* 단원명" name="unit_name" style="height: 70px;"></textarea></td>';
			htmls += '<td class="pm0 free_textarea"><input type="hidden" name="lp_seq" value="N_'+unit_seq+'_'+lpSeqs+'"/>';
			htmls += '<textarea class="tarea01" rows="1" readonly style="height: 70px;">'+lessonSubjectHtmls+'</textarea></td>';
			htmls += '<td class="ta_c"><input type="text" readonly class="ip01" value="'+period_cnt+'"></td>';
			htmls += '<td colspan="7" class="color1 ta_c"><button class="btn_tt3" onClick="addTLO(this);">TLO 추가</button></td>';
			htmls += '<td class=""><input type="checkbox" class="chk01" name="chk" value="'+unit_seq+'"></td></tr>';
			htmls += '<tr class="tr02"><td class="">&nbsp;</td>';
			htmls += '<td class="" colspan="2" ><button class="btn_tt3 open2" name="lesson">수업추가</button></td>';
			htmls += '<td colspan="8" class="ta_c"></td></tr></tbody>';
	        $("#iptab").append(htmls);
		}else{
			//상위 시퀀스
			var unit_seq = $("#iptab tbody[name="+parentUnitSeq+"] input[name=unit_seq]").val();
			var last_lp_seq = parseInt(unit_seq.split("_")[1])+1;			
			htmls += '<tr class="tr02 lv2" name="'+parentUnitSeq+'_'+lpSeqs+'"><td class="pm0 free_textarea"></td>';
            htmls += '<td class="pm0 free_textarea"><input type="hidden" name="lp_seq" value="N_'+parentUnitSeq+'_'+lpSeqs+'"/><textarea class="tarea01" rows="1" readonly style="height: 70px;">'+lessonSubjectHtmls+'</textarea></td>';
            htmls += '<td class="ta_c"><input type="text" readonly class="ip01" value="'+period_cnt+'"></td>';
            htmls += '<td colspan="7" class="color1 ta_c"><button class="btn_tt3" onClick="addTLO(this);">TLO 추가</button></td>';
            htmls += '<td class=""><input type="checkbox" class="chk01" name="chk" value="'+parentUnitSeq+'"></td></tr>';
			$("#iptab tbody[name='"+parentUnitSeq+"'] tr").last().before(htmls);			
		}
		
        $(".close2").click();
	}
	
	//TLO 추가 버튼
	function addTLO(obj){
		var parentTrName = $(obj).closest("tr").attr("name"); //부모 수업 tr name
		var lv3cnt = $("#iptab tr.lv3[name^='"+parentTrName+"_']").length; //부모 수업 tr 에 TLO 갯수 확인.
		if(lv3cnt == 0)
			lv3cnt = 1;
		else{
			lv3cnt = parseInt($("#iptab tr.lv3[name^='"+parentTrName+"_']").last().find("input[name=tlo_seq]").val().split("_")[1])+1;
		}
		var htmls='';
		
		htmls += '<tr class="tr02 lv3" name="'+parentTrName+'_'+lv3cnt+'"><td class="pm0 free_textarea" colspan="3"></td>';
		htmls += '<td colspan="4" class="color1 ta_c"><input type="hidden" name="tlo_seq" value="N_'+parentTrName+'_'+lv3cnt+'"/><span class="tts3">TLO '+($("#iptab tr.lv3[name^='"+parentTrName+"_']").length+1)+'</span></td>';
		htmls += '<td class="pm0 free_textarea"><textarea class="tarea01" rows="1" style="height: 70px;" name="skill" placeholder="* 기술"></textarea></td>';
		htmls += '<td class="pm0 free_textarea"><textarea class="tarea01" rows="1" style="height: 70px;" name="teaching_method" placeholder="* 수업방법"></textarea></td>';
		htmls += '<td class="pm0 free_textarea"><textarea class="tarea01" rows="1" style="height: 70px;" name="evaluation_method" placeholder="* 평가방법"></textarea></td>';
		htmls += '<td class=""><input type="checkbox" class="chk01" name="chk" value="'+parentTrName+'_'+lv3cnt+'"></td></tr>';
		htmls += '<tr class="tr02" name="">';
		htmls += '<td class="" colspan="3"></td>';
		htmls += '<td colspan="8" class="ta_c"><button class="btn_tt3" onClick="addELO(\''+parentTrName+'_'+lv3cnt+'\');">ELO추가</button></td></tr>';

        if(lv3cnt == 1)			
            $("#iptab tr[name='"+parentTrName+"']").after(htmls);
        else
        	$("#iptab tr[name^='"+parentTrName+"_']").last().next("tr").after(htmls);	        
	}
	
	//ELO 추가 버튼
	function addELO(lv3Name){
		var lv4cnt = $("#iptab tr.lv4[name^='"+lv3Name+"_']").length; //부모 TLO tr 갯수 확인.
		if(lv4cnt == 0)
			lv4cnt = 1;
		else
			lv4cnt = parseInt($("#iptab tr.lv4[name^='"+lv3Name+"_']").last().find("input[name=elo_seq]").val().split("_")[1])+1;
		
	    var htmls = '';
	    htmls += '<tr class="tr02 lv4" name="'+lv3Name+'_'+lv4cnt+'">';
	    htmls += '<td class=""><input type="hidden" name="elo_seq" value="N_'+lv3Name+'_'+lv4cnt+'"/></td>';
	    htmls += '<td class="">&nbsp;</td>';
	    htmls += '<td class="">&nbsp;</td>';
	    htmls += '<td class="ta_c">ㄴ</td>';
	    htmls += '<td class="ta_c">ELO '+($("#iptab tr.lv4[name^='"+lv3Name+"_']").length+1)+'</td>';
	    htmls += '<td class="ta_c pm0">';                    
	    htmls += '<div class="wrap1_lms_uselectbox">';
	    htmls += '<div class="uselectbox">';
	    htmls += '<span class="uselected" name="domain_code"><input type="hidden" name="domain_code" value=""/>선택</span>';
	    htmls += '<span class="uarrow">▼</span>';
	    htmls += '<div class="uoptions" style="display: none;">';
	    htmls += domain_code_html;
	    htmls += '</div>';
	    htmls += '</div>';                                  
	    htmls += '</div>';
	    htmls += '</td>';
	    htmls += '<td class="ta_c pm0">';
	    htmls += '<div class="wrap2_lms_uselectbox">';
	    htmls += '<div class="uselectbox">';
	    htmls += '<span class="uselected" name="level_code"><input type="hidden" name="level_code" value=""/>선택</span>';
	    htmls += '<span class="uarrow">▼</span>';
	    htmls += '<div class="uoptions" style="display: none;" name="levelListAdd">';
	    htmls += '</div>';
	    htmls += '</div>';
	    htmls += '</div>';
	    htmls += '</td>';
	    htmls += '<td colspan="3" class="pm0 free_textarea"><textarea class="tarea01" rows="1" style="height: 70px;" name="content" placeholder="* 내용"></textarea></td>';
	    htmls += '<td class=""><input type="checkbox" class="chk01" name="chk" value="'+lv3Name+'_'+(lv4cnt+1)+'"></td></tr>';

	    if(lv4cnt == 1)            
            $("#iptab tr[name='"+lv3Name+"']").after(htmls);
        else
            $("#iptab tr[name^='"+lv3Name+"_']").last().after(htmls);        	    
	}
	
	//단원저장
	function submitUnitForm(){
		if(!confirm("등록하시겠습니까?"))
			return false;
		
		if($("#iptab tbody").length == 0){
			alert("등록할 단원을 추가하세요");
			return false;
		}
		
		$("span[name=level_code] input").attr("name","level_code");
		$("span[name=domain_code] input").attr("name","domain_code");

		$("#unitForm").ajaxForm({
            type: "POST",
            url: "${HOME}/ajax/mpf/lesson/unit/create",
            dataType: "json",
            success: function(data, status){
               
                if (data.status == "200") {
                    alert("저장이 완료되었습니다.");
                    getUnitList();
                    lessaonAddPC();
                }else{
                    alert(data.status);
                }
            },
            error: function(xhr, textStatus) {
                alert(textStatus);
                //document.write(xhr.responseText);
                $.unblockUI();
            },
            beforeSend:function() {
                $.blockUI();
            },
            complete:function() {
                $.unblockUI();
            }
        }); 
        $("#unitForm").submit();        
	}
	
	function getUnitCode(level, unitCode, obj, newYn){
		
		$.ajax({
            type : "POST",
            url : "${HOME}/ajax/mpf/lesson/unit/unitCode",
            data : {
                "level" : level
                ,"l_uc_code" : unitCode
            },
            dataType : "json",
            success : function(data, status) {
            	var level_code_html = '<span class="uoption u2open"><input type="hidden" value="">선택</span>';
            	$.each(data.unitCode, function(index){
            		level_code_html+='<span class="uoption u2open"><input type="hidden" value="'+this.uc_code+'">'+this.uc_name+'</span>';
            	});
            	$(obj).closest("tr").find("div[name=levelListAdd]").html(level_code_html);
            	if(newYn != "N")
            		$(obj).closest("tr").find("div[name=levelListAdd] span:eq(0)").click();
            },
            error : function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            },
            beforeSend : function() {
            },
            complete : function() {
            }
        });
	}
	
	//단원 가져오기
	function getUnitList(){
		$.ajax({
            type : "POST",
            url : "${HOME}/ajax/mpf/lesson/unit/unitList",
            data : {
               "level" : "1"
            },
            dataType : "json",
            success : function(data, status) {
        		$("span[data-name='curr_name']").html(data.basicInfo.curr_name);
            	$("span[data-name='aca_system_name']").html("("+ data.basicInfo.aca_system_name + ")");
            	
                $("#iptab tbody").remove();
                $.each(data.domainCode, function(){
                    domain_code_html += '<span class="uoption u2open" onClick="getUnitCode(2,'+this.uc_code+', this);"><input type="hidden" value="'+this.uc_code+'">'+this.uc_name+'</span>';                    
                });
                
                if(data.unitList.length != 0){
                	$("button[name=saveBtn]").text("수정");
                }else{
                	$("button[name=saveBtn]").text("저장");
                }
                
                var pre_unit_seq = "";                
                $.each(data.unitList, function(index) {   
                    var htmls = "";             	
                	if(this.type == "UNIT"){
	                    if(this.unit_seq != pre_unit_seq){
	                    	htmls += '<tbody class="lv1" name="'+this.unit_seq+'"><tr class="tr02 lv2" name="'+this.unit_seq+'_'+this.lp_seq+'"><td class="pm0 free_textarea">';
	                        htmls += '<input type="hidden" name="unit_seq" value="Y_'+this.unit_seq+'"/>';
	                        htmls += '<textarea class="tarea01" rows="1" placeholder="* 단원명" name="unit_name" style="height: 70px;">'+this.unit_name+'</textarea></td>';
	                        htmls += '<td class="pm0 free_textarea"><input type="hidden" name="lp_seq" value="Y_'+this.unit_seq+'_'+this.lp_seq+'"/>';
	                        htmls += '<textarea class="tarea01" rows="1" readonly style="height: 70px;">'+this.lesson_subject+'</textarea></td>';
	                        htmls += '<td class="ta_c"><input type="text" readonly class="ip01" value="'+this.period_cnt+'"></td>';
	                        htmls += '<td colspan="7" class="color1 ta_c"><button class="btn_tt3" onClick="addTLO(this);">TLO 추가</button></td>';
	                        htmls += '<td class=""><input type="checkbox" class="chk01" name="chk" value="'+this.unit_seq+'_'+this.lp_seq+'"></td></tr>';
	                        htmls += '<tr class="tr02"><td class="">&nbsp;</td>';
	                        htmls += '<td class="" colspan="2" ><button class="btn_tt3 open2" name="lesson">수업추가</button></td>';
	                        htmls += '<td colspan="8" class="ta_c"></td></tr></tbody>';
	                        $("#iptab").append(htmls);
	                    }else{
	                        htmls += '<tr class="tr02 lv2" name="'+this.unit_seq+'_'+this.lp_seq+'"><td class="pm0 free_textarea"></td>';
	                        htmls += '<td class="pm0 free_textarea"><input type="hidden" name="lp_seq" value="Y_'+this.unit_seq+'_'+this.lp_seq+'_'+this.lp_seq+'"/>';
	                        htmls += '<textarea class="tarea01" rows="1" readonly style="height: 70px;">'+this.lesson_subject+'</textarea></td>';
	                        htmls += '<td class="ta_c"><input type="text" readonly class="ip01" value="'+this.period_cnt+'"></td>';
	                        htmls += '<td colspan="7" class="color1 ta_c"><button class="btn_tt3" onClick="addTLO(this);">TLO 추가</button></td>';
	                        htmls += '<td class=""><input type="checkbox" class="chk01" name="chk" value="'+this.unit_seq+'_'+this.lp_seq+'"></td></tr>';
	                        $("#iptab tbody[name='"+this.unit_seq+"'] tr").last().before(htmls); 
	                    }
                	}
                	else if(this.type == "TLO"){
                		htmls += '<tr class="tr02 lv3" name="'+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+'"><td class="pm0 free_textarea" colspan="3"></td>';
                        htmls += '<td colspan="4" class="color1 ta_c"><input type="hidden" name="tlo_seq" value="Y_'+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+'"/>';
                        htmls += '<span class="tts3">TLO '+this.tlo_order_num+'</span></td>';
                        htmls += '<td class="pm0 free_textarea"><textarea class="tarea01" rows="1" style="height: 70px;" name="skill" placeholder="* 기술">'+this.skill+'</textarea></td>';
                        htmls += '<td class="pm0 free_textarea"><textarea class="tarea01" rows="1" style="height: 70px;" name="teaching_method" placeholder="* 수업방법">'+this.teaching_method+'</textarea></td>';
                        htmls += '<td class="pm0 free_textarea"><textarea class="tarea01" rows="1" style="height: 70px;" name="evaluation_method" placeholder="* 평가방법">'+this.evaluation_method+'</textarea></td>';
                        htmls += '<td class=""><input type="checkbox" class="chk01" name="chk" value="'+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+'"></td></tr>';
                        htmls += '<tr class="tr02" name="">';
                        htmls += '<td class="" colspan="3"></td>';
                        htmls += '<td colspan="8" class="ta_c"><button class="btn_tt3" onClick="addELO(\''+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+'\');">ELO추가</button></td></tr>';
                        if($("#iptab tr[name^='"+this.unit_seq+'_'+this.lp_seq+"_']").length == 0)
                            $("#iptab tr[name='"+this.unit_seq+'_'+this.lp_seq+"']").after(htmls);
                        else
                        	$("#iptab tr[name^='"+this.unit_seq+'_'+this.lp_seq+"_']").last().next("tr").after(htmls);
                	}else if(this.type == "ELO"){
                		htmls += '<tr class="tr02 lv4" name="'+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+'_'+this.elo_seq+'">';
                        htmls += '<td class=""><input type="hidden" name="elo_seq" value="Y_'+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+'_'+this.elo_seq+'"/></td>';
                        htmls += '<td class="">&nbsp;</td>';
                        htmls += '<td class="">&nbsp;</td>';
                        htmls += '<td class="ta_c">ㄴ</td>';
                        htmls += '<td class="ta_c">ELO '+this.elo_order_num+'</td>';
                        htmls += '<td class="ta_c pm0">';                    
                        htmls += '<div class="wrap1_lms_uselectbox">';
                        htmls += '<div class="uselectbox">';
                        htmls += '<span class="uselected" name="domain_code"><input type="hidden" value="'+this.domain_code+'">'+this.domain_name+'</span>';
                        htmls += '<span class="uarrow">▼</span>';
                        htmls += '<div class="uoptions" style="display: none;">';
                        htmls += domain_code_html;
                        htmls += '</div></div></div></td>';
                        htmls += '<td class="ta_c pm0">';
                        htmls += '<div class="wrap2_lms_uselectbox">';
                        htmls += '<div class="uselectbox">';
                        htmls += '<span class="uselected" name="level_code"><input type="hidden" value="'+this.level_code+'">'+this.level_name+'</span>';
                        htmls += '<span class="uarrow">▼</span>';
                        htmls += '<div class="uoptions" style="display: none;" name="levelListAdd">';
                        htmls += '</div>';
                        htmls += '</div>';
                        htmls += '</div>';
                        htmls += '</td>';
                        htmls += '<td colspan="3" class="pm0 free_textarea"><textarea class="tarea01" rows="1" style="height: 70px;" name="content" placeholder="* 내용">'+this.content+'</textarea></td>';
                        htmls += '<td class=""><input type="checkbox" class="chk01" name="chk" value="'+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+'_'+this.elo_seq+'"></td></tr>';
                        if($("#iptab tr[name^='"+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+"_']").length == 0)            
                            $("#iptab tr[name='"+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+"']").after(htmls);
                        else
                            $("#iptab tr[name^='"+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+"_']").last().after(htmls);  
                	}
                    pre_unit_seq = this.unit_seq;
                });
                $.each($("textarea"),function(){
                	$(this).height($(this).prop("scrollHeight"));
                });
                
                $.each($("span[name=domain_code]"), function(index){
                	getUnitCode(2, $(this).find("input").val(), this, "N");     	
                });
            },
            error : function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            },
            beforeSend : function() {
            },
            complete : function() {
            }
        });
	}
	
	//선택 삭제
	function chkDelete(){
		if($(":checkbox[name='chk']:checked").length == 0){
			alert("삭제할 데이터를 선택해주세요");
			return false;
		}
		
		if(!confirm("삭제하시겠습니까?"))
			return false;
				
		var chk_value = "";
		$.each($(":checkbox[name='chk']:checked"), function(){
			$("#iptab tr[name^='"+$(this).val()+"']").remove();
		});		

        $("span[name=level_code] input").attr("name","level_code");
        $("span[name=domain_code] input").attr("name","domain_code");
        
		$("#unitForm").ajaxForm({
            type: "POST",
            url: "${HOME}/ajax/mpf/lesson/unit/create",
            dataType: "json",
            success: function(data, status){
               
                if (data.status == "200") {
                    alert("삭제 완료");
                    getUnitList();
                    lessaonAddPC();
                }else{
                    alert(data.status);
                }
            },
            error: function(xhr, textStatus) {
                alert(textStatus);
                //document.write(xhr.responseText);
                $.unblockUI();
            },
            beforeSend:function() {
                $.blockUI();
            },
            complete:function() {
                $.unblockUI();
            }
        }); 
        $("#unitForm").submit(); 
	}
	
	//수업추가현황
	function lessaonAddPC(){
		$.ajax({
            type : "POST",
            url : "${HOME}/ajax/mpf/lesson/unit/lessonAddPC",
            data : {
            },
            dataType : "json",
            success : function(data, status) {
            	$("span[name=totalCnt]").text(data.AddPCList.lp_cnt);
            	$("span[name=addCnt]").text(data.AddPCList.add_cnt);
            	$("span[name=waitingCnt]").text(data.AddPCList.lp_cnt-data.AddPCList.add_cnt);
            },
            error : function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            },
            beforeSend : function() {
            },
            complete : function() {
            }
        });
	}
</script>
</head>

<body>
    
            <div class="main_con adm_ccschd"> 
				<!-- s_메뉴 접기 버튼 -->	
				<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
				<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
				<!-- e_메뉴 접기 버튼 -->	
					
				<!-- s_tt_wrap -->                
				<div class="tt_wrap">
			    <h3 class="am_tt">
			    	<span class="tt" data-name="curr_name"></span>
			    	<span class="tt_s" data-name="academic_name"></span>
			    </h3>                    
			</div>
				<!-- e_tt_wrap -->
				
				<!-- s_tab_wrap_cc --> 
				<div class="tab_wrap_cc">
				<!-- s_해당 탭 페이지 class에 active 추가 --> 
					<button class="tab01 tablinks" onclick="pageMoveCurriculumView('${S_CURRICULUM_SEQ}');">교육과정계획서</button>	         
					<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/schedule'">시간표관리</button>	
					<button class="tab01 tablinks active" onclick="location.href='${HOME}/admin/lesson/unit'">단원관리</button>
					<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/lessonPlanCs'">단위 수업계획서</button>
					<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/survey'">만족도 조사</button>
					<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/currAssignMent'">종합 과제</button>
					<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/soosiGrade'">수시 성적</button>
					<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/grade'">종합 성적</button>	
					<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/report'">운영보고서</button>
				<!-- e_해당 탭 페이지 class에 active 추가 -->
				</div>

                <!-- s_mpf_tabcontent3 -->
                <div class="tabcontent3">
                    <!-- s_tt_wrap -->
                    <div class="tt_wrap">
                        <span class="tt_u1">단원 및 학습성과 관리</span>
                        <!-- <button class="btn_up1 open1">엑셀일괄등록</button> -->

                        <div class="tt_u2">
                            <span class="u_t1">수업추가현황</span>
                            <span class="u_t2">전체</span>
                            <span class="u_num" name="totalCnt"></span>
                            <span class="u_t3">차시</span>
                            <span class="u_t2">추가</span>
                            <span class="u_num" name="addCnt"></span>
                            <span class="u_t3">차시</span>
                            <span class="u_t2">대기</span>
                            <span class="u_num" name="waitingCnt"></span>
                        </div>

                    </div>
                    <!-- e_tt_wrap -->

                    <div class="t_wrap1">
                        <div class="t_text">* 단원 및 학습성과 관리는 교육과정 시간표 등록 후 이용 가능합니다.
                        </div>
                        <button class="btn_tt1" onClick="chkDelete();">선택 삭제</button>
                    </div>
                    <!-- s_tab_table ttb_u -->
                    <form id="unitForm" onSubmit="return false;" >
                                             
                        <table class="tab_table ttb_u" id="iptab">  
                            <thead class="mainTbody">                   
                                <tr class="tr02">
                                    <td class="th01 w11">단원명<br>(UNIT)</td>
                                    <td class="th01 w11">수업제목<br>(시수)
                                    </td>
                                    <td class="th01 w13">시수</td>
                                    <td colspan="2" class="th01 w14">구분</td>
                                    <td class="th01 w13">영역</td>
                                    <td class="th01 w13">수준</td>
                                    <td class="th01 w12">기술</td>
                                    <td class="th01 w11">수업<br>방법
                                    </td>
                                    <td class="th01 w11">평가<br>방법
                                    </td>
                                    <td class="th01 w11_1">선택</td>
                                </tr>   
                            </thead>
                        </table>
                    </form>
                    <!-- e_tab_table ttb_u -->

                    <div class="t_wrap2">
                        <button class="btn_tt2 open2" name="unit">단원 추가</button>
                        <button class="btn_tt1" onClick="chkDelete();">선택 삭제</button>
                    </div>

                    <div class="bt_wrap">
                        <!-- <button class="bt_1">임시 저장</button> -->
                        <button class="bt_2" name="saveBtn" onclick="submitUnitForm();">등록</button>
                        <!-- <button class="bt_3">취소</button> -->
                    </div>

                </div>
                <!-- e_mpf_tabcontent3 -->
            </div>
            <!-- e_main_con -->
    
	<!-- s_ 엑셀 일괄 등록 팝업 -->
	<div id="m_pop1" class="pop_up_ex pop_up_ex_1 mo1">
		<div class="pop_wrap">
			<p class="popup_title">교육과정 시간표 엑셀 업로드</p>

			<button class="pop_close close1" type="button">X</button>

			<button class="btn_exls_down_2">엑셀 양식 다운로드</button>

			<div class="t_title_1">
				<span class="sp_wrap_1">
                                시간표 등록 엑셀양식을 먼저 다운로드 받으신 뒤,
					<br>다운로드 받은 엑셀파일양식에 시간표 정보를 입력하여 파일 등록 하시면
					<br>전체 시간표가 일괄 업로드 됩니다.
				</span>
			</div>

			<div class="pop_ex_wrap">
				<div class="sub_fwrap_ex_1">
					<span class="ip_tt">엑셀 파일</span> 
					<input type="text" class="ip_sort1_1" value="">
					<button class="btn_r1_1">파일선택</button>
				</div>

				<!-- s_t_dd -->
				<div class="t_dd">

					<div class="pop_btn_wrap">
						<button type="button" class="btn01" onclick="">등록</button>
						<button type="button" class="btn02">취소</button>
					</div>

				</div>
				<!-- e_t_dd -->

			</div>
		</div>
	</div>
	<!-- e_ 엑셀 일괄 등록 팝업 -->

	<!-- s_ 팝업 : 단원별 수업 추가 -->
	<div id="m_pop2" class="pop_up_unitadd mo2">
		<div class="pop_wrap">
			<button class="pop_close close2" type="button">X</button>

			<p class="t_title">단원별 수업 추가</p>

			<!-- s_l_wrap -->
			<div class="l_wrap">
				<!-- s_pop_ipwrap1 -->
				<div class="pop_ipwrap1">
					<div class="pop_date">
						<input type="text" class="ip_search" id="lessonNameOrPfName" placeholder="수업주제 / 교수명">
						<button class="btn_search1" onClick="getScheduleList();">검색</button>
					</div>

				</div>
				<!-- e_pop_ipwrap1 -->

				<!-- s_table_b_wrap -->
				<div class="table_b_wrap">

					<div class="wrap1">
						<span class="tt_1">검색결과</span><span class="tt_2" id="searchCount"></span>
						<span class="tt_3">건</span>
						<!-- <button class="btn_add1">선택한 수업 추가</button> -->
					</div>

					<!-- s_pop_twrap0 -->
					<div class="pop_twrap0">
						<!-- s_box_tt -->
						<div class="box_tt">
							<span class="th01 w1">차시</span> 							
							<span class="th01 w2">수업주제</span>
							<span class="th01 w3">교수명</span> 
							<span class="th01 w4">시수</span>
							<span class="th01 w5">단원등록</span> 
							<span class="th01 w6">선택</span>
						</div>
						<!-- s_box_tt -->
					</div>
					<!-- e_pop_twrap0 -->
					<!-- s_pop_twrap1 -->
					<div class="pop_twrap1">
						<table class="tab_table ttb1">
							<tbody id="unit_list_add">
								<!-- <tr class="tr01">
									<td class="w1 ta_c">01</td>
									<td class="w2 ta_c">심전도</td>
									<td class="w3 ta_c">나교수</td>
									<td class="w4 ta_c">2</td>
									<td class="w5 ta_c">○</td>
									<td class="ta_c w7"><label class="chk01"> <input
											type="checkbox"> <span class="slider round">선택</span>
									</label></td>
								</tr> -->
								
							</tbody>
						</table>

					</div>
					<!-- e_pop_twrap1 -->
				</div>
				<!-- e_table_b_wrap -->
			</div>
			<!-- e_l_wrap -->

			<!-- s_r_wrap -->
			<div class="r_wrap">
				<!-- s_table_b_wrap -->
				<div class="table_b_wrap">
					<!-- s_pht -->
					<div class="pht">
						<div class="wrap2">
							<span class="tt_1">추가할 수업 List</span><span class="tt_2" id="lesson_count"></span>
						</div>

						<div class="con_wrap">
							<div class="con_s2" id="select_lesson_add">
								<!-- s_ 추가할 수업 1set -->
								<!-- <div class="a_mp">
									<span class="ts1">01.</span> <span class="ts2">수업 주제명 </span> <span
										class="sign">( </span> <span class="ts3">교수명</span> <span
										class="sign">,</span> <span class="ts4">○</span> <span
										class="">시수</span><span class="sign">)</span>
									<button class="btn_c" title="삭제하기">X</button>
								</div> -->
								<!-- e_ 추가할 수업1set -->
							</div>

						</div>
						<!-- e_pop_table -->

					</div>
					<!-- e_table_b_wrap -->
				</div>
				<!-- e_r_wrap -->

				<div class="t_dd">
	
					<div class="pop_btn_wrap2">
						<button type="button" class="btn01" onclick="selectLessonAdd();">등록</button>
						<button type="button" class="btn02" onClick="$('.close2').click();">취소</button>
					</div>
	
				</div>
			</div>
		</div>
	</div>
	<!-- e_ 팝업 : 단원별 수업 추가 -->

</body>
</html>