<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<script type="text/javascript">
	$(document).ready(function() {
		bindPopupEvent("#m_pop1", ".open1");
		bindPopupEvent("#m_pop2", ".open2");
		$(document).on("click",".open1", function(){
			if($(this).text()=="0")
				return;
			var lp = $(this).closest("tr").attr("name").split("_");
			getStList(lp[0]);
		});
		
		$(document).on("click",".open2", function(){		
			var lp = $(this).closest("tr").attr("name").split("_");
			
			getLessonPlan(lp[1],lp[0]);
		});

		getLessonPlanList();
		
		getCurriculum();
	});

	//교육과정 가져오기
	function getCurriculum() {
		$.ajax({
			type : "GET",
			url : "${HOME}/ajax/pf/lesson/curriculumInfo",
			data : {},
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {

					$("span[data-name=curr_name]").text(data.basicInfo.curr_name);
					$("span[data-name=academic_name]").text(": " + data.basicInfo.academic_name);
                    
				} else {

				}
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}
	
	function getLessonPlanList() {

		$.ajax({
			type : "POST",
			url : "${HOME}/ajax/pf/lesson/lessonPlanRegCs/list",
			data : {
				"searchInput" : $("input[name=searchInput]").val()
			},
			dataType : "json",
			success : function(data, status) {				
				if(data.status=="200"){
                	$("span[data-name='curr_name']").html(data.currInfo.curr_name);
            		$("span[data-name='aca_system_name']").html("("+ data.currInfo.aca_system_name + ")");
					$("#lpListAdd").empty();
					var htmls = "";
					$.each(data.lpList, function(index){
						var picturePath = this.picture_path;
						var lesson_method = this.lesson_method.split("\|\|");
						var lesson_subject = this.lesson_subject;
						var name = "";
						if(!isEmpty(this.name)){
							name = this.name+'('+this.code_name+')';
						}
						if(isEmpty(lesson_subject))
							lesson_subject = "미등록";
						if(isEmpty(picturePath))
							picturePath = "${IMG}/ph_3.png";
						else
							picturePath = "${RES_PATH}"+this.picture_path;	
						htmls = '<tr name="'+this.lp_seq+'_'+this.period_seq+'">'
							+'<td class="td_1"><span class="tt01">'+this.period_seq+'</span></td>'
							+'<td class="td_1"><span class="tt_t">'+this.lesson_date+'</span></td>'
							+'<td class="td_1"><span class="tt01">'+this.period+'</span></td>'
							+'<td title="수업계획서 보기" class="open2 td_1 t_l">'
							+'<span class="a_mp" style="width:100%;">'
							+'<span class="ssp1" style="width:auto;">'+name+'</span>'
							+'</span>'
							+'</td>'
							+'<td title="수업계획서 보기" class="open2 td_1 t_l">'+lesson_subject+'</td>'
							+'<td class="td_1">';
							if(this.lesson_data_chk == "Y")
								htmls+='<span class="sp sp01" onClick="pageMoveLessonData('+this.lp_seq+');" style="cursor:pointer;">작성</span>';							
							htmls+='</td><td class="td_1">';					
							+'<td class="td_1">';
						if(this.lecture_yn=='Y')
							htmls+='<span class="sp sp01">강의</span>'
						if(this.fe_cnt > 0)
							htmls+='<span class="sp sp02">형성</span>'
						
						if(!isEmpty(lesson_method)){
							htmls+='<span class="sp sp03">';
							for(var i=0;i<lesson_method.length;i++){
								htmls+=lesson_method[i];
								if(lesson_method.length != (i+1))
									htmls+='<br>';
							}							
							htmls+='</span>';
						}
							
						htmls+='</td>'
							+'<td class="td_1">'
							+'<div class="n_wrap">';
						if(this.attendance_flag > 0){							
							htmls+='<span class="n1">'+this.st_cnt+'</span>'
							+'<span class="sign"> / </span>'
							+'<span class="n2">'+this.attend+'</span>'
							+'<span class="sign"> / </span>'
							+'<span class="n3 open1" title="결석자 보기">'+this.absence+'</span>';
						}
						htmls+='</div>'
							+'</td>'
							+'</tr>';
							
						$("#lpListAdd").append(htmls);
						
					});
					
				}else{
					alert("수업리스트 가져오기 실패");
				}

			},
			error : function(xhr, textStatus) {
				alert("오류가 발생했습니다.");
			}
		});
	}
	
	function getLessonPlan(period_seq, lp_seq){
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/lessonPlanRegCs/lessonPlanInfo",
            data: {              
            	"period_seq" : period_seq,
            	"lp_seq" : lp_seq
            },
            dataType: "json",
            success: function(data, status) {
            	if(data.status=="200"){
            		var picturePath = "";
                	var lesson_date = data.lessonPlanBasicInfo.lesson_date.split("/");
                	var aca_name = data.lessonPlanBasicInfo.year + " " + data.lessonPlanBasicInfo.l_aca_system_name + " " + data.lessonPlanBasicInfo.m_aca_system_name + " " + data.lessonPlanBasicInfo.aca_system_name;
                	var lesson_subject = data.lessonPlanBasicInfo.lesson_subject
                	if(isEmpty(data.lessonPlanBasicInfo.lesson_subject))
                		lesson_subject = "[수업명 미등록]";
                	if(isEmpty(data.lessonPlanBasicInfo.picture_path))
                		picturePath = "${IMG}/ph.png";
            		else
            			picturePath = "${RES_PATH}"+data.lessonPlanBasicInfo.picture_path;
                	
                	$("p[data-name=title]").text("["+data.lessonPlanBasicInfo.curr_name+"] "+data.lessonPlanBasicInfo.period_seq+"차시 : "+data.lessonPlanBasicInfo.name+"("+data.lessonPlanBasicInfo.code_name+")");
                	$("span[data-name=pfName]").text(data.lessonPlanBasicInfo.name+"("+data.lessonPlanBasicInfo.code_name+")");                	
                	$("#userImg").attr("src",picturePath);                	
                	$("input[name=lesson_subject]").val(lesson_subject);                	
                	$("td[data-name=learning_outcome]").html(data.lessonPlanBasicInfo.learning_outcome);
                	$("td[data-name=lesson_code]").text(data.lessonPlanBasicInfo.lesson_code);
                	$("span[data-name=lesson_date]").text(data.lessonPlanBasicInfo.lesson_date + " - " + data.lessonPlanBasicInfo.period+"교시");
                	$("span[data-name=lesson_time]").text(data.lessonPlanBasicInfo.start_time + " ~ " + data.lessonPlanBasicInfo.end_time);
                	$("td[data-name=period_cnt]").text(data.lessonPlanBasicInfo.period_cnt);
                	$("span[data-name=title_aca_name]").text(aca_name);
                	$("td[data-name=classroom]").val(data.lessonPlanBasicInfo.classroom);
                	
               		$("span[data-name=title_lesson_subject]").html(data.lessonPlanBasicInfo.curr_name + 
               				'<span class="sign">&gt;</span>'+data.lessonPlanBasicInfo.lesson_subject+'<span class="sign">&gt;</span>'+lesson_date[0]+'월 ' + lesson_date[1]+'일 ' +
               				'['+data.lessonPlanBasicInfo.period+'교시] ('+data.lessonPlanBasicInfo.start_time + '~' + data.lessonPlanBasicInfo.end_time+')');            		
                	
                	var ranNum = 0;
                	var htmls='';
                	
                	//핵심 임상표현
                	$.each(data.lessonCoreClinicList,function(index){
                        ranNum = Math.floor(Math.random()*7) + 1;

                        htmls += '<div class="c_wrap"><span class="tt_c_1 color0'+ranNum+'">'+this.code_name+'</span>';  
                    });
                    $("#coreClinicListAdd").html(htmls);

                	htmls = '';
                	//임상표현
                	$.each(data.lessonClinicList,function(index){
                        ranNum = Math.floor(Math.random()*7) + 1;

                        htmls += '<div class="c_wrap"><span class="tt_c_1 color0'+ranNum+'">'+this.code_name+'</span>';   
                    });
                    $("#clinicListAdd").html(htmls);
                	
                    htmls = '';
                	//임상술기
                	$.each(data.lessonOsceList,function(index){
                        ranNum = Math.floor(Math.random()*7) + 1;

                        htmls += '<div class="c_wrap"><span class="tt_c_1 color0'+ranNum+'">'+this.osce_name+'</span>';          
                    });
                    $("#osceCodeListAdd").html(htmls);

                    htmls = '';                	
                    
                	//진단명
                    $.each(data.lessonDiagnosisList,function(index){
                        ranNum = Math.floor(Math.random()*7) + 1;
                        htmls += '<div class="c_wrap"><span class="tt_c_1 color0'+ranNum+'">'+this.code_name+'</span>';
                    });
                    $("#diaCodeAdd").html(htmls);
                    
                  //졸업역량
                    htmls='';
                    $.each(data.lessonFinishCapabilityList,function(index){
                        ranNum = Math.floor(Math.random()*7) + 1;
                        
                        htmls += '<span class="sp12">('+this.fc_code+') '+this.fc_name+'</span>';
                    });
                    $("#finishCapabilityCodeAdd").html(htmls); 
                    
                    //형성평가
                    htmls='';                    
                    $.each(data.formationEvaluationList,function(index){                    	
                        ranNum = Math.floor(Math.random()*7) + 1;                        
                        htmls += '<div class="c_wrap"><span class="tt_c_1 color0'+ranNum+'">'+this.code_name+'</span></div><div class="c_wrap">';
                    });
                    $("#feCodeAdd").html(htmls); 
                    
                    //비강의
                    htmls='';
                    $.each(data.lessonMethodList, function(index){                 	
                        ranNum = Math.floor(Math.random()*7) + 1;            
                    	htmls += '<div class="c_wrap"><span class="tt_c_1 color_tt'+ranNum+'">'+this.code_name+'</span></div>';
                    });
        			$("#lessonMethodAdd").html(htmls);
        			
        			htmls='';
        			
        			if(isEmpty(data.tloInfo))
        				$("#tloAdd").closest("tr").hide();
        			else
        				$("#tloAdd").closest("tr").show();
        			
        			$.each(data.tloInfo, function(index){
        				htmls+='<span class="sp12">(TLO '+this.order_num+') '+this.skill+'</span>';
        			});
        			$("#tloAdd").html(htmls);
        			
        			htmls='';
        			if(isEmpty(data.eloInfo))
        				$("#eloAdd").closest("tr").hide();
        			else
        				$("#eloAdd").closest("tr").show();
        			$.each(data.eloInfo, function(index){
        				htmls+='<span class="sp12">(TLO '+this.tlo_num+'-ELO '+this.elo_num+') '+this.content+'</span>';
        			});
        			$("#eloAdd").html(htmls);
        			
        			if(data.preNextInfo.pre_lp_seq == 0){
        				$("#preDiv").hide();
        			}else{
        				$("#preDiv").show();
        				$("#preDiv a").attr("href","javascript:getLessonPlan("+data.preNextInfo.pre_period_seq+","+data.preNextInfo.pre_lp_seq+");");
        				$("span[data-name=prePeriod]").text(data.preNextInfo.pre_period_seq+"차시");
        			}
        			
        			if(data.preNextInfo.next_lp_seq == 0){
        				$("#nextDiv").hide();
        			}else{
        				$("#nextDiv").show();
        				$("#nextDiv a").attr("href","javascript:getLessonPlan("+data.preNextInfo.next_period_seq+","+data.preNextInfo.next_lp_seq+");");
        				$("span[data-name=nextPeriod]").text(data.preNextInfo.next_period_seq+"차시");
        			}
        			
        			$("#m_pop2").show();
            	}else{
            		alert("수업계획서 가져오기 실패");
            	}
            },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            }
        }); 
    }
	
	var Color_arry = new Array("color01","color02","color03","color04","color05","color06","color07");
	
	function getStList(lp_seq){
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/lessonPlanRegCs/absenceSTList",
            data: {              
            	"lp_seq" : lp_seq
            },
            dataType: "json",
            success: function(data, status) {
            	if(data.status=="200"){
            		var htmls = "";
            		$("span[data-name=lesson_date]").text(data.lpInfo.lesson_date+' ('+getDayOfWeek(data.lpInfo.lesson_day)+')');
            		$("span[data-name=period]").text(data.lpInfo.period+"교시");
            		$("span[data-name=lesson_subject]").text(data.lpInfo.lesson_subject);
            		
            		$.each(data.stList, function(index){
            			var picture_path = this.picture_path;
            			if(isEmpty(picture_path))
            				picture_path = "${IMG}/ph_3.png";
            				else
            					picture_path = "${RES_PATH}"+this.picture_path;	
            			htmls+='<div class="a_mp">'
							+'<span class="pt01"><img src="'+picture_path+'" alt="사진" class="pt_img"></span>'
							+'<span class="ssp1">'+this.name+'</span>'
							+'</div>';
            		});
            		$("#stListAdd").html(htmls);
        			$("#m_pop1").show();
            	}else{
            		alert("결석자 리스트 가져오기 실패");
            	}
            },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            }
        }); 
	}
	
	function pageMoveLessonData(lp_seq){
		$.ajax({
            type: "POST",
            url: "${M_HOME}/ajax/pf/common/setCurrLpSeq",
            data: {   
            	"lp_seq" : lp_seq,
            	"curr_seq" : "${S_CURRICULUM_SEQ}"
            },
            dataType: "json",
            success: function(data, status) {
           		var win = window.open('${HOME}/admin/lesson/lessonData/view','_blank');
           		win.focus();
            },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            }
        });
	}
	
	function lessonPrint() {
		var initBody = document.body.innerHTML;
		var lessonSbuject = $("input[name=lesson_subject]").val();
		window.onbeforeprint = function () {
			document.body.innerHTML = "<div class='pop_up_classplan' style='display:block;'><div id='popupDiv' class='content'>"+document.getElementById("popupDiv").innerHTML+"</div></div>";
			$(".btn_prt").hide();
			$("input[name=lesson_subject]").val(lessonSbuject);
		}
		window.onafterprint = function () {

			document.body.innerHTML = initBody;
			$(".btn_prt").show();
			$("input[name=lesson_subject]").val(lessonSbuject);
		}
		window.print();
	}
</script>
</head>

<body>
<div class="main_con adm_ccschd">
	<!-- e_tt_wrap -->
<!-- s_메뉴 접기 버튼 -->	
<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
<!-- e_메뉴 접기 버튼 -->	
	
<!-- s_tt_wrap -->                
<div class="tt_wrap">
			    <h3 class="am_tt">
			    	<span class="tt" data-name="curr_name"></span>
			    	<span class="tt_s" data-name="academic_name"></span>
			    </h3>                    
			</div>
<!-- e_tt_wrap -->

<!-- s_tab_wrap_cc --> 
<div class="tab_wrap_cc">
<!-- s_해당 탭 페이지 class에 active 추가 --> 
	<button class="tab01 tablinks" onclick="pageMoveCurriculumView('${S_CURRICULUM_SEQ}');">교육과정계획서</button>	         
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/schedule'">시간표관리</button>	
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/unit'">단원관리</button>
	<button class="tab01 tablinks active" onclick="location.href='${HOME}/admin/lesson/lessonPlanCs'">단위 수업계획서</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/survey'">만족도 조사</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/currAssignMent'">종합 과제</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/soosiGrade'">수시 성적</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/grade'">종합 성적</button>	
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/report'">운영보고서</button>
<!-- e_해당 탭 페이지 class에 active 추가 -->
</div>


	<!-- s_mpf_tabcontent5 -->
	<div class="tabcontent5">
		<!-- s_tt_wrap -->
		<!-- <div class="tt_wrap1">
			<div class="tt_g1">
				<span class="g_t2">졸업역량</span>
				<span class="g_t3">총</span>
				<span class="g_num">37</span>
				<span class="g_t3">건 중</span>
				<span class="g_num">35</span>
				<span class="g_t3">건</span>
				<span class="g_t3"> 배정됨</span>
			</div>
		</div> -->
		<!-- e_tt_wrap -->

		<!-- s_tt_wrap -->
		<div class="tt_wrap2">

			<!-- <button class="tt1" onclick="location.href='mpf_schd_class_gd.html'">졸업역량별 보기</button> -->
			<div class="sch_wrap">
				<input type="text" class="ip_search" name="searchInput" placeholder="검색할 수업주제 ㆍ 교수명을 입력하세요." style="margin-top:5px;"
					 onkeypress="javascript:if(event.keyCode==13){getLessonPlanList(); return false;}">
				<button class="btn_search1" style="margin-top:5px;" onClick="getLessonPlanList();"></button>
			</div>
		</div>
		<!-- e_tt_wrap -->

		<!-- s_mlms_tb1 -->
		<table class="mlms_tb1">
			<thead>
				<tr>
					<th class="th01 bd01 w1" style="width:5%;">차시</th>
					<th class="th01 bd01 w2" style="width:8%;">날짜</th>
					<th class="th01 bd01 w2" style="width:8%;">교시</th>
					<th class="th01 bd01 w4" style="width:17%;">교수명</th>
					<th class="th01 bd01 w4_1" style="width:36%;">수업주제</th>
					<th class="th01 bd01 w4_1" style="width:8%;">수업자료</th>
					<th class="th01 bd01 w2" style="width:8%;">수업방법</th>
					<th class="th01 bd01 w3" style="width:10%;">출석</th>
				</tr>
			</thead>
			<tbody id="lpListAdd">
						
			</tbody>
		</table>
		<!-- e_mlms_tb1 -->


	</div>
	<!-- e_mpf_tabcontent5 -->
</div>
	<!-- s_ 팝업 : 결석자 -->
	<div id="m_pop1" class="pop_up_abs mo1">
		<div class="pop_wrap">
			<input type="button" class="pop_close close1" type="button" onClick="$('#m_pop1').hide();" value="X">

			<p class="t_title">결석자</p>

			<!-- s_table_b_wrap -->
			<div class="table_b_wrap">

				<!-- s_pht -->
				<div class="pht">
					<div class="wrap2">
						<span class="tt_1" data-name="lesson_date">9/26 (화)</span>
						<span class="tt_2" data-name="period">1교시</span>
						<span class="tt_2" data-name="lesson_subject">해부학 수업</span>
					</div>

					<div class="con_wrap">
						<div class="con_s2" id="stListAdd">
														
						</div>
					</div>

				</div>
				<!-- e_pop_table -->

			</div>
			<!-- e_pht -->

			<div class="t_dd">

				<div class="pop_btn_wrap2">
					<button type="button" class="btn01" onclick="$('#m_pop1').hide();">확인</button>
				</div>

			</div>
		</div>
	</div>
	<!-- e_ 팝업 : 결석자-->

	<!-- s_수업 계획서 팝업 -->
	<div id="m_pop2" class="pop_up_classplan mo2">
		<!-- s_pop_wrap -->
		<div class="pop_wrap">
			<input type="button" class="pop_close close2" type="button" onClick="$('#m_pop2').hide();" value="X">
 
			<p class="t_title" data-name="title"></p>

			<!-- s_content -->
			<div id="popupDiv" class="content">
				<div class="stab_wrap2">
					<div class="pt_wrap">
						<div class="in_wrap">
							<div class="in_wrap_s">
								<img src="" alt="교수 사진" class="pt1" id="userImg">							
							</div>
							<span class="btn_tt" data-name="pfName" style="width:auto;">홍길동(내과)</span>
						</div>

						<div class="btn_wrap_pp">
							<!-- <button class="btn_pdf" title="PDF다운로드"></button> -->
							<button class="btn_prt" title="인쇄하기" onClick="lessonPrint();"></button>
						</div>

						<div class="title_wrap">
							<!-- 미등록 시  class : nonregi 추가 -->
							<input class="tt nonregi" value="" disabled="disabled" type="text" name="lesson_subject">
							<div class="tts">
								<span class="sp01" data-name="title_aca_name"></span> 
								<span class="sp01" data-name="title_lesson_subject">
								</span>
							</div>
						</div>

					</div>
				</div>

				<table class="tab_table ttb1">
					<tbody>
						<tr class="tr02">
							<td class="th01 w1">수업<br>코드
							</td>
							<td class="w2" data-name="lesson_code"></td>
							<td class="th01 w1">일정</td>
							<td class="w5"><span class="sp07" data-name="lesson_date"></span><span class="sp08" data-name="lesson_time"></span></td>
							<td class="th01 w1">시수</td>
							<td class="w4" data-name="period_cnt">1</td>
							<td class="th01 w1">장소</td>
							<td class="w3" data-name="classroom"></td>
						</tr>
						<tr class="tr02">
							<td class="th01 w1">학습<br>성과</td>
							<td colspan="7" class="" data-name="learning_outcome"></td>
						</tr>
						<tr class="tr02">
							<td class="th01 w1">학습<br>방법
							</td>
							<td colspan="7" class="" id="lessonMethodAdd">								
							</td>
						</tr>
						<tr class="tr02">
							<td class="th01 w1">핵심<br>${code2.code_name }
							</td>
							<td colspan="7" class="" id="coreClinicListAdd">
							</td>
						</tr>
						<tr class="tr02">
							<td class="th01 w1">관련<br>${code2.code_name }
							</td>
							<td colspan="7" class="" id="clinicListAdd">
							</td>
						</tr>
						<tr class="tr02">
							<td class="th01 w1">${code1.code_name }
							</td>
							<td colspan="7" class="" id="osceCodeListAdd">
							</td>
						</tr>
						<tr class="tr02">
							<td class="th01 w1">${code3.code_name }
							</td>
							<td colspan="7" class="" id="diaCodeAdd">
							</td>
						</tr>
						<tr class="tr02">
							<td class="th01 w1">형성<br>평가
							</td>
							<td colspan="7" class="" id="feCodeAdd">								
							</td>
						</tr>
						<tr class="tr02">
							<td class="th01 w1">졸업<br>역량
							</td>
							<td colspan="7" class="" id="finishCapabilityCodeAdd">
							</td>
						</tr>						
						<tr class="tr02">
							<td class="th01 w1">TLO</td>
							<td colspan="7" class="" id="tloAdd">
							</td>
						</tr>
						<tr class="tr02">
							<td class="th01 w1">ELO</td>
							<td colspan="7" class="" id="eloAdd">
							</td>
						</tr>
						
						
					</tbody>
				</table>
			</div>

			<div class="t_dd">
				<div class="a_wrap" id="preDiv">
					<a href="#" class="arrow02">◀</a>
					<div class="pop_btn_wrap1">
						<span class="sp01">이전</span> <br> <span class="sp01" data-name="prePeriod"></span>
					</div>
				</div>

				<div class="a_wrap" id="nextDiv">
					<a href="#" class="arrow01">▶</a>
					<div class="pop_btn_wrap2">
						<span class="sp01">다음</span> <br> <span class="sp01" data-name="nextPeriod"></span>
					</div>
				</div>
			</div>
			<!-- e_content -->
		</div>
		<!-- e_pop_wrap -->
	</div>
	<!-- e_수업 계획서 팝업 -->

</body>
</html>