<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript">
	
	var ajaxTask;
	
	//일정 수정 팝업 열기
	function openEditSchedulePopup(lp_seq, curr_seq) {
    	$(":input[name='curr_seq']").val(curr_seq);
    	$(":input[name='lp_seq']").val(lp_seq);
		$("#m_pop1").show();
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/getScheduleDetail",
            data: {
            	"lp_seq" : lp_seq,
            	"curr_seq" : curr_seq
            },
            dataType: "json",
            success: function(data, status) {
            	$("#period_seq").html(data.curr_name);
            	$(":input[name='lesson_subject']").val(data.lesson_subject);
            	$(":input[name='lesson_date']").val(data.lesson_date);
            	$(":input[name='period']").val(data.period);
            	$(":input[name='start_time']").val(data.start_time_12h);
            	$(":input[name='end_time']").val(data.end_time_12h);
            	$(":input[name='classroom']").val(data.classroom);
            	
            	$(".schwrap_uselectbox .uoptions").empty().hide();
            	
            	if (data.user_seq != null) {
            		
            		var userPic = "${IMG}/ph_2.png";
                	
                	if (this.picture_path != null) {
                		userPic = "${RES_PATH}"+this.picture_path;
                	}
            		
	            	var html = '<span class="a_mp"><span class="pt01"><img src="' + userPic + '" alt="등록된 사진 이미지" class="pt_img"></span><span class="ssp1"><span class="userName">' + data.pf_name + '</span> (' + data.department + ')</span></span>';
	            	
	            	$("#pfSelected span.uselected").html(html);
	        		$("#pfSelected").show();
	        		$(".schwrap_uselectbox input.uselected").hide();
	        		
	            	$(":input[name='user_seq']").val(data.user_seq);
	            	$(":input[name='user_name_search']").val(data.pf_name);
	            	$(":input[name='user_name']").val(data.pf_name);
            	} else {
            		startSearchPf();
            	}
            	
            	var classMethod = "";
            	
            	//Y : 강의
            	if (data.lecture_yn == "Y") {
            		classMethod = '<span class="sp sp01">강의</span>';
            	}
            	
            	//Y : 형성평가
            	if (data.fe_flag == "Y") {
            		classMethod += '<span class="sp sp02">형성</span>';
            	}
            	
            	if (data.lesson_method != "") {
            		var methodList = data.lesson_method.split(",");
            		$.each(methodList, function() {
            			classMethod += '<span class="sp sp03">' + this + '</span>';
            		});
            	}
            	
            	$("#lessonMethod").html(classMethod);
            	
            	$(":input[name='learning_outcome']").val(data.learning_outcome);
            },
            error: function(xhr, textStatus) {
                alert("오류가 발생했습니다.");
            }
		});
	}
	
	//일정 상세 저장
	function saveScheduleDetail() {
		
		if (!$.trim($(":input[name='lesson_subject']").val())) {
			alert("수업 주제를 입력해주세요.");
			return;
		}
		if (!$(":input[name='lesson_date']").val()) {
			alert("날짜를 입력해주세요.");
			return;
		}
		if (!$(":input[name='period']").val()) {
			alert("교시를 입력해주세요.");
			return;
		}
		if (!$(":input[name='start_time']").val()) {
			alert("시작시간을 선택해주세요.");
			return;
		}
		if (!$(":input[name='end_time']").val()) {
			alert("종료시간을 선택해주세요.");
			return;
		}
		if (ampmTimeToMinute($(":input[name='start_time']").val()) > ampmTimeToMinute($(":input[name='end_time']").val())) {
			alert("종료시간을 시작시간 이후로 입력해주세요.");
			return;
		}
		/* if (!$(":input[name='user_seq']").val()) {
			alert("교수를 선택해주세요.");
			return;
		}	 */	
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/saveSchedule",
            data: $("#scheduleForm").serialize(),
            dataType: "json",
            success: function(data, status) {
            	if (data.status == "200") {
            		if(isEmpty($(":input[name='lp_seq']").val()))
            			alert("저장이 완료되었습니다.");
            		else
            			alert("수정이 완료되었습니다.");
            		            		
            		$("#m_pop1").hide();
            		if ("${param.pageName}" == "scheduleM") {
            			getScheduleMonth("${S_CURRICULUM_SEQ}");
            		} else {
            			getSchedule("${S_CURRICULUM_SEQ}");
            		}
            	} else {
            		alert("오류가 발생했습니다.");
            	}
            },
            error: function(xhr, textStatus) {
                alert("오류가 발생했습니다.");
            }
		});
	}
	
	//교수 검색
	function searchPf(event) {
		
		var user_name = $.trim($(":input[name='user_name']").val());
		
		//검색 키워드가 이전과 동일하면 검색 안함
		if (user_name == $(":input[name='user_name_search']").val()) {
			return;
		}
		
		//이전에 실행중인 ajax 작업이 있으면 검색 안함
		if(0 < $.active) {
			return;
        }
		
		var uoptions = $(".schwrap_uselectbox .uoptions");
		
		//검색값이 없으면 요청 취소하고 셀렉트박스 삭제
		if (user_name.length == 0) {
			uoptions.empty().hide();
			return;
		}
		
		clearTimeout(ajaxTask);
		
		ajaxTask = setTimeout(function() {
			
			$(":input[name='user_name_search']").val(user_name);
			
			$.ajax({
	            type: "POST",
	            url: "${HOME}/ajax/pf/lesson/getPfList",
	            data: {
	            	"user_name": $(":input[name='user_name_search']").val()
	            },
	            dataType: "json",
	            success: function(data, status) {
	            	uoptions.empty();
	            	
	            	if (0 < data.pfList.length) {
	            		uoptions.show();
	            	}
	            	
	            	$.each(data.pfList, function() {
	            		var userPic = "${IMG}/ph_2.png";
	                	
	                	if (this.picture_path != null) {
	                		userPic = this.picture_path;
	                	}
	            		
	            		var html = '<a onclick="javascript:selectPf(\'' + this.user_seq + '\', this);"><span class="uoption">'
		                        + '<span class="a_mp"><span class="pt01"><img src="' + userPic + '" alt="등록된 사진 이미지" class="pt_img"></span><span class="ssp1"><span class="userName">' + this.name + '</span> (' + this.department + ')</span></span>'
		                    	+ '</span></a>';
	                    uoptions.append(html);
	            	});
	            },
	            error: function(xhr, textStatus) {
	            }
	        });
		}, 100);
	}
	
	//교수 선택완료 처리
	function selectPf(user_seq, element) {
		var uoption = $("span.a_mp", element);
		$("#pfSelected span.uselected").append(uoption);
		$("#pfSelected").show();
		$(".schwrap_uselectbox input.uselected").hide();
		var user_name = $("span.userName", uoption).html();
		$(":input[name='user_seq']").val(user_seq);
		$(":input[name='user_name']").val(user_name);
		$(":input[name='user_name_search']").val(user_name);
		$(".schwrap_uselectbox .uoptions").empty().hide();
	}
	
	//기존 선택을 삭제하고 교수 검색 필드 출력
	function startSearchPf() {
		$("#pfSelected").hide();
		$("#pfSelected span.uselected").empty();
		$(".schwrap_uselectbox input.uselected").show();
		$(":input[name='user_seq']").val("");
		$(":input[name='user_name_search']").val("");
		$(":input[name='user_name']").val("");
		$(":input[name='user_name']").focus();
	}
	
	function createLesson(){
		$(":input[name=curr_seq]").val("${S_CURRICULUM_SEQ}");
		$(":input[name=lp_seq]").val("");
		$("#period_seq").html("");
    	$(":input[name='lesson_subject']").val("");
    	$(":input[name='lesson_date']").val("");
    	$(":input[name='period']").val("");
    	$(":input[name='start_time']").val("");
    	$(":input[name='end_time']").val("");
    	$(":input[name='classroom']").val("");
    	
    	$('.timepicker1').val("");
    	$('.timepicker2').val("");
    	startSearchPf();
   	
    	$("#lessonMethod").empty();
    	
    	$(":input[name='learning_outcome']").val("");
    	
		$("#m_pop1").show();
	}
</script>

<!-- s_ 팝업 : 시간표 수정 -->
<div id="m_pop1" class="pop_up_schd_mdf mo1">
<!-- s_pop_wrap -->
<div class="pop_wrap">
<form id="scheduleForm" onSubmit="return false;">
<input type="hidden" name="curr_seq">
<input type="hidden" name="lp_seq">
 		        <button class="pop_close close1" type="button">X</button>	  
                <p class="t_title"><span id="period_seq"></span></p>       

<!-- s_pop_swrap -->	   
<div class="pop_swrap">

<div class="swrap">
    <span class="tt">수업 주제</span>
    <div class="con"> 
<!-- 미등록 시 class : nonregi 추가 -->
    <textarea class="ip_ta1" name="lesson_subject" rows="2"></textarea>
    </div>    
</div>


<div class="swrap tarea free_textarea">
    <span class="tt t_a">학습성과</span>
    <div class="con">
<!-- 미등록 시 class : nonregi 추가 -->
    <textarea class="tarea1" name="learning_outcome" style="height: 40px;"></textarea>
    </div>
</div>	

<div class="swrap">
    <span class="tt">날짜</span>
    <div class="con"> 
        <div class="twrap1">
<!-- 미등록 시 class : nonregi 추가 -->
			<input type="text" name="lesson_date" class="dateyearpicker-input_1 ip_date" readonly>
        </div>
        <div class="twrap2">
<!-- 미등록 시 class : nonregi 추가 -->
            <input type="text" name="period" class="ip_tt3" value="3" maxlength="20"><span class="sign2">교시</span>
        </div>
    </div>
</div>
 
<div class="swrap time_pick2">
    <span class="tt">시간</span>
    <div class="con">  
<!-- 미등록 시 class : nonregi 추가 -->
    <input type="text" class="timepicker1 ip_time" name="start_time" readonly>
    <span class="sign">~</span>
<!-- 미등록 시 class : nonregi 추가 -->
    <input type="text" class="timepicker2 ip_time" name="end_time" readonly>
    </div>
</div>  
   
<div class="swrap">
    <span class="tt">장소</span>
    <div class="con">
    <!-- 미등록 시 class : nonregi 추가 -->
        <input type="text" name="classroom" class="ip_tt1">
    </div>
</div>  

<div class="swrap">
    <span class="tt">교수</span>
    <div class="con"> 
<!-- s_schwrap_uselectbox -->
      <div class="schwrap_uselectbox">
		                        <div class="uselectbox">
		                        		<input type="hidden" name="user_seq">
		                        		<input type="hidden" name="user_name_search">
		                        		<input type="text" name="user_name" class="uselected ip_tt1" onkeyup="javascript:searchPf();"  style="display: none;">
		                        		
		                        		<div id="pfSelected">
			                                <span class="uselected"></span>
			                                <a href="javascript:startSearchPf();"><span class="uarrow">검색</span></a>
										</div>
										
		                            <div class="uoptions" style="display: none;"></div>
		                        </div>  								
      </div>
<!-- e_schwrap_uselectbox -->
    </div>    
</div>

<div class="swrap">
    <span class="tt">수업방법</span>
    <div class="con">
<!-- 미등록 시 class : nonregi 추가 -->
<!-- 수업방법 - sp01 : 강의, sp02 : 형성, sp03 : 실습 -->
		<div id="lessonMethod"></div>      
    </div>
</div> 

			
</div>
<!-- e_pop_swrap --> 
<div class="t_dd" id="btnDiv">
                   
                    <div class="pop_btn_wrap">
                     <button type="button" class="btn01" onclick="javascript:saveScheduleDetail();">저장</button>
                     <button type="button" class="btn02 close">취소</button>
                    </div>
                
                </div>
</form>
</div> 
<!-- e_pop_wrap -->
</div>
<!-- e_ 팝업 : 시간표 수정 -->