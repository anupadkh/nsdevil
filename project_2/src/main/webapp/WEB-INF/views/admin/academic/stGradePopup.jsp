<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript">
	function getAcaGrade(){
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/admin/academic/academicReg/currScore/stScore",
            data: {
            	"user_seq" : $("input[name=user_seq]").val()
                },
            dataType: "json",
            success: function(data, status) {
            	$("#currScore").show();
            	$("#soosiScore").hide();
            	if(data.status == "200"){
            		var htmls = '';
            		$("#currScoreList").empty();
            		$.each(data.scoreList, function(index){
            			var grade = this.grade;
            			if(this.final_grade=="F")
            				grade = "-";
            			htmls+='<tr class="">'
        					+'<td class="th02"><span class="sp_1">'+this.curr_name+'</span></td>'  
        					+'<td class="td01"><span class="sp_2">'+grade+'</span></td>'
        					+'<td class="td01"><span class="sp_2">'+this.final_grade+'</span></td>'
        					+'<td class="td01"><span class="sp_2">'+this.change_score+'</span></td>'
        					+'</tr>';
            		});
            		
            		$("#currScoreList").html(htmls);
            	}else{
            		alert("성적가져오기 실패하였습니다.");
            	}
            },
            error: function(xhr, textStatus) {
                alert("실패");
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            },beforeSend:function() {
            	$.blockUI();
            },
            complete:function() {
            	$.unblockUI();
            }
        });
	}
	
	function getAcaSoosiGrade(){
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/admin/academic/academicReg/soosiScore/stScore",
            data: {
            	"user_seq" : $("input[name=user_seq]").val()
                },
            dataType: "json",
            success: function(data, status) {
            	$("#currScore").hide();
            	$("#soosiScore").show();
            	if(data.status == "200"){
            		var htmls = "";
            		var pre_curr_seq = "";
            		$("#soosiScore").empty();
            		$.each(data.scoreList, function(index){
            			if(index != 0 && pre_curr_seq != this.curr_seq){
            				htmls +='</table></div>';
            			}
            			
            			if(pre_curr_seq != this.curr_seq){
	            			htmls +='<div class="box">'
	            			+'<div class="s_tt">'
	            			+'<span class="tt1">'+this.curr_name+'</span>'
	            			+'<span class="sign">–</span><span class="tts">총</span>'
	            			+'<span class="tt_n">'+this.src_cnt+'</span><span class="tts">회 시행</span>'
	            			+'</div>'
	            			+'<table class="tb1">'
	            			+'<tr class="">'
	            			+'<td class="th01 w5" style="width:35%;">시험명</td>'
	            			+'<td class="th01 w1" style="width:15%;">시험일</td>'
	            			+'<td class="th01 w1" style="width:10%;">총문항</td>'
	            			+'<td class="th01 w2" style="width:10%;">정답수</td>'
	            			+'<td class="th01 w2" style="width:10%;">점수</td>'
	            			+'<td class="th01 w2" style="width:10%;">평균</td>'
	            			+'<td class="th01 w2" style="width:10%;">편차</td>'
	            			+'</tr>';
            			}
            			
						htmls+='<tr class="">'
						+'<td class="th02"><span class="sp_1">'+this.src_name+'</span></td>'
						+'<td class="td01"><span class="sp_2">'+this.src_date+'</span></td>'
						+'<td class="td01"><span class="sp_2">'+this.question_cnt+'</span></td>'
						+'<td class="td01"><span class="sp_2">'+this.answer_cnt+'</span></td>'
						+'<td class="td01"><span class="sp_2">'+this.score+'</span></td>'
						+'<td class="td01"><span class="sp_2">'+this.avg_score+'</span></td>'
						+'<td class="td01"><span class="sp_2">'+this.deviation+'</span></td>'
						+'</tr>';
						
						if(data.scoreList.length == (index+1))
            				htmls +='</table></div>';

						pre_curr_seq = this.curr_seq;
            		});
            		$("#soosiScore").html(htmls);
            		
            	}else{
            		alert("성적가져오기 실패하였습니다.");
            	}
            },
            error: function(xhr, textStatus) {
                alert("실패"+ textStatus);
                //alert("오류가 발생했습니다.");
                //document.write(xhr.responseText);
            },beforeSend:function() {
            	$.blockUI();
            },
            complete:function() {
            	$.unblockUI();
            }
        });
	}
	
	function getAcademicPopup(){
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/admin/academic/academicReg/titleInfo",
            data: {                      
            },
            dataType: "json",
            success: function(data, status) {
            	console.log(data);
            	if(data.status=="200"){
            		if(!isEmpty(data.acaInfo.academic_name))
                		$("span[data-name=academic_name]").text(data.acaInfo.academic_name);
            		
	            	$("span[data-name=academic_date]").text("("+data.acaInfo.aca_start_date +"~"+data.acaInfo.aca_end_date+")" );
            	}
            },
            error: function(xhr, textStatus) {
                document.write(xhr.responseText); 
                $.unblockUI();                        
            },beforeSend:function() {
                $.blockUI();                        
            },complete:function() {
                $.unblockUI();                         
            }                   
        });
	}
	
	function setNextPreSt(id){
		var this_index = $("#stListAdd tr[name="+id+"]").index();
		var pre_index = this_index-1;
		var next_index = this_index+1;
		
		if(this_index == 0)
			pre_index = -1;
		
		if((this_index+1) == $("#stListAdd tr").length){
			next_index = 0;
		}

		if(pre_index == -1){
			$("#preBtn").hide();
		}else{
			$("#preBtn").show();
			var id=$("#stListAdd tr:eq("+pre_index+")").find("td:eq(0) a").text();
			var name=$("#stListAdd tr:eq("+pre_index+")").find("td:eq(1) a").text();
			var user_seq=$("#stListAdd tr:eq("+pre_index+")").find("input[name=st_user_seq]").val(); 
			$("span[data-name=pre_st]").text(name);
			$("#preBtn").attr("onClick","getStGrade('"+id+"','"+name+"',"+user_seq+")");
		}
		
		if(next_index == 0){
			$("#nextBtn").hide();
		}else{
			$("#nextBtn").show();
			var id=$("#stListAdd tr:eq("+next_index+")").find("td:eq(0) a").text();
			var name=$("#stListAdd tr:eq("+next_index+")").find("td:eq(1) a").text();
			var user_seq=$("#stListAdd tr:eq("+next_index+")").find("input[name=st_user_seq]").val(); 
			$("span[data-name=next_st]").text(name);
			$("#nextBtn").attr("onClick","getStGrade('"+id+"','"+name+"',"+user_seq+")");
		}
		
	}
	
	function getStGrade(id, name, user_seq){
		$("span[data-name=stInfo]").text(id + " " + name);
		$("input[name=user_seq]").val(user_seq);

		if($("#type").attr("data-value")=="1"){
			getAcaGrade();	
		}else{
			getAcaSoosiGrade();
		}
		setNextPreSt(id);
	}
</script>
<!-- s_성적표 팝업 -->
		<div id="m_pop2" class="reportcard mo2">
			<input type="hidden" name="user_seq">
			<!-- s_pop_wrap -->
			<div class="pop_wrap">
				<button class="pop_close close2" type="button" onClick="$('#m_pop2').hide();"></button>
	
				<p class="t_title" data-name="stName">성적표</p>
	
				<!-- s_content -->
				<div class="content">
					<div class="top_wrap">
						<div class="tt_wrap">
	
							<div class="wrap_uselectbox">
								<div class="uselectbox">
									<span class="uselected" id="type" data-value=""></span> <span class="uarrow">▼</span>
									<div class="uoptions" id="popupType" style="display: none;">
										<span class="opentb1 uoption" data-value="1" onClick="getAcaGrade();">학사성적</span>
										<span class="opentb2 uoption" data-value="2" onClick="getAcaSoosiGrade();">수시성적</span>
									</div>
								</div>
							</div>
	
							<div class="fr">
								<div class="btn_wrap_pp">
									<button class="btn_down2" title="성적 다운로드">성적 다운로드</button>
									<button class="btn_prt" title="인쇄">인쇄</button>
								</div>
							</div>
	
						</div>
					</div>
	
					<div class="title_wrap">
						<div class="tt_wrap_s">
							<span class="tt_a" data-name="academic_name"></span>
							<span class="tt_b" data-name="academic_date"></span>
						</div>
						<div class="tt_wrap_s">
							<span class="tt_s" data-name="stInfo"></span>
						</div>
					</div>
	
					<!-- s_tb_wrap1 -->
					<div class="tb_wrap1" id="currScore">
	
						<!-- s_tb1 -->
						<table class="tb1">
							<thead>
								<tr class="">
									<td class="th01 w5">교육과정명</td>
									<td class="th01 w1">이수<br>학점 </td>
									<td class="th01 w1">확정<br>등급 </td>
									<td class="th01 w1">변환<br>점수 </td>
								</tr>
							</thead>
							<tbody id="currScoreList">
							</tbody>							
						</table>
						<!-- e_tb1 -->
					</div>
					<!-- e_tb_wrap1 -->
		
					<!-- s_tb_wrap3 -->
					<div class="tb_wrap3" id="soosiScore">
					
					</div>
					<!-- e_tb_wrap3 -->
	
				</div>
				<!-- e_content -->
				<div class="bfaf_wrap">
					<button class="bf_wrap" onclick="location.href='#'" id="preBtn">
						<span class="sp_tt" data-name="pre_st"></span>성적<span class="bf">이전</span>
					</button>
					<button class="af_wrap" onclick="location.href='#'" id="nextBtn">
						<span class="af">다음</span><span class="sp_tt" data-name="next_st"></span>성적
					</button>
				</div>
			</div>
			<!-- e_pop_wrap -->
		</div>
		<!-- e_성적표 팝업 -->