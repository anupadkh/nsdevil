<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<style>
.codeInput{width: 98%;
    height: 34px;
    line-height: 34px;
    padding-left: 5px;}
.codeInput:focus{width: 98%;
    height: 32px;
    line-height: 32px;
    padding-left: 5px;
    border:solid 1px rgb(154, 211, 255);}
</style>
<script>
	var order = "";
	$(document).ready(function() {				
		getCode(1); 
		
		$(".ip_box").hide();
		
		$(document).on("click",".order", function(){
        	if($(this).hasClass("up")){
        		order = $(this).attr("name") + " DESC";
        	    $(".order").not(this).html("▼");
        	    $(".order").removeClass("up");
                $(this).html("▲");
        		getCode(1);
        	}else{
        		order = $(this).attr("name") + " ASC"; 
        		$(this).addClass("up");
                $(".order").not(this).removeClass("up");
                $(".order").html("▼");
        		getCode(1);
        	}
        	
        });
	});

	function deleteCode(code){
		if(!confirm("삭제하시겠습니까?")){
			return;
		}
			
		$.ajax({
			type : "POST",
			url : "${HOME}/ajax/admin/academic/codeManagement/code2/delete",
			data : {
				"code" : code
			},
			dataType : "json",
			success : function(data, status) {
				if(data.status == "200"){
					alert("삭제 완료 하였습니다.");
					getCode(1); 
				}else if(data.status == "201"){
					alert("사용 중인 코드 입니다. 삭제할 수 없습니다.");
				}
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
				$.unblockUI();
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}
	
	function getCode(page) {

		$.ajax({
			type : "POST",
			url : "${HOME}/ajax/admin/academic/codeManagement/code2/list",
			data : {
				"page" : page
				,"code_name" : $("#code_name").val()
				,"order" : order
			},
			dataType : "json",
			success : function(data, status) {
				var htmls = "";
					
				$.each(data.list, function(index){
					htmls+='<tr>'
						+'<td class="td_1">'+this.row_num+'</td>'
						+'<td class="td_1">'
						+'<input type="hidden" name="code" value="'+this.cpx_code+'">'
						+'<input type="text" class="codeInput" name="code_name" value="'+this.cpx_name+'">'
						+'</td>'
						+'<td class="td_1">'+this.reg_date+'</td>'
						+'<td class="td_1">'
						+'<button class="btn_tt4" onclick="updateCode(this);">수정</button>'
						+'<button class="btn_c" title="삭제하기" onClick="deleteCode(\''+this.cpx_code+'\');">삭제</button>'
						+'</td>'
						+'</tr>';					
				});
				
				$("#codeListAdd").html(htmls);				

				$(".num").text(data.totalCnt);
                $("#pagingBtnAdd").html(data.pageNav);
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
				$.unblockUI();
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}
	
	function updateCode(obj){
		if(!confirm("수정하시겠습니까?")){
			return;
		}
		var code = $(obj).closest("tr").find("input[name=code]").val();
		var code_name = $(obj).closest("tr").find("input[name=code_name]").val();
		
		$.ajax({
			type : "POST",
			url : "${HOME}/ajax/admin/academic/codeManagement/code2/update",
			data : {
				"code" : code
				,"code_name" : code_name				 
			},
			dataType : "json",
			success : function(data, status) {
				if(data.status == "200"){
					alert("저장이 완료되었습니다.");
					getCode(1); 
				}else if(data.status == "201"){
					alert("사용 중인 코드 입니다. 삭제할 수 없습니다.");
				}
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
				$.unblockUI();
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});		
	}
	
	function searchCodeName(){
		if(isEmpty($("#code_name").val())){
			alert("검색할 단어를 입력하세요.");
			return;
		}
		
		$.ajax({
			type : "POST",
			url : "${HOME}/ajax/admin/academic/codeManagement/code2/search",
			data : {
				"code_name" : $("#code_name").val()				 
			},
			dataType : "json",
			success : function(data, status) {
				if(data.status == "200"){
					var htmls = "";
					$.each(data.list, function(index){
						htmls += '<li class="li_1" onClick="setSearch(\''+this.cpx_name+'\');">'+this.cpx_name+'</li>';	
					});		
					$("#searchListAdd").empty();
					$("#searchListAdd").html(htmls);
					if(data.list.length > 0)
						$(".ip_box").show();
					else
						alert("해당 단어가 포함된 항목이 없습니다.");
				}
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
				$.unblockUI();
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});		
	}
	
	function setSearch(code_name){
		$("#code_name").val(code_name);
		$(".ip_box").hide();
		getCode(1);
		
	}
	
	function excelDown(){
		location.href="${HOME}/admin/academic/codeManagement/code2/excelDown";
	}
</script>
</head>

<body>
	<!-- s_container_table -->
	<div id="container" class="container_table">
		<!-- s_contents -->
		<div class="contents main">

			<!-- s_left_mcon -->
			<div class="left_mcon aside_l grd">
				<div class="sub_menu adm_grd">
					<div class="title">학사관리</div>

					<!-- s_학사코드 메뉴 -->
					<div class="boardwrap">
						<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">학사체계관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on">학사코드관리</span>
							</div>
							<ul class="panel on">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>		
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2 on">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">교육과정관리</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">교육과정관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청관리</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicReg/list'">학사등록관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">졸업역량관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
					</div>
					<!-- e_학사코드 메뉴 -->

				</div>
			</div>
			<!-- e_left_mcon -->

			<!-- s_main_con -->
			<div class="main_con adm_grd">
				<!-- s_메뉴 접기 버튼 -->
				<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
				<!-- e_메뉴 접기 버튼 -->

				<!-- s_tt_wrap -->
				<div class="tt_wrap">
					<h3 class="am_tt">
						<span class="tt">코드2(${code_name })</span>
						<button class="btn_tt1n" onclick="post_to_url('${HOME}/admin/academic/codeManagement/code/nameChange', {'code_cate':'cpx_code_name'});">메뉴명 수정</button>
					</h3>
				</div>
				<!-- e_tt_wrap -->

				<!-- s_adm_content2n -->
				<div class="adm_content2n">
					<!-- s_tt_wrap -->

					<!-- s_wrap_wrap -->
					<div class="wrap_wrap">

						<!-- s_wrap_s1 -->
						<div class="wrap_s1">
							<span class="tt1">항목명</span> 
							<input type="hidden" id="code">
							<input type="text" class="ip_tt1" id="code_name" placeholder="[항목명에 포함된 단어]를 입력 후 검색 버튼을 누르세요."
								 onkeypress="javascript:if(event.keyCode==13){searchCodeName(); return false;}">
							<button class="btn_sch" onClick="searchCodeName();">검색</button>
							<div class="num_wrap">총 <span class="num"></span>건
							</div>
						</div>
						<!-- e_wrap_s1 -->
						<!-- s_wrap_s2 -->
						<div class="wrap_s2">
							<span class="bf1"></span>
							<!-- s_ip_box -->
							<ul class="ip_box" id="searchListAdd">
							</ul>
							<!-- e_ip_box2 -->
							<span class="af1"></span> <span class="af2"></span>
						</div>
						<!-- e_wrap_s -->

						<!-- s_btnwrap_s2 -->
						<div class="btnwrap_s2">

							<!-- s_btnwrap_s2_s2 -->
							<div class="btnwrap_s2_s2">
								<button class="btn_tt1" onclick="location.href='${HOME}/admin/academic/codeManagement/code2/reg'">등록</button>
								<!-- <button class="btn_up1 open1">엑셀업로드</button> -->
								<button class="btn_down1" onClick="excelDown();">엑셀다운로드</button>
							</div>
							<!-- e_btnwrap_s2_s2 -->

						</div>
						<!-- e_btnwrap_s2 -->


						<table class="mlms_tb c1">
							<thead>
								<tr>
									<th class="th01 bd01 wn1">no</th>
									<th class="th01 bd01 wn3">
										<div class="th_wrap pd1">항목명</div>
										<button name="cpx_name" class="down order">▼</button>
									</th>
									<th class="th01 bd01 wn2">
										<div class="th_wrap pd1">등록일</div>
										<button name="reg_date" class="down order">▼</button>
									</th>
									<th class="th01 b_1 wn2 pd1">
										<div class="th_wrap pd1">관리</div>
									</th>
								</tr>
							</thead>
							<tbody id="codeListAdd">
															
							</tbody>
						</table>

					</div>
					<!-- e_wrap_wrap -->

					<!-- s_pagination -->
					<div class="pagination" id="pagingBtnAdd">
						<ul>
							<li><a href="#" title="처음" class="arrow bba"></a></li>
							<li><a href="#" title="이전" class="arrow ba"></a></li>
							<li><a href="#" class="active">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#">6</a></li>
							<li><a href="#">7</a></li>
							<li><a href="#">8</a></li>
							<li><a href="#">9</a></li>
							<li><a href="#">10</a></li>
							<li><a href="#" title="다음" class="arrow na"></a></li>
							<li><a href="#" title="맨끝" class="arrow nna"></a></li>
						</ul>
					</div>
					<!-- e_pagination -->

				</div>
				<!-- e_adm_content2n -->

			</div>
			<!-- e_main_con -->

		</div>
		<!-- e_contents -->
	</div>
	<!-- e_container_table -->

</body>
</html>