<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>	
		<style>
			.wrap1_uselectbox1{width: 419px;padding: 0 !important;margin: 0;max-height: 40px;height: 40px !important;line-height: 40px;box-sizing: border-box;display: inline-block;border: solid 1px rgba(83, 173, 243, 1);background: rgba(249, 253, 255, 1);float: left;}
			.wrap1_uselectbox1:hover{border: solid 1px rgba(0, 171, 216, 1);background:rgba(227, 246, 255, 1);color:rgba(1, 96, 121,1);box-sizing: border-box;transition:.7s;}	
			.wrap1_uselectbox1 div.uselectbox{position: relative;top: 0;height: 3px;left: 0;display:inline-block;width:100%;cursor:pointer;text-align:left;clear:both;float:left;margin: 0;padding: 0;border: 0px;box-sizing: border-box;}	
			.wrap1_uselectbox1 span.uselected{width: 90%;background: rgba(255, 255, 255, 0);overflow:hidden;position:relative;top: 0px;float:left;height: 40px;line-height: 38px;font-size: 15px;z-index:1;color: rgba(36, 116, 185, 1);text-align:left;text-indent: 15px;padding:0;margin: 0;box-sizing: border-box;}	
			.wrap1_uselectbox1 span.uarrow{position:relative;top: 0;text-indent: 0;display: inline-block;width: 10%;float: right;height: 39px;line-height: 39px;z-index:1;box-sizing: border-box;text-align: left;font-size: 11px;background: rgba(255, 255, 255, 0);padding: 0;margin: 0;color: rgba(119, 181, 234, 1);}			
			.wrap1_uselectbox1 div.uoptions{position:absolute;top: 38px;left: -1px;width: 100%;height: auto;max-height: 145px;line-height: 38px;border: solid 1px rgba(83, 173, 243, 1);border-bottom-right-radius:5px;border-bottom-left-radius:5px;overflow-x: hidden;overflow-y: auto;background: rgb(236, 246, 255);padding: 0;display:none;color:rgba(1, 96, 121,1);opacity: 1;box-sizing: content-box;z-index: 9999;}	
			.wrap1_uselectbox1 span.uoption{position:relative;top: 0px;left:-1px;display:block;width: 100%;height: 40px;line-height: 40px;font-size: 15px;margin: -2px 0 0 0;padding:0 1px 0 1px;text-align:left;text-indent: 15px;border-bottom: solid 1px rgba(255, 255, 254, 1);opacity: 1;color: rgba(85, 85, 85, 1);}	
			.wrap1_uselectbox1 span.uoption:hover{color:#fff;background: rgba(117, 161, 216, 1);transition:.7s;}
		</style>	
		<script>
			$(document).ready(function() {
				bindPopupEvent("#m_pop1", ".open1");

				getAcademicTitle();
				getStList();
				getSoosiGradeList();
			});
			
			function getAcademicTitle(){
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicReg/titleInfo",
		            data: {                      
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		if(!isEmpty(data.acaInfo.academic_name))
		                		$("#academicNameTitle").text(data.acaInfo.academic_name);
		                	else
		                		$("#academicNameTitle").hide();
		            	
		            		var aca_state = "";
		                	switch(data.acaInfo.aca_state){
	                            case "01" : aca_state = "개설(비공개)";break;
	                            case "02" : aca_state = "개설(공개)";break;
	                            case "03" : aca_state = "학사 중(수업 중)";break;
	                            case "04" : aca_state = "학사종료(미확정)";break;
	                            case "05" : aca_state = "학사종료(성적산정)";break;
	                            case "06" : aca_state = "학사종료(성적발표)";break;
	                            case "07" : aca_state = "성적발표 종료(미확정)";break;
	                            case "08" : aca_state = "학사최종종료(확정)";break;                            
	                        }
			            	$("#academicState").text(aca_state); //타이틀 학사상태
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            }                  
		        });
			}
			
			function gradeNumCheck(obj){				
				
				if(isNaN(obj.value) || isEmpty(obj.value)){
					$(obj).val("");
					$(obj).focus("");
				}else{			
					$(obj).val($(obj).val().replace(/ /g,""));
				}

				var question_cnt = 0;
				
				var src_seq = $(obj).closest("td").attr("name");
				
				question_cnt = parseInt($(obj).closest("tr").find("td[name="+src_seq+"] input[name=question_cnt]").val());
								
				if(question_cnt < parseInt($(obj).val())){
					alert("총문항 보다 정답수가 클 수 없습니다.");
					$(obj).val("");
					$(obj).focus();
					return;
				}				
			}			

			function getSoosiGradeList(){
					
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/pf/lesson/soosiGrade/create/gradelist",
		            data: {
		            	"curr_seq" : $("#curr_seq").attr("data-value")
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	var htmls = '<span class="uoption" data-value="">신규 등록</span>';
		           		$.each(data.soosiList, function(index){
							htmls += '<span class="uoption" data-value="'+this.src_seq+'">'+this.src_name+'</span>';
		           		});         
		           		$("#srcSeqListAdd").html(htmls);
		            },
		            error: function(xhr, textStatus) {
		                alert("오류가 발생했습니다.");
		            }
		        });
			}			
			
			function getStList() {
	
				var curr_seq = $("#curr_seq").attr("data-value");
				
				if(isEmpty(curr_seq)){
					alert("교육과정을 선택해주세요");
					return;
				}
				
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academicReg/soosiScore/create/list",
		            data: {
		            	"st_id" : $("#st_id").val()
		            	,"curr_seq" : curr_seq
		            	,"st_name" : $("#st_name").val()            	
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	
		            	if(data.status=="200"){
		            		$("#scoreListAdd").empty();
		            		$("#stListAdd").empty();
			            	$("span.g_num").text(data.stList.length);
			            	var defaultTitleHtml = "";
			            	var srNoHtml = "";
			            	var srNameHtml = "";
			            	var srDateHtml = "";
			            	var srInfoHtml = "";
			            		
			            	defaultTitleHtml='<tr>';	
							srNameHtml='<tr>';
							srDateHtml='<tr>';
							srInfoHtml='<tr>';
							
							$.each(data.soosiList, function(index){
								defaultTitleHtml+='<th colspan="5" class="color5 th01 ths2 b_1 br1 w5n">'+(index+1)+'차</th>';
								srNameHtml+='<th colspan="5" class="color5 th01 ths2 b_1 br1 w5n">'+this.src_name+'</th>';
								srDateHtml+='<th colspan="5" class="color5 th01 ths2 b_1 br1 w5n">'+this.src_date+'</th>';
								srInfoHtml+='<th class="color5 th01 ths1 b_1 w1n">총문항</th>'
									+'<th class="color5 th01 ths1 b_1 w1n">정답수</th>'
									+'<th class="color5 th01 ths1 b_1 w1n">점수</th>'
									+'<th class="color5 th01 ths1 b_1 w1n">평균</th>'
									+'<th class="color5 th01 ths1 b_1 br1 w1n">편차</th>';							
							});						
							
							defaultTitleHtml+='</tr>';
							srNameHtml+='</tr>';
							srDateHtml+='</tr>';
							srInfoHtml+='</tr>';
			            
							$("#titleListAdd").html(defaultTitleHtml+srNameHtml+srDateHtml+srInfoHtml);
		
							var htmls = "";
							$.each(data.stList , function(index){
								var picture = this.picture_path;
								if(isEmpty(picture))
									picture = "${IMG}/ph_3.png";
								else
									picture = "${RES_PATH}"+this.picture_path;
									
								htmls='<tr name="'+this.id+'">'
								+'<td class="td_1">'+(index+1)+'<input type="hidden" name="user_seq" value="'+this.user_seq+'"></td>'
								+'<td class="td_1">'+this.id+'</td>'
								+'<td class="td_1 t_l">'
								+'<span class="a_mp">'
								+'<span class="pt01">'
								+'<img src="'+picture+'" alt="등록된 사진 이미지" class="pt_img">'
								+'</span>'
								+'<span class="ssp1">'+this.name+'</span>'
								+'</span>'
								+'</td>'
								+'<td class="td_1">'+this.group_number+'</td></tr>';
								
								$("#stListAdd").append(htmls);
								htmls='<tr name="'+this.id+'">';
								$.each(data.soosiList, function(){
									htmls+='<td class="td_1" name="'+this.src_seq+'"><input type="text" class="ip03" readonly name="question_cnt" value=""></td>'
										+'<td class="td_1" name="'+this.src_seq+'"><input type="text" class="ip03" name="answer_cnt" onKeyup="gradeNumCheck(this);" value=""></td>'
										+'<td class="td_1" name="'+this.src_seq+'"><input type="text" class="ip03" name="score" onKeyup="onlyNumCheck(this);" value=""></td>'
										+'<td class="td_1" name="'+this.src_seq+'"><input type="text" class="ip03" readonly name="avg_score" value=""></td>'
										+'<td class="td_1 br1" name="'+this.src_seq+'"><input type="text" class="ip03" readonly name="deviation" value=""></td>';								
								});
								htmls+="</tr>";

								$("#scoreListAdd").append(htmls);
							});
							
							$.each(data.scoreList, function(index){
								$("#scoreListAdd tr[name="+this.id+"]").find("td[name="+this.src_seq+"]").find("input[name=question_cnt]").val(this.question_cnt);
								$("#scoreListAdd tr[name="+this.id+"]").find("td[name="+this.src_seq+"]").find("input[name=answer_cnt]").val(this.answer_cnt);
								$("#scoreListAdd tr[name="+this.id+"]").find("td[name="+this.src_seq+"]").find("input[name=score]").val(this.score);
								$("#scoreListAdd tr[name="+this.id+"]").find("td[name="+this.src_seq+"]").find("input[name=avg_score]").val(this.avg_score);
								$("#scoreListAdd tr[name="+this.id+"]").find("td[name="+this.src_seq+"]").find("input[name=deviation]").val(this.deviation);
							});
		            	}
		            },
		            error: function(xhr, textStatus) {
		                alert("오류가 발생했습니다.");
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            } 
		        });
			}
			
			function submitGrade(){
				
				if(isEmpty($("#curr_seq").attr("data-value"))){
					alert("성적 등록할 교육과정을 선택해주세요");
					return;
				}
				
				if(!confirm("저장하시겠습니까?"))
					return;
				
				var arrayList = new Array();
				
				$.each($("#scoreListAdd tr"), function(index){
					var listInfo = new Object();
					var id = $(this).attr("name");
					var user_seq = $("#stListAdd tr:eq("+index+")").find("input[name=user_seq]").val();
					var src_seq = "";
					var answer_cnt = "";
					var score = "";
					var src_length = $(this).find("input[name=answer_cnt]").length;
					$.each($(this).find("input[name=answer_cnt]"), function(dIndex){
						src_seq += $(this).closest("td").attr("name");
						answer_cnt += $(this).val(); 
						score += $(this).closest("tr").find("td[name="+$(this).closest("td").attr("name")+"]").find("input[name=score]").val();
						if(src_length != (dIndex+1)){
							src_seq+="|";
							answer_cnt+="|";
							score+="|";
						}						
					});
					
					listInfo.user_seq=user_seq;
					listInfo.src_seq=src_seq;
					listInfo.answer_cnt=answer_cnt;
					listInfo.score=score;
					arrayList.push(listInfo);					
				});

				var scoreList = new Object();
				scoreList.list = arrayList;
				
				var jsonInfo = JSON.stringify(scoreList);

				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academicReg/soosiScore/create/insert",
		            data: {   
		            	"scorelist" : jsonInfo
		            	,"curr_seq" : $("#curr_seq").attr("data-value")
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status == "200"){
		            		getStList();
			            	alert("저장 완료 되었습니다.");	            	
		            	}else{
		            		alert("저장 실패 하였습니다.");
		            	}
		            },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                //document.write(xhr.responseText);
		            }
		        }); 
			}
						
			function file_nameChange(){    
			    if($("#xlsFile").val() != ""){
			        var fileValue = $("#xlsFile").val().split("\\");
			        var fileName = fileValue[fileValue.length-1]; // 파일명
			        
			        var fileLen = fileName.length; 
			        var lastDot = fileName.lastIndexOf('.'); //마지막 . 위치 가져온다.
			        var fileExt = fileName.substring(lastDot+1, fileLen).toLowerCase(); //마지막 . 위치로 확장자 자름

			        if(fileExt != "xlsx") {
			        	alert("xlsx 파일을 등록해주세요");
			        	$("#xlsFile").val("");
			        	return;			        	
			        }
			        
			        $("#xls_filename").val(fileName);
			    }
			}
			
			
			function excelSubmit() {
				if(isEmpty($("#pop_curr_seq").attr("data-value"))){
					alert("성적 등록할 교육과정을 선택해주세요");
					return;
				}

				if (isEmpty($("#xlsFile").val())) {
					alert("파일을 선택해주세요");
					return;
				}
				var form = document.getElementById("xlsForm");
				
				var hiddenField = document.createElement("input");
				
				hiddenField.type = "hidden";
				hiddenField.name = "curr_seq";
				hiddenField.value = $("#pop_curr_seq").attr("data-value");
				
				var srcSeqField = document.createElement("input");
				
				srcSeqField.type = "hidden";
				srcSeqField.name = "src_seq";
				srcSeqField.value = $("#src_seq").attr("data-value");
				
				form.appendChild(hiddenField);
				form.appendChild(srcSeqField);
				
				$("#xlsForm").ajaxForm({
					type : "POST",
					url : "${HOME}/ajax/admin/academicReg/soosiScore/excel/create",
					dataType : "json",
					success : function(data, status) {
						if (data.status == "200") {
							alert("엑셀업로드가 완료 되었습니다.");
							$("#xlsFile").val("");
							$("#xls_filename").val("");
							getStList();
						} else {
							alert(data.status);
						}
					},
					error : function(xhr, textStatus) {
						alert(textStatus);
						//document.write(xhr.responseText);
						$.unblockUI();
					},
					beforeSend : function() {
						$.blockUI();
					},
					complete : function() {
						$.unblockUI();
					}
				});
				$("#xlsForm").submit();
			}
			
			function excelUp(){
				$("#pop_curr_seq").attr("data-value",$("#curr_seq").attr("data-value"));
				$("#pop_curr_seq").text($("#curr_seq").text());
				
			}
			
			function excelTmplDown(){
				if(isEmpty($("#pop_curr_seq").attr("data-value"))){
					alert("양식다운로드할 교육과정을 선택하세요");
					return;
				}
				location.href='${HOME}/admin/academic/academicReg/soosiScore/TmplExcelDown?curr_seq='+$("#pop_curr_seq").attr("data-value");
			}
			
			function getAcademic(){
		    	post_to_url('${HOME}/admin/academic/academicReg/academicRegBasicInfo/create', {"aca_seq" : "${S_ACA_SEQ}"});
		    }
			
			function excelDown(){
				var gradeArray = new Array();
				var currArray = new Array();
				
				$.each($("#stListAdd tr"), function(index){
					var listInfo = new Object();
					listInfo.id = $(this).find("td").eq(1).text();
					listInfo.name = $(this).find("td").eq(2).text();
					listInfo.groupNum = $(this).find("td").eq(3).text();
					var score = "";
					
					//학생꺼 점수들 가져온다.
					$.each($("#scoreListAdd tr:eq("+index+") td"), function(sIndex){
						score += $(this).find("input").val();
						
						//마지막 데이터 빼고 구분자 넣어준다.
						if($("#scoreListAdd tr:eq("+index+") td").length != (sIndex+1))
							score += "|";
					});
					listInfo.grade = score;
					gradeArray.push(listInfo);							
				});

				var no = "";
				var curr_name = "";
				var srcDate = "";
				$.each($("#titleListAdd tr:eq(0) th"), function(index){
					no += $(this).text();
					
					if($("#titleListAdd tr:eq(0) th").length != (index+1))
						no += "|";
				});
				
				$.each($("#titleListAdd tr:eq(1) th"), function(index){
					curr_name += $(this).text();
					
					if($("#titleListAdd tr:eq(1) th").length != (index+1))
						curr_name += "|";
				});
				
				$.each($("#titleListAdd tr:eq(2) th"), function(index){
					srcDate += $(this).text();
					
					if($("#titleListAdd tr:eq(2) th").length != (index+1))
						srcDate += "|";
				});
				
				var scoreList = new Object();
				scoreList.list = gradeArray;
				
				var jsonInfo = JSON.stringify(scoreList);
				
				post_to_url("${HOME}/admin/academic/academicReg/soosiScore/create/excelDown", {"stList":jsonInfo, "no":no, "curr_name" : curr_name, "srcDate":srcDate});			
			}
			
			function pageMoveScore(type, aca_seq){
		    	$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/admin/academic/academicReg/setAcaSeq",
	                data: {
	                	"aca_seq" : aca_seq
	                    },
	                dataType: "json",
	                success: function(data, status) {
	                	if(type=="curr")
	               			location.href="${HOME}/admin/academic/academicReg/currScore";
		     			else if(type=="soosi")
		     				location.href="${HOME}/admin/academic/academicReg/soosiScore";
		     			else if(type=="fe")
		     				location.href="${HOME}/admin/academic/academicReg/feScore";
	                },
	                error: function(xhr, textStatus) {
	                    alert("실패");
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                },beforeSend:function() {
	                	$.blockUI();
	                },
	                complete:function() {
	                	$.unblockUI();
	                }
	            });
		    }
			
		</script>
	</head>
	
	<body>
		<input type="hidden" name="aca_seq" id="aca_seq" value="${S_ACA_SEQ }">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
		<!-- s_container_table -->
		<div id="container" class="container_table">
			<!-- s_contents -->
			<div class="contents main">
	
				<!-- s_left_mcon -->
				<div class="left_mcon aside_l grd">
					<div class="sub_menu adm_grd">
						<div class="title">학사관리</div>
						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">학사체계관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">학사코드관리</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">교육과정관리</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">교육과정관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청관리</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on" onClick="location.href='${HOME}/admin/academic/academicReg/list'">학사등록관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">졸업역량관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- e_left_mcon -->
	
				<!-- s_main_con -->
				<div class="main_con adm_grd">
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<!-- s_tt_wrap -->
					<div class="tt_wrap">
						<h3 class="am_tt">
							<span class="tt">학사등록관리<span class="sign1"> &gt; </span>수시성적관리<span class="sign1"> &gt; </span>성적수정</span>
						</h3>
						<span class="sp_state">학사상태 : <span class="tt" id="academicState"></span></span>
						<h4 class="h4_tt" id="academicNameTitle"></h4>
					</div>
					<!-- e_tt_wrap -->
	
					
						<!-- s_adm_content3 -->
					<div class="adm_content3 mpf_tabcontent4">
						<!-- s_tt_wrap -->	
						<!-- s_wrap_wrap -->
						<div class="wrap_wrap">	
							<div class="tab_wrap_cc">							
								<button class="tab01 tablinks" onclick="getAcademic();">기본정보</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/curriculumAssign'">교육과정배정</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/studentAssign'">학생배정</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/schedule'">시간표</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/SFSurvey/list'">만족도조사</button>
								<button class="tab01 tablinks" onclick="pageMoveScore('curr','${S_ACA_SEQ }');">종합성적</button>
								<button class="tab01 tablinks active" onclick="pageMoveScore('soosi','${S_ACA_SEQ }');">수시성적</button>								
							<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/currReport'">운영보고서</button>	
										
							</div>
							<!-- s_sch_wrap -->
							<div class="sch_wrap f1">
								<!-- s_rwrap -->
								<div class="rwrap">
									<!-- s_wrap_s -->
									<div class="wrap_s">
										<div class="wrap_s1_uselectbox">
											<div class="uselectbox">
												<span class="uselected" data-value="${curr_seq }" id="curr_seq">${curr_name }</span> <span class="uarrow">▼</span>
												<div class="uoptions" style="display: none;width:300px;">													
													<c:forEach var="currList" items="${currList}">
														<span class="uoption" data-value="${currList.curr_seq}">${currList.curr_name}</span>
					                                </c:forEach>
												</div>
											</div>
										</div>
										<button class="btn_tt1" onClick="getStList()">검색</button>
	
										<!-- s_r_wrap -->
										<div class="r_wrap mdf">
											<div class="sttwrap">
												<span class="sp01">총</span><span class="sp_n">${fn:length(currList)}</span>
												<span class="sp02">건의 교육과정 배정</span>
											</div>
										</div>
										<!-- e_r_wrap -->
									</div>
									<!-- e_wrap_s -->
	
								</div>
								<!-- e_rwrap -->
							</div>
							<!-- e_sch_wrap -->
	
							<!-- s_tt_wrap -->
							<div class="tt_wrap3">
								<button class="btn_up1 open1" onClick="excelUp();">엑셀일괄등록</button>
								<button class="btn_dw3" onClick="excelDown();">성적 다운로드</button>
	
								<div class="sch_wrap">
    								<input type="text" class="ip_search2" placeholder="학번" id="st_id" onkeypress="javascript:if(event.keyCode==13){getStList(); return false;}"> 
									<input type="text" class="ip_search1" placeholder="학생 이름" id="st_name" onkeypress="javascript:if(event.keyCode==13){getStList(); return false;}">
									<button class="btn_search1" onClick="getStList();"></button>
								</div>
							</div>
							<!-- e_tt_wrap -->
	
							<!-- s_tt_wrap -->
							<div class="tt_wrap1">
								<span class="tts5">성적 수정은 엑셀일괄등록이나 수정 버튼을 눌러 수정할 수 있습니다.</span>
	
								<div class="tt_g1">
									<span class="g_t2">검색결과</span><span class="g_num"></span><span
										class="g_t3">명</span>
								</div>

							<div class="scroll_wraps2">
								<div class="fwrap3">
									<table class="mlms_tb1 n2">
										<thead>
											<tr>
												<th class="color1 th01 b_2 w2n">No.</th>
												<th class="color1 th01 bd01 w4n">학번</th>
												<th class="color1 th01 bd01 w4n">학생</th>
												<th class="color1 th01 bd01 w3n">조</th>
											</tr>
										</thead>
										<tbody id="stListAdd">
										</tbody>
									</table>
								</div>
								<div class="fwrap4">
									<table class="mlms_tb1 n2">
										<thead id="titleListAdd">
											<tr>
												<th colspan="5" class="color5 th01 ths2 b_1 br1 w5n">1차</th>
												<th colspan="5" class="color5 th01 ths2 b_1 br1 w5n">2차</th>
												<th colspan="5" class="color5 th01 ths2 b_1 br1 w5n">3차</th>
												<th colspan="5" class="color5 th01 ths2 b_1 br1 w5n">4차</th>
											</tr>
											<tr>
												<th colspan="5" class="color5 th01 ths2 b_1 br1 w5n">수시 시험명</th>
												<th colspan="5" class="color5 th01 ths2 b_1 br1 w5n">수시 시험명</th>
												<th colspan="5" class="color5 th01 ths2 b_1 br1 w5n">수시 시험명</th>
												<th colspan="5" class="color5 th01 ths2 b_1 br1 w5n">수시 시험명</th>
											</tr>
											<tr>
												<th colspan="5" class="color5 th01 ths2 b_1 br1 w5n">2018.03.15</th>
												<th colspan="5" class="color5 th01 ths2 b_1 br1 w5n">2018.04.15</th>
												<th colspan="5" class="color5 th01 ths2 b_1 br1 w5n">2018.05.15</th>
												<th colspan="5" class="color5 th01 ths2 b_1 br1 w5n">2018.06.15</th>
											</tr>
											<tr>
												<th class="color5 th01 ths1 b_1 w1n">총문항</th>
												<th class="color5 th01 ths1 b_1 w1n">정답수</th>
												<th class="color5 th01 ths1 b_1 w1n">점수</th>
												<th class="color5 th01 ths1 b_1 w1n">평균</th>
												<th class="color5 th01 ths1 b_1 br1 w1n">편차</th>
											</tr>
										</thead>
										<tbody id="scoreListAdd">
											<tr>
												<td class="td_1"><input type="text" class="ip03" value="100"></td>
												<td class="td_1"><input type="text" class="ip03" value="97"></td>
												<td class="td_1"><input type="text" class="ip03" value="97"></td>
												<td class="td_1"><input type="text" class="ip03" value="85"></td>
												<td class="td_1 br1"><input type="text" class="ip03" value="7.02"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>

							<div class="bt_wrap adm">
								<button class="bt_2" onclick="submitGrade();">수정</button>
								<button class="bt_3" onclick="pageMoveScore('soosi','${S_ACA_SEQ }');">취소</button>
							</div>
	
						</div>
						<!-- e_wrap_wrap -->
	
	
					</div>
					<!-- e_adm_content3 -->
				</div>
			</div>
		</div>	
	</div>		
		
		<!-- s_ 엑셀 일괄 등록 팝업 -->
	<div id="m_pop1" class="pop_up_ex pop_up_ex_1 mo1">

		<div class="pop_wrap">
			<p class="popup_title">교육과정 성적관리 엑셀업로드</p>

			<button class="pop_close close1" type="button">X</button>

			<button class="btn_exls_down_2" onclick="javascript:excelTmplDown();">엑셀 양식 다운로드</button>

			<div class="t_title_1">
				<span class="sp_wrap_1">엑셀 양식을 다운로드 하시면, 해당 교육과정의 학생 리스트가
					자동으로 기입된 양식을 확인하실 수 있습니다.<br>해당 학생의 과목별 점수를 기입하셔서 업로드 해 주세요
				</span>
			</div>
			
			<div class="sub_fwrap_ex_1" style="display:none;">
				<span class="ip_tt">교육과정선택</span>
				<div class="wrap2_3_uselectbox grd">
					<div class="uselectbox">
						<span class="uselected" id="pop_curr_seq" data-value=""></span> <span class="uarrow">▼</span>
						<div class="uoptions" style="display: none;">
							<%-- <c:forEach var="currList" items="${currList}">
								<span class="uoption" data-value="${currList.curr_seq}">${currList.curr_name}</span>
							</c:forEach> --%>
						</div>
					</div>
				</div>
			</div>
			
			<div class="pop_ex_wrap">
				<form id="xlsForm" onSubmit="return false;">
				<div class="sub_fwrap_ex_1">
					<span class="ip_tt">등록 구분</span>
					<div class="wrap1_uselectbox1">
						<div class="uselectbox" >
							<span class="uselected" id="src_seq" data-value="">신규 등록</span> <span class="uarrow">▼</span>
							<div class="uoptions" style="display: none;" id="srcSeqListAdd">
							</div>
						</div>
					</div>
				</div>
				<div class="sub_fwrap_ex_1">
						<span class="ip_tt">엑셀 파일</span> 
						<input type="text" readonly class="ip_sort1_1" id="xls_filename" value="" onClick="$('#xlsFile').click();">
						<button class="btn_r1_1" onClick="$('#xlsFile').click();">파일선택</button>         
						<input type="file" style="display:none;" id="xlsFile" name="xlsFile" onChange="file_nameChange();">					
				</div>
				</form>
				
				<span class="t_title_2">등록 가능한 파일확장자 : xlsx</span>

				<!-- s_t_dd -->
				<div class="t_dd">

					<div class="pop_btn_wrap">
						<button type="button" class="btn01" onclick="excelSubmit();">등록</button>
						<button type="button" class="btn02" onClick="$('.close1').click();">취소</button>
					</div>

				</div>
				<!-- e_t_dd -->

			</div>
		</div>
	</div>
	<!-- e_ 엑셀 일괄 등록 팝업 -->
	</body>
</html>