<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <script>
        
	        $(document).ready(function(){
	        	layerPopupCloseInit(['div.uoptions']); 

				acasystemStageOfList(1);
				
				getApplicationList(1);
	        });		   

	        $(document).on("click",".close1", function(){
	        	$("#xls_filename").val("");
	        	$("#xlsFile").val("");	        	
	        });
	        
	        function acaSystemSet(level, seq){
	       		if(level==1){
	       			if(isEmpty(seq)){
	       				$("#m_seq_add span").remove();
	                    $("#m_seq").attr("data-value","");
	                    $("#m_seq").text("중분류");
	       				$("#s_seq_add span").remove();
	                    $("#s_seq").attr("data-value","");
	                    $("#s_seq").text("소분류");	
	       			}else{
	       				acasystemStageOfList(2, seq);	
	       			}	       			
	       		}else if(level==2){
	       			if(isEmpty(seq)){
	       				$("#s_seq_add span").remove();
	                    $("#s_seq").attr("data-value","");
	                    $("#s_seq").text("소분류");	       				
	       			}else{
		       			acasystemStageOfList(3, $("#l_seq").attr("data-value"), seq);
	       			}	
	       		}else if(level==3){
	       			acasystemStageOfList(4, $("#l_seq").attr("data-value"), $("#m_seq").attr("data-value"), seq);
	       		}
	       	}
	        
            function acasystemStageOfList(level, l_seq, m_seq, s_seq){
            	
            	if(level == ""){
            		alert("분류 없음");
            		return;
            	}
            	$.ajax({
            		type: "POST",
            		url: "${HOME}/ajax/admin/academic/academicSystem/acaStageOfList",
            		data: {
            			"level" : level,
            			"l_seq" : l_seq,                	   
            			"m_seq" : m_seq,
            			"s_seq" : s_seq
            			},
           			dataType: "json",
           			success: function(data, status) {
           				var cate = "";
           				if(level == 1)
           					cate = "대분류";
           				else if(level == 2)
           					cate = "중분류";
           				else if(level == 3)
           					cate = "소분류";
           				
           				
           				var htmls = '<span class="uoption" onClick="acaSystemSet('+level+',\'\');" data-value="">'+cate+'</span>';
           				$.each(data.list, function(index){
           					htmls +=  '<span class="uoption" onClick="acaSystemSet('+level+','+this.aca_system_seq+');" data-value="'+this.aca_system_seq+'">'+this.aca_system_name+'</span>';
           				});

           				if(level == 1){
           					$("#l_seq_add span").remove();
           					$("#l_seq_add").html(htmls);        
           				}	                   
           				else if(level == 2){
           					$("#m_seq_add span").remove();
           					$("#m_seq_add").html(htmls);         
           				}
           				else if(level == 3){
           					$("#s_seq_add span").remove();
           					$("#s_seq_add").html(htmls);
        				}
        			},
        			error: function(xhr, textStatus) {
        				//alert("오류가 발생했습니다.");
        				document.write(xhr.responseText);
       				},beforeSend:function() {
  					},
  					complete:function() {
    				}
     			}); 
		   }
		   		   
            
		   function reset(){
			   $("#l_seq").html("전체");
               $("#l_seq").attr("data-value","");
               $("#m_seq").html("전체");
               $("#m_seq").attr("data-value","");
               $("#s_seq").html("전체");
               $("#s_seq").attr("data-value","");
               $("#year").html("전체");
               $("#year").attr("data-value","");
               $("input[name=aca_name]").val("");
               getApplicationList(1);
		   }
		   
		   function pageMoveView(ca_seq){
			   post_to_url('${HOME}/admin/academic/courseApplication/view', {'ca_seq': ca_seq});
		   }
		   
		   function getApplicationList(page){			   
			   $.ajax({
                   type: "POST",
                   url: "${HOME}/ajax/admin/academic/courseApplication/list",
                   data: {
                	   "year" : $("#year").attr("data-value"),
                       "l_seq" : $("#l_seq").attr("data-value"),                       
                       "m_seq" : $("#m_seq").attr("data-value"),
                       "s_seq" : $("#s_seq").attr("data-value"),
                       "academic_name" : $("input[name=aca_name]").val(),
                       },
                   dataType: "json",
                   success: function(data, status) {
                	   $("#caListAdd").empty();   

                       var htmls = '';
                       $.each(data.list, function(index){
                           var ap_num = "";
                           var end_date = "";
                           
                           if(isEmpty(this.application_num))
                        	   ap_num = this.st_cnt;
                           else
                        	   ap_num = this.st_cnt + "/" + this.application_num;
                           
                           if(this.magam_date == 0)
                        	   end_date = "오늘까지 ("+this.accept_end_date_mm_dd+")";
                           else if(this.magam_date > 0)
                        	   end_date = "마감"+this.magam_date+"일전("+this.accept_end_date_mm_dd+")";
                           else
                        	   end_date = "마감됨("+this.accept_end_date_mm_dd+")";
                           
                    	   htmls += '<tr class="addtr">'
                    	   +'<td class="td_1">'+this.year+'</td>'
                    	   +'<td class="td_1">'+this.laca_system_name+'</td>'
                    	   +'<td class="td_1">'+this.maca_system_name+'</td>'
                    	   +'<td class="td_1">'+this.saca_system_name+'</td>'
                    	   +'<td class="td_1">'+this.aca_system_name+'</td>'
                    	   +'<td class="td_1">'+this.academic_name+'</td>'
                    	   +'<td class="td_1"><a href="#" class="en_num" title="교육과정명">'+this.curr_name+'</a></td>'
                    	   +'<td class="td_1"><a href="#" class="en_num" title="신청자수">'+ap_num+'</a></td>'
                    	   +'<td class="td_1"><a href="#" class="en_num" title="D-day">'+end_date+'</a></td>'
                    	   +'<td class="td_1">'
                    	   +'<button class="btn2_2" onclick="pageMoveView('+this.ca_seq+');">조회</button>'
                    	   +'</td>'
                    	   +'</tr>';
                       }); 
                       $("#caListAdd").html(htmls);
                       $("#pagingBtnAdd").html(data.pageNav);
                   },
                   error: function(xhr, textStatus) {
                	   alert("실패");
                       //alert("오류가 발생했습니다.");
                       document.write(xhr.responseText);
                   },beforeSend:function() {
                   },
                   complete:function() {
                   }
               }); 
		   }
		   
		   //교육과정 삭제하기
		   function currOneDelete(curr_seq){
				   
			   if(confirm("삭제하시겠습니까?")){
				   $.ajax({
	                   type: "POST",
	                   url: "${HOME}/ajax/admin/academic/curriculum/delete",
	                   data: {
	                       "curr_seqs" : curr_seq
	                       },
	                   dataType: "json",
	                   success: function(data, status) {
	                	   if(data.status == 200){
	                		   alert("삭제 완료 되었습니다.");
	                		   getCurriculumList(1);
	                           $(".order").removeClass("up");
	                           $(".order").html("▼");
	                	   }else if(data.status == 201){
	                		   alert("교육과정명 : " + data.msg + " \n배정 이력이 있어 삭제가 불가능합니다.");   
	                	   }else{
	                		   alert("삭제 실패 하였습니다.");
	                	   }
	                   },
	                   error: function(xhr, textStatus) {
	                       alert("실패");
	                       //alert("오류가 발생했습니다.");
	                       document.write(xhr.responseText);
	                   },beforeSend:function() {
	                   },
	                   complete:function() {
	                   }
	               }); 
			   }
		   }
		   		   
		   function file_nameChange(){    
			    if($("#xlsFile").val() != ""){
			        var fileValue = $("#xlsFile").val().split("\\");
			        var fileName = fileValue[fileValue.length-1]; // 파일명
			        
			        var fileLen = fileName.length; 
			        var lastDot = fileName.lastIndexOf('.'); //마지막 . 위치 가져온다.
			        var fileExt = fileName.substring(lastDot+1, fileLen).toLowerCase(); //마지막 . 위치로 확장자 자름

			        if(fileExt != "xlsx") {
			        	alert("xlsx 파일을 등록해주세요");
			        	$("#xlsFile").val("");
			        	return;			        	
			        }
			        
			        $("#xls_filename").val(fileName);
			    }
			}
		   

		   function xlsUp(){
		       if (isEmpty($("#xlsFile").val())) {
		           alert("파일을 선택해주세요");
		           return;
		       }
		       		       
		       $("#xlsForm").ajaxForm({
		           type: "POST",
		           url: "${HOME}/ajax/admin/curriculum/excel/create",
		           dataType: "json",
		           success: function(data, status){
		        	  
		               if (data.status == "200") {
                           alert("저장 완료 되었습니다.");
                           $("#xlsFile").val("");
                           $("#xls_filename").val("");
                           $('.avgrund-overlay').trigger('click');
                           getCurriculumList(1);
		               }else{
		            	   alert(data.status);
		               }
		           },
		           error: function(xhr, textStatus) {
		               alert(textStatus);
		               //document.write(xhr.responseText);
                       $.unblockUI();
		           },
		           beforeSend:function() {
		               $.blockUI();
		           },
		           complete:function() {
		               $.unblockUI();
		           }
		       }); 
		       $("#xlsForm").submit();
		   }

		   function curriculumXlsxDown(){
		       location.href="${HOME}/admin/academic/curriculum/curriculumListXlxsDown?l_seq="+$("#l_seq").attr("value") +                       
               "&m_seq=" + $("#m_seq").attr("value") +
               "&s_seq=" + $("#s_seq").attr("value") +
               "&curr_code=" + $("#curr_code").val().trim() +
               "&curr_name=" + $("#curr_name").val().trim() +
               "&mpf_name=" + $("#mpf_name").val().trim() +
               "&complete_code=" + $("#complete_code").attr("value") +
               "&administer_code=" + $("#administer_code").attr("value") +
               "&target_code=" + $("#target_code").attr("value")
		       
		   }
		</script>
	</head>

	<body>
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
			<!-- s_container_table -->
			<div id="container" class="container_table">
				<!-- s_contents -->
				<div class="contents main">
	
					<!-- s_left_mcon -->
					<div class="left_mcon aside_l grd">
						<div class="sub_menu adm_grd">
							<div class="title">학사관리</div>
							<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">학사체계관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">학사코드관리</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on">교육과정관리</span>
							</div>
							<ul class="panel on">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">교육과정관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2 on">수강신청관리</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicReg/list'">학사등록관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">졸업역량관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
						</div>
					</div>
					<!-- e_left_mcon -->
	
					<!-- s_main_con -->
					<div class="main_con adm_grd">
						<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
						<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
	
						<!-- s_tt_wrap -->
						<div class="tt_wrap">
							<h3 class="am_tt"><span class="tt">수강신청관리</span></h3>
						</div>
						<!-- e_tt_wrap -->
	
								<!-- s_adm_content2 -->
						<div class="adm_content2">
							<!-- s_tt_wrap -->
		
							<!-- s_wrap_wrap -->
							<div class="wrap_wrap">
								<!-- s_sch_wrap -->
								<div class="sch_wrap">
									<!-- s_rwrap -->
									<div class="rwrap">
										<!-- s_wrap_s -->
										<div class="wrap_s a1">
											<span class="tt2">년도</span>
		
											<div class="wrap_s1_uselectbox">
												<div class="uselectbox">
													<span class="uselected" id="year" data-value="">선택</span> <span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;">
														<c:forEach var="academicYearList" items="${academicYearList}">
		                                                    <span class="uoption" data-value="${academicYearList.year}">${academicYearList.year}</span>
		                                                </c:forEach>
													</div>
												</div>
											</div>
				
											<span class="tt2">학사명</span> 
											<input class="ip_tt" value="" name="aca_name" type="text" style="width:52%;">
										</div>
										<!-- e_wrap_s -->
		
										<!-- s_wrap_s -->
										<div class="wrap_s a1">
											<span class="tt2">대분류</span>
		
											<div class="wrap_s1_uselectbox">
												<div class="uselectbox">
													<span class="uselected" id="l_seq" data-value="">전체</span> <span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;width:230px;" id="l_seq_add">
														<span class="uoption" data-value="">전체</span>
													</div>
												</div>
											</div>
		
											<span class="tt2">중분류</span>
		
											<div class="wrap_s1_uselectbox">
												<div class="uselectbox">
													<span class="uselected" id="m_seq" data-value="">전체</span><span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;width:230px;" id="m_seq_add">
														
													</div>
												</div>
											</div>
		
											<span class="tt2">소분류</span>
		
											<div class="wrap_s1_uselectbox">
												<div class="uselectbox">
													<span class="uselected" id="s_seq" data-value="">전체</span><span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;width:230px;" id="s_seq_add">
														
													</div>
												</div>
											</div>
										</div>
										<!-- e_wrap_s -->
		
										<!-- e_wrap_s -->
									</div>
									<!-- e_rwrap -->
								</div>
								<!-- e_sch_wrap -->
								<!-- s_btnwrap_s1f -->
								<div class="btnwrap_s1f">   
								      <button class="btn_tt1" onclick="location.href='${HOME}/admin/academic/courseApplication/notes'">유의사항 관리</button>
								      <button class="btn_tt2" onclick="location.href='${HOME}/admin/academic/courseApplication/refund'">환불규정 관리</button>
								</div>
								<!-- e_btnwrap_s1f --> 
								<!-- s_btnwrap_s1 -->
								<div class="btnwrap_s1">
									<button class="btn_tt1" onClick="getApplicationList(1);">검색</button>
									<button class="btn_tt2" onClick="reset();">초기화</button>
								</div>
								<!-- e_btnwrap_s1 -->
		
							</div>
							<!-- e_wrap_wrap -->
		
							<!-- s_wrap_wrap -->
							<div class="wrap_wrap">
								<!-- s_btnwrap_s2 -->
								<div class="btnwrap_s2">
									<!-- s_btnwrap_s2_s -->
									<div class="btnwrap_s2_s">
										<button class="btn_tt1_1" onclick="post_to_url('${HOME}/admin/academic/courseApplication/insert');">수강신청 등록</button>
										<button class="btn_up1 open1">엑셀업로드</button>
										<button class="btn_down1">엑셀다운로드</button>
									</div>
									<!-- e_btnwrap_s2_s -->
		
								</div>
								<!-- e_btnwrap_s2 -->
		
								<table class="mlms_tb curri">
									<thead>
										<tr>
											<th class="th01 bd01 wn1">년도</th>
											<th class="th01 bd01 wn2">대분류</th>
											<th class="th01 bd01 wn1">중분류</th>
											<th class="th01 bd01 wn1">소분류</th>
											<th class="th01 bd01 wn1">기간</th>
											<th class="th01 bd01 wn4">학사명</th>
											<th class="th01 bd01 wn3">수강신청<br>교육과정명</th>
											<th class="th01 bd01 wn2">신청자</th>
											<th class="th01 bd01 wn3">신청 D-day</th>
											<th class="th01 bd01 wn2">관리</th>
										</tr>
									</thead>
									<tbody id="caListAdd">
										
									</tbody>
								</table>
		
							</div>
							<!-- e_wrap_wrap -->
		
							<!-- s_pagination -->
							<div class="pagination" id="pagingBtnAdd">
								<ul>
									<li><a href="#" title="처음" class="arrow bba"></a></li>
									<li><a href="#" title="이전" class="arrow ba"></a></li>									
									<li><a href="#" title="다음" class="arrow na"></a></li>
									<li><a href="#" title="맨끝" class="arrow nna"></a></li>
								</ul>
							</div>
							<!-- e_pagination -->
		
						</div>
						<!-- e_adm_content2 -->
					</div>
					<!-- e_main_con -->
	
				</div>
				<!-- e_contents -->
			</div>
			<!-- e_container_table -->
	
	
		<!-- s_ 엑셀 일괄 등록 팝업 -->
		<div id="m_pop1" class="pop_up_ex pop_up_ex_1 mo1">
	
			<div class="pop_wrap">
				<p class="popup_title">교육과정 성적관리 엑셀업로드</p>
	
				<button class="pop_close close1" type="button">X</button>
	
				<button type="button" class="btn_exls_down_2" onClick="curriculumTemplateDown();">엑셀 양식 다운로드</button>
	
				<div class="t_title_1">
					<span class="sp_wrap_1">교육과정 등록 엑셀양식을 먼저 다운로드 받으신 뒤,<br>다운로드
						받은 엑셀파일양식에 교육과정 정보를 입력하여 파일 등록하시면 전체 교육과정이 일괄등록됩니다.<br>※ 등록 전
						학사체계가 반드시 등록되어야 합니다.<br>※ 신규 교육과정 등록 시에만 엑셀일괄등록이 가능합니다. (기존
						교육과정 수정은 불가)
					</span>
				</div>
	
				<div class="pop_ex_wrap">
					<div class="sub_fwrap_ex_1">
					    <form id="xlsForm" onSubmit="return false;">
							<span class="ip_tt">엑셀 파일</span> 
							<input type="text" readonly class="ip_sort1_1" id="xls_filename" value="" onClick="$('#xlsFile').click();">
                            <button class="btn_r1_1" onClick="$('#xlsFile').click();">파일선택</button>         
                            <input type="file" style="display:none;" id="xlsFile" name="xlsFile" onChange="file_nameChange();">
                        </form>
					</div>
	
					<!-- s_t_dd -->
					<div class="t_dd">
	
						<div class="pop_btn_wrap">
							<button type="button" class="btn01" onclick="xlsUp();">등록</button>
							<button type="button" class="btn02" onclick="$('.close1').click();">취소</button>
						</div>
	
					</div>
					<!-- e_t_dd -->
	
				</div>
			</div>
		</div>
		<!-- e_ 엑셀 일괄 등록 팝업 -->
	
	</body>
</html>