<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<script>
	$(document).ready(function() {
		$(".mdf_wrap_b").hide();
	    $(document).on("click",".ip01a", function(){
	        $(this).parent("div.mdf_wrap_a").hide();
	        $(this).parent().parent().children("div.mdf_wrap_b").show();
	        $(this).closest("td").find("input.ip01b").focus();
	    });
	    
		$(document).on("click", ".btn_x", function(){
			if(confirm("삭제하시겠습니까?")){
				$.ajax({
					type : "POST",
					url : "${HOME}/ajax/admin/academic/codeManagement/deleteCode",
					data : {
						"code_cate" : "formation_evaluation"
						,"code" : $(this).siblings("input[name=code]").val()
					},
					dataType : "json",
					success : function(data, status) {
						if(data.status == "200"){
							alert("삭제 완료 하였습니다.");
							getCode();
						}else if(data.status == "201"){
							alert("해당 코드는 사용중인 코드 입니다.\n삭제할 수 없습니다.");
						}else{
							alert("삭제 실패하였습니다.");
						}
					},
					error : function(xhr, textStatus) {
						document.write(xhr.responseText);
						$.unblockUI();
					},
					beforeSend : function() {
						$.blockUI();
					},
					complete : function() {
						$.unblockUI();
					}
				});				
			}	        		
	    });
		
		$(document).on("click", ".btn_add", function(){
			var htmls = '';
			htmls += '<tr>'
				+'<td class="td_1">'
				+'<div class="mdf_wrap_b" style="">'
				+'<input type="text" class="ip01b" name="new_code_name" value="">'
				+'<button class="btn_x" title="삭제하기">X</button>'
				+'</div>'
				+'</td>'
				+'</tr>';	
			$("#codeListAdd tr").last().before(htmls);
		});
				
		getCode();
	});

	function getCode() {
		$.ajax({
			type : "POST",
			url : "${HOME}/ajax/admin/academic/codeManagement/codeList",
			data : {
				"code_cate" : "formation_evaluation"
			},
			dataType : "json",
			success : function(data, status) {
				var htmls = "";

				$.each(data.codeList, function(index){
					htmls += '<tr>'
						+'<td class="td_1">'
						+'<div class="mdf_wrap_a">'
						+'<input type="text" class="ip01a" value="'+this.code_name+'">'
						+'</div>'
						+'<div class="mdf_wrap_b" style="display:none;">'
						+'<input type="hidden" name="code" value="'+this.code+'">'
						+'<input type="text" class="ip01b" name="code_name" value="'+this.code_name+'">'
						+'<button class="btn_x" title="삭제하기">X</button>'
						+'</div>'
						+'</td>'
						+'</tr>';					
				});
				
				htmls+='<tr>'
					+'<td class="td_1">'
					+'<button class="btn_add" title="추가"></button>'
					+'</td>'
					+'</tr>';
					
				$("#codeListAdd").html(htmls);
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
				$.unblockUI();
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}
	
	function insertCode(){
		if(!confirm("저장하시겠습니까?")){
			return;
		}
			
		var form = document.getElementById("codeForm");
		
		var code_explan = document.createElement("input");
		
		code_explan.type = "hidden";
		code_explan.name = "code_explan";
		code_explan.value = "수업 계획서 형성평가 종류";
		
		var code_cate = document.createElement("input");
		
		code_cate.type = "hidden";
		code_cate.name = "code_cate";
		code_cate.value = "formation_evaluation";

		var lang_type = document.createElement("input");
		
		lang_type.type = "hidden";
		lang_type.name = "lang_type";
		lang_type.value = "ko";
		
		form.appendChild(code_explan);
		form.appendChild(lang_type);
		form.appendChild(code_cate);
		
		$("#codeForm").ajaxForm({
			type : "POST",
			url : "${HOME}/ajax/admin/academic/codeManagement/insert",
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {
					alert("등록이 완료 되었습니다.");
					getCode();
				}else {
					alert("등록 실패 하였습니다.");
				}
			},
			error : function(xhr, textStatus) {
				alert(textStatus);
				//document.write(xhr.responseText);
				$.unblockUI();
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
		$("#codeForm").submit();
		
	}
</script>
</head>

<body>
	<!-- s_container_table -->
	<div id="container" class="container_table">
		<!-- s_contents -->
		<div class="contents main">

			<!-- s_left_mcon -->
			<div class="left_mcon aside_l grd">
				<div class="sub_menu adm_grd">
					<div class="title">학사관리</div>

					<!-- s_학사코드 메뉴 -->
					<div class="boardwrap">
						<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">학사체계관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on">학사코드관리</span>
							</div>
							<ul class="panel on">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2 on">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">교육과정관리</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">교육과정관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청관리</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicReg/list'">학사등록관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">졸업역량관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
					</div>
					<!-- e_학사코드 메뉴 -->

				</div>
			</div>
			<!-- e_left_mcon -->

			<!-- s_main_con -->
			<div class="main_con adm_grd">
				<!-- s_메뉴 접기 버튼 -->
				<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
				<!-- e_메뉴 접기 버튼 -->

				<!-- s_tt_wrap -->
				<div class="tt_wrap">
					<h3 class="am_tt">
						<span class="tt">학사코드 관리 - 형성평가</span>
					</h3>
				</div>
				<!-- e_tt_wrap -->

				<!-- s_adm_content2n -->
				<div class="adm_content2n">
					<!-- s_tt_wrap -->

					<!-- s_wrap_wrap -->
					<div class="wrap_wrap">

						<!-- s_wrap_s -->
						<div class="wrap_s">
							<div class="wrap_s1_uselectbox">
								<div class="uselectbox">
									<span class="uselected">형성평가</span> <span class="uarrow">▼</span>
									<div class="uoptions" style="display: none;">
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/completeCode'">이수구분</span>
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/administerCode'">관리구분</span>
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/lessonMethodCode'">비강의유형</span>
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/feCode'">형성평가</span>
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/gradeCode'">학점기준</span>
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/seCode'">총괄평가기준</span>
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/unitCode'">수업 영역/수준</span>
									</div>
								</div>
							</div>
							<button class="btn_tt1">검색</button>

						</div>
						<!-- e_wrap_s -->


						<form id="codeForm"  onSubmit="return false;">
							<table class="mlms_tb s1">
								<thead>
									<tr>
										<th class="th01 b_2 w0">형성평가</th>
									</tr>
								</thead>
								<tbody id="codeListAdd">								
									
								</tbody>
							</table>
						</form>
						
						
					</div>
					<!-- e_wrap_wrap -->

					<div class="bt_wrap">
						<button class="bt_2" onclick="insertCode();">저장</button>
						<!-- <button class="bt_3">취소</button> -->
					</div>

				</div>
				<!-- e_adm_content2n -->
			</div>
			<!-- e_main_con -->

		</div>
		<!-- e_contents -->
	</div>
	<!-- e_container_table -->

</body>
</html>