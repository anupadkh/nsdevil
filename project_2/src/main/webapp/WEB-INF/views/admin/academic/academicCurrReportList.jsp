<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<link rel="stylesheet" href="${CSS}/nwagon.css" type="text/css">
		<link rel="stylesheet" href="${CSS}/pf_style.css" type="text/css">
		<script src="${JS}/lib/nwagon.js"></script>
		<script>
			$(document).ready(function() {

                bindPopupEvent("#m_pop1", ".open1");
                bindPopupEvent("#m_pop2", ".open2");	
				getAcademicTitle();
				getCurriculumList();
			});
			
			function getAcademicTitle(){
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicReg/titleInfo",
		            data: {                      
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		if(!isEmpty(data.acaInfo.academic_name))
		                		$("#academicNameTitle").text(data.acaInfo.academic_name);
		                	else
		                		$("#academicNameTitle").hide();
		            	
		            		var aca_state = "";
		                	switch(data.acaInfo.aca_state){
	                            case "01" : aca_state = "개설(비공개)";break;
	                            case "02" : aca_state = "개설(공개)";break;
	                            case "03" : aca_state = "학사 중(수업 중)";break;
	                            case "04" : aca_state = "학사종료(미확정)";break;
	                            case "05" : aca_state = "학사종료(성적산정)";break;
	                            case "06" : aca_state = "학사종료(성적발표)";break;
	                            case "07" : aca_state = "성적발표 종료(미확정)";break;
	                            case "08" : aca_state = "학사최종종료(확정)";break;                            
	                        }
			            	$("#academicState").text(aca_state); //타이틀 학사상태
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
			
			function getCurriculumList(){
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicReg/curriculumAssign/list",
		            data: {        
		            	"assignYN" : "Y"
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
			            	var htmls = "";
			            	$("#currListAdd").empty();
			            	$.each(data.currList, function(index){
			            		htmls = '<tr>'
		                            +'<td class="td_1 w1 bd01">'+(index+1)+'</td>'
		                            +'<td class="td_1 w2 bd01">'+this.curr_code+'</td>'
		                            +'<td class="td_1 w4 bd01">'+this.curr_name+'</td>'
		                            +'<td class="td_1 w3 bd01">'+this.mpf_name+'</td>'
		                            +'<td class="td_1 w3 bd01">'+this.curr_start_date+'<br>'+this.curr_end_date+'</td>'
		                            +'<td class="td_1 w1 bd01">'+this.period_cnt+'</td>'
		                            +'<td class="td_1 w1_1 bd01"><button class="btn2_2 open1" onClick="getReport('+this.curr_seq+','+this.mpf_user_seq+')">조회</button></td>'
		                            +'</tr>';
			            		$("#currListAdd").append(htmls);
			            	});			            	
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
						
			function pageMove(curr_seq, curr_name){
				post_to_url("${HOME}/admin/academic/academicReg/currScore/create", {"curr_seq":curr_seq, "curr_name":curr_name});
			}
			
			function getAcademic(){
		    	post_to_url('${HOME}/admin/academic/academicReg/academicRegBasicInfo/create', {"aca_seq" : "${S_ACA_SEQ}"});
		    }
			
			function pageMoveScore(type, aca_seq){
		    	$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/admin/academic/academicReg/setAcaSeq",
	                data: {
	                	"aca_seq" : aca_seq
	                    },
	                dataType: "json",
	                success: function(data, status) {
	                	if(type=="curr")
	               			location.href="${HOME}/admin/academic/academicReg/currScore";
		     			else if(type=="soosi")
		     				location.href="${HOME}/admin/academic/academicReg/soosiScore";
		     			else if(type=="fe")
		     				location.href="${HOME}/admin/academic/academicReg/feScore";
	                },
	                error: function(xhr, textStatus) {
	                    alert("실패");
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                },beforeSend:function() {
	                	$.blockUI();
	                },
	                complete:function() {
	                	$.unblockUI();
	                }
	            });
		    }
			
			function getReport(curr_seq, mpf_user_seq){
	            
	            $.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/admin/academic/academicReg/currReport/list",
	                data: {    
	                	"curr_seq" : curr_seq
	                	,"user_seq" : mpf_user_seq
	                },
	                dataType: "json",
	                success: function(data, status) {
	                	if(data.status == "200"){	    
	                		$("#m_pop1").show();
	                		$("td[data-name=curr_date]").text(data.basicInfo.curr_start_date+"~"+data.basicInfo.curr_end_date+" ("+data.basicInfo.curr_week+"주 / "+data.basicInfo.period_cnt+"차시)");
	                		$("td[data-name=curr_name]").text(data.basicInfo.curr_name);
	                		$("td[data-name=complete_name]").text(data.basicInfo.complete_name);
	                		$("td[data-name=administer_name]").text(data.basicInfo.administer_name);
	                		$("td[data-name=grade]").text(data.basicInfo.grade+"학점");
	                		
	                		if(data.basicInfo.req_charge == 0)
	                			$("span[data-name=req_charge]").text("");
	                		else
	                			$("span[data-name=req_charge]").text(numberWithCommas(data.basicInfo.req_charge+"")+"원");
	                		
	                		var mpfList = "";
	    					$.each(data.mpfList, function(index){
	                			if(index != 0)
	                				mpfList += ",";
	                			if(isEmpty(this.department_name))
	                				mpfList += this.name;
	                			else
	                				mpfList += this.name+"("+this.department_name+")";
	                		});
	    					$("td[data-name=mpfList]").text(mpfList);
	                		
	    					var dpfList = "";
	    					$.each(data.dpfList, function(index){
	    						if(index != 0)
	    							dpfList += ",";
	                			if(isEmpty(this.department_name))
	                				dpfList += this.name;
	                			else
	                				dpfList += this.name+"("+this.department_name+")";
	    					});
	    					$("td[data-name=pfList]").text(dpfList);	                		               			                		
	                		
	                		$("span[data-name='curr_name']").html(data.basicInfo.curr_name);
	                    	$("span[data-name='aca_system_name']").html("("+ data.basicInfo.aca_system_name + ")");
	                    	
	                    	$("span[data-name=totalPeriod]").text(data.progressInfo.max_lp);
	                    	$("span[data-name=thisPeriod]").text(data.progressInfo.this_lp);
	                    	
	                    	if(data.progressInfo.max_lp == 0 || data.progressInfo.this_lp == 0){
	                    		$("span[data-name=progress]").css("width","0px");
	                    		$("span[data-name=percent]").text("0%");
	                    	}else{
	                    		var percent = "";
	                    		percent = parseInt(data.progressInfo.this_lp)/parseInt(data.progressInfo.max_lp)*100+"";
	                    		
	                    		percent = parseInt(percent);
	                    		/* if(percent.indexOf(".") > 0){
	                    			percent.str.substring(0,percent.indexOf("."));
	                    		} */
	                    		$("span[data-name=percent]").text(parseInt(percent)+"%");
	                    		$("span[data-name=progress]").css("width", parseInt(percent)*2);
	                    	}
	                    		
	                    	
	                		$("span[data-name=curr_week]").text(data.basicInfo.curr_week);
	                		$("span[data-name=period_cnt]").text(data.basicInfo.period_cnt);
	                		$("span[data-name=exclude_period_cnt]").text(data.basicInfo.exclude_period_cnt);
	                		$("span[data-name=lecture_cnt]").text(data.basicInfo.lecture_cnt);
	                		$("span[data-name=unlecture_cnt]").text(data.basicInfo.unlecture_cnt);

	                		if(!isEmpty(data.basicInfo.period_cnt)){
	                    		//주당평균시간 구하기
	                            var period_avg = (data.basicInfo.period_cnt/data.basicInfo.curr_week);
	                    		
		                		$("span[data-name=week_period_cnt]").text(period_avg.toFixed(2));  
	                        }
	                		
	                		var htmls = "";
	                		
	                		//그래프	                		
	                		$.each(data.currGraph, function(index){
	                			if(this.count==0)
	                				return false;
	                			var graph_id = "";
	                			var fields_id = "";
	                			var title_id = "";
	                			var title = "";
	                			if(this.gubun == "MY"){
	                				graph_id = "myChart";
		                			fields_id = "myCurr";
		                			title_id = "myName";
		                			title = this.year;
	                			}else if(this.gubun == "TOTAL"){
	                				graph_id = "totalChart";
		                			fields_id = "totalCurr";
		                			title_id = "totalName";
		                			title = this.year+" 전체";
	                			}else if(this.gubun == "PRE"){
	                				graph_id = "preChart";
		                			fields_id = "preCurr";
		                			title_id = "preName";
		                			title = this.year+" 동일 교육과정";
	                			}	                				
								
	                			$("span[data-name="+title_id+"]").text(title);
	                			
	                			htmls = '<div class="wrap"><span class="rt1"></span><span class="tt1">강의</span></div>';
	                			
	                			var FieldArray = new Array();
	                			var colorArray = ['rgb(99, 178, 230)', 'rgb(120, 191, 163)', 'rgb(233, 210, 90)', 'rgb(131, 148, 220)', 'rgb(255, 140, 000)'
	                				, 'rgb(238, 130, 238)', 'rgb( 192, 192, 192)', 'rgb( 128, 128, 000)', 'rgb(148, 000, 211)', 'rgb( 128, 000, 000)'
	                				, 'rgb( 000, 000, 205)', 'rgb( 000, 250, 154)', 'rgb( 000, 000, 128)', 'rgb( 107, 142, 35)', 'rgb( 216, 192, 216)'];
	                			var lessonCntArray = new Array();
	                			
	                			FieldArray.push("강의");
	                			lessonCntArray[0] = this.lecture_y;
	                			
	                			var name = this.code_name.split("\|");
	                			var lesson_method = this.lesson_method_cnt.split("\|");
	                			
	                			if(!isEmpty(this.code_name)){
		                			for(var i=0;i<name.length;i++){
		                				FieldArray[i+1] = name[i];
		                				htmls+='<div class="wrap"><span class="rt'+(i+2)+'"></span><span class="tt1">'+name[i]+'</span></div>';
		                			}
	                			}
	                			
	                			$("#"+fields_id).html(htmls);
	                			
	                			if(!isEmpty(this.lesson_method_cnt)){
		                			for(var i=0;i<lesson_method.length;i++){
		                				lessonCntArray[i+1] = parseInt(lesson_method[i]);
		                			}
	                			}
	                			
	                			$("#"+graph_id).empty();
	                			
	                			var options = {
                					'dataset': {
                						title: '',
                						values:lessonCntArray,
                						colorset: colorArray,
                						fields: FieldArray 
                					},
                					'donut_width' : 100, 
                					'core_circle_radius':0,
                					'chartDiv': graph_id,
                					'chartType': 'pie',
                					'chartSize': {width:200, height:400}
                				};
                				Nwagon.chart(options);
	                		});
	                		
	                		htmls = "";
	                			                		
	                		//형성평가 리스트
	                		$.each(data.feList, function(index){
	                			var feName = this.code_name.split("\|");
	                			var feCnt = this.cnt.split("\|");
	                			var gubun = "";
	                			var sumCnt = 0 ;
	                			htmls = "";
	                			
								
	                			if(index == 0){
	                				htmls='<tr class="">'
										+'<td class="th01 w22">구분</td>';
									if(!isEmpty(feName)){
										for(var i=0;i<feName.length;i++){
											htmls+='<td class="th01 w21">'+feName[i]+'</td>';
										}
									}
									htmls+='<td class="th01 w21">계</td></tr>';									
	                			}
	                			
	                			if(this.gubun=="MY")
	                				gubun = "MY";
	                			else if(this.gubun=="TOTAL")
	                				gubun = this.year+" 전체";
	                			else if(this.gubun=="PRE")
	                				gubun = this.year+" 동일 교육과정";
	                			
	                			htmls+='<tr class=""><td class="th01">'+gubun+'</td>';
	                			
	                			if(!isEmpty(feCnt)){
									for(var i=0;i<feCnt.length;i++){
										var cnt = feCnt[i];
										sumCnt+=parseInt(feCnt[i]);
										if(cnt=="0")
											cnt = "-";
										htmls+='<td class="td1"><span class="t1">'+cnt+'</span></td>';
									}
								}
	                			htmls+='<td class="td1"><span class="t1">'+sumCnt+'</span></td></tr>';
	                			$("#feListAdd").append(htmls);
	                		});
	                		
	                		//성적분포
	                		$.each(data.gradeList, function(index){
	                			var graph_id = "";
	                			var title_id = "";
	                			var title = "";
	                			var total = 0;
	                			if(this.gubun == "MY"){
	                				graph_id = "gradeMy";
		                			title_id = "gradeMyName";
		                			title = this.year;
	                			}else if(this.gubun == "TOTAL"){
	                				graph_id = "gradeTotal";
		                			title_id = "gradeTotalName";
		                			title = this.year+" 전체";
	                			}else if(this.gubun == "PRE"){
	                				graph_id = "gradePre";
		                			title_id = "gradePreName";
		                			title = this.year+" 동일 교육과정";
	                			}
	                				                			
	                			$("span[data-name="+title_id+"]").text(title); 
	                			
	                			var grade = this.grade.split("\|");
	                			var grade_cnt = this.grade_cnt.split("\|");
	                			htmls = "";
	                			
	                			if(index==0){
	                				htmls+='<tr class=""><td class="th01 w42">구분</td>';
	                				
	                				for(var i=0;i<grade.length;i++){
	                					htmls+='<td class="th01 w41">'+grade[i]+'</td>';
	                				}
									htmls+='<td class="th01 w41">합계</td></tr>';
	                			}                			
							
	                			htmls+='<tr class=""><td class="th01">'+title+'</td>';
	                			for(var i=0;i<grade_cnt.length;i++){
	                				total += parseInt(grade_cnt[i]);
	                				htmls+='<td class="td1"><span class="t3">'+grade_cnt[i]+'</span></td>';
	                			}
							
								htmls+='<td class="td1"><span class="t3">'+total+'</span></td></tr>';
								
								//숫자형으로 변환한다....
								var arrayGrade = new Array();
								
								for(var ix=0; ix<grade_cnt.length;ix++){
									arrayGrade[ix] = parseInt(grade_cnt[ix]);
								}
								
								var options = {
									'legend':{
										names: grade,
										hrefs: []
											},
									'dataset': {
										title: '',
										values: [arrayGrade], 
										bgColor: '#f9f9f9',
										fgColor: 'rgba(74, 195, 255, 1)'
									},
									'chartDiv': graph_id,
									'chartType': 'radar',
									'chartSize': {width:200, height:400}
								};
								Nwagon.chart(options);
								
								$("#gradeListAdd").append(htmls);
	                		});
	                		
	                		htmls="";
	                		
	                		//졸업 역량
	                		if(data.fcList.length==0)
	                			$("#fcDiv").hide();
	                		$.each(data.fcList, function(index){
	                			var fc_name = "";
	                			if(!isEmpty(this.fc_code))
	                				fc_name = "("+this.fc_code+") "+this.fc_name;
	                			if(index==0 || index%2==0)
	                				htmls+='<tr class="">';
	                			htmls+='<td class="td2">'+fc_name+'</td><td class="td1 pop open1" onClick="popupInfo(\'fc\',\''+this.fc_seq+'\',\''+fc_name+'\','+curr_seq+','+mpf_user_seq+');"><span class="t1">'+this.lp_cnt+'</span></td>';
	                			if(index%2==1)
	                				htmls+='</tr>';	    							
	                		});
	                		
	                		if(data.fcList.length%2==1)
	                			htmls+='<td class="td2"></td><td class="td1 pop open1"><span class="t1">-</span></td></tr>';
	                		
	                		$("#fcListAdd").append(htmls);
	                		
	                		htmls="";
	                		
	                		//단원관리
	                		if(data.unitList.length==0)
	                			$("#unitDiv").hide();
	                		var pre_unit_seq = "";
	                		$.each(data.unitList, function(index) {   
			                    var htmls = "";                 
			                    if(this.type == "UNIT"){
			                        if(this.unit_seq != pre_unit_seq){
			                            htmls += '<tbody class="lv1" name="'+this.unit_seq+'"><tr class="tr02 lv2" name="'+this.unit_seq+'_'+this.lp_seq+'"><td class="pm0 free_textarea">';
			                            htmls += '<input type="hidden" name="unit_seq" value="Y_'+this.unit_seq+'"/>'+this.unit_name+'</td>';
			                            htmls += '<td class="pm0 free_textarea"><input type="hidden" name="lp_seq" value="Y_'+this.unit_seq+'_'+this.lp_seq+'"/>'+this.lesson_subject+'</td>';
			                            htmls += '<td class="ta_c">'+this.period_cnt+'</td>';
			                            htmls += '<td colspan="7" class="color1 ta_c"></td></tr></tbody>';
			                            $("#unitTab").append(htmls);
			                        }else{
			                            htmls += '<tr class="tr02 lv2" name="'+this.unit_seq+'_'+this.lp_seq+'"><td class="pm0 free_textarea"></td>';
			                            htmls += '<td class="pm0 free_textarea"><input type="hidden" name="lp_seq" value="Y_'+this.unit_seq+'_'+this.lp_seq+'_'+this.lp_seq+'"/>'+this.lesson_subject+'</td>';
			                            htmls += '<td class="ta_c">'+this.period_cnt+'</td>';
			                            htmls += '<td colspan="7" class="color1 ta_c"></td></tr>';
			                            $("#unitTab").append(htmls);
			                        }
			                    }
			                    else if(this.type == "TLO"){
			                        htmls += '<tr class="tr02 lv3" name="'+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+'"><td class="pm0 free_textarea" colspan="3"></td>';
			                        htmls += '<td colspan="4" class="color1 ta_c"><input type="hidden" name="tlo_seq" value="Y_'+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+'"/>';
			                        htmls += '<span class="tts3">TLO '+this.tlo_order_num+'</span></td>';
			                        htmls += '<td class="pm0 free_textarea">'+this.skill+'</td>';
			                        htmls += '<td class="pm0 free_textarea">'+this.teaching_method+'</td>';
			                        htmls += '<td class="pm0 free_textarea">'+this.evaluation_method+'</td></tr>';                       
			                        if($("#unitTab tr[name^='"+this.unit_seq+'_'+this.lp_seq+"_']").length == 0)
			                            $("#unitTab tr[name='"+this.unit_seq+'_'+this.lp_seq+"']").after(htmls);
			                        else
			                            $("#unitTab tr[name^='"+this.unit_seq+'_'+this.lp_seq+"_']").last().after(htmls);
			                    }else if(this.type == "ELO"){
			                        htmls += '<tr class="tr02 lv4" name="'+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+'_'+this.elo_seq+'">';
			                        htmls += '<td class="" colspan="3"><input type="hidden" name="elo_seq" value="Y_'+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+'_'+this.elo_seq+'"/></td>';
			                        htmls += '<td class="ta_c">ㄴ</td>';
			                        htmls += '<td class="ta_c">ELO '+this.elo_order_num+'</td>';
			                        htmls += '<td class="ta_c pm0">'+this.domain_name+'</td>';
			                        htmls += '<td class="ta_c pm0">'+this.level_name+'</td>';
			                        htmls += '<td colspan="3" class="pm0 free_textarea">'+this.content+'</td></tr>';
			                        if($("#unitTab tr[name^='"+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+"_']").length == 0)            
			                            $("#unitTab tr[name='"+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+"']").after(htmls);
			                        else
			                            $("#unitTab tr[name^='"+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+"_']").last().after(htmls);  
			                    }
			                    pre_unit_seq = this.unit_seq;
			                });
	                		
	                		//임상표현
	                		htmls = "";
	                		
	                		$.each(data.clinicList, function(index){
	                			if(index==0 || index%4==0)
	                				htmls+='<tr class="">';
	                			
	                			htmls+='<td class="td2">'+this.code_name+'</td><td class="td1 pop open2" onClick="popupInfo(\'clinic\',\''+this.clinic_code+'\',\''+this.code_name+'\','+curr_seq+','+mpf_user_seq+');"><span class="t3">'+this.lp_cnt+'</span></td>';
	                			
	                			if(index%4==3){
	                				htmls+='</tr>';
	                			}	                			
	                		});
	                		
	                		for(var i=data.clinicList.length%4;i<4;i++){
	                			htmls+='<td class="td2"></td><td class="td1 pop open2"><span class="t3">-</span></td>';
	                		}
	                		
	                		htmls+='</tr>';
	                		
	                		$("#clinicListAdd").append(htmls);
	                		
	                		//진단
	                		htmls = "";
	                		
	                		$.each(data.diaList, function(index){
	                			if(index==0 || index%4==0)
	                				htmls+='<tr class="">';
	                			
	                			htmls+='<td class="td2">'+this.code_name+'</td><td class="td1 pop open3" onClick="popupInfo(\'dia\',\''+this.dia_code+'\',\''+this.code_name+'\','+curr_seq+','+mpf_user_seq+');"><span class="t3">'+this.lp_cnt+'</span></td>';
	                			
	                			if(index%4==3){
	                				htmls+='</tr>';
	                			}	                			
	                		});
	                		
	                		for(var i=data.diaList.length%4;i<4;i++){
	                			htmls+='<td class="td2"></td><td class="td1 pop open3"><span class="t3">-</span></td>';
	                		}
	                		
	                		htmls+='</tr>';
	                		
	                		$("#diaListAdd").append(htmls);
	                		
	                	}else{
	                		alert("보고서 가져오기 실패");
	                	}
	                }, error: function(xhr, textStatus) {
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                }, beforeSend : function() {
						$.blockUI();
					}, complete : function() {
						$.unblockUI();
					}
	            }); 
	        }
			
			function reportPrint() {

				var initBody = document.body.innerHTML;				
				
				 window.onbeforeprint = function () {
					 document.body.innerHTML = "<div class='r_report1' style='display:block;position:relative;padding:0;overflow:hidden;'><div class='content'>"+document.getElementById("ReportTitle").innerHTML+"</div></div><div class='mpf_tabcontent6'>"+document.getElementById("Report").innerHTML+"</div>";
					$(".btn_prt").hide();
				}
				window.onafterprint = function () {

					document.body.innerHTML = initBody;
					$(".btn_prt").show();
				}
				window.print();
			}
			
			function popupInfo(gubun, code, title, curr_seq, user_seq){
				$.ajax({ 
	                type: "POST",
	                url: "${HOME}/ajax/admin/academic/academicReg/currReport/popup/list",
	                data: {          
	                	"gubun" : gubun
	                	,"code" : code	 
	                	,"curr_seq" : curr_seq
	                	,"user_seq" : user_seq
	                },
	                dataType: "json",
	                success: function(data, status) {
	                	if(data.status == "200"){	        			
	                		$("p[data-name=popupTitle]").text(title);
	                		var htmls = "";
	                		var pfName = "";
	                		var picture = "";
	                		$.each(data.list, function(index){
	                			if(!isEmpty(this.name)){
	                				if(!isEmpty(this.specialty))
	                					pfName = this.name+"("+this.specialty+")";
	                				else
	                					pfName = this.name;
	                			}else{
	                				pfName = "교수 미등록";
	                			}
	                			
	                			if(!isEmpty(this.picture_path))
	                				picture = "${RES_PATH}" + this.picture_path;
                				else
                					picture = "${IMG}/ph_3.png";
	                			
	                			
	                			htmls += '<span class="tt1" title="'+title+'">'+this.lesson_subject+'</span><div class="a_mp">'      
	                            	+'<span class="pt01"><img src="'+picture+'" alt="사진" class="pt_img"></span><span class="ssp1">'+pfName+'</span>'
	                        		+'</div>';
	                		});
	                		
	                		$("div[data-name=popupListAdd]").html(htmls);
	                		
	                		$("#m_pop2").show();
	                	}
	                },
	                error: function(xhr, textStatus) {
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                }
	            }); 
			}
		</script>
	</head>
	
	<body>
		<input type="hidden" name="aca_seq" id="aca_seq" value="${S_ACA_SEQ }">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
		<!-- s_container_table -->
		<div id="container" class="container_table">
			<!-- s_contents -->
			<div class="contents main">
	
				<!-- s_left_mcon -->
				<div class="left_mcon aside_l grd">
					<div class="sub_menu adm_grd">
						<div class="title">학사관리</div>
						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">학사체계관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">학사코드관리</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">교육과정관리</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">교육과정관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청관리</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on" onClick="location.href='${HOME}/admin/academic/academicReg/list'">학사등록관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">졸업역량관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- e_left_mcon -->
	
				<!-- s_main_con -->
				<div class="main_con adm_grd" style="width:100%;">
					<a class="btn_l" style="top:15px;"><span>좌측 메뉴 접기/펴기</span></a>
					<!-- s_tt_wrap -->
					<div class="tt_wrap" style="margin:15px 0 0 0;">
						<h3 class="am_tt" style="padding:0;margin-left:5px;">
							<span class="tt" style="line-height:40px;top:0;padding:0 25px;">학사등록관리<span class="sign1"> &gt; </span>운영보고서</span>
						</h3>
						<span class="sp_state">학사상태 : <span class="tt" id="academicState"></span></span>
						<h4 class="h4_tt" id="academicNameTitle"></h4>
					</div>
					<!-- e_tt_wrap -->
	
					<!-- s_adm_content1 -->
					<div class="adm_content3">
						<!-- s_tt_wrap -->
	
						<!-- s_wrap_wrap -->
						<div class="wrap_wrap">
	
							<div class="tab_wrap_cc">							
								<button class="tab01 tablinks" onclick="getAcademic();">기본정보</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/curriculumAssign'">교육과정배정</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/studentAssign'">학생배정</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/schedule'">시간표</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/SFSurvey/list'">만족도조사</button>				
								<button class="tab01 tablinks" onclick="pageMoveScore('curr','${S_ACA_SEQ }');">종합성적</button>
								<button class="tab01 tablinks" onclick="pageMoveScore('soosi','${S_ACA_SEQ }');">수시성적</button>											
								<button class="tab01 tablinks active" onclick="location.href='${HOME}/admin/academic/academicReg/currReport'">운영보고서</button>									
							</div>
	
							<!-- s_top_tt_wrap --> 
							<div class="top_tt_wrap schd">
							    <span class="sp03">교육과정 코드 또는 교육과정명을 선택하시면 해당 교육과정 운영보고서 화면으로 바로 이동됩니다.</span>
							</div>
							<!-- e_top_tt_wrap -->

							<div class="tb_wrap_x">
								<table class="mlms_tb schd">
									<thead>
										<tr>
											<td rowspan="2" class="th01 w1">no</td>
											<td colspan="2" class="th01 bd01 w5">교육과정</td>
											<td rowspan="2" class="th01 w3">책임<br>교수
											</td>
											<td rowspan="2" class="th01 w3">기간</td>
											<td rowspan="2" class="th01 w1">시수</td>
											<td rowspan="2" class="th01 w6">운영보고서</td>
										</tr>
										<tr>
											<td class="th01 w2">코드</td>
											<td class="th01 w4">교육과정명</td>
										</tr>
									</thead>
									<tbody id="currListAdd">
	
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- s_운영보고서 팝업 -->
		<div id="m_pop1" class="r_report1 mo1">
			<!-- s_pop_wrap -->
			<div class="pop_wrap">
				<button class="pop_close close1" type="button" onClick="$('#m_pop1').hide();"></button>
	
				<p class="t_title">운영보고서</p>
	
				<!-- s_content -->
				<div class="content" id="ReportTitle">
					<div class="tt_wrap1">	
						<span class="ttswrap2"> 
							<span class="title">해부학 수업</span>
						</span>	
					</div>
	
					<div class="wrap1">
						<div class="ttswrap1">
							<div class="swrap">
								<span class="tt1">책임교수</span>
								<span class="tt2" data-name="mpf_list">김가나다라(내과)</span>
							</div>
							<div class="swrap">
								<span class="tt1">부책임교수</span>
								<span class="tt2" data-name="dpf_list">이가나다라(내과), 최가나다라(내과)</span>
							</div>
						</div>
						<div class="ttswrap1">
							<div class="swrap">
								<span class="tt1">교육과정명</span>
								<span class="tt2" data-name="curr_name">해부학 수업</span>
							</div>
							<div class="swrap">
								<span class="tt1">교육과정기간</span>
								<span class="tt2" data-name="curr_date">2018.03.01 ~ 2018.05.31 8주 45차시</span>
							</div>
							<div class="swrap">
								<span class="tt4" data-name="complete_name">전공선택</span>								
								<span class="tt4" data-name="administer_name">실습과정</span>
								<span class="tt4" data-name="grade">4학점</span>
								<span class="tt1_1">비용</span>
								<span class="num" data-name="req_charge"></span>
							</div>
						</div>
	
						<div class="btn_wrap_pp">
							<button class="btn_prt" title="인쇄하기" onClick="reportPrint();">인쇄</button>
						</div>
					</div>
				</div>
				
				<div class="content" id="Report">
					<!-- s_tt_wrap -->
					<div class="tt_wrap1">	
						<div class="tt_g1">
							<span class="g_t1">교과과정 진척률 : </span>
			
							<div class="wrap_prg">
								<!-- s_ProgressBar width 200px 백분률로 변환하여 값 삽입 -->
								<div class="prg">
									<span class="bar" style="width: 100px;" data-name="progress"></span>
								</div>
								<!-- e_ProgressBar width 200px 백분률로 변환하여 값 삽입 -->
								<span class="sp1" data-name="percent">%</span>
							</div>
			
							<span class="g_t3">총</span>
							<span class="g_num" data-name="totalPeriod"></span>
							<span class="g_t3">차시 중</span>
							<span class="g_num" data-name="thisPeriod"></span>
							<span class="g_t3">차시까지 진행됨</span>
						</div>			
					</div>
					<!-- e_tt_wrap -->
			
					<!-- s_wrap_wrap -->
					<div class="wrap_wrap">
						<p class="tt">수업시간 및 수업방법 분석</p>
			
						<!-- s_tab_table_u ttb1 -->
						<table class="tab_table_u ttb1">
							<tbody>
								<tr class="">
									<td class="th01 w21">주수</td>
									<td class="th01 w21">총시수</td>
									<td class="th01 w22">주당 평균시수</td>
									<td class="th01 w21">시수 외</td>
									<td class="th01 w21">강의</td>
									<td class="th01 w21">비강의</td>
								</tr>
								<tr class="">
									<td class="td1"><span class="t1" data-name="curr_week"></span><span class="t2">주</span></td>
									<td class="td1"><span class="t1" data-name="period_cnt"></span><span class="t2">시간</span></td>
									<td class="td1"><span class="t1" data-name="week_period_cnt"></span><span class="t2">시간/</span><span class="t1">1</span><span class="t2">주</span></td>
									<td class="td1"><span class="t1">-</span><span class="t2">시간</span></td>
									<td class="td1"><span class="t1" data-name="lecture_cnt"></span><span class="t2">시간</span></td>
									<td class="td1"><span class="t1" data-name="unlecture_cnt"></span><span class="t2">시간</span></td>
								</tr>
							</tbody>
						</table>
						<!-- e_tab_table_u ttb1 -->
			
						<!-- s_cht_wrap -->
						<div class="cht_wrap">
			
							<!-- s_cht1 -->
							<div class="cht1" style="height:285px;">
								<div class="wraps">
									<span class="tt" data-name="myName">My</span>
									<div id="myChart" class="chart100"></div>
			
									<div class="fields1" id="myCurr">
									</div>	
								</div>
							</div>
							<!-- s_cht1 -->
			
							<!-- s_cht1 -->
							<div class="cht1" style="height:285px;">
								<div class="wraps">
									<span class="tt" data-name="totalName"></span>
			
									<div id="totalChart" class="chart100"></div>
			
									<div class="fields1" id="totalCurr">								
									</div>
			
								</div>
							</div>
							<!-- s_cht1 -->
			
							<!-- s_cht1 -->
							<div class="cht1" style="height:285px;">
								<div class="wraps">
									<span class="tt" data-name="preName"></span>
			
									<div id="preChart" class="chart100"></div>
			
									<div class="fields1" id="preCurr">
										
									</div>
			
								</div>
							</div>
							<!-- s_cht1 -->
			
						</div>
						<!-- s_cht_wrap -->
			
					</div>
					<!-- e_wrap_wrap -->
			
					<!-- s_wrap_wrap -->
					<div class="wrap_wrap">
						<p class="tt">형성평가 횟수 및 방법 분석</p>
			
						<!-- s_tab_table_u ttb1 -->
						<table class="tab_table_u ttb1">
							<tbody id="feListAdd">
								
							</tbody>
						</table>
						<!-- e_tab_table_u ttb1 -->
			
					</div>
					<!-- e_wrap_wrap -->
			<!-- 
					s_wrap_wrap
					<div class="wrap_wrap">
						<p class="tt">평가 방법 및 결과 분석</p>
			
						s_tab_table_u ttb1
						<table class="tab_table_u ttb1">
							<tbody>
								<tr class="">
									<td class="th01 w32">구분</td>
									<td class="th01 w31">필기</td>
									<td class="th01 w31">CPX</td>
									<td class="th01 w31">OSCE</td>
									<td class="th01 w31">출석 / 태도</td>
									<td class="th01 w31">총점</td>
									<td class="th01 w31">학점</td>
								</tr>
								<tr class="">
									<td class="th01">평가 비중</td>
									<td class="td1"><span class="t1">20</span><span class="t2">%</span></td>
									<td class="td1"><span class="t1">30</span><span class="t2">%</span></td>
									<td class="td1"><span class="t1">20</span><span class="t2">%</span></td>
									<td class="td1"><span class="t1">30</span><span class="t2">%</span></td>
									<td class="td1"><span class="t1">100</span><span class="t2">%</span></td>
									<td class="td1"><span class="t1">-</span></td>
								</tr>
								<tr class="">
									<td class="th01">My 평균</td>
									<td class="td1"><span class="t1">20</span><span class="t2">%</span></td>
									<td class="td1"><span class="t1">20.5</span><span class="t2">%</span></td>
									<td class="td1"><span class="t1">19.5</span><span class="t2">%</span></td>
									<td class="td1"><span class="t1">30</span><span class="t2">%</span></td>
									<td class="td1"><span class="t1">90</span><span class="t2">%</span></td>
									<td class="td1"><span class="t1">A</span></td>
								</tr>
							</tbody>
						</table>
						e_tab_table_u ttb1
					</div>
					e_wrap_wrap
			 -->
					<!-- s_wrap_wrap -->
					<div class="wrap_wrap">
						<p class="tt">성적 분포 분석</p>
			
						<!-- s_tab_table_u ttb1 -->
						<table class="tab_table_u ttb1">
							<tbody id="gradeListAdd">						
							</tbody>
						</table>
						<!-- e_tab_table_u ttb1 -->
			
						<!-- s_cht_wrap -->
						<div class="cht_wrap" style="page-break-before:always;">
			
							<!-- s_cht2 -->
							<div class="cht2">
								<div class="wraps">
									<span class="tt" data-name="gradeMyName">My</span>							
									<!-- 백분율로 변환하여 해당 id의 javascript values에 -->
									<div id="gradeMy" class="chart200"></div>
			
								</div>
							</div>
							<!-- s_cht2 -->
			
							<!-- s_cht2 -->
							<div class="cht2">
								<div class="wraps">
									<span class="tt" data-name="gradeTotalName"></span>	
									<div id="gradeTotal" class="chart200"></div>
			
								</div>
							</div>
							<!-- s_cht2 -->
			
							<!-- s_cht2-->
							<div class="cht2">
								<div class="wraps">
									<span class="tt" data-name="gradePreName"></span>
			
									<div id="gradePre" class="chart200"></div>
			
								</div>
							</div>
							<!-- s_cht2 -->
			
						</div>
						<!-- s_cht_wrap -->
			
					</div>
					<!-- e_wrap_wrap -->
			
					<!-- s_wrap_wrap -->
					<div class="wrap_wrap" style="display:none;">
						<p class="tt">
							수업만족도
							<button class="btn1 open4">상세보기</button>
						</p>
			
						<!-- s_tab_table_u ttb1 -->
						<table class="tab_table_u ttb1">
							<tbody>
								<tr class="">
									<td class="th01 w42">구분</td>
									<td class="th01 w41">교수1</td>
									<td class="th01 w41">교수2</td>
									<td class="th01 w41">교수3</td>
									<td class="th01 w41">교수4</td>
									<td class="th01 w41">교수5</td>
									<td class="th01 w41">교수6</td>
									<td class="th01 w41">교수7</td>
									<td class="th01 w41">교수8</td>
									<td class="th01 w41">전체</td>
								</tr>
								<tr class="">
									<td class="th01">평균</td>
									<td class="td1"><span class="t1">4.5</span></td>
									<td class="td1"><span class="t1">4.5</span></td>
									<td class="td1"><span class="t1">4</span></td>
									<td class="td1"><span class="t1">4</span></td>
									<td class="td1"><span class="t1">3.5</span></td>
									<td class="td1"><span class="t1">3.5</span></td>
									<td class="td1"><span class="t1">3.5</span></td>
									<td class="td1"><span class="t1">4.5</span></td>
									<td class="td1"><span class="t1">4.0</span></td>
								</tr>
							</tbody>
						</table>
						<!-- e_tab_table_u ttb1 -->
			
						<!-- s_cht_wrap -->
						<div class="cht_wrap">
			
							<!-- s_cht3 -->
							<div class="cht3">
								<div class="wraps">
									<!-- 200px에 대한 height 비율로 계산하여 style에 예:5점 만점일 경우 200px , title에 값 입력 -->
									<div class="bar bar1" style="height: 180px;" title="4.5">4.5</div>
								</div>
								<span class="tt">교수1</span>
							</div>
							<!-- s_cht3 -->
			
							<!-- s_cht3 -->
							<div class="cht3">
								<div class="wraps">
									<div class="bar bar1" style="height: 180px;" title="4.5">4.5</div>
								</div>
								<span class="tt">교수2</span>
							</div>
							<!-- s_cht3 -->
			
							<!-- s_cht3 -->
							<div class="cht3">
								<div class="wraps">
									<div class="bar bar1" style="height: 160px;" title="4.0">4.0</div>
								</div>
								<span class="tt">교수3</span>
							</div>
							<!-- s_cht3 -->
			
							<!-- s_cht3 -->
							<div class="cht3">
								<div class="wraps">
									<div class="bar bar1" style="height: 160px;" title="4.0">4.0</div>
								</div>
								<span class="tt">교수4</span>
							</div>
							<!-- s_cht3 -->
			
							<!-- s_cht3 -->
							<div class="cht3">
								<div class="wraps">
									<div class="bar bar1" style="height: 140px;" title="3.5">3.5</div>
								</div>
								<span class="tt">교수5</span>
							</div>
							<!-- s_cht3 -->
			
							<!-- s_cht3 -->
							<div class="cht3">
								<div class="wraps">
									<div class="bar bar1" style="height: 140px;" title="3.5">3.5</div>
								</div>
								<span class="tt">교수6</span>
							</div>
							<!-- s_cht3 -->
			
							<!-- s_cht3 -->
							<div class="cht3">
								<div class="wraps">
									<div class="bar bar1" style="height: 140px;" title="3.5">3.5</div>
								</div>
								<span class="tt">교수7</span>
							</div>
							<!-- s_cht3 -->
			
							<!-- s_cht3 -->
							<div class="cht3">
								<div class="wraps">
									<div class="bar bar1" style="height: 180px;" title="4.5">4.5</div>
								</div>
								<span class="tt">교수8</span>
							</div>
							<!-- s_cht3 -->
			
							<!-- s_cht3 -->
							<div class="cht3">
								<div class="wraps">
									<div class="bar bar2" style="height: 180px;" title="4.5">4.5</div>
								</div>
								<span class="tt">My<br>전체
								</span>
							</div>
							<!-- s_cht3 -->
			
							<!-- s_cht3 -->
							<div class="cht3">
								<div class="wraps">
									<div class="bar bar3" style="height: 160px;" title="4.0">4.0</div>
								</div>
								<span class="tt">학년<br>전체
								</span>
							</div>
							<!-- s_cht3 -->
			
							<!-- s_cht3 -->
							<div class="cht3">
								<div class="wraps">
									<div class="bar bar4" style="height: 140px;" title="3.5">3.5</div>
								</div>
								<span class="tt">전년</span>
							</div>
							<!-- s_cht3 -->
			
			
						</div>
						<!-- s_cht_wrap -->
			
					</div>
					<!-- e_wrap_wrap -->
			
					<!-- s_wrap_wrap -->
					<div class="wrap_wrap" id="fcDiv">
						<p class="tt">졸업역량 연관성 분석</p>
			
						<!-- s_tab_table_u ttb1 -->
						<table class="tab_table_u ttb1">
							<tbody id="fcListAdd">
								<tr class="">
									<td class="th01 w51">졸업역량</td>
									<td class="th01 w52">시수</td>
									<td class="th01 w51">졸업역량</td>
									<td class="th01 w52">시수</td>
								</tr>						
							</tbody>
						</table>
						<!-- e_tab_table_u ttb1 -->
					</div>
					<!-- e_wrap_wrap -->
			
					<!-- s_wrap_wrap -->
					<div class="wrap_wrap" id="unitDiv">
						<p class="tt">단원 및 학습성과 분석</p>
			
						<!-- s_tab_table_u ttb1 -->
						<table class="tab_table_u ttb1" id="unitTab">
							<tbody>
								<tr class="">
									<td class="th01 w11">단원명<br>(UNIT)
									</td>
									<td class="th01 w11">수업제목<br>(시수)
									</td>
									<td class="th01 w13">시수</td>
									<td colspan="2" class="th01 w14">구분</td>
									<td class="th01 w13">영역</td>
									<td class="th01 w13">수준</td>
									<td class="th01 w12">기술</td>
									<td class="th01 w11">수업<br>방법
									</td>
									<td class="th01 w11">평가<br>방법
									</td>
								</tr>
							</tbody>
						</table>
						<!-- e_tab_table_u ttb1 -->
					</div>
					<!-- e_wrap_wrap -->
			
					<!-- s_wrap_wrap -->
					<div class="wrap_wrap">
						<p class="tt">임상표현 연관성 분석</p>
			
						<!-- s_tab_table_u ttb1 -->
						<table class="tab_table_u ttb1">
							<tbody id="clinicListAdd">
								<tr class="">
									<td class="th01 w61">임상표현</td>
									<td class="th01 w62">시수</td>
									<td class="th01 w61">임상표현</td>
									<td class="th01 w62">시수</td>
									<td class="th01 w61">임상표현</td>
									<td class="th01 w62">시수</td>
									<td class="th01 w61">임상표현</td>
									<td class="th01 w62">시수</td>
								</tr>
														
							</tbody>
						</table>
						<!-- e_tab_table_u ttb1 -->
					</div>
					<!-- e_wrap_wrap -->
			
					<!-- s_wrap_wrap -->
					<div class="wrap_wrap">
						<p class="tt">진단명 연관성 분석</p>
			
						<!-- s_tab_table_u ttb1 -->
						<table class="tab_table_u ttb1">
							<tbody id="diaListAdd">
								<tr class="">
									<td class="th01 w61">진단명</td>
									<td class="th01 w62">시수</td>
									<td class="th01 w61">진단명</td>
									<td class="th01 w62">시수</td>
									<td class="th01 w61">진단명</td>
									<td class="th01 w62">시수</td>
									<td class="th01 w61">진단명</td>
									<td class="th01 w62">시수</td>
								</tr>
													
							</tbody>
						</table>
						<!-- e_tab_table_u ttb1 -->
					</div>
					<!-- e_wrap_wrap -->
	
	
					</div>
				</div>
				<!-- s_content -->
	
			</div>
		<!-- e_운영보고서 팝업 -->
		<!-- s_ 팝업 : 졸업역량 수업 상세 -->
		<div id="m_pop2" class="pop_up_grdclass mo1">
			<div class="pop_wrap">
				<button class="pop_close close2" type="button">X</button>
	
				<p class="t_title" data-name="popupTitle"></p>
	
				<!-- s_table_b_wrap -->
				<div class="table_b_wrap">
					<!-- s_pht -->
					<div class="pht">
	
						<div class="conwrap" data-name="popupListAdd">														
						</div>
	
					</div>
					<!-- e_pht -->
				</div>
			</div>
		</div>
		<!-- e_ 팝업 : 졸업역량 수업 상세 -->
</body>
</html>