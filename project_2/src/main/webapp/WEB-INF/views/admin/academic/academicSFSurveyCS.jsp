<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<link rel="stylesheet" href="${CSS}/nwagon.css" type="text/css">
<style>
/* s_schwrap_uselectbox */
.schwrap_uselectbox{width: 469px;padding: 0 !important;max-height: 36px;height: 36px !important;line-height: 30px;box-sizing: border-box;display: inline-block;border: solid 1px rgba(83, 173, 243, 1);background: rgba(247, 251, 253, 1);float: left;}
.schwrap_uselectbox:hover{border: solid 1px rgba(0, 171, 216, 1);background:rgba(223, 244, 255, .7);color:rgba(1, 96, 121,1);box-sizing: border-box;transition:.7s;}	
.schwrap_uselectbox div.uselectbox{position: relative;top: 0;height: 34px !important;max-height: initial;line-height: 35px !important;left: 0;display:inline-block;width:100%;cursor:pointer;text-align:left;clear:both;float:left;margin: 0;padding: 0;border: 0px;box-sizing: border-box;}	
.schwrap_uselectbox span.uselected{width: 407px;background: rgba(255, 255, 255, 0);overflow:hidden;position:relative;top: 0px;float:left;height: 34px;font-size: 15px;z-index:1;color: rgba(36, 116, 185, 1);text-align:left;text-indent: 0;padding:0 0 0 10px;margin: 0;box-sizing: border-box;}	
.schwrap_uselectbox span.uarrow{position:relative;top: 0;text-indent: 0;display: inline-block;width: 60px;float: right;height: 34px;line-height: 34px;z-index:1;box-sizing: border-box;text-align: center;font-size: 14px;background: rgba(83, 173, 243, 1);padding: 0;margin: 0;color: rgb(255, 255, 255);}			
.schwrap_uselectbox span.uarrow:hover{background: rgba(36, 119, 187, 1);border-right:solid 7px rgb(200, 200, 200);transition:.7s;}	
.schwrap_uselectbox div.uoptions{position:absolute;top: 34px;left: -1px;width: 467px;height: auto;max-height: 190px;line-height: 28px;border: solid 1px rgba(83, 173, 243, 1);border-bottom-right-radius:5px;border-bottom-left-radius:5px;overflow-x: hidden;overflow-y: auto;background: rgb(255, 255, 255);padding: 0;display:none;color:rgba(1, 96, 121,1);opacity: 1;box-sizing: content-box;z-index: 9999;box-shadow: 3px 3px 5px rgb(163, 163, 163);}	
.schwrap_uselectbox .uoption{position:relative;top: 0px;left:-1px;display:table;width: 100%;height: auto;line-height: normal;font-size: 15px;margin: -2px 0 0 0;padding: 5px 0 3px 10px;text-align:left;text-indent: 0;border-bottom: dashed 1px rgb(102, 167, 223);opacity: 1;color: rgba(85, 85, 85, 1);box-sizing: border-box;}	
.schwrap_uselectbox .uoption.schopt1{padding: 9px 0 7px 10px;background: rgba(117, 161, 216, .3);color: rgb(90, 90, 90);border-bottom: solid 1px rgb(255, 255, 255);line-height: 20px;height: 22px;}
.schwrap_uselectbox .uoption:hover{color:rgba(255,255,255,1);background: rgba(117, 161, 216, .5);transition:.7s;}
.schwrap_uselectbox .a_mp{display: table;height: auto;width: auto;margin: 0 0 0 0 ;padding:0;float: left;letter-spacing: -1.5px;border-radius: 0;background: rgba(226, 241, 253, 0);box-shadow: none;overflow: visible;}
.schwrap_uselectbox .a_mp .pt01{display: inline-block;background: rgba(255, 255, 255, 1);width: 28px;height: 28px;margin: 0px 5px 0 0;padding:0;border: solid 1px rgba(134, 188, 241, 1);border-radius: 22px;overflow: hidden;}
.schwrap_uselectbox .a_mp .pt01 img{display:block;width: 30px;height: auto;margin: 0;padding:0;background: rgba(255, 255, 255, 1);}
.schwrap_uselectbox .a_mp .ssp1{display:inline-block;width: auto;height: auto;font-size:14px;color: rgba(85, 85, 85, 1);margin: 0 7px 0 0;padding: 0;line-height: 1.2;letter-spacing: 0px;text-align: left;text-indent: 5px;}
.ip_tt1{display:inline-block;box-sizing: border-box;background: rgba(255, 255, 255, 1);width: 100%;text-align: left;text-indent:0px;
	line-height: 34px;height: 34px;/* border: solid 1px rgba(83, 173, 243, 1); */font-size: 14px;letter-spacing: 1px;
	color: rgba(36, 116, 185, 1);margin: 8px 0 9px 9px;padding:0 10px;cursor:pointer;float: left;}
.schwrap_uselectbox input.uselected, .schwrap_uselectbox input.uselected:hover{margin: 0; border-style: none solid solid none}
/* e_schwrap_uselectbox */
</style>
<script src="${JS}/lib/nwagon.js"></script>
<script>
var ajaxTask;
			$(document).ready(function() {
				//getCurrList("","","");
			});
			
			function getCurrList(curr_seq, lp_seq, user_seq){
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/SFSurvey/list",
		            data: {       
		            	"curr_seq" : curr_seq
		            	,"lp_seq" : lp_seq
		            	,"user_seq" : user_seq
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	var htmls = "";
		            	var aca_state = "";
	                	switch(data.acaInfo.aca_state){
                            case "01" : aca_state = "개설(비공개)";break;
                            case "02" : aca_state = "개설(공개)";break;
                            case "03" : aca_state = "학사 중(수업 중)";break;
                            case "04" : aca_state = "학사종료(미확정)";break;
                            case "05" : aca_state = "학사종료(성적산정)";break;
                            case "06" : aca_state = "학사종료(성적발표)";break;
                            case "07" : aca_state = "성적발표 종료(미확정)";break;
                            case "08" : aca_state = "학사최종종료(확정)";break;                            
                        }
	                	if(!isEmpty(data.acaInfo.academic_name))
	                		$("#academicNameTitle").text(data.acaInfo.academic_name);
	                	else
	                		$("#academicNameTitle").hide();

		            	$("#academicState").text(aca_state); //타이틀 학사상태
		            	
		            	$("#listAdd").empty();
		            	if(data.status=="200"){
		            		$.each(data.surveyList, function(index){
		            			htmls ='<tr>'
									+'<td class="td_1 w2 bd01">N0001</td>'
									+'<td class="td_1 w4 bd01 ta_l"><div class="t_wrap2">'+this.curr_name+'</div></td>'
									+'<td class="td_1 w4 bd01 ta_l"><div class="t_wrap3">'+this.lesson_subject+'</div></td>'
									+'<td class="td_1 w3 bd01">'+this.lesson_date+'</td>'
									+'<td class="td_1 w3 bd01">'+this.name+'</td>'
									+'<td class="td_1 w1 bd01"></td>'
									+'<td class="td_1 w3 bd01">'
									+'<span class="sp_n"></span><span class="sign">/</span>'
									+'<span	class="sp_n"></span><span class="">진행 중</span>'
									+'</td>'
									+'<td class="td_1 w3_1 bd01 open1" onClick="getSTList('+this.curr_seq+','+this.lp_seq+');"><div class="sp_wrap">'
									+'<span class="sp_n">'+this.sr_cnt+'</span><span class="sign">/</span>'
									+'<span	class="sp_n">'+this.st_cnt+'</span></div>'
									+'</td>'
									+'<td class="td_1 w2_1 bd01"><button class="btn_sv1 open2" onClick="getResultList('+this.lp_seq+');">조회</button></td>'
									+'</tr>';
								$("#listAdd").append(htmls);
		            		});
		            		
		            		$("td[data-name=aca_system_name]").text(data.currInfo.aca_system_name);
		            		$("td[data-name=academic_name]").text(data.currInfo.academic_name);
		            		$("td[data-name=curr_date]").text(data.currInfo.curr_start_date+"~"+data.currInfo.curr_end_date+" ("+data.currInfo.curr_week+"주 / "+data.currInfo.period_cnt+"차시)");
		            		$("td[data-name=curr_name]").text(data.currInfo.curr_name);
		            		$("td[data-name=complete_name]").text(data.currInfo.complete_name);
		            		$("td[data-name=administer_name]").text(data.currInfo.administer_name);
		            		$("td[data-name=grade]").text(data.currInfo.grade);
		            		
		            		if(data.currInfo.req_charge == 0)
		            			$("span[data-name=req_charge]").text("");
		            		else
		            			$("span[data-name=req_charge]").text(numberWithCommas(data.currInfo.req_charge+"")+"원");
		            		
		            		var mpfList = "";
							$.each(data.mpfList, function(index){
		            			if(index != 0)
		            				mpfList += ",";
		            			if(isEmpty(this.department_name))
		            				mpfList += this.name;
		            			else
		            				mpfList += this.name+"("+this.department_name+")";
		            		});
							$("td[data-name=mpfList]").text(mpfList);
		            		
							var dpfList = "";
							$.each(data.dpfList, function(index){
								if(index != 0)
									dpfList += ",";
		            			if(isEmpty(this.department_name))
		            				dpfList += this.name;
		            			else
		            				dpfList += this.name+"("+this.department_name+")";
							});
							$("td[data-name=pfList]").text(dpfList);
							
							$("#openClass").attr("data-value",data.currInfo.end_chk);
							
							$("span[data-name=currSubmitCnt]").text(data.stCnt.submit_cnt);
							$("span[data-name=currTotalCnt]").text(data.stCnt.st_cnt);
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
			
			//검색조건 교육과정정
			function currClick(curr_seq){
				getCurrList(curr_seq, "", "");
				$("#lp_seq").text("선택");
				$("#lp_seq").attr("data-value","");
				$("#user_seq").text("선택");
				$("#user_seq").attr("data-value","");
				getLessonNameList(curr_seq);
			}
			
			function lpClick(lp_seq){
				getCurrList($("#curr_seq").attr("data-value"),lp_seq,$("input[name=user_seq]").val());			
				
			}
						
			//검색조건 수업리스트 가져오기
			function getLessonNameList(curr_seq){
				if(isEmpty(curr_seq))
					return;
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/SFSurvey/lessonList",
		            data: {       
		            	"curr_seq" : curr_seq
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	var htmls = "";
		            	if(data.status=="200"){
		            		$.each(data.lessonList, function(index){
		            			htmls +='<span class="uoption" data-value="'+this.lp_seq+'" onClick="lpClick('+this.lp_seq+');">'+this.lesson_subject+'</span>';								
		            		});	
		            		$("#lpListAdd").html(htmls);
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });				
			}
			
			//교수 검색
			function searchPf(event) {
				
				var user_name = $.trim($(":input[name='user_name']").val());
				
				//검색 키워드가 이전과 동일하면 검색 안함
				if (user_name == $(":input[name='user_name_search']").val()) {
					return;
				}
				
				//이전에 실행중인 ajax 작업이 있으면 검색 안함
				if(0 < $.active) {
					return;
		        }
				
				var uoptions = $(".pfSearch .uoptions");
				
				//검색값이 없으면 요청 취소하고 셀렉트박스 삭제
				if (user_name.length == 0) {
					uoptions.empty().hide();
					return;
				}
				
				clearTimeout(ajaxTask);
				
				ajaxTask = setTimeout(function() {
					
					$(":input[name='user_name_search']").val(user_name);
					
					$.ajax({
			            type: "POST",
			            url: "${HOME}/ajax/pf/lesson/getPfList",
			            data: {
			            	"user_name": $(":input[name='user_name_search']").val()
			            },
			            dataType: "json",
			            success: function(data, status) {
			            	uoptions.empty();
			            	
			            	if (0 < data.pfList.length) {
			            		uoptions.show();
			            	}
			            	
			            	$.each(data.pfList, function() {
			            		var userPic = "${IMG}/ph_2.png";
			                	
			                	if (this.picture_path != null) {
			                		userPic = "${RES_PATH}" + this.picture_path;
			                	}
			            		
			            		var html = '<a onclick="javascript:selectPf(\'' + this.user_seq + '\', this);"><span class="uoption">'
				                        + '<span class="a_mp"><span class="pt01"><img src="' + userPic + '" alt="등록된 사진 이미지" class="pt_img"></span><span class="ssp1"><span class="userName">' + this.name + '</span> (' + this.department + ')</span></span>'
				                    	+ '</span></a>';
			                    uoptions.append(html);
			            	});
			            },
			            error: function(xhr, textStatus) {
			            }
			        });
				}, 100);
			}
			
			//교수 선택완료 처리
			function selectPf(user_seq, element) {
				var uoption = $("span.a_mp", element);
				$("#pfSelected span.uselected").append(uoption);
				$("#pfSelected").show();
				$(".schwrap_uselectbox input.uselected").hide();
				var user_name = $("span.userName", uoption).html();
				$(":input[name='user_seq']").val(user_seq);
				$(":input[name='user_name']").val(user_name);
				$(":input[name='user_name_search']").val(user_name);
				$(".schwrap_uselectbox .uoptions").empty().hide();
				getCurrList($("#curr_seq").attr("data-value"),$("#lp_seq").attr("data-value"), user_seq);
			}
			
			//기존 선택을 삭제하고 교수 검색 필드 출력
			function startSearchPf() {
				$("#pfSelected").hide();
				$("#pfSelected span.uselected").empty();
				$(".schwrap_uselectbox input.uselected").show();
				$(":input[name='user_seq']").val("");
				$(":input[name='user_name_search']").val("");
				$(":input[name='user_name']").val("");
				$(":input[name='user_name']").focus();
				getCurrList($("#curr_seq").attr("data-value"),$("#lp_seq").attr("data-value"), "");
			}
			
			//수업만족도 미제출 학생 리스트
			function getSTList(curr_seq, lp_seq){
				if(isEmpty(curr_seq))
					return;
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/SFSurvey/stList",
		            data: {       
		            	"curr_seq" : curr_seq
		            	,"lp_seq" : lp_seq
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	var htmls = "";
		            	if(data.status=="200"){
		            		$("span[data-name=lessonSubject]").text(data.lpSubject.lesson_subject);
		            		$("span[data-name=st_cnt]").text(data.stCnt);
		            		$.each(data.stList, function(index){
		            			htmls +='<li class="">'
									+'<span class="sp_1">'+this.name+'</span>'
									+'<span class="sp_3">'+this.tel+'</span>'
									+'</li>';								
		            		});	
		            		$("#stList").html(htmls);
		            		$("#m_pop1").show();
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });	
			}
			
			//과정만족도 미제출 학생 리스트
			function getCurrSTList(){
				if(isEmpty($("#curr_seq").attr("data-value")))
					return;
				
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/SFSurvey/curStList",
		            data: {       
		            	"curr_seq" : $("#curr_seq").attr("data-value")
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	var htmls = "";
		            	if(data.status=="200"){
		            		$("span[data-name=pop_curr_name]").text(data.currInfo.curr_name);
		            		$("span[data-name=curr_st_cnt]").text(data.stCnt);
		            		$.each(data.stList, function(index){
		            			htmls +='<li class="">'
									+'<span class="sp_1">'+this.name+'</span>'
									+'<span class="sp_3">'+this.tel+'</span>'
									+'</li>';								
		            		});	
		            		$("#currStListAdd").html(htmls);
		            		$("#m_pop1b").show();
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });	
			}			
			
			//수업만족도 조회
			function getResultList(lp_seq){
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/SFSurvey/resultList",
		            data: {       
		            	"lp_seq" : lp_seq
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	var htmls = "";
		            	$("#surveyListAdd").empty();
		            	if(data.status=="200"){
		            		$("input[name=lp_seq]").val(lp_seq);
		            		$("#m_pop2").show();
		            		var pfName = "교수 미등록";
		            		if(!isEmpty(data.titleInfo.name))
		            			pfName = data.titleInfo.name+"("+data.titleInfo.code_name+")";
		            		
		            		$("span[data-name=pfInfo]").text(pfName);
		            		
		            		if(isEmpty(data.titleInfo.picture_path))
		            			$("#pfPicture").attr("src","${IMG}/ph_3.png");
		            		else
		            			$("#pfPicture").attr("src","${RES_PATH}"+data.titleInfo.picture_path);
		            		
		            		var period = data.titleInfo.period.split(",");
		                	
		                	if(period.length > 1)
		                		period = "["+period[0]+"~"+period[period.length-1]+"교시]";
		                	else
		                		period = "["+data.titleInfo.period+"교시]";
		            		
		            		
		            		
		            		$("td[data-name=lpSubject]").text(data.titleInfo.lesson_subject);
		            		$("td[data-name=currName]").text(data.titleInfo.curr_name);
		            		$("td[data-name=lpDate]").text(data.titleInfo.lesson_date+ " " + period);
		            		$("span[data-name=submitStCnt]").text(data.titleInfo.sr_cnt);
		            		$("span[data-name=totalStCnt]").text(data.titleInfo.st_cnt);
		            		$("span[data-name=unsubmissionStCnt]").text(data.titleInfo.st_cnt-data.titleInfo.sr_cnt);
		            		$.each(data.resultList, function(index){
		            			htmls = "";
		            			if(this.srh_type=="1"){
		            				var sri_item_explan = this.sri_item_explan.split("\|");
		            				var sri_seq =  this.sri_seq.split("\|");
		            				var answer_sri_seq = this.answer_sri_seq.split("\|");
		            				
		            				var answer_cnt = [];
		            				
		            				var color = ["#63B2E6","#78BFA3","#E9D25A","#BDBDBD","#7A7A7A","#E2FCFF","#FF7171"];
		            				
		            				for(var i=0; i<sri_item_explan.length;i++){
		            					answer_cnt[i] = 0;
									}
		            				
		            				for(var i=0; i<sri_seq.length;i++){
		            					for(var j=0; j<answer_sri_seq.length;j++){
		            						if(sri_seq[i] == answer_sri_seq[j])
		            							answer_cnt[i]++;
		            					}
									}
		            				
		            				var print_class = '';
		            				
		            				if(index != 0 && index%2==0)
		            					print_class = 'style="page-break-before:always;"';
		            					
		            				htmls = '<div class="tb_wrap" '+print_class+'>'
				    					+'<div class="wrap_wrap">'
				    					+'<p class="tt">'+this.srh_num+'. '+this.srh_subject+'</p>';

		        					if(!isEmpty(this.srh_explan))
				    					htmls+='<p class="tt_s">'+this.srh_explan+'</p>';
				    					
			    					htmls+='<div class="wrap_s1">'
				    					+'<div class="cht1">'
				    					+'<div class="wraps">'
				    					+'<div id="chart'+index+'" class="chart100"></div>'
				    					+'<div class="fields1">';
				    				//챠트 아래 항목
				    				for(var i=0; i<sri_item_explan.length;i++){
				    					htmls+='<div class="wrap"><span class="rt'+(i+1)+'" style="background:'+color[i]+';"></span><span class="tt1">'+sri_item_explan[i]+'</span></div>'
									}
				    				
				    				var answerCnt = 0;
				    				
				    				if(!isEmpty(answer_sri_seq))
				    					answerCnt = answer_sri_seq.length;
				    						
				    			
				    				htmls += '</div></div></div></div>'
				    					+'<div class="wrap_s2">'
				    					+'<table class="tab_table_u ttb1">'
				    					+'<tr class="">'
				    					+'<td class="th01 w1">점수</td>'
				    					+'<td class="th01 w2 td_l">항목</td>'
				    					+'<td class="th01 w3"><span class="sp_sign">응답</span>'
				    					+'<span class="sp_n">'+answerCnt+'</span><span class="sp_sign">명</span></td>'
				    					+'</tr>'
				    					
				    				//답가지 응답 수
			    					for(var i=0; i<sri_item_explan.length;i++){
				    					htmls+='<tr class="">'
				    						+'<td class="th01"></td>'
				    						+'<td class="td1 td_l">'+sri_item_explan[i]+'</td>'
				    						+'<td class="td1"><span class="sp_n">'+answer_cnt[i]+'</span>'
				    						+'<span class="sp_sign">명</span></td>'
				    						+'</tr>'				    						
									}
				    				
				    				htmls += '</table></div></div></div>';
				    				
				    				$("#surveyListAdd").append(htmls);
				    				var options = {
			    						'dataset': {
			    							title: '',
			    							values:answer_cnt,
			    							colorset:color,
			    							fields:sri_item_explan 
			    						},
			    						'donut_width' : 100, 
			    						'core_circle_radius':0,
			    						'chartDiv': 'chart'+index,
			    						'chartType': 'pie',
			    						'chartSize': {width:200, height:400}
			    					};
			    					Nwagon.chart(options);
		            			}else if(this.srh_type=="2"){
		            				var answer = this.answer.split("\|\|");
		            				
		            				htmls += '<div class="tb_wrap">'
		        						+'<div class="wrap_wrap">'
		        						+'<p class="tt">'+this.srh_num+'. '+this.srh_subject+'</p>';
		        						
		        					if(!isEmpty(this.srh_explan))
		        						htmls+='<p class="tt_s">'+this.srh_explan+'</p>';
		        						
	        						htmls+='<div class="wrap_s3">'
		        						+'<table class="tab_table_u ttb2">'
		        						+'<tr class="">'
		        						+'<td class="th01 w4">no</td>'
		        						//+'<td class="th01 w5">응답자</td>'
		        						+'<td class="th01 w6" colspan="2">응답 내용</td>'
		        						+'</tr>';
		        					if(!isEmpty(answer)){
			        					for(var i=0; i<answer.length; i++){
			        						var answer_detail = answer[i].split("\|");
			        						htmls+='<tr class="">'
			        						+'<td class="th01">'+(i+1)+'</td>'
			        						/* +'<td class="td1 td_l"><span class="sp_1">'+answer_detail[0]+'</span>'
			        						+'<span class="sign">(</span><span class="sp_2">'+this.m_aca_name+' '+this.aca_name+'</span>'
			        						+'<span class="sign">)</span></td>' */
			        						+'<td class="td1 td_l" colspan="2">'+answer_detail[1]+'</td>'
			        						+'</tr>';
			        					}
		        					}
		        					
		        					htmls +='</table></div></div></div>';
		        					$("#surveyListAdd").append(htmls);
		            			}		
		            		});	
		            		$("#m_pop2").show();
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });					
			}
					
			//과정만족도 조회
			function getCurrResultList(){

				if(isEmpty($("#curr_seq").attr("data-value"))){
					alert("조회할 교육과정을 선택하세요");
					return;
				}
				
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/SFSurvey/currResultList",
		            data: {       
		            	"curr_seq" : $("#curr_seq").attr("data-value")
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	var htmls = "";
		            	$("#currSurveyListAdd").empty();
		            	if(data.status=="200"){

		            		var mpfList = "";
		            		
		            		$.each(data.mpfList, function(index){
		            			if(index != 0)
		            				mpfList += "<br>";
		            			
		            			if(isEmpty(this.department_name))
		            				mpfList += this.name;
		            			else
		            				mpfList += this.name+"("+this.department_name+")";
		            		});
		            		
							var dpfList = "";
		            		
		            		$.each(data.dpfList, function(index){
		            			if(index != 0)
		            				dpfList += "<br>";
		            			
		            			if(isEmpty(this.department_name))
		            				dpfList += this.name;
		            			else
		            				dpfList += this.name+"("+this.department_name+")";
		            		});
		            		
		            		$("td[data-name=pop_mpfList]").html(mpfList);
		            		$("td[data-name=pop_dpfList]").html(dpfList);
		            		$("td[data-name=pop_curr_name]").text(data.currInfo.curr_name);
		            		$("td[data-name=pop_curr_date]").text(data.currInfo.curr_start_date+"~"+data.currInfo.curr_end_date+" "+data.currInfo.curr_week+"주"+" "+data.currInfo.period_cnt+"차시");
		            		$("span[data-name=pop_complete_name]").text(data.currInfo.complete_name);
		            		$("span[data-name=pop_administer_name]").text(data.currInfo.administer_name);
		            		$("span[data-name=pop_grade]").text(data.currInfo.grade);
		            				            		
		            		if(data.currInfo.req_charge == 0)
		            			$("span[data-name=pop_req_charge]").text("");
		            		else
		            			$("span[data-name=pop_req_charge]").text(numberWithCommas(data.currInfo.req_charge+""));
		            		
		            		$("span[data-name=pop_submitStCnt]").text(data.stCnt.submit_cnt);
		            		$("span[data-name=pop_totalStCnt]").text(data.stCnt.st_cnt);
		            		$("span[data-name=pop_unSubmitStCnt]").text(data.stCnt.st_cnt-data.stCnt.submit_cnt);
		            			            		
		            		
		            		$.each(data.resultList, function(index){
		            			htmls = "";
		            			if(this.srh_type=="1"){
		            				var sri_item_explan = this.sri_item_explan.split("\|");
		            				var sri_seq =  this.sri_seq.split("\|");
		            				var answer_sri_seq = this.answer_sri_seq.split("\|");
		            				
		            				var answer_cnt = [];
		            				
		            				var color = ["#63B2E6","#78BFA3","#E9D25A","#BDBDBD","#7A7A7A","#E2FCFF","#FF7171"];
		            				
		            				for(var i=0; i<sri_item_explan.length;i++){
		            					answer_cnt[i] = 0;
									}
		            				
		            				for(var i=0; i<sri_seq.length;i++){
		            					for(var j=0; j<answer_sri_seq.length;j++){
		            						if(sri_seq[i] == answer_sri_seq[j])
		            							answer_cnt[i]++;
		            					}
									}
		            				
		            				var print_class = '';
		            				
		            				if(index != 0 && index%2==0)
		            					print_class = 'style="page-break-before:always;"';
		            					
		            				htmls = '<div class="tb_wrap" '+print_class+'>'
				    					+'<div class="wrap_wrap">'
				    					+'<p class="tt">'+this.srh_num+'. '+this.srh_subject+'</p>';

		        					if(!isEmpty(this.srh_explan))
				    					htmls+='<p class="tt_s">'+this.srh_explan+'</p>';
				    					
			    					htmls+='<div class="wrap_s1">'
				    					+'<div class="cht1">'
				    					+'<div class="wraps">'
				    					+'<div id="chart'+index+'" class="chart100"></div>'
				    					+'<div class="fields1">';
				    				//챠트 아래 항목
				    				for(var i=0; i<sri_item_explan.length;i++){
				    					htmls+='<div class="wrap"><span class="rt'+(i+1)+'" style="background:'+color[i]+';"></span><span class="tt1">'+sri_item_explan[i]+'</span></div>'
									}
				    				
				    				var answerCnt = 0;
				    				
				    				if(!isEmpty(answer_sri_seq))
				    					answerCnt = answer_sri_seq.length;
				    						
				    			
				    				htmls += '</div></div></div></div>'
				    					+'<div class="wrap_s2">'
				    					+'<table class="tab_table_u ttb1">'
				    					+'<tr class="">'
				    					+'<td class="th01 w1">점수</td>'
				    					+'<td class="th01 w2 td_l">항목</td>'
				    					+'<td class="th01 w3"><span class="sp_sign">응답</span>'
				    					+'<span class="sp_n">'+answerCnt+'</span><span class="sp_sign">명</span></td>'
				    					+'</tr>'
				    					
				    				//답가지 응답 수
			    					for(var i=0; i<sri_item_explan.length;i++){
				    					htmls+='<tr class="">'
				    						+'<td class="th01"></td>'
				    						+'<td class="td1 td_l">'+sri_item_explan[i]+'</td>'
				    						+'<td class="td1"><span class="sp_n">'+answer_cnt[i]+'</span>'
				    						+'<span class="sp_sign">명</span></td>'
				    						+'</tr>'				    						
									}
				    				
				    				htmls += '</table></div></div></div>';
				    				
				    				$("#currSurveyListAdd").append(htmls);
				    				var options = {
			    						'dataset': {
			    							title: '',
			    							values:answer_cnt,
			    							colorset:color,
			    							fields:sri_item_explan 
			    						},
			    						'donut_width' : 100, 
			    						'core_circle_radius':0,
			    						'chartDiv': 'chart'+index,
			    						'chartType': 'pie',
			    						'chartSize': {width:200, height:400}
			    					};
			    					Nwagon.chart(options);
		            			}else if(this.srh_type=="2"){
		            				var answer = this.answer.split("\|\|");
		            				
		            				htmls += '<div class="tb_wrap">'
		        						+'<div class="wrap_wrap">'
		        						+'<p class="tt">'+this.srh_num+'. '+this.srh_subject+'</p>';
		        						
		        					if(!isEmpty(this.srh_explan))
		        						htmls+='<p class="tt_s">'+this.srh_explan+'</p>';
		        						
	        						htmls+='<div class="wrap_s3">'
		        						+'<table class="tab_table_u ttb2">'
		        						+'<tr class="">'
		        						+'<td class="th01 w4">no</td>'
		        						//+'<td class="th01 w5">응답자</td>'
		        						+'<td class="th01 w6" colspan="2">응답 내용</td>'
		        						+'</tr>';
		        					if(!isEmpty(answer)){
			        					for(var i=0; i<answer.length; i++){
			        						var answer_detail = answer[i].split("\|");
			        						htmls+='<tr class="">'
			        						+'<td class="th01">'+(i+1)+'</td>'
			        						/* +'<td class="td1 td_l"><span class="sp_1">'+answer_detail[0]+'</span>'
			        						+'<span class="sign">(</span><span class="sp_2">'+this.m_aca_name+' '+this.aca_name+'</span>'
			        						+'<span class="sign">)</span></td>' */
			        						+'<td class="td1 td_l" colspan="2">'+answer_detail[1]+'</td>'
			        						+'</tr>';
			        					}
		        					}
		        					
		        					htmls +='</table></div></div></div>';
		        					$("#currSurveyListAdd").append(htmls);
		            			}		
		            		});	
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });					
			}
			
			function getAcademic(){
		    	post_to_url('${HOME}/admin/academic/academicReg/academicRegBasicInfo/create', {"aca_seq" : "${S_ACA_SEQ}"});
		    }
			
			function surveyPrint() {
				var initBody = document.body.innerHTML;
				document.body.innerHTML = "<div class='class_survey' style='display:block;position:relative;'><div class='content' style='margin:0px;'>"+document.getElementById("printDiv").innerHTML+"</div></div>";
				 window.onbeforeprint = function () {
					document.body.innerHTML = "<div class='class_survey' style='display:block;'><div class='content'>"+document.getElementById("printDiv").innerHTML+"</div></div>";
					$(".btn_prt").hide();
				}
				window.onafterprint = function () {

					document.body.innerHTML = initBody;
					$(".btn_prt").show();
				}
				window.print();
			}
			
			function currSurveyPrint() {
				var initBody = document.body.innerHTML;
				document.body.innerHTML = "<div class='class_survey' style='display:block;position:relative;'><div class='content' style='margin:0px;'>"+document.getElementById("printDivCurr").innerHTML+"</div></div>";
				 window.onbeforeprint = function () {
					document.body.innerHTML = "<div class='class_survey' style='display:block;'><div class='content'>"+document.getElementById("printDivCurr").innerHTML+"</div></div>";
					$(".btn_prt").hide();
				}
				window.onafterprint = function () {

					document.body.innerHTML = initBody;
					$(".btn_prt").show();
				}
				window.print();
			}
			
			function surveyExcelDown(){	    	
		    	location.href='${HOME}/admin/academic/SFSurvey/excelDown?curr_seq='+$("#curr_seq").attr("data-value")
		    			+'&lp_seq='+$("#lp_seq").attr("data-value")
		    			+'&user_seq='+$("input[name=user_seq]").val();       
		    }
			
			function pageMoveScore(type, aca_seq){
		    	$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/admin/academic/academicReg/setAcaSeq",
	                data: {
	                	"aca_seq" : aca_seq
	                    },
	                dataType: "json",
	                success: function(data, status) {
	                	if(type=="curr")
	               			location.href="${HOME}/admin/academic/academicReg/currScore";
		     			else if(type=="soosi")
		     				location.href="${HOME}/admin/academic/academicReg/soosiScore";
		     			else if(type=="fe")
		     				location.href="${HOME}/admin/academic/academicReg/feScore";
	                },
	                error: function(xhr, textStatus) {
	                    alert("실패");
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                },beforeSend:function() {
	                	$.blockUI();
	                },
	                complete:function() {
	                	$.unblockUI();
	                }
	            });
		    }
			
			function getResultCurr(obj){
				if(!isEmpty($(obj).attr("data-value"))){
					if($(obj).attr("data-value") == "Y"){
						getCurrResultList();
						$("#m_pop3").show();					
					}else{
						$("#m_pop3_1").show();
					}				
				}
			}
		</script>
</head>

<body>
	<input type="hidden" name="aca_seq" id="aca_seq" value="${S_ACA_SEQ }">
	<noscript title="브라우저 자바스크립트 차단 해제 안내">
		<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
	</noscript>

	<!-- s_skipnav -->
	<div id="skipnav">
		<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
	</div>
	<!-- e_skipnav -->

	<!-- s_container_table -->
	<div id="container" class="container_table">
		<!-- s_contents -->
		<div class="contents main">

			<!-- s_left_mcon -->
			<div class="left_mcon aside_l grd">
				<div class="sub_menu adm_grd">
						<div class="title">학사관리</div>
						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">학사체계관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						 	<div class="title1">
								<span class="sp_tt">학사코드관리</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">교육과정관리</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">교육과정관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청관리</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on" onClick="location.href='${HOME}/admin/academic/academicReg/list'">학사등록관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">졸업역량관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
					</div>
			</div>
			<!-- e_left_mcon -->

			<!-- s_main_con -->
			<div class="main_con adm_grd">
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
				<!-- s_tt_wrap -->
				<div class="tt_wrap">
					<h3 class="am_tt">
						<span class="tt">학사등록관리<span class="sign1"> &gt; </span>만족도 조사</span> 
					</h3>
					<span class="sp_state">학사상태 : <span class="tt" id="academicState"></span></span>
					<h4 class="h4_tt" id="academicNameTitle"></h4>
				</div>
				<!-- e_tt_wrap -->

				<!-- s_adm_content3 -->
				<div class="adm_content3 survey">
					<!-- s_tt_wrap -->

					<!-- s_wrap_wrap -->
					<div class="wrap_wrap">

						<div class="tab_wrap_cc">
							<button class="tab01 tablinks" onclick="getAcademic();">기본정보</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/curriculumAssign'">교육과정배정</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/studentAssign'">학생배정</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/schedule'">시간표</button>
								<button class="tab01 tablinks active" onclick="location.href='${HOME}/admin/academic/SFSurvey/list'">만족도조사</button>						
								<button class="tab01 tablinks" onclick="pageMoveScore('curr','${S_ACA_SEQ }');">종합성적</button>
								<button class="tab01 tablinks" onclick="pageMoveScore('soosi','${S_ACA_SEQ }');">수시성적</button>								
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/currReport'">운영보고서</button>
						</div>

						<!-- s_wrap_wrap -->
						<div class="wrap_wrap">

							<!-- s_sch_wrap -->
							<div class="sch_wrap">
								<!-- s_rwrap -->
								<div class="rwrap">
									<!-- s_wrap_s -->
									<div class="wrap_s" style="display:block;">
										<div style="width:100%;height:36px;line-height:36px;float:left;">
											<span class="tt2" style="line-height:36px;">교육과정</span>
	
											<div class="wrap_s1_uselectbox" style="width:469px;">
												<div class="uselectbox">
													<span class="uselected" id="curr_seq" data-value="">전체</span> 
													<span class="uarrow" style="text-align: right;padding-right: 10px;">▼</span>
													<div class="uoptions" style="display: none;">
														<c:forEach var="currList" items="${currList}">
		                                                    <span class="uoption" data-value="${currList.curr_seq}" onClick="currClick('${currList.curr_seq}');">${currList.curr_name}</span>
		                                                </c:forEach>
													</div>
												</div>
											</div>
										</div>
										<div style="width:100%;height:36px;line-height:36px;float:left;">
										<span class="tt2" style="line-height:36px;">수업명</span>

											<div class="wrap_s2_uselectbox" style="width:469px;">
												<div class="uselectbox">
													<span class="uselected" id="lp_seq" data-value="">전체</span> 
													<span class="uarrow" style="width:5%;"></span>
													<div class="uoptions" style="display: none;" id="lpListAdd">
														
													</div>
												</div>
											</div>
										</div>
										<div style="width:100%;height:40px;line-height:40px;float:left;">
											<span class="tt2" style="line-height:40px;">교수명</span>							
										
											<div class="schwrap_uselectbox pfSearch">
												<div class="uselectbox">
													<input type="hidden" name="user_seq"> 
													<input type="hidden" name="user_name_search"> 
													<input type="text" name="user_name" class="uselected ip_tt1" onkeyup="searchPf();"
													style="display: inline-block;">
	
													<div id="pfSelected" style="display: none;">
														<span class="uselected"></span> 
															<a href="javascript:startSearchPf();">
														<span class="uarrow">검색</span></a>
													</div>
	
													<div class="uoptions" style="display: none;"></div>
												</div>
											</div>
										</div>
									</div>
									<!-- e_wrap_s -->
								</div>
								<!-- e_rwrap -->
							</div>
							<!-- e_sch_wrap -->

							<!-- s_btnwrap_s2 -->
							<div class="btnwrap_s2">
								<!-- s_btnwrap_s2_s -->
								<div class="btnwrap_s2_s">
									<button class="btn_down1" onClick="surveyExcelDown();">엑셀다운로드</button>
								</div>
								<!-- e_btnwrap_s2_s -->

							</div>
							<!-- e_btnwrap_s2 -->

						</div>
						<!-- e_wrap_wrap -->

						<!-- s_tb_wrap -->
						<div class="tb_wrap_n">

							<table class="mlms_tb survey tb1n">
								<tr>
									<td class="th1n w1">학사체계</td>
									<td class="td1n w2" data-name="aca_system_name"></td>
									<td class="th1n w1">학사명</td>
									<td class="td1n w2" data-name="academic_name"></td>
								</tr>
								<tr>
									<td class="th1n">기간</td>
									<td class="td1n" colspan="2" data-name="curr_date"></td>
									<td class="td1n tt1n_wrap" id="openClass" onClick="getResultCurr(this);">
										<span class="tt1n">과정만족도</span>
										<span class="num_wrap"> 
										<span class="num" data-name="currSubmitCnt"></span> 
										<span class="sign">/</span>
										<span class="num" data-name="currTotalCnt"></span>
									</span></td>
								</tr>
								<tr>
									<td class="th1n">교육과정명</td>
									<td class="td1n" colspan="3" data-name="curr_name"></td>
								</tr>
								<tr>
									<td class="th1n">이수구분</td>
									<td class="td1n" data-name="complete_name"></td>
									<td class="td1n">관리구분</td>
									<td class="td1n" data-name="administer_name"></td>
								</tr>
								<tr>
									<td class="th1n">학점</td>
									<td class="td1n" data-name="grade"></td>
									<td class="td1n">신청비용</td>
									<td class="td1n">
										<span class="tt2n" data-name="req_charge"></span>
									</td>
								</tr>
								<tr>
									<td class="th1n">책임교수</td>
									<td class="td1n" colspan="3" data-name="mpfList"></td>
								</tr>
								<tr>
									<td class="th1n">부책임교수</td>
									<td class="td1n" colspan="3" data-name="pfList"></td>
								</tr>
							</table>
						</div>
						<!-- e_tb_wrap -->

						<table class="mlms_tb survey th">
							<tr>
								<td class="th01 w2">교육과정<br>코드
								</td>
								<td class="th01 w4">교육과정명</td>
								<td class="th01 w4">수업명</td>
								<td class="th01 w3">수업일</td>
								<td class="th01 w3">교수명</td>
								<td class="th01 w1">평균점</td>
								<td class="th01 w3">수업만족도<br>여부</td>
								<td class="th01 w3_1">제출 상태</td>
								<td class="th01 w2">결과보기</td>
							</tr>
						</table>
						<!-- s_tb_wrap -->
						<div class="tb_wrap">
							<table class="mlms_tb survey" id="listAdd">
								
							</table>
						</div>
						<!-- e_tb_wrap -->
					</div>
					<!-- e_wrap_wrap -->
				</div>
				<!-- e_adm_content3 -->
			</div>

		</div>
		<!-- e_contents -->
	</div>
	<!-- e_container_table -->

	<!-- s_미제출자 리스트 -->
	<div id="m_pop1" class="pop_up_nonsub mo1">
		<input type="hidden" name="lp_seq">
		
		<div class="pop_wrap">
			<button class="pop_close close1" type="button" onClick="$('#m_pop1').hide();">X</button>

			<p class="t_title">미제출자 리스트</p>

			<!-- s_list_wrap -->
			<div class="list_wrap">

				<!-- s_wrap -->
				<div class="wrap">
					<div class="wrap_s">
						<span class="sp_1">수업명</span>
						<span class="sp_2" data-name="lessonSubject"></span> 
						<span class="sp_1">미제출자</span>
						<span class="sp_2">
							<span class="tt_s">총</span>
							<span class="sp_n" data-name="st_cnt"></span>
							<span class="tt_s">명</span>
						</span>
					</div>

					<ul class="wrap_s" id="stList">
											
					</ul>
				</div>
				<!-- e_wrap -->

			</div>
			<!-- e_list_wrap -->

		</div>
		<!-- e_pop_wrap -->
	</div>
	<!-- e_미제출자 리스트 -->

	<!-- s_수업 만족도 팝업 -->
	<div id="m_pop2" class="class_survey mo2">
		<!-- s_pop_wrap -->
		<div class="pop_wrap">
			<button class="pop_close close2" type="button" onClick="$('#m_pop2').hide();">X</button>

			<p class="t_title">수업만족도 결과</p>

			<!-- s_content -->
			<div class="content" id="printDiv">
				<div class="title_wrap">			
				   <span class="title1">수업만족도 조사</span>
			       <button class="btn_prt" title="인쇄하기" onClick="surveyPrint();">인쇄</button>				   
				</div> 
				
				<div class="top_wrap"> 		
				    <table class="top_tb">
						<tr>
							<td rowspan="4" class="w1">
							    <div class="in_wrap">
			                        <div class="in_wrap_s">
			                            <img src="" alt="평가자 사진" class="pt1" id="pfPicture">
			                        </div>
			                        <span class="btn_tt" data-name="pfInfo"></span>
			                    </div>
							</td>
							<td class="w2">교육과정명</td>
							<td class="w3" data-name="currName"></td>
						</tr>
						<tr>
							<td class="w2">수업명</td>
							<td class="w3" data-name="lpSubject"></td>
						</tr>
						<tr>
							<td class="w2">수업일</td>
							<td class="w3" data-name="lpDate"></td>
						</tr>
						<tr>
							<td colspan="2" class="w4">
							    <div class="title_wrap">
			                        <div class="tt_wrap_s pop open1a" onClick="getSTList($('#curr_seq').attr('data-value'),$('input[name=lp_seq]').val());">
						                 <span class="sp_n" data-name="submitStCnt">10</span>
						                 <span class="sign">/</span>
						                 <span class="sp_n" data-name="totalStCnt">20</span>
						                 <span class="sign">(</span>
						                 <span class="sp_n" data-name="unsubmissionStCnt">5</span>
						                 <span class="tt_s1">명 미제출</span>
						                 <span class="sign">)</span>
						            </div>
						            <div class="tt_wrap_s">	
			                             <span class="tt_s">최고점</span>
			                             <span class="sp_n"></span>
			                             <span class="tt_s">최저점</span>
			                             <span class="sp_n"></span>
			                             <span class="tt_s">평균점</span>
			                             <span class="sp_n"></span>
						            </div>
			                    </div>
							</td>
						</tr>
					</table>	
			
			   </div> 
			   
				<div id="surveyListAdd">
				
				</div>

			</div>
			<!-- e_content -->
		</div>
		<!-- e_pop_wrap -->
	</div>
	<!-- e_수업 만족도 팝업 -->
	
	<!-- s_과정만족도 팝업 -->
	<div id="m_pop3" class="class_survey cc mo3">
		<!-- s_pop_wrap -->
		<div class="pop_wrap">
			<button class="pop_close close3" type="button" onClick="$('#m_pop3').hide();">X</button>

			<p class="t_title">과정만족도 결과</p>


			<!-- s_content -->
			<div class="content" id="printDivCurr">
				<div class="title_wrap">

					<span class="title1">과정만족도 조사</span>
					<button class="btn_prt" title="인쇄하기" onClick="currSurveyPrint()">인쇄</button>

				</div>

				<div class="top_wrap">

					<table class="top_tb">
						<tr>
							<td rowspan="2" class="w1a">책임교수</td>
							<td rowspan="2" class="w1b" data-name="pop_mpfList"></td>
							<td class="w2">교육과정명</td>
							<td class="w3" data-name="pop_curr_name"></td>
						</tr>
						<tr>
							<td class="w2">교육과정기간</td>
							<td class="w3" data-name="pop_curr_date">2018.03.01 ~ 2018.05.31 8주 45차시</td>
						</tr>
						<tr>
							<td rowspan="2" class="w1a">부책임교수</td>
							<td rowspan="2" class="w1b" data-name="pop_dpfList"></td>
							<td colspan="2" class="w4">
								<div class="title_wrap">
									<div class="tt_wrap_s">
										<span class="tt_s4" data-name="pop_complete_name"></span>
									</div>
									<div class="tt_wrap_s">
										<span class="tt_s4" data-name="pop_administer_name"></span>
									</div>
									<div class="tt_wrap_s">
										<span class="sp_n2" data-name="pop_grade"></span>
										<span class="tt_s3">학점</span>
									</div>
									<div class="tt_wrap_s">
										<span class="tt_st">비용</span>
										<span class="sp_n2" data-name="pop_req_charge"></span>
										<span class="tt_s3">원</span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="w4">
								<div class="title_wrap">
									<div class="tt_wrap_s pop open1b" onclick="getCurrSTList();">
										<span class="sp_n" data-name="pop_submitStCnt"></span>
										<span class="sign">/</span>
										<span class="sp_n" data-name="pop_totalStCnt"></span>
										<span class="sign">(</span>
										<span class="sp_n" data-name="pop_unSubmitStCnt"></span>
										<span class="tt_s1">명 미제출</span>
										<span class="sign">)</span>
									</div>
									<div class="tt_wrap_s">
										<span class="tt_s">최고점</span>
										<span class="sp_n"></span>
										<span class="tt_s">최저점</span>
										<span class="sp_n"></span>
										<span class="tt_s">평균점</span>
										<span class="sp_n"></span>
									</div>
								</div>
							</td>
						</tr>
					</table>

				</div>

				<div id="currSurveyListAdd">
								
				</div>
			</div>
			<!-- e_content -->
		</div>
		<!-- e_pop_wrap -->
	</div>
	<!-- e_과정만족도 팝업 -->

	<!-- s_실시 기간이 아닙니다 팝업 -->
	<div id="m_pop3_1" class="popup_deposit_1_s3 mo3_1">
		<!-- s_pop_wrap -->
		<div class="pop_wrap">
			<button class="pop_close close3_1" type="button">X</button>

			<p class="t_title">확인1</p>

			<!-- s_pop_swrap -->
			<div class="pop_swrap">

				<div class="swrap">
					<span class="tt">교육과정이 종료된 뒤 교과정만족도 조사가 진행됩니다.<br> ( 실시
						기간이 아닙니다. )
					</span>
				</div>

			</div>
			<!-- e_pop_swrap -->
			<div class="t_dd">

				<div class="pop_btn_wrap">
					<button type="button" class="btn01"
						onclick="location.href='adm_grd_regi_r_survey.html'">확인</button>
				</div>

			</div>
		</div>
	</div>
	<!-- e_실시 기간이 아닙니다 팝업 -->
	
	<!-- s_과정만족도 미제출자 리스트 -->
	<div id="m_pop1b" class="pop_up_nonsub mo1b">
		<div class="pop_wrap">
			<button class="pop_close close1b" type="button" onClick="$('#m_pop1b').hide();">X</button>

			<p class="t_title">과정만족도 미제출자 리스트</p>

			<!-- s_list_wrap -->
			<div class="list_wrap">

				<!-- s_wrap -->
				<div class="wrap">
					<div class="wrap_s">
						<span class="sp_1">교육과정명</span>
						<span class="sp_2" data-name="pop_curr_name"></span> 
						<span class="sp_1">미제출자</span>
						<span class="sp_2">
							<span class="tt_s">총</span>
							<span class="sp_n" data-name="curr_st_cnt"></span>
							<span class="tt_s">명</span>
						</span>
					</div>

					<ul class="wrap_s" id="currStListAdd">
						
					</ul>
				</div>
				<!-- e_wrap -->

			</div>
			<!-- e_list_wrap -->

		</div>
		<!-- e_pop_wrap -->
	</div>
	<!-- e_과정만족도 미제출자 리스트 -->
</body>
</html>