<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <script>
        
	        $(document).ready(function(){
	        	getRefundInfo();
	        });		  
	        
	        function getRefundInfo(){
            	$.ajax({
            		type: "POST",
            		url: "${HOME}/ajax/admin/academic/courseApplication/refund/list",
            		data: {
            			},
           			dataType: "json",
           			success: function(data, status) {
           				if(data.status=="200"){
           					var htmls = "";
           					$("#content").html(data.list.content);
           					
           					if(!isEmpty(data.list.file_path)){
           						htmls += '<li class="li_1"><span>'+data.list.file_name+'</span>'
           						+'<button class="dw" title="다운로드" onclick="fileDownload(\'${HOME}\', \'${RES_PATH}'+data.list.file_path+'\', \'\', \''+data.list.file_name+'\');">다운로드</button></li>';           						
           						$("#fileListAdd").html(htmls);
           						
           					}
           				
           				}
        			},
        			error: function(xhr, textStatus) {
        				//alert("오류가 발생했습니다.");
        				document.write(xhr.responseText);
       				},beforeSend:function() {
  					},
  					complete:function() {
    				}
     			}); 
		  	}
		</script>
	</head>

	<body>
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
			<!-- s_container_table -->
			<div id="container" class="container_table">
				<!-- s_contents -->
				<div class="contents main">
	
					<!-- s_left_mcon -->
					<div class="left_mcon aside_l grd">
						<div class="sub_menu adm_grd">
							<div class="title">학사관리</div>
							<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">학사체계관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">학사코드관리</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on">교육과정관리</span>
							</div>
							<ul class="panel on">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">교육과정관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2 on">수강신청관리</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicReg/list'">학사등록관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">졸업역량관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
						</div>
					</div>
					<!-- e_left_mcon -->
	
					<!-- s_main_con -->
					<div class="main_con adm_grd">
						<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>	
						<!-- s_tt_wrap -->
						<div class="tt_wrap">
							<h3 class="am_tt"><span class="tt">환불규정관리</span></h3>
						</div>
						<!-- e_tt_wrap -->
	
						<!-- s_adm_content2 -->
						<div class="adm_content2">
							<!-- s_tt_wrap -->
		
							<!-- s_wrap_wrap -->
							<div class="wrap_wrap">
		
								<!-- s_btnwrap_s1f -->
								<div class="btnwrap_s1f">
									<button class="ic_v3" onclick="location.href='${HOME}/admin/academic/courseApplication/list'">수강신청 목록</button>
								</div>
								<!-- e_btnwrap_s1f -->
							</div>
							<!-- e_wrap_wrap -->
		
							<!-- s_wrap_wrap -->
							<div class="wrap_wrap">
								<!-- s_btnwrap_s2 -->		
								<ul class="attd_list" id="fileListAdd">
											
								</ul>		
								<div class="attd_con" id="content"></div>
							</div>
							<!-- e_wrap_wrap -->
		
							<!-- s_수정 삭제 -->
							<div class="btn_wrap_v"> 
								<button class="bt_2" onclick="location.href='${HOME}/admin/academic/courseApplication/refund/reg'">수정</button>
								<button class="bt_3" onclick="location.href='${HOME}/admin/academic/courseApplication/list'">취소</button>		
							</div>
							<!-- e_수정 삭제 -->
		
						</div>
						<!-- e_adm_content2 -->
					</div>
					<!-- e_main_con -->
	
				</div>
				<!-- e_contents -->
			</div>
			<!-- e_container_table -->	
	</body>
</html>