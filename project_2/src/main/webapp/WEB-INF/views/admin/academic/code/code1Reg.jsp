<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<script src="${JS}/lib/jquery-ui"></script>
<script>
	var order = "";
	$(document).ready(function() {				
		//getCode(); 
		$( ".reorder" ).sortable({
			stop : function( event, ui ) {
				$.each($("#codeListAdd div.dv1"), function(index){
					$(this).find("span[data-name=row_num]").text((index+1));
				});
			}
		});
	    $( ".reorder" ).disableSelection();
	});

	function deleteCode(obj){
		if(!confirm("삭제하시겠습니까?")){
			return;
		}
			
		$(obj).closest("div.dv1").remove();		
	}
	
	function getCode() {

		$.ajax({
			type : "POST",
			url : "${HOME}/ajax/admin/academic/codeManagement/code1/search",
			data : {
				"code_name" : ""
				,"order" : ""
			},
			dataType : "json",
			success : function(data, status) {
				var htmls = "";
					
				$.each(data.list, function(index){
					htmls+='<div class="dv1"><input type="hidden" name="code" value="'+this.osce_code+'">'
						+'<div class="wrap">'
						+'<div class="tt1">'
						+'<span class="num" data-name="row_num">'+(index+1)+'</span>'
						+'</div>'
						+'<div class="tt2">'
						+'<span class="tt" data-name="code_name">'+this.osce_name+'</span>'
						+'</div>'
						+'<div class="tt3">'
						+'<button class="odbtn_x" title="삭제하기" onClick="deleteCode(\''+this.osce_code+'\');">X</button>'
						+'</div>'
						+'</div>'
						+'</div>';					
				});
				
				$("#codeListAdd").html(htmls);
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
				$.unblockUI();
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}
	
	function insertCode(){
		
		var code_name = $("#code_name").val();
		if(isEmpty(code_name)){
			alert("추가할 항목명을 입력하세요.");
			return;
		}
		var index = $("#codeListAdd div.dv1").length;
		var htmls='<div class="dv1">'
		+'<div class="wrap">'
		+'<div class="tt1">'
		+'<span class="num" data-name="row_num">'+(index+1)+'</span>'
		+'</div>'
		+'<div class="tt2">'
		+'<span class="tt" data-name="code_name">'+code_name+'</span>'
		+'</div>'
		+'<div class="tt3">'
		+'<button class="odbtn_x" title="삭제하기" onClick="deleteCode(this);">X</button>'
		+'</div>'
		+'</div>'
		+'</div>';	
		
		$("#codeListAdd").append(htmls);
		
		$("#code_name").val("");
	}
	
	function updateCode(){
		if(!confirm("등록 하시겠습니까?")){
			return;
		}
		
		var arrayList = new Array();
		
		$.each($("#codeListAdd div.dv1"), function(index){
			var listInfo = new Object();
			listInfo.code_name=$(this).find("span[data-name=code_name]").text();
			listInfo.row_num=$(this).find("span[data-name=row_num]").text();
			arrayList.push(listInfo);
		});
		
		var list = new Object();
		list.list = arrayList;
		
		var jsonInfo = JSON.stringify(list);
		
		$.ajax({
			type : "POST",
			url : "${HOME}/ajax/admin/academic/codeManagement/code1/insert",
			data : {
				"list" : jsonInfo				 
			},
			dataType : "json",
			success : function(data, status) {
				if(data.status == "200"){
					alert("등록이 완료되었습니다.");
					location.href="${HOME}/admin/academic/codeManagement/code1";
				}
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
				$.unblockUI();
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});		
	}
</script>
</head>

<body>
	<!-- s_container_table -->
	<div id="container" class="container_table">
		<!-- s_contents -->
		<div class="contents main">

			<!-- s_left_mcon -->
			<div class="left_mcon aside_l grd">
				<div class="sub_menu adm_grd">
					<div class="title">학사관리</div>

					<!-- s_학사코드 메뉴 -->
					<div class="boardwrap">
						<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">학사체계관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on">학사코드관리</span>
							</div>
							<ul class="panel on">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>			
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2 on">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">교육과정관리</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">교육과정관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청관리</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicReg/list'">학사등록관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">졸업역량관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
					</div>
					<!-- e_학사코드 메뉴 -->

				</div>
			</div>
			<!-- e_left_mcon -->

			<!-- s_main_con -->
			<div class="main_con adm_grd">
				<!-- s_메뉴 접기 버튼 -->
				<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
				<!-- e_메뉴 접기 버튼 -->

				<!-- s_tt_wrap -->
				<div class="tt_wrap">
					<h3 class="am_tt">
						<span class="tt">코드1(${code_name })</span>
						<button class="btn_tt1n" onclick="post_to_url('${HOME}/admin/academic/codeManagement/code/nameChange', {'code_cate':'osce_code_name'});">메뉴명 수정</button>
					</h3>
				</div>
				<!-- e_tt_wrap -->

				<!-- s_adm_content2n -->
				<div class="adm_content2n">
					<!-- s_tt_wrap -->

					<!-- s_wrap_wrap -->
					<div class="wrap_wrap">

						<!-- s_sort_wrap -->
						<div class="sort_wrap">

							<!-- s_wrap_s1 -->
							<div class="wrap_s1">
								<span class="tt1">항목 명</span> 
								<input type="text" class="ip_tt1" id="code_name" value=""
									onkeypress="javascript:if(event.keyCode==13){insertCode(); return false;}">
								<button class="btn_add" title="추가하기" onClick="insertCode();" >추가</button>
							</div>
							<!-- e_wrap_s1 -->

							<span class="sstt">마우스로 끌어 순서를 서로 바꿀 수 있습니다.</span>
							<div class="reorder" id="codeListAdd">

							</div>

						</div>
						<!-- e_sort_wrap -->

					</div>
					<!-- e_wrap_wrap -->

					<div class="bt_wrap">
						<button class="bt_2" onclick="updateCode();">등록</button>
						<button class="bt_3" onClick="location.href='${HOME}/admin/academic/codeManagement/code1'">취소</button>
					</div>

				</div>
				<!-- e_adm_content2n -->

			</div>
			<!-- e_main_con -->

		</div>
		<!-- e_contents -->
	</div>
	<!-- e_container_table -->

</body>
</html>