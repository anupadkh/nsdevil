<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<script>
	$(document).ready(function() {
		getGCversion();		
		
		
		//setTimeout(,1000);
		
		var list_count = 0;
		//중분류 값 변경 시
        $(document).on("DOMSubtreeModified", "#version" ,function(){           
            if(list_count == 0){
            	getGraduationCapabilityList();
            }else{
                list_count = 0;
            }
        });		
	});
	
	function file_nameChange(){    
        if($("#xlsFile").val() != ""){
            var fileValue = $("#xlsFile").val().split("\\");
            var fileName = fileValue[fileValue.length-1]; // 파일명
            
            var fileLen = fileName.length; 
            var lastDot = fileName.lastIndexOf('.'); //마지막 . 위치 가져온다.
            var fileExt = fileName.substring(lastDot+1, fileLen).toLowerCase(); //마지막 . 위치로 확장자 자름

            if(fileExt != "xlsx") {
                alert("xlsx 파일을 등록해주세요");
                $("#xlsFile").val("");
                return;                     
            }
            
            $("#xls_filename").val(fileName);
        }
    }
	
	function xlsUp(){
		
		if(!confirm("등록하시겠습니까?"))
			return false;
				
        if (!$("#xlsFile").val()) {
            alert("파일을 선택하세요");
            return;
        }
                    
        $("#xlsForm").ajaxForm({
            type: "POST",
            url: "${HOME}/ajax/admin/graduationCapability/excel/create",
            dataType: "json",
            success: function(data, status){
               
                if (data.status == "200") {
                    alert("저장 완료 되었습니다.");
                    $("#xlsFile").val("");
                    $("#xls_filename").val("");
                    $('.avgrund-overlay').trigger('click');
                    getGCversion();
                }else{
                    alert(data.status);
                }
            },
            error: function(xhr, textStatus) {
                alert(textStatus);
                //document.write(xhr.responseText);
                $.unblockUI();
            },
            beforeSend:function() {
                $.blockUI();
            },
            complete:function() {
                $.unblockUI();
            }
        }); 
        $("#xlsForm").submit();
    }
	
	function getGCversion(){              
        $.ajax({
            type: "POST",
            url: "${HOME}/ajax/admin/academic/graduationCapability/versionList",
            data: {
               
                },
            dataType: "json",
            success: function(data, status) {
                $("#gc_version_list span").remove();   
                var htmls = '';
                $.each(data.list, function(index){                                        
                    htmls += '<span class="uoption" value="'+this.version+'">'+this.version+'</span>';                
                }); 
                $("#gc_version_list").append(htmls);   
                $("#last_version").html(data.last_version.version);
                $("#last_reg_date").html(data.reg_date);
                
                setTimeout(function(){
                    $("#version>div span:eq(0)").click(); // signals that the filter has changed
                }, 1000);
            },
            error: function(xhr, textStatus) {
                alert("실패");
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            },beforeSend:function() {
            },
            complete:function() {
            }
        }); 
    }
	
	function getGraduationCapabilityList(){
		
		if($("#version").attr("value") == "")
			return false;
		
        $.ajax({
            type: "POST",
            url: "${HOME}/ajax/admin/academic/graduationCapability/list",
            data: {                
                "version" : $("#version").attr("value")                
                },
            dataType: "json",
            success: function(data, status) {
                $("#gc_list_add tr").remove();   
                
                $.each(data.list, function(index){
                    var htmls = '';
                    
                    htmls += '<tr><td class="td_1 w1 bd01">'+(index+1)+'</td>';
                    htmls += '<td class="td_1 w5 bd01">'+this.fc_name+'</td>';
                    htmls += '<td class="td_1 w2 bd01">'+this.content+'</td>';
                    htmls += '<td class="td_1 w3 bd01"></td>';
                    htmls += '<td class="td_1 w4 bd01"></td></tr>';
                    
                    $("#gc_list_add").append(htmls);
                }); 
            },
            error: function(xhr, textStatus) {
                alert("실패");
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            },beforeSend:function() {
            },
            complete:function() {
            }
        }); 
    }
</script>
</head>

<body>
	<noscript title="브라우저 자바스크립트 차단 해제 안내">
		<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
	</noscript>

	<!-- s_skipnav -->
	<div id="skipnav">
		<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
	</div>
	<!-- e_skipnav -->

	<!-- s_container_table -->
	<div id="container" class="container_table">
		<!-- s_contents -->
		<div class="contents main">

			<!-- s_left_mcon -->
			<div class="left_mcon aside_l grd">
				<div class="sub_menu adm_grd">
					<div class="title">학사관리</div>
					<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">학사체계관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">학사코드관리</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">교육과정관리</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">교육과정관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청관리</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicReg/list'">학사등록관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">졸업역량관리</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
				</div>
			</div>
			<!-- e_left_mcon -->

			<!-- s_main_con -->
			<div class="main_con adm_grd">
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>

				<!-- s_tt_wrap -->
				<div class="tt_wrap">
					<h3 class="am_tt">
						<span class="tt">졸업역량관리</span>
					</h3>
				</div>
				<!-- e_tt_wrap -->

				<!-- s_adm_content1 -->
				<div class="adm_content4">
					<!-- s_tt_wrap -->

					<!-- s_wrap_wrap -->
					<div class="wrap_wrap">
						<div class="ex_wrap">
							<button class="btn_exls_down_2" onclick="location.href='${HOME}/resources/file/graduationCapability.xlsx'">졸업역량 엑셀 양식 다운로드</button>

							<div class="t_title_1">
								<span class="sp_wrap_1">졸업역량 엑셀 양식을 먼저 다운로드 받으신 뒤,<br>다운로드
									받은 엑셀파일 양식에 졸업역량 정보를 입력하여 엑셀 파일로 일괄 업로드 합니다.
								</span>
							</div>

							<div class="ip_wrap">
								<span class="ip_tt">엑셀 파일</span> 
								<form id="xlsForm" onSubmit="return false;">
									<input type="text" readonly class="ip_sort1_1" id="xls_filename" value="" onClick="$('#xlsFile').click();">		                                     
		                            <input type="file" style="display:none;" id="xlsFile" name="xlsFile" onChange="file_nameChange();">					    
							    </form>  
                                <button class="btn_r1_1 open1" onClick="xlsUp();">등록</button>      
							</div>
						</div>
					</div>
					<!-- e_wrap_wrap -->

					<!-- s_wrap_wrap -->
					<div class="wrap_wrap">
						<!-- s_tb_top_wrap -->
						<div class="tb_top_wrap">
							<span class="btn_tt2">[ 현재버전 Ver. <span id="last_version"></span> ] 최종 등록 일시
								<span id="last_reg_date"></span></span>
								

							<div class="wrap_s1_uselectbox">
								<div class="uselectbox" id="version" value="">
									<span class="uselected"></span> <span class="uarrow">▼</span>
									<div class="uoptions" id="gc_version_list">
										
									</div>
								</div>
							</div>

						</div>
						<!-- e_tb_top_wrap -->

						<table class="mlms_tb schd th">
							<tr>
								<td class="th01 w1">no</td>
								<td class="th01 w5">졸업역량</td>
								<td class="th01 w2">내용</td>
								<td class="th01 w3"></td>
								<td class="th01 w4"></td>
							</tr>
						</table>

						<!-- s_tb_wrap -->
						<div class="tb_wrap">
							<table class="mlms_tb schd" id="gc_list_add">
								
							</table>
						</div>
						<!-- e_tb_wrap -->

					</div>
					<!-- e_wrap_wrap -->

				</div>
				<!-- e_adm_content1 -->
			</div>
			<!-- e_main_con -->

		</div>
		<!-- e_contents -->


	</div>
	<!-- e_container_table -->

</body>
</html>