<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function(){
				getPBLDetail();
			});
			
			function getPBLDetail() {
				$.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/admin/board/pbl/detail",
			        data: {
			        	"board_seq":"${seq}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var pblInfo = data.pbl_info;
			        		var attachList = data.attach_list;
			        		var nextPBLInfo = data.next_pbl_info;
			        		var prevPBLInfo = data.prev_pbl_info;
			        		var startDate = pblInfo.start_date;
			        		var endDate = pblInfo.end_date;
			        		var startEndDate = startDate + " ~ " + endDate;
			        		var cateCode = pblInfo.board_cate_code;
			        		var notiSpan = "";
			        		if (cateCode == "96") {
			        			notiSpan = "공지";
			        		}
			        		
			        		if (notiSpan != "") {
			        			$("#notiSpan").html('<span class="s1_99">'+notiSpan+'</span>');
			        		}
			        		
			        		//대상
			        		var showTarget = pblInfo.show_target;
			        		var targetSpan = "";
			        		if (showTarget == "00") {
			        			targetSpan = '<span class="t_s1_all">전체</span>';
			        		} else if (showTarget == "02") {
		        				targetSpan = '<span class="t_s1_all">학생 - 전체</span>';
			        		} else if (showTarget == "03") {
			        			var targetList = data.target_list;
			        			if (targetList.length > 0) {
			        				$(targetList).each(function(idx) {
			        					targetSpan += '<span class="t_s6_st">' + this.target_name + '</span>';
			        				});
			        			}
			        		}
			        		
			        		if (targetSpan != "") {
			        			$("#showTargetArea").append(targetSpan);
			        		}
			        		
			        		$("#title").html(pblInfo.title);
			        		$("#regDate").html(pblInfo.reg_date);
			        		$("#startEndDate").html(startEndDate);
			        		$("#content").html(pblInfo.content);
			        		
			        		if (attachList.length > 0) {
			        			var attachHtml = '<ul class="h_list m_tb custom">';
				        		$(attachList).each(function() {
				        			attachHtml += '<li class="li_1" onclick="fileDownload(\'${HOME}\', \''+this.file_path+'\', \'\', \''+this.file_name+'\')"><span>' + this.file_name + '</span></li>';
				        			
				        		});
				        		attachHtml += '</ul>';
				        		$("#attachListArea").html(attachHtml);
			        		} else {
			        			$("#attachListArea").html("");
			        		}
			        		
			        		if (nextPBLInfo != null) {
			        			$("#nextBtn").attr("onclick", "javascript:location.href='${HOME}/admin/board/pbl/detail?seq=" + nextPBLInfo.next_board_seq + "'");
			        			$("#nextBtn").append('<span class="sp_tt">' + nextPBLInfo.next_title + '</span>');
			        		}
			        		
			        		if (prevPBLInfo != null) {
			        			$("#prevBtn").attr("onclick", "javascript:location.href='${HOME}/admin/board/pbl/detail?seq=" + prevPBLInfo.prev_board_seq + "'");
			        			$("#prevBtn").append('<span class="sp_tt">' + prevPBLInfo.prev_title + '</span>');
			        		}
			        		
			        	} else {
			        		alert("PBL 불러오기에 실패하였습니다. [" + data.status + "]");
			        		location.href="${HOME}/admin/board/pbl/list";
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function removeBoard() {
				if(confirm("삭제하시겠습니까?")){
				   	$.ajax({
				        type: "POST",
				        url: "${HOME}/ajax/admin/board/remove",
				        data: {"removeBoardSeqs":"${seq}"},
				        dataType: "json",
				        success: function(data, status) {
				        	if (data.status == "200") {
				        		alert("삭제 완료되었습니다.");
				        		location.href="${HOME}/admin/board/pbl/list";
				        	} else {
				        		alert("삭제에 실패하였습니다. [" + data.status + "]");
				        		location.reload();
				        	}
				        },
				        error: function(xhr, textStatus) {
				            document.write(xhr.responseText);
				        },
				        beforeSend:function() {
				            $.blockUI();
				        },
				        complete:function() {
				            $.unblockUI();
				        }
				    });
				}
			}
			
			function getPBLModifyView() {
				post_to_url('${HOME}/admin/board/pbl/modify', {"seq":"${seq}"});
			}
		</script>
	</head>
	<body>
		<div id="container" class="container_table adm_bd">
			<div class="contents sub">
				<div class="left_mcon aside_l" id="boardMenuArea"></div><!-- 게시판관리 메뉴 목록 -->
				<div class="sub_con">
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
		            <a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<div class="tt_wrap">
						<h3 class="am_tt"><span class="tt" id="boardTitleValue"> 상세보기</span></h3>
						<div class="wrap">
							<button class="ic_v3" onClick="location.href='${HOME}/admin/board/pbl/list'" title="목록 보기">목록</button>
						</div>               
					</div>
				
					<div class="tt_wrap2">    
						<!-- s_긴급공지 s1_1 , 시스템점검 s1_2 ,  기타 s1_3 -->                      
						<div id="notiSpan"></div>
						<!-- s_긴급공지 s1_1 , 시스템점검 s1_2 ,  기타 s1_3 -->                            
						<span class="tt_con" id="title"></span>
						<span class="tt_date" id="regDate"></span>    
					</div>	
					<div class="tt_wrap2s"> 
					    <div class="wrap_s">
					        <span class="t1">기한</span><span class="t2" id="startEndDate"></span><span class="t3">마감됨</span>
					    </div>
					</div>
					<div class="rfr_con">
						<div id="attachListArea"></div>
						
						<div class="con_wrap"> 
							<div class="con_s con_s_h" id="content"></div>
						</div>
						
						<div class="h_list mp_1">
							<div class="wrap" id="showTargetArea">
								<span class="t1">대상</span>
								<!-- s_전체 t_s1, 본과1 t_s2, 본과2 t_s3, 본과3 t_s4, 본과4 t_s5, 예과1 t_s6, 예과1 t_s7, 교수-전체 t_s8, 학생-전체 t_s9 -->
							</div>
						</div>
						<div class="btn_wrap_v">
							<button class="bt_2" onclick="getPBLModifyView();">수정</button>
							<button class="bt_3" onclick="removeBoard();">삭제</button>
		 				</div>
					</div> 
				
					<div class="btn_wrap">
						<button class="btn1" id="nextBtn" onclick="javascript:alert('다음 글이 없습니다.'); return false;"><span><span class="ic">▲</span>다음 글</span></button>
				    	<button class="btn2" id="prevBtn" onclick="javascript:alert('이전 글이 없습니다.'); return false;"><span><span class="ic">▼</span>이전 글</span></button>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>