<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script src="${JS}/lib/datetimep_1.js"></script>
		<script src="${JS}/lib/monthdatepicker_ui.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				 getBoardMenuList();
				 boardListView();
			});
			
			function boardListView() {
				var boardMenuCodeValue = $("#boardMenuSelectBox").attr("value");
				$("#searchBoardMenuCode").val(boardMenuCodeValue);
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/admin/board/list",
			        data: $("#searchFrm").serialize(),
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var list = data.list;
				        	var listHtml = '';
				        	var showBoardCnt = 0;
				        	var totalBoardCnt = list.length;
				        	$(list).each(function(){
				        		var menuName = this.menu_name;
				        		var boardOrder = this.order;
				        		var boardName = this.board_name;
				        		var boardType = this.board_type;
				        		var regDate = this.reg_date;
				        		var stopDate = this.stop_date;
				        		var contentCnt = this.content_cnt;
				        		var showFlag = this.show_flag;
				        		var boardUrl = this.url;
				        		var boardMSeq = this.board_m_seq;
				        		listHtml += '<tr>';
				        		listHtml += '    <td class="td_1">' + menuName + '</td>';
				        		listHtml += '    <td class="td_1">' + boardOrder + '</td>';
				        		listHtml += '    <td class="td_1 ft13"><a class="go_bd board_name" href="${HOME}' + boardUrl + '">' + boardName + '</a></td>';
				        		var showTextTag = "";
				        		if (showFlag == "N") {
				        			showTextTag = '<div class="notused">사용안함</div>';
				        		} else {
				        			stopDate = "";
				        		}
				        		var boardTypeText = "기본게시판";
				        		listHtml += '    <td class="td_1 board_type">' + boardTypeText + '<div>' + showTextTag + '</div></td>';
				        		listHtml += '    <td class="td_1 ft13">' + regDate + '</td>';
				        		listHtml += '    <td class="td_1 ft13">' + stopDate + '</td>';
				        		listHtml += '	 <td class="td_1">' + contentCnt + '</td>';
				        		listHtml += '    <td class="td_1 pd0">';
				        		var showCheckedTag = "";
				        		if (showFlag == "Y") {
				        			showCheckedTag = "on";
				        			showBoardCnt++;
				        		}
				        		listHtml += '        <label class="swt1"><span class="btn_slider_custom '+showCheckedTag+'" onclick="boardShowChange(this, ' + boardMSeq + ');"></span></label>';
				        		listHtml += '	 </td>';
				        		listHtml += '</tr>';
				        	});
				        	if (list.length > 0) {
				        		$(".showBoardCnt").html(showBoardCnt);
				        		$(".totalBoardCnt").html(totalBoardCnt);
				        		
				        		$("#boardList").html(listHtml);
				        	}
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			
			//관리
			function boardShowChange(t, boardMSeq) {
				var boardName = $(t).parent().parent().parent().find("a.board_name").text();
				var showFlag = "N";
				var msg = "[ " + boardName + " ] 게시판을 사용안함으로 하시겠습니까?";
				if (!$(t).hasClass("on")) {
					msg = "[ " + boardName + " ] 게시판을 사용함으로 하시겠습니까?";
					showFlag = "Y";
				}
				
				if (confirm(msg)) {
					$.ajax({
				        type: "POST",
				        url: "${HOME}/ajax/admin/board/show/modify",
				        data: {
				        	"boardMSeq" : boardMSeq
				        	, "showFlag" : showFlag
				        },
				        dataType: "json",
				        success: function(data, status) {
				        	if (data.status == "200") {
				        		boardListView();
				        	}
				        },
				        error: function(xhr, textStatus) {
				            document.write(xhr.responseText);
				        },
				        beforeSend:function() {
				            $.blockUI();
				        },
				        complete:function() {
				            $.unblockUI();
				        }
				    });
				}
			}

			function getBoardMenuList() {
				$.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/admin/board/menu/list",
			        data: {},
			        dataType: "json",
			        success: function(data, status) {
						boardMenuSelectBoxSetting(data.menuCodeList);
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function boardMenuSelectBoxSetting(list) {
				var target = $("#boardMenuSelectBox").find("div.uoptions");
				var listHtml = '<span class="uoption" value="">전체</span>';
				$(list).each(function() {
					listHtml += '<span class="uoption" value="' + this.code + '">' + this.code_name + '</span>';
				});
				$(target).html(listHtml);
			}
		</script>
	</head>
	<body>
		<div id="container" class="container_table adm_bd">
		    <div class="contents sub">
		        <div class="left_mcon aside_l" id="boardMenuArea"></div><!-- 게시판관리 메뉴 목록 -->
		        <div class="sub_con">
		            <a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
		            <a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
		            <div class="tt_wrap">
		                <h3 class="am_tt"><span class="tt" id="boardTitleValue">전체게시판 관리</span></h3>
		                <form id="searchFrm" name="searchFrm" method="get" onsubmit="return false;">
		                	<input type="hidden" id="searchBoardMenuCode" name="searchBoardMenuCode" value="">
			                <div class="sch_wrap2 custom">  
			                    <span class="tt">게시판 메뉴</span>
			                    <div class="wrap2_2_uselectbox w120px">
									<div class="uselectbox" id="boardMenuSelectBox" value="">
										<span class="uselected">전체</span>
										<span class="uarrow">▼</span>			
										<div class="uoptions" style="display: none;">
											<span class="uoption firstseleted">전체</span>
										</div>
									</div>
			                    </div>
			                    <span class="tt">게시판명</span>
			                    <input type="text" class="ip_search" placeholder="게시판명" id="searchText" name="searchText" value="">
			                    <button class="btn_search1" onclick="boardListView();">검색</button>
	<!-- 		                	<button class="btn_add1 open1">게시판 추가</button> -->
			                </div>
						</form>
		            </div>
		
		            <div class="wrap_wrap">
		                <div class="btnwrap_s2">
		                    <div class="btnwrap_s2_s">
<!-- 		                          <button class="btn_tt1 open2">순서변경</button> -->
<!-- 		                          <button class="btn_up1 open3">주요 관리 게시판 6개 선정</button> -->
		                    </div>
		                    <div class="btnwrap_s3_s">
		                    	<span class="tx">총</span><span class="num showBoardCnt">0</span><span class="tx">개의 게시판 운영 중</span><span class="sign">(</span><span class="num showBoardCnt">0</span> <span class="sign">/</span><span class="num totalBoardCnt">0</span><span class="sign">)</span>
		                    </div>
		                </div>
		                <table class="mlms_tb curri">
		                    <thead>
		                        <tr>
		                            <th class="th01 bd01 w2">메뉴</th>
		                            <th class="th01 bd01 w0">순서</th>
		                            <th class="th01 bd01 w4">게시판명</th>
		                            <th class="th01 bd01 w2">구분</th>
		                            <th class="th01 bd01 w3">생성일</th>
		                            <th class="th01 bd01 w3">중지일</th>
		                            <th class="th01 bd01 w1">사용량</th>
		                            <th class="th01 bd01 w5">관리</th>
		                        </tr>
		                    </thead>
		                    <tbody id="boardList"> 
		                     </tbody>
		                </table>
		            </div>	
		        </div>
		    </div>
		</div>
	</body>
</html>