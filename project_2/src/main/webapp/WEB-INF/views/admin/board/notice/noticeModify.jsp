<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script src="${HOME}/resources/smarteditor_v2.9/js/service/HuskyEZCreator.js"></script>
		<script src="${JS}/lib/datetimep_1.js"></script>
		<script src="${JS}/lib/monthdatepicker_ui.js"></script> 
		<script type="text/javascript">
			var editor_object = [];
			$(document).ready(function(){
				getNoticeDetail();
				
				nhn.husky.EZCreator.createInIFrame({
					oAppRef: editor_object,
				    elPlaceHolder: "content",
		            sSkinURI : "${HOME}/resources/smarteditor_v2.9/SmartEditor2Skin_ko_KR.html",
		            htParams : {
	                    // 툴바 사용 여부 (true:사용/ false:사용하지 않음)
	                    bUseToolbar : true,            
	                    // 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
	                    bUseVerticalResizer : true,    
	                    // 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
	                    bUseModeChanger : true
				    },
		            fOnAppLoad:function(){
		            	editor_object.getById["content"].exec("SE_FIT_IFRAME", [0,400]);	   
					}
				 });
				
				$.datetimepicker.setLocale('kr');
			    $('#startDate').datetimepicker({
			        timepicker:false,
			        datepicker:true,
			        format:'y-m-d',
			        formatDate:'y-m-d',
			        scrollMonth : false,
					scrollInput : false
			    });
			    
			    $('#endDate').datetimepicker({
			        timepicker:false,
			        datepicker:true,
			        format:'y-m-d',
			        formatDate:'y-m-d',
			        scrollMonth : false,
					scrollInput : false
			    });
			    
			    $("#startDate").on("change", function(e){
			    	var startDate = $("#startDate").val();
			    	if (startDate != "") {
				    	$('#endDate').datetimepicker({minDate: startDate});
			    	}
			    });
			    
			    $("#endDate").on("change", function(e){
			    	var endDate = $("#endDate").val();
			    	if (endDate != "") {
				    	$('#startDate').datetimepicker({maxDate: endDate});
			    	}
			    });
			});
			
			function noticeTargetSelect(t) {
				var targetValue = $(t).attr("value");
				if (targetValue == "03") { //학생 그룹 선택시
					$("#stAcademicArea").removeClass("hidden");
					$("#stAcademicListArea").removeClass("hidden");
					getAcademicSystem('', '', 1);
				} else {
					$("#stAcademicArea").addClass("hidden");
					$("#stAcademicListArea").addClass("hidden");
					$("#stAcademicListArea").html("");
				}
			}
			
			function getAcademicSystem(lSeq, mSeq, level) {
				var searchFlag = true;
				
				if (level == 1) {
        			acaSelectInit('#mAcaSelectBox', 2);
        			acaSelectInit('#sAcaSelectBox', 3);
        		} else if (level == 2) {
        			acaSelectInit('#mAcaSelectBox', 2);
        			acaSelectInit('#sAcaSelectBox', 3);
        			if (lSeq == '') {
        				searchFlag = false;
        			}
        		} else if (level == 3) {
       				acaSelectInit('#sAcaSelectBox', 3);
        			if (lSeq == '' && mSeq == '') {
        				searchFlag = false;
        			}
        		} else if (level == 4) {
        			searchFlag = false;
        		}
				
				
				if (searchFlag) {
					$.ajax({
				        type: "GET",
				        url: "${HOME}/ajax/admin/board/academic/list",
				        data: {
				        	"l_seq": lSeq
				        	, "m_seq": mSeq
				        },
				        dataType: "json",
				        success: function(data, status) {
				        	if (data.status == "200") {
				        		if (level == 1) {
				        			acaSelectBoxSetting('#lAcaSelectBox', data.l_aca_list, 1);
				        		} else if (level == 2) {
				        			acaSelectBoxSetting('#mAcaSelectBox', data.m_aca_list, 2);
				        		} else if (level == 3) {
				        			acaSelectBoxSetting('#sAcaSelectBox', data.s_aca_list, 3);
				        		}
				        	} else {
				        		alert("공지대상 불러오기에 실패하였습니다. [" + data.status + "]");
				        	}
				        },
				        error: function(xhr, textStatus) {
				            document.write(xhr.responseText);
				        },
				        beforeSend:function() {
				            $.blockUI();
				        },
				        complete:function() {
				            $.unblockUI();
				        }
				    });
				}
			}
			
			function acaSelectInit(t, level) {
				$(t).find("span.uselected").html("전체");
				$(t).attr("value", "");
				$(t).find("div.uoptions").html('<span class="uoption" value="" onclick="getAcademicSystem(\'\', \'\', '+(level+1)+');">전체</span>');
			}
			
			function acaSelectBoxSetting(t, list, level) {
				var target = $(t).find("div.uoptions");
				var tmpLevel = level;
				tmpLevel++;
				var listHtml = '<span class="uoption" value="" onclick="getAcademicSystem(\'\', \'\', '+tmpLevel+');">전체</span>';
				if (tmpLevel > 2) {
					tmpLevel = 3;
				}
				$(list).each(function() {
					if (level == 1) {
						listHtml += '<span class="uoption" value="' + this.aca_system_seq + '" onclick="getAcademicSystem(' + this.aca_system_seq + ', \'\', ' + tmpLevel + ');">' + this.aca_system_name + '</span>';
					} else if (level == 2) {
						listHtml += '<span class="uoption" value="' + this.aca_system_seq + '" onclick="getAcademicSystem(' + this.l_seq + ', '+this.aca_system_seq+', ' + tmpLevel + ');">' + this.aca_system_name + '</span>';
						$(target).find(".uselected").html("전체");
					} else {
						listHtml += '<span class="uoption" value="' + this.aca_system_seq + '">' + this.aca_system_name + '</span>';
					}
				});
				$(target).html(listHtml);
			}
			
			
			function addAcademic() {
				var currentLSeq = $("#lAcaSelectBox").attr("value");
				var currentMSeq = $("#mAcaSelectBox").attr("value");
				var currentSSeq = $("#sAcaSelectBox").attr("value");
				var currentLevel = 0;
				var acaName = "";
				if (currentLSeq != "" && currentMSeq == "" && currentSSeq == "") {
					currentLevel = 1;
					acaName = $("#lAcaSelectBox").find(".uselected").text();
				} else if (currentLSeq != "" && currentMSeq != "" && currentSSeq == "") {
					currentLevel = 2;
					acaName = $("#mAcaSelectBox").find(".uselected").text();
				} else if (currentLSeq != "" && currentMSeq != "" && currentSSeq != "") {
					currentLevel = 3;
					acaName = $("#mAcaSelectBox").find(".uselected").text()+ " "  + $("#sAcaSelectBox").find(".uselected").text();
				} else {
					alert("학생 그룹을 선택해주세요");
					return false;
				}
				
				
				//유효성검사
				var addTypeFlag = "00"; //00:추가 01:나랑 똑같은 녀석이 이미 있음 02: 나보다 높은 녀석이 있음 03:나보다 낮은녀석있음
				$("#stAcademicListArea div.targetDiv").each(function(){
					var lSeq = $(this).attr("lSeq");
					var mSeq = $(this).attr("mSeq");
					var sSeq = $(this).attr("sSeq");
					var level = $(this).attr("level");
					
					//추가할 대상과 같은 대상이 있는지 체크
					if (lSeq == currentLSeq && mSeq == currentMSeq && sSeq == currentSSeq) {
						addTypeFlag = "01";
						return false;
					}
					
					//추가할 대상보다 상위 대상이 있는지 확인
					if (currentLevel > level) {
						if (lSeq == currentLSeq && mSeq == currentMSeq && currentLevel == 3) {
							addTypeFlag = "02";
							return false;
						}
						
						if ((lSeq == currentLSeq && (currentLevel == 2 || currentLevel == 3)  && level == 1)) {
							addTypeFlag = "02";
							return false;
						}
					}
					
					//추가할 대상보다 하위 대상이 있는지 확인
					if (currentLevel < level) {
						if ((lSeq == currentLSeq && mSeq == currentMSeq) || (lSeq == currentLSeq)) {
							addTypeFlag = "03";
							return false;
						}
					}
				});
				
				var currSeq = currentLSeq;
				if (currentLevel == 2) {
					currSeq = currentMSeq;
				} else if (currentLevel == 3) {
					currSeq = currentSSeq;
				}
				
				//유효성 검사에 따른 처리
				var targetDivHtml = '<div class="wrap targetDiv" lSeq="'+currentLSeq+'" mSeq="'+currentMSeq+'" sSeq="'+currentSSeq+'" currSeq="'+currSeq+'" level="'+currentLevel+'"><span class="sp1">'+acaName+'</span><button class="btn_c" title="삭제하기" onclick="removeNoticeTarget(this);">X</button></div>';
				if (addTypeFlag == "00" || addTypeFlag == "03") { //01:추가
					if (addTypeFlag == "03") {//03:추가할 대상보다 하위 대상이 있음 (하위 삭제 후 추가)
						$("#stAcademicListArea div.targetDiv").each(function(){
							var lSeq = $(this).attr("lSeq");
							var mSeq = $(this).attr("mSeq");
							var level = $(this).attr("level");
							if (currentLevel < level) {//추가할 대상보다 하위 대상이 있는지 확인
								if ((lSeq == currentLSeq && mSeq == currentMSeq) || (lSeq == currentLSeq)) {
									$(this).remove();
								}
							}
						});
					}
					$("#stAcademicListArea").append(targetDivHtml);
				} else if (addTypeFlag == "01") { //이미 추가되어 있음
					alert("이미 추가 되어있습니다.");
					return false;
				} else if (addTypeFlag == "02") { //추가할 대상보다 상위 대상이 있음
					alert("이미 상위 대상이 추가 되어있습니다.");
					return false;
				}
			}
			
			function removeNoticeTarget(t) {
				$(t).parent().remove();
			}
			
			var fileCnt = 0;
			function getNoticeDetail() {
				$.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/admin/board/notice/modify/detail",
			        data: {
			        	"board_seq":"${seq}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var noticeInfo = data.notice_info;
			        		var attachList = data.attach_list;
			        		var targetList = data.target_list;
			        		//공지대상
			        		var showTarget = noticeInfo.show_target;
			        		
			        		$("#noticeTargetSelectBox").find(".uoptions > span[value='"+showTarget+"']").click();
			        		
			        		if (showTarget == "03") {
			        			$(targetList).each(function(){
			        				var lSeq = "";
			        				var mSeq = "";
			        				var sSeq = "";
			        				var currSeq = this.curr_seq
			        				var level = this.level;
			        				var lAcaName = this.l_aca_name;
			        				var mAcaName = this.m_aca_name;
			        				var currAcaName = this.curr_aca_name;
			        				var acaName = "";
			        				if (level == 1) {
			        					lSeq = this.curr_seq;
			        					acaName = currAcaName;
			        				} else if (level == 2) {
			        					lSeq = this.l_seq;
			        					mSeq = this.curr_seq;
			        					acaName = currAcaName;
			        				} else if (level == 3) {
			        					lSeq = this.l_seq;
			        					mSeq = this.m_seq;
			        					sSeq = this.curr_seq
			        					acaName = mAcaName + " " + currAcaName;
			        				}
			        				
				        			var targetDivHtml = '<div class="wrap targetDiv" lSeq="'+lSeq+'" mSeq="'+mSeq+'" sSeq="'+sSeq+'" currSeq="'+currSeq+'" level="'+level+'"><span class="sp1">'+acaName+'</span><button class="btn_c" title="삭제하기" onclick="removeNoticeTarget(this);">X</button></div>';
									$("#stAcademicListArea").append(targetDivHtml);
			        			});
			        		}
			        		var cateCode = noticeInfo.board_cate_code;
			        		$("#notiCateSelectBox").find(".uoptions > span[value='"+cateCode+"']").click();
			        		$("#title").val(noticeInfo.title);
			        		$("#content").val(noticeInfo.content);
			        		
			        		var startDate = noticeInfo.start_date;
			        		var endDate = noticeInfo.end_date;
			        		if (startDate != "" && endDate != "") {
			        			$("#periodDateValue").val("Y");
			        			$("#periodDateRadio[value='Y']").prop("checked", true);
			        			$("#startDate").val(startDate);
			        			$("#endDate").val(endDate);
			        		} else {
			        			$("#periodDateValue").val("N");
			        			$("#periodDateRadio[value='N']").prop("checked", true);
			        		}
			        		
			        		
			        		if (attachList.length > 0) {
			        			$(attachList).each(function() {
				        			var attachSeq = this.board_attach_seq;
				        			var fileName = this.file_name;
				        			$("#fileUploadArea").append('<div class="wrap"><span class="sp1">'+fileName+'</span><button class="btn_c" title="삭제하기" onclick="removeAttachFile(this,'+(fileCnt++)+', ' + attachSeq + ')">X</button></div>');
			        			});
			        		} else {
			        			$("#attachListArea").html("");
			        		}
			        	} else {
			        		alert("공지사항 불러오기에 실패하였습니다. [" + data.status + "]");
			        		location.href="${HOME}/admin/board/notice/list";
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function fileSelect() {
				var div = $("<div>").addClass(".wrap");
				var fileInput = $("<input>").attr("type", "file").attr("id", "uploadFile"+fileCnt).attr("name", "uploadFile").addClass("blind-position");
				$("#fileUploadArea").append(fileInput);
				
				$(fileInput).change(function(){
					var file = $(this).val();
					if (file != "") {
						var attachFileName = file.substring(file.lastIndexOf('\\') + 1); //파일명
						var ext = attachFileName.substring(attachFileName.lastIndexOf('.') + 1).toLowerCase(); //파일 확장자
						var fileMaxSize = 10; //10MB
						var fileSize = this.files[0].size/1024/1024;
						if (fileSize > fileMaxSize) {
							alert("10MB이하의 파일만 업로드 가능합니다.");
							fileValueInit(fileInput);
							return false;
						}
						
						//파일 확장자 유효성검사
						if (/(jpg|png|bmp|mp3|mp4|pdf|hwp|ppt|zip)$/i.test(ext) == false) {
							alert("파일의 확장자를 확인해주세요.(jpg, png, bmp, mp3, mp4 , pdf, hwp, ppt, zip)");
							fileValueInit(fileInput);
							return false;
						}
						$("#fileUploadArea").append('<div class="wrap"><span class="sp1">'+attachFileName+'</span><button class="btn_c" title="삭제하기" onclick="removeAttachFile(this,'+(fileCnt++)+', \'\')">X</button></div>');
					}
				});
				$(fileInput).click();
			}
			
			function removeAttachFile(t, num, removeAttachSeq) {
				$(t).parent().remove();
				$("#uploadFile"+num).remove();
				if (removeAttachSeq != "") {
					var removeAttachSeqStr = $("#removeAttachSeqStr").val();
					if (removeAttachSeqStr == "") {
						removeAttachSeqStr = removeAttachSeq;
					} else {
						removeAttachSeqStr += "," + removeAttachSeq;
					}
					$("#removeAttachSeqStr").val(removeAttachSeqStr);
				}
			}
			
			function submitNotice() {
				editor_object.getById["content"].exec("UPDATE_CONTENTS_FIELD", []);
				$("#boardSeq").val("${seq}");
				
				var periodDateValue = $("#periodDateRadio:checked").val();
				if (periodDateValue == "Y") {
					$("#periodDateValue").val("Y");
					var startDate = $("#startDate").val();
					var endDate = $("#endDate").val();
					if (isEmpty(startDate) || isBlank(startDate)) {
						alert("시작일을 입력해주세요.");
						return false;
					}
					if (isEmpty(endDate) || isBlank(endDate)) {
						alert("종료일을 입력해주세요.");
						return false;
					}
					
					var startDateTime = new Date(startDate + " 00:00:00");
					var endDateTime = new Date(endDate + " 23:59:59");
					if ((startDateTime.getTime() < endDateTime.getTime()) == false) {
						alert("노출 기간을 올바르게 입력해주세요.");
						return false;
					}
				} else {
					$("#periodDateValue").val("N");
				}
				
				var noticeTargetValue = $("#noticeTargetSelectBox").attr("value");
				$("#showTarget").val(noticeTargetValue);
				if (noticeTargetValue == "03") {
					if($("#stAcademicListArea").find(".targetDiv").length < 1) {
						alert("학생 그룹을 추가해주세요.");
						return false;
					}
					
					var academicArray = new Array();
					$("#stAcademicListArea div.targetDiv").each(function(){
						var obj = new Object();
						obj.l_seq = $(this).attr("lSeq");
						obj.m_seq = $(this).attr("mSeq");
						obj.s_seq = $(this).attr("sSeq");
						obj.curr_seq = $(this).attr("currSeq");
						obj.level = $(this).attr("level");
						academicArray.push(obj);
					});
					$("#academicJsonArrayStr").val(JSON.stringify(academicArray));
				}
				
				var title = $("#title").val();
				if (isEmpty(title) || isBlank(title)){
					alert("제목을 입력해주세요");
					return false;
				}
				
				var cateCodeValue = $("#notiCateSelectBox").attr("value");
				$("#boardCateCode").val(cateCodeValue);
				
				var content = $("#content").val();
				if (content == "<p><br></p>") {
					alert("내용을 입력해주세요");
					return false;
				}
				
				$("#noticeFrm").attr("action", "${HOME}/ajax/admin/board/notice/modify").ajaxForm({
					beforeSend: function () {
						$.blockUI();
					},
					type: "POST",
					dataType:"json",
					success:function(data){
						if (data.status == "200") {
							alert("수정이 완료되었습니다.");
							location.href='${HOME}/admin/board/notice/list';
						} else {
							alert("수정에 실패하였습니다. [" + data.status + "]");
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						document.write(xhr.responseText);
				    },
			        complete:function() {
			            $.unblockUI();
			        }
				}).submit();
			}
		</script>
	</head>
	<body>
		<form id="noticeFrm" name="noticeFrm" onsubmit="return false;" enctype="multipart/form-data">
			<input type="hidden" id="boardSeq" name="boardSeq" value="${seq}">
			<input type="hidden" id="boardCateCode" name="boardCateCode">
			<input type="hidden" id="showTarget" name="showTarget">
			<input type="hidden" id="academicJsonArrayStr" name="academicJsonArrayStr">
			<input type="hidden" id="removeAttachSeqStr" name="removeAttachSeqStr">
			<input type="hidden" id="periodDateValue" name="periodDateValue" value="Y">
			<div id="container" class="container_table adm_bd">
				<div class="contents sub">
					<div class="left_mcon aside_l" id="boardMenuArea"></div><!-- 게시판관리 메뉴 목록 -->
					<div class="sub_con">
						<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
		            	<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
						<div class="tt_wrap">
							<h3 class="am_tt"><span class="tt" id="boardTitleValue"> 글 수정</span></h3>
						</div>
						<div class="rfr_con">   
							<table class="mlms_tb4">
								<tbody>
									<tr>
										<th class="w1" scope="row"><span class="sp1">기간</span></th>	
										<td class="w2">	
											<div class="dt_wrap">
												<input type="radio" name="rdgroup1" id="periodDateRadio" name="periodDateRadio" value="N" class="rd1">
												<span class="tt">설정하지 않음</span>
												<input type="radio" name="rdgroup1" id="periodDateRadio" name="periodDateRadio" value="Y" class="rd1" checked="checked">					
												<span class="tt">노출기간 설정</span>
												<input class="dateyearpicker-input_1 ip_date" placeholder="시작일" type="text" id="startDate" name="startDate">
												<span class="sign">~</span>
												<input class="dateyearpicker-input_1 ip_date" placeholder="종료일" type="text" id="endDate" name="endDate">
											</div>
											<div class="select_wrap2">
												<span class="ment">! 안내 : 노출기간을 설정하시면 공지대상에게 로그인 시 공지 팝업이 출력됩니다.</span>
											</div>
										</td>
									</tr>
									<tr>
										<th class="w1" scope="row"><span class="sp1">공지대상</span></th>
										<td class="w2">
											<div class="select_wrap1">
												<!-- s_wrap1_lms_uselectbox -->
												<div class="wrap1_lms_uselectbox">
													<div class="uselectbox" id="noticeTargetSelectBox" value="00">
														<span class="uselected">전체</span>
														<span class="uarrow">▼</span>
														<div class="uoptions">
															<span class="uoption" value="00" onclick="noticeTargetSelect(this);">전체</span>
															<span class="uoption" value="01" onclick="noticeTargetSelect(this);">교수 전체</span>
															<span class="uoption" value="02" onclick="noticeTargetSelect(this);">학생 전체</span>
															<span class="uoption" value="03" onclick="noticeTargetSelect(this);">학생 그룹</span>
														</div>
													</div>
												</div>
												<div id="stAcademicArea" class="hidden">
													
													<!-- 대분류 -->
													<div class="wrap1_1_lms_uselectbox">
														<div class="uselectbox" id="lAcaSelectBox" value="">
															<span class="uselected">전체</span>
															<span class="uarrow">▼</span>
															<div class="uoptions">
																<span class="uoption firstseleted">전체</span>
															</div>
														</div>
													</div>
		
													<!-- 중분류 -->
													<div class="wrap1_1_lms_uselectbox">
														<div class="uselectbox" id="mAcaSelectBox" value="">
															<span class="uselected">전체</span>
															<span class="uarrow">▼</span>
															<div class="uoptions">
																<span class="uoption firstseleted">전체</span>
															</div>
														</div>
													</div>
													
													<!-- 소분류 -->	
													<div class="wrap1_1_lms_uselectbox">
														<div class="uselectbox" id="sAcaSelectBox" value="">
															<span class="uselected">전체</span>
															<span class="uarrow">▼</span>
															<div class="uoptions">
																<span class="uoption firstseleted">전체</span>
															</div>
														</div>  								
													</div>
													<button class="bt_5" onclick="addAcademic();">추가</button>
												
												</div>											
											</div>
											<div class="select_wrap2 hidden" id="stAcademicListArea"></div>
										</td>
									</tr>
									<tr>
										<th scope="row"><span class="sp1">제목</span></th>
										<td>
											<div class="wrap2_lms_uselectbox">
												<div class="uselectbox" id="notiCateSelectBox" value="">
													<span class="uselected">일반</span>
													<span class="uarrow">▼</span>
													<div class="uoptions">
														<span class="uoption" value="">일반</span>
														<span class="uoption" value="99">긴급공지</span>
														<span class="uoption" value="98">시스템점검</span>
														<span class="uoption" value="97">기타</span>
													</div>
												</div>
											</div>
											<!-- e_wrap2_lms_uselectbox -->
											<input type="text" class="ip_pt1" placeholder="제목을 입력해주세요." id="title" name="title" maxlength="100">
										</td>
									</tr>
									<tr>
										<th scope="row"><span class="sp1">내용</span></th>
										<td class="tarea free_textarea">
											<textarea class="tarea01 margin-none" style="height: 100px;display:none;" id="content" name="content"></textarea>
										</td>
									</tr>
									<tr>
										<th rowspan="2" scope="row"><span class="sp1">파일첨부</span></th>
										<td>
											<input type="text" class="ip_pt2" value="파일을 첨부해주세요." readonly="readonly">
											<button class="btn1" onclick="fileSelect();">선택</button>
										</td>
									</tr>
									<tr>
										<td class="f_add1" id="fileUploadArea"></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="btn_wrap_r">
							<button class="bt_2" onclick="submitNotice();">수정</button>
							<button class="bt_3" onclick="location.href='${HOME}/admin/board/notice/list'">취소</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</body>
</html>