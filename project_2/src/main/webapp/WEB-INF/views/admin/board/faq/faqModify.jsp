<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script src="${HOME}/resources/smarteditor_v2.9/js/service/HuskyEZCreator.js"></script> 
		<script type="text/javascript">
			var editor_object = [];
			$(document).ready(function(){
				getBoardDetail();
				
				nhn.husky.EZCreator.createInIFrame({
					oAppRef: editor_object,
				    elPlaceHolder: "content",
		            sSkinURI : "${HOME}/resources/smarteditor_v2.9/SmartEditor2Skin_ko_KR.html"     ,
		            htParams : {
	                    // 툴바 사용 여부 (true:사용/ false:사용하지 않음)
	                    bUseToolbar : true,            
	                    // 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
	                    bUseVerticalResizer : true,    
	                    // 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
	                    bUseModeChanger : true
				    },
		            fOnAppLoad:function(){
		            	editor_object.getById["content"].exec("SE_FIT_IFRAME", [0,400]);	   
					}
				 });
			});
			
			function getBoardDetail() {
				$.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/admin/board/faq/modify/detail",
			        data: {
			        	"board_seq":"${seq}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var boardInfo = data.board_info;
			        		$("#title").val(boardInfo.title);
			        		$("#content").val(boardInfo.content);
			        	} else {
			        		alert("자주 찾는 질문 불러오기에 실패하였습니다. [" + data.status + "]");
			        		location.href="${HOME}/admin/board/faq/list";
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function submitBoard() {
				editor_object.getById["content"].exec("UPDATE_CONTENTS_FIELD", []);
				$("#boardSeq").val("${seq}");
				var title = $("#title").val();
				if (isEmpty(title) || isBlank(title)){
					alert("제목을 입력해주세요");
					return false;
				}
				
				var content = $("#content").val();
				if (content == "<p><br></p>") {
					alert("내용을 입력해주세요");
					return false;
				}
				
				$("#boardFrm").attr("action", "${HOME}/ajax/admin/board/faq/modify").ajaxForm({
					beforeSend: function () {
						$.blockUI();
					},
					type: "POST",
					dataType:"json",
					success:function(data){
						if (data.status == "200") {
							alert("수정이 완료되었습니다.");
							location.href='${HOME}/admin/board/faq/list';
						} else {
							alert("수정에 실패하였습니다. [" + data.status + "]");
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						document.write(xhr.responseText);
				    },
			        complete:function() {
			            $.unblockUI();
			        }
				}).submit();
			}
		</script>
	</head>
	<body>
		<form id="boardFrm" name="boardFrm" onsubmit="return false;" enctype="multipart/form-data">
			<input type="hidden" id="boardSeq" name="boardSeq" value="${seq}">
			<div id="container" class="container_table adm_bd">
				<div class="contents sub">
					<div class="left_mcon aside_l" id="boardMenuArea"></div><!-- 게시판관리 메뉴 목록 -->
					<div class="sub_con">
						<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
		            	<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
						<div class="tt_wrap">
							<h3 class="am_tt"><span class="tt" id="boardTitleValue"> 글 수정</span></h3>
						</div>
						<div class="rfr_con">   
							<table class="mlms_tb4">
								<tbody>
									<tr>
										<th scope="row"><span class="sp1">제목</span></th>
										<td>
											<input type="text" class="ip_pt_custom" placeholder="제목을 입력해주세요." id="title" name="title" maxlength="100">
										</td>
									</tr>
									<tr>
										<th scope="row"><span class="sp1">내용</span></th>
										<td class="tarea free_textarea">
											<textarea class="tarea01 margin-none" style="height: 100px;display:none;" id="content" name="content"></textarea>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="btn_wrap_r">
							<button class="bt_2" onclick="submitBoard();">수정</button>
							<button class="bt_3" onclick="location.href='${HOME}/admin/board/faq/list'">취소</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</body>
</html>