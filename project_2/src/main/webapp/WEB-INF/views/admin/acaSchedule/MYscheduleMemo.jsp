<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<link rel="stylesheet" href="${RES}/fullcalendar-v3.6.2/css/calendar1.css">
<link rel="stylesheet" href="${RES}/fullcalendar-v3.6.2/css/fullcalendar.print.css" media="print">
<script src="${RES}/fullcalendar-v3.6.2/js/moment.min.js"></script>
<script src="${RES}/fullcalendar-v3.6.2/js/fullcalendar.js"></script>
<script src="${RES}/fullcalendar-v3.6.2/js/locale-all.js"></script>
<script src="${JS}/schedule.js"></script>
<style>
	.tt_wrap .sch_wrap .bt04{display:inline-block;width: 140px;height: 35px;margin: 1px auto 0 0;padding: 0;border-radius: 7px;font-size: 15px;text-align: center;letter-spacing: 0px;background:rgb(1, 41, 121);color: rgb(255, 255, 255);float: left;}
	.main .main_con .tt_wrap h3.am_tt.tt2{position:relative;top:0;display:inline-block;width: 300px;height: 40px;line-height: 40px;text-align: left;text-indent:0;font-size: 17px;padding: 0 25px;background: rgba(224, 249, 255, 1);border-left: solid 0px rgb(50, 116, 171);border-bottom: solid 1px rgba(204, 204, 204, .5);float:left;color: rgba(1, 96, 121, 1);letter-spacing: -.5px;box-sizing: border-box;}
	.pop_up_schd_write .t_dd .pop_btn_wrap .btn02_right{width: 310px;height: 41px;line-height: 38px;text-align: center;text-indent: 0;font-size: 16px;letter-spacing: 0px;padding: 0;margin: 0;border: solid 1px rgba(0, 49, 119, 1);border-top: solid 2px rgba(1, 96, 122, 1);border-bottom-right-radius: 11px;background: rgba(255, 132, 132, 1);color: rgba(255, 255, 255, 1);display:inline-block;box-sizing: border-box;float: right;}
	.pop_up_schd_write .t_dd .pop_btn_wrap .btn02_right:hover{color:rgba(255, 46, 46, 1);background: rgb(255, 255, 255);transition:.7s;}
	.pop_up_schd_write_op_af .t_dd .pop_btn_wrap .btn01.full {width:100%; border-bottom-right-radius:11px}
</style>
<script type="text/javascript">
	
	$(document).ready(function() {
		$("body").addClass("full_schd");
		$('.free_textarea').on( 'keyup', 'textarea', function (e){
			$(this).css('height', 'auto' );
			$(this).height( this.scrollHeight );
		});
		$('.free_textarea').find( 'textarea' ).keyup();
		
		$('.timepicker1').timepicki();
    	$('.timepicker2').timepicki({custom_classes:"time2"});
    	
    	$(document).on("change", "input[name='all_day_flag']", function(index){
			if($(this).is(":checked")){
				$(":input[name='memo_start_time']").val("");
				$(":input[name='memo_end_time']").val("");
				$(":input[name='memo_start_time']").prop("disabled", true);
				$(":input[name='memo_end_time']").prop("disabled", true);
				$(":input[name='memo_edate']")
				if(!isEmpty($(":input[name='memo_date']").val())){
					$(":input[name='memo_edate']").val($(":input[name='memo_date']").val());
					$(":input[name='memo_edate']").prop("disabled", true);							
				}						
				$(":input[name=all_day]").val("Y");
			}else{
				$(":input[name='memo_edate']").prop("disabled", false);
				$(":input[name='memo_start_time']").prop("disabled", false);
				$(":input[name='memo_end_time']").prop("disabled", false);
				$(":input[name=all_day]").val("N");						
			}
		});
		$(document).on("change", "input[name='memo_date']",function(){
			if($(":input[name='all_day_flag']").is(":checked"))
				$(":input[name='memo_edate']").val($(this).val());
		});
		
		$(document).click(function(){
		    $.datetimepicker.setLocale('kr');
		    $('.dateyearpicker-input_1').datetimepicker({
		        timepicker:false,
		        datepicker:true,
		        format:'y-m-d',
		        formatDate:'y-m-d'
		    });
		});
		
		bindPopupEvent("#m_pop_memo", ".open99");
		
		$("#open_01").hide();
        $("#fold_01").click(function(){
	        $("#top_pop").hide();
			$("#open_01").show();
	        $("#fold_01").hide();
	    });
        $("#open_01").click(function(){
	        $("#top_pop").show();
			$("#open_01").hide();
	        $("#fold_01").show();
	    });
		
        $('#calendar1').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaTwoWeek,agendaWeek,agendaDay,listWeek'
			},
			navLinks: true,
			editable: true,
			locale: "ko",
			eventLimit: true,
			fixedWeekCount: true,	//true: 6주로 고정
			viewRender : function (view, element) {
				//년도가 변경되었을때만 로드 
  				var calendarYear = $('#calendar1').fullCalendar('getDate').format("YYYY");
				if ($(":input[name='calendarYear']").val() != calendarYear) {
					getScheduleMemo("");
				}
			},
			eventDrop: function(event, delta, revertFunc) {
				revertFunc();
				return;
			
				if (event.event_type == "lesson" && "${S_USER_LEVEL}" == "3" && "${S_USER_SEQ}" != event.reg_user_seq) {
					alert("변경할 권한이 없습니다.");
					revertFunc();
					return;
				}
				if (confirm("\"" + event.title + "\" 일정을 " + event.start.format("YYYY-MM-DD") + " " + event.start.format("a hh:mm") + "로 변경하시겠습니까?")) {
		        	changeEventDate(event.event_seq, event.event_type, event.start.format("YYYY-MM-DD"), event.start.format("HH:mm"), event.event_minute);
		        } else {
		        	revertFunc();
		        }
		    },
		    eventResize: function(event, delta, revertFunc) {
		    	revertFunc();
		    },
			views: {
				agendaTwoWeek: {
					type: 'basic',
					duration: { weeks: 2 },
					buttonText: '2주'
				}
			},
			minTime: '08:00:00',
	        maxTime: '22:00:00'
		});        
	});
	
	//시간표 목록
	function getScheduleMemo() {
		/* 
		if (curr_seq == "") {
			alert("교육과정을 선택해 주세요.");
			history.back(-1);
			return;
		} */
		
		var dateStart = $('#calendar1').fullCalendar('getView').start.format("YYYY-MM-DD");
		var dateEnd = $('#calendar1').fullCalendar('getView').end.format("YYYY-MM-DD");
		var calendarYear = $('#calendar1').fullCalendar('getDate').format("YYYY");
		
        $.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/getMYScheduleMemo",
            data: {
            	"dateStart" : dateStart,
            	"dateEnd" : dateEnd,
            	"calendarYear": calendarYear
            },
            dataType: "json",
            success: function(data, status) {

            	$(":input[name='calendarYear']").val(calendarYear);
            	
            	var html = "";
            	var pre_curr_seq = "";

            	var eventList = [];
            	
            	$.each(data.memo, function() {
                	var startDate = this.memo_date;
                	var endDate = this.memo_edate;
                	var eventMinute = "";
                	if (this.start_time != null && this.end_time != null) {
                		startDate += "T" + this.start_time;
                		endDate += "T" + this.end_time;
                		eventMinute = ampmTimeToMinute(this.end_time) - ampmTimeToMinute(this.start_time);
                	}
                	
                	var newEvent = {
                		event_type: "memo",
                		event_seq : this.memo_seq,
                		title: "[개인일정]"+this.memo_name,
						url: 'javascript:openEditMemoPopup(' + this.memo_seq + ');',
						start: startDate,
						end: endDate,
						event_minute: eventMinute,
						textColor: "#FFFFFF",
						backgroundColor: "#EAA361"
					}
                	
                	$('.fc-event').css('font-size', '0.9em');
                	
					eventList.push(newEvent);
                });

            	$('#calendar1').fullCalendar('removeEventSources');
                $('#calendar1').fullCalendar('addEventSource', eventList);
                
                bindPopupEvent("#m_pop1", ".open1");
            },
            error: function(xhr, textStatus) {
                alert("오류가 발생했습니다.");
                //document.write(xhr.responseText);
            }
        });
    }
	
	function changeEventDate(event_seq, event_type, event_date, start_time, event_minute) {
		//종료시간 계산
		var endMinute = (Number(start_time.substring(0, 2)) * 60) + Number(start_time.substring(3, 5)) + event_minute;
		var end_time = Math.floor(endMinute / 60) + ":" + endMinute % 60;
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/changeEventDate",
            data: {
            	"event_seq" : event_seq,
            	"event_type" : event_type,
            	"event_date" : event_date,
            	"start_time" : start_time,
            	"end_time" : end_time
            },
            dataType: "json",
            success: function(data, status) {
            	if (data.status == "200") {
            		alert("변경 되었습니다.");
            		getScheduleMemo("${S_CURRICULUM_SEQ}");
            	} else {
					alert(data.msg);
				}
            },
            error: function(xhr, textStatus) {
                alert("오류가 발생했습니다.");
                //document.write(xhr.responseText);
            }
        });
	}
	
	//일정 수정
	function openEditMemoPopup(memo_seq) {
		$("p.t_title").text("일정 수정");
		$("#m_pop_memo .pop_btn_wrap .btn01").html("수정");
		$("#m_pop_memo .pop_btn_wrap .btn01").removeClass("full");
		$("#m_pop_memo .pop_btn_wrap .btn02_right.deleteMemo").show();
		$("#m_pop_memo").show();
		$.ajax({
	        type: "POST",
	        url: "${HOME}/ajax/pf/lesson/getMemo",
	        data: {
	        	"memo_seq": memo_seq
	        },
	        dataType: "json",
	        success: function(data, status) {
	        	$(":input[name='memo_seq']").val(data.memo_seq);
	        	$(":input[name='public_flag']").val(data.public_flag);
	        	$(":input[name='memo_name']").val(data.memo_name);
	        	$(":input[name='memo_date']").val(data.memo_date);
	        	$(":input[name='memo_edate']").val(data.memo_edate);
	        	$(":input[name='memo_start_time']").val(data.start_time_12h);
	        	$(":input[name='memo_end_time']").val(data.end_time_12h);
	        	$(":input[name='place']").val(data.place);
	        	$(":input[name='content']").val(data.content);
	        	if(data.all_day_flag == "Y")
	        		$(":input[name=all_day_flag]").prop("checked",true).change();
	        	else
	        		$(":input[name=all_day_flag]").prop("checked",false).change();
	        },
	        error: function(xhr, textStatus) {
	            alert("오류가 발생했습니다.");
	        }
	    });
	}
	//일정쓰기 팝업열기
	function openWriteMemo() {
		$("p.t_title").text("일정 등록");
		$(":input[name='memo_seq']").val("");
		$(":input[name='memo_name']").val("");
		$(":input[name='memo_date']").val("");
		$(":input[name='memo_edate']").val("");
		$(":input[name='memo_start_time']").val("");
		$(":input[name='memo_end_time']").val("");
		$(":input[name='place']").val("");
		$(":input[name='content']").val("");
		$(":input[name='all_day_flag']").prop("checked",false).change();
		$("#m_pop_memo .pop_btn_wrap .btn01").html("등록");
		$("#m_pop_memo .pop_btn_wrap .btn01").addClass("full");
		$("#m_pop_memo .pop_btn_wrap .btn02_right.deleteMemo").hide();
		
		$("#m_pop_memo").show();
	}
	
	//일정쓰기
	function addMemo() {
		
		if (!$.trim($(":input[name='memo_name']").val())) {
			alert("일정명을 입력해주세요.");
			return;
		}
		
		if (!$(":input[name='memo_date']").val()) {
			alert("일정 시작 날짜를 입력해주세요.");
			return;
		}
		
		if (!$(":input[name='memo_edate']").val()) {
			alert("일정 종료 날짜를 입력해주세요.");
			return;
		}

		
		if(!$(":input[name='all_day_flag']").is(":checked")){
			$(":input[name='all_day_flag']").val("N");
			
			if (!$(":input[name='memo_start_time']").val()) {
				alert("시작시간을 선택해주세요.");
				return;
			}
			
			if (!$(":input[name='memo_end_time']").val()) {
				alert("종료시간을 선택해주세요.");
				return;
			}

			var start = $(":input[name='memo_date']").val() + " " + $(":input[name='memo_start_time']").val();
			var end = $(":input[name='memo_edate']").val() + " " +  $(":input[name='memo_end_time']").val();
			
			var startDate = new Date(start);
			var endDate = new Date(end);
			
			if(startDate.getTime()>=endDate.getTime()){
				alert("종료시간을 시작시간 이후로 입력해주세요\n일자 또는 시간을 확인하세요.");
				return;
			}
			/* 
			if (ampmTimeToMinute($(":input[name='memo_start_time']").val()) > ampmTimeToMinute($(":input[name='memo_end_time']").val())) {
				alert("종료시간을 시작시간 이후로 입력해주세요.");
				return;
			} */
			
		}else{
			$(":input[name='all_day_flag']").val("Y");
		}
		
		
		
		$(":input[name='memo_edate']").prop("disabled", false);
		$(":input[name='memo_start_time']").prop("disabled", false);
		$(":input[name='memo_end_time']").prop("disabled", false);	
		
		$.ajax({
	        type: "POST",
	        url: "${HOME}/ajax/pf/lesson/addMemo",
	        data: $("#memoForm").serialize(),
	        dataType: "json",
	        success: function(data, status) {
	        	if (data.status == "200") {
	        		if ($(":input[name='memo_seq']").val()) {
	        			alert("수정이 완료되었습니다.");
	        		} else {
	        			alert("등록이 완료되었습니다.");
	        		}
            		$("#m_pop_memo").hide();
            		getScheduleMemo();
            	} else {
            		alert("오류가 발생했습니다.");
            	}
	        },
	        error: function(xhr, textStatus) {
	            alert("오류가 발생했습니다.");
	        }
	    });
	}
	
	function deleteMemo() {
		
		if (!confirm("일정을 삭제하시겠습니까?")) {
			return;
		}
		
		$.ajax({
	        type: "POST",
	        url: "${HOME}/ajax/pf/lesson/deleteMemo",
	        data: $("#memoForm").serialize(),
	        dataType: "json",
	        success: function(data, status) {
	        	if (data.status == "200") {
	        		alert("삭제가 완료되었습니다.");
            		$("#m_pop_memo").hide();
            		getScheduleMemo();
            	} else {
            		alert("오류가 발생했습니다.");
            	}
	        },
	        error: function(xhr, textStatus) {
	            alert("오류가 발생했습니다.");
	        }
	    });
	}
</script>
</head>

<body class="">

	<!-- s_container_table -->
	<div id="container" class="container_table">
		<!-- s_contents -->
		<div class="contents main">

			<!-- s_left_mcon -->
			<div class="left_mcon aside_l schd">
				<div class="sub_menu adm_schd">
					<div class="title">학사일정</div>

					<!-- s_메뉴 -->
					<div class="boardwrap">
						<div class="title1 wrapx">
							<span class="sp_tt" onClick="location.href='${HOME }/admin/acaSchedule/scheduleManagement'">학사일정</span>
						</div>
						<ul class="panel" style="display: table !important;">
							<li class="wrap xx"></li>
						</ul>
						<div class="title1 wrapx">
							<span class="sp_tt on" onClick="location.href='${HOME }/admin/acaSchedule/MYscheduleMemo'">개인일정</span>
						</div>
						<ul class="panel on">
							<li class="wrap xx"></li>
						</ul>
					</div>
					<!-- e_메뉴 -->

				</div>
			</div>

			<!-- s_main_con -->
			<div class="main_con cld">
				<!-- s_메뉴 접기 버튼 -->
				<a class="btn_l" style="z-index:99;"><span>좌측 메뉴 접기/펴기</span></a>
				<!-- e_메뉴 접기 버튼 -->

				<!-- s_mpf_tabcontent2 -->
				<div class="mpf_tabcontent2">
					<!-- s_tt_wrap -->
					<div class="tt_wrap">
						<h3 class="am_tt tt2">개인일정</h3>
						<!-- s_sch_wrap -->
						<div class="sch_wrap">
						
						<button type="button" class="btn bt04 open99" onClick="openWriteMemo();">등록</button>

						</div>
						<!-- e_sch_wrap -->
					</div>
					<!-- e_tt_wrap -->

					<!-- s_calendar1 -->
					<div id="calendar1">
						<input type="hidden" name="calendarYear">
					</div>
					<!-- e_calendar1 -->
				</div>
			</div>
		</div>
	</div>
<!-- s_ 팝업 : 일정 쓰기 -->
<div id="m_pop_memo" class="pop_up_schd_write pop_up pop_up_schd_write_op_af mo99">
<!-- s_pop_wrap -->
<div class="pop_wrap">
<form id="memoForm" onSubmit="return false;">
<input type="hidden" name="memo_seq">
<input type="hidden" name="public_flag" value="01">
 		        <button class="pop_close close99" type="button">X</button>	  
                <p class="t_title">일정 등록</p>       

<!-- s_pop_swrap -->	   
<div class="pop_swrap">

<div class="swrap">
    <span class="tt">일정명</span>
    <div class="con"> 
    <textarea name="memo_name" class="ip_ta1" rows="2"></textarea>
    </div>    
</div>

<div class="swrap">
    <span class="tt">날짜</span>
    <div class="con"> 
        <div class="twrap1">
        	<input type="text" name="memo_date" class="dateyearpicker-input_1 ip_date" readonly style="width:170px;">
        	<span class="sign">~</span>
        	<input type="text" name="memo_edate" class="dateyearpicker-input_1 ip_date" readonly style="width:170px;">
        </div>
        <div class="twrap2">
        </div>
    </div>
</div>
 
<div class="swrap time_pick2">
    <span class="tt">시간</span>
    <div class="con">  
<!-- 미등록 시 class : nonregi 추가 -->
    <input type="text" class="timepicker1 ip_time" name="memo_start_time" readonly>
    <span class="sign">~</span>
<!-- 미등록 시 class : nonregi 추가 -->
    <input type="text" class="timepicker2 ip_time" name="memo_end_time" readonly>
    <span class="sign"><input type="checkbox" name="all_day_flag" style="width:20px;height:20px;"></span><span style="line-height: 50px;font-size: 13px;">하루종일
    	<input type="hidden" name="all_day" value="N">
    </span>
    </div>
</div>  
   
<div class="swrap">
    <span class="tt">장소 (선택)</span>
    <div class="con"> 
<!-- 미등록 시 class : nonregi 추가 -->
        <input type="text" name="place" class="ip_tt1" value="">
    </div>
</div>  

<div class="swrap tarea free_textarea">
    <span class="tt t_a">일정 내용 (선택)</span>
    <div class="con">
<!-- 미등록 시 class : nonregi 추가 -->
    <textarea name="content" class="tarea1" style="height: 40px;"></textarea>
    </div>
</div>	
<%--
<div class="spop_wrap" style="display:none;">
                    <div class="a_mp">
                        <span class="pt01"><img src="${IMG}/ph_m.png" alt="학생공개" class="pt_img"></span>
                        <div class="ssp1">
                            <span class="sp1">해부학 수업</span>
                            <span class="sp2">의과대학&#62;본과&#62;2학년</span>                        
                        </div>
                        <button class="btn_c" title="삭제하기">X</button>
                    </div>

 </div>			
--%>
</div>
<!-- e_pop_swrap --> 
                <div class="t_dd">
                   
                    <div class="pop_btn_wrap">
                    <button type="button" class="btn01 full" onclick="javascript:addMemo();">등록</button>
                    <button type="button" class="btn02_right deleteMemo" style="display:none;" onclick="deleteMemo();">삭제</button>
                     <!--
                     <button type="button" class="btn01">나만 보기 등록</button>
                     <button type="button" class="btn02_right" onclick="toggleSubPopup('#pop_spop1')">학생 공개 등록</button>
                     -->                    
                    </div>
 <!-- s_팝업 속 팝업 : 공개 등록 -->             
            <div id="pop_spop1">

                <div class="spop_wrap show">
                    <div class="sswrap">
                        <input type="checkbox" class="chk1">
                        <span class="sp1">학생 전체공개</span>
                    </div>
                    <div class="sswrap">
                        <input type="checkbox" class="chk1">
                        <span class="sp1">해부학 수업</span>
                        <span class="sp2">의과대학&#62;본과&#62;2학년</span>
                    </div>
                    <div class="sswrap">
                        <input type="checkbox" class="chk1">
                        <span class="sp1">해부학 실습</span>
                        <span class="sp2">의과대학&#62;본과&#62;2학년</span>
                    </div>
                    <div class="sswrap">
                        <input type="checkbox" class="chk1">
                        <span class="sp1">형성평가</span>
                        <span class="sp2">의과대학&#62;본과&#62;1학년</span>
                    </div>                        
                   <button class="btn01 open101">등록</button>
                   
                   <span onclick="toggleSubPopup('#pop_spop1')" class="pop_close_s" title="닫기">X</span>
                </div>
            </div>
<!-- e_팝업 속 팝업 : 공개 등록 --> 
                </div>
</form>
</div> 
<!-- e_pop_wrap -->
</div>
<!-- e_ 팝업 : 일정 쓰기 -->
</body>
</html>