<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<script>
	$(document).ready(function() {
		bindPopupEvent("#m_pop1", ".open1");
		getSFResearchList();
	});
	var this_version_seq = "";
	function getSFResearchList() {
		$.ajax({
			type : "POST",
			url : "${HOME}/ajax/admin/survey/lessonSatisfaction/list",
			data : {
				"sr_type" : 1
			},
			dataType : "json",
			success : function(data, status) {
				if(data.status=="200"){
					$("#sfResearchListAdd").html("");
					var htmls = "";					
					$.each(data.sfResearchList,function(index){
						var checked = "";
						htmls = '<tr class="base">';
						htmls += '<td class="td_1 w1 bd01">'+(index+1)+'</td>';
						htmls += '<td class="td_1 w2 bd01">'+this.reg_date+'</td>';
						htmls += '<td class="td_1 w3 bd01">'+this.multiplequestion+'</td>';
						htmls += '<td class="td_1 w3 bd01">'+this.essayquestion+'</td>';
						htmls += '<td class="td_1 w4 bd01"><button class="btn_sv2 open1" onClick="popupOpen('+this.sr_seq+');">Web 미리보기</button>';
						htmls += '<button class="btn_sv3" onclick="modifyResearch('+this.sr_seq+');">수정하기</button></td>';
						htmls += '<td class="td_1 w5 bd01">';
						htmls += '<div class="chk_wrap">';
						if(!isEmpty(this.version)){
							htmls += '<span class="base_chk"></span><span class="version">v '+this.version+'</span>';
							checked = 'checked="checked"';
							this_version_seq = this.sr_seq;
						}
						htmls += '</div><input class="btn_ip" type="radio" name="rd_chk" '+checked+' value="'+this.sr_seq+'">';
						htmls += '</td>';
						htmls += '</tr>';	
						$("#sfResearchListAdd").append(htmls);
					});
					
				}else{
					alert("수업만족도 조사 리스트 가져오기 실패");
				}
			},
			error : function(xhr, textStatus) {
				alert("실패");
				//alert("오류가 발생했습니다.");
				document.write(xhr.responseText);
			},
			beforeSend : function() {
			},
			complete : function() {
			}
		});
	}
	
	function popupOpen(sr_seq){
		$.ajax({
			type : "POST",
			url : "${HOME}/ajax/admin/survey/lessonSatisfaction/create/list",
			data : {
				"sr_seq" : sr_seq
			},
			dataType : "json",
			success : function(data, status) {
				$("#popupListAdd").empty();
				if (data.status == "200") {
					var htmls = "";
					var pre_srh_seq = "";
					var pre_srh_detail_type = "";
					$.each(data.questionList, function(index) {
						
						if(index != 0 && pre_srh_seq != this.srh_seq){
							htmls+='</div></td></tr></table>';
					        $("#popupListAdd").append(htmls);
						}
	            			
						if(pre_srh_seq != this.srh_seq){
							htmls='<table class="tab_table_u ip_tb01" style="display: table;"><tr class="">'
				            	+ '<td class="th01 list w1">'+this.srh_num+'</td>'
				            	+ '<td class="th01 w2 td_l">'+this.srh_subject+'</td>'
				            	+ '</tr>'
				            	+ '<tr class="blk">'
				            	+ '<td class="th01 blk">&nbsp;</td>'  
				            	+ '<td class="td1 td_l">'
				            	+ '<div class="type type1">'	
								
							
						}
						
						//설문 유형 1:객관식 2:주관식 3:표형
						if(this.srh_type=="1"){
							htmls+='<div class="ip_wrap">'
							    + '<input type="radio" name="Rd1" id="Rd1_0" class="rd01">'
							    + '<span class="ip_tt">'+this.sri_item_explan+'</span>'
							    + '</div>'
						}else if(this.srh_type=="2"){
							htmls+= '<div class="ip_wrap free_textarea">'
								+'<textarea class="tarea01a" style="height: 70px;" placeholder="※ 여기에 서술해 주십시오."></textarea></div>';
						}
						
				        if(data.questionList.length == (index+1)){
				        	htmls+='</div></td></tr></table>';
					        $("#popupListAdd").append(htmls);
				        }
				        pre_srh_seq = this.srh_seq;
					});
				} else {
					alert("수업만족도 설문 리스트 가져오기 실패");
				}
			},
			error : function(xhr, textStatus) {
				alert("실패");
				//alert("오류가 발생했습니다.");
				document.write(xhr.responseText);
			},
			beforeSend : function() {
			},
			complete : function() {
			}
		});
		$("#m_pop1").show();
			
	}
	
	function modifyResearch(sr_seq){
		post_to_url("${HOME}/admin/survey/lessonSatisfaction/create", {"sr_seq":sr_seq});
	}
		
	function change(obj){
		$(obj).hide();
		$("#saveBtn").show();	
		$("div.chk_wrap").hide();
		$("input[name=rd_chk]").show();
	}
	
	function saveVersion(){
		if(!confirm("해당 설문을 기본 설문으로 설정하시겠습니까?")){
			return;
		}
		
		var sr_seq = $("input[name=rd_chk]:checked").val();
		//현재 설정된 설문이랑 같은 설문 일경우
		if(this_version_seq == sr_seq){
			alert("이미 기본 설문으로 설정된 설문 입니다.")
			return;
		}
			
		$.ajax({
			type : "POST",
			url : "${HOME}/ajax/admin/survey/lessonSatisfaction/basicResearch/insert",
			data : {
				"sr_seq":$("input[name=rd_chk]:checked").val()
				,"sr_type" : 1
			},
			dataType : "json",
			success : function(data, status) {
				if(data.status=="200"){
					getSFResearchList();
					$("#saveBtn").hide();
					$("#changeBtn").show();
					alert("설정 완료 되었습니다.");
				}else{
					alert("설정 실패 하였습니다.");
				}
			},
			error : function(xhr, textStatus) {
				alert("실패");
				//alert("오류가 발생했습니다.");
				//document.write(xhr.responseText);
			},
			beforeSend : function() {
			},
			complete : function() {
			}
		});
	}
	
</script>
</head>

<body>
	<noscript title="브라우저 자바스크립트 차단 해제 안내">
		<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
	</noscript>

	<!-- s_skipnav -->
	<div id="skipnav">
		<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
	</div>
	<!-- e_skipnav -->

	<!-- s_container_table -->
	<div id="container" class="container_table">
		<!-- s_contents -->
		<div class="contents main">

			<!-- s_left_mcon -->
			<div class="left_mcon">
				<div class="sub_menu adm_survey">
					<div class="title">설문</div>
					<ul class="sub_panel">
						<li class="mn"><a href="${HOME }/admin/survey/lessonSatisfaction" class="on">수업만족도 조사</a></li>
						<li class="mn"><a href="${HOME }/admin/survey/currSatisfaction">과정만족도 조사</a></li>
						<li class="mn"><a href="#" class="">설문 등록 / 관리</a></li>
					</ul>
				</div>
			</div>
			<!-- e_left_mcon -->

			<!-- s_main_con -->
			<div class="main_con adm_survey">

				<!-- s_tt_wrap -->
				<div class="tt_wrap">
					<h3 class="am_tt">
						<span class="tt">수업만족도 조사</span>
					</h3>
				</div>
				<!-- e_tt_wrap -->

				<!-- s_adm_content1 -->
				<div class="adm_content1 survey">
					<!-- s_tt_wrap -->

					<!-- s_wrap_wrap -->
					<div class="wrap_wrap">

						<div class="tab_wrap_cc">
							<button class="tab01 tablinks active" onclick="location.href='${HOME}/admin/survey/lessonSatisfaction'">양식 등록</button>
							<button class="tab01 tablinks" onclick="">결과 확인</button>
						</div>

						<!-- s_wrap_wrap -->
						<div class="wrap_wrap">
							<!-- s_btnwrap_s2 -->
							<div class="btnwrap_s2">

								<!-- s_btnwrap_s2_s -->
								<div class="btnwrap_s2_s">
									<button class="btn_tt1" onclick="post_to_url('${HOME}/admin/survey/lessonSatisfaction/create');">등록</button>
									<!-- <button class="btn_up1 open1">엑셀업로드</button> -->
								</div>
								<!-- e_btnwrap_s2_s -->

							</div>
							<!-- e_btnwrap_s2 -->
						</div>
						<!-- e_wrap_wrap -->

						<table class="mlms_tb survey th">
							<tr>
								<td class="th01 w1">no</td>
								<td class="th01 w2">등록일</td>
								<td class="th01 w3">객관식</td>
								<td class="th01 w3">주관식</td>
								<td class="th01 w4">설문 미리보기</td>
								<td class="th01 w5">기본 설문
									<button class="btn_sv1a" id="changeBtn" onClick="change(this);" style="display: inline-block">변경</button>
									<button class="btn_sv1b" id="saveBtn" onClick="saveVersion();" style="display: none">저장</button>
								</td>
							</tr>
						</table>
						<!-- s_tb_wrap -->
						<div class="tb_wrap">
							<table class="mlms_tb survey" id="sfResearchListAdd">
								
								
							</table>
						</div>
						<!-- e_tb_wrap -->

					</div>
					<!-- e_wrap_wrap -->

				</div>
				<!-- e_adm_content1 -->
			</div>
			<!-- e_main_con -->
		</div>
		<!-- e_contents -->


	</div>
	<!-- e_container_table -->
	
	<!-- s_수업 만족도 조사 양식 팝업 -->
	<div id="m_pop1" class="form_survey_web mo1">
	<!-- s_pop_wrap --> 
	    <div class="pop_wrap">
			<button class="pop_close close1" type="button">X</button>
				<p class="t_title">수업만족도 조사</p>   
	                
			<!-- s_content -->     
			<div class="content">   
			
				<!-- s_tb_wrap -->  
				<div class="tb_wrap">
				
					<div class="wrap_wrap">
						<div class="tt">본 설문지는 수업개선의 기초 및 교육의 질적인 향상을 위해 실시합니다.<br>
							<span class="sp_wrap2">기간 내 수업만족도 조사를 완료하지 않을 경우 매주 -0.5 점씩 감점 처리 됩니다. <br>바로 설문을 완료하여 제출해주세요.</span>
						</div>
						
					
						<div class="wrap_s3" id="popupListAdd">
							
							
						</div>
						<!-- s_bt_wrap -->	
				        <div class="bt_wrap" style="display: table;">
		                    <button class="bt_2" onClick="$('.close1').click();">확인</button>
						</div>
						<!-- e_bt_wrap -->
					</div>
				
				</div>
				<!-- e_tb_wrap --> 
			
			</div>                                                                                                 
			<!-- e_content -->                                                                                                            
		</div>
		<!-- e_pop_wrap -->                
	</div>
	<!-- e_수업 만족도 조사 양식 팝업 -->
</body>
</html>