<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function(){
				getCodeList();
			});
			
			function getCodeList() {
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/admin/user/code/list",
			        data: {},
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var d_list = data.staff_department_code_list;
			        		codeSelectBoxSetting("#departmentSelectBox", d_list);
			        	} else {
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			
			function codeSelectBoxSetting(t, list) {
				var target = $(t).find("div.uoptions");
				var listHtml = '<span class="uoption" value="">선택</span>';
				$(list).each(function() {
					if (t == "#departmentSelectBox") {
						listHtml += '<span class="uoption" value="' + this.code + '">' + this.code_name + '</span>';
					}
				});
				$(target).html(listHtml);
			}
			
			
			function previewPictureUpload() {
				var target = $("#uploadFile");
				target.change(function(){
					var file = $(this).val();
					var previewPicture = $("#previewPicture");
					if (file != "") {
						var attachFileName = file.substring(file.lastIndexOf('\\') + 1);
						var ext = attachFileName.substring(attachFileName.lastIndexOf('.') + 1).toLowerCase();
						var fileMaxSize = 10; //10MB
						var fileSize = this.files[0].size/1024/1024;
						if (/(jpg|png|jpeg)$/i.test(ext) && (fileSize < fileMaxSize)) {
							previewPicture.attr("src", loadPreview(this, "previewPicture"));
						} else {
							alert("10MB이하 이미지 파일(jpg, png, jpeg)만 첨부 가능합니다.");
							previewPicture.attr("src", "${IMG}/profile_default.png");
							if (/msie/.test(navigator.userAgent.toLowerCase())) { // ie
								$("#uploadFile").replaceWith($("#uploadFile").clone(true)); 
							} else { // other browser 
								$("#uploadFile").val(""); 
							}
						}
					} else {
						previewPicture.attr("src", "${IMG}/profile_default.png");
					}
				});
				target.click();
			}
			
			function userSubmit() {
				var name = $("#userName").val();
				if (isBlank(name) || isEmpty(name)) {
					alert("이름을 입력해주세요.");
					$("#userName").focus();
					return false;
				}
				
				var tel1 = $("#tel1").val().trim();
				var tel2 = $("#tel2").val().trim();
				var tel3 = $("#tel3").val().trim();
				if (tel1 != "" || tel2 != "" || tel3 != "") {
					var tel = tel1 + "-" + tel2 + "-" + tel3;
					if (!isTelType(tel)) {
						alert("연락처의 형식이 올바르지 않습니다.");
						$("#tel1").focus();
						return false;
					}
					$("#tel").val(tel);
				}
				
				var professorId = $("#professorId").val().trim();
				$("#professorId").val(professorId);
				
				var email = $("#userEmail").val();
				if (isBlank(email) || isEmpty(email)) {
					alert("E-mail을 입력해주세요.");
					$("#userEmail").focus();
					return false;
				}
				
				if (!isEmailType(email)) {
					alert("E-mail의 형식이 올바르지 않습니다.");
					$("#userEmail").focus();
					return false;
				}
				
				var userId = $("#userId").val().trim();
				$("#userId").val(userId);
				
				var departmentValue = $("#departmentSelectBox").attr("value");
				$("#departmentCode").val(departmentValue);
				
				$("#userFrm").attr("action","${HOME}/ajax/admin/user/staff/create").ajaxForm({
					beforeSend: function () {
						$.blockUI();
					},
					type: "POST",
					dataType:"json",
					success:function(data){
						if (data.status == "200") {
							alert("등록 되었습니다.");
							location.href="${HOME}/admin/user/staff/list";
						} else if (data.status == "301") {//사번 중복검사
							alert("사번이 중복됩니다.");
							$("#professorId").focus();
						} else if (data.status == "302") {//이메일 중복검사
							alert("이메일이 중복됩니다.");
							$("#userEmail").focus();
						} else if (data.status == "303") {//아이디 중복검사
							alert("아이디가 중복됩니다.");
							$("#userId").focus();
						} else {
							alert("등록에 실패 하였습니다.");
							location.reload();
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						document.write(xhr.responseText);
				    },
			        complete:function() {
			            $.unblockUI();
			        }
				}).submit();
				
			}
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
			<div class="contents main">
				<div class="left_mcon aside_l user">
		            <div class="sub_menu user">
		                <div class="title">이용자관리</div>
						<div class="m_schwrap">
							<div class="wrap_s1_uselectbox">
							 	<div class="uselectbox" id="userTypeSelectBox" value="PROFESSOR">
									<span class="uselected">교수</span>
									<span class="uarrow">▼</span>
									<div class="uoptions">
										<span class="uoption" value="PROFESSOR">교수</span>
										<span class="uoption" value="STUDENT">학생</span>
										<span class="uoption" value="STAFF">직원</span>
									</div>
								</div>
							</div>
							<input type="text" class="ip_nm" id="searchUserName" value="" placeholder="이름">
							<button class="btn_sch" onclick="searchUser();"></button>
						</div>
						
		                <div class="boardwrap">
							<div class="title1">
								<span class="sp_tt two">이용자 등록정보 관리<br><span class="stt">( 교수 / 직원 )</span></span>
							</div>
							<ul class="m1 panel">
								<li class="wrap"><a href="${HOME}/admin/user/code/attend/list" class="tt_2">재직상태 목록관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/department/list" class="tt_2">소속 목록관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/position/list" class="tt_2">교수 직위 목록관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/specialty/list" class="tt_2">교수 세부전공 목록관리</a></li>
							</ul>
							<div class="title1"><span class="sp_tt">교수 관리</span></div>
							<ul class="panel" style="display: table !important;">
		                        <li class="wrap"><a href="${HOME}/admin/user/pf/list" class="tt_2">등록 / 검색</a></li>
		                    </ul>
							<div class="title1"><span class="sp_tt">학생 관리</span></div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/user/st/list" class="tt_2">등록 / 검색</a></li>
<!-- 								<li class="wrap"><a href="../adm/adm_user_system.html" class="tt_2">학생 학사체계별 조회</a></li> -->
<!-- 								<li class="wrap"><a href="#" class="tt_2">성취도 관리</a></li> -->
							</ul>
							<div class="title1 wrapx"><span class="sp_tt on" onclick="location.href='${HOME}/admin/user/staff/list'">직원 관리</span></div>
							<ul class="panel on">
								<li class="wrap xx"></li>
							</ul>
						</div>
		            </div>
		        </div>
				<div class="main_con user">	
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<div class="tt_wrap">
					    <h3 class="am_tt">
							<span class="tt">이용자관리<span class="sign1">&gt;</span>직원 관리<span class="sign1">&gt;</span>직원 등록</span>
					    </h3>                    
					</div>
					<div class="adm_content3 user">
						<form id="userFrm" name="userFrm" onsubmit="return false;" enctype="multipart/form-data">
		            		<input type="file" id="uploadFile" name="uploadFile" value="" class="blind-position">
		            		<input type="hidden" id="departmentCode" name="departmentCode" value="">
		            		<input type="hidden" id="tel" name="tel" value="">
							<table class="table_my">
							    <tbody>
							        <tr>
							            <td rowspan="7" class="td01 w01 bd04">
							            	<div class="ph_wrap">
					                        	<div class="photo1"><img id="previewPicture" src="${IMG}/profile_default.png" alt="프로필 사진"></div>
					                        </div> 
					                        <button type="button" class="btn_reset" onclick="previewPictureUpload();">사진등록(선택)</button>
							            </td>
										<td class="td01 w02">이름<span class="tts1">*(필수)</span></td>
							            <td class="td01 w03">
							                <input type="text" class="ip_001" id="userName" name="userName" placeholder="이름입력(필수)">
							            </td>
							        </tr>
									<tr>
							            <td class="td01 w02">사번<span class="tts2">(선택)</span></td>
							            <td class="td01 w03">
							                <input type="text" class="ip_001" id="professorId" name="professorId" placeholder="사번입력(선택)">
							            </td>
							        </tr>
							        <tr>
							            <td class="td01 w02">소속<span class="tts2">(선택)</span></td>
							            <td class="td01 w03">
									    	<div class="wrap_s1_uselectbox">
						                        <div class="uselectbox" id="departmentSelectBox" value="">
							                        <span class="uselected">선택</span>
							                        <span class="uarrow">▼</span>			
							                        <div class="uoptions maxH200px">
							                        	<span class="uoption">선택</span>
							                        </div>
						                        </div>
					                        </div>
							            </td>
									</tr>
							       	<tr>
							            <td class="td01 w02">연락처<span class="tts2">(선택)</span></td>
							            <td class="td01 td_wrap1 w03">
											<input type="text" class="ip_tel1" id="tel1" maxlength="4" value="">            
					                        <span class="sign1">-</span>
					                        <input type="text" class="ip_tel1" id="tel2" maxlength="4" value="">
					                        <span class="sign1">-</span>
					                        <input type="text" class="ip_tel1" id="tel3" maxlength="4" value="">
							            </td>
							        </tr>
							        <tr>
							            <td class="td01 w02">E-mail<span class="tts1">*(필수)</span></td>
							            <td class="td01 w03">
							                <input type="text" class="ip_001" id="userEmail" name="userEmail" placeholder="E-mail입력(필수)">
							            </td>
							        </tr>
									<tr>
							            <td class="td01 w02">아이디<span class="tts2">(선택)</span></td>
							            <td class="td01 w03">
							                <input type="text" class="ip_001" id="userId" name="userId" placeholder="등록하지 않을 시 E-mail 정보로 자동 등록됩니다.">
							            </td>
							        </tr>
							        <tr>
							            <td class="td01 w02">패스워드<span class="tts3">(자동)</span></td>
							            <td class="td01 w03">등록 시 자동으로 ${initPassword}로 지정됩니다.</td>
							        </tr>
							    </tbody>
							</table>
						</form>
						<div class="bt_wrap">
						    <button class="bt_2" onclick="userSubmit();">등록</button>
							<button class="bt_3" onclick="location.href='${HOME}/admin/user/staff/list'">취소</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>