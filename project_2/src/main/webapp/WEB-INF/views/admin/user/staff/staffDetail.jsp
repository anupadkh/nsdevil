<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function(){
				getPFDetail();
				
				$(document).mouseup(function(e){
					layerPopupCloseInit(['div[id="pwdInitPopup"]']);
				});
			});
			
			function getPFDetail() {
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/admin/user/staff/detail",
			        data: {
			        	"user_seq":"${user_seq}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		userInfoSetting(data.user_info);
			        	} else {
			        		alert("정보 불러오기에 실패하였습니다.\n목록으로 이동합니다.");
			        		location.href="${HOME}/admin/user/staff/list";
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function userInfoSetting(info) {
				var userInfo = info;
				var userName = info.name;
				var attendName = info.attend_code_name;
				var accountUseState = info.account_use_state;
				var professorId = info.professor_id;
				var departmentName = info.staff_department_code_name;
				var tel = info.tel;
				var userEmail = info.email;
				var userId = info.id;
				var picturePath = info.picture_path;
				var regDate = info.reg_date;
				var dayOfWeek = info.day_of_week;
				if (isEmpty(picturePath)) {
					picturePath = "${DEFAULT_PICTURE_IMG}";
				} else {
					picturePath = "${RES_PATH}"+picturePath;
        		}
				$("#profileImg").attr("src", picturePath);
				
				var week = new Array('일', '월', '화', '수', '목', '금', '토');
				dayOfWeek = week[dayOfWeek];
				regDate = regDate + " ("+dayOfWeek+")";
				$("#regDate").html(regDate);
				$("#userName").html(userName);
				$("#pwdUserName").html(userName);
				
				var accountUseStateText = "계정 사용중";
				var accountUseClass = "";
				if (accountUseState == "N") {
					accountUseStateText = "계정 사용안함";
					accountUseClass = "xx";
				}
				$("#accountUseState").addClass(accountUseClass);
				$("#accountUseState").html(accountUseStateText);
				
				$("#attendName").html(attendName);
				$("#professorId").html(professorId);
				$("#departmentName").html(departmentName);
				if (tel != "") {
					$("#tel1").html(tel.split("-")[0]);
					$("#tel2").html(tel.split("-")[1]);
					$("#tel3").html(tel.split("-")[2]);
				}
				$("#userEmail").html(userEmail);
				$("#userId").html(userId);
			}
			
			function pwdInitPopupOpen() {
				$("#pwdInitPopup").show();
			}
			
			function pwdInitPopupClose() {
				$("#pwdInitPopup").hide();
			}
			
			function pwdInit() {
				$.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/admin/user/pwd/init",
			        data: {
			        	"user_seq":"${user_seq}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		
			        	} else {
			        		alert("패스워드 초기화에 실패하였습니다.");
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function getUserModifyView() {
				post_to_url('${HOME}/admin/user/staff/modify', {"userSeq":"${user_seq}"});
			}
			
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
			<div class="contents main">
				<div class="left_mcon aside_l user">
		            <div class="sub_menu user">
		                <div class="title">이용자관리</div>
						<div class="m_schwrap">
							<div class="wrap_s1_uselectbox">
							 	<div class="uselectbox" id="userTypeSelectBox" value="PROFESSOR">
									<span class="uselected">교수</span>
									<span class="uarrow">▼</span>
									<div class="uoptions">
										<span class="uoption" value="PROFESSOR">교수</span>
										<span class="uoption" value="STUDENT">학생</span>
										<span class="uoption" value="STAFF">직원</span>
									</div>
								</div>
							</div>
							<input type="text" class="ip_nm" id="searchUserName" value="" placeholder="이름">
							<button class="btn_sch" onclick="searchUser();"></button>
						</div>
						
		                <div class="boardwrap">
							<div class="title1">
								<span class="sp_tt two">이용자 등록정보 관리<br><span class="stt">( 교수 / 직원 )</span></span>
							</div>
							<ul class="m1 panel">
								<li class="wrap"><a href="${HOME}/admin/user/code/attend/list" class="tt_2">재직상태 목록관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/department/list" class="tt_2">소속 목록관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/position/list" class="tt_2">교수 직위 목록관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/specialty/list" class="tt_2">교수 세부전공 목록관리</a></li>
							</ul>
							<div class="title1"><span class="sp_tt">교수 관리</span></div>
							<ul class="panel" style="display: table !important;">
		                        <li class="wrap"><a href="${HOME}/admin/user/pf/list" class="tt_2">등록 / 검색</a></li>
		                    </ul>
							<div class="title1"><span class="sp_tt">학생 관리</span></div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/user/st/list" class="tt_2">등록 / 검색</a></li>
<!-- 								<li class="wrap"><a href="../adm/adm_user_system.html" class="tt_2">학생 학사체계별 조회</a></li> -->
<!-- 								<li class="wrap"><a href="#" class="tt_2">성취도 관리</a></li> -->
							</ul>
							<div class="title1 wrapx"><span class="sp_tt on" onclick="location.href='${HOME}/admin/user/staff/list'">직원 관리</span></div>
							<ul class="panel on">
								<li class="wrap xx"></li>
							</ul>
						</div>
		            </div>
		        </div>
				<div class="main_con user">
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<div class="tt_wrap">
					    <h3 class="am_tt"><span class="tt">이용자관리<span class="sign1">&gt;</span>직원 관리<span class="sign1">&gt;</span>직원 조회</span></h3>                    
					</div>
					<div class="adm_content3 user">
						<table class="table_my v1">
						    <tbody>
								<tr>
						            <td rowspan="8" class="td01 w01 bd04">
						                <div class="ph_wrap custom">
		                                    <div class="photo1"><img src="${DEFAULT_PICTURE_IMG}" alt="프로필 사진" id="profileImg"></div>
		                                </div>
						            </td>
									<td class="td00" colspan="2">
										<span class="sp_tt">[ 직원 ]</span>시스템 등록일 : <span class="sp_tm" id="regDate"></span>
						            </td>
						        </tr>
						        <tr>
		                            <td class="td01 w02">이름</td>
		                            <td class="td01 w03">
		                                <span class="ip_001v dis" id="userName"></span>
		                                <span class="ipid" id="accountUseState"></span>
		                                <span class="ip_001vaf dis" id="attendName"></span>
		                            </td>
		                        </tr>
								<tr>
		                        	<td class="td01 w02">사번</td>
		                            <td class="td01 w03">
		                                <span class="ip_001 dis" id="professorId"></span>
		                            </td>
		                        </tr>
						        <tr>
		                            <td class="td01 w02">소속</td>
		                            <td class="td01 w03">
		                                <span class="ip_001 dis" id="departmentName"></span>
		                            </td>
		                        </tr>
						       	<tr>
		                        	<td class="td01 w02">연락처</td>
		                            <td class="td01 td_wrap1 w03">
		                                <span class="ip_tel1 dis" id="tel1"></span>
		                                <span class="sign1">-</span>
		                                <span class="ip_tel1 dis" id="tel2"></span>
		                                <span class="sign1">-</span>
		                                <span class="ip_tel1 dis" id="tel3"></span>
		                            </td>
	                        	</tr>
						        <tr>
		                            <td class="td01 w02">E-mail</td>
		                            <td class="td01 w03">
		                                <span class="ip_001 dis" id="userEmail"></span>
		                            </td>
		                        </tr>
								<tr>
		                            <td class="td01 w02">아이디</td>
		                            <td class="td01 w03">
		                                <span class="ip_001 dis" id="userId"></span>
		                            </td>
		                        </tr>
						        <tr>
		                            <td class="td01 w02">패스워드<span class="tts3"></span></td>
		                            <td class="td01 w03">
		                                * * * *
		                                <button type="button" class="btn_reset0 open_s1" onclick="pwdInitPopupOpen();">패스워드 초기화</button>
		                            </td>
		                        </tr>
						    </tbody>
						</table>
						<div class="bt_wrap">
						    <button class="bt_1" onclick="getUserModifyView();">수정</button>
		                    <button class="bt_3" onclick="javascript:history.back();">목록</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- s_패스워드 초기화 팝업 --> 
		<div class="pop_s1 mo_s1" id="pwdInitPopup">
			<div class="wrap">
				<span class="tt">패스워드 초기화</span>
			    <span class="close close_s1" title="닫기" onclick="pwdInitPopupClose();">X</span> 
			    <div class="spop_wrap">
			    	<div class="sswrap1"></div>
			        <div class="sswrap3"><span class="nm" id="pwdUserName"></span>교수의<br>패스워드를 초기화 하시겠습니까?<br>( 초기 비밀번호 :  ${initPassword} ) 로 변경됨</div>                   
				</div>
				<div class="btn_wrap">
			    	<button class="btn1" onclick="javascript:pwdInit();">예</button>
				    <button class="btn2" onclick="pwdInitPopupClose();">아니오</button>
				</div>
			</div>               
		</div>
		<!-- e_패스워드 초기화 팝업 -->		
	</body>
</html>