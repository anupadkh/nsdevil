<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function(){		   
				bindPopupEvent("#pop_s1", ".open_s1");
				getStDetail();
			});
			
			function pwdResetPopupOpen(){
				$("#pop_s1").show();
			}
			
			function pwdResetPopupClose(){
				$("#pop_s1").hide();
			}
			
			function pwdReset(){
				$.ajax({
            		type: "GET",
            		url: "${HOME}/ajax/admin/user/pwd/init",
            		data: {
            			"user_seq" : "${user_seq}"
            			},
           			dataType: "json",
           			success: function(data, status) {
           				if(data.status=="200"){
           					alert("패스워드 초기화 되었습니다.");
           					pwdResetPopupClose();
           				}else{
           					alert("패스워드 초기화 실패");
           				}
        			},
        			error: function(xhr, textStatus) {
        				//alert("오류가 발생했습니다.");
        				document.write(xhr.responseText);
       				},beforeSend:function() {
  					},
  					complete:function() {
    				}
     			});
			}
			
			function getStDetail(){
				
				$.ajax({
            		type: "POST",
            		url: "${HOME}/ajax/admin/user/st/info",
            		data: {
            			"user_seq" : "${user_seq}"
            			},
           			dataType: "json",
           			success: function(data, status) {
           				if(data.status=="200"){
           					var tel = data.stInfo.tel;
           					var picturePath = "";
           					var day = "";
           					
           					var week = new Array('일', '월', '화', '수', '목', '금', '토');
           					
           					$("span[data-name=reg_date]").text(data.stInfo.reg_date + "("+week[data.stInfo.reg_date_day]+")");
           					
           					if(!isEmpty(tel)){
           						$("span[data-name=tel1]").text(data.stInfo.tel.split("-")[0]);
               					$("span[data-name=tel2]").text(data.stInfo.tel.split("-")[1]);
               					$("span[data-name=tel3]").text(data.stInfo.tel.split("-")[2]);	
           					}          					
           					$("span[data-name=id]").text(data.stInfo.id);
           					$("span[data-name=st_attend]").text(data.stInfo.code_name);
           					$("span[data-name=name]").text(data.stInfo.name);
           					$("span[data-name=email]").text(data.stInfo.email);  
           					
           					if (isEmpty(data.stInfo.picture_path)) {
           						picturePath = "${DEFAULT_PICTURE_IMG}";
           					} else {
           						picturePath = "${RES_PATH}"+data.stInfo.picture_path;
           	        		}
           					$("#profileImg").attr("src", picturePath);
           					
           					var htmls = '';
           					
           					$.each(data.stAcaInfo, function(index){
           						htmls += '<tr>'
								+'<td class="td_1">'+(index+1)+'</td>'
								+'<td class="td_1">'+this.year+'</td>'
								+'<td class="td_1">'+this.laca_name+'</td>'
								+'<td class="td_1">'+this.maca_name+'</td>'
								+'<td class="td_1">'+this.saca_name+'</td>'
								+'<td class="td_1">'+this.aca_name+'</td>'
								+'<td class="td_1">'+this.academic_name+'</td>'
								+'<td class="td_1"><button class="btn_tt4" onclick="">조회</button></td>'
								+'<td class="td_1"><button class="btn_tt4" onclick="">조회</button></td>'
								+'<td class="td_1"><button class="btn_tt4" onclick="">조회</button></td>'
								+'<td class="td_1"><button class="btn_tt4" onclick="">조회</button></td>'
								+'<td class="td_1"><button class="btn_tt4" onclick="">조회</button></td>'
								+'</tr>';           						
           					});
           					
           					$("#acaListAdd").html(htmls);
           					
           					$("span[data-name=year]").text(data.stNowAcaInfo.year);
           					$("span[data-name=laca_name]").text(data.stNowAcaInfo.laca_name);
           					$("span[data-name=maca_name]").text(data.stNowAcaInfo.maca_name);
           					$("span[data-name=saca_name]").text(data.stNowAcaInfo.saca_name);
           					$("span[data-name=aca_name]").text(data.stNowAcaInfo.aca_name);
           					$("span[data-name=academic_name]").text(data.stNowAcaInfo.academic_name);
           					
           				}else{
           					alert("학생정보 가져오기 실패");
           				}
        			},
        			error: function(xhr, textStatus) {
        				//alert("오류가 발생했습니다.");
        				document.write(xhr.responseText);
       				},beforeSend:function() {
  					},
  					complete:function() {
    				}
     			}); 
			}
									
			function acaSystemSet(level, seq){
	       		if(level==1){
       				$("#m_seq_add span").remove();
                    $("#m_seq").attr("data-value","");
                    $("#m_seq").text("중분류");
       				$("#s_seq_add span").remove();
                    $("#s_seq").attr("data-value","");
                    $("#s_seq").text("소분류");	
       				$("#aca_seq_add span").remove();
                    $("#aca_seq").attr("data-value","");
                    $("#aca_seq").text("기간");
	       			acasystemStageOfList(2, seq);	
	       		
	       		}else if(level==2){
	       			$("#s_seq_add span").remove();
	       			$("#s_seq").attr("data-value","");
	       			$("#s_seq").text("소분류");
       				$("#aca_seq_add span").remove();
                    $("#aca_seq").attr("data-value","");
                    $("#aca_seq").text("기간");	       			
	       			acasystemStageOfList(3, $("#l_seq").attr("data-value"), seq);
	       				
	       		}else if(level==3){
       				$("#aca_seq_add span").remove();
                    $("#aca_seq").attr("data-value","");
                    $("#aca_seq").text("기간");	       			
       				acasystemStageOfList(4, $("#l_seq").attr("data-value"), $("#m_seq").attr("data-value"), seq);	       			
	       		}
	       	}
	        
            function acasystemStageOfList(level, l_seq, m_seq, s_seq){
            	
            	if(level == ""){
            		alert("분류 없음");
            		return;
            	}
            	$.ajax({
            		type: "POST",
            		url: "${HOME}/ajax/admin/academic/academicSystem/acaStageOfList",
            		data: {
            			"level" : level,
            			"l_seq" : l_seq,                	   
            			"m_seq" : m_seq,
            			"s_seq" : s_seq
            			},
           			dataType: "json",
           			success: function(data, status) {
           				var cate = "";
           				if(level == 1)
           					cate = "대분류";
           				else if(level == 2)
           					cate = "중분류";
           				else if(level == 3)
           					cate = "소분류";
           				else if(level == 4)
           					cate = "기간";
           				
           				var htmls = '<span class="uoption" onClick="acaSystemSet('+level+',\'\');" data-value="">'+cate+'</span>';
           				$.each(data.list, function(index){
           					htmls +=  '<span class="uoption" onClick="acaSystemSet('+level+','+this.aca_system_seq+');" data-value="'+this.aca_system_seq+'">'+this.aca_system_name+'</span>';
           				});

           				if(level == 1){
           					$("#l_seq_add span").remove();
           					$("#l_seq_add").html(htmls);        
           				}	                   
           				else if(level == 2){
           					$("#m_seq_add span").remove();
           					$("#m_seq_add").html(htmls);         
           				}
           				else if(level == 3){
           					$("#s_seq_add span").remove();
           					$("#s_seq_add").html(htmls);
        				}else{
        					$("#aca_seq_add span").remove();
           					$("#aca_seq_add").html(htmls);
        				}
        			},
        			error: function(xhr, textStatus) {
        				//alert("오류가 발생했습니다.");
        				document.write(xhr.responseText);
       				},beforeSend:function() {
  					},
  					complete:function() {
    				}
     			}); 
		   }
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
		    <div class="contents main">
		        <div class="left_mcon aside_l user">
		            <div class="sub_menu user">
		                <div class="title">이용자관리</div>
						<div class="m_schwrap">
							<div class="wrap_s1_uselectbox">
							 	<div class="uselectbox" id="userTypeSelectBox" value="PROFESSOR">
									<span class="uselected">교수</span>
									<span class="uarrow">▼</span>
									<div class="uoptions">
										<span class="uoption" value="PROFESSOR">교수</span>
										<span class="uoption" value="STUDENT">학생</span>
										<span class="uoption" value="STAFF">직원</span>
									</div>
								</div>
							</div>
							<input type="text" class="ip_nm" id="searchUserName" value="" placeholder="이름">
							<button class="btn_sch" onclick="searchUser();"></button>
						</div>
						
						
						<div class="boardwrap">
							<div class="title1">
								<span class="sp_tt two">이용자 등록정보 관리<br><span class="stt">( 교수 / 직원 )</span></span>
							</div>
							<ul class="m1 panel">
								<li class="wrap"><a href="${HOME}/admin/user/code/attend/list" class="tt_2">재직상태 목록관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/department/list" class="tt_2">소속 목록관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/position/list" class="tt_2">교수 직위 목록관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/specialty/list" class="tt_2">교수 세부전공 목록관리</a></li>
							</ul>
							<div class="title1"><span class="sp_tt">교수 관리</span></div>
							<ul class="panel" style="display: table !important;">
		                        <li class="wrap"><a href="${HOME}/admin/user/pf/list" class="tt_2">등록 / 검색</a></li>
		                    </ul>
							<div class="title1 wrapx"><span class="sp_tt">학생 관리</span></div>
							<ul class="panel on">
								<li class="wrap"><a href="${HOME }/admin/user/st/list" class="tt_2 on">등록 / 검색</a></li>
<!-- 								<li class="wrap"><a href="../adm/adm_user_system.html" class="tt_2">학생 학사체계별 조회</a></li> -->
<!-- 								<li class="wrap"><a href="#" class="tt_2">성취도 관리</a></li> -->
							</ul>
							<div class="title1 wrapx"><span class="sp_tt" onclick="location.href='${HOME}/admin/user/staff/list'">직원 관리</span></div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
		            </div>
		        </div>
		
				<!-- s_main_con -->
				<div class="main_con user">
					<!-- s_메뉴 접기 버튼 -->
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a> 
					<!-- e_메뉴 접기 버튼 -->
	
					<!-- s_tt_wrap -->
					<div class="tt_wrap">
						<h3 class="am_tt">
							<span class="tt">이용자관리<span class="sign1">&gt;</span>학생 관리<span class="sign1">&gt;</span>학생 조회</span>
						</h3>
					</div>
					<!-- e_tt_wrap -->
	
					<!-- s_adm_content3 -->
					<div class="adm_content3 user">
						<!-- s_tt_wrap -->
						<!-- s_.table_my -->
						<table class="table_my v1">
							<tbody>
								<tr>
									<td rowspan="6" class="td01 w01 bd04">
										<!-- s_ph_wrap -->
										<div class="ph_wrap custom">
											<div class="photo1">
												<img src="" alt="프로필 사진" id="profileImg">
											</div>
										</div> <!-- s_ph_wrap -->
	
									</td>
									<td class="td00" colspan="2"><span class="sp_tt">[학생 ]</span>시스템 등록일 : 
									<span class="sp_tm" data-name="reg_date"></span></td>
								</tr>
								<tr>
									<td class="td01 w02">학번<span class="tts1">*(필수)</span></td>
									<td class="td01 w03">
										<span class="ip_001v dis" data-name="id"></span>
										<span class="ip_tt1">재학상태</span> <span class="ip_001vaf dis" data-name="st_attend"></span>
									</td>
								</tr>
	
								<tr>
									<td class="td01 w02">이름<span class="tts1">*(필수)</span></td>
									<td class="td01 w03"><span class="ip_001 dis" data-name="name"></span>
									</td>
								</tr>
	
								<tr>
									<td class="td01 w02">연락처<span class="tts2">(선택)</span></td>
									<td class="td01 td_wrap1 w03"><span class="ip_tel1 dis" data-name="tel1"></span>
										<span class="sign1">-</span> <span class="ip_tel1 dis" data-name="tel2"></span>
										<span class="sign1">-</span> <span class="ip_tel1 dis" data-name="tel3"></span>
	
									</td>
								</tr>
	
								<tr>
									<td class="td01 w02">E-mail<span class="tts1">*(필수)</span></td>
									<td class="td01 w03"><span class="ip_001 dis" data-name="email"></span>
									</td>
								</tr>
	
								<tr>
									<td class="td01 w02">패스워드<span class="tts3"></span></td>
									<td class="td01 w03">* * * *
										<button type="button" class="btn_reset0 open_s1" onClick="pwdResetPopupOpen();">패스워드 초기화</button>
									</td>
								</tr>
							</tbody>
						</table>
						<!-- e_.table_my -->
	
						<div class="tt_sys">진행중 학사체계</div>
						<!-- s_st_sys_wrap -->
						<div class="st_sys_wrap v1">
							<!-- s_rwrap -->
							<div class="rwrap">
	
								<!-- s_wrap_s -->
								<div class="wrap_s a1st">
	
									<div class="wrap_ss">
										<div class="ss1">
											<span class="tts1">학사체계</span> 
											<span class="ip_001a dis" data-name="year"></span>	
											<span class="ip_001b dis" data-name="laca_name"></span>
											<span class="ip_001b dis" data-name="maca_name"></span> 
											<span class="ip_001b dis" data-name="saca_name"></span>
											<span class="ip_001a dis" data-name="aca_name"></span>
										</div>
									</div>
								</div>
								<!-- e_wrap_s -->
	
								<!-- s_wrap_s -->
								<div class="wrap_s a1_1st">
	
									<div class="wrap_ss">
										<div class="ss1">
	
											<span class="tt3">학사명</span> 
											<span class="tt4" data-name="academic_name"></span>
										</div>
	
									</div>
								</div>
								<!-- e_wrap_s -->
	<!-- 
								s_wrap_s
								<div class="wrap_s a2st">
	
									<div class="wrap_ss">
										<div class="ss1">
	
											<span class="tt3">조 정보</span> <span class="ip_001c dis">3조</span>
											<span class="tt4">조장</span>
										</div>
	
									</div>
									e_wrap_s
								</div>
								s_wrap_s
	 -->
							</div>
							<!-- e_rwrap -->
	
						</div>
						<!-- s_st_sys_wrap -->
	
						<div class="bt_wrap">
							<button class="bt_1" onclick="post_to_url('${HOME}/admin/user/st/modify',{'user_seq':'${user_seq }'})">수정</button>
							<button class="bt_3" onclick="location.href='${HOME}/admin/user/st/list'">목록</button>
						</div>
	
						<!-- s_.usertb_wrap -->
						<div class="usertb_wrap">
	
							<span class="tts">학사 이력</span>
	
							<table class="mlms_tb userst_mdf">
								<thead>
									<tr>
										<th rowspan="2" class="th01 bd01 w1">No.</th>
										<th colspan="5" class="th01 bd01">학사체계</th>
										<th rowspan="2" class="th01 bd01 w3">
											<div class="th_wrap pd1">학사명</div>
											<button class="down">▼</button>
										</th>
										<th colspan="2" class="th01 bd01">만족도조사</th>
										<th colspan="3" class="th01 bd01">성적표</th>
									</tr>
									<tr>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">년도</div>
											<button class="down">▼</button>
										</th>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">대분류</div>
											<button class="down">▼</button>
										</th>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">중분류</div>
											<button class="down">▼</button>
										</th>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">소분류</div>
											<button class="down">▼</button>
										</th>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">기간</div>
											<button class="down">▼</button>
										</th>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">
												과정<br>만족도
											</div>
											<button class="down">▼</button>
										</th>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">
												수업<br>만족도
											</div>
											<button class="down">▼</button>
										</th>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">
												형성<br>평가
											</div>
											<button class="down">▼</button>
										</th>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">
												수시<br>평가
											</div>
											<button class="down">▼</button>
										</th>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">
												종합<br>성적
											</div>
											<button class="down">▼</button>
										</th>
									</tr>
								</thead>
								<tbody id="acaListAdd">
									
								</tbody>
							</table>
	
						</div>
						<!-- e_.usertb_wrap -->
	
					</div>
					<!-- e_adm_content3 -->
	
				</div>
				<!-- e_main_con -->
	
			</div>
		</div>
		
		<!-- s_패스워드 초기화 팝업 --> 
		<div class="pop_s1 mo_s1" id="pop_s1">
		<div class="wrap">
		<span class="tt">패스워드 초기화</span>
		     <span class="close close_s1" title="닫기" onClick="pwdResetPopupClose();">X</span> 
		     <div class="spop_wrap">
		            <div class="sswrap1">                   
		            </div>
		            <div class="sswrap3">
						<span class="nm" data-name="name">최가나다라</span> 학생의<br>
		                        패스워드를 초기화 하시겠습니까?<br>
		                        ( 초기 패스워드 : 1234 ) 로 변경됨
		            </div>                   
		      </div>
			
			  <div class="btn_wrap">
		          <button class="btn1" onClick="pwdReset();">예</button>
			      <button class="btn2" onClick="pwdResetPopupClose();">아니오</button>
			  </div>
		</div>               
		</div>
		<!-- e_패스워드 초기화 팝업 -->		
</body>
</html>