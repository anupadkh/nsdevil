<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function(){
				$(".btn_f2").hide();
			        $(".btn_f1").click(function(){
			        $(".btn_f1").hide();
					$(".btn_f2").show();
			        $(".rwrap").hide();
			    });
		        $(".btn_f2").click(function(){
			        $(".btn_f2").hide();
					$(".btn_f1").show();
			        $(".rwrap").show();
			    });
		        
				//getCodeList();
			});
			
			function previewPictureUpload() {
				var target = $("#uploadFile");
				target.change(function(){
					var file = $(this).val();
					var previewPicture = $("#previewPicture");
					if (file != "") {
						var attachFileName = file.substring(file.lastIndexOf('\\') + 1);
						var ext = attachFileName.substring(attachFileName.lastIndexOf('.') + 1).toLowerCase();
						var fileMaxSize = 10; //10MB
						var fileSize = this.files[0].size/1024/1024;
						if (/(jpg|png|jpeg)$/i.test(ext) && (fileSize < fileMaxSize)) {
							previewPicture.attr("src", loadPreview(this, "previewPicture"));
						} else {
							alert("10MB이하 이미지 파일(jpg, png, jpeg)만 첨부 가능합니다.");
							previewPicture.attr("src", "${IMG}/profile_default.png");
							if (/msie/.test(navigator.userAgent.toLowerCase())) { // ie
								$("#uploadFile").replaceWith($("#uploadFile").clone(true)); 
							} else { // other browser 
								$("#uploadFile").val(""); 
							}
						}
					} else {
						previewPicture.attr("src", "${IMG}/profile_default.png");
					}
				});
				target.click();
			}
			
			function userSubmit() {
				if(isEmpty($("#user_id").val())){
					alert("학번을 입력해주세요.");
					$("#user_id").focus();
					return
				}
				
				var name = $("#user_name").val();
				if (isBlank(name) || isEmpty(name)) {
					alert("이름을 입력해주세요.");
					$("#user_name").focus();
					return;
				}
				
				var tel1 = $("#tel1").val();
				var tel2 = $("#tel2").val();
				var tel3 = $("#tel3").val();
				if (tel1 != "" || tel2 != "" || tel3 != "") {
					var tel = tel1 + "-" + tel2 + "-" + tel3;
					if (!isTelType(tel)) {
						alert("연락처의 형식이 올바르지 않습니다.");
						$("#tel1").focus();
						return;
					}
					$("#tel").val(tel);
				}
				
				var email = $("#email").val();
				if (isBlank(email) || isEmpty(email)) {
					alert("E-mail을 입력해주세요.");
					$("#email").focus();
					return;
				}
				
				if (!isEmailType(email)) {
					alert("E-mail의 형식이 올바르지 않습니다.");
					$("#email").focus();
					return;
				}
				
				$("#stUserForm").ajaxForm({
                    type: "POST",
                    url: "${HOME}/ajax/admin/user/st/create",
                    dataType: "json",
                    success: function(data, status){
                        if (data.status == "200") {
                            alert("저장이 완료되었습니다.");
                            location.href='${HOME}/admin/user/st/list';
                        }else if(data.status=="301"){
                        	alert("해당 학번은 이미 등록된 학번입니다.");
                        }else if(data.status=="302"){
                        	alert("해당 메일 주소는 이미 등록된 주소입니다.");
                        }else {                        
                            alert(data.status);
                            $.unblockUI();                          
                        }
                        
                    },
                    error: function(xhr, textStatus) {
                        document.write(xhr.responseText); 
                        $.unblockUI();                      
                    },beforeSend:function() {
                        $.blockUI();                        
                    },complete:function() {
                        $.unblockUI();                      
                    }                       
                });     
    			$("#stUserForm").submit();       							
			}
			
			function acaSystemSet(level, seq){
	       		if(level==1){
       				$("#m_seq_add span").remove();
                    $("#m_seq").attr("data-value","");
                    $("#m_seq").text("중분류");
       				$("#s_seq_add span").remove();
                    $("#s_seq").attr("data-value","");
                    $("#s_seq").text("소분류");	
       				$("#aca_seq_add span").remove();
                    $("#aca_seq").attr("data-value","");
                    $("#aca_seq").text("기간");
	       			acasystemStageOfList(2, seq);	
	       		
	       		}else if(level==2){
	       			$("#s_seq_add span").remove();
	       			$("#s_seq").attr("data-value","");
	       			$("#s_seq").text("소분류");
       				$("#aca_seq_add span").remove();
                    $("#aca_seq").attr("data-value","");
                    $("#aca_seq").text("기간");	       			
	       			acasystemStageOfList(3, $("#l_seq").attr("data-value"), seq);
	       				
	       		}else if(level==3){
       				$("#aca_seq_add span").remove();
                    $("#aca_seq").attr("data-value","");
                    $("#aca_seq").text("기간");	       			
       				acasystemStageOfList(4, $("#l_seq").attr("data-value"), $("#m_seq").attr("data-value"), seq);	       			
	       		}
	       	}
	        
            function acasystemStageOfList(level, l_seq, m_seq, s_seq){
            	
            	if(level == ""){
            		alert("분류 없음");
            		return;
            	}
            	$.ajax({
            		type: "POST",
            		url: "${HOME}/ajax/admin/academic/academicSystem/acaStageOfList",
            		data: {
            			"level" : level,
            			"l_seq" : l_seq,                	   
            			"m_seq" : m_seq,
            			"s_seq" : s_seq
            			},
           			dataType: "json",
           			success: function(data, status) {
           				var cate = "";
           				if(level == 1)
           					cate = "대분류";
           				else if(level == 2)
           					cate = "중분류";
           				else if(level == 3)
           					cate = "소분류";
           				else if(level == 4)
           					cate = "기간";
           				
           				var htmls = '<span class="uoption" onClick="acaSystemSet('+level+',\'\');" data-value="">'+cate+'</span>';
           				$.each(data.list, function(index){
           					htmls +=  '<span class="uoption" onClick="acaSystemSet('+level+','+this.aca_system_seq+');" data-value="'+this.aca_system_seq+'">'+this.aca_system_name+'</span>';
           				});

           				if(level == 1){
           					$("#l_seq_add span").remove();
           					$("#l_seq_add").html(htmls);        
           				}	                   
           				else if(level == 2){
           					$("#m_seq_add span").remove();
           					$("#m_seq_add").html(htmls);         
           				}
           				else if(level == 3){
           					$("#s_seq_add span").remove();
           					$("#s_seq_add").html(htmls);
        				}else{
        					$("#aca_seq_add span").remove();
           					$("#aca_seq_add").html(htmls);
        				}
        			},
        			error: function(xhr, textStatus) {
        				//alert("오류가 발생했습니다.");
        				document.write(xhr.responseText);
       				},beforeSend:function() {
  					},
  					complete:function() {
    				}
     			}); 
		   }
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
		    <div class="contents main">
		        <div class="left_mcon aside_l user">
		            <div class="sub_menu user">
		                <div class="title">이용자관리</div>
						<div class="m_schwrap">
							<div class="wrap_s1_uselectbox">
							 	<div class="uselectbox" id="userTypeSelectBox" value="PROFESSOR">
									<span class="uselected">교수</span>
									<span class="uarrow">▼</span>
									<div class="uoptions">
										<span class="uoption" value="PROFESSOR">교수</span>
										<span class="uoption" value="STUDENT">학생</span>
										<span class="uoption" value="STAFF">직원</span>
									</div>
								</div>
							</div>
							<input type="text" class="ip_nm" id="searchUserName" value="" placeholder="이름">
							<button class="btn_sch" onclick="searchUser();"></button>
						</div>
						
						
						<div class="boardwrap">
							<div class="title1">
								<span class="sp_tt two">이용자 등록정보 관리<br><span class="stt">( 교수 / 직원 )</span></span>
							</div>
							<ul class="m1 panel">
								<li class="wrap"><a href="${HOME}/admin/user/code/attend/list" class="tt_2">재직상태 목록관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/department/list" class="tt_2">소속 목록관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/position/list" class="tt_2">교수 직위 목록관리</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/specialty/list" class="tt_2">교수 세부전공 목록관리</a></li>
							</ul>
							<div class="title1"><span class="sp_tt">교수 관리</span></div>
							<ul class="panel" style="display: table !important;">
		                        <li class="wrap"><a href="${HOME}/admin/user/pf/list" class="tt_2">등록 / 검색</a></li>
		                    </ul>
							<div class="title1 wrapx"><span class="sp_tt">학생 관리</span></div>
							<ul class="panel on">
								<li class="wrap"><a href="${HOME }/admin/user/st/list" class="tt_2 on">등록 / 검색</a></li>
<!-- 								<li class="wrap"><a href="../adm/adm_user_system.html" class="tt_2">학생 학사체계별 조회</a></li> -->
<!-- 								<li class="wrap"><a href="#" class="tt_2">성취도 관리</a></li> -->
							</ul>
							<div class="title1 wrapx"><span class="sp_tt" onclick="location.href='${HOME}/admin/user/staff/list'">직원 관리</span></div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
		            </div>
		        </div>
		
				<!-- s_main_con -->
				<div class="main_con user">
					<!-- s_메뉴 접기 버튼 -->
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a> 
					<!-- e_메뉴 접기 버튼 -->
	
					<!-- s_tt_wrap -->
					<div class="tt_wrap">
						<h3 class="am_tt">
							<span class="tt">이용자관리<span class="sign1">&gt;</span>학생 관리<span class="sign1">&gt;</span>학생 등록</span>
						</h3>
					</div>
					<!-- e_tt_wrap -->
	
					<!-- s_adm_content3 -->
					<div class="adm_content3 user">
						<!-- s_tt_wrap -->
	
		            	<form id="stUserForm" name="stUserForm" onsubmit="return false;" enctype="multipart/form-data">
	            			<input type="file" id="uploadFile" name="uploadFile" value="" class="blind-position">
		            		<input type="hidden" id="tel" name="tel" value="">
		            		<input type="hidden" id="user_seq" name="user_seq" value="">
							<!-- s_.table_my -->
							<table class="table_my">
								<tbody>
									<tr>
										<td rowspan="5" class="td01 w01 bd04">
											<!-- s_ph_wrap -->
											<div class="ph_wrap">
												<div class="photo1">
													<img src="${IMG}/ph150.png" alt="프로필 사진" id="previewPicture">
												</div>
											</div> <!-- s_ph_wrap -->
		
											<button type="button" class="btn_reset" onclick="previewPictureUpload();">사진등록(선택)</button>
		
										</td>
										<td class="td01 w02">학번<span class="tts1">*(필수)</span></td>
										<td class="td01 w03"><input type="text" id="user_id" name="user_id" class="ip_001" value=""></td>
									</tr>
		
									<tr>
										<td class="td01 w02">이름<span class="tts1">*(필수)</span></td>
										<td class="td01 w03"><input type="text" name="user_name" id="user_name" class="ip_001" value=""></td>
									</tr>
		
									<tr>
										<td class="td01 w02">연락처<span class="tts2">(선택)</span></td>
										<td class="td01 td_wrap1 w03">
											<input type="text" class="ip_tel1" maxlength="4" id="tel1" name="tel1" value="">
											<span class="sign1">-</span> 
											<input type="text" class="ip_tel1" maxlength="4" id="tel2" name="tel2" value=""> 
											<span class="sign1">-</span>
											<input type="text" class="ip_tel1" maxlength="4" id="tel3" name="tel3" value="">	
										</td>
									</tr>
		
									<tr>
										<td class="td01 w02">E-mail<span class="tts1">*(필수)</span></td>
										<td class="td01 w03"><input type="text" id="email" name="email" class="ip_001"></td>
									</tr>
		
									<tr>
										<td class="td01 w02">패스워드<span class="tts3">(자동)</span></td>
										<td class="td01 w03">등록 시 자동으로 0000로 지정됩니다.</td>
									</tr>
								</tbody>
							</table>
							<!-- e_.table_my -->
						</form>
		            	<!-- 
						<div class="tt_sys">
							학사체계 설정 <span class="tts">(선택)</span>
						</div>
						s_st_sys_wrap
						<div class="st_sys_wrap">
							s_rwrap
							<div class="rwrap">	
								s_wrap_s
								<div class="wrap_s a1st">	
									<div class="wrap_ss">
										<div class="ss1">
											<div class="wrap_s1_uselectbox">
												<div class="uselectbox">
													<span class="uselected">2018</span> <span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;">
														<span class="uoption firstseleted">2018</span><span
															class="uoption">2017</span><span class="uoption">2016</span><span
															class="uoption">2015</span><span class="uoption">2014</span><span
															class="uoption">2013</span><span class="uoption">2012</span><span
															class="uoption">2011</span><span class="uoption">2010</span><span
															class="uoption">2009</span>
													</div>
												</div>
											</div>
	
											<div class="wrap_s2_uselectbox">
												<div class="uselectbox">
													<span class="uselected">대분류</span> <span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;">
														<span class="uoption firstseleted">대분류</span><span
															class="uoption">전체</span><span class="uoption">의과대학</span><span
															class="uoption">치과대학</span>
													</div>
												</div>
											</div>
	
											<div class="wrap_s2_uselectbox">
												<div class="uselectbox">
													<span class="uselected">중분류</span> <span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;">
														<span class="uoption firstseleted">중분류</span><span
															class="uoption">전체</span><span class="uoption">예과</span><span
															class="uoption">본과</span>
													</div>
												</div>
											</div>
	
											<div class="wrap_s2_uselectbox">
												<div class="uselectbox">
													<span class="uselected">소분류</span> <span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;">
														<span class="uoption firstseleted">소분류</span><span
															class="uoption">전체</span><span class="uoption">1학년</span><span
															class="uoption">2학년</span><span class="uoption">3학년</span><span
															class="uoption">4학년</span>
													</div>
												</div>
											</div>
	
											<div class="wrap_s1_uselectbox">
												<div class="uselectbox">
													<span class="uselected">기간</span> <span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;">
														<span class="uoption firstseleted">기간</span><span
															class="uoption">전체</span><span class="uoption">1학기</span><span
															class="uoption">2학기</span><span class="uoption">기간3</span><span
															class="uoption">기간4</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								e_wrap_s
	
								s_wrap_s
								<div class="wrap_s a2st">
	
									<div class="wrap_ss">
										<div class="ss1">
	
											<span class="tt3">조 선택</span>
											<div class="wrap_s2_uselectbox">
												<div class="uselectbox">
													<span class="uselected">전체</span> <span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;">
														<span class="uoption">1조</span>
														<span class="uoption">2조</span>
														<span class="uoption">3조</span>
														<span class="uoption">4조</span>
														<span class="uoption">5조</span>
														<span class="uoption">6조</span>
														<span class="uoption">7조</span>
														<span class="uoption">8조</span>
														<span class="uoption">9조</span>
														<span class="uoption">10조</span>
														<span class="uoption">11조</span>
														<span class="uoption">12조</span>
														<span class="uoption">13조</span>
														<span class="uoption">14조</span>
														<span class="uoption">15조</span>
													</div>
												</div>
											</div>
	
											<span class="tt3">조장여부</span> <input class="ip_chk_1 open_s2" type="checkbox"> <span class="tt4">조장</span>
										</div>
	
									</div>
									e_wrap_s
								</div>
								s_wrap_s
	
							</div>
							e_rwrap
	
						</div>
						s_st_sys_wrap
	 -->
						<div class="bt_wrap">
							<button class="bt_2" onclick="userSubmit();">등록</button>
							<button class="bt_3" onClick="location.href='${HOME}/admin/user/st/list'">취소</button>
						</div>
	
					</div>
					<!-- e_adm_content3 -->
	
				</div>
				<!-- e_main_con -->
	
			</div>
		</div>
		
		<!-- s_조장 여부 설정 안내 팝업 -->
		<div class="pop_s2 mo_s2" id="pop_s2">
			<div class="wrap">
				<span class="tt">조장 여부 설정 안내</span> <span class="close close_s2"
					title="닫기">X</span>
				<div class="spop_wrap">
					<div class="sswrap1"></div>
					<div class="sswrap3">이미 설정된 조장이 있습니다.<br> [<span class="nm">20151236</span>|<span class="nm">최가나다라</span>(으)로 조장 설정됨 ]<br>
					<br> 1조 – 조장을<br> <span class="nm">최가나다라</span>에서 <span class="nm">김가나다라</span>(으)로<br> 변경하시겠습니까? </div>
				</div>
	
				<div class="btn_wrap">
					<button class="btn1">예</button>
					<button class="btn2">아니오</button>
				</div>
			</div>
		</div>
		<!-- e_조장 여부 설정 안내 팝업 -->
</body>
</html>