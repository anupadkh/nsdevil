<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function() {
				boardHearderInit();
				noticeListView(1);
			});
			
			function noticeListView(page) {
				if (typeof page == "undefined") {
					page = 1;
				}
				$("input[name='page']").val(page);
				
				var searchText = $("#boardSearchText").val();
				if (!isEmpty(searchText) && !isBlank(searchText)){
					$(".sch_ts").show();
					$("#search_text").val(searchText);
				} else {
					$(".sch_ts").hide();
					$("#search_text").val('');
				}
				
			    $.ajax({
			        type: "GET",
			        url: "${M_HOME}/ajax/common/SLife/notice/list",
			        data: $("#searchFrm").serialize(),
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var list = data.list;
			        		var totalCnt = data.totalCnt;
				        	var listHtml = '';
				        	$(list).each(function(){
				        		var boardSeq = this.board_seq;
				        		var title = this.title;
				        		var regDate = this.reg_date;
				        		var cateCode = this.board_cate_code;
				        		var notifyTextHtml = "";
				        		if (cateCode == "97") {
				        			notifyTextHtml = '<span class="tts">기타</span>';
				        		} else if(cateCode == "98") {
				        			notifyTextHtml = '<span class="tts tts_1">시스템 점검</span>';
				        		} else if(cateCode == "99") {
				        			notifyTextHtml = '<span class="tts tts_1">긴급 공지</span>';
				        		}
				        						        		
				        		listHtml += '<tr onclick="noticeDetail('+boardSeq+')">';
				        		listHtml += '  <td>';
				        		listHtml += '	  <div class="box_s w3">';
				        		listHtml += notifyTextHtml;
				        		listHtml += '		  <span class="tt">' + title + '</span>';
				        		listHtml += '	  </div>';
				        		listHtml += '	  <div class="w3"><div class="box_s w1"><span class="ssp_num">' + regDate + '</span></div></div>';
				        		listHtml += '  </td>';
				        		listHtml += '</tr>';
				        	});
			        		$("#boardTotalCnt").html(totalCnt);
				        	if (list.length > 0) {
				        		$("#boardList").html(listHtml);
					        	$("#footerNationArea").attr("class", "");
					        	$("#footerNationArea").addClass("pagination");
					        	$("#footerNationArea").html(data.pageNav);
				        	} else {
				        		$("#boardList").html('<td><div class="box_s w3"><span class="tt">등록된 게시글이 없습니다.</span></div><div class="w3"></div></td>');
				        	}
			        	} else {
			        		//TODO
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function noticeDetail(boardSeq) {
				if (typeof boardSeq !=  "undefined" && boardSeq != "") {
					location.href = '${M_HOME}/common/SLife/notice/detail?seq='+boardSeq
				}
			}
		</script>
	</head>
	<body class="color1">
		<form id="searchFrm" name="searchFrm" method="get" onsubmit="return false;">
			<input type="hidden" id="page" name="page" value="">
			<input type="hidden" id="search_text" name="search_text" value="">
		</form>
		<div class="contents">
			<div class="mtb_wrap">
				<div class="rfrsch">
				    <input class="ip_search" id="boardSearchText" placeholder="제목 + 내용 검색" type="text">
				    <button class="btn_search1" onclick="noticeListView(1, true);">검색</button>
				</div>
						
				<div class="sch_wrap3">
					<div class="wrap">
					    <span class="swrap">
							<span class="sch_ts">검색결과</span><span>총</span><span class="num" id="boardTotalCnt">0</span><span>건</span>
					    </span>
						<div class="renewwrap"><button class="renew" onclick="noticeListView(1);"></button></div>
<!-- 						<button class="regi" onClick="location.href='cc_rfr_r1.html'">등록</button> -->
					</div>
				</div>
				<div class="rfrtb_wrap">	
					<table class="rfrtb1" id="boardList">
					</table>
				</div>
			</div>	
		</div>
	</body>
</html>
