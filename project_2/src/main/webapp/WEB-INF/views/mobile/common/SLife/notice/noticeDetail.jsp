<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function() {
				boardHearderInit();
				$("body").addClass("slife").addClass("v1");
				$("div.sch_f1").remove();
				getBoardDetail();
			});
			
			function getBoardDetail() {
				$.ajax({
			        type: "GET",
			        url: "${M_HOME}/ajax/common/SLife/notice/detail",
			        data: {
			        	"board_seq":"${seq}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var noticeInfo = data.notice_info;
			        		var attachList = data.attach_list;
			        		var title = noticeInfo.title;
			        		if (title.length > 10) {
			        			title = title.substring(0,10)+"...";
			        		}
			        		$("div.top_title>span").html(title);
			        		$("#title").html(noticeInfo.title);
			        		$("#regDate").html(noticeInfo.reg_date);
			        		$("#content").html(noticeInfo.content);
			        		$("#hits").html(noticeInfo.hits);
			        		$(".sch_ts").show();
			        		
			        		if (attachList.length > 0) {
			        			var attachHtml = '';
				        		$(attachList).each(function() {
			        				attachHtml += '<div class="wrap_s dw">';
				        			if(this.attach_type == "I") {
			    	        			attachHtml += '	<div class="preview1"><img src="${RES_PATH}'+this.file_path+'" alt="미리보기 이미지"></div>';
			            				attachHtml += '	<div class="sp_wrap"><span class="sp_1">' + this.file_name + '</span><span class="sp_2">사진</span></div>';
				        			} else {
						        		attachHtml += '	<div class="sp_wrap"><span class="sp_1">' + this.file_name + '</span></div>';
				        			}
					        		attachHtml += '	<button class="dw" onclick="fileDownload(\'${HOME}\', \'${RES_PATH}'+this.file_path+'\',  \''+this.file_name+'\')"></button>';
					        		attachHtml += '</div>';
				        		});
				        		$("#attachListArea").html(attachHtml);
				        		$("#attachCnt").html(attachList.length);
			        		} else {
			        			$("#attachListArea").html("");
			        			$("#attachCnt").parent().remove();
			        		}
			        	} else {
			        		alert("공지지사항 불러오기에 실패하였습니다. [" + data.status + "]");
			        		location.href="${M_HOME}/common/SLife/notice/list";
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
		</script>
	</head>
	<body class="color1">
		<div class="contents">
			<div class="mtb_wrap rfr">
				<span class="tth" id="title"></span>
				<div class="sch_wrap4">
					<span class="ssp_num" id="regDate"></span>
					<div class="wrap">
						<span class="swrap"><span class="sch_ts">조회</span><span class="num" id="hits">0</span><span>건</span></span>
					</div>
				</div>
				<div class="rfrform_wrap">	
					<div class="con" id="content"></div>
					<div class="sch_wrap5">
						<div class="wrap">
							<span class="swrap"><span>총</span><span class="num" id="attachCnt">0</span><span class="numt">건</span><span>의 자료 업로드</span></span>
						</div>
					</div>
					<div class="box_s" id="attachListArea"></div>
				</div>
			</div>
		</div>
	</body>
</html>