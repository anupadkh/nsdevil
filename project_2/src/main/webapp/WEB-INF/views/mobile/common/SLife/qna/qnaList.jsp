<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script src="${JS}/lib/jquery.ui.accordion.js"></script>
		<script type="text/javascript">
		
			var questionCnt = 0;
			var answerCnt = 0;
			
			$(document).ready(function() {
				boardHearderInit();
				$("body").addClass("qna");
				boardListView(1);
			});
			
			function accordionInit() {
				$("#qnaList").accordion({collapsible: true, active: false}).accordion("refresh");
			}
			
			
			function boardListView(page) {
				if (typeof page == "undefined") {
					page = 1;
				}
				$("input[name='page']").val(page);
				var searchText = $("#boardSearchText").val();
				if (!isEmpty(searchText) && !isBlank(searchText)){
					$(".sch_ts").show();
					$("#search_text").val(searchText);
				} else {
					$(".sch_ts").hide();
					$("#search_text").val('');
				}
				
			    $.ajax({
			        type: "GET",
			        url: "${M_HOME}/ajax/common/SLife/qna/list",
			        data: $("#searchFrm").serialize(),
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		questionCnt = 0;
			        		answerCnt = 0;
			        		var list = data.list;
			        		var totalCnt = data.totalCnt;
				        	var listHtml = '';
				        	$(list).each(function(){
				        		listHtml += qnaListCreate(this);
				        	});
				        	if (list.length > 0) {
				        		$("#questionCnt").html(questionCnt);
				        		$("#answerCnt").html(answerCnt);
				        		$("#qnaList").html(listHtml);
					        	accordionInit();
				        	} else {
				        		$("#qnaList").addClass("regix_custom");
				        	}
			        	} else {
			        		$("#qnaList").addClass("regix_custom");
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function qnaListCreate(t) {
				var qPicture = "${DEFAULT_PICTURE_IMG}";
				var answerPicture = "${DEFAULT_PICTURE_IMG}";
				var qnaContent = "";
				if (t.qPicture != "" && typeof t.qPicture != "undefined") {
					qPicture = t.qPicture;
				}
				if (t.answerPicture != "" && typeof t.answerPicture != "undefined") {
					answerPicture = t.answerPicture;
				}
				if (t.answer_state == 'Y') { //답변완료
					answerCnt++;
					qnaContent += '<div class="acc"><span class="sp01">' + t.q_content + '</span>';
					qnaContent += '	<div class="qna_swrap">';
					qnaContent += '        <span class="a_x a_o">답변완료</span>';
					qnaContent += '	    <span class="a_mp">';
					qnaContent +='			<span class="ssp1">' + t.q_name + '(' + t.q_department_name + ')</span>';
					qnaContent += '			<span class="pt01"><img src="../img/ph_r1.png" alt="사진" class="pt_img"></span>';
					qnaContent += '		</span>	';
					qnaContent += '    </div>';
					qnaContent += '</div>';
					qnaContent += '<div class="panel acc1">	';
					qnaContent += '    	<input type="hidden" name="qnaSeq" value="' + t.qna_seq + '">';
					qnaContent += '	    <div class="d_wrap1 tarea free_textarea">';
					if ("${S_USER_LEVEL}" < 4) { //학생이상(교수,책임교수,행정,관리자)
						qnaContent += '	        <textarea class="tarea01" style="height:75px;" name="answerContent">' + t.answer_content + '</textarea>';
					} else {
						qnaContent += '	        <textarea class="tarea01" style="height:75px;" disabled name="answerContent">' + t.answer_content + '</textarea>';
					}
					qnaContent += '	    </div>';
					if ("${S_USER_LEVEL}" < 4) { //학생이상(교수,책임교수,행정,관리자)
						qnaContent += '	    <button class="btn_qnar2" title="답변 삭제하기" onclick="answerRemove(this);">삭제</button>';
						qnaContent += '	    <button class="btn_qnar3" title="답변 수정하기" onclick="answerModify(this);">수정</button>';
					}
					qnaContent += '</div>';
				} else { //대기 중
					questionCnt++;
					if ("${sessionScope.S_USER_PICTURE_PATH}" != "") {
						answerPicture = "${sessionScope.S_USER_PICTURE_PATH}";
					}
				
					qnaContent += '<div class="acc"><span class="sp01">' + t.q_content + '</span>';
					qnaContent += '	<div class="qna_swrap">';
					qnaContent += '        <span class="a_x">대기</span>';
					qnaContent += '	    <span class="a_mp">';
					qnaContent += '			<span class="ssp1">' + t.q_name + '(' + t.q_department_name + ')</span>';
					qnaContent += '			<span class="pt01"><img src="../img/ph_r1.png" alt="사진" class="pt_img"></span>';
					qnaContent += '		</span>	';
					qnaContent += '    </div>';
					qnaContent += '</div>';
					qnaContent += '<div class="panel acc1">';
					qnaContent += '    	<input type="hidden" name="qnaSeq" value="' + t.qna_seq + '">';
					qnaContent += '		<div class="d_wrap1 tarea free_textarea">';
					if (t.q_answer_user_seq == "${sessionScope.S_USER_SEQ}" || "${sessionScope.S_USER_LEVEL}" < 3) {//답변자거나 행정직원
						qnaContent += '	        <textarea class="tarea01" style="height:75px;" name="answerContent" placeholder="답변을 등록하세요. (최대 500자)"></textarea>';
					} else {
						qnaContent += '	        <textarea class="tarea01" style="height:75px;" disabled name="answerContent" placeholder="답변을 등록하세요. (최대 500자)"></textarea>';
					}
					qnaContent += '	    </div>';
					if (t.q_answer_user_seq == "${sessionScope.S_USER_SEQ}" || "${sessionScope.S_USER_LEVEL}" < 3) {//답변자거나 행정직원
						qnaContent += '	    <button class="btn_qnar1" title="답변 등록하기" onclick="answerSubmit(this);">등록</button>';
					}
					qnaContent += '</div>';
				}
				return qnaContent;
			}
			
			
			function answerSubmit(t) {
				if(confirm("답변을 등록하시겠습니까?")) {
					var ulPanel = $(t).parent();
					var answerContent = $(ulPanel).find("textarea[name='answerContent']").val();
					if (isEmpty(answerContent) || isBlank(answerContent)) {
						alert("답변 내용을 입력해주세요.");
						$(answerContent).focus();
						return false;
					}
					
					$.ajax({
				        type: "POST",
				        url: "${HOME}/ajax/common/SLife/qna/answer/create",
				        data: {
				        	"qna_seq" : $(ulPanel).find("input[name='qnaSeq']").val(),
				        	"answer_content" : answerContent
				        },
				        dataType: "json",
				        success: function(data, status) {
				            if (data.status == "200") {
				            	alert("답변이 등록되었습니다.");
				            } else {
				            	alert("답변이 등록이 실패하였습니다.");
				            }
				            location.reload();
				        },
				        error: function(xhr, textStatus) {
				            document.write(xhr.responseText);
				        },
				        beforeSend:function() {
				            $.blockUI();
				        },
				        complete:function() {
				            $.unblockUI();
				        }
				    });
				}
			}
			
			function answerModify(t) {
				if(confirm("답변의 내용을 수정하시겠습니까?")) {
					var ulPanel = $(t).parent();
					var answerContent = $(ulPanel).find("textarea[name='answerContent']").val();
					if (isEmpty(answerContent) || isBlank(answerContent)) {
						alert("답변 내용을 입력해주세요.");
						$(answerContent).focus();
						return false;
					}
					
					$.ajax({
				        type: "POST",
				        url: "${HOME}/ajax/common/SLife/qna/answer/modify",
				        data: {
				        	"qna_seq" : $(ulPanel).find("input[name='qnaSeq']").val(),
				        	"answer_content" : answerContent
				        },
				        dataType: "json",
				        success: function(data, status) {
				        	if (data.status == "200") {
				            	alert("답변의 내용이 수정되었습니다.");
				            } else {
				            	alert("답변의 내용 수정이 실패하였습니다.");
				            }
				            location.reload();
				        },
				        error: function(xhr, textStatus) {
				            document.write(xhr.responseText);
				        },
				        beforeSend:function() {
				            $.blockUI();
				        },
				        complete:function() {
				            $.unblockUI();
				        }
				    });
				}
			}
			
			function answerRemove(t) {
				if(confirm("답변을 삭제하시겠습니까?")) {
					var ulPanel = $(t).parent().parent();
					$.ajax({
				        type: "POST",
				        url: "${HOME}/ajax/common/SLife/qna/answer/remove",
				        data: {
				        	"qna_seq" : $(ulPanel).find("input[name='qnaSeq']").val()
				        },
				        dataType: "json",
				        success: function(data, status) {
				        	if (data.status == "200") {
				            	alert("답변이 삭제되었습니다.");
				            } else {
				            	alert("답변 삭제에 실패하였습니다.");
				            }
				            location.reload();
				        },
				        error: function(xhr, textStatus) {
				            document.write(xhr.responseText);
				        },
				        beforeSend:function() {
				            $.blockUI();
				        },
				        complete:function() {
				            $.unblockUI();
				        }
				    });
				}
			}
		</script>
	</head>
	<body class="color1">
		<form id="searchFrm" name="searchFrm" method="get" onsubmit="return false;">
			<input type="hidden" id="page" name="page" value="">
			<input type="hidden" id="search_text" name="search_text" value="">
		</form>
		<div class="contents">
			<div class="mtb_wrap">
				<div class="rfrsch">
				    <input class="ip_search" id="boardSearchText" placeholder="제목 + 내용 검색" type="text">
				    <button class="btn_search1" onclick="boardListView(1);">검색</button>
				</div>
				<div class="qna_wrap1">
					<div class="wrap">
						<div class="swrap">
						    <span class="sswrap s1">
							    <span>대기</span><span class="num" id="questionCnt">0</span><span>건</span>
					        </span>
						    <span class="sswrap s2">
							    <span>답변완료</span><span class="num" id="answerCnt">0</span><span>건</span>
					        </span>
						</div>
					</div>
				</div>
				<div class="mtb_wrap accwrap " id="qnaList">
				</div>
			</div>	
		</div>
	</body>
</html>
