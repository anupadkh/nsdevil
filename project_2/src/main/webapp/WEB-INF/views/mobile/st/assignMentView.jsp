<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function() {
				getAssignMentList();

				getLessonPlanTitleInfo();//상단 타이틀 입력
				$(document).on("click", ".twrap_a", function(){
					$(this).hide();
					$(this).siblings("div.twrap_b").show();
				});
				$(document).on("click", ".twrap_b", function(){
					$(this).hide();
					$(this).siblings("div.twrap_a").show();
				});
			});
			
			function getAssignMentList(){
				
		        $.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/st/assignMent/list",
		            data: {      
		            	"asgmt_type" : "2",
		            	"s_lp_seq" : "${S_LP_SEQ}",
		            	"curr_seq" : "${S_CURRICULUM_SEQ}"
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		var htmls = "";
		            		if(data.assignMentList.length == 0){
		            			$("#assignMentListAdd").addClass("regix");
		            			
		            		}else{
		            			$("#assignMentListAdd").removeClass("regix");
			            		$.each(data.assignMentList, function(index){
			            			
				            		htmls='<tr class="box_wrap">';
				            	    htmls+='<td class="box_s">';
				            	    htmls+='<div class="wrap1">';
				            	    htmls+='<div class="box1">';
				            				
				            	    htmls+='<div class="wrap_ss">';
				            	    htmls+='<div class="num">'+(index+1)+'</div>';
				            	    htmls+='<div class="prg_t dt">'+this.start_date+' ~ '+this.end_date+'</div>';
				            	    htmls+='</div>';
				            	    
				            	    if(!isEmpty(this.asgmt_name)){
					            	    htmls+='<div class="twrap_a">'+this.asgmt_name+'</div>';
					            	    htmls+='<div class="twrap_b" style="display:none;">'+this.full_asgmt_name+'</div>';
					            	}else{
					            		htmls+='<div class="twrap_a">'+this.full_asgmt_name+'</div>';
					            	    htmls+='<div class="twrap_b" style="display:none;">'+this.full_asgmt_name+'</div>';
					            	}
				            	    
				            		if(!isEmpty(this.file_name)){
				            			var file_name = this.file_name.split("||");
				            			var file_path = this.file_path.split("||");
				            			
				            			for(var i=0; i<file_name.length; i++){
					            			htmls+='<div class="list_wrap">';
						            	    htmls+='<ul class="dw_list">';
						            	    htmls+='<li class="li_1">';
						            	    htmls+='<div class="tt">'+file_name[i]+'</div><div class="bt_wrap">';
						            	    htmls+='<button class="dw" title="다운로드" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+file_path[i]+'\',\''+file_name[i]+'\');">다운로드</button></div>';                   
						            	    htmls+='</li>';		            	               
				           	                htmls+='</ul>';
				           	                htmls+='</div>';		            
				            			}
				            		}
		           	                htmls+='<div class="wrap2">';
		           	                htmls+='<div class="btn1">';
		           					if(this.submit_chk == "0")	
		           	                	htmls+='<span class="sp_1">미제출</span>';
	           	                	else
	           	                		htmls+='<span class="sp_2">제출 완료</span>';
		           					htmls+='</div>';
		           					if(this.submit_chk == "0")
		           	                	htmls+='<span class="sp_1s">과제가 제출되지 않았습니다.<br>( 과제는 PC웹으로만 제출가능 )</span>';
		           	                htmls+='</div>';		            	                		            				
		           	                htmls+='</div>';
		           	                htmls+='</div>';	
		           	                htmls+='</td>';
		           	                htmls+='</tr>';	
		           	                $("#assignMentListAdd").append(htmls);
	
			            		});
		            		}
		            	}else{
		            		alert("과제리스트 가져오기 실패");
		            	}
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        }); 
		    }
			
			function getLessonPlanTitleInfo(){
				
				$.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/st/lessonPlanTitleInfo",
		            data: {                  
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status == "200"){
			            	var lesson_date = data.info.lesson_date.split("-");
			            	//교시
							var periodStr = data.info.period; //교시
							var periodList = periodStr.split(',');
							var startPeriod = periodList[0];
							if (periodList.length > 1) {
								periodStr = startPeriod + "~" + periodList[periodList.length-1] + "교시";  
							} else {
								periodStr = startPeriod + "교시";
							}
							
			            	//수업일
			            	var lessonDate = lesson_date[0]+"월 "+lesson_date[1]+"일";
							
			            	//수업시간
							var ampmText = data.info.ampm;
			        		if(ampmText == "pm"){
			        			ampmText = "오후";
			        		} else {
			        			ampmText = "오전";
			        		}
			        		
			        		var startTime = data.info.start_time_12;
							var endTime = data.info.end_time_12;
			            	
			        		var lessonPlanDateTitle = lessonDate + " " + periodStr;//mm월 xx일 x교시
			        		var lessonPlanTimeTitle = ampmText + " " + data.info.start_time + "~" + data.info.end_time;//오전/오후 xx:xx~xx:xx
			        		
			            	var titleHtml = '<span class="cc_s1">' + lessonPlanDateTitle + '</span><span class="cc_s2">(' + lessonPlanTimeTitle + ')</span>';
			            	var titleDiv = $("div[name='titleDiv']").addClass("cc").html(titleHtml);
			            	
			            	var lessonInfoHtml = '<span class="cc_t1">'+data.info.lesson_subject+'</span>'
								+ '<span class="cc_t2"><span class="ts1">교수</span>';
	
							if(isEmpty(data.info.name)){
								lessonInfoHtml+='<span class="ts2">미배정</span>';
							}else{
								lessonInfoHtml+='<span class="ts2">'+data.info.name+'</span>'
								+ '<span class="sign">(</span><span class="ts2">'+data.info.code_name+'</span><span class="sign">)</span></span>';
							}
							
							if(data.info.attendance_state == "WAIT")
								lessonInfoHtml +='<span class="cc_t3">출석체크 전</span>';
							else if(data.info.attendance_state == "00")
								lessonInfoHtml +='<span class="absent">결석</span>';
							else if(data.info.attendance_state == "02")
								lessonInfoHtml +='<span class="late">지각</span>';
							else if(data.info.attendance_state == "03")
								lessonInfoHtml +='<span class="attend">출석</span>';
							else
								lessonInfoHtml +='<span class="cc_t3">출석 미입력</span>';
									
							$("#lessonInfo").html(lessonInfoHtml);
		            	}
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        }); 
			}
			
			function getFormationEvaluationListView() {
				post_to_url("${M_HOME}/st/formationEvaluation/list", {"lp_seq" : "${sessionScope.S_LP_SEQ}", "curr_seq":"${sessionScope.S_CURRICULUM_SEQ}"});
			}
		
			function getAttendanceListView() {
				post_to_url("${M_HOME}/st/attendance/list", {"lp_seq" : "${sessionScope.S_LP_SEQ}", "curr_seq":"${sessionScope.S_CURRICULUM_SEQ}"});
			}
		
			function getLessonPlanView() {
				post_to_url("${M_HOME}/st/lessonPlan", {"lp_seq" : "${sessionScope.S_LP_SEQ}", "curr_seq":"${sessionScope.S_CURRICULUM_SEQ}"});
			}
			
			function getAssignMentListView(){
				post_to_url("${M_HOME}/st/assignMent", {"lp_seq":"${sessionScope.S_LP_SEQ}"});
			}
			
			function bindPopup(popupName, groupFlag, asgmt_seq) {
				var popup = $(popupName);
				getNotSubmitStudentList(groupFlag, asgmt_seq);
				$("input[name=group_flag]").val(groupFlag);
				$("input[name=asgmt_seq]").val(asgmt_seq);
				popup.show();
								
				$(document).click(function(e) {
				    if (popup.is(e.target)) {
				    	popup.hide();
				    }
				});
			}			
			
			function getNotSubmitStudentList(groupFlag, asgmt_seq, reload_flag){
				 $.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/pf/assignMent/notSubmitSTList",
		            data: {   
		            	"groupFlag" : groupFlag,
		            	"asgmt_seq" : asgmt_seq
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){			            		
		            		var htmls = "";
		            		$.each(data.stList, function(){
			            		htmls+='<div class="a_mp">';
			            		if(groupFlag=="N"){
			            			htmls+='<span class="pt01"><img src="'+this.file_path+'" alt="사진" class="pt_img"></span><span class="ssp1">'+this.name+'</span></div>';
			            			$("span[name=groupName]").text("명");
			            		}
		            			else{
		            				htmls+='<span class="ssp1">&nbsp;&nbsp;&nbsp;'+this.group_number+'조 조장 '+this.name+'</span></div>';
		            				$("span[name=groupName]").text("개조");
		            			}
		            		});
		            		$("#STListAdd").html(htmls);
		            		$("span[name=submitSTCnt]").text(data.stList.length);
		            		
		            		if(reload_flag == "Y")
		            			alert("미제출자 리스트를 다시 가져왔습니다.");
		            		
		            	}else{
		            		alert("과제 미제출 리스트 가져오기 실패");
		            	}
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        }); 
			}
			
			function refresh(){
				getNotSubmitStudentList($("input[name=group_flag]").val(), $("input[name=asgmt_seq]").val(), "Y");				
			}
			
			function getCurriculumView(){
				post_to_url("${M_HOME}/st/curriculum", {"curr_seq" : "${sessionScope.S_CURRICULUM_SEQ}"});
			}
		</script>
	</head>
	<body class="color1">
		<input type="hidden" name="group_flag">
		<input type="hidden" name="asgmt_seq">
		<!-- s_contents -->
		<div class="contents">
	
			<!-- s_top_tt-->
			<div class="top_tt" id="lessonInfo">
			</div>
			<!-- e_top_tt -->
	
			<!-- s_mn_wrap -->
			<div class="mn_wrap hwork">
				<div class="wrap_s">
					<span class="cc_mn cc_mn01" onClick="javascript:getCurriculumView();">교육과정</span>
					<span class="cc_mn cc_mn02" onClick="javascript:getLessonPlanView();">수업계획</span>
					<span class="cc_mn cc_mn03" onClick="location.href='${M_HOME}/st/lessonData'">자료</span>
					<span class="cc_mn cc_mn04" onClick="javascript:getFormationEvaluationListView();">형성평가</span>
					<span class="cc_mn cc_mn05 on" onClick="location.href='${M_HOME}/st/assignMent';">과제</span>
				</div>
			</div>
			<!-- e_mn_wrap -->
	
			<!-- s_ptb_wrap -->
			<div class="ptb_wrap">
				<!-- s_tb_hwork -->
				<table class="tb_hwork" id="assignMentListAdd">
					
				</table>
				<!-- e_tb_hwork -->
			</div>
			<!-- e_ptb_wrap -->
	
		</div>
		<!-- e_contents -->
		
		
		<!-- s_미제출자 -->	
		<div id="m_pop1" class="pop_up_sbmx mo1">  
		          <div class="pop_wrap">  
		
					  <p class="t_title"><span class="sp_un">미제출</span><span class="sp_nn" name="submitSTCnt">20</span><span class="sp_un" name="groupName">명</span></p>      
		 		     
			          <button class="renew_wrap" onClick="refresh();"><span class="renew"></span></button>
		 <!-- s_table_b_wrap -->
		<div class="table_b_wrap">	
		
		<!-- s_pht -->
		<div class="pht">                      
		          <div class="con_wrap"> 
		                <div class="con_s2" id="STListAdd"> 
		              
		                </div>
		         </div>
		         </div>
		<!-- e_pop_table -->	             
		                     
		</div>               
		<!-- e_pht --> 
		
		               <div class="t_dd">
		                   
		                    <div class="pop_btn_wrap2">
		                     <button type="button" class="btn01 close1" onClick="$('#m_pop1').hide();">닫기</button>               
		                    </div>
		                
		                </div>
		 </div>  
		</div>	
		<!-- e_미제출자 -->		
	</body>
</html>
