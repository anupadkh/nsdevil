<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta property="og:title" content="MLMS">
		<meta property="og:image" content="img/favicon.png">
		<meta property="og:description" content="의과대학 LMS">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no, maximum-scale=1, width=device-width" />
		<title>MLMS</title>
		
		<link rel="icon" type="img/png" href="${M_IMG}/favicon.png">
		<link rel="shortcut icon" href="${M_IMG}/favicon.png">
		<link rel="icon" href="${M_IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${M_IMG}/favicon.ico">
		<link rel="stylesheet" href="${CSS}/css_m3.css" type="text/css">
		<link rel="stylesheet" href="${CSS}/swiper.min.css">
		<link rel="stylesheet" href="${JS}/lib/mobile/fullcalendar-v3.6.2/css/calendar1.css">
		<link rel="stylesheet" href="${CSS}/dev_css_m3.css">
		<script src="${JS}/lib/jquery-1.11.1.js"></script>
		<script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.form.min.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/lib/DateTimePicker.js"></script>
		<script src="${JS}/lib/swiper.min.js"></script>
		<script src="${JS}/mobileCommon.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				getAssignMentList();

				$(document).on("click", ".twrap_a", function(){
					$(this).hide();
					$(this).siblings("div.twrap_b").show();
				});
				$(document).on("click", ".twrap_b", function(){
					$(this).hide();
					$(this).siblings("div.twrap_a").show();
				});
			});
			
			function getAssignMentList(){
				
		        $.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/st/assignMent/list",
		            data: {      
		            	"asgmt_type" : "1",
		            	"curr_seq" : "${curr_seq}"
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		$("div[data-name=titleDiv]").html('<span>'+data.curr_info.curr_name+'</span>');
		            		var curr_date = "";
		            		var curr_mpf = "";
		            		if(!isEmpty(data.curr_info.curr_start_date_mmdd))
		            			curr_date = '<span class="ts1">교육과정 기간</span><span class="ts2">'+data.curr_info.curr_start_date_mmdd+' ~ '+data.curr_info.curr_end_date_mmdd+'</span><span class="sign">)</span>';
		            		
		            		if(!isEmpty(data.currMpf.name)){
		            			if(!isEmpty(data.currMpf.code_name))
		            				curr_mpf = '<span class="ts1">교수</span><span class="ts2">'+data.currMpf.name+'</span><span class="sign">(</span><span class="ts2">'+data.currMpf.code_name+'</span><span class="sign">)</span>';
	            				else
	            					curr_mpf = '<span class="ts1">교수</span><span class="ts2">'+data.currMpf.name+'</span></span>';
		            		}
		    				
		    				$("#currName").text(data.curr_info.curr_name);
		    				$("#currDate").html(curr_date);
		    				$("#currInfo").html(curr_mpf);
		    				
		            		var htmls = "";
		            		if(data.assignMentList.length == 0){
		            			$("#assignMentListAdd").addClass("regix");
		            			
		            		}else{
		            			$("#assignMentListAdd").removeClass("regix");
			            		$.each(data.assignMentList, function(index){
			            			
				            		htmls='<tr class="box_wrap">';
				            	    htmls+='<td class="box_s">';
				            	    htmls+='<div class="wrap1">';
				            	    htmls+='<div class="box1">';
				            				
				            	    htmls+='<div class="wrap_ss">';
				            	    htmls+='<div class="num">'+(index+1)+'</div>';
				            	    htmls+='<div class="prg_t dt">'+this.start_date+' ~ '+this.end_date+'</div>';
				            	    htmls+='</div>';
				            	    
				            	    if(!isEmpty(this.asgmt_name)){
					            	    htmls+='<div class="twrap_a">'+this.asgmt_name+'</div>';
					            	    htmls+='<div class="twrap_b" style="display:none;">'+this.full_asgmt_name+'</div>';
					            	}else{
					            		htmls+='<div class="twrap_a">'+this.full_asgmt_name+'</div>';
					            	    htmls+='<div class="twrap_b" style="display:none;">'+this.full_asgmt_name+'</div>';
					            	}
				            	    
				            		if(!isEmpty(this.file_name)){
				            			var file_name = this.file_name.split("||");
				            			var file_path = this.file_path.split("||");
				            			
				            			for(var i=0; i<file_name.length; i++){
					            			htmls+='<div class="list_wrap">';
						            	    htmls+='<ul class="dw_list">';
						            	    htmls+='<li class="li_1">';
						            	    htmls+='<div class="tt">'+file_name[i]+'</div><div class="bt_wrap">';
						            	    htmls+='<button class="dw" title="다운로드" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+file_path[i]+'\',\''+file_name[i]+'\');">다운로드</button></div>';                   
						            	    htmls+='</li>';		            	               
				           	                htmls+='</ul>';
				           	                htmls+='</div>';		            
				            			}
				            		}
		           	                htmls+='<div class="wrap2">';
		           	                htmls+='<div class="btn1">';
		           					if(this.submit_chk == "0")	
		           	                	htmls+='<span class="sp_1">미제출</span>';
	           	                	else
	           	                		htmls+='<span class="sp_2">제출 완료</span>';
		           					htmls+='</div>';
		           					if(this.submit_chk == "0")
		           	                	htmls+='<span class="sp_1s">과제가 제출되지 않았습니다.<br>( 과제는 PC웹으로만 제출가능 )</span>';
		           	                htmls+='</div>';		            	                		            				
		           	                htmls+='</div>';
		           	                htmls+='</div>';	
		           	                htmls+='</td>';
		           	                htmls+='</tr>';	
		           	                $("#assignMentListAdd").append(htmls);
	
			            		});
		            		}
		            	}else{
		            		alert("과제리스트 가져오기 실패");
		            	}
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        }); 
		    }
						
			function bindPopup(popupName, groupFlag, asgmt_seq) {
				var popup = $(popupName);
				getNotSubmitStudentList(groupFlag, asgmt_seq);
				$("input[name=group_flag]").val(groupFlag);
				$("input[name=asgmt_seq]").val(asgmt_seq);
				popup.show();
								
				$(document).click(function(e) {
				    if (popup.is(e.target)) {
				    	popup.hide();
				    }
				});
			}			
		</script>
	</head>
	<body class="color1">
		<!-- s_header -->
		<header class="header"> 
			<div class="wrap">
				
				<!-- s_gobf -->
				<div class="gobf" title="이전 페이지로 가기" onclick="javascript:history.go(-1); return false;"></div>
				<!-- e_gobf -->
					
				<!-- s_top_title -->
				<div class="top_title" data-name="titleDiv">
					
				</div>
				<!-- e_top_title -->
				
			</div>
		</header>
		<!-- e_header -->    
		<input type="hidden" name="group_flag">
		<input type="hidden" name="asgmt_seq">
		<!-- s_contents -->
		<div id="container" class="container_table">
		<div class="contents">
	
			<!-- s_top_tt-->
			<div class="top_tt_1">
				<span class="cc_t1" id="currName"></span>
				<span class="cc_t2" id="currDate"></span>
				<span class="cc_t2" id="currInfo"></span>	
				
			</div>
			<!-- e_top_tt -->
		
			<!-- s_ptb_wrap -->
			<div class="ptb_wrap">
				<!-- s_tb_hwork -->
				<table class="tb_hwork" id="assignMentListAdd">
					
				</table>
				<!-- e_tb_hwork -->
			</div>
			<!-- e_ptb_wrap -->
	
		</div>
		<!-- e_contents -->
		</div>
		
		<!-- s_미제출자 -->	
		<div id="m_pop1" class="pop_up_sbmx mo1">  
		          <div class="pop_wrap">  
		
					  <p class="t_title"><span class="sp_un">미제출</span><span class="sp_nn" name="submitSTCnt"></span><span class="sp_un" name="groupName">명</span></p>      
		 		     
			          <button class="renew_wrap" onClick="refresh();"><span class="renew"></span></button>
		 <!-- s_table_b_wrap -->
		<div class="table_b_wrap">	
		
		<!-- s_pht -->
		<div class="pht">                      
		          <div class="con_wrap"> 
		                <div class="con_s2" id="STListAdd"> 
		              
		                </div>
		         </div>
		         </div>
		<!-- e_pop_table -->	             
		                     
		</div>               
		<!-- e_pht --> 
		
		               <div class="t_dd">
		                   
		                    <div class="pop_btn_wrap2">
		                     <button type="button" class="btn01 close1" onClick="$('#m_pop1').hide();">닫기</button>               
		                    </div>
		                
		                </div>
		 </div>  
		</div>	
		<!-- e_미제출자 -->		
	</body>
</html>
