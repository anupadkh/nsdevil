<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
	$(document).ready(function() {
		getSFResearchList("N", "N");
		$("body").addClass("bd_svy");
		if("${type}" == "1")
			$("div[name=titleDiv]").html('<span class="survey">수업만족도 조사</span>');
		if("${type}" == "2")
			$("div[name=titleDiv]").html('<span class="survey">과정만족도 조사</span>');

		$(".tab").click(function() {
			$(".tab").removeClass("on");
			$(this).addClass("on");
		});
	});

	function getSFResearchList(yn_flag, minusPoint_flag) {

		var type = "";
		
		if("${type}" == "1")
			type = "1";
		if("${type}" == "2")
			type = "2";
		
		$.ajax({
			type : "POST",
			url : "${M_HOME}/ajax/st/sfResearch/list",
			data : {
				"sr_type" : type,
				"researchYN" : yn_flag,
				"minusPoint_flag" : minusPoint_flag
			},
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {
					$("#sfResearchListAdd").empty();
					var htmls = "";

					$.each(data.sfResearchList, function(index) {
						//수업종료 부터 오늘까지 일수 / 7
						var week_chk = 0;
						if(this.enddate_to_now > 0 && this.enddate_to_now <= 7)
							week_chk = 1;
						else
							week_chk = parseInt(this.enddate_to_now / 7);
												
						var research_submit_week = 0;
						//제출기한 부터 제출일까지 일수
						if(this.research_to_now > 0)
							research_submit_week = parseInt(this.research_to_now / 7);
						
						htmls = '<tr class="box_wrap">';
						htmls += '<td class="box_s w1">';
						//제출 완료 인지 체크
						if(this.research_chk == "N"){
							//수업종료 후 1주 이내 일 때
							if(week_chk <= 1){
								htmls += '<div class="wrap_s s1">';
								htmls += '<span class="num">'+this.end_date_monday+'</span>';
								htmls += '<span class="sp_s1">까지</span>';
								htmls += '</div>';
							}else{
								htmls += '<div class="wrap_s s2">';
								htmls += '<span class="sp_s1">감점</span>';
								htmls += '<span class="num">'+(week_chk*0.5)+'점</span>';
								htmls += '</div>';
							}
						}else{
							htmls += '<div class="wrap_s s3">';
							htmls += '<span class="sp_s1">제출<br>완료</span>';
							htmls += '</div>';
						}
						htmls += '</td>';
						htmls += '<td class="box_s w2">';
						if(this.enddate_to_now <= 0)
							htmls += '<div class="wrap_s ssw">';
						else
							htmls += '<div class="wrap_s ssw" onclick="getSFResearchView('+this.curr_seq+','+this.lp_seq+',\''+type+'\')">';	
						htmls += '<div class="wrap_ss">';
						htmls += '<span class="tm tm1">응답기간</span>';
						htmls += '<span class="tm num_s1">'+this.start_date+'</span>';
						htmls += '<span class="tm sign">~</span>';
						htmls += '<span class="tm num_s1">'+this.research_end_date+'</span>';
						if(week_chk > 0 && this.research_chk == "N"){
							htmls += '<span class="tm sign">(</span>';
							htmls += '<span class="tm num_2">'+week_chk+'</span>';
							htmls += '<span class="tm tm1">주 지남</span>';
							htmls += '<span class="tm sign">)</span>';
						}else if(research_submit_week > 0 && this.research_chk == "Y"){
							htmls += '<span class="tm sign">(</span>';
							htmls += '<span class="tm num_2">'+research_submit_week+'</span>';
							htmls += '<span class="tm tm1">주 지남</span>';
							htmls += '<span class="tm sign">)</span>';
						}
					
						htmls += '</div>';
						htmls += '<div class="wrap_ss">';
						
						if(!isEmpty(this.lesson_subject))
							htmls += '<span class="sp_1">'+this.curr_name+' - '+this.lesson_subject+'</span>';
						else
							htmls += '<span class="sp_1">'+this.curr_name+'</span>';
							
						if(research_submit_week > 0){
							htmls += '<span class="sp_3">'+(research_submit_week*0.5)+'점 감점 됨</span>';
						}else{
							if("${type}" == "1")
								htmls += '<span class="sp_2">( 주 - 0.5 감점 )  수업만족도 조사를 제출하세요.</span>';
							else if("${type}" == "2")
								htmls += '<span class="sp_2">( 주 - 0.5 감점 )  과정만족도 조사를 제출하세요.</span>';
						}
						htmls += '</div>';
						htmls += '</div>';
						htmls += '</td>';
						htmls += '</tr>';
						$("#sfResearchListAdd").append(htmls);
					});
				}
			},
			error : function(xhr, textStatus) {
				//alert("오류가 발생했습니다.");
				document.write(xhr.responseText);
			}
		});
	}

	function getSFResearchView(curr_seq, lp_seq, sr_type) {
		post_to_url("${M_HOME}/st/sfResearch/create", { "curr_seq" : curr_seq, "s_lp_seq" : lp_seq , "sr_type" : sr_type});
	}
</script>
</head>
<body class="color1">
	<!-- s_contents -->
	<div class="contents">

		<!-- survey1.html survey2.html survey3.html : 수업만족도 조사 설문 데이터가 매우 많아 질 수 있다하여 tab부분을 페이지로 제작한 것 (기본) 
survey.html : tab부분을 그대로 제작한 것 (추가:필요시 사용) -->

		<!-- s_mtb_wrap -->
		<div class="mtb_wrap">

			<div class="svy_ttwrap">
				<span class="svy1 tab on" onClick="getSFResearchList('N','N');">대기</span>
				<span class="svy2 tab" onClick="getSFResearchList('','Y');">감점</span>
				<span class="svy3 tab" onClick="getSFResearchList('','');">전체</span>
			</div>

			<div class="mtb_wrap">
				<!-- s_tb_svy2 -->
				<table class="tb_svy tb_svy2" id="sfResearchListAdd">

				</table>
				<!-- e_tb_svy2 -->

			</div>
			<!-- e_mtb_wrap -->

		</div>
		<!-- e_mtb_wrap -->
	</div>
	<!-- e_contents -->
</body>
</html>
