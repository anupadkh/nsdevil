<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
	$(document).ready(function() {	
		
		$(document).on("change","#date2",function(){
			if(isEmpty($(this).val()))
				return false;
			var week = ['일', '월', '화', '수', '목', '금', '토'];
			var dayOfWeek = week[new Date($(this).val()).getDay()];
			var date = new Date($(this).val());
			
			$("div[name=titleDiv] span").remove();
			
			var htmls = '<span class="dt_m" name="">'+(date.getMonth()+1)+'</span>';
			htmls += '<span class="sign_1" name="">/</span>';
			htmls += '<span class="dt_d" name="">'+date.getDate()+'</span>';
			htmls += '<span class="sign_1">(</span>';
			htmls += '<span class="dt_dw">'+dayOfWeek+'</span>';
			htmls += '<span class="sign_1">)</span>';
			$("div[name=titleDiv] #date2").before(htmls);
 			$("input[name=year]").val(date.getFullYear());
			getMobileLessonPlan(date.getFullYear(), $(this).val());
		});
		
		$("#dtBox").DateTimePicker({
			
			dateFormat: "MM-dd-yyyy",
			timeFormat: "HH:mm",
			dateTimeFormat: "MM-dd-yyyy HH:mm:ss AA",
			
			addEventHandlers: function()
			{
				var dtPickerObj = this;
				dtPickerObj.setDateTimeStringInInputField($("#date2"));				
			}
		});
	});
		
	function getMobileLessonPlan(year, date){
		$.ajax({
	        type: "POST",
	        url: "${HOME}/mobile/ajax/st/main/lessonPlanList",
	        data: {
	        	"date" : date,
	        	"year" : year
	        },
	        dataType: "json",
	        success: function(data, status) {
	            if (data.status == "200") {
					var htmls = '';
					if(data.lessonPlanList.length == 0){
						$("#lessonPlanListAdd").addClass("regix2");
					}else{					
						$("#lessonPlanListAdd").removeClass("regix2");
		            	$.each(data.lessonPlanList, function(index){
		            		var period = this.period.split(",");
		            		var ampm = "";
		            		if(this.ampm == "pm")
		            			ampm = "오후";
		            		else if(this.ampm == "am")
		            			ampm = "오전";
		            		
		            		//지난 수업은 회식으로 표시
		            		if(this.lesson_state == 'END')
		            			htmls+='<tr class="box_wrap box1">';
	            			else
	            				htmls+='<tr class="box_wrap box0">';
	            			            				
		            		htmls+='<td class="box_s w1">';
		            		htmls+='<div class="wrap_s s1">';
		            		if(period.length > 1){
			            		htmls+='<span class="num n2">'+period[0]+'</span> ';
			            		htmls+='<span class="num_sign">~</span>'; 
			            		htmls+='<span class="num n2">'+period[period.length-1]+'</span>';
		            		}else
		            			htmls+='<span class="num n1">'+this.period+'</span>';
		            		htmls+='</div>';
		            		htmls+='</td>';
		            		htmls+='<td class="box_s w2" onClick="getLessonPlanView('+this.lp_seq+','+this.curr_seq+');">';	            		
		            		htmls+='<div class="wrap_s s2">';
		            		htmls+='<div class="wrap_ss">';
		            		htmls+='<span class="tm tm1">'+ampm+'</span><span class="tm tm2">'+this.start_time+'</span>';
		            		htmls+='<span class="tm sign">~</span><span class="tm tm3">'+this.end_time+'</span>';
		            		htmls+='</div>';
		            		htmls+='<div class="wrap_ss">';
		            		htmls+='<span class="sp_1">'+this.curr_name+'</span>';
		            		if(!isEmpty(this.lesson_subject))
		            			htmls+='<span class="sp_2">'+this.lesson_subject+'</span>';
	            			else
	            				htmls+='<span class="sp_2">수업주제 미입력</span>';
		            		htmls+='</div>';
		            		if(this.fe_cnt != 0)
		            			htmls+='<span class="sp_quiz">형성평가</span>';
		            			
		            		//학생이면 출결 표시
		            		if(this.userlevel == 4){
		            			if(this.attendance_state == "00")
	            					htmls+='<span class="absent">결석</span>';
		            			else if(this.attendance_state == "02")
	            					htmls+='<span class="late">지각</span>';
		            			else if(this.attendance_state == "03")
	            					htmls+='<span class="attend">출석</span>';
		            		}
		            			
		            		htmls+='</div>';
		            		htmls+='</td>';
		            		htmls+='</tr>';
		            	});
		            	$("#lessonPlanListAdd").html(htmls);	
					}
	            } else {
	                alert("계획서 가져오기 실패.");
	            }
	        },
	        error: function(xhr, textStatus) {
	            //alert("오류가 발생했습니다.");
	            document.write(xhr.responseText);
	        },
	        beforeSend:function() {
	            $.blockUI();
	        },
	        complete:function() {
	            $.unblockUI();
	        }
	    });
	}

	function getLessonPlanView(lp_seq, curr_seq) {
		post_to_url("${M_HOME}/st/lessonPlan", {"lp_seq" : lp_seq,"curr_seq" : curr_seq});
	}
</script>
</head>
<body>
	
	<!-- s_contents -->
	<div class="contents">
		<!-- s_mtb_wrap -->
		<div class="mtb_wrap">
			<!-- s_tb_m1 -->
			<table class="tb_m1" id="lessonPlanListAdd">

			</table>
			<!-- e_tb_m1 -->
		</div>
		<!-- e_mtb_wrap -->

	</div>
	<!-- e_contents -->
</body>
</html>