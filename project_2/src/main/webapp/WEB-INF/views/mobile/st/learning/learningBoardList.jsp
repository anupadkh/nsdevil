<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function() {
				mobileHeaderInit('학습자료실');
				getNewDataList();
			});
			
			function getNewDataList() {
			    $.ajax({
			        type: "GET",
			        url: "${M_HOME}/ajax/common/board/list",
			        data: {},
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	var newBoard = data.new_board;
			            	var learning_cnt = newBoard.learning_cnt;
			            	var pbl_cnt =  newBoard.pbl_cnt;
			            	var cpx_cnt = newBoard.cpx_cnt;
			            	var osce_cnt =  newBoard.osce_cnt;
			            	var boardMenuList = $.parseJSON('${sessionScope.S_BOARD_MENU_LIST}');
						    $(boardMenuList).each(function() {
						    	var code = this.board_menu_code;
						    	var boardName = this.board_name;
						    	var url = this.url;
						    	var newTag = '<span class="new">NEW</span>';
						    	if (url.indexOf("/learning/list") > -1) {
						    		if (!(learning_cnt > 0)) {
						    			newTag = "";
					            	}
						    	} else if (url.indexOf("/pbl/list") > -1) {
						    		if (!(pbl_cnt > 0)) {
						    			newTag = "";
					            	}
						    	} else if (url.indexOf("/cpx/list") > -1) {
						    		if (!(cpx_cnt > 0)) {
						    			newTag = "";
					            	}
						    	} else if (url.indexOf("/osce/list") > -1) {
						    		if (!(osce_cnt > 0)) {
						    			newTag = "";
					            	}
						    	} else {
						    		newTag = "";
						    	}
						    	
						    	if (code == "00") {
						    		$("#learningMenuArea").append('<div class="box_s"><div class="wrap_s" onclick="location.href=\'${M_HOME}' + url + '\'"><span class="sp_2">' + boardName + '</span>'+newTag+'</div></div>');
						    	}
						    });
			            }
			        },
			        error: function(xhr, textStatus) {
			            //alert("오류가 발생했습니다.");
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
		</script>
	</head>
	<body class="color1">
		<div class="contents">
			<div class="mtb_wrap">	
				<div class="rfrmn" id="learningMenuArea">
<!-- 				    <div class="box_s"> -->
<!-- 				        <div class="wrap_s" onclick="location.href='cc_rfr_lrc.html'">         -->
<!-- 				            <span class="sp_1">LRC</span> -->
<!-- 							<span class="sp_1s">( LEARNING RESOURCE CENTER )</span> -->
<!-- 							<span class="new">NEW</span> -->
<!-- 				        </div> -->
<!-- 				    </div> -->
				</div>
			</div>
		</div>	
	</body>
</html>
