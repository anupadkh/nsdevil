<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function() {
				$("#dtBox").DateTimePicker({
					dateFormat: "MM-dd-yyyy",
					timeFormat: "HH:mm",
					dateTimeFormat: "MM-dd-yyyy HH:mm:ss AA",
					addEventHandlers: function() {
						this.setDateTimeStringInInputField($("#date2"));				
					}
				});
				
				var paramDate = "${memo_date}";
				if (typeof paramDate == "undefined" || paramDate == "") {
					paramDate = getTodayYYYYMMDD();
				}
				
				$("#date2").val(paramDate);
				changeDate(paramDate);
			});
		
			function changeDate(dateValue) {
				if (isEmpty(dateValue)) {
					return false;
				}
				var dayOfWeek = getDayOfWeek(dateValue);
				var date = new Date(dateValue);
				$("div[name='titleDiv'] span").remove();
				var htmls = '<span class="dt_m" name="">'+(date.getMonth()+1)+'</span>';
				htmls += '<span class="sign_1" name="">/</span>';
				htmls += '<span class="dt_d" name="">'+date.getDate()+'</span>';
				htmls += '<span class="sign_1">(</span>';
				htmls += '<span class="dt_dw">'+dayOfWeek+'</span>';
				htmls += '<span class="sign_1">)</span>';
				$("div[name='titleDiv'] #date2").before(htmls);
				getPrivateScheduleList(dateValue);
			}
			
			$(document).on("change", "#date2", function(){
				changeDate($(this).val());
			});
			
			function getPrivateScheduleList(date) {
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/st/schedule/private/list",
			        data: {
			        	"current_date" : date
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	//나만보기:01
			        	//조별보기:02
			        	//학생전체:03
			        	//그외 04
			            if (data.status == "200") {
			            	var privateSchdList = data.private_schd_list;
			            	var schdHtml = "";
			            	$(privateSchdList).each(function() {
			            		var memoSeq = this.memo_seq;
			            		var memoName = this.memo_name;
			            		var content = this.content;
								var memoFinishFlag = this.memo_finish_flag;
								var memoFinishYnClass = "box0";
								if (memoFinishFlag == "Y") {
									memoFinishYnClass = "box1"; //지난 메모
								}
								var startTime = this.start_time;
								var endTime = this.end_time;
								var startAmpmText = this.start_ampm;
								var endAmpmText = this.end_ampm;
			            		
								if(startAmpmText == "pm"){
			            			startAmpmText = "오후";
			            		} else {
			            			startAmpmText = "오전";
			            		}
								
								if(endAmpmText == "pm"){
									endAmpmText = "오후";
			            		} else {
			            			endAmpmText = "오전";
			            		}
			            		var place = this.place;
			            		
				            	schdHtml+= '<tr class="box_wrap  ' + memoFinishYnClass + '"" onclick="javascript:getPrivateScheduleDetailView(' + memoSeq + ', \'' + date + '\');">';
				            	schdHtml+= '	<td class="box_s w1">';
				            	schdHtml+= '		<div class="wrap_s"></div>';
				            	schdHtml+= '	</td>';
				            	schdHtml+= '	<td class="box_s w2">';
				            	schdHtml+= '		<div class="wrap_s s2">';
				            	schdHtml+= '			<div class="wrap_ss">';
				            	schdHtml+= '				<span class="tm tm1">' + startAmpmText + '</span><span class="tm tm2">' + startTime + '</span><span class="tm sign">~</span><span class="tm tm1">' + endAmpmText + '</span><span class="tm tm3">' + endTime + '</span>';
				            	schdHtml+= '			</div>';
				            	schdHtml+= '			<div class="wrap_ss">';
				            	schdHtml+= '				<span class="sp_1">' + memoName + '</span>';
				            	schdHtml+= '				<span class="sp_2">' + content + '</span>';
				            	schdHtml+= '			</div>';
				            	if (place != '') {
					            	schdHtml+= '		<div class="wrap_ss">';
				            		schdHtml+= '			<span class="spc1">' + place + '</span>';
			            			schdHtml+= '		</div>';
				            	}
				            	schdHtml+= '		</div>';
				            	schdHtml+= '	</td>';
				            	schdHtml+= '</tr>';
			            	});
			            	
			            	$("#schdListArea").html(schdHtml);
			            	
			            } else {
			            	alert("개인일정을 불러오지 못하였습니다. [" + data.status + "]");
			            }
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function getPrivateScheduleDetailView(memoSeq, memoDate) {
            	post_to_url("${M_HOME}/st/schedule/private/detail", {"memo_seq":memoSeq, "memo_date":memoDate});				
			}
		</script>
	</head>
	<body>
		<div class="contents">
			<div class="ptb_wrap">	
				<table class="tb_p1" id="schdListArea"></table>
			</div>	
		</div>
	</body>
</html>