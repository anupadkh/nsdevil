<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function() {
				getLessonPlanInfo();//수업계획서 정보
				getLessonPlanTitleInfo();//상단 타이틀 입력
			});
			
			function getLessonPlanTitleInfo(){
					
				$.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/st/lessonPlanTitleInfo",
		            data: {                  
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status == "200"){
			            	var lesson_date = data.info.lesson_date.split("-");
			            	//교시
							var periodStr = data.info.period; //교시
							var periodList = periodStr.split(',');
							var startPeriod = periodList[0];
							if (periodList.length > 1) {
								periodStr = startPeriod + "~" + periodList[periodList.length-1] + "교시";  
							} else {
								periodStr = startPeriod + "교시";
							}
							
			            	//수업일
			            	var lessonDate = lesson_date[0]+"월 "+lesson_date[1]+"일";
							
			            	//수업시간
							var ampmText = data.info.ampm;
			        		if(ampmText == "pm"){
			        			ampmText = "오후";
			        		} else {
			        			ampmText = "오전";
			        		}
			        		
			        		var startTime = data.info.start_time_12;
							var endTime = data.info.end_time_12;
			            	
			        		var lessonPlanDateTitle = lessonDate + " " + periodStr;//mm월 xx일 x교시
			        		var lessonPlanTimeTitle = ampmText + " " + data.info.start_time + "~" + data.info.end_time;//오전/오후 xx:xx~xx:xx
			        		
			            	var titleHtml = '<span class="cc_s1">' + lessonPlanDateTitle + '</span><span class="cc_s2">(' + lessonPlanTimeTitle + ')</span>';
			            	var titleDiv = $("div[name='titleDiv']").addClass("cc").html(titleHtml);
			            	
			            	var lessonInfoHtml = '<span class="cc_t1">'+data.info.lesson_subject+'</span>'
								+ '<span class="cc_t2"><span class="ts1">교수</span>';
	
							if(isEmpty(data.info.name)){
								lessonInfoHtml+='<span class="ts2">미배정</span>';
							}else{
								lessonInfoHtml+='<span class="ts2">'+data.info.name+'</span>'
								+ '<span class="sign">(</span><span class="ts2">'+data.info.code_name+'</span><span class="sign">)</span></span>';
							}
							
							if(data.info.attendance_state == "WAIT")
								lessonInfoHtml +='<span class="cc_t3">출석 전</span>';
							else if(data.info.attendance_state == "00")
								lessonInfoHtml +='<span class="absent">결석</span>';
							else if(data.info.attendance_state == "02")
								lessonInfoHtml +='<span class="late">지각</span>';
							else if(data.info.attendance_state == "03")
								lessonInfoHtml +='<span class="attend">출석</span>';
							else
								lessonInfoHtml +='<span class="cc_t3">출석 미입력</span>';
									
							$("#lessonInfo").html(lessonInfoHtml);
		            	}
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        });
			}
			
			function getLessonPlanInfo(){
				
		        $.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/pf/lesson/lessonPlanBasicInfo",
		            data: {                  
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	//교시
						var periodStr = data.lessonPlanBasicInfo.period; //교시
						var periodList = periodStr.split(',');
						var startPeriod = periodList[0];
						if (periodList.length > 1) {
							periodStr = startPeriod + "~" + periodList[periodList.length-1] + "교시";  
						} else {
							periodStr = startPeriod + "교시";
						}
						    
						//수업시간
						var ampmText = data.lessonPlanBasicInfo.ampm;
		        		if(ampmText == "pm"){
		        			ampmText = "오후";
		        		} else {
		        			ampmText = "오전";
		        		}
		        		
		            	var aca_name = data.lessonPlanBasicInfo.year + " " + data.lessonPlanBasicInfo.m_aca_system_name + " " + data.lessonPlanBasicInfo.s_aca_system_name + " " + data.lessonPlanBasicInfo.aca_system_name;
		            	
						
		            	$("span[name=lesson_date]").text(data.lessonPlanBasicInfo.lesson_date + " [" + periodStr + "]");
		            	$("span[name=lesson_time]").text(ampmText + " " + data.lessonPlanBasicInfo.start_time_12 + " ~ " + data.lessonPlanBasicInfo.end_time_12);
		            	$("span[name=acaName]").text(aca_name);
		            	
		            	$("#currName").text(data.lessonPlanBasicInfo.curr_name);
		            	$("#lessonSubject").text(data.lessonPlanBasicInfo.lesson_subject);
		            	$("#emName").text(data.lessonPlanBasicInfo.evaluation_method_name);
		            	$("div[name=learning_outcome]").text(data.lessonPlanBasicInfo.learning_outcome);
		            	$("td[name=lesson_code]").text(data.lessonPlanBasicInfo.lesson_code);
		            	$("span[name=period_cnt]").text(data.lessonPlanBasicInfo.period_cnt);
		            	$("span[name=classroom]").text(data.lessonPlanBasicInfo.classroom);
		            	
		            	if(isEmpty(data.lessonPlanBasicInfo.evaluation_method_name))
		            		$("#emName").closest("tr").hide();
		            	
		            	
		            	if(isEmpty(data.lessonPlanBasicInfo.domain_name) && isEmpty(data.lessonPlanBasicInfo.level_name)){
		            		$("#lessonLevel").hide();
		            	}else if(!isEmpty(data.lessonPlanBasicInfo.domain_name) && isEmpty(data.lessonPlanBasicInfo.level_name)){
		            		var htmlss = '<td class="th01 w1">영역</td><td class="w2" colspan="3"><span class="sp_tt1">'+data.lessonPlanBasicInfo.domain_name+'</span></td>';
		            		$("#lessonLevel").html(htmlss);
		            	}else if(isEmpty(data.lessonPlanBasicInfo.domain_name) && !isEmpty(data.lessonPlanBasicInfo.level_name)){
		            		var htmlss = '<td class="th01 w1">수준</td><td class="w2" colspan="3"><span class="sp_tt1">'+data.lessonPlanBasicInfo.level_name+'</span></td>';
		            		$("#lessonLevel").html(htmlss);
		            	}else{
		            		var htmlss = '<td class="th01 w1">영역</td><td class="w2"><span class="sp_tt1">'+data.lessonPlanBasicInfo.domain_name+'</span></td>';
		            		htmlss += '<td class="th01 w1">수준</td><td class="w2"><span class="sp_tt1">'+data.lessonPlanBasicInfo.level_name+'</span></td>';
		            		$("#lessonLevel").html(htmlss);
		            	}
		            	
		            		
							
		            		
		            	$("span[name=domain_name]").text(data.lessonPlanBasicInfo.domain_name);
		            	$("span[name=level_name]").text(data.lessonPlanBasicInfo.level_name);
		            	
		            	var ranNum = 0;
		            	var htmls='';
		            	
		            	//핵심 임상표현
	                	$.each(data.lessonCoreClinicList,function(index){
	                        ranNum = Math.floor(Math.random()*7) + 1;
	                        
	                        htmls += '<div class="c_wrap">';
		                    htmls += '<span class="tt_c color0'+ranNum+'">'+this.code_name+'</span></div>';         
	                    });
	                    $("#coreClinicListAdd").html(htmls);

		                if(data.lessonCoreClinicList.length == 0){
		                	$("#coreClinicListAdd").closest("tr").hide();
		                }	                                       
	                    
	                	htmls = '';
	                	//임상표현
	                	$.each(data.lessonClinicList,function(index){
	                        ranNum = Math.floor(Math.random()*7) + 1;
	                        
	                        htmls += '<div class="c_wrap">';
		                    htmls += '<span class="tt_c color0'+ranNum+'">'+this.code_name+'</span></div>';          
	                    });
	                    $("#clinicListAdd").html(htmls);

		                if(data.lessonClinicList.length == 0){
		                	$("#clinicListAdd").closest("tr").hide();
		                }	                                       
	                    
	                    htmls = '';
	                	//임상술기
	                	$.each(data.lessonOsceList,function(index){
	                        ranNum = Math.floor(Math.random()*7) + 1;
	                        
	                        htmls += '<div class="c_wrap">';
		                    htmls += '<span class="tt_c color0'+ranNum+'">'+this.osce_name+'</span></div>';        
		                       
	                    });
	                    $("#osceCodeListAdd").html(htmls);

		                if(data.lessonOsceList.length == 0){
		                	$("#osceCodeListAdd").closest("tr").hide();
		                }
	                    
	                	htmls = '';
	                	//진단명
	                    $.each(data.lessonDiagnosisList,function(index){
	                        ranNum = Math.floor(Math.random()*7) + 1;

	                        htmls += '<div class="c_wrap">';
		                    htmls += '<span class="tt_c color0'+ranNum+'">'+this.code_name+'</span></div>';  		                      
	                    });
	                	$("#diaCodeAdd").html(htmls);

		                if(data.lessonDiagnosisList.length == 0){
		                	$("#diaCodeAdd").closest("tr").hide();
		                }
		                
		              	//졸업역량
		                htmls='';
		                $.each(data.lessonFinishCapabilityList,function(index){
		                    ranNum = Math.floor(Math.random()*7) + 1;

		                    htmls += '<div class="c_wrap">';
		                    htmls += '<span class="tt_c color0'+ranNum+'">('+this.fc_code+') '+this.fc_name+'</span></div>';
		                });
		                $("#fcListAdd").html(htmls);
		                
		                if(data.lessonFinishCapabilityList.length == 0){
		                	$("#fcListAdd").closest("tr").hide();
		                }
		                
		                
		                //형성평가
		                htmls='';
		              	var fe_cnt = 0;
		                $.each(data.formationEvaluationList,function(index){
		                	 ranNum = Math.floor(Math.random()*6) + 2;
		                	 
		                	 var colorClass = "";
		                	 if(this.fe_cate_code=="01")
		                		 colorClass = "color01";
		                	 else
		                		 colorClass = "color0"+ranNum;
		                	 
		                	 htmls += '<div class="c_wrap">';
		                	 htmls += '<span class="tt_c '+colorClass+'">'+this.code_name+'</span></div>';
		                	 fe_cnt++;                   
		                });
		                $("#feListAdd").html(htmls); 
		                $("div[name=fe_num]").text(fe_cnt);
		                
		                //비강의
	                    htmls='';
		                if(data.lessonMethodList.length == 0){
		                	htmls += '<div class="c_wrap">';
		                    htmls += '<span class="tt_c color0'+ranNum+'">강의</span></div>';		    
		                }else{
		                    $.each(data.lessonMethodList, function(index){  
		                    	htmls += '<div class="c_wrap">';
			                    htmls += '<span class="tt_c color0'+ranNum+'">'+this.code_name+'</span></div>';		                                
		                    });
		                }
	        			$("#lessonMethodAdd").html(htmls);
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        }); 
		    }
			
			function getLessonPlanView(){
				post_to_url("${M_HOME}/st/lessonPlan", {"lp_seq":"${sessionScope.S_LP_SEQ}", "curr_seq":"${sessionScope.S_CURRICULUM_SEQ}"});
			}

			function getCurriculumView(){
				post_to_url("${M_HOME}/st/curriculum", {"curr_seq" : "${sessionScope.S_CURRICULUM_SEQ}"});
			}			

			function getFormationEvaluationListView() {
				post_to_url("${M_HOME}/st/formationEvaluation/list", {"lp_seq" : "${sessionScope.S_LP_SEQ}", "curr_seq":"${sessionScope.S_CURRICULUM_SEQ}"});
			}
		</script>
	</head>
	<body class="color1">
		<!-- s_contents -->
		<div class="contents">
	
			<!-- s_top_tt-->
			<div class="top_tt" id="lessonInfo">
			</div>
			<!-- e_top_tt -->
	
			<!-- s_mn_wrap -->
			<div class="mn_wrap syllabus">
				<div class="wrap_s">
					<span class="cc_mn cc_mn01" onClick="javascript:getCurriculumView();">교육과정</span>
					<span class="cc_mn cc_mn02 on" onClick="javascript:getLessonPlanView();">수업계획</span>
					<span class="cc_mn cc_mn03" onClick="location.href='${M_HOME}/st/lessonData'">자료</span>
					<span class="cc_mn cc_mn04" onClick="javascript:getFormationEvaluationListView();">형성평가</span>
					<span class="cc_mn cc_mn05" onClick="location.href='${M_HOME}/st/assignMent';">과제</span>
				</div>
			</div>
			<!-- e_mn_wrap -->
	
			<!-- s_ptb_wrap -->
			<div class="ptb_wrap">
				<!-- s_sbus -->
				<table class="sbus">
					<tbody>
						<tr class="tr02">
							<td class="th01 w1">강의<br>코드</td>
							<td class="w2" name="lesson_code"></td>
							<td class="th01 w1">일정</td>
							<td class="w2"><span class="sp01" name="lesson_date"></span>
							<span class="sp02" name="lesson_time"></span></td>
						</tr>
						<tr class="tr02">
							<td class="th01">시수</td>
							<td class="w2"><span class="sp_tt" name="period_cnt"></span></td>
							<td class="th01">장소</td>
							<td class="w2"><span class="sp_tt1" name="classroom"></span></td>
						</tr>
						<tr class="tr02" id="lessonLevel">
							
						</tr>
						<tr class="tr02">
							<td class="th01 w1">평가<br>방법</td>
							<td colspan="3" class="td_cnt" id="emName"></td>
						</tr>
						<tr class="tr02">
							<td class="th01 w1">학습<br>방법</td>
							<td colspan="3" class="td_cnt" id="lessonMethodAdd">
								
							</td>
						</tr>
						<tr class="tr02">
							<td class="th01 w1">핵심 ${code2.code_name }</td>
							<td colspan="3" class="td_cnt" id="coreClinicListAdd">
								
							</td>
						</tr>
	
						<tr class="tr02">
							<td class="th01 w1">관련 ${code2.code_name }</td>
							<td colspan="3" class="td_cnt" id="clinicListAdd">
								
							</td>
						</tr>
						
						<tr class="tr02">
							<td class="th01 w1">${code1.code_name }</td>
							<td colspan="3" class="td_cnt" id="osceCodeListAdd">
								
							</td>
						</tr>
						
						<tr class="tr02">
							<td class="th01 w1">${code3.code_name }</td>
							<td colspan="3" class="td_cnt" id="diaCodeAdd">
								
							</td>
						</tr>
	
						<tr class="tr02">
							<td class="th01 w1">형성<br>평가<br>여부</td>
							<td colspan="2" class="td_fnum" id="feListAdd">
								
							</td>
							<td class="td_num">
								<div class="tt_num" name="fe_num"></div>
							</td>
						</tr>
						<tr class="tr02">
							<td class="th01 w1">졸업<br>역량</td>
							<td colspan="3" class="td_cnt" id="fcListAdd">
								
							</td>
						</tr>
					</tbody>
				</table>
				<!-- e_sbus -->
	
				<!-- s_학습성과 -->
				<div class="sbus_title">학습성과</div>
				<div class="sbus_tcon" name="learning_outcome"></div>
				<!-- e_학습성과 -->
	
			</div>
			<!-- e_ptb_wrap -->
	
		</div>
		<!-- e_contents -->
	</body>
</html>
