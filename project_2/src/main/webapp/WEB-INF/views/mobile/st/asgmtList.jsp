<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>		
		<script type="text/javascript">
			$(document).ready(function(){
				customSelectBoxInit();
				initPageTopButton();
				getAsgmtScoreNew();
				getAsgmtList();
				
				$("body").addClass("homework");
				$("div[name=titleDiv]").removeClass("top_date").addClass("top_title").empty().html('<span class="survey">과제</span>');
			});			
			
			function changeTab(obj){
				//탭클릭 lp = 수업과제, curr = 과정과제
				if($(obj).attr("data-name") == "lp"){
					$("span[data-name=lp]").addClass("on");
					$("span[data-name=curr]").removeClass("on");
					$("div[data-name=homeClass]").removeClass("hwork_w2").addClass("hwork_w1");
					getAsgmtList();
				}else{
					$("span[data-name=curr]").addClass("on");
					$("span[data-name=lp]").removeClass("on");
					$("div[data-name=homeClass]").removeClass("hwork_w1").addClass("hwork_w2");
					getAsgmtList();
				}
			}
			
			function getAsgmtScoreNew(){
				 $.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/st/asgmtScore/newCount",
		            data: {                  
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		if(data.newCnt.curr_asgmt_cnt != 0){
		            			$("span[data-name=curr_asgmt]").show();
		            		}else{
		            			$("span[data-name=curr_asgmt]").hide();
		            		}
		            		if(data.newCnt.lp_asgmt_cnt != 0){
		            			$("span[data-name=lp_asgmt]").show();		            			
		            		}else{
		            			$("span[data-name=lp_asgmt]").hide();
		            		}
		            	}
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        }); 			
			}	
			
			function getAsgmtList() {

				var asgmt_type = "";
				
				if($("span[data-name=lp]").hasClass("on"))
					asgmt_type="2";
				else
					asgmt_type="1";
				
				$.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/st/asgmt/list",
		            data: {
		            	"asgmt_type" : asgmt_type
		            	,"submit_yn" : $("#submitYN").attr("data-value")            	
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	var htmls="";
		            	$("#asgmtListAdd").empty();
		            	$.each(data.asgmtList, function(index){		
		            		var submit_class = "sp_ttb";
		            		var submit_yn = "미제출";
		            		
		            		if(this.asgmt_submit_yn != "N"){
		            			submit_class = "sp_tta";
		            			submit_yn = "제출";
		            		}
		            		
		            		if($("span[data-name=lp]").hasClass("on")){
			            		
			            		var atd_class = "";
			            		var atd_state = "";
			            		var period = this.period.split(",");
			            		var ampm = "";
			            		
			            		if(this.ampm == "am")
			            			ampm = "오전";
			            		else
			            			ampm = "오후";
			            			
			            		if(period.length > 1)
			            			period=period[0]+"~"+period[period.length-1];
			            				            		
			            		if(this.attendance_state == "00"){
			            			atd_class="absent";
			            			atd_state="결석";
			            		} else if(this.attendance_state == "02"){
			            			atd_class="late";
			            			atd_state="지각";
			            		} else if(this.attendance_state == "03"){
			            			atd_class="attend";
			            			atd_state="출석";
			            		} else if(this.attendance_state == ""){
			            			atd_class="att_bf";
			            			atd_state="출석체크 전";		            			
			            		} else{
			            			atd_state="";
			            		} 			
			            			
			            		htmls='<tr class="box_wrap" onclick="getLpAsgmt('+this.lp_seq+','+this.curr_seq+');">'
			            		+'<td class="box_s w1">'
			            		+'<div class="wrap_s s1">'
			            		+'<span class="'+submit_class+'">'+submit_yn+'</span>'
			            		//+'<span class="sp_s1"><span class="num1">1</span><span class="sign">/</span><span class="num2">2</span></span>'
			            		+'</div>'
			            		+'</td>'						
			            		+'<td class="box_s w2">'
			            		+'<div class="wrap_s">'
			            		+'<div class="wrap_ss">'
			            		+'<span class="tm num_s1">'+this.lesson_date+'</span>'	
			            		+'<span class="'+atd_class+'">'+atd_state+'</span>'
			            		+'</div>'
			            		+'<div class="wrap_ss">'
			            		+'<span class="sp_1">['+this.curr_name+'] '+this.lesson_subject+'</span>'
			            		+'<span class="sp_2">'
			            		+'<span class="num1">'+period+'</span><span class="tt1">교시</span>'
			            		+'<span class="sign">|</span><span class="num2">'+ampm+' '+this.start_time+'~'+this.end_time+'</span>'
			            		+'<span class="tt2">'+this.name+'</span>'
			            		+'</span>'
			            		+'</div>'
			            		+'</div>'
								+'</td>'						
								+'</tr>';
			            	}else{
			            		htmls = '<tr class="box_wrap" onclick="getAsgmtView('+this.curr_seq+');">'
		            	    	+'<td class="box_s w1">'
		            	    	+'<div class="wrap_s s1">'
			            		+'<span class="'+submit_class+'">'+submit_yn+'</span>'
		            	    	//+'<span class="sp_s1"><span class="num1">1</span><span class="sign">/</span><span class="num2">2</span></span>'
		            	    	+'</div>'
		            	    	+'</td>'
		            	    	+'<td class="box_s w2">'
		            	    	+'<div class="wrap_s">'
		            	    	+'<div class="wrap_ss">'
		            	    	+'<span class="tt2">'+this.curr_name+'</span>'
		            	    	+'</div>'
		            	    	+'<div class="wrap_ss">'
		            	    	+'<span class="sp_2">'
		            	    	+'<span class="num2">'+this.curr_start_date+'~'+this.curr_end_date+'</span>';
		            	    	
		            	    	if(!isEmpty(this.grade))
		            	    		htmls+='<span class="sign">–</span><span class="num1">'+this.grade+'</span><span class="tt1">학점</span>';
		            	    		
		            	    	htmls+='</span>'
		            	    	+'<span class="sp_2">'
		            	    	+'<span class="tt3">책임교수</span><span class="tt4">'+this.mpf_name+'</span>'
		            	    	+'</span>'
		            	    	+'</div>'
		            	    	+'</div>'
		            	    	+'</td>'
		            	    	+'</tr>';			            		
			            	}
		            		$("#asgmtListAdd").append(htmls);
		            	});
		    	    	
		            },
		            error: function(xhr, textStatus) {
		                alert("오류가 발생했습니다.");
		            }
		        });
			}
			
			function getLpAsgmt(lp_seq, curr_seq){
				$.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/pf/common/setCurrLpSeq",
		            data: {   
		            	"lp_seq" : lp_seq,
		            	"curr_seq" : curr_seq
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	location.href='${M_HOME}/st/assignMent';
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        });
			}
			
			function getAsgmtView(curr_seq){
				post_to_url("${M_HOME}/st/currAssignMent", { "curr_seq" : curr_seq});				
			}
		</script>
	</head>
	<body class="color2 inquiry v1">
	<!-- s_contents -->
	<div class="contents">

		<!-- Tab : cc_hwork_list.html, 각각을 Page로 나눈 것 cc_hwork_list1.html, cc_hwork_list2.html : 필요에 따라 선택 사용 -->
		<!-- s_hworktb_wrap -->
		<div class="hworktb_wrapv">

			<!-- s_hwork_w1 -->
			<div class="hwork_w1" data-name="homeClass">

				<div class="hwork_ttwrap">
					<span class="hwork1 on" data-name="lp" onClick="changeTab(this);">수업과제<span class="newsign" data-name="lp_asgmt">N</span></span> 
					<span class="hwork2" data-name="curr" onClick="changeTab(this);">교육과정 과제<span class="newsign" data-name="curr_asgmt">N</span></span>
				</div>

				<!-- s_sch_wrap-->
				<div class="sch_wrap">
					<span class="tts">제출여부</span>
					<div class="wrap_uselectbox1">
						<div class="uselectbox">
							<span class="uselected" data-value="" id="submitYN">전체</span> <span class="uarrow">▼</span>
							<div class="uoptions">
								<span class="uoption firstseleted" data-value="">전체</span>
								<span class="uoption" data-value="Y">제출</span>
								<span class="uoption" data-value="N">미제출</span>
							</div>
						</div>
					</div>

					<button class="btn_search1" onClick="getAsgmtList();">검색</button>
				</div>
				<!-- e_sch_wrap -->

				<div class="mtb_wrap">
					<!-- s_tb_hwork1 -->
					<table class="tb_hwork1" id="asgmtListAdd">			
						
					</table>
					<!-- e_tb_hwork1 -->
				</div>
			</div>
			<!-- e_hwork_w1 -->
		</div>
		<!-- e_hworktb_wrap -->
	</div>
	<!-- e_contents -->
</body>
</html>