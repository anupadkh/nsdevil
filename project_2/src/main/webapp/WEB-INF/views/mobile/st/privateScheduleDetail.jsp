<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta property="og:title" content="MLMS">
		<meta property="og:image" content="img/favicon.png">
		<meta property="og:description" content="의과대학 LMS">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no, maximum-scale=1, width=device-width" />
		<title>MLMS</title>
		
		<link rel="icon" type="img/png" href="${M_IMG}/favicon.png">
		<link rel="shortcut icon" href="${M_IMG}/favicon.png">
		<link rel="icon" href="${M_IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${M_IMG}/favicon.ico">
		<link rel="stylesheet" href="${CSS}/css_m1.css" type="text/css">
		<link rel="stylesheet" href="${CSS}/swiper.css">
		<link rel="stylesheet" href="${CSS}/dev_css_m1.css">
		<script src="${JS}/lib/jquery-1.11.1.js"></script>
		<script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.form.min.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/lib/swiper.js"></script>
		<script src="${JS}/mobileCommon.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				getPrivateScheduleDetail("${memo_seq}")
			});
		
			function getPrivateScheduleDetail(memoSeq) {
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/st/schedule/private/detail",
			        data: {
			        	"memo_seq" : memoSeq
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	var schdInfo = data.schd_info;
			            	
			            	var memoSeq = schdInfo.memo_seq;
							var startTime = schdInfo.start_time;
							var endTime = schdInfo.end_time;
							var startAmpmText = this.start_ampm;
							var endAmpmText = this.end_ampm;
							
							if(startAmpmText == "pm"){
		            			startAmpmText = "오후";
		            		} else {
		            			startAmpmText = "오전";
		            		}
							
							if(endAmpmText == "pm"){
								endAmpmText = "오후";
		            		} else {
		            			endAmpmText = "오전";
		            		}
							var memoDate = schdInfo.memo_date;
							var dayOfWeek = getDayOfWeek(memoDate);
		            		var memoName = schdInfo.memo_name;
		            		var content = schdInfo.content;
		            		var place = schdInfo.place;
		            		memoDate = memoDate.substring(5, memoDate.length);
		            		var dateLabel = memoDate.replace("-", "월 ") + "일 (" + dayOfWeek + ")"; //02월 02일 (금)
		            		var timeLabel = "(" + startAmpmText + " " + startTime + "~" + endAmpmText + " " + endTime + ")"; //(오후 02:00~02:50)
		            		
			            	var memoAreaHtml = "";
		            		memoAreaHtml += '<div class="wrap_ss">';
							memoAreaHtml += '	<span class="sp_1">' + memoName + '</span>';
							memoAreaHtml += '	<span class="sp_2">' + content + '</span>';
							memoAreaHtml += '</div>';
							if (place != "") {
								memoAreaHtml += '<div class="wrap_ss">';
								memoAreaHtml += '	<span class="spc1">' + place + '</span>';
								memoAreaHtml += '</div>';
							}
			            	$("#memoArea").html(memoAreaHtml);
			            	$("#dateLabel").html(dateLabel);
		            		$("#timeLabel").html(timeLabel);
			            	$("#memoRemovePopup #memoRemoveBtn").attr("onclick", "javascript:memoRemove(" + memoSeq + ");");
			            } else {
			            	alert("개인일정을 불러오지 못하였습니다. [" + data.status + "]");
			            }
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function getPrivateScheduleDetailView(memoSeq) {
            	post_to_url("${M_HOME}/st/schedule/private/detail", {"memo_seq":memoSeq});
			}
			
			function privateScheduleListView() {
				post_to_url("${M_HOME}/st/schedule/private/list", {"memo_date":"${memo_date}"});
			}
			
			function memoRemovePopupOpen() {
				$("#memoRemovePopup").show();
			}
			
			function memoRemovePopupClose() {
				$("#memoRemovePopup").hide();
			}
			
			function memoRemove(memoSeq) {
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/st/schedule/private/remove",
			        data: {
			        	"memo_seq" : memoSeq
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	memoRemovePopupClose();
			            	privateScheduleListView();
			            } else {
			            	alert("개인일정 삭제에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
		</script>
	</head>
	<body class="color1 bd_pnv1">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
		    <p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
		<div id="skipnav">
			<a href=".header">주 메뉴 바로가기</a>
			<a href="#container">컨텐츠 바로가기</a>
		</div>
		<div id="wrap">
			<header class="header"> 
				<div class="wrap">
				    <div class="golist"><a href="javascript:void(0);" onclick="javascript:privateScheduleListView(); return false;" title="일정 리스트 페이지로 가기"></a></div> 
					<div class="top_date cc">
						<span class="cc_s1" id="dateLabel"></span><span class="cc_s2" id="timeLabel"></span>
					</div>
					<div class="menu_button"><div class="menu_list open1" title="일정 삭제하기" onclick="javascript:memoRemovePopupOpen();"></div></div>
				</div>
			</header>
			<div id="container" class="container_table">
				<div class="contents">
					<div class="ptb_wrap">	
						<table class="tb_p1v">
							<tr class="box_wrap box0 box_v1">
								<td class="box_s">
									<div class="wrap_s" id="memoArea"></div>
								</td>
							</tr>
						</table>
						<div class="tmwrap"><span class="schd_tt">개인 일정</span></div>
					</div>	
				</div>
			</div>
			<a href="#" id="backtotop" title="To top" class="totop"></a>
		</div>
		
		
		<!-- s_개인일정 삭제 -->	
		<div id="memoRemovePopup" class="pop_up_schd_px mo1">  
			<div class="pop_wrap">  
				<p class="t_title"><span class="sp_un">개인일정 삭제</span></p>      
			    <div class="b_wrap">	
					<span class="tt1">개인 일정을 삭제하시겠습니까?</span>
					<span class="tt2">(삭제한 메모는 복구되지 않습니다.)</span>
			    </div>               
               	<div class="t_dd">
					<div class="pop_btn_wrap2">
                    	<button type="button" class="btn01 close1" onclick="javascript:memoRemovePopupClose();">아니오</button>   
						<button type="button" class="btn02" id="memoRemoveBtn">예</button> 
					</div>
				</div>
 			</div>  
		</div>	
		<!-- e_개인일정 삭제 -->		
		
	</body>
</html>