<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script src="${JS}/lib/jquery.ui.accordion.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("div[name=titleDiv]").html('<span class="list">교육과정별 강의리스트</span>');		
				getMobileCurrLessonPlan();
			});
			
			function setAccordion(){
				$( ".accwrap" ).accordion({
					collapsible: true
				});
			}
			function getMobileCurrLessonPlan(){
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/st/main/currLessonList",
			        data: {
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
							var htmls = "";
							var pre_curr_seq = "";
							if(data.lessonPlanList.length == 0){
								$("#currLessonListAdd table").addClass("regix1");
							}else{
								$("#currLessonListAdd table").removeClass("regix1");
				            	$.each(data.lessonPlanList, function(index){
				            		var period = this.period.split(",");
				            		var periodName = "";
				            		var lesson_state = "";
				            		var lesson_subject = this.lesson_subject;
				            		var lesson_date = this.lesson_date.split("/");
				            		var ampm = "";
				            		if(this.ampm == "pm")
				            			ampm = "오후";
				            		else if(this.ampm == "am")
				            			ampm = "오전";
				            		
				            		//교시가 여러 교시 1,2,3 일경우
				            		if(period.length > 1)
				            			periodName = period[0]+"~"+period[period.length-1];
				            		else
				            			periodName = this.period;
				            		
				            		if(isEmpty(this.lesson_subject))
				            			lesson_subject = "수업주제 미입력";
				            		
				            		
				            		if(index != 0 && pre_curr_seq != this.curr_seq)
				            			htmls+='</div>';			            		
				            			
				            		if(pre_curr_seq != this.curr_seq){					            						            		
					            		htmls+='<div class="box"><button class="acc" name="curr_seq_'+this.curr_seq+'"><img src="${M_IMG}/m_cntr_act.png" alt="sign" class="sign"><span class="sp01">'+this.curr_name+'</span>';
					            		if(!isEmpty(this.curr_start_date)){
					            			currDate = this.curr_start_date+' ~ '+this.curr_end_date;
					            			if(this.period_cnt != 0)
					            				currDate += ' - '+this.period_cnt+'시수';
					            		}else{
					            			currDate = "과정기간 미입력";
					            			if(this.period_cnt != 0)
					            				currDate += ' - '+this.period_cnt+'시수';
					            		}
				            			htmls+='<span class="sp01_s">'+currDate+'</span></button>';
					            		
					            		if(this.asgmt_cnt != 0)
					            			htmls+='<span class="hw" onclick="getAsgmtView('+this.curr_seq+');">과제</span>';
					            			
					            		htmls+='</div><div class="panel acc1">';
					            		htmls+='<button type="button" class="btn_go1" onClick="getCurriculumView(\''+this.curr_seq+'\');">교육과정 계획서 바로가기<span class="sign">▶</span></button>';
				            		}
				            		
				            		if(this.lesson_state == "END" || this.lesson_state == "NULL")
				            			lesson_state = "box1"
				            		
				            		if(this.lp_seq != 0){
					            		htmls+='<div class="accwrap_s '+lesson_state+'" onClick="getLessonPlanView('+this.lp_seq+','+this.curr_seq+');">';  			            				
					            		htmls+='<div class="d_wrap1"><span class="date1">'+lesson_date[0]+'<span class="sp_ss">월</span></span>';
					            		htmls+='<span class="date2">'+lesson_date[1]+'<span class="sp_ss">일</span></span></div>';			            				
					            		htmls+='<div class="d_wrap2">';
					            		htmls+='<p class="p1">'+lesson_subject+'</p>';
					            		htmls+='<p class="p2"><span class="sp1">'+periodName+'교시</span><span class="sp2">'+ampm+' '+this.start_time+'~'+this.end_time+'</span>';
					            		if(this.fe_cnt != 0)
					            			htmls+='<span class="sp_quiz">형성평가</span>';
				            			
					            		htmls+='</p>';
					            		if(this.attendance_state == "00")
											htmls +='<span class="absent">결석</span>';
										else if(this.attendance_state == "02")
											htmls +='<span class="late">지각</span>';
										else if(this.attendance_state == "03")
											htmls +='<span class="attend">출석</span>';
					            		htmls+='</div></div>';
				            		}
				            					            		
				            		if(data.lessonPlanList.length == (index+1))
				            			htmls+='</div>';			            		
				            			
									pre_curr_seq = this.curr_seq;
				            	});
				            	
				            	$("#currLessonListAdd").html(htmls);
				            	setAccordion();
								
							}
			            	
			            } else {
			                alert("계획서 가져오기 실패.");
			            }
			        },
			        error: function(xhr, textStatus) {
			            //alert("오류가 발생했습니다.");
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function getAsgmtView(curr_seq){
				post_to_url("${M_HOME}/st/currAssignMent", { "curr_seq" : curr_seq});				
			}

			function getLessonPlanView(lp_seq, curr_seq) {
				post_to_url("${M_HOME}/st/lessonPlan", {"lp_seq" : lp_seq,"curr_seq" : curr_seq});
			}
			
			function getAttendanceListView(lp_seq, curr_seq) {
				post_to_url("${M_HOME}/st/attendance/list", { "lp_seq" : lp_seq, "curr_seq" : curr_seq});
			}
			
			function getCurriculumView(curr_seq){
				post_to_url("${M_HOME}/st/curriculum", {"curr_seq" : curr_seq});
			}
		</script>
	</head>
	<body>
		<!-- s_contents -->
		<div class="contents">	
			<!-- s_mtb_wrap accwrap-->
			<div class="mtb_wrap accwrap" id="currLessonListAdd">	
				<table class="tb_m1">

				</table>
			</div>
		</div>
		<!-- e_contents -->
	</body>
</html>