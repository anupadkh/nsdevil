<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta property="og:title" content="MLMS">
		<meta property="og:image" content="img/favicon.png">
		<meta property="og:description" content="의과대학 LMS">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no, maximum-scale=1, width=device-width" />
		<title>MLMS</title>
		
		<link rel="icon" type="img/png" href="${M_IMG}/favicon.png">
		<link rel="shortcut icon" href="${M_IMG}/favicon.png">
		<link rel="icon" href="${M_IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${M_IMG}/favicon.ico">
		<link rel="stylesheet" href="${CSS}/css_m3.css" type="text/css">
		<link rel="stylesheet" href="${CSS}/swiper.css">
		<link rel="stylesheet" href="${CSS}/dev_css_m3.css">
		<script src="${JS}/lib/jquery-1.11.1.js"></script>
		<script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.form.min.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/lib/swiper.js"></script>
		<script src="${JS}/mobileCommon.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				getQuizWait();
			});
			
			function getQuizWait() {
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/st/formationEvaluation/quiz/wait",
			        data: {
			        	"fe_seq" : "${fe_seq}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	$("#startQuizWaitArea").css("display", "none");
			            if (data.status == "200") {
			            	var quizInfo = data.quiz_info;
			            	var lessonSubject = quizInfo.lesson_subject;
			            	if ("${sessionScope.S_USER_PICTURE_PATH}" != "") {
			            		$("#stPicture").attr("src", "${sessionScope.S_USER_PICTURE_PATH}");
			            	}
			            	
			            	//헤더 형성평가 명
			            	$("#headerLessonSubject").html(quizInfo.fe_name);
			            	
			            	var quizThumbnail = quizInfo.thumbnail_path;
			            	if (quizThumbnail != "") {
				            	$("#quizThumbnailArea").html('<img class="preview" src="${RES_PATH}' + quizThumbnail + '" alt="형성평가 이미지 미리보기">');
			            	}
			            	
			            	//형성평가 명
			            	$("#lessonSubject").html(lessonSubject);
			            	
			            	//전체 문제 수
			            	var totalQuizCnt = quizInfo.quiz_cnt;
			            	$("#totalQuizCnt").html(totalQuizCnt);
			            	
			            	var feTestState = data.fe_test_state;
			            	var startQuizInfo = data.start_quiz_info;
			            	if (feTestState == "START") {
			            		var currQuizSeq = startQuizInfo.quiz_seq;
			            		var currQuizOrder = startQuizInfo.quiz_order;
			            		var nextQuizOrder = startQuizInfo.next_quiz_order;
			            		var quizStartCnt = startQuizInfo.quiz_start_cnt;
			            		var totalQuizCnt = startQuizInfo.quiz_cnt;
			            		var quizScoreCnt = startQuizInfo.quiz_score_cnt;
			            		var answerNumber = startQuizInfo.answer_number;
			            		if (quizStartCnt > 0 && quizScoreCnt > 0 && typeof nextQuizOrder != "undefined") { //다음 문제 대기
			            			$("#notice").html('<div class="guide2"><span class="num">'+currQuizOrder+'</span>번 문제를 풀었습니다.<br><br><span class="num">' + nextQuizOrder +'</span>번 문제는 교수님 코멘트를 듣고,<br><span class="st_sp02">다음문제</span>버튼을 눌러주세요.</div></div>');
			            			$("#reloadBtn").hide();
			            			$("#startQuizWaitArea").show();
			            			$("#currQuizOrder").html(currQuizOrder);
			            			$("#answerNumber").html(answerNumber);
			            		} else if (quizScoreCnt > 0 && typeof nextQuizOrder == "undefined") { //결과 대기
									$("#notice").html('<div class="guide1">[형성평가 결과를 대기해 주세요]<br>교수님의 형성평가 종료 안내를 확인하신 뒤,<br>새로고침을 버튼을 누르시면<br>본인의 점수를 확인하실 수 있습니다.</div>');
			            		} else { //시작
			            			getStartQuizInfo();
			            		}
			            	} else if (feTestState == "END") {
			            		getQuizResultView();
			            	} else {
			            		$("#notice").html('<div class="guide1">형성평가가 시작되지 않았습니다.<br><br>교수님께서 시작되었다는 안내가 있을 경우, <br>「새로고침」 버튼을 눌러주세요.</div>');			            		
			            	}
			            	
			            } else {
			            	alert("형성평가 불러오기에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        timeout: 60000,
			        error: function(xhr, textStatus) {
			        	errorCase(textStatus);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function getStartQuizInfo() {
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/st/formationEvaluation/quiz/state",
			        data: {
			        	"fe_seq" : "${fe_seq}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	var quizInfo = data.quiz_info;
			            	if (quizInfo != "") {
			            		var quizSeq = quizInfo.quiz_seq;
			            		post_to_url("${M_HOME}/st/formationEvaluation/quiz/detail", {"fe_seq":"${fe_seq}", "quiz_seq":quizSeq});
			            	}
			            } else {
			            	alert("형성평가 불러오기에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        timeout: 60000,
			        error: function(xhr, textStatus) {
			        	errorCase(textStatus);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function getFormationEvaluationListView() {
				post_to_url("${M_HOME}/st/formationEvaluation/list", {"lp_seq":"${sessionScope.S_LP_SEQ}", "curr_seq" : "${sessionScope.S_CURRICULUM_SEQ}"});
			}
			
			function getQuizResultView() {
				post_to_url("${M_HOME}/st/formationEvaluation/quiz/result", {"fe_seq":"${fe_seq}"});
			}
		</script>
	</head>
	<body class="color1 bd_quiz">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
		<div id="skipnav">
			<a href="#gnb_q">주 메뉴 바로가기</a>
			<a href="#container">컨텐츠 바로가기</a>
		</div>
		<div id="wrap">
			<header class="header">
				<div class="wrap">
					<div class="icpt"><img src="${DEFAULT_PICTURE_IMG}" alt="사진 이미지" id="stPicture"></div>
					<div class="top_date quiz">
						<span class="sp_q1" id="headerLessonSubject"></span>
					</div>
					<div class="wrap_goquiz"><div class="goquiz" title="형성평가 닫기" onclick="javascript:getFormationEvaluationListView();"></div></div>
				</div>
			</header>
			<div id="container" class="container_table">
				<div class="contents">
					<div class="ptb_wrap">
						<div class="pr_quiz">
							<div class="preview_wrap" id="quizThumbnailArea"></div>
							<div class="b_wrap">
								<div id="gnb_q" class="tt_wrap">
									<div class="title" id="lessonSubject"></div>
									<div class="tt"><span class="sp_t">전체 문제 수</span><span class="sp_n" id="totalQuizCnt">0</span><span class="sp_un">문항</span></div>
									<button id="reloadBtn" class="renew_wrap" onClick="javascript:getQuizWait();"><span class="renew">새로고침</span></button>
									<div id="notice"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="go_wrap" id="startQuizWaitArea" style="display: none;">                   
			<div class="pop_btn_wrap">
				<table class="as_wrap">
				     <tr>
				         <td class="as_td1"><span class="num" id="currQuizOrder"></span></td>
				         <td class="as_td2">번 문제</td>
				         <td rowspan="2" class="as_td3" id="answerNumber"></td>
				         <td rowspan="2" class="as_td4">번</td>
				     </tr>
				     <tr>
				         <td colspan="2" class="as_td1">제출답안</td>
				     </tr>
				</table>
				<button class="fgo" onclick="getQuizWait();">다음문제</button>            
			</div>
		</div>
		
	</body>
</html>