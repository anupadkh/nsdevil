<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function() {
				$("div[name='titleDiv']").addClass("cc");
				getFormationEvaluationList();
			});
			
			function getFormationEvaluationList() {
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/st/formaationEvaluation/list",
			        data: {},
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	titleSetting(data.lesson_plan_info);
			            	feListSetting(data.formation_evaluation_list);
			            } else {
			            	alert("형성평가 불러오기에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        timeout: 60000,
			        error: function(xhr, textStatus) {
			        	errorCase(textStatus);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function titleSetting(lessonPlanInfo) {
				//교시
				var periodStr = lessonPlanInfo.period; //교시
				var periodList = periodStr.split(',');
				var startPeriod = periodList[0];
				if (periodList.length > 1) {
					periodStr = startPeriod + "~" + periodList[periodList.length-1] + "교시";  
				} else {
					periodStr = startPeriod + "교시";
				}
				
            	//수업일
            	var lessonDate = lessonPlanInfo.lesson_date;
            	lessonDate = lessonDate.replace("-", "월 ") + "일";
				
            	//수업시간
				var ampmText = lessonPlanInfo.ampm;
        		if(ampmText == "pm"){
        			ampmText = "오후";
        		} else {
        			ampmText = "오전";
        		}
        		
        		var startTime = lessonPlanInfo.start_time;
				var endTime = lessonPlanInfo.end_time;
            	
        		var lessonPlanDateTitle = lessonDate + " " + periodStr;//mm월 xx일 x교시
        		var lessonPlanTimeTitle = ampmText + " " + startTime + "~" + endTime;//오전/오후 xx:xx~xx:xx
        		
            	var titleHtml = '<span class="cc_s1">' + lessonPlanDateTitle + '</span><span class="cc_s2">(' + lessonPlanTimeTitle + ')</span>';
            	var titleDiv = $("div[name='titleDiv']").addClass("cc").html(titleHtml);
            	
            	//교육과정 명
            	var currName = lessonPlanInfo.curr_name;
            	
            	//수업계획서 명
            	var lessonSubject = lessonPlanInfo.lesson_subject;
            	
            	var pfName = lessonPlanInfo.name;
            	var pfDepartment = lessonPlanInfo.code_name;
            	var pfInfo = '<span class="ts1">교수</span>';
            	
				if(isEmpty(pfName)){
					pfInfo+='<span class="ts2">미배정</span>';
				}else{
					pfInfo+='<span class="ts2">'+pfName+'</span>'
					+ '<span class="sign">(</span><span class="ts2">'+pfDepartment+'</span><span class="sign">)</span></span>';
				}
            	
            	$("#currName").html(currName+"<br>"+lessonSubject);
            	$("#pfInfo").html(pfInfo);
            	var attendanceState = lessonPlanInfo.attendance_state;
            	
            	if (attendanceState == "WAIT") {
            		attendanceState = '<span class="cc_t3">출석체크 전</span>';
				} else if (attendanceState == "00") {
            		attendanceState = '<span class="absent">결석</span>';
				} else if (attendanceState == "03") {
					attendanceState = '<span class="attend">출석</span>';
				} else if (attendanceState == "02") {
					attendanceState = '<span class="late">지각</span>';
				} else {
					attendanceState = '';
				}
            	if (attendanceState != '') {
            		$("#pfInfo").after(attendanceState);
            	}
			}
			
			function feListSetting(feList) {
				var feListAreaHtml = "";
				$(feList).each(function() {
					var feSeq = this.fe_seq;
					var cateCode = this.fe_cate_code;
					var feName = this.fe_name;
					if (feName.length >= 18) {
						feName = feName.substring(0, 18)+"...";
					}
        			var testState = this.test_state;
        			var quizTypeName = this.quiz_type;
        			var quizTotalCnt = this.quiz_total_cnt;
        			var solveEndQuizCnt = this.solve_end_quiz_cnt;
        			var feScore = this.fe_score;
        			var avgScore = this.avg_score;
        			var absenceFlag = this.absence_flag;

        			if (cateCode == "01") {
    					if (testState == "WAIT") {
    						feListAreaHtml += '<tr class="box_wrap">';
    						feListAreaHtml += '	<td class="box_s">';
    						feListAreaHtml += '		<div class="wrap_s prg1">';
    						feListAreaHtml += '			<div class="wrap_ss">';
    						feListAreaHtml += '				<div class="tt1">' + feName + '</div>';
    						feListAreaHtml += '				<div class="prg_t">대기</div>';
    						feListAreaHtml += '			</div>';
    						feListAreaHtml += '			<div class="tt3"><span class="prg_s1">대기</span><span class="sp_wrap"><span class="sign">(</span><span class="num1">' + quizTotalCnt + '</span><span class="sign">문제 )</span></span></div>';
    						feListAreaHtml += '			<div class="quiz_wrap_sc">';
    						feListAreaHtml += '				<div class="wrap_s">';
    						feListAreaHtml += '					<button class="quiz_sc" onClick="javascript:getQuizWaitView(' + feSeq + ', ' + quizTotalCnt +');">시작하기</button>';
    						feListAreaHtml += '				</div>';
    						feListAreaHtml += '			</div>';
    						feListAreaHtml += '		</div>';
    						feListAreaHtml += '	</td>';
    						feListAreaHtml += '</tr>';
    					} else if (testState == "START") {
    						feListAreaHtml += '<tr class="box_wrap">';
    						feListAreaHtml += '	<td class="box_s">';
    						feListAreaHtml += '		<div class="wrap_s prg2">';
    						feListAreaHtml += '			<div class="wrap_ss">';
    						feListAreaHtml += '				<div class="tt1">' + feName + '</div>';
    						feListAreaHtml += '				<div class="prg_t">진행중</div>';
    						feListAreaHtml += '			</div>';
    						feListAreaHtml += '			<div class="tt3"><span class="prg_s1">진행 중</span><span class="sp_wrap"><span class="num1">' + solveEndQuizCnt + '</span><span class="sign">/</span><span class="num1">' + quizTotalCnt + '</span></span></div>';
    						feListAreaHtml += '			<div class="quiz_wrap_sc">';
    						feListAreaHtml += '				<div class="wrap_s">';
							feListAreaHtml += '					<button class="quiz_sc" onClick="javascript:getQuizWaitView(' + feSeq + ', ' + quizTotalCnt +');">시작하기</button>';
    						feListAreaHtml += '				</div>';
    						feListAreaHtml += '			</div>';
    						feListAreaHtml += '		</div>';
    						feListAreaHtml += '	</td>';
    						feListAreaHtml += '</tr>';
    					} else if (testState == "END") {
    						feListAreaHtml += '<tr class="box_wrap">';
    						feListAreaHtml += '	<td class="box_s">';
    						feListAreaHtml += '		<div class="wrap_s prg3">';
    						feListAreaHtml += '			<div class="wrap_ss">';
    						feListAreaHtml += '				<div class="tt1">' + feName + '</div>';
    						feListAreaHtml += '				<div class="prg_t">종료</div>';
    						feListAreaHtml += '			</div>';
    						feListAreaHtml += '			<div class="tt3"><span class="prg_s1">종료</span><span class="sp_wrap"><span class="sign">(</span><span class="num1">' + quizTotalCnt + '</span><span class="sign">문제 -</span><span class="num1">' + quizTotalCnt + '</span><span class="sign">점 만점 )</span></span></div>';
    						feListAreaHtml += '			<div class="quiz_wrap_sc" onclick="javascript:getQuizResultView(' + feSeq + ');">';
    						feListAreaHtml += '				<div class="wrap_s">';
    						feListAreaHtml += '					<span class="quiz_sc">';
   							feListAreaHtml += '						<span>나의점수</span><span class="sp1">' + feScore + '</span><span class="sp0">점</span>';
  							feListAreaHtml += '					</span>';
							feListAreaHtml += '					<span class="quiz_sc">';
							feListAreaHtml += '					    <span>평균</span><span class="sp3">' + avgScore + '</span><span class="sp0">점</span>';
							feListAreaHtml += '					</span>';
    						feListAreaHtml += '				</div>';
    						feListAreaHtml += '			</div>';
    						feListAreaHtml += '		</div>';
    						feListAreaHtml += '	</td>';
    						feListAreaHtml += '</tr>';
    					}
        			}
				});
				
				if (feList.length > 0) {
					$("#feListArea").html(feListAreaHtml);
				} else {
					//등록된 온라인 형성평가가 없습니다.
					//TODO 디자인 없음
					$("#feListArea").html("");
				}
			}
			
			
			function getQuizWaitView(feSeq, quizCnt) {
				if (quizCnt > 0) {
					post_to_url("${M_HOME}/st/formationEvaluation/quiz/wait", {"fe_seq":feSeq});
				}
			}
			
			function getCurriculumView(){
				post_to_url("${M_HOME}/st/curriculum", {"curr_seq" : "${sessionScope.S_CURRICULUM_SEQ}"});
			}
			
			function getFormationEvaluationListView() {
				post_to_url("${M_HOME}/st/formationEvaluation/list", {"lp_seq":"${sessionScope.S_LP_SEQ}", "curr_seq":"${sessionScope.S_CURRICULUM_SEQ}"});
			}
			
			function getAttendanceListView() {
				post_to_url("${M_HOME}/st/attendance/list", {"lp_seq" : "${sessionScope.S_LP_SEQ}", "curr_seq":"${sessionScope.S_CURRICULUM_SEQ}"});
			}
		
			function getLessonPlanView() {
				post_to_url("${M_HOME}/st/lessonPlan", {"lp_seq" : "${sessionScope.S_LP_SEQ}", "curr_seq" : "${sessionScope.S_CURRICULUM_SEQ}"});
			}
			
			function getAssignMentListView(){
				post_to_url("${M_HOME}/st/assignMent", {"lp_seq":"${sessionScope.S_LP_SEQ}"});
			}
			
			function getQuizResultView(feSeq) {
				post_to_url("${M_HOME}/st/formationEvaluation/quiz/result", {"fe_seq":feSeq});
			}
		</script>
	</head>
	<body>
		<div class="contents">
			<div class="top_tt">
				<span class="cc_t1" id="currName"></span>
				<span class="cc_t2" id="pfInfo"></span>
			</div>
			<div class="mn_wrap quiz">
				<div class="wrap_s">
					<span class="cc_mn cc_mn01" onClick="javascript:getCurriculumView();">교육과정</span>
					<span class="cc_mn cc_mn02" onClick="javascript:getLessonPlanView();">수업계획</span>
					<span class="cc_mn cc_mn03" onClick="location.href='${M_HOME}/st/lessonData'">자료</span>
					<span class="cc_mn cc_mn04 on" onClick="javascript:getFormationEvaluationListView();">형성평가</span>
					<span class="cc_mn cc_mn05" onClick="location.href='${M_HOME}/st/assignMent'">과제</span>
				</div>
			</div>
			<div class="ptb_wrap">	
				<table class="tb_quiz" id="feListArea"></table>
			</div>	
		</div>
	</body>
</html>