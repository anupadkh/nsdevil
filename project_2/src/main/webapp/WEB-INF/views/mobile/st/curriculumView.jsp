<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			var pfPage = 1;
			$(document).ready(function() {
				getCurriculum();
				getCurriculumPFList(pfPage);
			});
		
			//교육과정 가져오기			
			function getCurriculum() {
				$.ajax({
					type : "POST",
					url : "${M_HOME}/ajax/st/curriculum/list",
					data : {},
					dataType : "json",
					success : function(data, status) {
						if (data.status == "200") {
							titleSetting(data.lesson_plan_info);
							var htmls = "";
							$("#curr_name").text(data.curriculumInfo.curr_name);
							$("#aca_system_name").text("(" + data.curriculumInfo.aca_system_name + ")");
							$("#curr_code").text(data.curriculumInfo.curr_code);
							$("#curr_name").text(data.curriculumInfo.curr_name);
							$("#aca_name").text(data.curriculumInfo.aca_name);
							$("#aca_system_name").text(data.curriculumInfo.aca_system_name);
							$("#complete_name").text(data.curriculumInfo.complete_name);
							$("#administer_name").text(data.curriculumInfo.administer_name);
							$("#grade").text(data.curriculumInfo.grade);
							$("#target_name").text(data.curriculumInfo.target_name);
							$("#lesson_date").html('<span class="sp_tt1">'+data.curriculumInfo.curr_start_date+'<span class="sign">~</span>'+data.curriculumInfo.curr_end_date+'</span>');
							
							if(isEmpty(data.curriculumInfo.curr_summary))
								$(".curr_summary").hide();
							if(isEmpty(data.curriculumInfo.reference_data))
								$(".reference_data").hide();
							if(isEmpty(data.curriculumInfo.management_plan))
								$(".management_plan").hide();
							if(isEmpty(data.curriculumInfo.curr_outcome))
								$(".curr_outcome").hide();
							
							$("#curr_summary").html(data.curriculumInfo.curr_summary);
							$("#reference_data").html(data.curriculumInfo.reference_data);
							$("#management_plan").html(data.curriculumInfo.management_plan);
							$("#curr_outcome").html(data.curriculumInfo.curr_outcome);
							
							var grade_method_name = "";
							if(data.curriculumInfo.grade_method_code == "00")
								grade_method_name = "이용안함";
							else if(data.curriculumInfo.grade_method_code == "01")
	                                  grade_method_name = "상대평가";
							else if(data.curriculumInfo.grade_method_code == "02")
	                                  grade_method_name = "절대평가";
							$("#grade_method_name").text(grade_method_name);					

							//주당평균시간 구하기
							var period_avg = (data.curriculumInfo.total_period / data.curriculumInfo.curr_week) + "";
							if (period_avg.indexOf(".") > 0)
								period_avg = period_avg.substring(0, period_avg.indexOf(".") + 2);

							$("#period_avg").text(period_avg);
							$("#lecture_cnt").text(data.curriculumInfo.lecture_period);
							$("#unlecture_cnt").text(data.curriculumInfo.unlecture_period);
							$("#lp_cnt").text(data.curriculumInfo.period_cnt);
							$("#etc_period").text(data.curriculumInfo.etc_period);
							$("#total_period_cnt").text(data.curriculumInfo.total_period);
							$("#lesson_week").text(data.curriculumInfo.curr_week);
																					
							htmls = "";	
							
							//비강의 가져오기
							$.each(data.lessonMethodList, function(index) {
								//비강의 수업 있는 거만 뿌려준다.
								if(this.lp_cnt > 0)
									htmls+='<div class="td_sbox"><div class="tt">'+this.code_name+'</div><div class="con">'+this.lp_cnt+'</div></div>';
							});
							$("#lessonMethodList").html(htmls);

							//형성평가 리스트        		
							htmls = '';
							var fe_sum = 0;
							var fe_name_arry = new Array();
							var fe_cnt_arry = new Array();

							$.each(data.formationEvaluationCs, function(index) {
								fe_name_arry[index] = this.code_name
								fe_cnt_arry[index] = this.cnt;
								fe_sum += fe_cnt_arry[index] * 1;
							});

							for_int = 0;
							if(fe_name_arry.length <= 2)
								for_int = 1;
							else if ((fe_name_arry.length) % 2 == 0)
								for_int = fe_name_arry.length / 2;
							else
								for_int = parseInt((fe_name_arry.length + 1) / 2) + 1;

							for (var i = 0; i < for_int * 2; i += 2) {
								htmls += '<tr class="tr01">';
								if (fe_name_arry.length > i){
									htmls += '<td class="w7"><div class="td_box2 box"><span class="sp03">' + fe_name_arry[i] + '</span>';
									htmls += '<span class="sign">(</span><span class="sp_num">'+fe_cnt_arry[i]+'</span><span class="sign">)</span></div></td>';
								}
								else{
									htmls += '<td class="w7"><div class="td_box2 box"><span class="sp03"></span>';
									htmls += '<span class="sign">(</span><span class="sp_num"></span><span class="sign">)</span></div></td>';
								}
								
								if (fe_name_arry.length > (i + 1)){
									htmls += '<td class="w7"><div class="td_box2 box"><span class="sp03">' + fe_name_arry[i+1] + '</span>';
									htmls += '<span class="sign">(</span><span class="sp_num">'+fe_cnt_arry[i+1]+'</span><span class="sign">)</span></div></td>';
								}
								else{
									htmls += '<td class="w7"><div class="td_box2 box"><span class="sp03"></span>';
									htmls += '<span class="sign">(</span><span class="sp_num"></span><span class="sign">)</span></div></td>';
								}
								
								//맨처음거만 합계 넣어준당.
								if(i ==0){
									htmls += '<td rowspan="'+for_int+'" class="sum">';
									htmls += '<span class="sp03">총</span>';
									htmls += '<span class="sp_num">'+fe_sum+'</span><span class="sp03">회</span>';
									htmls += '</td>';
								}
								htmls += '</tr>';
									
							}
							$("#feAdd").html(htmls);
							htmls = '';
							 
							//총괄평가기준
							$.each(data.summativeEvaluationCs,function(index) {
								htmls += '<table class="prcs tb2">';
								htmls += '<tbody>';
								htmls += '<tr>';
								htmls += '<td class="w7">';
								htmls += '<div class="td_wrap">' + this.eval_method_name + '</div>';
								htmls += '</td>';
								htmls += '<td class="w7">';
								htmls += '<div class="td_wrap">' + this.eval_domain_name + '</div>';
								htmls += '</td>';
								htmls += '<td class="w8">';
								htmls += '<div class="td_wrap">';
								htmls += '<span class="sp_num_s">' + this.importance + '</span><span class="sign">%</span>';
								htmls += '</div>';
								htmls += '</td>';
								htmls += '</tr>';
								if(!isEmpty(this.eval_method_text) || !isEmpty(this.eval_etc)){									
									htmls += '<tr>';
									htmls += '<td colspan="3" class="w0">';
									htmls += '<div class="td_box">';			 
									htmls += '<div class="td_sbox">';
									if(!isEmpty(this.eval_method_text))
										htmls += '<div class="tt">'+ this.eval_method_text + '</div>';
									if(!isEmpty(this.eval_etc))
										htmls += '<div class="con"><span class="note">비고</span>' + this.eval_etc	+ '</div>';
									htmls += '</div>';
									htmls += '</div>';
									htmls += '</td>';
									htmls += '</tr>';     
								}   
								htmls += '</tbody>';
								htmls += '</table>';
							});
							$("#AddEvalList").after(htmls);
							htmls = '';

							//졸업역량
							$.each(data.mpCurriculumGraduationCapabilityList, function(index) {
								var fc_name = '('+this.fc_code+') '+ this.fc_name;
								
								if(isEmpty(this.fc_code))
									fc_name=this.fc_name;
								htmls += '<table class="prcs tb2">';
								htmls += '<tbody>';
								htmls += '<tr>';
								htmls += '<td colspan="2">';
								htmls += '<div class="td_wrap w0">'+fc_name+ '</div>';
								htmls += '</td>';
								htmls += '</tr>';
								htmls += '<tr>';
								htmls += '<td class="w11">';
								htmls += '<div class="td_wrap">' + this.teaching_method + '</div>';
								htmls += '</td>';
								htmls += '<td class="w11">';
								htmls += '<div class="td_wrap">' + this.evaluation_method + '</div>';
								htmls += '</td>';
								htmls += '</tr>';
								htmls += '<tr>';
								htmls += '<td colspan="2" class="w0">';
								htmls += '<div class="td_box">';			 
								htmls += '<div class="td_sbox">';
								htmls += '<div class="tt">' + this.process_result + '</div>';
								htmls += '</div>';
								htmls += '</div>';
								htmls += '</td>';
								htmls += '</tr>';      
								htmls += '</tbody>';
								htmls += '</table>';								
							});

							$("#mpFinishCapabilityList").after(htmls);							

							if(data.mpCurriculumGraduationCapabilityList.length==0)
								$("#mpFinishCapabilityList").hide();
/* 
							//학점 기준
	                        var htmls1='<tr class="tr01">';
	                        var htmls2='<tr class="tr01">';
	                        var sumGrade = 0;

	                        $.each(data.gradeStandard, function(index){
	                        	htmls1+='<td class="th01 w9"><div class="td_wrap1">'+this.code_name+'</div></td>';
	                            htmls2+='<td class="w9"><div class="td_wrap1">'+this.grade_importance+'%</div></td>';
	                            sumGrade += this.grade_importance;
	                        });
	                        htmls1+='<td class="th01 w10"><div class="td_wrap1">계</div></td></tr>';
	                        htmls2+='<td class="w10"><div class="td_wrap1">'+sumGrade+'%</div></td></tr>';
	                        $("#gradeStandardList").html(htmls1+htmls2);
	                         */
						} else {

						}
					},
					error : function(xhr, textStatus) {
						document.write(xhr.responseText);
					},
					beforeSend : function() {
						$.blockUI();
					},
					complete : function() {
						$.unblockUI();
					}
				});
			}
			
			function getCurriculumPFList(page){
				$.ajax({
					type : "POST",
					url : "${M_HOME}/ajax/pf/curriculum/PFlist",
					data : {
						"page" : page
					},
					dataType : "json",
					success : function(data, status) {
						if (data.status == "200") {
							if(data.pfPageCnt == 1){
								$("div.pagination").hide();
							}

							$("#startNum").text(page);
							$("#endNum").text(data.pfPageCnt);
							$("#pfCnt").text(data.pfCnt);
							//교수 추가
							$("#pfListAdd").empty();
							var pf_level = "";
							var htmls = "";
							$.each(data.curriculumPFList,function() {
								if(this.professor_level == "1")
									pf_level = "책임";
								else if(this.professor_level == "2")
									pf_level = "부책임";
								else if(this.professor_level == "3")
									pf_level = "교수";
								htmls += '<tr>';
								htmls += '<td class="w4"><span class="tt_box'+this.professor_level+'">'+pf_level+'</span></td>';
								htmls += '<td class="w5_1"><span class="sp02">'+this.name+'</span><span class="sp02">('+this.code_name+')</span></td>';
								htmls += '<td class="w5_1"><span class="sp02">'+this.specialty+'</span></td>';
								htmls += '<td class="w5_2"><span class="sp02">'+this.email+'</span></td>';
								htmls += '</tr>';								
							});
							$("#pfListAdd").html(htmls);				
	                        
						} else {
							alert(data.status);
						}
					},
					error : function(xhr, textStatus) {
						document.write(xhr.responseText);
					},
					beforeSend : function() {
						$.blockUI();
					},
					complete : function() {
						$.unblockUI();
					}
				});
			}
			
			function nextPfList(){
				pfPage++;
				if(pfPage > $("#endNum").text()){
					alert("마지막 페이지 입니다.");
					pfPage--;
				}else{
					getCurriculumPFList(pfPage);
				}				
			}
			
			function prePfList(){
				pfPage--;
				if(pfPage == 0){
					alert("맨 처음 페이지 입니다.");
					pfPage++;
				}else{
					getCurriculumPFList(pfPage);
				}
			}
		
			function titleSetting(lessonPlanInfo) {
				if(!isEmpty(lessonPlanInfo)){
					//교시				
					var periodStr = lessonPlanInfo.period; //교시
					var periodList = periodStr.split(',');
					var startPeriod = periodList[0];
					if (periodList.length > 1) {
						periodStr = startPeriod + "~" + periodList[periodList.length - 1] + "교시";
					} else {
						periodStr = startPeriod + "교시";
					}
			
					//수업일
					var lessonDate = lessonPlanInfo.lesson_date;
					lessonDate = lessonDate.replace("-", "월 ") + "일";
			
					//수업시간
					var ampmText = lessonPlanInfo.ampm;
					if (ampmText == "pm") {
						ampmText = "오후";
					} else {
						ampmText = "오전";
					}
			
					var startTime = lessonPlanInfo.start_time;
					var endTime = lessonPlanInfo.end_time;
			
					var lessonPlanDateTitle = lessonDate + " " + periodStr;//mm월 xx일 x교시
					var lessonPlanTimeTitle = ampmText + " " + startTime + "~" + endTime;//오전/오후 xx:xx~xx:xx
			
					var titleHtml = '<span class="cc_s1">' + lessonPlanDateTitle 
						+ '</span><span class="cc_s2">(' + lessonPlanTimeTitle
						+ ')</span>';
					var titleDiv = $("div[name='titleDiv']").addClass("cc").html(titleHtml);
					
					var lessonInfoHtml = '<span class="cc_t1">'+lessonPlanInfo.lesson_subject+'</span>'
						+ '<span class="cc_t2"><span class="ts1">교수</span>';

						if(isEmpty(lessonPlanInfo.name)){
							lessonInfoHtml+='<span class="ts2">미배정</span>';
						}else{
							lessonInfoHtml+='<span class="ts2">'+lessonPlanInfo.name+'</span>'
							+ '<span class="sign">(</span><span class="ts2">'+lessonPlanInfo.code_name+'</span><span class="sign">)</span></span>';
						}
					
					if(lessonPlanInfo.attendance_state == "WAIT")
						lessonInfoHtml +='<span class="cc_t3">출석체크 전</span>';
					else if(lessonPlanInfo.attendance_state == "00")
						lessonInfoHtml +='<span class="absent">결석</span>';
					else if(lessonPlanInfo.attendance_state == "02")
						lessonInfoHtml +='<span class="late">지각</span>';
					else if(lessonPlanInfo.attendance_state == "03")
						lessonInfoHtml +='<span class="attend">출석</span>';
					else
						lessonInfoHtml +='<span class="cc_t3">출석 미입력</span>';
							
					$("#lessonInfo").html(lessonInfoHtml);
				}else{
					$("div[name='titleDiv']").addClass("cc").html('<span class="cc_s1">강의 정보 없음</span>');
				}
			}
		
			function getFormationEvaluationListView() {
				post_to_url("${M_HOME}/st/formationEvaluation/list", {"lp_seq" : "${sessionScope.S_LP_SEQ}", "curr_seq":"${sessionScope.S_CURRICULUM_SEQ}"});
			}
		
			function getAttendanceListView() {
				post_to_url("${M_HOME}/st/attendance/list", { "lp_seq" : "${sessionScope.S_LP_SEQ}", "curr_seq":"${sessionScope.S_CURRICULUM_SEQ}"});
			}
		
			function getLessonPlanView() {
				post_to_url("${M_HOME}/st/lessonPlan", {"lp_seq" : "${sessionScope.S_LP_SEQ}","curr_seq" : "${sessionScope.S_CURRICULUM_SEQ}"});
			}
		
			function getAssignMentListView() {
				post_to_url("${M_HOME}/st/assignMent", {"lp_seq" : "${sessionScope.S_LP_SEQ}"});
			}

			function getCurriculumView(){
				post_to_url("${M_HOME}/st/curriculum", {"curr_seq" : "${sessionScope.S_CURRICULUM_SEQ}"});
			}
			
		</script>
	</head>
	<body class="color1">
		<input type="hidden" name="group_flag">
		<input type="hidden" name="asgmt_seq">
		<!-- s_contents -->
		<div class="contents">
	
			<!-- s_top_tt-->
			<div class="top_tt" id="lessonInfo">
			</div>
			<!-- e_top_tt -->
	
			<!-- s_mn_wrap -->
			<div class="mn_wrap syllabus">
				<div class="wrap_s">
		
					<span class="cc_mn cc_mn01 on" onClick="javascript:getCurriculumView();">교육과정</span>
				<c:if test="${null ne S_LP_SEQ}">					
				<span class="cc_mn cc_mn02" onClick="javascript:getLessonPlanView();">수업계획</span>
					<span class="cc_mn cc_mn03" onClick="location.href='${M_HOME}/st/lessonData'">자료</span>
					<span class="cc_mn cc_mn04" onClick="getFormationEvaluationListView();">형성평가</span>
					<span class="cc_mn cc_mn05" onClick="location.href='${M_HOME}/st/assignMent';">과제</span>
				</c:if>
				<c:if test="${null eq S_LP_SEQ}">
					<span class="cc_mn cc_mn02" onClick="alert('선택된 수업계획서가 없습니다.');">수업계획</span>
					<span class="cc_mn cc_mn03" onClick="alert('선택된 수업계획서가 없습니다.');">자료</span>
					<span class="cc_mn cc_mn04" onClick="alert('선택된 수업계획서가 없습니다.');">형성평가</span>
					<span class="cc_mn cc_mn05" onClick="alert('선택된 수업계획서가 없습니다.');">과제</span>				
				</c:if>
				</div>
			</div>
			<!-- e_mn_wrap -->
	
			<!-- s_ptb_wrap -->
			<div class="ptb_wrap prcs_wrap">
	
				<!-- s_교육과정 기본 정보 -->
				<div class="prcs_title">교육과정 기본 정보</div>
				<table class="prcs tb1">
					<tbody>
						<tr>
							<td class="th01 w1">
								<div class="td_wrap">교육<br>과정<br>코드</div>
							</td>
							<td class="w2"><span class="sp01" id="curr_code"></span></td>
							<td class="th01 w1">
								<div class="td_wrap">교육<br>과정명</div>
							</td>
							<td class="w2"><span class="sp01" id="curr_name"></span></td>
						</tr>
						<tr>
							<td class="th01">
								<div class="td_wrap">
									개설<br>학기
								</div>
							</td>
							<td class="w2"><span class="sp01" id="aca_name"></span></td>
							<td class="th01">
								<div class="td_wrap">장소</div>
							</td>
							<td class="w2"><span class="sp_tt1" id="classroom"></span></td>
						</tr>
						<tr>
							<td class="th01 w1">
								<div class="td_wrap">
									학사<br>체계
								</div>
							</td>
							<td colspan="3" class="w3"><span class="sp_tt" id="aca_system_name"></span></td>
						</tr>
						<tr>
							<td class="th01 w1">
								<div class="td_wrap">이수<br>구분</div>
							</td>
							<td class="w2"><span class="sp_tt1" id="complete_name"></span></td>
							<td class="th01 w1">
								<div class="td_wrap">관리<br>구분</div>
							</td>
							<td class="w2"><span class="sp_tt1" id="administer_name"></span></td>
						</tr>
						<tr>
							<td class="th01 w1">
								<div class="td_wrap">대상</div>
							</td>
							<td class="w2"><span class="sp_tt1" id="target_name"></span></td>
							<td class="th01 w1">
								<div class="td_wrap">학점</div>
							</td>
							<td class="w2"><span class="sp_num" id="grade"></span></td>
						</tr>
						<!-- <tr>
							<td class="th01 w1">
								<div class="td_wrap">실습</div>
							</td>
							<td colspan="3" class="w3"><span class="sp_tt1">PBL,
									CPX</span></td>
						</tr> -->
					</tbody>
				</table>
				<!-- e_교육과정 기본 정보 -->
	
				<!-- s_교수자 정보 -->
				<div class="prcs_title">
					교수자 정보 <span class="tt_s">총<span class="num" id="pfCnt"></span>명
					</span>
				</div>
				<table class="prcs tb1">
					<tbody id="pfListAdd">
						
					</tbody>
				</table>
	
				<!-- s_pagination_s1 -->
				<div class="pagination s1">
					<ul>
						<li><a href="#" title="이전" class="arrow ba" onClick="prePfList();"></a></li>
						<li><a href="#" class="active" id="startNum"></a></li>
						<li>/</li>
						<li><a href="#" id="endNum"></a></li>
						<li><a href="#" title="다음" class="arrow na" onClick="nextPfList();"></a></li>
					</ul>
				</div>
				<!-- e_pagination_s1 -->
	
				<!-- s_학습기간 및 시간관리 -->
				<div class="prcs_title">학습기간 및 시간관리</div>
				<table class="prcs tb1">
					<tbody>
						<tr>
							<td class="th01 w1">
								<div class="td_wrap">기간</div>
							</td>
							<td colspan="5" class="w3" id="lesson_date"></td>
						</tr>
						<tr>
							<td class="th01 w1">
								<div class="td_wrap">주수</div>
							</td>
							<td class="w2_1"><span class="sp_num" id="lesson_week"></span></td>
							<td class="th01 w1">
								<div class="td_wrap">시수 외</div>
							</td>
							<td class="w6_1"><span class="sp_num_s" id="etc_period"></span></td>
							<td class="th01 w1">
								<div class="td_wrap">총 시수</div>
							</td>
							<td class="w6_1"><span class="sp_num_s" id="total_period_cnt"></span></td>
						</tr>
						<tr>
							<td class="th01 w1">
								<div class="td_wrap">
									주당<br>평균<br>시수
								</div>
							</td>
							<td class="w2_1">
								<span class="sp_num" id="period_avg"></span>
								<span class="sp03">시간</span>
								<span class="sign">/</span>
								<span class="sp_num"></span>
								<span class="sp03">주</span>
							</td>
							<td class="th01 w1">
								<div class="td_wrap">차시</div>
							</td>
							<td colspan="3" class="w2_2"><span class="sp_num_s" id="lp_cnt"></span></td>
						</tr>
						<tr>
							<td class="th01 w1">
								<div class="td_wrap">강의</div>
							</td>
							<td class="w2_1"><span class="sp_num_s" id="lecture_cnt"></span><span class="sp03">시간</span></td>
							<td class="th01 w1">
								<div class="td_wrap">비강의</div>
							</td>
							<td colspan="3" class="w2_2"><span class="sp_num_s" id="unlecture_cnt"></span><span class="sp03">시간</span></td>
						</tr>
						<tr>
							<td colspan="6" class="th01">
								<div class="td_wrap">비강의 상세</div>
							</td>
						</tr>
						<tr>
							<td colspan="6" class="w0">
								<div class="td_box" id="lessonMethodList">
									
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				<!-- e_학습기간 및 시간관리 -->
	
				<!-- s_형성평가 실시 여부 및 횟수 -->
				<div class="prcs_title">형성평가 실시 여부 및 횟수</div>
				<table class="prcs bd0">
					<tbody id="feAdd">
						
					</tbody>
				</table>
				<!-- e_형성평가 실시 여부 및 횟수 -->
	
				<!-- s_총괄평가 기준 -->
				<div class="prcs_title" id="AddEvalList">총괄평가 기준</div>
				
				<!-- 	
				s_학점 기준
				<div class="prcs_title">
					학점 기준 : <span class="sp04" id="grade_method_name"></span>
				</div>
				<table class="prcs tb1">
					<tbody id="gradeStandardList">
						
					</tbody>
				</table>
				e_학점 기준
 				-->
 				
				<!-- s_교육과정 개요 및 목적 -->
				<div class="prcs_title curr_summary">교육과정 개요 및 목적</div>
				<div class="prcs_wrap_s curr_summary">
					<div class="prcs_tt_box" id="curr_summary">
											
					</div>
				</div>
				<!-- e_교육과정 개요 및 목적 -->
	
				<!-- s_교재 및 부교재, 참고 자료 -->
				<div class="prcs_title reference_data">교재 및 부교재, 참고 자료</div>
				<div class="prcs_wrap_s reference_data">
					<div class="prcs_tt_box" id="reference_data">
						
					</div>
				</div>
				<!-- e_교재 및 부교재, 참고 자료 -->
	
				<!-- s_자체 운영 계획 및 규정 -->
				<div class="prcs_title management_plan">자체 운영 계획 및 규정</div>
				<div class="prcs_wrap_s management_plan">
					<div class="prcs_tt_box" id="management_plan"></div>
				</div>
				<!-- e_자체 운영 계획 및 규정 -->
	
				<div class="prcs_title curr_outcome">과정 성과</div>
				<div class="prcs_wrap_s curr_outcome">
					<div class="prcs_tt_box" id="curr_outcome"></div>
				</div>	
				<!-- s_졸업역량 및 과정 성과 -->
				<div class="prcs_title" id="mpFinishCapabilityList">졸업역량 및 과정 성과</div>
				
				<!-- e_졸업역량 및 과정 성과 -->
	
				<div class="prcs_wrap_s">
					<p class="end1">교육과정 계획 끝.</p>
				</div>
	
				<a href="#totop1" id="backtotop1" title="To top" class="totop"></a>
	
			</div>
			<!-- e_ptb_wrap -->
	
		</div>
		<!-- e_contents -->
	</body>
</html>
