<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta property="og:title" content="MLMS">
		<meta property="og:image" content="img/favicon.png">
		<meta property="og:description" content="의과대학 LMS">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no, maximum-scale=1, width=device-width" />
		<title>MLMS</title>
		
		<link rel="icon" type="img/png" href="${M_IMG}/favicon.png">
		<link rel="shortcut icon" href="${M_IMG}/favicon.png">
		<link rel="icon" href="${M_IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${M_IMG}/favicon.ico">
		<link rel="stylesheet" href="${CSS}/css_m1.css" type="text/css">
		<link rel="stylesheet" href="${CSS}/swiper.min.css">
		<link rel="stylesheet" href="${JS}/lib/mobile/fullcalendar-v3.6.2/css/calendar1.css">
		<link rel="stylesheet" href="${CSS}/dev_css_m1.css">
		<script src="${JS}/lib/jquery-1.11.1.js"></script>
		<script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.form.min.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/lib/DateTimePicker.js"></script>
		<script src="${JS}/lib/swiper.min.js"></script>
		<script src="${JS}/mobileCommon.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				customSelectBoxInit();
				initPageTopButton();
				
				getFeScoreList();
			});
			
			
			function getFeScoreList(){
				
				$.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/pf/gradeAsgmt/fe/Detail/list",
		            data: {
		            	"curr_seq" : "${curr_seq}"
		            	,"fe_seq" : "${fe_seq}"            	
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status == "200"){
		            		$("span[data-name=feName]").text(data.feInfo.fe_name);
		            		$("span[data-name=onFe]").text(data.feInfo.fe_cnt);
		            		$("span[data-name=offFe]").text(data.feInfo.st_cnt-data.feInfo.fe_cnt);
		            		$("span[data-name=maxScore]").text(data.feInfo.max_score);
		            		$("span[data-name=minScore]").text(data.feInfo.min_score);
		            		$("span[data-name=avgScore]").text(data.feInfo.avg_score);
		            			            
			            	var htmls="";
				    	    $.each(data.feList, function(index){	
				    	    	var picture = "";
				    	    	if(isEmpty(this.picture_path))
				    	    		picture = "${IMG}/ph_3.png";
			    	    		else
			    	    			picture = "${RES_PATH}"+this.picture_path;
			    	    			
				    	    	if(!isEmpty(this.fe_score)){
				            		htmls = '<div class="a_mp">'
				    					+'<span class="pt01"><img src="'+picture+'" alt="사진" class="pt_img"></span>'
				    					+'<span class="ssp1">'+this.name+'</span>'
				    					+'<span class="sc">'+this.fe_score+'</span>'
				    					+'</div>';
					            	$("#onFeScoreListAdd").append(htmls);
			    	    		}else {
			    	    			htmls = '<div class="a_mp">'
				    					+'<span class="pt01"><img src="'+picture+'" alt="사진" class="pt_img"></span>'
				    					+'<span class="ssp1">'+this.name+'</span>'
				    					+'</div>';
					            	$("#offFeScoreListAdd").append(htmls);
			    	    		}
			            	});
		            	}
		            },
		            error: function(xhr, textStatus) {
		                alert("오류가 발생했습니다.");
		            }
		        });
				
			}
		</script>
	</head>
	<body class="color1 bd_quiz">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb_q">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
		<!-- s_wrap -->
		<div id="wrap">
	
			<!-- s_header -->
			<header class="header">
				<div class="wrap">
	
					<!-- s_icpt -->
					<!-- <div class="icpt">
						<img src="img/ph04.png" alt="사진 이미지">
					</div> -->
					<!-- e_icpt -->
	
					<!-- s_top_date -->
					<div class="top_date quiz" style="max-width:90%;">
						<span class="sp_q1 qend" data-name="feName" style="padding:0 0 0 0;"></span>
					</div>
					<!-- e_top_date -->
	
				</div>
	
			</header>
			<!-- e_header -->
	
			<!-- s_container_table -->
			<div id="container" class="container_table">
				<!-- s_contents -->
				<div class="contents">
	
					<!-- s_ptb_wrap -->
					<div class="ptb_wrap">
						<!-- s_pr_quiz -->
						<div class="pr_quiz end">
	
							<div class="b_wrap">
								<!-- s_응시 결시 -->
								<div id="gnb_q" class="tt_wrap2">
									<div class="wrap_s1">
										<div class="wrap_ss">
											<div class="tt_a on">
												<span class="sp_t">응시</span>
												<span class="sp_n" data-name="onFe"></span>
												<span class="sp_un">명</span>
											</div>
											<div class="tt_b">
												<span class="sp_t">결시</span>
												<span class="sp_n" data-name="offFe"></span>
												<span class="sp_un">명</span>
											</div>
										</div>
									</div>
								</div>
								<!-- e_응시 결시 -->
								<!-- s_최고 최저 평균 -->
								<div class="quiz_wrap_sc">
									<div class="wrap_s">
										<span class="quiz_sc">
											<span>최고점</span>
											<span class="sp1" data-name="maxScore"></span>
										</span> 
										<span class="quiz_sc"> 
											<span>최저점</span>
											<span class="sp2" data-name="minScore"></span>
										</span> <span class="quiz_sc"> 
											<span>평균</span>
											<span class="sp3" data-name="avgScore"></span>
										</span>
									</div>
								</div>
								<!-- s_최고 최저 평균 -->
								<!-- s_응시자 점수 -->
								<div class="pht">
									<div class="con_wrap">
										<div class="con_s2" id="onFeScoreListAdd"> 
										</div>
										<div class="con_s3" id="offFeScoreListAdd"> 
										</div>
									</div>
								</div>
								<!-- e_응시자 점수 -->
							</div>
	
						</div>
						<!-- e_pr_quiz -->
					</div>
					<!-- e_ptb_wrap -->
	
				</div>
				<!-- e_contents -->
			</div>
			<!-- e_container_table -->
	
			<a href="#" id="backtotop" title="To top" class="totop"></a>
	
			<!-- s_이전, 다음 -->
			<div class="go_wrap">
	
				<div class="pop_btn_wrap">
					<button type="button" class="btn01" onclick="javascript:window.close();">닫기</button>
				</div>
	
			</div>
			<!-- e_이전, 다음 -->
		</div>
		<!-- e_wrap -->
	
	</body>
</html>