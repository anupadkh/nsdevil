<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta property="og:title" content="MLMS">
		<meta property="og:image" content="img/favicon.png">
		<meta property="og:description" content="의과대학 LMS">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no, maximum-scale=1, width=device-width" />
		<title>MLMS</title>
		
		<link rel="icon" type="img/png" href="${M_IMG}/favicon.png">
		<link rel="shortcut icon" href="${M_IMG}/favicon.png">
		<link rel="icon" href="${M_IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${M_IMG}/favicon.ico">
		<link rel="stylesheet" href="${CSS}/css_m1.css" type="text/css">
		<link rel="stylesheet" href="${CSS}/swiper.min.css">
		<link rel="stylesheet" href="${JS}/lib/mobile/fullcalendar-v3.6.2/css/calendar1.css">
		<link rel="stylesheet" href="${CSS}/dev_css_m1.css">
		<script src="${JS}/lib/jquery-1.11.1.js"></script>
		<script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.form.min.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/lib/DateTimePicker.js"></script>
		<script src="${JS}/lib/swiper.min.js"></script>
		<script src="${JS}/mobileCommon.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				customSelectBoxInit();
				initPageTopButton();
				
				getMobileCurrLessonPlan();
				
				$(document).on("click", ".twrap_a", function(){
					$(this).hide();
					$(this).siblings("div.twrap_b").show();
				});
				$(document).on("click", ".twrap_b", function(){
					$(this).hide();
					$(this).siblings("div.twrap_a").show();
				});
				
				if("${pf_level}" == "3"){
					$("#asgmtCombo").hide();
				}
			});
			
			function getFeDetail(curr_seq, fe_seq){
				fePostDetail("${M_HOME}/pf/gradeAsgmt/fe/Detail",{"curr_seq" : curr_seq, "fe_seq" : fe_seq});
			}
			
			function fePostDetail(action, params) {
				var form = document.createElement("form");
				window.open('', 'viewer');
				form.target = "viewer";
				form.method = "post";				
				form.action = action;
				
				for (var key in params) {
					var hiddenField = document.createElement("input");
					hiddenField.type = "hidden";
					hiddenField.name = key;
					hiddenField.value = params[key];
					
					form.appendChild(hiddenField);
				}
				
				document.body.appendChild(form);
				form.submit();
			}
			
			function tabChange(tab){
				if(tab == "tab1"){
					$("div.iqr_w2").hide();
					$("div.iqr_w1").show();
					getMobileCurrLessonPlan();					
				}else{
					$("div.iqr_w1").hide();	
					$("div.iqr_w2").show();		
					getScore();
				}
			}
			
			function getMobileCurrLessonPlan(){			
				var type = $("#asgmt_type span:eq(0)").attr("data-value");
				
				$.ajax({
                    type: "POST",
                    url: "${M_HOME}/ajax/pf/gradeAsgmt/detailList/asgmtList",
                    data: {
                    	"curr_seq" : "${curr_seq}",
                        "lp_seq_yn" : "N",
                        "asgmt_type" : type,
                        "asgmt_name" : $("input[name=asgmt_name]").val()
                    },
                    dataType: "json",
                    success: function(data, status) {
                        if(data.status=="200"){
                        	$("span[data-name=currName]").text(data.currInfo.curr_name);
                        	$("span[data-name=currDate]").text(data.currInfo.curr_start_date+"~"+data.currInfo.curr_end_date_mmdd);
                        	$("span[data-name=mpfName]").text(data.currInfo.name);
                        	$("span[data-name=departmentName]").text(data.currInfo.code_name);
                        	
                        	if(type=="1"){
                        		$(".curr_asgmt").show();
	           	                $(".lesson_asgmt").hide();
                        	}else{
                        		$(".curr_asgmt").hide();
	           	                $(".lesson_asgmt").show();
                        	}
                        		
                            $("#asgmtListAdd").empty();
           	                $("#assignMentListAdd").empty(); 
           	                           	                
           	                var htmls = '';
           	                
           	                if(data.assignMentList.length == 0){
	           	                if(type=="1"){
	           	                	$("#assignMentListAdd").addClass("regix"); 
	           	                }else{
	           	                	$("#asgmtListAdd").addClass("regix");
	           	                }
           	                }else{
           	                	if(type=="1"){
	           	                	$("#assignMentListAdd").removeClass("regix"); 
	           	                }else{
	           	                	$("#asgmtListAdd").removeClass("regix");
	           	                }
           	                }           	                
           	                
                            $.each(data.assignMentList, function(){
                            	if(type=="1"){
                            		htmls='<tr class="box_wrap">';
				            	    htmls+='<td class="box_s">';
				            	    htmls+='<div class="wrap1">';
				            	    htmls+='<div class="box1">';
				            				
				            	    htmls+='<div class="wrap_ss">';
				            	    htmls+='<div class="num">1</div>';
				            	    htmls+='<div class="prg_t dt">'+this.start_date_mmdd+' ~ '+this.end_date_mmdd+'</div>';
				            	    htmls+='</div>';
				            	    
				            	    if(!isEmpty(this.asgmt_name)){
					            	    htmls+='<div class="twrap_a">'+this.asgmt_name+'</div>';
					            	    htmls+='<div class="twrap_b" style="display:none;">'+this.full_asgmt_name+'</div>';
					            	}else{
					            		htmls+='<div class="twrap_a">'+this.full_asgmt_name+'</div>';
					            	    htmls+='<div class="twrap_b" style="display:none;">'+this.full_asgmt_name+'</div>';
					            	}
				            	    
				            		if(!isEmpty(this.file_name)){
				            			var file_name = this.file_name.split("||");
				            			var file_path = this.file_path.split("||");
				            			
				            			for(var i=0; i<file_name.length; i++){
					            			htmls+='<div class="list_wrap">';
						            	    htmls+='<ul class="dw_list">';
						            	    htmls+='<li class="li_1">';
						            	    htmls+='<div class="tt">'+file_name[i]+'</div><div class="bt_wrap">';
						            	    htmls+='<button class="dw" title="다운로드" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+file_path[i]+'\',\''+file_name[i]+'\');">다운로드</button></div>';                   
						            	    htmls+='</li>';		            	               
				           	                htmls+='</ul>';
				           	                htmls+='</div>';		            
				            			}
				            		}
		           	                htmls+='<div class="wrap2">';
		           	                htmls+='<button class="btn1 open1" onClick="bindPopup(\'#m_pop1\','+this.asgmt_seq+');">';
		           	                if(this.group_flag == "N")
		           	                	htmls+='<span class="sp_num">'+this.submit_cnt+'</span><span>명 제출 (전체</span><span class="sp_num">'+this.st_cnt+'</span><span>명)</span>';
	           	                	else
	           	                		htmls+='<span class="sp_num">'+this.submit_cnt+'</span><span>조 제출 (전체</span><span class="sp_num">'+this.st_cnt+'</span><span>조)</span>';
		           	                htmls+='</button>';
		           	                htmls+='</div>';		            	                		            				
		           	                htmls+='</div>';
		           	                htmls+='</div>';	
		           	                htmls+='</td>';
		           	                htmls+='</tr>';	
		           	                $("#assignMentListAdd").append(htmls);                            		
                            	}
                            	if(type=="2"){
	                                var state = "", day = "", group_YN="";
	                                var start_date = new Date(this.start_date);
	                                var end_date = new Date(this.end_date);
	                                var state_class = "";
	                                var period = this.period.split(",");
	                                var lp_period = "";
									if(period.length > 1)
										lp_period = period[0]+"~"+period[period.length-1];
									else
										lp_period = period;
									
	                                if(this.group_flag=="Y") 
	                                	group_YN="그룹과제";
	                                else 
	                                	group_YN="개별과제";
	                                
	                                if(this.assignment_state == "1"){
	                                    state = "대기중";
	                                    state_class = "att_bf";
	                                }else if(this.assignment_state == "2"){
	                                	state = "진행중";
	                                    state_class = "attend";
	                                }else if(this.assignment_state == "3"){
	                                    state = "마감";
	                                    state_class = "absent";
	                                }
	
	                                htmls='<tr class="box_wrap">'
										+'<td class="box_s w1">'
										+'<div class="wrap_s s1">'
										+'<span class="sp_tta">'+group_YN+'</span>'; 
									if(this.submit_cnt==this.st_cnt){
										htmls+='<span class="sp_s1">전원 제출</span>';
									}else if(this.submit_cnt==0){
										htmls+='<span class="sp_ttb">전원 미제출</span>';
									}else{
										htmls+='<span class="sp_s1">'
										+'<span class="num1">'+this.submit_cnt+'</span>'
										+'<span class="sign">/</span>'
										+'<span class="num2">'+this.st_cnt+'</span>'
										+'</span>';
									}		
									
									
									htmls+='</div>'
										+'</td>'
										+'<td class="box_s w2" onclick="getLpAsgmt('+this.lp_seq+','+this.curr_seq+');">'
										+'<div class="wrap_s">'
										+'<div class="wrap_ss">'
										+'<span class="tm num_s1">'+this.lesson_date+'</span>'
										+'<span class="'+state_class+'">'+state+'</span>'
										+'</div>'
										+'<div class="wrap_ss">'
										+'<span class="sp_1">'+this.lesson_subject+'</span>' 
										+'<span class="sp_2">'
										+'<span class="num1">'+lp_period+'</span><span class="tt1">교시</span>'
										+'<span class="sign">|</span><span class="num2">'+this.lp_start_time+'~'+this.lp_end_time+'</span>'
										+'<span class="tt2">'+this.name+'</span>'
										+'</span>'
										+'</div>'
										+'</div>'
										+'</td>'
										+'</tr>';    	           	                
	                                $("#asgmtListAdd").append(htmls);
                            	}
                            });
                        }else{
                            alert("실패");
                        }
                        
                    },
                    error: function(xhr, textStatus) {
                        //alert("오류가 발생했습니다.");
                        document.write(xhr.responseText);
                    }
                }); 
			}
			
			function getLpAsgmt(lp_seq, curr_seq){
				$.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/pf/common/setCurrLpSeq",
		            data: {   
		            	"lp_seq" : lp_seq,
		            	"curr_seq" : curr_seq
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	location.href='${M_HOME}/pf/assignMent';
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        });
			}
			
			function bindPopup(popupName, asgmt_seq) {
				var popup = $(popupName);
				getNotSubmitStudentList(asgmt_seq);
				$("input[name=asgmt_seq]").val(asgmt_seq);
				popup.show();
								
				$(document).click(function(e) {
				    if (popup.is(e.target)) {
				    	popup.hide();
				    }
				});
			}	
			
			function getNotSubmitStudentList(asgmt_seq, reload_flag){
				 $.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/pf/assignMent/notSubmitSTList",
		            data: {   
		            	"asgmt_seq" : asgmt_seq,
		            	"curr_seq" : "${curr_seq}"
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){			            		
		            		var htmls = "";
		            		$.each(data.stList, function(){
		            			var picture = "";
		            			if(isEmpty(this.picture_path))
		            				picture = "${IMG}/ph_3.png"
	            				else
	            					picture = "${RES_PATH}"+this.picture_path;
			            		htmls+='<div class="a_mp">';
		            			htmls+='<span class="pt01"><img src="'+picture+'" alt="사진" class="pt_img"></span><span class="ssp1">'+this.name+'</span></div>';
			            		
		            		});
		            		$("#STListAdd").html(htmls);
		            		$("span[name=submitSTCnt]").text(data.stList.length);
		            		
		            		if(reload_flag == "Y")
		            			alert("미제출자 리스트를 다시 가져왔습니다.");
		            		
		            	}else{
		            		alert("과제 미제출 리스트 가져오기 실패");
		            	}
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        }); 
			}
			
			function refresh(){
				getNotSubmitStudentList($("input[name=asgmt_seq]").val(), "Y");				
			}
			
			function getScore(){
				//type : 1 수시성적 , 2 형성평가, 3 교과정성적
				var type = $("#scoreType span:eq(0)").attr("data-value");
				
				if(isEmpty(type))
					return;
				
				if(type == "1"){
					getSooSiScore();
					$("#scoreListAdd").show();
					$("#currTable").hide();					
				} else if(type == "2"){
					getFeScore();
					$("#scoreListAdd").show();
					$("#currTable").hide();
				} else if(type == "3"){
					getCurrScore("id");
					$("#scoreListAdd").hide();
					$("#currTable").show();
				}
			}
			
			function getFeScore(){
				$.ajax({
                    type: "POST",
                    url: "${M_HOME}/ajax/pf/gradeAsgmt/detailList/feList",
                    data: {
                    	"curr_seq" : "${curr_seq}",
                        "scoreName" : $("input[name=scoreName]").val()
                    },
                    dataType: "json",
                    success: function(data, status) {
                        if(data.status=="200"){
                            $("#scoreListAdd").empty();
                            
                            if(data.list.length == 0)
    		    	    		$("#scoreListAdd").addClass("regix");
    		    	    	else
    		    	    		$("#scoreListAdd").removeClass("regix");
                            
                            var htmls = '';                            
                            $.each(data.list, function(){                            	
                            	var state = "";
                            	var state_class = "";
                            	var period = this.period.split(",");
                            	var quiz_cnt = "";;
                            	
                            	if(this.quiz_cnt == 0)
                            		quiz_cnt = "문제 미등록";
                            	else
                            		quiz_cnt = "만점:<span class='num'>"+this.quiz_cnt+"</span>";
                            	
                            	if(period.length > 1)
                            		period = period[0]+"~"+period[period.length-1];
                            	else
                            		period = this.period;
                            		
                            	if(this.test_state == "WAIT"){
                                    state = "대기중";
                                    state_class = "att_bf";
                                }else if(this.test_state == "START"){
                                	state = "진행중";
                                    state_class = "attend";
                                }else if(this.test_state == "END"){
                                    state = "종료";
                                    state_class = "absent";
                                }

                            	
                                htmls='<tr class="box_wrap">'
                                	+'<td class="box_s w1">'
                                	+'<div class="wrap_s s1a">'
                                	+'<span class="sp_tta"><span class="t2">최고:</span><span class="num1_1">'+this.max_score+'</span></span>'
                                	+'<span class="sp_tta"><span class="t2">최저:</span><span class="num1_1">'+this.min_score+'</span></span>'
                                	+'<span class="sp_tta"><span class="t2">평균:</span><span class="num1_1">'+this.avg_score+'</span></span>'
                                	+'</div>'
                                	+'</td>';
                                	
                                if(this.test_state == "END")
                                	htmls+='<td class="box_s w2" onClick="getFeDetail(${curr_seq},'+this.fe_seq+');">';
                               	else
                               		htmls+='<td class="box_s w2">';	
                               		
                                htmls+='<div class="wrap_s">'
                                	+'<div class="wrap_ss">'
                                	+'<span class="tm num_s1">'+this.lesson_date+'</span><span class="tm num_s1b">('+quiz_cnt+')</span>'
                                	+'<span class="'+state_class+'">'+state+'</span>'
                                	+'</div>'
                                	+'<div class="wrap_ss">'
                                	+'<span class="sp_1a">'+this.lesson_subject+'</span>'
                                	+'<span class="sp_2a""><span class="num1">'+period+'</span>' 
									+'<span class="tt1">교시</span><span class="sign">|</span><span class="num2">'+this.start_time+'~'+this.end_time+'</span><span class="tt2">'+this.name+'</span>'
                                	+'</div>'
                                	+'</div>'
                                	+'</td>'
                                	+'</tr>';     
                                $("#scoreListAdd").append(htmls);     
                            });
                        }else{
                            alert("실패");
                        }
                        
                    },
                    error: function(xhr, textStatus) {
                        //alert("오류가 발생했습니다.");
                        document.write(xhr.responseText);
                    }
                }); 
			}
			
			function getSooSiScore() {

				$.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/pf/gradeAsgmt/detailList/soosiList",
		            data: {
						"curr_seq" : "${curr_seq}"
		            	,"src_name" : $("input[name=scoreName]").val()            	
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	var htmls="";
		    	    	$("#scoreListAdd").empty();
		    	    	
		    	    	if(data.scoreList.length == 0){
		    	    		$("#scoreListAdd").addClass("regix4");
		    	    	} else{
		    	    		$("#scoreListAdd").removeClass("regix4");
		    	    	}
		    	    	
			    	    $.each(data.scoreList, function(index){	
			    	    	htmls='<tr class="box_wrap">'
                        	+'<td class="box_s w2">'	
                       		+'<div class="wrap_s">'
                        	+'<div class="wrap_ss">'
                        	+'<span class="tm num_s1">'+this.src_date+'</span>'
                        	+'</div>'
                        	+'<div class="wrap_ss">'
                        	+'<span class="sp_1a">'+this.curr_name+'</span>'
            			    +'<span class="sp_1b">'+this.src_name+'</span>' 
            			    +'<span class="sp_2a">'
            			    +'<span class="tt1">총문항</span><span class="num2">'+this.question_cnt+'</span>'
            			    +'<span class="tt1">평균정답</span><span class="num2">'+this.avg_answer_cnt+'</span>'
            			    +'<span class="tt1">평균점수</span><span class="num2">'+this.avg_score+'</span>'
                        	+'</span></div>'
                        	+'</div>'
                        	+'</td>'
                        	+'</tr>';    
                        	
			            	$("#scoreListAdd").append(htmls);
		            	});
		            },
		            error: function(xhr, textStatus) {
		                alert("오류가 발생했습니다.");
		            }
		        });
			}
			
			function getCurrScore(order_column) {

				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/pf/lesson/grade/getStList",
		            data: {
		            	"order" : order_column
		            	,"curr_seq" : "${curr_seq}"
		            	,"st_name" : $("input[name=scoreName]").val()            	
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	var htmls="";
		    	    	$("#currScoreListAdd").empty();
		    	    	
		    	    	if(data.stList.length == 0){
		    	    		$("#currTable").addClass("regix");
							$("#currTable thead").hide();
		    	    	} else{
		    	    		$("#currTable").removeClass("regix");
							$("#currTable thead").show();
		    	    	}
		    	    	
			    	    $.each(data.stList, function(index){	
			    	    	var score = this.score.split("\|");
		            		var totalScore = 0;
							if(!isEmpty(score)){
								for(var i=0; i<score.length;i++){
									totalScore += parseFloat(score[i]);							
								}							               
							}
		            		htmls = '<tr class="box_wrap open2" onClick="getCurrDetailScore(\''+this.id+'\');">'
		            		    +'<td class="box_s w1s">'
		            		    +'<span class="tt">' + this.id + '</span>'
		            		    +'</td>'		            			
		            		    +'<td class="box_s w2s">'
		            		    +'<span class="tt">' + this.name + '</span>'
		            		    +'</td>'
		            		    +'<td class="box_s w3s">'
		            		    +'<span class="tt">'+totalScore+'</span>'
		            		    +'</td>'
		            		    +'<td class="box_s w4s">'
		            		    +'<span class="tt">'+this.change_score+'</span>'
		            		    +'</td>'		            			
		            		    +'<td class="box_s w5s">'
		            		    +'<span class="tt">'+this.final_grade+'</span>'
		            		    +'</td>'		            			
		            		    +'</tr>';
			            	$("#currScoreListAdd").append(htmls);
		            	});
		            },
		            error: function(xhr, textStatus) {
		                alert("오류가 발생했습니다.");
		            }
		        });
			}
			
			function getCurrDetailScore(id) {

				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/pf/lesson/grade/getStList",
		            data: {
		            	"order" : "id"
		            	,"curr_seq" : "${curr_seq}"
		            	,"st_name" : ""
		            	,"id" : id
		            },
		            dataType: "json",
		            success: function(data, status) {
	            		
		            	if(data.status == "200"){
			            	var htmls="";	
			            	$("#m_pop2").show();
				    	    $.each(data.stList, function(index){	
				    	    	var score = this.score.split("\|");
			            		var totalScore = 0;
								if(!isEmpty(score)){
									for(var i=0; i<score.length;i++){
										totalScore += parseFloat(score[i]);							
									}							               
								}
								$.each(data.subjectList,function(index){
									htmls+= '<tr class="box_wrap">'
										+'<th class="box_s w4"><span class="tt">'+this.subject+'</span></th>'
										+'<td class="box_s w5"><span class="tt">'+score[index]+'</span></td></tr>';								
								});
								
								$("span[data-name=stInfo]").text(this.id+" "+this.name);
								
								$("span[data-name=currTScore]").text(totalScore);
								$("span[data-name=currSScore]").text(this.change_score);
								$("span[data-name=currGrade]").text(this.final_grade);
								
				            	$("#currScoreSubject").html(htmls);
			            	});
		            	}
		            },
		            error: function(xhr, textStatus) {
		                alert("오류가 발생했습니다.");
		            }
		        });
			}
		</script>
	</head>
	<body class="color2 inquiry v1">
	<noscript title="브라우저 자바스크립트 차단 해제 안내">
		<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
	</noscript>

	<!-- s_skipnav -->
	<div id="skipnav">
		<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
	</div>
	<!-- e_skipnav -->

	<!-- s_wrap -->
	<div id="wrap">

		<!-- s_header -->
		<header class="header">
			<div class="wrap">

				<!-- s_gobf -->
				<div class="gobf" title="이전 페이지로 가기" onclick="location.href='${M_HOME}/pf/gradeAsgmt'"></div>
				<!-- e_gobf -->

				<!-- s_top_title -->
				<div class="top_title inquiry">
					<span>${curr_name }</span>
				</div>
				<!-- e_top_title -->

			</div>

		</header>
		<!-- e_header -->

		<!-- s_container_table -->
		<div id="container" class="container_table">
			<!-- s_contents -->
			<div class="contents">

				<!-- s_iqrtb_wrap -->
				<div class="iqrtb_wrapv">

					<!-- s_iqr_w1 -->
					<div class="iqr_w1">

						<div class="iqr_ttwrap">
							<span class="iqr1 on">과제</span> 
							<span class="iqr2" onClick="tabChange('tab2');">성적</span>
						</div>


						<!-- s_sch_wrap-->
						<div class="sch_wrap">
							<div class="wrap_uselectbox1" id="asgmtCombo">
								<div class="uselectbox" id="asgmt_type">
									<span class="uselected" data-value="2">수업과제</span><span
										class="uarrow">▼</span>
									<div class="uoptions">
										<span class="uoption firstseleted" data-value="1">과정과제</span>
										<span class="uoption" data-value="2">수업과제</span>
									</div>
								</div>
							</div>

							<input type="text" class="ip_search" placeholder="과제명"
								name="asgmt_name"
								onkeypress="javascript:if(event.keyCode==13){getMobileCurrLessonPlan(); return false;}">
							<button class="btn_search1" onClick="getMobileCurrLessonPlan();">검색</button>
						</div>
						<!-- e_sch_wrap -->
						
						<div class="top_tt_1 curr_asgmt">
							<span class="cc_t1" data-name="currName"></span>
							<span class="cc_t2"><span class="ts1">교육과정 기간</span><span class="ts2" data-name="currDate"></span></span>
							<span class="cc_t2"><span class="ts1">교수</span><span class="ts2" data-name="mpfName"></span><span class="sign">(</span><span class="ts2" data-name="departmentName"></span><span class="sign">)</span></span>	
						</div>
						<!-- s_ptb_wrap -->
						<div class="ptb_wrap curr_asgmt" id="curr_asgmt">
							<!-- s_tb_hwork -->
							<table class="tb_hwork" id="assignMentListAdd">

							</table>
							<!-- e_tb_hwork -->
						</div>
						<!-- e_ptb_wrap -->

						<div class="mtb_wrap lesson_asgmt" id="lesson_asgmt" style="display: none;">

							<!-- s_tb_iqr1 -->
							<table class="tb_iqr1 v1" id="asgmtListAdd">

							</table>
							<!-- e_tb_iqr1 -->
						</div>


					</div>
					<!-- e_iqr_w1 -->

					<!-- s_iqr_w2 -->
					<div class="iqr_w2" style="display: none;">

						<div class="iqr_ttwrap">
							<span class="iqr1" onClick="tabChange('tab1');">과제</span> <span
								class="iqr2 on">성적</span>
						</div>

						<!-- s_sch_wrap-->
						<div class="sch_wrap">
							<div class="wrap_uselectbox1">
								<div class="uselectbox" id="scoreType">
									<span class="uselected" data-value="1">수시성적</span> <span
										class="uarrow">▼</span>
									<div class="uoptions">
										<span class="uoption firstseleted" data-value="1">수시성적</span>
										<span class="uoption" data-value="2">형성평가</span>
										<c:if test='${pf_level ne 3}'>
											<span class="uoption" data-value="3">교과정성적</span>
										</c:if>
									</div>
								</div>
							</div>

							<input type="text" class="ip_search" placeholder=""
								name="scoreName"
								onkeypress="javascript:if(event.keyCode==13){getScore(); return false;}">
							<button class="btn_search1" onClick="getScore();">검색</button>
						</div>
						<!-- e_sch_wrap -->


						<div class="mtb_wrap">

							<!-- s_tb_iqr1 -->
							<table class="tb_iqr1 v2_st" id="scoreListAdd">


							</table>
							<!-- e_tb_iqr1 -->

							<table class="tb_iqr1 v2_3_st" id="currTable" style="display:none;">
								<thead>
									<tr class="box_wrap">
										<th class="box_s w1s"><span class="tt">학번</span></th>
										<th class="box_s w2s"><span class="tt">이름</span></th>
										<th class="box_s w3s"><span class="tt">총점</span></th>
										<th class="box_s w4s"><span class="tt">변환<br>점수
										</span></th>
										<th class="box_s w5s"><span class="tt">등급</span></th>
									</tr>
								</thead>
								<tbody id="currScoreListAdd">

								</tbody>
							</table>
						</div>

					</div>
					<!-- e_iqr_w1 -->

				</div>
				<!-- e_iqrtb_wrap -->

			</div>
			<!-- e_contents -->
		</div>
	</div>
	<a href="#" id="backtotop" title="To top" class="totop"></a>
	
	<!-- s_미제출자 -->
	<div id="m_pop1" class="pop_up_sbmx mo1">
		<div class="pop_wrap">

			<p class="t_title">
				<span class="sp_un">미제출</span> <span class="sp_nn"
					name="submitSTCnt"></span> <span class="sp_un">명</span>
			</p>

			<button class="renew_wrap" onClick="refresh();">
				<span class="renew"></span>
			</button>
			<!-- s_table_b_wrap -->
			<div class="table_b_wrap">

				<!-- s_pht -->
				<div class="pht">
					<div class="con_wrap">
						<div class="con_s2" id="STListAdd"></div>
					</div>
				</div>
				<!-- e_pop_table -->

			</div>
			<!-- e_pht -->

			<div class="t_dd">

				<div class="pop_btn_wrap2">
					<button type="button" class="btn01 close1"
						onClick="$('#m_pop1').hide();">닫기</button>
				</div>

			</div>
		</div>
	</div>
	<!-- e_미제출자 -->


	<!-- s_교과정성적 조회 -->
	<div id="m_pop2" class="pop_up_inqv mo1">
		<div class="pop_wrap">
			<p class="t_title">
				<span class="sp_un" data-name="stInfo"></span>
			</p>

			<!-- s_table_wrap -->
			<div class="table_wrap">
				<table class="tb1">
					<tr class="box_wrap">
						<th class="box_s w1"><span class="tt">총점</span></th>
						<th class="box_s w2"><span class="tt">변환 점수</span></th>
						<th class="box_s w3"><span class="tt">등급</span></th>
					</tr>
					<tr class="box_wrap">
						<td class="box_s w1"><span class="tt"
							data-name="currTScore"></span></td>
						<td class="box_s w2"><span class="tt"
							data-name="currSScore"></span></td>
						<td class="box_s w3"><span class="tt" data-name="currGrade"></span></td>
					</tr>
				</table>

				<table class="tb2" id="currScoreSubject">

				</table>
			</div>

			<div class="t_dd">

				<div class="pop_btn_wrap2">
					<button type="button" class="btn01 close2"
						onClick="$('#m_pop2').hide();">닫기</button>
				</div>

			</div>
		</div>
	</div>
	<!-- e_교과정성적 조회 -->

	<!-- s_팝업 -->
</body>
</html>