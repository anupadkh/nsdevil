<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function() {
				getAssignMentList();
				
				$(document).on("click", ".twrap_a", function(){
					$(this).hide();
					$(this).siblings("div.twrap_b").show();
				});
				$(document).on("click", ".twrap_b", function(){
					$(this).hide();
					$(this).siblings("div.twrap_a").show();
				});
				
				if("${lpPFCheck}" != "Y")
					$("body").addClass("pf");
			});
			
			function getAssignMentList(){
				
		        $.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/pf/assignMent/list",
		            data: {                  
		            	"asgmt_type" : "2",
		            	"curr_seq" : "${S_CURRICULUM_SEQ}",		            	
		            	"s_lp_seq" : "${S_LP_SEQ}"
		            	
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		titleSetting(data.lesson_plan_info);
		            		var htmls = "";
		            		if(data.assignMentList.length == 0){
		            			$("#assignMentListAdd").addClass("regix");
		            			
		            		}else{
		            			$("#assignMentListAdd").removeClass("regix");
			            		$.each(data.assignMentList, function(){
			            			
				            		htmls='<tr class="box_wrap">';
				            	    htmls+='<td class="box_s">';
				            	    htmls+='<div class="wrap1">';
				            	    htmls+='<div class="box1">';
				            				
				            	    htmls+='<div class="wrap_ss">';
				            	    htmls+='<div class="num">1</div>';
				            	    htmls+='<div class="prg_t dt">'+this.start_date+' ~ '+this.end_date+'</div>';
				            	    htmls+='</div>';
				            	    
				            	    if(!isEmpty(this.asgmt_name)){
					            	    htmls+='<div class="twrap_a">'+this.asgmt_name+'</div>';
					            	    htmls+='<div class="twrap_b" style="display:none;">'+this.full_asgmt_name+'</div>';
					            	}else{
					            		htmls+='<div class="twrap_a">'+this.full_asgmt_name+'</div>';
					            	    htmls+='<div class="twrap_b" style="display:none;">'+this.full_asgmt_name+'</div>';
					            	}
				            	    
				            		if(!isEmpty(this.file_name)){
				            			var file_name = this.file_name.split("||");
				            			var file_path = this.file_path.split("||");
				            			
				            			for(var i=0; i<file_name.length; i++){
					            			htmls+='<div class="list_wrap">';
						            	    htmls+='<ul class="dw_list">';
						            	    htmls+='<li class="li_1">';
						            	    htmls+='<div class="tt">'+file_name[i]+'</div><div class="bt_wrap">';
						            	    htmls+='<button class="dw" title="다운로드" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+file_path[i]+'\',\''+file_name[i]+'\');">다운로드</button></div>';                   
						            	    htmls+='</li>';		            	               
				           	                htmls+='</ul>';
				           	                htmls+='</div>';		            
				            			}
				            		}
		           	                htmls+='<div class="wrap2">';
		           	                htmls+='<button class="btn1 open1" onClick="bindPopup(\'#m_pop1\','+this.asgmt_seq+');">';
		           	             	if(this.group_flag == "N")
		           	                	htmls+='<span class="sp_num">'+this.submit_cnt+'</span><span>명 제출 (전체</span><span class="sp_num">'+this.st_cnt+'</span><span>명)</span>';
	           	                	else
	           	                		htmls+='<span class="sp_num">'+this.submit_cnt+'</span><span>조 제출 (전체</span><span class="sp_num">'+this.st_cnt+'</span><span>조)</span>';
		           	                htmls+='</button>';
		           	                htmls+='</div>';		            	                		            				
		           	                htmls+='</div>';
		           	                htmls+='</div>';	
		           	                htmls+='</td>';
		           	                htmls+='</tr>';	
		           	                $("#assignMentListAdd").append(htmls);
	
			            		});
		            		}
		            	}else{
		            		alert("과제리스트 가져오기 실패");
		            	}
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        }); 
		    }
			
			function titleSetting(lessonPlanInfo) {
				//교시
				var periodStr = lessonPlanInfo.period; //교시
				var periodList = periodStr.split(',');
				var startPeriod = periodList[0];
				if (periodList.length > 1) {
					periodStr = startPeriod + "~" + periodList[periodList.length-1] + "교시";  
				} else {
					periodStr = startPeriod + "교시";
				}
				
            	//수업일
            	var lessonDate = lessonPlanInfo.lesson_date;
            	lessonDate = lessonDate.replace("-", "월 ") + "일";
				
            	//수업시간
				var ampmText = lessonPlanInfo.ampm;
        		if(ampmText == "pm"){
        			ampmText = "오후";
        		} else {
        			ampmText = "오전";
        		}
        		
        		var startTime = lessonPlanInfo.start_time;
				var endTime = lessonPlanInfo.end_time;
            	
        		var lessonPlanDateTitle = lessonDate + " " + periodStr;//mm월 xx일 x교시
        		var lessonPlanTimeTitle = ampmText + " " + startTime + "~" + endTime;//오전/오후 xx:xx~xx:xx
        		
            	var titleHtml = '<span class="cc_s1">' + lessonPlanDateTitle + '</span><span class="cc_s2">(' + lessonPlanTimeTitle + ')</span>';
            	var titleDiv = $("div[name='titleDiv']").addClass("cc").html(titleHtml);
            	
            	//교육과정 명
            	var currName = lessonPlanInfo.curr_name;
            	
            	//수업계획서 명
            	var lessonSubject = lessonPlanInfo.lesson_subject;
            	
            	$("#currName").html(currName);
            	$("#lessonSubject").html(lessonSubject);
			}
			
			function getFormationEvaluationListView() {
				post_to_url("${M_HOME}/pf/formationEvaluation/list", {"lp_seq" : "${sessionScope.S_LP_SEQ}"});
			}
		
			function getAttendanceListView() {
				post_to_url("${M_HOME}/pf/attendance/list", {"lp_seq" : "${sessionScope.S_LP_SEQ}", "curr_seq":"${sessionScope.S_CURRICULUM_SEQ}"});
			}
		
			function getLessonPlanView() {
				post_to_url("${M_HOME}/pf/lessonPlan", {"lp_seq" : "${sessionScope.S_LP_SEQ}", "curr_seq":"${sessionScope.S_CURRICULUM_SEQ}"});
			}
			
			function getAssignMentListView(){
				post_to_url("${M_HOME}/pf/assignMent", {"lp_seq":"${sessionScope.S_LP_SEQ}"});
			}
			
			function bindPopup(popupName, asgmt_seq) {
				var popup = $(popupName);
				getNotSubmitStudentList(asgmt_seq);
				$("input[name=asgmt_seq]").val(asgmt_seq);
				popup.show();
								
				$(document).click(function(e) {
				    if (popup.is(e.target)) {
				    	popup.hide();
				    }
				});
			}			
			
			function getNotSubmitStudentList(asgmt_seq, reload_flag){
				 $.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/pf/assignMent/notSubmitSTList",
		            data: {   
		            	"asgmt_seq" : asgmt_seq,
		            	"curr_seq" : "${S_CURRICULUM_SEQ}"
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){			            		
		            		var htmls = "";
		            		$.each(data.stList, function(){
		            			var picture = "";
		            			if(isEmpty(this.picture_path))
		            				picture = "${IMG}/ph_3.png"
	            				else
	            					picture = "${RES_PATH}"+this.picture_path;
	            					
			            		htmls+='<div class="a_mp">';
		            			htmls+='<span class="pt01"><img src="'+picture+'" alt="사진" class="pt_img"></span><span class="ssp1">'+this.name+'</span></div>';
			            		
		            		});
		            		$("#STListAdd").html(htmls);
		            		$("span[name=submitSTCnt]").text(data.stList.length);
		            		
		            		if(reload_flag == "Y")
		            			alert("미제출자 리스트를 다시 가져왔습니다.");
		            		
		            	}else{
		            		alert("과제 미제출 리스트 가져오기 실패");
		            	}
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        }); 
			}
			
			function refresh(){
				getNotSubmitStudentList($("input[name=asgmt_seq]").val(), "Y");				
			}
			
			function getCurriculumView(){
				post_to_url("${M_HOME}/pf/curriculum", {"curr_seq" : "${sessionScope.S_CURRICULUM_SEQ}"});
			}
		</script>
	</head>
	<body class="color1">
		<input type="hidden" name="asgmt_seq">
		<!-- s_contents -->
		<div class="contents">
	
			<!-- s_top_tt-->
			<div class="top_tt">
				<span class="cc_t1" id="currName"></span>
				<span class="cc_t2" id="lessonSubject"></span>
			</div>
			<!-- e_top_tt -->
	
			<!-- s_mn_wrap -->
			<div class="mn_wrap hwork">
				<div class="wrap_s">
					<span class="cc_mn cc_mn01" onClick="javascript:getCurriculumView();">교육과정</span>
					<span class="cc_mn cc_mn02" onClick="javascript:getLessonPlanView();">수업계획</span>
					<span class="cc_mn cc_mn03" onClick="location.href='${M_HOME}/pf/lessonData'">자료</span>
					<!-- 수업교수인지 체크 -->
					<c:if test='${lpPFCheck == "Y" }'>
						<span class="cc_mn cc_mn04" onClick="javascript:getFormationEvaluationListView();">형성평가</span>
					</c:if>
					<span class="cc_mn cc_mn05 on" onClick="location.href='${M_HOME}/pf/assignMent';">과제</span>
					<span class="cc_mn cc_mn06" onClick="javascript:getAttendanceListView();">출석</span>
				</div>
			</div>
			<!-- e_mn_wrap -->
	
			<!-- s_ptb_wrap -->
			<div class="ptb_wrap">
				<!-- s_tb_hwork -->
				<table class="tb_hwork" id="assignMentListAdd">
					
				</table>
				<!-- e_tb_hwork -->
			</div>
			<!-- e_ptb_wrap -->
	
		</div>
		<!-- e_contents -->
		
		
		<!-- s_미제출자 -->	
		<div id="m_pop1" class="pop_up_sbmx mo1">  
		          <div class="pop_wrap">  
		
					  <p class="t_title"><span class="sp_un">미제출</span><span class="sp_nn" name="submitSTCnt">20</span><span class="sp_un">명</span></p>      
		 		     
			          <button class="renew_wrap" onClick="refresh();"><span class="renew"></span></button>
		 <!-- s_table_b_wrap -->
		<div class="table_b_wrap">	
		
		<!-- s_pht -->
		<div class="pht">                      
		          <div class="con_wrap"> 
		                <div class="con_s2" id="STListAdd"> 
		              
		                </div>
		         </div>
		         </div>
		<!-- e_pop_table -->	             
		                     
		</div>               
		<!-- e_pht --> 
		
		               <div class="t_dd">
		                   
		                    <div class="pop_btn_wrap2">
		                     <button type="button" class="btn01 close1" onClick="$('#m_pop1').hide();">닫기</button>               
		                    </div>
		                
		                </div>
		 </div>  
		</div>	
		<!-- e_미제출자 -->		
	</body>
</html>
