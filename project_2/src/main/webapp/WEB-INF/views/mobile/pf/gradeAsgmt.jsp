<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>		
		<script type="text/javascript">
			$(document).ready(function(){
				$("div[name=titleDiv]").removeClass("top_date").addClass("top_title inquiry");
				$("div[name=titleDiv]").html('<span>성적 및 과제 조회</span>');
				getMobileCurrLessonPlan();
			});
			
			function getMobileCurrLessonPlan(){			
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/pf/gradeAsgmt/currlist",
			        data: {
			        	
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	var htmls = "";
			        	
			            if (data.status == "200") {
			            	$.each(data.currList, function(index){
			            		htmls ='<tr class="box_wrap">'
			    					+'<td class="box_s w2">'
			    					+'<div class="wrap_s s2" onclick="getDetailList('+this.curr_seq+',\''+this.curr_name+'\');">'
			    					+'<div class="wrap_ss">'
			    					+'<span class="sp_1">'+this.curr_name+'</span>'
			    					+'</div>'
			    					+'<div class="wrap_ss">'
			    					+'<span class="tm tm2">'+this.curr_start_date+'</span><span class="tm sign">~</span>'
			    					+'<span class="tm tm3">'+this.curr_end_date+'</span><span class="tm tm1"></span>'
			    					+'<span class="tm sign"></span>'
			    					+'</div>'
			    					+'<div class="wrap_ss">'
			    					+'<span class="sp_2">'+this.laca_system_name+'<span class="sign">&gt;</span>'+this.maca_system_name+'<span class="sign">&gt;</span>'+this.aca_system_name+'</span>'
			    					+'</div>'
			    					+'</div>'
			    					+'</td>'
			    					+'</tr>';
			    				$("#currListAdd").append(htmls);
							});
			            } else {
			                alert("교육과정 리스트 가져오기 실패.");
			            }
			        },
			        error: function(xhr, textStatus) {
			            //alert("오류가 발생했습니다.");
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function getDetailList(curr_seq, curr_name){
				post_to_url("${M_HOME}/pf/gradeAsgmt/detailList", {"curr_seq" : curr_seq, "curr_name" : curr_name});
			}
		</script>
	</head>
	<body>

	<!-- s_contents -->
	<div class="contents">

		<!-- s_mtb_wrap -->
		<div class="mtb_wrap">
			<!-- s_tb_iqr -->
			<table class="tb_iqr" id="currListAdd">			
				
			</table>
			<!-- e_tb_iqr -->
		</div>
		<!-- e_mtb_wrap -->

	</div>
	<!-- e_contents -->
</body>
</html>