<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<style>
	.lms_table_am td{opacity: 1;border-bottom: solid 1px rgba(202, 243, 255, 1);border-left: solid 1px rgba(202, 243, 255, 1);text-align: left;vertical-align: top;box-sizing:border-box;margin: 0;padding: 0;font-size: 15px;line-height: 18px;width: 50px;height: 175px;color: rgba(0, 0, 0, 1);}
</style>
<script type="text/javascript">

	var currentMonth;
	var weekDataList = [];

	$(document).ready(function() {
		$("body").addClass("pf_am");
		loadCalendar();
		
		$(document).on("click","span.uoption", function() {
			
			currentMonth.setYear(Number($(this).attr('value')));
			
			loadCalendar();
		});
		
		$(".btn_l").bind("click", function(){
	        $(".btn_l").toggleClass("move-trigger_l");
	        $(".aside_l").toggleClass("folding");
	        $(".main_con").toggleClass("");
	    });
	});
	
	function loadCalendar(monthIndex) {
		
		var today = new Date();
		
		if (monthIndex != null) {
			if (monthIndex == 0) {
				monthIndex = null;
				currentMonth = null;
			} else {
				currentMonth.setMonth(currentMonth.getMonth() + monthIndex);
			}
		}
		
		if (currentMonth == null) {
			currentMonth = today;
		}
		
		var year = currentMonth.getFullYear();
		var month = currentMonth.getMonth();
		
		$(":input[name='searchOption']").attr("onchange", "javascript:loadCalendar();");
		
		$("#titleMonth").html(month + 1);
		$("#yearSelect .uselected").html(year);
		$("#yearSelect").attr("value", year);
		$("#yearSelect .uoptions").empty();
		
		//년도 드롭다운 출력
		for (var index = today.getFullYear() - 4; index <= today.getFullYear(); index++) {
			$("#yearSelect .uoptions").append('<span class="uoption" value="' + index + '">' + index + '</span>');
		}
		
		var startDay = new Date(year, month, 1);
		startDay.setDate(startDay.getDate() - startDay.getDay());
		
		var endDay = new Date(year, month + 1, 0);
		endDay.setDate(endDay.getDate() - endDay.getDay() + 6);
		
		var searchOption = [];
		
		$(":input[name='searchOption']:checked").each(function() {
			searchOption.push("'" + $(this).val() + "'");
		});
		
		$.ajax({
			type: "POST",
			url: "${HOME}/ajax/pf/lesson/getAcademicList",
			data: {
				"start_date" : dateToString(startDay),
				"end_date" : dateToString(endDay),
				"aca_system_name" : searchOption.join(",")
			},
			dataType: "json",
			success: function(data, status) {
				
				$("#calendar").empty();
				
				//막대의 상단 마진값을 일별로 저장하기 위한 변수
				var marginTopList = [];
				
				var setLastWeek = false;
				
				//이전 주 버튼으로 이동되었다면 마지막 주를 선택하게 함
				if (weekDataList["display"] < 0) {
					setLastWeek = true;
				}
				
				var displayIndex = 0;
				
				if (weekDataList["display"] != null) {
					displayIndex = weekDataList["display"];
				}
				
				weekDataList = [];
				weekDataList["display"] = displayIndex;
				
				for (var row = 0; row < 6; row++) {
					
					$("#calendar").append("<tr></tr>");
					var weekData = [];
					
					for (var col = 0; col < 7; col++) {
						
						var tdClass = "";
						var dayClass = "";
						
						if (col == 0) {
							tdClass = "bd02";
							dayClass = " color05";
							weekData["start_date"] = (startDay.getMonth() + 1) + "월 " + startDay.getDate() + "일";
						}
						if (col == 6) {
							dayClass = " color06";
							weekData["end_date"] = (startDay.getMonth() + 1) + "월 " + startDay.getDate() + "일";
						}
						if(currentMonth.getMonth() != startDay.getMonth()) {
							tdClass += " b_a";
						}
						
						if (dateToString(startDay) == dateToString(today)) {
							if (monthIndex == null) {
								weekDataList["display"] = row;
							}
							dayClass += " amtoday";
						}
						
						var academic = "";
						
						if (data.list != null) {
							$.each(data.list, function(index) {
								//시작일, 종료일이 없으면 다음루프로 넘김
								if (this.start_date == null || this.end_date == null) {
									return true;
								}
								
								if (stringToDate(this.start_date).getTime() <= startDay.getTime() && startDay.getTime() <= stringToDate(this.end_date).getTime()) {
									
									if (marginTopList[row + "-" + col] == null) {
										marginTopList[row + "-" + col] = 40;
									}
									
									var eventElement = $("#aca_" + row + "_" + this.aca_seq);
									
									//출력한 막대가 없을때만 출력
									if (eventElement.length == 0) {
										
										var color = "color1";
										
										if (this.aca_system_name == "예과1학년") {
											color = "color2";
										} else if (this.aca_system_name == "예과2학년") {
											color = "color3";
										} else if (this.aca_system_name == "본과1학년") {
											color = "color4";
										} else if (this.aca_system_name == "본과2학년") {
											color = "color5";
										} else if (this.aca_system_name == "본과3학년") {
											color = "color6";
										} else if (this.aca_system_name == "본과4학년") {
											color = "color7";
										}
										
										//막대가 몇일까지 출력될지 날짜 계산(주 단위로 출력하기 때문에 현재일을 제외하고 최대 6일이 넘지않게 함)
										var weekendDay = new Date(startDay.getTime());
										weekendDay.setDate(weekendDay.getDate() - weekendDay.getDay() + 6);
										
										var diffMax = dateDiff(startDay, weekendDay);
										
										var diff = dateDiff(startDay, stringToDate(this.end_date));
										
										if (diffMax < diff) {
											diff = diffMax;
										}
										
										var width = 50 + (50 * diff);
										
										academic += "<a id=\"aca_" + row + "_" + this.aca_seq + "\" class=\"m_ttx " + color + "\" style=\"width:" + width + "px;height:20px; margin-top:" + marginTopList[row + "-" + col] + "px;\" title=\"" + this.academic_name + "\">["+this.aca_system_name+"]"+this.academic_name+"</a>";
										
										weekData.push(this);
									}
									marginTopList[row + "-" + col] = marginTopList[row + "-" + col] + 22;
								}
							});
						}
						var html = "<td class=\"" + tdClass + "\">"
								+ "<span class=\"sp_date" + dayClass + "\">" + startDay.getDate() + "</span>"
								+ academic
								+ "</td>";
						$("#calendar tr:eq(" + row + ")").append(html);
						
						startDay.setDate(startDay.getDate() + 1);
					}
					


					[row] = weekData;
					
					if(currentMonth.getMonth() != startDay.getMonth()) {
						break;
					}
				}
				
				if (setLastWeek) {
					weekDataList["display"] = weekDataList.length - 1;
				}
				
				displayWeekData();
				
			},
			error: function(xhr, textStatus) {
				alert("오류가 발생했습니다.");
			}
		});
	}
	
	function displayWeekData(weekIndex) {
		
		if (weekIndex != null) {
			var weekIndex = weekDataList["display"] + weekIndex;
			
			if (weekIndex < 0) {
				weekDataList["display"] = -1;
				loadCalendar(-1);
				return;
			}
			if (weekDataList.length <= weekIndex) {
				weekDataList["display"] = 0;
				loadCalendar(1);
				return;
			}
			weekDataList["display"] = weekIndex;
		}
		
		var weekData = weekDataList[weekDataList["display"]];
		
		$("#calendar tr").removeClass("select_wrap");
		$("#calendar tr:eq(" + weekDataList["display"] + ")").addClass("select_wrap");
		$("#selectedWeekDate").html(weekData["start_date"] + " ~ " + weekData["end_date"]);
		
		$("#weekDataList").empty();
		
		$.each(weekData, function() {
			
			var color = "opt1";
			
			if (this.aca_system_name == "예과1학년") {
				color = "opt2";
			} else if (this.aca_system_name == "예과2학년") {
				color = "opt3";
			} else if (this.aca_system_name == "본과1학년") {
				color = "opt4";
			} else if (this.aca_system_name == "본과2학년") {
				color = "opt5";
			} else if (this.aca_system_name == "본과3학년") {
				color = "opt6";
			} else if (this.aca_system_name == "본과4학년") {
				color = "opt7";
			}
			
			var html = '<tr>'
	            + '<td class="w_ic"><span class="am_option ' + color + '">[' + this.aca_system_name.replace("학년", "") + ']</span></td>'
	            + '<td class="w_con">'
	            + '<span class="am_opt_tt">' + this.academic_name + '</span>'
	            + '<span class="am_opt_tts">' + this.start_date + ' ~ ' + this.end_date + '</span>'
	            + '</td>'
	            + '</tr>';
	            
			$("#weekDataList").append(html);
		});
	}
	
</script>
</head>

<body class="pf_am">
<!-- s_container_table -->
<div id="container" class="container_table">
<!-- s_contents -->
<div class="contents main">

<!-- s_left_mcon -->
<div class="left_mcon aside_l">   
<!-- s_학습자료 메뉴 -->
<div class="sub_menu st_am">
    <div class="title">학사일정</div>   
    <ul class="sub_panel">
        <li class="mn"><a href="${HOME }/aca/academicM" class="on">학 사 력</a></li>
        <c:if test='${S_USER_LEVEL == 3 }'>
        	<li class="mn"><a href="${HOME }/aca/MYscheduleM" class="">MY 시간표</a></li>	
			<li class="mn"><a href="${HOME }/aca/MYscheduleMemo" class="">개인일정</a></li>
		</c:if>
       	<c:if test="${S_USER_LEVEL == 4}">
       		<li class="mn"><a href="${HOME }/aca/MYscheduleMST" class="">MY 시간표</a></li>
        	<li class="mn"><a href="${HOME }/st/aca/caList" class="">수강신청</a></li>
       	</c:if>
        
    </ul>
</div>
<!-- e_학습자료 메뉴 -->
</div>
<!-- e_left_mcon -->

<div class="main_con st">

<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
<!-- s_tt_wrap -->                
<div class="tt_wrap">
    <h3 class="am_tt tt1">학사일정</h3>                    
</div>
<!-- e_tt_wrap -->
    
<div class="btn_wrap_am">
<!-- s_date_wrap -->
      <div class="date_wrap">         
<!-- s_"wrap1_lms_uselectbox -->  
          <div class="wrap1_lms_uselectbox">
		                        <div class="uselectbox" id="yearSelect">
		                                <span class="uselected"></span>
		                                <span class="uarrow">▼</span>
			
		                            <div class="uoptions"></div>
		                        </div>  								
          </div>
<!-- e_"wrap1_lms_uselectbox -->                    
<span class="date01" id="titleMonth"></span><span class="date01">월</span>
          <div class="ic_wrap" id="monthNavButton">
             <a id="monthPrev" href="javascript:loadCalendar(-1);" class="ic_ar01"></a>
             <a id="monthNext" href="javascript:loadCalendar(1);" class="ic_ar02"></a>
          </div>
      </div>
<!-- e_date_wrap -->  
      
<!-- s_오른쪽 버튼 -->  
                        <button class="btn04  btn_on" onclick="loadCalendar(0);">오늘</button>
<!-- e_오른쪽 버튼 --> 

<!-- s_am_option --> 
          <div class="am_option_wrap">              
                  <label class="lable_opt">
                      <input type="checkbox" name="searchOption" value="공통" class="ip_check" checked>
                      <span class="am_option opt1">공통</span></label>
                      
                  <label class="lable_opt">
                      <input type="checkbox" name="searchOption" value="예과1학년" class="ip_check" checked>
                      <span class="am_option opt2">예과1</span></label>

                  <label class="lable_opt">
                      <input type="checkbox" name="searchOption" value="예과2학년" class="ip_check" checked>
                      <span class="am_option opt3">예과2</span></label>

                  <label class="lable_opt">
                      <input type="checkbox" name="searchOption" value="본과1학년" class="ip_check" checked>
                      <span class="am_option opt4">본과1</span></label>

                  <label class="lable_opt">
                      <input type="checkbox" name="searchOption" value="본과2학년" class="ip_check" checked>
                      <span class="am_option opt5">본과2</span></label>

                  <label class="lable_opt">
                      <input type="checkbox" name="searchOption" value="본과3학년" class="ip_check" checked>
                      <span class="am_option opt6">본과3</span></label>
                      
                  <label class="lable_opt">
                      <input type="checkbox" name="searchOption" value="본과4학년" class="ip_check" checked>
                      <span class="am_option opt7">본과4</span></label>
          </div>
<!-- e_am_option --> 
</div>    

     
<div class="am_tbwrap">               
<!-- s_lms_table_m -->                                                          
<table class="lms_table_am">
                    <thead>
                        <tr class="">
                            <th class="th01 bd01 w1 bd02"><span class="sp_tt color05">일</span></th>
                            <th class="th01 bd01 w1"><span class="sp_tt">월</span></th>
                            <th class="th01 bd01 w1"><span class="sp_tt">화</span></th>
                            <th class="th01 bd01 w1"><span class="sp_tt">수</span></th>
                            <th class="th01 bd01 w1"><span class="sp_tt">목</span></th>
                            <th class="th01 bd01 w1"><span class="sp_tt">금</span></th>
                            <th class="th01 bd01 w1 bd03"><span class="sp_tt color06">토</span></th>
                        </tr>
                    </thead>    
                   <tbody id="calendar"></tbody>
</table>
<!-- e_lms_table_m -->
<div class="am_rt_wrap">	
<!-- s_lms_table_list -->                                                          
<table class="lms_table_list top">
                    <thead>
                        <tr class="">
                            <th class="th01">
    <!-- s_주간 날짜 -->       
                                <div class="ic_wrap">
                                    <a id="weekPrev" href="javascript:displayWeekData(-1);" class="ic_ar01"></a>                                     
                                    <span class="sp_tt" id="selectedWeekDate"></span>                                     
                                    <a id="weekNext" href="javascript:displayWeekData(1);" class="ic_ar02"></a>
                                </div>
    <!-- s_주간 날짜 -->                            
                             </th>
                        </tr>
                    </thead> 
</table> 

<!-- s_wrap_table_list -->
<div class="wrap_table_list">
<table class="lms_table_list">
	<tbody id="weekDataList"></tbody>
</table>
<!-- e_lms_table_list -->
</div>
</div>
<!-- e_wrap_table_list -->

</div>

</div>
</div>
</div>

</body>
</html>