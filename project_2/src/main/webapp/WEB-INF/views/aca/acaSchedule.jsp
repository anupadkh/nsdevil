<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>	
	
		<link rel="stylesheet" href="${RES}/fullcalendar-v3.6.2/css/calendar1.css">
		<link rel="stylesheet" href="${RES}/fullcalendar-v3.6.2/css/fullcalendar.print.css" media="print">
		<link rel="stylesheet" href="${CSS}/admin_style.css" type="text/css">
		<script src="${RES}/fullcalendar-v3.6.2/js/moment.min.js"></script>
		<script src="${RES}/fullcalendar-v3.6.2/js/fullcalendar.js"></script>
		<script src="${RES}/fullcalendar-v3.6.2/js/locale-all.js"></script>
		
		<!--체크 셀렉트 박스(MIT) https://codepen.io/elmahdim/pen/hlmri   -->
		<style>
			.am_option_wrap .am_option {
				display: inline-block;
				margin: 0;
				width: auto;
				padding: 0;
				font-size: 14px;
				color: rgba(0, 66, 118, 1);
				text-align: left;
				text-indent: 18px;
				letter-spacing: 0px;
			}
			
			div.mutliSelect .selet_chk {
				display: inline-block;
				width: 20px;
				height: 20px;
				margin: 0 10px 0 0;
				padding: 0;
				float: none;
			}
			
			.dropdown dt span.uarrow {
				position: relative;
				top: 0;
				text-indent: 0;
				display: inline-block;
				width: 15%;
				float: right;
				height: 33px;
				line-height: 33px;
				z-index: 1;
				box-sizing: border-box;
				text-align: center;
				font-size: 11px;
				background: none;
				padding: 0;
				margin: 0;
				color: rgba(1, 96, 121, .7);
			}
			
			.dropdown {
				margin-left: 5px;
				float:left;
			}
			
			.dropdown dt {
				background: rgba(240, 251, 255, 1);
				color: rgba(0, 49, 119, 1);
				display: block;
				padding: 0;
				min-height: 25px;
				line-height: 24px;
				overflow: hidden;
				border: 0;
				width: 200px;
				border: solid 1px rgba(104, 195, 235, 1);
			}
			
			.dropdown ul {
				margin: -1px 0 0 0;
			}
			
			.dropdown dd {
				position: relative;
			}
			
			.dropdown a, .dropdown a:visited {
				text-decoration: none;
				outline: none;
				font-size: 15px;
			}
			
			.dropdown dt a {
				height: 34px;
				line-height: 34px;
				overflow: hidden;
				border: 0;
				width: 155px;
				display: block;
				float: left;
				text-align:left;
				padding-left:15px;
			}
			
			.dropdown dt a span, .multiSel span {
				cursor: pointer;
				display: inline-block;
				padding: 0 3px 2px 0;
			}
			
			.dropdown dd ul {
				z-index: 999999;
				position: absolute;
				top: 0px;
				left: -1px;
				width: 100%;
				height: auto;
				max-height: none;
				line-height: 34px;
				border: 1px solid rgb(65, 169, 233);
				border-bottom-right-radius: 5px;
				border-bottom-left-radius: 5px;
				overflow-x: hidden;
				overflow-y: auto;
				background: rgb(255, 255, 255);
				padding: 0;
				display: none;
				color: rgba(1, 96, 121, 1);
				opacity: 1;
				z-index: 10001;
				box-sizing: content-box;
			}
			
			.dropdown span.value {
				display: none;
			}
			
			.dropdown dd ul li {
				position: relative;
				top: 0px;
				left: -1px;
				display: block;
				width: 100%;
				height: 35px;
				line-height: 34px;
				font-size: 15px;
				margin: -1px 0 2px 0;
				padding: 0 1px 0 1px;
				text-align: left;
				text-indent: 20px;
				opacity: 1;
				color: rgba(85, 85, 85, 1);
				background: rgba(149, 203, 252, 0.4);
			}
			
			.dropdown dd ul li:hover {
				color: rgba(255, 255, 255, 1);
				background-color: rgb(102, 152, 200);
			}
		</style>
		<script>
			var color = [
				 "#F78880"
				, "#F6B190"
				, "#D0BB63"
				, "#8DD4C3"
				, "#86CCED"
				, "#7EA6E9"
				, "#BC95E8"
				, "#F6B190"
				, "#D0BB63"
				, "#8DD4C3"
				, "#86CCED"
				, "#7EA6E9"
				, "#BC95E8"
				, "#F78880"
				, "#C600FF"
				, "#FF8000" 
				, "#E5CC80"
				, "#F48CBA"
				, "#AAD372"
				, "#FFD100"
				, "#FF0000"
				, "#00FF00"
				, "#71D5FF"
				, "#FFFF98"
				, "#6D6E70"
				, "#0081FF"
				, "#FFF468"
				, "#9382C9"
				, "#00FFBA"
				, "#88AAFF"
				, "#40BF40"
				, "#FFFF00"
				, "#634E37"
				, "#7A465D"
				, "#556A39"
				, "#807A34"
				, "#620F1E"
				, "#122D80"
				, "#346678"
				, "#4A4165"
				, "#803E05"
				, "#"
				, "#"
				, "#"
				, "#"
				]
			
			var page = 0;
			$(document).ready(function() {
				bindPopupEvent("#m_pop99mdf", ".open99");

				if($("#lacaListAdd span").length == 1)
					$("#laca").closest("div.s1_uselectbox").hide();
											
				$("#lacaListAdd span:eq(0)").click().change();
				$("#lacaPopupList span:eq(0)").click().change();
				
				$(document).on("change", "input[name='all_day_flag']", function(index){
					if($(this).prop("checked")){
						$("input[name='as_stime']").val("");
						$("input[name='as_etime']").val("");
						$("input[name='as_stime']").prop("disabled", true);
						$("input[name='as_etime']").prop("disabled", true);
						$("input[name=all_day]").val("Y");
					}else{
						$("input[name='as_stime']").prop("disabled", false);
						$("input[name='as_etime']").prop("disabled", false);
						$("input[name=all_day]").val("N");						
					}
				});
				
				 $('#calendar1').fullCalendar({
					header: {
						left: 'prev,next today',
						center: 'title',
						right: 'month,agendaTwoWeek,agendaWeek,agendaDay,listWeek'
					},
					navLinks: true,
					editable: true,
					locale: "ko",
					eventLimit: true,
					fixedWeekCount: true,	//true: 6주로 고정
					allDayDefault: true,
					viewRender : function (view, element) {
						getAcaSchedule();
					},
					eventDrop: function(event, delta, revertFunc) {
						revertFunc();
						return;
						
				    },
				    eventResize: function(event, delta, revertFunc) {
				    	revertFunc();
				    },
					views: {
						agendaTwoWeek: {
							type: 'basic',
							duration: { weeks: 2 },
							buttonText: '2주'
						}
					},
					minTime: '06:00:00',
			        maxTime: '24:00:00'
				});
				 
				//체크 셀렉트 박스
				$(".dropdown dt a").on('click', function() {
					$(this).closest("dl.dropdown").find("dd ul").slideToggle('fast');					
				});

				$(document).bind('click', function(e) {
					var $clicked = $(e.target);					
					if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();					
				});

				$(document).on("change", "input[name=holidayFlag]", function(){
					getAcaSchedule();
				});
				
				$(document).on("change", "#macaListAdd input[type=checkbox]", function(){
					var aca_name = "";
					
					if($("#macaListAdd input[type=checkbox]:checked").length != 0){
						if($("#macaListAdd input[name=chkAll]").prop("checked")){
							aca_name = $("#macaListAdd input[name=chkAll]").closest("li").text();
						}else{
							if($("#macaListAdd input[type=checkbox]:checked").length == 1)
								aca_name = $("#macaListAdd input[type=checkbox]:checked").closest("li").text();
							else{
								aca_name = "일부 선택("+$("#macaListAdd input[type=checkbox]:checked").length+")";
							}
						}
					}
					
					$("#selectShow").text(aca_name);
				});
				
				$(document).on("change", "#macaListAddPopup input[type=checkbox]", function(){
					var aca_name = "";
					
					if($("#macaListAddPopup input[type=checkbox]:checked").length != 0){
						if($("#macaListAddPopup input[name=common]").prop("checked")){
							aca_name = $("#macaListAddPopup input[name=common]").closest("li").text();
						}else{
							if($("#macaListAddPopup input[type=checkbox]:checked").length == 1)
								aca_name = $("#macaListAddPopup input[type=checkbox]:checked").closest("li").text();
							else{
								aca_name = "일부 선택("+$("#macaListAddPopup input[type=checkbox]:checked").length+")";
							}
						}
					}
					
					$("#popuptSelectShow").text(aca_name);
				});				
			});
				
			//학사일정 가져오기
			function getAcaSchedule() {
				//학사체계 선택된게 없으면..
				if($("#acaSelectAdd label").length == 0)
					return;
				
				var selectAca = [];
				var holiday_flag = "N";
				
				//휴일 체크 확인
				if($("input[name=holidayFlag]").prop("checked"))
					holiday_flag = "Y";
				
				//공통 선택유무
				if($("#acaSelectAdd label[data-name=common]").length != 0)
					selectAca.push("''");
				
				//학사체계 선택된거
				$.each($("#acaSelectAdd label:not([data-name=common])"), function(index){
					selectAca.push("'"+$(this).attr("data-name")+"'");
				});				
				
				var dateStart = $('#calendar1').fullCalendar('getView').start.format("YYYY-MM-DD");
				var dateEnd = $('#calendar1').fullCalendar('getView').end.format("YYYY-MM-DD");
				
		        $.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/acaSchedule/scheduleManagement/list",
		            data: {
		            	"dateStart" : dateStart,
		            	"dateEnd" : dateEnd,
		            	"aca_system_seq" : selectAca.join(","),
		            	"holiday_flag" : holiday_flag,
		            	"laca_seq" : $("#laca").attr("data-value")		            	 
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	var htmls = "";

		            	var eventList = [];
		            	var color_index = 0;
		            	
		            	$("#acaScheduleListAdd").empty();

		            	$.each(data.list, function(index){
		            		var startDate = this.as_sdate;
		                	var endDate = this.as_edate;
		                	var eventMinute = "";
		                	var allDay_flag;
		                	
		                	if(this.as_target==1){
		                		color_index = 0;		                		
		                	}else{
		                		color_index = $("#acaSelectAdd").find("label[data-name="+this.aca_system_seq+"]").attr("data-colorindex");
		                	}	                		
		                	
		                	/* 
		                		종일일 때 시작일 종료일 다른 경우 마지막일이 하루 없어져서 표시됨
		                				                	
		                	*/ 
		                	if (this.as_stime != null && this.as_etime != null) {		                		
		                		startDate += "T" + this.as_stime;
		                		endDate += "T" + this.as_etime;
			                	eventMinute = ampmTimeToMinute(this.as_etime) - ampmTimeToMinute(this.as_stime);
			                	allDay_flag = false;
		                	}else{
		                		startDate += "T00:00:00";
		                		endDate += "T24:00:00";
		                	}
		                			                	
		            		var asEventList =	{
	            				event_type: "acas",
		                		as_seq : this.as_seq,
		                		title: this.as_name,
								url: 'javascript:scheduleEditPopup(' + this.as_seq + ');',
								start: startDate,
								end: endDate,
								cal_date : this.cal_date,
								event_minute: eventMinute,
		    					color : color[color_index],
		    					textColor : "white",
		    					allDay : allDay_flag
		                	};
		            		
		            		

		            		eventList.push(asEventList);
		            		
		            		var aca_system_name = this.aca_system_name;
		            		var date = "";
		            		
		            		if(this.aca_system_name == "common")
		            			aca_system_name = "공통";
		            		
		            		if(this.as_sdate==this.as_edate)
		            			date = this.as_sdate;
		            		else
		            			date = this.as_sdate+" ~ "+this.as_edate_mmdd;
		            			
		            		
		            		htmls = '<tr>'
								+'<td class="w_ic"><span class="am_option" style="background:'+color[color_index]+';width:15px;height:15px;"></span>['+aca_system_name+']</td>'
								+'<td class="w_con">'
								+'<span class="am_opt_tt">'+this.as_name+'</span>' 
								+'<span class="am_opt_tts">'+date+'</span>'
								+'</td>'
								+'<td class="w_btn">'
								+'<button class="schd_btn1 open99mdf" onclick="scheduleEditPopup(' + this.as_seq + ');">수정</button>'
								+'<button class="schd_btn2" onClick="acaScheduleDelete(' + this.as_seq + ');">삭제</button>'
								+'</td>'
								+'</tr>';
							$("#acaScheduleListAdd").append(htmls);
		            	});

		            	$('#calendar1').fullCalendar('removeEvents');
		                $('#calendar1').fullCalendar('addEventSource', eventList);
		            },
		            error: function(xhr, textStatus) {
		                alert("오류가 발생했습니다.");
		                //document.write(xhr.responseText);
		            }
		        });
		    }
			
			//종,소분류 가져오기 메인
			function getAcaMLList(seq){
				if($("#laca").attr("data-value") != seq){
					$("#acaScheduleListAdd").empty();
					$("#acaSelectAdd").empty();
	            	$('#calendar1').fullCalendar('removeEvents');
				}else{
					return;
				}
            	
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/acaSchedule/scheduleManagement/msAcaList",
		            data: {
		            	"aca_system_seq" : seq		            	
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		var htmls = '<li><input type="checkbox" name="chkAll" class="selet_chk" value="all" onClick="allAcaChk(this);">학사체계전체</li>'
		            		+ '<li><input type="checkbox" name="chk" class="selet_chk" value="0_common" onClick="acaSelect(this);">공통</li>';
		            		$.each(data.acaMSList, function(index){
		            			htmls += '<li><input type="checkbox" name="chk" class="selet_chk" onClick="acaSelect(this);" value="'+(index+1)+'_'+this.aca_system_seq+'">'+this.aca_system_name+'</li>'
		            		});
		            		$("#macaListAdd").html(htmls);
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
			
			//학사체계 전체 선택
			function allAcaChk(obj){
				if($(obj).prop("checked") == true){
					$("#macaListAdd").find("input[name=chk]").prop("checked", true);
					$("#acaSelectAdd").empty();
					$.each($("#macaListAdd").find("input[name=chk]"), function(index){
						var htmls = '<label class="lable_opt" data-name="'+$(this).val().split("_")[1]+'" data-colorIndex="'+$(this).val().split("_")[0]+'"><span class="am_option" style="width:15px;height:15px;background:'+color[$(this).val().split("_")[0]]+'"></span><span class="am_option">'+$(this).closest("li").text();+'</span></label>';				
						$("#acaSelectAdd").append(htmls);
					});
					selectAcaShow();
				}else{
					$('#calendar1').fullCalendar('removeEvents');
	            	$("#acaScheduleListAdd").empty();
					$("#acaSelectAdd").empty();
					$("#macaListAdd").find("input[name=chk]").prop("checked", false);
				}				
			}
			
			//학사체계 선택했을 경우
			function acaSelect(obj){
				var index = $(obj).val().split("_")[0];
				var aca_seq = $(obj).val().split("_")[1];
				var name = $(obj).closest("li").text();
				if($(obj).prop("checked") == true){
					var htmls = '<label class="lable_opt" data-name="'+aca_seq+'" data-colorIndex="'+index+'"><span class="am_option" style="width:15px;height:15px;background:'+color[index]+'"></span><span class="am_option">'+name+'</span></label>';				
					$("#acaSelectAdd").append(htmls);	
					selectAcaShow();
				}else{
					$("#acaSelectAdd label[data-name="+aca_seq+"]").remove();
					selectAcaShow();
				}				
				$("#macaListAdd input[name=chkAll]").prop("checked", false);
			}
			
			//학사체계 선택된거 보여준다. 5개씩
			function selectAcaShow(){
				if($("#acaSelectAdd label").length <= 5)
					page = 0;
				
				if(page < 0)
					page = 0;
				
				if(parseInt($("#acaSelectAdd label").length/5) <= page){
					page = parseInt($("#acaSelectAdd label").length/5);
				}
				
				var start = page*5;
				var end = page*5+5;

				$("#acaSelectAdd label").hide();
				$("#acaSelectAdd label").slice(start, end).show();
				getAcaSchedule();
			}
			
			//선택된거 넥스트
			function nextPage(){
				page+=1;
				selectAcaShow();
			}
			
			//선택된거 이전
			function prePage(){
				page-=1;
				selectAcaShow();
			}
			

			//중,소분류 가져오기 팝업에서
			function getAcaMLListPop(seq){
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/acaSchedule/scheduleManagement/msAcaList",
		            data: {
		            	"aca_system_seq" : seq		            	
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		var htmls = '<li><input type="checkbox" name="common" class="selet_chk" value="Y" onClick="chkCommon(this);">공통</li>';
		            		$.each(data.acaMSList, function(index){
		            			htmls += '<li><input type="checkbox" name="chk" class="selet_chk" value="'+this.aca_system_seq+'" onClick="chkAca(this);">'+this.aca_system_name+'</li>'
		            		});
		            		$("#macaListAddPopup").html(htmls);
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
			
			//팝업에서 공통 선택 했을 경우
			function chkCommon(obj){
				if($(obj).prop("checked") == true){
					$("#macaListAddPopup input[name=chk]").prop("checked", false);
				}
			}
			
			//팝업에서 학사체계 선택 했을 경우
			function chkAca(obj){
				if($(obj).prop("checked") == true){
					$("#macaListAddPopup input[name=common]").prop("checked", false);
				}
			}
			
			//일정 저장
			function caSubmit() {
				var save_modify = "저장 하시겠습니까?";
				var save_complete = "저장 완료하였습니다.";
				if(!isEmpty($("input[name=ca_seq]").val())){
					save_modify = "수정 하시겠습니까?";
					save_complete ="수정 완료하였습니다.";					
				}
				
				if (!confirm(save_modify)) {
					return;
				}

				if (!$.trim($(":input[name='as_name']").val())) {
					alert("일정명을 입력해주세요.");
					return;
				}
				
				if (!$(":input[name='as_sdate']").val()) {
					alert("일정 시작 날짜를 입력해주세요.");
					return;
				}
				
				if (!$(":input[name='as_edate']").val()) {
					alert("일정 종료 날짜를 입력해주세요.");
					return;
				}

				
				if(!$(":input[name='all_day_flag']").prop("checked")){
					$(":input[name='all_day_flag']").val("N");
					
					if (!$(":input[name='as_stime']").val()) {
						alert("시작시간을 선택해주세요.");
						return;
					}
					
					if (!$(":input[name='as_etime']").val()) {
						alert("종료시간을 선택해주세요.");
						return;
					}

					var start = $(":input[name='as_sdate']").val() + " " + $(":input[name='as_stime']").val();
					var end = $(":input[name='as_edate']").val() + " " +  $(":input[name='as_etime']").val();
					
					var startDate = new Date(start);
					var endDate = new Date(end);
					
					if(startDate.getTime()>=endDate.getTime()){
						alert("종료시간을 시작시간 이후로 입력해주세요\n일자 또는 시간을 확인하세요.");
						return;
					}
				}else{
					$(":input[name='all_day_flag']").val("Y");
				}
				
				if($("input[name=holiday_flag]").prop("checked")){
					$("input[name=holiday]").val("Y");
				}else{
					$("input[name=holiday]").val("N");
				}
				
				if($("#macaListAddPopup input[name=common]").prop("checked")){
					$("input[name=as_target]").val("1");
				}else{
					$("input[name=as_target]").val("2");
				}
				
				if($("#macaListAddPopup input[type=checkbox]:checked").length == 0){
					alert("학사체계를 선택해주세요.");
					return;
				}				
				
				var form = document.getElementById("caForm");
				
				var hiddenField = document.createElement("input");
				
				hiddenField.type = "hidden";
				hiddenField.name = "laca_seq";
				hiddenField.value = $("#lacaPopup").attr("data-value");
				
				form.appendChild(hiddenField);
				
				
				$(":input[name='as_stime']").prop("disabled", false);
				$(":input[name='as_etime']").prop("disabled", false);
				
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/acaSchedule/scheduleManagement/insert",
		            data: $("#caForm").serialize(),	
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		alert(save_complete);
		            		reset();
		            		$("#m_pop99mdf").hide();
		            		getAcaSchedule();
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
			
			//일정 가져오기
			function scheduleEditPopup(as_seq){
				reset();
				
				$("#m_pop99mdf").show();
				
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/acaSchedule/scheduleManagement/oneList",
		            data: {
		            	"as_seq" : as_seq	
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		$("input[name=as_seq]").val(data.listH.as_seq);
							$("#lacaPopupList span[data-value="+data.listH.laca_seq+"]").click();
							if(data.listH.holiday_flag == "Y")
								$("input[name=holiday_flag]").prop("checked", true);
							else
								$("input[name=holiday_flag]").prop("checked", false);
							
							$("input[name=as_name]").val(data.listH.as_name);
							$("input[name=as_sdate]").val(data.listH.as_sdate);
							$("input[name=as_edate]").val(data.listH.as_edate);							
							$("input[name=as_stime]").val(data.listH.as_stime_12h);
							$("input[name=as_etime]").val(data.listH.as_etime_12h);
							$("input[name=as_place]").val(data.listH.as_place);
							$("textarea[name=as_content]").val(data.listH.as_content);
							
							if(data.listH.all_day_flag=="Y")
								$("input[name=all_day_flag]").prop("checked", true);
							else
								$("input[name=all_day_flag]").prop("checked", false);
							
							setTimeout(function(){
								if(data.listH.as_target == 1)
									$("#macaListAddPopup").find("input[name=common]").prop("checked", true).change();
								else{
									$.each(data.listI, function(index){
										$("#macaListAddPopup").find("input[value="+this.aca_system_seq+"]").prop("checked", true).change();	
									});
								}								
							},1000);
							
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
			
			function acaScheduleChange(as_seq, sdate, edate, start_time, event_minute, allDay, cal_date) {
				//종료시간 계산
				var endMinute = (Number(start_time.substring(0, 2)) * 60) + Number(start_time.substring(3, 5)) + event_minute;
				var end_time = Math.floor(endMinute / 60) + ":" + endMinute % 60;
				
				if(allDay){
					start_time="";
					end_time="";
				}
					
				
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/acaSchedule/scheduleManagement/modify",
		            data: {
		            	"as_seq" : as_seq,
		            	"as_sdate" : sdate,
		            	"cal_date" : cal_date,
		            	"as_stime" : start_time,
		            	"as_etime" : end_time
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if (data.status == "200") {
		            		alert("변경 되었습니다.");
		    				$("#m_pop99mdf").hide();
		    				getAcaSchedule();
		            	} else {
							alert(data.msg);
						}
		            },
		            error: function(xhr, textStatus) {
		                alert("오류가 발생했습니다.");
		                //document.write(xhr.responseText);
		            }
		        });
			}
			
			function acaScheduleDelete(as_seq){
				if(!confirm("해당 일정을 삭제하시겠습니까?")){
					return;
				}
			
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/acaSchedule/scheduleManagement/delete",
		            data: {
		            	"as_seq" : as_seq
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if (data.status == "200") {
		            		alert("삭제 완료 되었습니다.");
		    				getAcaSchedule();
		            	} else {
							alert(data.msg);
						}
		            },
		            error: function(xhr, textStatus) {
		                alert("오류가 발생했습니다.");
		                //document.write(xhr.responseText);
		            }
		        });
			}
			
			function reset(){
				$("input[name=as_seq]").val("");
				$("#lacaPopupList span:eq(0)").click();
				$("input[name=holiday_flag]").prop("checked", false);
				$("input[name=as_name]").val("");
				$("input[name=as_sdate]").val("");
				$("input[name=as_edate]").val("");
				$("input[name=as_stime]").val("");
				$("input[name=as_etime]").val("");
				$("input[name=all_day_flag]").prop("checked", false);
				$("input[name=as_place]").val("");				
				$("textarea[name=as_content]").val("");				
			}
		</script>
	</head>
	
	<body>
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
		
		<!-- s_container_table -->
		<div id="container" class="container_table">
			<!-- s_contents -->
			<div class="contents main">
	
				<!-- s_left_mcon -->
				<div class="left_mcon aside_l">   
					<!-- s_학습자료 메뉴 -->
					<div class="sub_menu st_am">
					    <div class="title">학사일정</div>   
					    <ul class="sub_panel">
					        <li class="mn"><a href="${HOME }/aca/academicM" class="on">학 사 력</a></li>
					        <c:if test='${S_USER_LEVEL == 3 }'>
					        	<li class="mn"><a href="${HOME }/aca/MYscheduleM" class="">MY 시간표</a></li>	
								<li class="mn"><a href="${HOME }/aca/MYscheduleMemo" class="">개인일정</a></li>
							</c:if>
					       	<c:if test="${S_USER_LEVEL == 4}">
					       		<li class="mn"><a href="${HOME }/aca/MYscheduleMST" class="">MY 시간표</a></li>
								<li class="mn"><a href="${HOME }/aca/MYscheduleMemo" class="">개인일정</a></li>
					        	<li class="mn"><a href="${HOME }/st/aca/caList" class="">수강신청</a></li>
					       	</c:if>
					        
					    </ul>
					</div>
					<!-- e_학습자료 메뉴 -->
				</div>
				<!-- e_left_mcon -->
	
				<!-- s_main_con -->
				<div class="main_con adm_schd">
					<!-- s_메뉴 접기 버튼 -->
					<a class="btn_l" style="z-index:99;"><span>좌측 메뉴 접기/펴기</span></a>
					<!-- e_메뉴 접기 버튼 -->
	
					<!-- s_tt_wrap -->
					<div class="tt_wrap" style="padding:15px 0 0 0;margin:0 0 0 20px;">					
						<h3 class="am_tt">
							<span class="tt">학사일정<span class="sign1">&gt;</span>학사력</span>
						</h3>
					</div>
					<!-- e_tt_wrap -->
	
					<!-- s_adm_s_wrap -->
					<div class="adm_s_wrap">
	
						<!-- <button class="btn_r open99" onclick="reset();">일정 등록</button> -->
	
						<!-- s_sch_wrap -->
						<div class="sch_wrap">
	
							<span class="c_lb"><input type="checkbox" class="ip_chk1" name="holidayFlag">휴일</span>
	
							<!-- s_s1_uselectbox -->
							<div class="s1_uselectbox">
								<div class="uselectbox">
									<span class="uselected" id="laca" data-value=""></span> <span class="uarrow">▼</span>
	
									<div class="uoptions" id="lacaListAdd">
										<c:forEach var="lacaList" items="${lacaList}">
                                           <span class="uoption" data-value="${lacaList.aca_system_seq}" onclick="getAcaMLList('${lacaList.aca_system_seq}');">${lacaList.aca_system_name}</span>
                                       </c:forEach>
                                    </div>
								</div>
							</div>
						
							<dl class="dropdown">

								<dt>
									<a href="#" class="multiSelect" id="selectShow" >									
									</a>
									<span class="uarrow" style="width:30px;">▼</span>
								</dt>
								<dd>
									<div class="mutliSelect">
										<ul id="macaListAdd">
										
										</ul>
									</div>
								</dd>
							</dl>
						</div>
						<!-- e_sch_wrap -->
	
					</div>
					<!-- e_adm_s_wrap -->
	
					<div class="schd_box" style="padding:0;margin-top:10px;">
	
						<div class="btn_wrap_am">
	
							<a href="#" class="ic_ar1" onClick="prePage();"></a>
							<!-- s_am_option -->
							<div class="am_option_wrap" id="acaSelectAdd" style="height:30px;">
								
							</div>
							<!-- e_am_option -->
							<a href="#" class="ic_ar2" onClick="nextPage();"></a>
						</div>
	
	
						<div class="am_tbwrap">
	
							<!-- s_calendar1 -->
							<div id="calendar1"></div>
							<!-- e_calendar1 -->
	
							<!-- s_am_rt_wrap -->
							<div class="am_rt_wrap">
								<!-- s_lms_table_list -->
								<!-- <table class="lms_table_list top">
									<thead>
										<tr class="">
											<th class="th01">
												s_주간 날짜
												<div class="ic_wrap">
													<span class="sp_tt bf">[ 주별 ]</span> <a href="#"
														class="ic_ar01"></a> <span class="sp_tt">11월 12일 ~
														11월 18일</span> <a href="#" class="ic_ar02"></a>
												</div> s_주간 날짜
											</th>
										</tr>
									</thead>
								</table> -->
	
								<!-- s_wrap_table_list -->
								<div class="wrap_table_list">
									<table class="lms_table_list">
										<tbody id="acaScheduleListAdd">
																					
										</tbody>
									</table>
									<!-- e_lms_table_list -->
								</div>
								<!-- e_wrap_table_list -->
							</div>
							<!-- e_am_rt_wrap -->
	
						</div>
	
					</div>
	
				</div>
				<!-- e_main_con -->
	
			</div>
			<!-- e_contents -->
		</div>
		<!-- e_container_table -->
		<!-- s_ 팝업 : 일정 수정 -->
		<div id="m_pop99mdf" class="pop_up_schd_write mo99mdf">
			<!-- s_pop_wrap -->
			<form id="caForm" onSubmit="return false;">
			<input type="hidden" name="as_seq" value="">
			<input type="hidden" name="all_day" value="N">
			<input type="hidden" name="holiday" value="N">
			<input type="hidden" name="as_target" value="1">
			<div class="pop_wrap">
				<button class="pop_close close99" type="button">X</button>
				<p class="t_title">일정 확인</p>
	
				<!-- s_pop_swrap -->
				<div class="pop_swrap">
	
					<div class="swrapbf">
						<!-- s_s2_uselectbox -->
						<div class="s2_uselectbox" style="width:35%;">
							<div class="uselectbox">
								<span class="uselected" id="lacaPopup" data-value=""></span> <span class="uarrow">▼</span>
	
								<div class="uoptions" id="lacaPopupList">
									<c:forEach var="lacaList" items="${lacaList}">
                                        <span class="uoption" data-value="${lacaList.aca_system_seq}" onclick="getAcaMLListPop('${lacaList.aca_system_seq}');">${lacaList.aca_system_name}</span>
                                    </c:forEach>
								</div>
							</div>
						</div>
						<dl class="dropdown">
							<dt>
								<a href="#" class="multiSelect" id="popuptSelectShow"></a>
								<span class="uarrow" style="width:30px;">▼</span>
							</dt>
							<dd>
								<div class="mutliSelect">
									<ul id="macaListAddPopup">
									
									</ul>
								</div>
							</dd>
						</dl>
						<!-- e_s2_uselectbox -->
						<span class="c_lb" style="float:right;margin:0;">
							<span class="tt_bf">휴일 여부</span>
							<input type="checkbox" class="ip_chk1" name="holiday_flag">
							<span class="tt_af">휴일</span>
						</span>
					</div>
	
					<div class="swrap">
						<span class="tt">일정명</span>
						<div class="con">
							<input type="text" class="ip_ta1" name="as_name">
						</div>
					</div>
	
					<div class="swrap">
						<span class="tt">날짜</span>
						<div class="con">
							<div class="twrap1">
								<input type="text" class="ip_date adm dateyearpicker-input_1" value="" name="as_sdate"> 
								<span class="sign">~</span> 
								<input type="text" class="ip_date adm dateyearpicker-input_1" value="" name="as_edate">
							</div>
						</div>
					</div>
	
					<div class="swrap time_pick2">
						<span class="tt">시간</span>
						<div class="con">
							<input type="text" class="timepicker1 ip_time" value="" name="as_stime"> 
							<span class="sign">~</span>
							<input type="text" class="timepicker2 ip_time" value="" name="as_etime">
	
							<div class="cwrap adm">
								<input type="checkbox" class="chk" name="all_day_flag"> <span class="tt2">종일</span>
							</div>
						</div>
					</div>
	
					<div class="swrap">
						<span class="tt">장소 (선택)</span>
						<div class="con">
							<!-- 미등록 시 class : nonregi 추가 -->
							<input type="text" class="ip_tt1" value="" name="as_place">
						</div>
					</div>
	
					<div class="swrap tarea free_textarea">
						<span class="tt t_a">일정 내용 (선택)</span>
						<div class="con">
							<!-- 미등록 시 class : nonregi 추가 -->
							<textarea class="tarea1" style="height: 40px;" name="as_content"></textarea>
						</div>
					</div>
	
				</div>
			</div>
			</form>
		</div>
		<!-- e_pop_wrap -->
</body>
</html>