<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">

	$(document).ready(function() {
		getLessonPlanTitle();
		getFormationEvaluationList();
	});
    
	function getLessonPlanTitle(){
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/st/lesson/lessonPlanTitle",
            data: {                 
            },
            dataType: "json",
            success: function(data, status) {
            	if(data.status=="200"){
	            	var times = "", sosok = "", attendance = "", pf_name = "";
	            	$('#attendance').removeAttr('class');

	            	if(data.attendance == "0")
	            		attendance = "t4";
	            	else if(data.attendance == "00")
	            		attendance = "t3";
	            	else if(data.attendance == "02")
	            		attendance = "t2";
	            	else if(data.attendance == "03")
	            		attendance = "t1";
	            	
	            	$('#attendance').addClass(attendance);
	            	if(!isEmpty(data.lessonPlan.specialty))
	            		sosok="("+data.lesson_plan.specialty+")";
	            	if(!isEmpty(data.lessonPlan.start_time) && !isEmpty(data.lessonPlan.end_time))
	            		times = data.lessonPlan.start_time+"~"+data.lessonPlan.end_time;
	            	
	            	if(isEmpty(data.lessonPlan.name))
	            		pf_name = "교수 미등록";
	            	else
	            		pf_name = data.lessonPlan.name+sosok;
	            	$("span[name=lessonDate]").text(data.lessonPlan.lesson_date);
	            	$("span[name=lessonTime]").text("["+data.lessonPlan.period+"교시] " + times);
	            	$("span[name=lessonSubject]").text(data.lessonPlan.lesson_subject);
	            	$("span[name=pfName]").text(pf_name);
            	}
            },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            }
        });		
	}
	
	
	function getFormationEvaluationList(){
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/st/lesson/formationEvaluation/list",
            data: {                 
            },
            dataType: "json",
            success: function(data, status) {
            	if(data.status == "200"){
            		var htmls="";
            		$.each(data.feList,function(){
            			var state = '', state_num = '', picture = '', date = '', htmls_2 = '';
            			var lesson_date = this.lesson_date.split("/");
            			if(this.test_state == "WAIT"){
            				state = "대기 중";
            				state_num = "1";
            				htmls_2 = "";
            			}
            			else if(this.test_state == "START"){
            				var bar = 0;
            				if(this.quiz_submit_cnt != 0)
            					bar = this.quiz_submit_cnt / this.quiz_cnt * 200;
            				
            				state = "진행 중";
            				state_num = "2";
            				
            				htmls_2 = '<div class="wrap_prg">';
            				htmls_2 += '<span class="sp1">풀이현황 : '+this.quiz_submit_cnt+' / '+this.quiz_cnt+'</span>';
            				htmls_2 += '<div class="prg">';
            				htmls_2 += '<span class="bar" style="width: '+bar+'px;"></span>';
            				htmls_2 += '</div>';
            				htmls_2 += '</div>';
            			}
            			else if(this.test_state == "END"){
            				state = "평가 완료";
            				state_num = "3";
            				
            				htmls_2 = '<div class="q_wrap1"><span>평균 : '+this.avg_score+'</span><span>나의 점수 : '+this.score+'</span></div>';
            			}
            			
            			if(!isEmpty(this.thumbnail_name))
            				picture = this.thumbnail_path;
            				
            			date = lesson_date[0] + '월 '+lesson_date[1] + '일 '+this.period+'교시 '+this.start_time+'~'+this.end_time;
            			
            			
	            		htmls='<table class="tab_table ttb1">';
	            		htmls+='<tbody>';
	            		htmls+='<tr class="tr01">';
	            		htmls+='<td colspan="2" class=""><span class="tt_1">'+this.code_name+'</span>';
	            		htmls+='<span class="tt_2_'+state_num+'">'+state+'</span></td>';
	            		htmls+='</tr>';
	            		htmls+='<tr class="tr01">';
	            		htmls+='<td class="th02 w1">';
	            		htmls+='<div class="in_wrap">';
	            		htmls+='<div class="in_wrap_s">';
	            		if(!isEmpty(picture))
	            			htmls+='<img src="${RES_PATH}'+picture+'" alt="표지 사진" class="pt1">';
	            		htmls+='</div>';
	            		htmls+='</div>';
	            		htmls+='</td>';
	            		htmls+='<td class="">';
	            		htmls+='<div class="wrap_tt">';
	            		htmls+='<span class="sp1s">'+date+'</span>'; 
	            		htmls+='<span class="sp1">'+this.lesson_subject+'</span>';
	            		htmls+='</div>';
	            		htmls+=htmls_2;
	            		htmls+='</td>';
	            		htmls+='</tr>';
	            		htmls+='</tbody>';
	            		htmls+='</table>';
	            		
	            		$("#tab001").append(htmls);
            		});
            		
            		if(data.feList.length == 0){
            			htmls += '<div class="exam"> <div class="cont"> </div></div>';
	            		$("#tab001").append(htmls);
            		}
            			
            	}else{
            		alert("형성평가 목록가져오기 실패");
            	}
            },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            }
        });		
	}
</script>
</head>
<body>
	<!-- s_tt_wrap -->
	<div class="tt_wrap cc">
		<h3 class="am_tt">
			<span class="h_date" name="lessonDate"></span> <span class="tt"
				name="lessonTime"></span> <span class="tt_s" name="lessonSubject"></span>
		</h3>

		<div class="r_wrap">
			<!-- class:  출석 t1, 지각 t2, 결석 t3 -->
			<span class="" id="attendance"></span>

			<div class="wrap2">
				<span class="a_mp"> <span class="pt01"> <img src=""
						name="pfPicture" class="ph_img">
				</span> <span class="ssp1" name="pfName"></span></span>
			</div>
		</div>
	</div>
	<!-- e_tt_wrap -->

	<!-- s_tab_wrap_cc -->
	<div class="tab_wrap_cc">
		<button class="tab01 tablinks" onclick="pageMoveLessonPlanView(${S_LP_SEQ},${S_CURRICULUM_SEQ });">수업계획서</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/lessonData'">수업자료</button>
		<button class="tab01 tablinks active" onclick="location.href='${HOME}/st/lesson/formationEvaluation'">형성평가</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/st/lesson/assignMent'">과제</button>
		<button class="tab01 tablinks long" onclick="alert('작업중인 페이지 입니다.');">수업만족도 조사</button>
	</div>
	<!-- e_tab_wrap_cc -->


	<!-- s_tabcontent -->
	<div id="tab001" class="tabcontent tabcontent4">
		
	</div>
	<!-- e_tabcontent -->

</body>
</html>