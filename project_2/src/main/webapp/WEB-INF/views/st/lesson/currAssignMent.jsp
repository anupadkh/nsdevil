<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">

	$(document).ready(function() {
		getAssignMentList();
		getCurriculum();
		$(document).on("click",".btnDel", function(){
			var obj = $(this);
			//저장값 있으면 디비에서 삭제
			if(!isEmpty($("input[name=asgmt_submit_seq]").val())){
				if(confirm("첨부파일을 삭제하시겠습니까?")){
					$.ajax({
			            type: "POST",
			            url: "${HOME}/ajax/st/lesson/assignMent/submitFileDel",
			            data: {    
			            	"asgmt_submit_seq" : $("input[name=asgmt_submit_seq]").val()
			            },
			            dataType: "json",
			            success: function(data, status) {
			            	if(data.status=="200")
			            		obj.closest("div.set").remove();
			            	else
			            		alert("실패");
			            },
			            error: function(xhr, textStatus) {
			                //alert("오류가 발생했습니다.");
			                document.write(xhr.responseText);
			            }
			        });				
				}
			}else{
				$obj.closest("div.set").remove();
			}				
		});
	});
	

	//교육과정 가져오기
	function getCurriculum() {
		$.ajax({
			type : "GET",
			url : "${HOME}/ajax/pf/lesson/curriculumInfo",
			data : {},
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {

					$("#curr_name").text(data.basicInfo.curr_name);
					$("#aca_system_name").text("(" + data.basicInfo.aca_system_name + ")");
                    
				} else {

				}
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}
	
	
	function getAssignMentList(){
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/st/lesson/assignMent/list",
            data: {           
            	"asgmt_type" : "1"
            },
            dataType: "json",
            success: function(data, status) {
            	if(data.status=="200"){
            		$("#assignMentListAdd").show();
            		$("#submitDiv").hide();
            		$("#assignMentListAdd").empty();
	            	var htmls = '', submit_yn = '', submit_class = '', state_yn = '', state_class = '', day = '';
	            	$.each(data.assignMentList,function(){
	            		if(this.submit_yn == 0){
	            			submit_class='tt_x';
	            			submit_yn='미제출(제출하기)';
	            		}else{
	            			submit_class='tt_s';
	            			submit_yn='제출완료(수정하기)';
	            			if(this.feedbackcnt != 0)
	            				submit_yn='제출완료(수정불가)';            			
	            		}
	            			
	            		if(this.assignment_state == "1"){
	            			state_yn = '대기중';
	            			state_class='tt_2_2';
	            		}else if(this.assignment_state == "2"){
	            			state_yn = '진행중';
	            			state_class='tt_2_2';
	            		}else{
	            			state_yn = '마감';
	            			state_class='tt_2_3';
	            		}
	            		
	            		switch(this.day){
		                    case 0 : day="일";break;
		                    case 1 : day="월";break;
		                    case 2 : day="화";break;
		                    case 3 : day="수";break;
		                    case 4 : day="목";break;
		                    case 5 : day="금";break;
		                    case 6 : day="토";break;
		                }
	            		
	            		htmls='<table class="tab_table ttb1">';
	            		htmls+='<tbody>';
	            		htmls+='<tr class="tr01">';
	            		htmls+='<td class="w1">';
	            		htmls+='<div class="in_wrap">';
	            		htmls+='<span class="'+state_class+'">[ '+state_yn+' ]</span> <span class="tt_1">'+this.start_date+'~'+this.end_date+'</span>';
	            		htmls+='</div>';
	            		htmls+='</td>';
	            		htmls+='<td class="w6">';
	            		htmls+='<button class="'+submit_class+'" onclick="detailAssignMentSubmit('+this.asgmt_seq+')">'+submit_yn+'</button>';
	            		htmls+='<div class="wrap_tt">';							
	            		htmls+='<span class="sp1">'+this.asgmt_name+'</span>';
	            		htmls+='</div>'; 
	            		htmls+='<span class="tt_4">';
	            		htmls+='<span class="sp_t1">마감</span>';
	            		htmls+='<span>'+this.end_date+' ('+day+')</span>';
	            		//htmls+='<span>19:00</span>';
	            		htmls+='</span>';
	            		htmls+='</td>';
	            		htmls+='</tr>';
	            		htmls+='</tbody>';
	            		htmls+='</table>';
	            		$("#assignMentListAdd").append(htmls);
	            	});
            	}else{
            		alert("과제 목록 가져오기 실패");
            	}
            },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            }
        });	
	}
	
	function detailAssignMentSubmit(asgmt_seq){
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/st/lesson/assignMent/Onelist",
            data: {         
            	"asgmt_seq" : asgmt_seq            	
            },
            dataType: "json",
            success: function(data, status) {
            	if(data.status=="200"){
            		$("#assignMentListAdd").hide();
            		$("#submitDiv").show();
            		
            		//피드백 받은 과제는 제출버튼 숨긴다. 저장 시 컨트롤러에서 다시 체크함.
            		if(!isEmpty(data.assignMentSubmit)){
	            		if(data.assignMentSubmit.feedbackcnt != 0){
	            			$("#submitBtn").hide();
	            			$(".feedback").show();
	            			$("td[data-name=feedback_content]").html(data.assignMentSubmit.feedback_content);
	            			$("span[data-name=score]").text(data.assignMentSubmit.score);
	            		}else{
	            			$("#submitBtn").show();
	            			$(".feedback").hide();
	            		}
            		}else{
            			$("#submitBtn").show();
            			$(".feedback").hide();
            		}
            		
            		//조원은 제출버튼 숨긴다.
            		if(data.assignMent.group_flag == "Y" && data.assignMent.group_leader_flag == "N")
						$("#submitBtn").hide();
            		
            		$("input[name=group_flag]").val(data.assignMent.group_flag);
            		
            		
            		
            		if(data.assignMent.assignment_state == "3")
            			$("div[name=expire]").show();
            		else
            			$("div[name=expire]").hide();
            		
            		var start_day = '', end_day = '', htmls = '';
            		
            		$("span[name=asgmt_name]").text(data.assignMent.asgmt_name);
            		$("input[name=asgmt_seq]").val(asgmt_seq);
            		
            		if(data.assignMent.group_flag == 'N')
            			$("div[name=groupYN]").html('<span class="tt_3_1">개별과제</span>');
            		else
            			$("div[name=groupYN]").html('<span class="tt_3_2">그룹과제</span>');
            		
            		switch(data.assignMent.start_day){
						case 0 : start_day="일";break;
						case 1 : start_day="월";break;
						case 2 : start_day="화";break;
						case 3 : start_day="수";break;
						case 4 : start_day="목";break;
						case 5 : start_day="금";break;
						case 6 : start_day="토";break;
					}
            		
            		switch(data.assignMent.end_day){
						case 0 : end_day="일";break;
						case 1 : end_day="월";break;
						case 2 : end_day="화";break;
						case 3 : end_day="수";break;
						case 4 : end_day="목";break;
						case 5 : end_day="금";break;
						case 6 : end_day="토";break;
					}
            		$("span[name=start_date]").text(data.assignMent.start_date+"("+start_day+")");
            		$("span[name=end_date]").text(data.assignMent.end_date+"("+end_day+")");
            		$("td[name=content]").html(data.assignMent.content);
            		$("td[name=content]").find("img").css("max-width","640px");
            		
            		
            		$.each(data.assignMentAttachList, function(){
            			htmls+='<li class="li_1">';
            			htmls+='<span>'+this.file_name+'</span>';
            			htmls+='<button class="dw" title="다운로드" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');">다운로드</button></li>';
            			
            		});
            		$("#assignMnetFileListAdd").html(htmls);
            		
            		if(!isEmpty(data.assignMentSubmit)){
	            		if(!isEmpty(data.assignMentSubmit.file_name)){
	            			htmls='<div class="set">';          
	            			htmls+='<div class="pt_wrap">';
	            			htmls+='<span class="sp02" style="cursor:pointer;" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+data.assignMentSubmit.file_path+'\',\''+data.assignMentSubmit.file_name+'\');">'+data.assignMentSubmit.file_name+'</span>';                      
	            			
	            			if(data.assignMentSubmit.feedbackcnt == 0)
	            				htmls+='<button class="btn_c btnDel" title="삭제하기">X</button>';      
	            			htmls+='</div></div>';
	            			
	            			$("#submitFileListAdd").html(htmls);
	            		}
	            		$("input[name=asgmt_submit_seq]").val(data.assignMentSubmit.asgmt_submit_seq);
	            		$("textarea[name=content]").val(data.assignMentSubmit.content);
            		}
            	}else{
            		alert("과제 상세보기 실패");	
            	}
            	
            },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            }
        });	
		
	}
	
	function file_nameChange(){ 
	    if($("#File").val() != ""){	   
    		var fileValue = $("#File").val().split("\\");
	        var fileName = fileValue[fileValue.length-1]; // 파일명
	    	if($("#submitFileListAdd div.set").length > 0){
		    	if(confirm("기존 파일 삭제후 선택한 파일로 대체하시겠습니까?")){
			        $("input[name=filename]").val(fileName);	
			        $("#submitFileListAdd").empty();
		    	}else{
		    		$("#File").val("");
		    	}	        
	    	}else{
		        $("input[name=filename]").val(fileName);	
		        $("#submitFileListAdd").empty();
	    	}
	    }
	}
	
	function stAssigmMentSubmit(){
		
		
		$("#asgmtSubmitForm").ajaxForm({
            type: "POST",
            url: "${HOME}/ajax/st/lesson/assignMent/create",
            dataType: "json",
            success: function(data, status){
                if (data.status == "200") {    
                	$("input[name=filename]").val("");
                	$("#File").val("");
                	if(isEmpty($("input[name=asgmt_submit_seq]").val())){
                		alert("제출 완료 되었습니다.");	
                	}else{
                		alert("수정 완료 되었습니다.");
                	}
                                                
                	getAssignMentList();reset();
                } else {
                	//저장시 피드백 받은 데이터인지 체크함.
                    alert(data.status);
                    $.unblockUI();                          
                }
                
            },
            error: function(xhr, textStatus) {
                document.write(xhr.responseText); 
                $.unblockUI();                      
            },beforeSend:function() {
                $.blockUI();                        
            },complete:function() {
                $.unblockUI();                      
            }                       
        });     
		$("#asgmtSubmitForm").submit();   		
	}
	
	function reset(){
		$("textarea[name=content]").val("");
		$("#submitFileListAdd").empty();
		$("input[name=group_flag]").val("");
		$("input[name=asgmt_seq]").val("");
		$("input[name=asgmt_submit_seq]").val("");
	}
</script>
</head>
<body>
	<!-- s_tt_wrap -->
	<div class="tt_wrap">
		<h3 class="am_tt" style="margin-left:15px;">
			<span class="tt" id="curr_name"></span>
			<span class="tt_s" id="aca_system_name"></span>
		</h3>
	</div>
	<!-- e_tt_wrap -->

	<!-- s_tab_wrap_cc -->
	<div class="tab_wrap_cc">
		<button class="tab01 tablinks" onClick="pageMoveCurriculumView(${S_CURRICULUM_SEQ});">과정 계획</button>
		<button class="tab01 tablinks" onClick="">수업시간표</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/unit'">단원관리</button>		
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/sfCurrResearch'">만족도조사</button>
		<button class="tab01 tablinks active" onClick="location.href='${HOME}/st/lesson/currAsgmt'">종합과제</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/feScore'">형성평가</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/soosiGrade'">수시성적</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/currGrade'">종합성적</button>		
	</div>
	<!-- e_tab_wrap_cc -->


	<!-- s_tabcontent -->
	<div id="tab001" class="tabcontent tabcontent5">

		<!-- s_hw_wrap-->
		<div class="hw_wrap_v" id="assignMentListAdd">

		</div>
		<!-- e_hw_wrap-->
		
		<div id="submitDiv" style="display:none;">
			<div class="st_tt_s1">
            	<span class="tt1" name="asgmt_name"></span>

	            <div class="ttwrap" name="groupYN">
				</div>
		    </div>
			    
			<!-- s_과제 자료 -->
			<table class="tab_table ttb1">
				<tbody>
					<tr class="tr01">
						<td class="pt">
							<ul class="h_list m_tb" id="assignMnetFileListAdd">
							</ul>
						</td>
					</tr>
				</tbody>
			</table> 
			<!-- e_과제 자료 --> 
			    
		    <div class="st_tt_s2">
		   		<span class="tt_4"><span class="sp_t">과제오픈일</span><span name="start_date"></span></span>
		    	<span class="tt_4"><span class="sp_t">제출마감일</span><span name="end_date"></span></span>
		    </div>
			    
			<table class="tab_table edit">
				<tbody>
					<tr class="edit_wrap">
						<td class="tarea free_textarea" name="content">
							<!-- <span class="sp2">* 예방과 관리에 적용할 수 있는 방안을 추가 기술할 경우 +1점</span> -->
			            </td>
					</tr>
				</tbody>
			</table>
			
			<form onsubmit="return false;" id="asgmtSubmitForm" enctype="multipart/form-data">
				<input type="hidden" name="group_flag" value="">
				<input type="hidden" name="asgmt_seq" value="">
				<input type="hidden" name="asgmt_submit_seq" value="${asgmt_submit_seq }">
				<table class="tab_table edit_s">
					<tbody>
						<tr>
							<td>
				<!-- tt_cm1 : 과제 제출기한이 만료 시 노출 -->
		                        <div class="tt_cm1" name="expire">* 과제 제출기한이 만료되었습니다. 제출기한이 만료되어도 과제 제출은 가능 ( 성적 감점 있음 )</div>
		                        <div class="tt_cm2">* 과제제출 시 코멘트를 등록하실 수 있습니다. ( 최대 1,000 자 )<br>* 파일이 여러 개인 경우 zip 으로 압축하여 제출하시면 됩니다.</div>
							</td>
						</tr>
						<tr>    
							<td class="tarea free_textarea">
							     <textarea class="tarea01_1" style="height: 20px;" name="content" placeholder="여기에 코멘트를 작성하실 수 있습니다."></textarea>
							     
							</td>
						</tr>
					</tbody>
				</table>
				
				<!-- s_파일 첨부 -->
				<div class="f_wrap" style="margin-bottom:15px;">
				    <table class="f_table ttb1">
				        <tbody>
				        <tr class="tr01">
				            <td rowspan="2" class="th02 w1">
				                <span class="sp01">파일 등록</span>
				            </td>
				            <td class="w2">                  
	                       		<input type="file" style="display:none;" id="File" name="File" onChange="file_nameChange();">
				                <input type="text" class="ip_pt1"  disabled="disabled" placeholder="파일을 선택해주세요." name="filename">
				            </td>
				            <td class=" w4">
				                    <button class="btn3" onClick="$('#File').click();return false;">파일 선택</button> 
				            </td>
				        </tr>
				        <tr class="tr01">
				            <td colspan="2" class="w3" id="submitFileListAdd">		
				            </td>
				        </tr>
				        </tbody>
				    </table> 
				</div>
				<!-- e_파일 첨부 -->    
			</form>
			
			<div class="st_tt_s1 feedback" style="margin-top:10px;display:none;">
            	<span class="tt1">교수님 피드백 내용</span>
		    </div>
			<div class="st_tt_s2 feedback" style="display:none;">
		   		<span class="tt_4" style="float:left;"><span class="sp_t">점수 </span><span data-name="score"></span></span>
		    </div>
			    
			<table class="tab_table edit feedback" style="display:none;">
				<tbody>
					<tr class="edit_wrap">
						<td class="tarea free_textarea" data-name="feedback_content">
							<!-- <span class="sp2">* 예방과 관리에 적용할 수 있는 방안을 추가 기술할 경우 +1점</span> -->
			            </td>
					</tr>
				</tbody>
			</table>
			<div class="bt_wrap">
			    <button class="bt_2" id="submitBtn" onclick="stAssigmMentSubmit();">등록</button>
			    <button class="bt_3" onclick="getAssignMentList();reset();">목록</button>
			</div>
		</div>
	</div>
	<!-- e_tabcontent -->

</body>
</html>