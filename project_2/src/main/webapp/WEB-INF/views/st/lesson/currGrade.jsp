<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
	$(document).ready(function() {
		getCurriculum();
		getCurrScore();

		if($.inArray("${aca_state}", ["03","04","05","06","07"]) > -1){
			getCurrScore();	
		}else{
			$("#scoreDiv").removeClass("class_x").addClass("class_0");
		}
	});		
	
	//교육과정 가져오기
	function getCurriculum() {
		$.ajax({
			type : "GET",
			url : "${HOME}/ajax/pf/lesson/curriculumInfo",
			data : {},
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {

					$("#curr_name").text(data.basicInfo.curr_name);
					$("#aca_system_name").text("(" + data.basicInfo.aca_system_name + ")");
                    
				} else {

				}
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}
	
	function getCurrScore(){
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/st/lesson/currGrade/list",
            data: {    
            },
            dataType: "json",
            success: function(data, status) {
            	if(data.status=="200"){
            		var htmls="";
            		$("#currListAdd").empty();
            		
            		if(data.currList.length == 0){
            			$("#scoreDiv").removeClass("class_0").addClass("class_x");
            		}
            		
            		$.each(data.currList , function(index){
            			var grade = this.grade;
            			if(this.final_grade == "F" || this.grade == "0")
            				grade = "-";
            			
            			htmls='<tr class="">'
                            +'<td>'+this.start_date+'~<br>'+this.end_date+'</td>'
                            +'<td>'+this.curr_code+'</td>'
                            +'<td class="t_l">'+this.curr_name+'</td>'
                            +'<td class="">'+this.name+'</td>'
                            +'<td>'+grade+'</td>'
                            +'<td>'+this.final_grade+'</td>'
                            +'</tr>';
						
						$("#currListAdd").append(htmls);
            		});
            	}else
            		alert("실패");
            },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            }
        });					
	}
</script>
</head>
<body>
	<!-- s_tt_wrap -->
	<div class="tt_wrap">
		<h3 class="am_tt" style="margin-left:15px;">
			<span class="tt" id="curr_name"></span>
			<span class="tt_s" id="aca_system_name"></span>
		</h3>
	</div>
	<!-- e_tt_wrap -->

	<!-- s_tab_wrap_cc -->
	<div class="tab_wrap_cc">
		<button class="tab01 tablinks" onClick="pageMoveCurriculumView(${S_CURRICULUM_SEQ});">과정 계획</button>
		<button class="tab01 tablinks" onClick="">수업시간표</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/unit'">단원관리</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/sfCurrResearch'">만족도조사</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/currAsgmt'">종합과제</button>		
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/feScore'">형성평가</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/soosiGrade'">수시성적</button>
		<button class="tab01 tablinks active" onClick="location.href='${HOME}/st/lesson/currGrade'">종합성적</button>		
	</div>
	<!-- e_tab_wrap_cc -->

	<div class="sub">
	<!-- s_sub_con -->
	<div class="sub_con rcard">

		<!-- s_tt_wrap -->
		<div class="tt_wrap grdcard">
			<h3 class="am_tt">
				<span class="tt_s">종합성적</span>
				<%-- <span class="tt">${aca_name }</span> --%>
			</h3>
			<div class="w_r">
				<!-- <button class="btn_pdf" title="PDF다운로드"></button> -->
				<button class="btn_prt" title="인쇄하기"></button>
			</div>
		</div>
		<!-- e_tt_wrap -->

		<!-- s_rfr_con -->
		<!-- s_등록된 성적이 없을 때 class : class_x, 조회기간이 아닐 때 class : class_0 추가 -->
		<div class="rfr_con grd1" id="scoreDiv">
			<table class="mlms_tb card">
				<thead>
					<tr>
                           <th class="th01 bd01 wn1_3">기간</th>
                           <th class="th01 bd01 wn1_2">교과정코드</th>
                           <th class="th01 bd01 wn1_5">교육과정명</th>
                           <th class="th01 bd01 wn1_4">책임교수</th>
                           <th class="th01 bd01 wn1_1">이수 학점</th>
                           <th class="th01 bd01 wn1_1">확정 등급</th>
                       </tr>
				</thead>
				<tbody id="currListAdd">
					
				</tbody>
			</table>

		</div>
		<!-- e_rfr_con -->

	</div>
	<!-- e_sub_con -->
	</div>
</body>
</html>