<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<script src="${JS}/lib/pdfobject.min.js"></script>
<script type="text/javascript">
var slideCount = $('#slider ul li').length;
var slideWidth = $('#slider ul li').width();
var slideHeight = $('#slider ul li').height();
var sliderUlWidth = slideCount * slideWidth;
	$(document).ready(function() {
		getLessonPlanTitle();
		getLessonData();
		
		$('a.control_prev').click(function () {
	        moveLeft();
	    });

	    $('a.control_next').click(function () {
	        moveRight();
	    });

	    $('a[name=control_prev100]').click(function () {
	        moveLeftMax();
	    });

	    $('a[name=control_next100]').click(function () {
	        moveRightMax();
	    });
	});
		
	function moveLeft() {
        $('#slider ul').animate({
            left: + slideWidth
        }, 10, function () {
            $('#slider ul li:last-child').prependTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    function moveRight() {
        $('#slider ul').animate({
            left: - slideWidth
        }, 10, function () {
            $('#slider ul li:first-child').appendTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };   
	    
    function moveLeftMax() {
        $('#slider100 ul').animate({
            left: + slideWidth
        }, 10, function () {
            $('#slider100 ul li:last-child').prependTo('#slider100 ul');
            $('#slider100 ul').css('left', '');
        });
    };

    function moveRightMax() {
        $('#slider100 ul').animate({
            left: - slideWidth
        }, 10, function () {
            $('#slider100 ul li:first-child').appendTo('#slider100 ul');
            $('#slider100 ul').css('left', '');
        });
    };
    
	function getLessonPlanTitle(){
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/st/lesson/lessonPlanTitle",
            data: {                 
            },
            dataType: "json",
            success: function(data, status) {
            	if(data.status=="200"){
	            	var times = "", sosok = "", attendance = "", pf_name = "";
	            	$('#attendance').removeAttr('class');

	            	if(data.attendance == "0")
	            		attendance = "t4";
	            	else if(data.attendance == "00")
	            		attendance = "t3";
	            	else if(data.attendance == "02")
	            		attendance = "t2";
	            	else if(data.attendance == "03")
	            		attendance = "t1";
	            	
	            	$('#attendance').addClass(attendance);
	            	if(!isEmpty(data.lessonPlan.specialty))
	            		sosok="("+data.lesson_plan.specialty+")";
	            	if(!isEmpty(data.lessonPlan.start_time) && !isEmpty(data.lessonPlan.end_time))
	            		times = data.lessonPlan.start_time+"~"+data.lessonPlan.end_time;
	            	
	            	if(isEmpty(data.lessonPlan.name))
	            		pf_name = "교수 미등록";
	            	else
	            		pf_name = data.lessonPlan.name+sosok;
	            	$("span[name=lessonDate]").text(data.lessonPlan.lesson_date);
	            	$("span[name=lessonTime]").text("["+data.lessonPlan.period+"교시] " + times);
	            	$("span[name=lessonSubject]").text(data.lessonPlan.lesson_subject);
	            	$("span[name=pfName]").text(pf_name);
                    $("div[data-name=lessonInfo]").html(data.lessonPlan.curr_name +'<span class="sign">&gt;</span>'+data.lessonPlan.lesson_subject+' ('+data.lessonPlan.lesson_date+')');
                    
            	}
            },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            }
        });		
	}
	
	function getLessonData(){
		if(isEmpty($("input[name=lesson_data_seq]").val()))
			return false;
		
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/lessonData/list",
            data: {
                "lesson_data_seq" : $("input[name=lesson_data_seq]").val()                   
            },
            dataType: "json",
            success: function(data, status) {                	
            	if(data.status == "200"){
            		$("div[name=mediaListAdd]").empty();
            		$("div[name=fileListAdd]").empty();
					$("div[name=memo]").html(data.lessonDataList.memo);
					$("td[name=content]").html(data.lessonDataList.content);
					
					$("td[name=content] img").css("max-width","650px");
					
					$("span[name=dataCount]").text(data.lessonAttachList.length);
					
					var htmls = '';
					$.each(data.lessonAttachList, function(index){
						if(this.attach_type =="I"){
					    	var filename = this.file_name.split("\||")[0];
					    	if(this.file_name.split("\||").length == 1){
					    		htmls='<div class="wrap_wrap">';
								htmls+='<div class="l_wrap3">';
								htmls+='<div class="pt_wrap">';                            
								htmls+='<div class="imgs">';
								htmls+='<img src="${RES_PATH}'+this.file_path+'" alt="'+this.file_name+'" width="220"></div></div>';
								htmls+='<div class="tt_wrap">';
								htmls+='<span class="sp01" title="'+this.explan+'">'+this.explan+'</span>';
								htmls+='<span class="sp02">사진</span>';
								htmls+='<span class="sp03"></span>';
								htmls+='<button class="dw" title="다운로드" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');">다운로드</button>';
								htmls+='<button class="fl open3" onClick="popUpOpen(\'img\',\''+this.file_name+'\',\''+this.file_path+'\',\'${RES_PATH}\',\''+this.explan+'\');" title="파일">파일</button></div></div>';	
					    	}else{
					    		htmls='<div class="wrap_wrap">';
								htmls+='<div class="l_wrap3">';
								htmls+='<div class="pt_wrap">';                            
								htmls+='<div class="imgs">';
								htmls+='<img src="${RES_PATH}'+this.file_path.split("\||")[0]+'" alt="'+filename+'" width="220"></div></div>';
								htmls+='<div class="tt_wrap">';
								htmls+='<span class="sp01" title="'+this.explan+'">'+this.explan+'</span>';
								htmls+='<span class="sp02">사진</span>';
								htmls+='<span class="sp03"></span>';
								htmls+='<button class="dw" title="다운로드" onClick="multiFileDownload(\'${HOME}\',\''+this.file_path+'\',\''+this.file_name+'\');">다운로드</button>';
								htmls+='<button class="fl open3" onClick="popUpOpen(\'img\',\''+this.file_name+'\',\''+this.file_path+'\',\'${RES_PATH}\',\''+this.explan+'\');" title="파일">파일</button></div>';
						        htmls += '<span class="sp_repre_nx">'+this.file_name.split("\||").length+'장</span></div>';
					    	}					    		
				            $("div[name=mediaListAdd]").append(htmls);
					    }else if(this.attach_type=="A"){			                    	
	                    	htmls = '<div class="wrap_wrap">';
	                    	htmls += '<div class="l_wrap1">';
	                    	htmls += '<div class="pt_wrap">';   
	                    	htmls += '<span class="mv" style="display:none;">▶</span>';      
                               htmls += '<audio src="${RES_PATH}'+this.file_path+'"></div>';		                    	        
	                    	htmls += '<div class="tt_wrap">';           
	                    	htmls += '<span class="sp01 title="'+this.explan+'">'+this.explan+'</span>';  
							htmls+='<span class="sp02">오디오</span>';
							htmls+='<span class="sp03"></span>';
	                    	htmls += '<button class="dw" title="다운로드" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');">다운로드</button>';
	                    	htmls += '<button class="fl open1" onClick="popUpOpen(\'mp3\',\''+this.file_name+'\',\''+this.file_path+'\',\'${RES_PATH}\',\''+this.explan+'\');" title="파일">파일</button></div></div>';
	                    	
   				            $("div[name=mediaListAdd]").append(htmls);
	                    }else if(this.attach_type=="V"){
	                    	htmls = '<div class="wrap_wrap">';
	                    	htmls += '<div class="l_wrap1">';
	                    	htmls += '<div class="pt_wrap">';   
	                    	htmls += '<span class="mv" style="display:none;">▶</span>';      
	                    	htmls += '<video width="220" height="140" controls><source src="${RES_PATH}'+this.file_path+'" type="video/mp4"></video></div>';		                    	        
	                    	htmls += '<div class="tt_wrap">';           
	                    	htmls += '<span class="sp01 title="'+this.explan+'">'+this.explan+'</span>';   
							htmls+='<span class="sp02">동영상</span>'; 
							htmls+='<span class="sp03"></span>'; 
	                    	htmls += '<button class="dw" title="다운로드" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');">다운로드</button>';
	                    	htmls += '<button class="fl open1" onClick="popUpOpen(\'mp4\',\''+this.file_name+'\',\''+this.file_path+'\',\'${RES_PATH}\',\''+this.explan+'\');" title="파일">파일</button></div></div>';
	                    	
   				            $("div[name=mediaListAdd]").append(htmls);
	                    }else if(this.attach_type=="Y"){	
	                    	
	                    	htmls = '<div class="wrap_wrap">';
	                    	htmls += '<div class="l_wrap2">';
	                    	htmls += '<div class="pt_wrap">';                         
	                    	htmls += '<div class="video_container">';
	                    	htmls += '<iframe src="'+this.youtube_url+'" frameborder="0" encrypted-media" allowfullscreen></iframe></div></div>';	                    	        
	                    	htmls += '<div class="tt_wrap">';
	                    	htmls += '<span class="sp01" title="'+this.explan+'">'+this.explan+'</span>';  
							htmls+='<span class="sp02">유튜브</span>';
							htmls+='<span class="sp03"></span>';
	                    	htmls += '</div>';
	                    	htmls += '<button class="fl open1" onClick="popUpOpen(\'youtube\',\''+this.youtube_url+'\',\'\',\'${RES_PATH}\',\''+this.explan+'\');" title="파일">파일</button>';
	                    	htmls += '</div>';
	                    	htmls += '</div>';	                    	
				            $("div[name=mediaListAdd]").append(htmls);
					    }else{
	                    	htmls = '<li class="li_1"><span>'+this.file_name+'</span>';
	                    	if(this.attach_type=="P"){
	                    		if(PDFObject.supportsPDFs)
	                    			htmls += '<button class="fl open2" title="파일" onClick="pdfView(\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');">파일</button>';
	                    	}
	                    	htmls += '<button class="dw" title="다운로드" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');">다운로드</button></li>';
	                    	$("ul[name=fileListAdd]").append(htmls);
	                    }
	                    
					});
            	}
            },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            }
        }); 
	}
		
	function pdfView(file_path, file_name){
		PDFObject.embed(file_path+file_name, "#example1");
		$("#m_pop2").show();		
	}
		
	function popUpOpen(type, file_name, file_path, res_path, explan){
		if(isEmpty(explan)){
			$("div[data-name=dataExplan]").hide();			
		}else{
			$("div[data-name=dataExplan]").show();
			$("div[data-name=dataExplan]").text(explan);
		}
		
		if(type=="img"){
			slideCount = $('#slider ul li').length;
			slideWidth = $('#slider ul li').width();
			slideHeight = $('#slider ul li').height();
			sliderUlWidth = slideCount * slideWidth;
			$('#slider').css({ width: slideWidth, height: slideHeight });			
			$('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });			
			$('#slider ul li:last-child').prependTo('#slider ul');
   
			var fileList = file_path.split("||");
			var htmls = "";
			for(var i=0;i<fileList.length;i++){
				htmls+='<li><img src="'+res_path+fileList[i]+'"></li>';
			}
			if(fileList.length == 1){
				$("#m_pop3").find(".control_next").hide();
				$("#m_pop3").find(".control_prev").hide();
			}else{
				$("#m_pop3").find(".control_next").show();
				$("#m_pop3").find(".control_prev").show();
			}
			$("#imgPopupListAdd").html(htmls);
			$("#m_pop3").show();
		}else if(type=="mp4"){
			var htmls = '<video width="894" controls><source src="'+res_path+file_path+'" type="video/mp4" id="videoSrc"></video>'; 
			$("#mediaAdd").html(htmls);
			$("#m_pop1").show();
		}else if(type=="mp3"){
			var htmls = '<audio width="894" controls><source src="'+res_path+file_path+'" type="audio/mpeg" id="videoSrc"></audio>';
			$("#mediaAdd").html(htmls);
			$("#m_pop1").show();
		}else if(type=="youtube"){
			
			var htmls = '<object data="'+file_name+'" width="894" height="500"></object>';
			$("#mediaAdd").html(htmls);
			$("#m_pop1").show();
		}
	}
	
	function zoomImg(){
		$("#zoomImgPopupListAdd").html($("#imgPopupListAdd").html());
		
		slideCount = $('#slider100 ul li').length;
		slideWidth = $('#slider100 ul li').width();
		slideHeight = $('#slider100 ul li').height();
		sliderUlWidth = slideCount * slideWidth;		
		$('#slider100').css({ width: '100%', height: slideHeight });		
		$('#slider100 ul').css({ width: '100%', marginLeft: - slideWidth });	
	    
	    if($('#slider100 ul li').length == 1){
			$("#m_pop3_100").find(".control_next").hide();
			$("#m_pop3_100").find(".control_prev").hide();
		}else{
			$("#m_pop3_100").find(".control_next").show();
			$("#m_pop3_100").find(".control_prev").show();
		}
	    
		$("#m_pop3").hide();
		$("#m_pop3_100").show();				
	}
	
	function zoomOutImg(){
		$("#imgPopupListAdd").html($("#zoomImgPopupListAdd").html());
		
		slideCount = $('#slider ul li').length;
		slideWidth = $('#slider ul li').width();
		slideHeight = $('#slider ul li').height();
		sliderUlWidth = slideCount * slideWidth;
		$('#slider').css({ width: slideWidth, height: slideHeight });			
		$('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
		
		if($('#slider ul li').length == 1){
			$("#m_pop3").find(".control_next").hide();
			$("#m_pop3").find(".control_prev").hide();
		}else{
			$("#m_pop3").find(".control_next").show();
			$("#m_pop3").find(".control_prev").show();
		}
		$("#m_pop3").show();
		$("#m_pop3_100").hide();
	}
	
	function lessonDataPrint(){
		var initBody = document.body.innerHTML;
		window.onbeforeprint = function () {

			document.body.innerHTML = '<div class="main_con st"><div id="currDiv" class="tabcontent tabcontent3">'+document.getElementById("lpdataDiv").innerHTML+'</div></div>';
		}
		window.onafterprint = function () {

			document.body.innerHTML = initBody;

		}
		window.print();
	}
</script>
</head>
<body>
	<!-- s_tt_wrap -->
	<div class="tt_wrap cc">
		<h3 class="am_tt">
			<span class="h_date" name="lessonDate"></span> <span class="tt"
				name="lessonTime"></span> <span class="tt_s" name="lessonSubject"></span>
		</h3>

		<div class="r_wrap">
			<!-- class:  출석 t1, 지각 t2, 결석 t3 -->
			<span class="" id="attendance"></span>

			<div class="wrap2">
				<span class="a_mp"> <span class="pt01"> <img src="" name="pfPicture" class="ph_img">
				</span> <span class="ssp1" name="pfName"></span></span>
			</div>
		</div>
	</div>
	<!-- e_tt_wrap -->

	<!-- s_tab_wrap_cc -->
	<div class="tab_wrap_cc">
		<button class="tab01 tablinks" onclick="pageMoveLessonPlanView(${S_LP_SEQ},${S_CURRICULUM_SEQ });">수업계획서</button>
		<button class="tab01 tablinks active" onclick="location.href='${HOME}/st/lesson/lessonData'">수업자료</button>
		<button class="tab01 tablinks " onclick="location.href='${HOME}/st/lesson/formationEvaluation'">형성평가</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/st/lesson/assignMent'">과제</button>
		<button class="tab01 tablinks long" onclick="alert('작업중인 페이지 입니다.');">수업만족도 조사</button>
	</div>
	<!-- e_tab_wrap_cc -->


	<!-- s_tabcontent -->
	<div id="lpdataDiv" class="tabcontent tabcontent3">
        <input type="hidden" name="lesson_data_seq" value="${lesson_data_seq }"/>
		<!-- s_stab_wrap  -->
		<table class="stab_wrap">
			<tbody>
				<!-- s_메모 -->
				<tr class="set">
					<td class="td_f">

						<div class="mm_wrap chng1">
							<div class="mm_ttx dis tarea">
								<div class="tarea01 tt_t" name="memo"></div>
							</div>
						</div>

					</td>
				</tr>
				<!-- e_메모 -->
			</tbody>
		</table>
		<!-- e_stab_wrap  -->


		<div class="st_tt">
			<div class="w_r" style="float:right;">
				<!-- <button class="btn_pdf" title="PDF다운로드"></button> -->
				<button class="btn_prt" title="인쇄하기" onClick="lessonDataPrint();"></button>
			</div>
		</div>

		<table class="tab_table edit">
			<tbody>
				<tr class="edit_wrap">
					<td class="tarea free_textarea" name="content">
					</td>
				</tr>
			</tbody>
		</table>

		<!-- s_등록된 자료 -->
		<table class="tab_table ttb1">
			<tbody>
				<tr class="tr01">
					<td class="th02_v"><span class="sp1">수업준비 등록자료</span><span
						class="sign">(</span><span class="sp_num" name="dataCount"></span><span
						class="sign">건</span><span class="sign">)</span></td>
				</tr>

				<tr class="tr01">
					<td class="pt">

						<ul class="h_list m_tb" name="fileListAdd">
						</ul> <!-- s_set  -->
						<div class="set" name="mediaListAdd">
						</div> <!-- e_set  -->

					</td>
				</tr>
			</tbody>
		</table>
		<!-- e_등록된 자료 -->

	</div>
	<!-- e_tabcontent -->
	
	
	<!-- s_ 팝업 : 강의자료 viewer1 -->
	<div id="m_pop1" class="pop_up_view1 mo1">    
		<div class="pop_wrap">
			<button class="pop_close close1" type="button" onClick="$('#m_pop1').hide();">X</button>	  
		
			<p class="t_title">[ 동영상 ]강의자료</p>       
	 		     		     		     		     		     		    
			<!-- s_table_b_wrap -->
			<div class="table_b_wrap">	
				<!-- s_pop_table -->	   
				<div class="pop_table">				    
		<div class="tt" data-name="lessonInfo"></div> 
		<div class="tt" data-name="dataExplan" style="margin-top:-2px;"></div>   
					<!-- s_pop_twrap1 -->
					<div class="pop_twrap">   
					<!-- s_video_wrap -->
					<div class="video_wrap" id="mediaAdd">					                                     
				                  
					</div>
					<!-- e_video_wrap -->
					</div>        
				<!-- e_pop_twrap1 -->
				</div>		
				<!-- e_pop_table -->
			</div>               
		<!-- e_table_b_wrap -->   
		 </div>  
	</div>
	<!-- e_ 팝업 : 강의자료 viewer1 -->
	
	<!-- s_ 팝업 : 강의자료 viewer2 -->
	<div id="m_pop2" class="pop_up_view2 mo2" style="left:0px;">    
	          <div class="pop_wrap">
	 		        <button class="pop_close close2" type="button" onClick="$('#m_pop2').hide();$('#example1').empty();">X</button>	  
	
	                <p class="t_title">[ 파일 ]강의자료</p>       
	 		     		     		     		     		     		    
	 <!-- s_table_b_wrap -->
	<div class="table_b_wrap">	
	
	<!-- s_pop_table -->	   
	<div class="pop_table">			     		    
		<div class="tt" data-name="lessonInfo"></div> 
		<div class="tt" data-name="dataExplan" style="margin-top:-2px;"></div> 
	<!-- s_pop_twrap1 -->
	<div class="pop_twrap">   
	<!-- s_f_output -->   
	    <div class="f_output" style="height:500px;"> 
	       <div class="file_con" id="example1" style="height:450px;">   
	        </div>
	    </div>
	<!-- e_f_output -->
	</div>        
	<!-- e_pop_twrap1 -->    
	
	 </div>		
	<!-- e_pop_table -->
	</div>               
	<!-- e_table_b_wrap -->   
	 </div>  
	</div>
	<!-- e_ 팝업 : 강의자료 viewer2 -->
	
	<!-- s_ 팝업 : 강의자료 viewer3 -->
	<div id="m_pop3" class="pop_up_view3 mo3">    
	          <div class="pop_wrap">
	 		        <button class="pop_close close3" type="button" onClick="$('#m_pop3').hide();">X</button>	  
	
	                <p class="t_title">[ 이미지 ]강의자료</p>       
	 		     	<button class="open3_100" type="button" title="크게보기" onClick="zoomImg();"></button>	     		     		     		     		    
	 <!-- s_table_b_wrap -->
	<div class="table_b_wrap">	
	
	<!-- s_pop_table -->	   
	<div class="pop_table">			     		    
		<div class="tt" data-name="lessonInfo"></div> 
		<div class="tt" data-name="dataExplan" style="margin-top:-2px;"></div> 
	<!-- s_pop_twrap1 -->
	<div class="pop_twrap">   
	<!-- s_slider -->   
	    <div id="slider" >
	              <a href="javascript:return false;" class="control_next">&#8250;</a>
	                <a href="javascript:return false;" class="control_prev">&#8249;</a>
	                <ul id="imgPopupListAdd">                  
	               </ul>  
	         </div>
	<!-- e_slider -->
	</div>        
	<!-- e_pop_twrap1 -->    
	
	 </div>		
	<!-- e_pop_table -->
	</div>               
	<!-- e_table_b_wrap -->   
	 </div>  
	</div>
	<!-- e_ 팝업 : 강의자료 viewer3 -->
	
	<!-- s_ 팝업 : 강의자료 viewer3 .pop_up_view3_100 -->
	<div id="m_pop3_100" class="pop_up_view3_100 mo3_100">    
	          <div class="pop_wrap">
	 		        <button class="pop_close close3_100" type="button" title="이전 크기로 되돌아 가기" onClick="zoomOutImg();"></button>	  
	
	                <p class="t_title">[ 이미지 ]강의자료</p>       
	 		     		     		     		     		     		    
	 <!-- s_table_b_wrap -->
	<div class="table_b_wrap">	
	
	<!-- s_pop_table -->	   
	<div class="pop_table">			    		    
		<div class="tt" data-name="lessonInfo"></div> 
		<div class="tt" data-name="dataExplan" style="margin-top:-2px;"></div>  
	<!-- s_pop_twrap1 -->
	<div class="pop_twrap">   
	<!-- s_slider100 -->   
	    <div id="slider100">
	              <a href="javascript:return false;" class="control_next" name="control_next100">&#8250;</a>
	                <a href="javascript:return false;" class="control_prev" name="control_prev100">&#8249;</a>
	                <ul id="zoomImgPopupListAdd">
	                                      
	               </ul>  
	         </div>
	<!-- e_slider100 -->
	</div>        
	<!-- e_pop_twrap1 -->    
	
	 </div>		
	<!-- e_pop_table -->
	</div>               
	<!-- e_table_b_wrap -->   
	 </div>  
	</div>
	<!-- e_ 팝업 : 강의자료 viewer3 .pop_up_view3_100 -->
	
</body>
</html>