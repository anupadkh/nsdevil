<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
	$(document).ready(function() {
		getLessonPlanTitle();
		getLessonPlanInfo();
	});
	
	function getLessonPlanInfo(){
		
        $.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/lessonPlanBasicInfo",
            data: {                  
            },
            dataType: "json",
            success: function(data, status) {
            	var lesson_date = data.lessonPlanBasicInfo.lesson_date.split("/");
            	var aca_name = data.lessonPlanBasicInfo.year + " " + data.lessonPlanBasicInfo.l_aca_system_name + " " + data.lessonPlanBasicInfo.m_aca_system_name + " " + data.lessonPlanBasicInfo.aca_system_name;
            	if(isEmpty(data.lessonPlanBasicInfo.picture_path))
            		picturePath = "${IMG}/ph.png";
        		else
        			picturePath = "${RES_PATH}"+data.lessonPlanBasicInfo.picture_path;
            	
            	$("img[name=pfPicture]").attr("src",picturePath);
				$("#currName").text(data.lessonPlanBasicInfo.curr_name);				
            	$("span[name=lesson_date]").text(data.lessonPlanBasicInfo.lesson_date + " [" + data.lessonPlanBasicInfo.period+"교시]");
            	$("span[name=lesson_time]").text(data.lessonPlanBasicInfo.start_time + " ~ " + data.lessonPlanBasicInfo.end_time);
            	$("span[name=acaName]").text(aca_name);
            	var htmls = data.lessonPlanBasicInfo.curr_name;
            	htmls+='<span class="sign">&gt;</span>'+data.lessonPlanBasicInfo.lesson_subject;
            	htmls+='<span class="sign">&gt;</span>'+lesson_date[0]+"월 " + lesson_date[1]+"일";
            	htmls+="("+data.lessonPlanBasicInfo.start_time + " ~ " + data.lessonPlanBasicInfo.end_time+")";
            	$("span[name=lesson_subject]").html(htmls);
            	$("td[name=learning_outcome]").text(data.lessonPlanBasicInfo.learning_outcome);
            	$("td[name=lesson_code]").text(data.lessonPlanBasicInfo.lesson_code);
            	$("td[name=period_cnt]").text(data.lessonPlanBasicInfo.period_cnt);
            	$("td[name=classroom]").text(data.lessonPlanBasicInfo.classroom);
            	
            	var ranNum = 0;
            	var htmls='';
            	
            	//핵심 임상표현
            	$.each(data.lessonCoreClinicList,function(index){
                    ranNum = Math.floor(Math.random()*7) + 1;

                    htmls += '<div class="c_wrap"><span class="tt_c_1 color0'+ranNum+'">'+this.code_name+'</span>';  
                });
                $("#coreClinicListAdd").html(htmls);
                
                if(data.lessonCoreClinicList.length == 0){
                	$("#coreClinicListAdd").closest("tr").hide();
                }	                   
                
            	htmls = '';
            	//임상표현
            	$.each(data.lessonClinicList,function(index){
                    ranNum = Math.floor(Math.random()*7) + 1;

                    htmls += '<div class="c_wrap"><span class="tt_c_1 color0'+ranNum+'">'+this.code_name+'</span>';   
                });
                $("#clinicListAdd").html(htmls);
            	
                if(data.lessonClinicList.length == 0){
                	$("#clinicListAdd").closest("tr").hide();
                }	
                
                htmls = '';
            	//임상술기
            	$.each(data.lessonOsceList,function(index){
                    ranNum = Math.floor(Math.random()*7) + 1;

                    htmls += '<div class="c_wrap"><span class="tt_c_1 color0'+ranNum+'">'+this.osce_name+'</span>';          
                });
                $("#osceCodeListAdd").html(htmls);

                if(data.lessonOsceList.length == 0){
                	$("#osceCodeListAdd").closest("tr").hide();
                }
                
                htmls = '';                	
                
            	//진단명
                $.each(data.lessonDiagnosisList,function(index){
                    ranNum = Math.floor(Math.random()*7) + 1;
                    htmls += '<div class="c_wrap"><span class="tt_c_1 color0'+ranNum+'">'+this.code_name+'</span>';
                });
                $("#diaCodeAdd").html(htmls);

                if(data.lessonDiagnosisList.length == 0){
                	$("#diaCodeAdd").closest("tr").hide();
                }
              	//비강의
                htmls='';
                $.each(data.lessonMethodList, function(index){                 	
                    ranNum = Math.floor(Math.random()*7) + 1;            
                	htmls += '<div class="c_wrap"><span class="tt_c_1 color_tt'+ranNum+'">'+this.code_name+'</span></div>';
                });
    			$("#lessonMethodAdd").html(htmls);
    			
              	//졸업역량
                htmls='';
                $.each(data.lessonFinishCapabilityList,function(index){
                    ranNum = Math.floor(Math.random()*7) + 1;
                    
                    htmls += '<span class="tt_d">('+this.fc_code+') '+this.fc_name+'</span>';
                });
                $("#fcListAdd").html(htmls);

    			if(data.lessonFinishCapabilityList.length == 0){
                	$("#fcListAdd").closest("tr").hide();
                }
    			
                //형성평가
                htmls='';
              	var fe_cnt = 0;
                $.each(data.formationEvaluationList,function(index){
                    ranNum = Math.floor(Math.random()*7) + 1;
                                    
                    htmls += '<div class="c_wrap">';
                    htmls += '<span class="tt_c color0'+ranNum+'">'+this.code_name+'</span></div>';
                    fe_cnt++;                   
                });
                $("#feListAdd").html(htmls); 
                $("span[name=fe_num]").text(fe_cnt);
                
                
            },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            }
        }); 
    }
	
	function getLessonPlanTitle(){
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/st/lesson/lessonPlanTitle",
            data: {                 
            },
            dataType: "json",
            success: function(data, status) {
            	if(data.status=="200"){
	            	var times = "", sosok = "", attendance = "", pf_name = "";
	            	$('#attendance').removeAttr('class');

	            	if(data.attendance == "0")
	            		attendance = "t4";
	            	else if(data.attendance == "00")
	            		attendance = "t3";
	            	else if(data.attendance == "02")
	            		attendance = "t2";
	            	else if(data.attendance == "03")
	            		attendance = "t1";
	            	
	            	$('#attendance').addClass(attendance);
	            	if(!isEmpty(data.lessonPlan.specialty))
	            		sosok="("+data.lesson_plan.specialty+")";
	            	if(!isEmpty(data.lessonPlan.start_time) && !isEmpty(data.lessonPlan.end_time))
	            		times = data.lessonPlan.start_time+"~"+data.lessonPlan.end_time;
	            	
	            	if(isEmpty(data.lessonPlan.name))
	            		pf_name = "교수 미등록";
	            	else
	            		pf_name = data.lessonPlan.name+sosok;
	            	$("span[name=lessonDate]").text(data.lessonPlan.lesson_date);
	            	$("span[name=lessonTime]").text("["+data.lessonPlan.period+"교시] " + times);
	            	$("span[name=lessonSubject]").text(data.lessonPlan.lesson_subject);
	            	$("span[name=pfName]").text(pf_name);
            	}
            },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            }
        });		
	}
	
	function lessonPrint(){
		var initBody = document.body.innerHTML;
		window.onbeforeprint = function () {

			document.body.innerHTML = '<div class="main_con st"><div id="currDiv" class="tabcontent tabcontent2">'+document.getElementById("lpDiv").innerHTML+'</div></div>';
		}
		window.onafterprint = function () {

			document.body.innerHTML = initBody;

		}
		window.print();
	}
	
</script>
</head>
<body>
	<!-- s_tt_wrap -->
	<div class="tt_wrap cc">
		<h3 class="am_tt">
			<span class="h_date" name="lessonDate"></span> 
			<span class="tt" name="lessonTime"></span> 
			<span class="tt_s" name="lessonSubject"></span>
		</h3>

		<div class="r_wrap">
			<!-- class:  출석 t1, 지각 t2, 결석 t3 -->
			<span class="" id="attendance"></span>

			<div class="wrap2">
				<span class="a_mp">
					<span class="pt01">
						<img src="" name="pfPicture" class="ph_img">
					</span> 
				<span class="ssp1" name="pfName"></span></span>
			</div>
		</div>
	</div>
	<!-- e_tt_wrap -->

	<!-- s_tab_wrap_cc -->
	<div class="tab_wrap_cc">
		<button class="tab01 tablinks active" onclick="pageMoveLessonPlanView(${S_LP_SEQ},${S_CURRICULUM_SEQ });">수업계획서</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/st/lesson/lessonData'">수업자료</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/st/lesson/formationEvaluation'">형성평가</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/st/lesson/assignMent'">과제</button>
		<button class="tab01 tablinks long" onclick="location.href='${HOME}/st/lesson/sfResearch'">수업만족도 조사</button>
	</div>
	<!-- e_tab_wrap_cc -->


	<!-- s_tabcontent -->
	<div id="lpDiv" class="tabcontent tabcontent2">

		<div class="st_tt">
			<div class="w_l">
				<span class="tt1" id="currName"></span>
			</div>
			<div class="w_r">
				<!-- <button class="btn_pdf" title="PDF다운로드"></button> -->
				<button class="btn_prt" title="인쇄하기" onClick="lessonPrint();"></button>
			</div>
		</div>

		<div class="stab_wrap2">
			<div class="pt_wrap">

				<div class="in_wrap">
					<div class="in_wrap_s">
						<img src="" name="pfPicture" alt="사진" class="pt1" style="width:100px;height:120px;">
					</div>
				</div>

				<div class="title_wrap">
					<div class="tts">
						<span class="sp01" name="acaName"></span> 
						<span class="sp01" name="lesson_subject"></span>
					</div>
				</div>

				<span class="tt_name" name="pfName"></span>
			</div>
		</div>

		<table class="tab_table ttb1">
			<tbody>
				<tr class="tr02">
					<td class="th01 w1">수업<br>코드</td>
					<td class="w2" name="lesson_code"></td>
					<td class="th01 w1">일정</td>
					<td class="w5">
						<span class="sp07" name="lesson_date"></span>
						<span class="sp08" name="lesson_time"></span>
					</td>					
					<td class="th01 w1">시수</td>
					<td class="w4" name="period_cnt"></td>
					<td class="th01 w1">장소</td>
					<td colspan="2" class="w3" name="classroom"></td>
				</tr>
				<tr class="tr02">
					<td class="th01 w1">학습<br>성과</td>
					<td colspan="8" class="" name="learning_outcome"></td>
				</tr>
				<tr class="tr02">
					<td class="th01 w1">학습<br>방법
					</td>
					<td colspan="8" id="lessonMethodAdd">
					</td>
				</tr>
				<tr class="tr02">
					<td class="th01 w1">핵심<br>임상<br>표현
					</td>
					<td colspan="8" class="" id="coreClinicListAdd">
					</td>
				</tr>
				<tr class="tr02">
					<td class="th01 w1">관련<br>임상<br>표현
					</td>
					<td colspan="8" class="" id="clinicListAdd">
					</td>
				</tr>
				<tr class="tr02">
					<td class="th01 w1">임상<br>술기
					</td>
					<td colspan="8" class="" id="osceCodeListAdd">
					</td>
				</tr>
				<tr class="tr02">
					<td class="th01 w1">진단명
					</td>
					<td colspan="8" class="" id="diaCodeAdd">
					</td>
				</tr>
				<tr class="tr02">
					<td class="th01 w1">형성<br>평가
					</td>
					<td colspan="7" class="w7_1" id="feListAdd">
						
					</td>
					<td class="w3"><span class="tt_num" name="fe_num"></span></td>
				</tr>
				<tr class="tr02">
					<td class="th01 w1">졸업<br>역량
					</td>
					<td colspan="8" class="" id="fcListAdd">
					</td>
				</tr>				
			</tbody>
		</table>
	</div>
	<!-- e_tabcontent -->
</body>
</html>