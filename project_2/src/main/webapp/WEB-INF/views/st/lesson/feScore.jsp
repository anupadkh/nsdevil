<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
	$(document).ready(function() {
		getCurriculum();
		getFeScore();
	});		
	
	//교육과정 가져오기
	function getCurriculum() {
		$.ajax({
			type : "GET",
			url : "${HOME}/ajax/pf/lesson/curriculumInfo",
			data : {},
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {

					$("#curr_name").text(data.basicInfo.curr_name);
					$("#aca_system_name").text("(" + data.basicInfo.aca_system_name + ")");
                    
				} else {

				}
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}
	
	function getFeScore(){
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/st/lesson/feScore/list",
            data: {    
            },
            dataType: "json",
            success: function(data, status) {
            	if(data.status=="200"){
            		var htmls="";
            		
            		$("#feListAdd").empty();
            		
            		if(data.feList.length == 0){
            			$("#scoreDiv").removeClass("class_0").addClass("class_x");
            		}
            		
            		$.each(data.feList , function(index){
            			var state = "";
            			var score = "";
            			var avg_score = "";
            			
            			if(this.test_state == "WAIT" || this.test_state == "START"){
            				score = "대기";
            				avg_score = "대기";
            			}else{
            				score = this.fe_score;
            				avg_score = this.avg_score;		            				
            			}		            				
            			
            			htmls='<tr class="">'
							+'<td>'+this.lesson_date+'</td>'
							+'<td class="t_l">'+this.curr_name+'</td>'
							+'<td class="t_l">'+this.lesson_subject+'</td>'
							+'<td class="">'+this.name+'</td>'
							+'<td>'+score+'</td>'
							+'<td><span class="num1">'+avg_score+'</span><span class="sign">/</span>'
							+'<span class="num2">'+this.quiz_cnt+'</span></td>'
							+'</tr>';
						
						$("#feListAdd").append(htmls);
            		});
            	}else
            		alert("실패");
            },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            }
        });					
	}
</script>
</head>
<body>
	<!-- s_tt_wrap -->
	<div class="tt_wrap">
		<h3 class="am_tt" style="margin-left:15px;">
			<span class="tt" id="curr_name"></span>
			<span class="tt_s" id="aca_system_name"></span>
		</h3>
	</div>
	<!-- e_tt_wrap -->

	<!-- s_tab_wrap_cc -->
	<div class="tab_wrap_cc">
		<button class="tab01 tablinks" onClick="pageMoveCurriculumView(${S_CURRICULUM_SEQ});">과정 계획</button>
		<button class="tab01 tablinks" onClick="">수업시간표</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/unit'">단원관리</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/sfCurrResearch'">만족도조사</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/currAsgmt'">종합과제</button>
		<button class="tab01 tablinks active" onClick="location.href='${HOME}/st/lesson/feScore'">형성평가</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/soosiGrade'">수시성적</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/currGrade'">종합성적</button>		
	</div>
	<!-- e_tab_wrap_cc -->

	<div class="sub">
	<!-- s_sub_con -->
	<div class="sub_con rcard">

		<!-- s_tt_wrap -->
		<div class="tt_wrap grdcard">
			<h3 class="am_tt">
				<span class="tt_s">형성평가</span>
			</h3>
			<div class="w_r">
				<!-- <button class="btn_pdf" title="PDF다운로드"></button> -->
				<button class="btn_prt" title="인쇄하기"></button>
			</div>
		</div>
		<!-- e_tt_wrap -->

		<!-- s_rfr_con -->
		<!-- s_등록된 성적이 없을 때 class : class_x, 조회기간이 아닐 때 class : class_0 추가 -->
		<div class="rfr_con grd3" id="scoreDiv">
			<table class="mlms_tb card">
				<thead>
					<tr>
						<th class="th01 bd01 w4">날짜</th>
						<th class="th01 bd01 w5">교육과정명</th>
						<th class="th01 bd01 w6">수업명</th>
						<th class="th01 bd01 w3">교수명</th>
						<th class="th01 bd01 w1">내점수</th>
						<th class="th01 bd01 w2">평균</th>
					</tr>
				</thead>
				<tbody id="feListAdd">
					
				</tbody>
			</table>

		</div>
		<!-- e_rfr_con -->

	</div>
	<!-- e_sub_con -->
	</div>
</body>
</html>