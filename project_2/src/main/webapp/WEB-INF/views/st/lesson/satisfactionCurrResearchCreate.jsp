<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<style>
		.main_con.st .tabcontent4n .survey.class_x:before{content:'"현재는 과정만족도 조사 기간이 아닙니다.';position: relative;top: 244px;left:0;margin: 0 auto;width: 350px;color:rgba(48, 139, 212, 1);font: 400 22px/27px "Nanum Gothic", NanumGothic, "NanumGothic", "Dotum", '돋움', Helvetica, "Apple SD Gothic Neo", sans-serif;text-align: center;letter-spacing: -3px;word-spacing: 5px;line-height:27px;display:block;text-shadow: 1px 1px 0px rgb(255, 255, 255);overflow-wrap: break-word;overflow-x: hidden;overflow-y: visible;}
.main_con.st .tabcontent4n .survey.class_x:after{content:'추후 공지를 통해 안내해 드리겠습니다."';position: relative;top: 244px;left:0;margin: 0 auto;width: 350px;color:rgba(48, 139, 212, 1);font: 400 22px/27px "Nanum Gothic", NanumGothic, "NanumGothic", "Dotum", '돋움', Helvetica, "Apple SD Gothic Neo", sans-serif;text-align: center;letter-spacing: -3px;word-spacing: 5px;line-height:27px;display:block;text-shadow: 1px 1px 0px rgb(255, 255, 255);overflow-wrap: break-word;overflow-x: hidden;overflow-y: visible;}
	</style>
<script type="text/javascript">
	$(document).ready(function() {
		if("${end_chk}" == "N"){
			$("#state").addClass("class_x");
			$("#footer").hide();
		}else{
			getResearchList();
		}
		
		$(document).on("change","#researchListAdd input:radio", function(){
			$(this).closest("div.svyf_box_s1").find("input[name=sri_seq]").val($(this).val());
		});
		
		$(document).on("keyup","#researchListAdd textarea", function(){
			$(this).closest("div.svyf_box_s1").find("input[name=sri_answer]").val($(this).attr("name")+"||"+$(this).val());
			
		});
		
		getCurriculum();
	});
	
	function getResearchList(){
		$.ajax({
			type : "POST",
			url : "${M_HOME}/ajax/st/sfResearch/questionList",
			data : {
				"lp_seq" : $("#lp_seq").val(),
				"curr_seq" : $("#curr_seq").val(),
				"sr_type" : $("#sr_type").val()
			},
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {
					$("#researchListAdd").empty();
					var htmls = "";
					var pre_srh_seq = "";
					var num = 0;
					var disable_flag = "";
					
					if(data.SFResearchSubmitChk != 0){
						disable_flag = 'disabled="disabled"';
						$("#footer").remove();
					}
					
					$.each(data.sfResearchQuestionList, function(index) {
						
						if(index != 0 && pre_srh_seq != this.srh_seq){
							htmls+= '</table>';  
					        htmls+= '</div>';
					        htmls+= '</div>';
					        $("#researchListAdd").append(htmls);
						}
	            			
						if(pre_srh_seq != this.srh_seq){
					        num++;
							htmls = '<div class="svyf_box_s1">';
																					
							//객관식일때 sri_seq 에 넣고 주관식일 때 sri_answer에 넣는다.
							//1=객관식, 2=주관식, 3=표형
							if(this.srh_type=="1")
								htmls+= '<input type="hidden" name="sri_seq" value=""/>';
							else if(this.srh_type=="2")
								htmls+= '<input type="hidden" name="sri_answer" value=""/>';
								
							htmls+= '<input type="hidden" name="essential_flag" value="'+this.essential_flag+'"/>';
							htmls+= '<input type="hidden" name="srh_type" value="'+this.srh_type+'"/>';
					        htmls+= '<table class="svyf tb2">';		 
					        htmls+= '<tr>';
					        htmls+= '<td class="t1"><span>'+this.srh_num+'</span></td>';
					        htmls+= '<td class="t2">';
					        htmls+= '<div>'+this.srh_subject+'</div>';
					        htmls+= '</td>';	
					        htmls+= '</tr>';
					        htmls+= '</table>';		
					        htmls+= '<div class="svyf2_wrap">';                             
					        htmls+= '<table class="svyf2 selectable ui-selectable">';	
					        if(this.srh_type=="1"){
						        htmls+= '<tr class="n1">';
						        htmls+= '<td class="w3" colspan="2">';
						        if(!isEmpty(this.srh_explan))						        	
						        	htmls+= '<div>'+this.srh_explan+'</div>';		
						        htmls+= '</td>';
						        htmls+= '</tr>';
					        }
						}
						
						//설문 유형 1:객관식 2:주관식 3:표형
						if(this.srh_type=="1"){
							
					        htmls+= '<tr class="n1">';
					        htmls+= '<td class="w1">';
					        htmls+= '<label class="ckwrap">';
					        if(this.answer_seq == this.sri_seq)
					        	htmls+= '<input type="radio" '+disable_flag+' checked name="rd'+num+'" value="'+this.srh_seq+'||'+this.sri_seq+'">';
				        	else
				        		htmls+= '<input type="radio" '+disable_flag+' name="rd'+num+'" value="'+this.srh_seq+'||'+this.sri_seq+'">';
					        htmls+= '<span class="ckmark"></span>';
					        htmls+= '</label>';		
					        htmls+= '</td>';
					        htmls+= '<td class="w2"><div>'+this.sri_item_explan+'</div></td>';
					        htmls+= '</tr>';
						}else if(this.srh_type=="2"){
							htmls+= '<tr class="n1">';
					        htmls+= '<td class="w3">';		
					        htmls+= '<div class="ip_wrap free_textarea"> <textarea '+disable_flag+' name="'+this.srh_seq+'" class="tarea01" style="height: 60px;">'+this.answer+'</textarea> </div>'
					        htmls+= '</td>';
					        htmls+= '</tr>';
						}
						
				        if(data.sfResearchQuestionList.length == (index+1)){
					        htmls+= '</table>';  
					        htmls+= '</div>';
					        htmls+= '</div>';
					        $("#researchListAdd").append(htmls);
				        }
				        pre_srh_seq = this.srh_seq;
					});
				}
			},
			error : function(xhr, textStatus) {
				//alert("오류가 발생했습니다.");
				document.write(xhr.responseText);
			}
		});
	}
	
	function sfResearchSubmit(){
		var chk = true;
		var radioArray = new Array();
		var pre_radio_name = "";
		var radioIndex = 0;
		
		//필수문제인거 체크
		$.each($("#researchListAdd div.svyf_box_s1"), function(){
			//필수 일 경우
			if($(this).find("input[name=essential_flag]").val() == "Y"){			
				if($(this).find("input[name=srh_type]").val() == "1"){
					if(!$(this).find("input[type=radio]").is(":checked")){
						$(this).attr("tabindex", -1).focus();		
						alert("선택되지 않은 설문이 있습니다.");	
						chk = false;
						return false;
					}
				}else if($(this).find("input[name=srh_type]").val() == "2"){
					if(isEmpty($(this).find("textarea").val())){
						$(this).attr("tabindex", -1).focus();		
						alert("입력하지 않은 설문이 있습니다.");
						chk = false;
						return false;
					}
				}else{
					
				}			
			}
		});
		
		if(chk == false)
			return;
		
		/* 
		//라디오 동적으로 추가라 라디온 name 명 가져옴.
		$.each($("#researchListAdd input:radio"),function(){
			if(pre_radio_name != $(this).attr("name")){
				radioArray[radioIndex] = $(this).attr("name");
				radioIndex++;				
			}				
			pre_radio_name = $(this).attr("name");				
		});
		
		//라디오 박스 체크 했는지 확인
		for(var i=0;i<radioArray.length;i++){			
			if(!$("#researchListAdd input:radio[name="+radioArray[i]+"]").is(":checked")){
				$("#researchListAdd input:radio[name="+radioArray[i]+"]").closest("div.svyf_box_s1").attr("tabindex", -1).focus();
				chk = false;		
				alert("선택되지 않은 값이 있습니다.");		
				return;
			}
		}
		 */
		if(!confirm("제출하시겠습니까?\n제출 후 수정할 수 없습니다."))
			return;
		
		$("#sfResearchForm").ajaxForm({
			type: "POST",
			url: "${M_HOME}/ajax/st/sfResearch/insert",
			dataType: "json",
			success: function(data, status){
				if (data.status == "200") {
					alert("제출 완료 되었습니다.");
					location.href="${HOME}/st/lesson/sfCurrResearch";
				} else {
					alert("제출 실패 하였습니다.");
					$.unblockUI();							
				}				
			},
			error: function(xhr, textStatus) {
				document.write(xhr.responseText); 
				$.unblockUI();						
			},beforeSend:function() {
				$.blockUI();						
			},complete:function() {
				$.unblockUI();						
			}    					
		});		
		$("#sfResearchForm").submit();
	}
	
	//교육과정 가져오기
	function getCurriculum() {
		$.ajax({
			type : "GET",
			url : "${HOME}/ajax/pf/lesson/curriculumInfo",
			data : {},
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {

					$("#curr_name").text(data.basicInfo.curr_name);
					$("#aca_system_name").text("(" + data.basicInfo.aca_system_name + ")");
                    
				} else {

				}
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}
</script>
</head>
<body>
	
	<!-- s_tt_wrap -->
	<div class="tt_wrap">
		<h3 class="am_tt" style="margin-left:15px;">
			<span class="tt" id="curr_name"></span>
			<span class="tt_s" id="aca_system_name"></span>
		</h3>
	</div>
	<!-- e_tt_wrap -->
	
	<!-- s_tab_wrap_cc -->
	<div class="tab_wrap_cc">
		<button class="tab01 tablinks" onClick="pageMoveCurriculumView(${S_CURRICULUM_SEQ});">과정 계획</button>
		<button class="tab01 tablinks" onClick="">수업시간표</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/unit'">단원관리</button>
		<button class="tab01 tablinks active" onClick="">만족도조사</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/currAsgmt'">종합과제</button>		
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/feScore'">형성평가</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/soosiGrade'">수시성적</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/currGrade'">종합성적</button>		
	</div>
	<!-- e_tab_wrap_cc -->
				
			<!-- s_survey -->
<div id="tab001" class="tabcontent tabcontent4n">   
			<div class="survey" id="state">   
			<!-- s_cont -->  	
			<div class="cont">	
			<!-- s_svyf_wrap -->
				<div class="svyf_wrap">
				    <div class="svyf_wrap_s">
			            <div class="svyf_tt_box">본 설문지는 수업개선의 기초 및 교육의 질적인 향상을 위해 실시합니다.<br>						
			            <span class="c1">기간 내 과정만족도 조사를 완료하지 않을 경우 매주 -0.5 점씩 감점 처리 됩니다. <br>
						바로 설문을 완료하여 제출해주세요.</span>
			            </div>	
			        </div>
			
			 </div>
			
				<div class="svyf_wrap_s">      
        <table class="svyf tb1">
            <tr>
			<td class="w1">
				<span class="tt_box">강의</span>
			</td>
            <td class="w2">
				<span class="sp01">${info.curr_name }</span>
			</td>
            </tr>
            <tr>
            <td class="w1">
				<span class="tt_box">일시</span>
			</td>
			<td class="w2">
				<span class="sp02">${ info.curr_start_date} ~ ${info.curr_end_date }</span>
			</td>
            </tr>
            <tr>
            <td class="w1">
				<span class="tt_box">교수</span>
			</td>
			<td class="w2">
				<span class="sp02">${mpfInfo.mpf }</span>
			</td>
            </tr> 
        </table>
        </div>
			<form id="sfResearchForm" onsubmit="return false;">
				<input type="hidden" id="lp_seq" name="lp_seq" value="${lp_seq }">
				<input type="hidden" id="curr_seq" name="curr_seq" value="${curr_seq }">
				<input type="hidden" id="sr_type" name="sr_type" value="${sr_type }">
				<!-- s_svyf_box1 -->
				<div class="svyf_box1" id="researchListAdd">
	
					
				</div>			
				<!-- e_svyf_box1 -->
			</form>
		</div>
		<!-- e_ptb_wrap -->
		<div class="wrap_btn_svy" id="footer">
			<button class="btn_svy" onClick="sfResearchSubmit();">제출하기</button>
		</div>
		</div>
	</div>
	<!-- e_contents -->
	
</body>
</html>
