<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<link rel="stylesheet"
	href="${RES}/fullcalendar-v3.6.2/css/calendar1.css">
<link rel="stylesheet"
	href="${RES}/fullcalendar-v3.6.2/css/fullcalendar.print.css"
	media="print">
<script src="${RES}/fullcalendar-v3.6.2/js/moment.min.js"></script>
<script src="${RES}/fullcalendar-v3.6.2/js/fullcalendar.js"></script>
<script src="${RES}/fullcalendar-v3.6.2/js/locale-all.js"></script>
<script src="${JS}/schedule.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
		getCaList();
	});
	
	
	function getCaList() {
		
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/st/aca/caDetail",
            data: {
        		"ca_seq" : "${ca_seq}"
            },
            dataType: "json",
            success: function(data, status) {
            	if (data.status == "200") {
            		var htmls = "";
            		var pfName = "";
            		$("span[data-name=curr_name]").text(data.caInfo.curr_name);
            		$("div[data-name=content]").html(data.caInfo.content);
            		if(!isEmpty(data.caInfo.picture_path))
            			$("img[name=pfImg]").attr("src","${HOME}"+data.caInfo.picture_path);
            		
            		if(!isEmpty(data.caInfo.code_name))            		
            			pfName = data.caInfo.name + "("+data.caInfo.code_name+")";
            		else
            			pfName = data.caInfo.name;
            		
            		$("span[data-name=pfName]").text(pfName);
            		$("span[data-name=req_charge]").text(data.caInfo.req_charge);
            		//$("span[data-name=req_charge]").text(numberWithCommas(data.caInfo.req_charge));
            		$("span[data-name=bankInfo]").text(data.caInfo.bank_name+ " ("+data.caInfo.account_num+") "+ data.caInfo.account_holder);
            		$("div[data-name=anDateMsg]").html("접수결과발표일("+data.caInfo.an_date+")<br>신청결과를 확인하세요.");
            		
            		htmls='<span class="tm1">'
					+'<span class="tt">접수기간</span>'
					+data.caInfo.accept_start_date+' ~ '+data.caInfo.accept_end_date+' (자정마감)</span>' 
					+'<span class="tm2">'
					+'<span class="tt">접수결과발표</span>'
					+data.caInfo.an_date+'</span>'
					+'<span class="tm3">'
					+'<span class="tt">과정기간</span>'
					+data.caInfo.curr_start_date+' ~ '+data.caInfo.curr_end_date+'</span>';
					$("div[data-name=caDate]").html(htmls);
					
					if(data.caAttach){
						$("span[data-name=currContent]").after('<button class="guide1" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+data.caAttach.file_path+'\',\''+data.caAttach.file_name+'\');">'+data.caAttach.file_name+'</button>');						
					}
					
					if(data.notesInfo){
						if(!isEmpty(data.notesInfo.file_path))
							$("span[data-name=notes]").after('<button class="guide1" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+data.notesInfo.file_path+'\',\''+data.notesInfo.file_name+'\');">'+data.notesInfo.file_name+'</button>');
						$("div[data-name=notesContent]").html(data.notesInfo.content);						
					}
					
					if(data.refundInfo){
						if(!isEmpty(data.refundInfo.file_path))
							$("span[data-name=rr]").after('<button class="guide1" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+data.refundInfo.file_path+'\',\''+data.refundInfo.file_name+'\');">'+data.refundInfo.file_name+'</button>');
						$("div[data-name=rrContent]").html(data.refundInfo.content);
					}
					
					if(data.caInfo.end_chk == "Y"){
						$("button[name=saveBtn]").remove();
					}
					
					if(data.caInfo.cas_seq > 0){
						$("button[name=saveBtn]").remove();
					}
            	} else {
					alert(data.msg);
				}
            },
            error: function(xhr, textStatus) {
                alert("오류가 발생했습니다.");
                //document.write(xhr.responseText);
            }
        });
	}	
	
	function insertApplication(){
		if(!confirm("신청하시겠습니까?")){
			return;
		}
			
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/st/aca/ca/insert",
            data: {
        		"ca_seq" : "${ca_seq}"
            },
            dataType: "json",
            success: function(data, status) {
            	if (data.status == "200") {
            		$("#spop301r").show();
            	}else if(data.status == "201"){
            		alert("신청 기간이 지났습니다.");      
            		getCaList();
            	}else if(data.status == "202"){
            		alert("이미 수강신청하였습니다.");            		
            	}else {
					alert(data.msg);
				}
            },
            error: function(xhr, textStatus) {
                alert("오류가 발생했습니다.");
                //document.write(xhr.responseText);
            }
        });
		
	}
</script>
</head>

<body class="full_schd">

	<!-- s_container_table -->
	<div id="container" class="container_table">
		<!-- s_contents -->
		<div class="contents main">

			<!-- s_left_mcon -->
			<div class="left_mcon aside_l">

				<div class="sub_menu st_am">
					<div class="title">학사일정</div>
					<ul class="sub_panel">
        				<li class="mn"><a href="${HOME }/aca/academicM" class="">학 사 력</a></li>
			       		<li class="mn"><a href="${HOME }/aca/MYscheduleMST" class="">MY 시간표</a></li>
						<li class="mn"><a href="${HOME }/aca/MYscheduleMemo" class="">개인일정</a></li>
			        	<li class="mn"><a href="${HOME }/st/aca/caList" class="on">수강신청</a></li>
					</ul>
				</div>

			</div>
			<!-- e_left_mcon -->
			
			<!-- s_main_con -->
			<div class="main_con st">

				<!-- s_메뉴 접기 버튼 -->
				<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a> 				
				<!-- e_메뉴 접기 버튼 -->

				<!-- s_tt_wrap -->
				<div class="tt_wrap">
					<h3 class="am_tt tt1">수강신청하기</h3>
				</div>
				<!-- e_tt_wrap -->


				<!-- s_stcon_wrap -->
				<div class="stcon_wrap">

					<!-- s_scont -->
					<div class="scont mg1">

						<!-- s_cont_tt -->
						<div class="cont_tt">
							<span class="tt2" data-name="curr_name"></span>

							<div class="wrap2">
								<span class="a_mp">
									<span class="pt01">
									<img src="${IMG }/ph_3.png" alt="등록된 사진 이미지" class="pt_img" name="pfImg"></span>
									<span class="ssp1" data-name="pfName"></span>
								</span>
							</div>
						</div>
						<!-- e_cont_tt -->

						<!-- s_cont_con -->
						<div class="cont_con">
							<div class="wrap_wrap">
								<div class="wrap1" data-name="caDate">
									
								</div>
							</div>
							<div class="wrap_wrap2">
								<div class="wrap3">
									<span class="tt" data-name="currContent">과목개요</span>
									<div class="con" data-name="content"></div>
								</div>
							</div>

							<div class="wrap_wrap2">
								<div class="wrap3">
									<span class="tt" data-name="notes">유의사항 안내</span>
									<div class="con" data-name="notesContent"></div>
								</div>
							</div>

							<div class="wrap_wrap2">
								<div class="wrap3">
									<span class="tt" data-name="rr">환불규정 안내</span>
									<div class="con" data-name="rrContent">
									</div>
								</div>
								<div class="wrap4">
									<span class="tt1">비용안내</span>
									<span class="num" data-name="req_charge"></span>
									<span class="sign">원</span>
									<span class="tt2" data-name="bankInfo"></span>
								</div>
							</div>
						</div>
						<!-- e_cont_con -->
					</div>
					<!-- e_scont -->

					<div class="bt_wrap">
						<button class="bt_2" name="saveBtn" onClick="insertApplication();">신청하기</button>
						<button class="bt_3" onclick="location.href='${HOME}/st/aca/caList'">취소</button>
					</div>

					<!-- s_수강신청 완료 팝업 -->
					<div class="spop301r mo301r" id="spop301r">
						<div class="wrap">
							<span class="tt">수강신청 완료</span> 
							<span class="close close301r" onclick="location.href='${HOME}/st/aca/caList'" title="닫기">X</span>
							<div class="spop_wrap">
								<div class="sswrap1">
									[인체의 기능 Ⅱ]<br>수강신청이 완료되었습니다. <br>미입금 및 조기 마감으로<br>신청이
									쥐소될 수 있습니다.
								</div>
								<div class="sswrap2" data-name="anDateMsg">
								</div>
							</div>
							<button class="btn1" title="확인" onclick="location.href='${HOME}/st/aca/caList'">확인</button>
						</div>
					</div>
					<!-- e_수강신청 완료 팝업 -->

				</div>
				<!-- e_stcon_wrap -->
			</div>
			<!-- e_main_con -->

		</div>
	</div>

</body>
</html>