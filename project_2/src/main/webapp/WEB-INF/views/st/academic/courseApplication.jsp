<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<link rel="stylesheet"
	href="${RES}/fullcalendar-v3.6.2/css/calendar1.css">
<link rel="stylesheet"
	href="${RES}/fullcalendar-v3.6.2/css/fullcalendar.print.css"
	media="print">
<script src="${RES}/fullcalendar-v3.6.2/js/moment.min.js"></script>
<script src="${RES}/fullcalendar-v3.6.2/js/fullcalendar.js"></script>
<script src="${RES}/fullcalendar-v3.6.2/js/locale-all.js"></script>
<script src="${JS}/schedule.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
		getCaList();
	});
	
	
	function getCaList() {
		
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/st/aca/caList",
            data: {
        
            },
            dataType: "json",
            success: function(data, status) {
            	if (data.status == "200") {
            		var htmls = '';
            		$.each(data.caList, function(index){
            			var picture = "";
            			var name = "";
            			
            			if(isEmpty(this.code_name))
            				name = this.name;
            			else
            				name = this.name + "("+this.code_name+")";
            			
            			if(isEmpty(this.picture_path))
            				picture = "${IMG}/ph_3.png";
           				else
           					picture = "${RES_PATH}"+this.picture_path;
            			
            			var state = "접수중";
            			var state_class = "t1";
            			if(this.end_chk == "Y"){
            				state = "마감";
            				state_class = "t2";
            			}
            			
            			htmls = '<div class="scont">'
						+'<div class="cont_tt">'
						+'<span class="tt1 t1">'+state+'</span> <span class="tt2">'+this.curr_name+'</span>';
						
						if(this.cas_seq == 0 && this.end_chk != "Y"){
							htmls+='<button class="btn1 bt1" onClick="movePage('+this.ca_seq+');">신청하기</button>'
						}else if(this.cas_state=="Y"){
							htmls+='<button class="btn1 bt3" onClick="movePage('+this.ca_seq+');">승인완료</button>'
						}else if(this.deposit_state=="N"){
							htmls+='<button class="btn1 bt2" onClick="openPopup(this);">접수결과 대기중</button>'
							+'<div class="spop301 mo301" data-name="spop301">'
							+'<div class="wrap">'
							+'<span class="close301" title="닫기" onClick="closePopup(this);">X</span>' 
							+'<div class="spop_wrap">'
							+'<div class="sswrap1">신청접수 내역<br>행정팀 확인 중</div>'
							+'<div class="sswrap2">( 결제가 완료되어야<br>선착순 승인 처리됩니다. )</div>'                   
							+'</div>'
							+'</div>'               
							+'</div>';
						}else if(this.deposit_state=="Y"){
							htmls+='<button class="btn1 bt2" onClick="movePage('+this.ca_seq+');">입금확인 승인대기</button>'
						}else if(this.deposit_state=="C"){
							htmls+='<button class="btn1 bt3" onClick="movePage('+this.ca_seq+');">입금 취소</button>'
						}else if(this.end_chk == "Y"){
							htmls+='<button class="btn1 bt3">마감 됨</button>'
						}
						htmls+='</div>'
						+'<div class="cont_con">'
						+'<div class="wrap_wrap">'
						+'<div class="wrap1">'
						+'<span class="tm1" style="width:55%;"><span class="tt">접수기간</span>'+this.accept_start_date+' ~ '+this.accept_end_date+'</span>' 
						+'<span class="tm2"><span class="tt">접수결과발표</span>'+this.an_date+'</span>'
						+'<span class="tm3"><span class="tt">과정기간</span>'+this.curr_start_date+' ~ '+this.curr_end_date+'</span>'
						+'</div>'
						+'<div class="wrap2">'
						+'<span class="a_mp">'
						+'<span class="pt01">'
						+'<img src="'+picture+'" alt="등록된 사진 이미지" class="pt_img"></span>'
						+'<span class="ssp1">'+name+'</span></span>'
						+'</div>'
						+'</div>'
						+'<div class="wrap_wrap">'
						+'<div class="wrap3">'
						+'<span class="tt">과목개요</span>'
						+'<div class="con">'+this.content+'</div>'
						+'</div>'
						+'</div>'
						+'</div>'
						+'</div>'
            			$("#caListAdd").append(htmls);
            		});
            	} else {
					alert(data.msg);
				}
            },
            error: function(xhr, textStatus) {
                alert("오류가 발생했습니다.");
                //document.write(xhr.responseText);
            }
        });
	}	

	function openPopup(obj){
		$(obj).siblings("div[data-name=spop301]").show();
	}
	
	function closePopup(obj){
		$(obj).closest("div[data-name=spop301]").hide();
	}	
	
	function movePage(ca_seq){
		post_to_url("${HOME}/st/aca/caDetail", {"ca_seq":ca_seq});
		
	}
</script>
</head>

<body class="full_schd">

	<!-- s_container_table -->
	<div id="container" class="container_table">
		<!-- s_contents -->
		<div class="contents main">

			<!-- s_left_mcon -->
			<div class="left_mcon aside_l">

				<div class="sub_menu st_am">
					<div class="title">학사일정</div>
					<ul class="sub_panel">
        				<li class="mn"><a href="${HOME }/aca/academicM" class="">학 사 력</a></li>
			       		<li class="mn"><a href="${HOME }/aca/MYscheduleMST" class="">MY 시간표</a></li>
						<li class="mn"><a href="${HOME }/aca/MYscheduleMemo" class="">개인일정</a></li>
			        	<li class="mn"><a href="${HOME }/st/aca/caList" class="on">수강신청</a></li>
					</ul>
				</div>

			</div>
			<!-- e_left_mcon -->
			<!-- s_main_con -->
			<div class="main_con st">

				<!-- s_메뉴 접기 버튼 -->
				<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a> 
				<!-- e_메뉴 접기 버튼 -->

				<!-- s_tt_wrap -->
				<div class="tt_wrap">
					<h3 class="am_tt tt1">수강신청</h3>
				</div>
				<!-- e_tt_wrap -->

				<!-- s_stcon_wrap -->
				<div class="stcon_wrap" id="caListAdd">

				</div>
				<!-- e_stcon_wrap -->
			</div>
			<!-- e_main_con -->
		</div>
	</div>
</body>
</html>