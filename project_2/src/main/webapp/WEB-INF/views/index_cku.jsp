<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="Description" content="의과대학 LMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="width=1100">
		
		<title>MLMS</title>
		
		<link rel="icon" type="${IMG}/png" href="${IMG}/favicon.png">
		<link rel="shortcut icon" href="${IMG}/favicon.png">
		<link rel="icon" href="${IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${IMG}/favicon.ico"> 
		<link rel="stylesheet" href="${CSS}/pf_style.css" type="text/css" >
		<link rel="stylesheet" href="${CSS}/datetime.css" type="text/css" >
		<link rel="stylesheet" href="${CSS}/dev_pf_style.css" type="text/css" >
		
		<script src="${JS}/lib/jquery-1.11.1.js"></script>
		<script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.form.min.js"></script>
		<script src="${JS}/lib/flexslider.js"></script>
		<script src="${JS}/lib/slied_js.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/common.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				topPopupInit();
				initPageTopButton();
				customSelectBoxInit();
				$(document).mouseup(function(e){
					layerPopupCloseInit(['div[name="myProfilePopupDiv"]']);
				});
				
				//TODO
				//각게시판 불러오기
				//롤링이미지가져오기
				//메뉴세팅
				//기관홍보배너삽입
				
			});
			
			if ('${overlapLogin}' == 'Y') {
				alert("다른 사용자가 로그인을 하였습니다.");
				location.href = '${HOME}/login';
			}
		</script>
	</head>

	<body>
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
		    <p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a>
			<a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
		<!-- s_wrap -->
		<div id="wrap">
			<!-- s_top_pop -->
			<div id="top_pop">
				<div class="wrap">
					<div class="slide-wrap">
						<ul>
						    <li class="tt"><img src="${IMG}/pop001.png" alt="의과대학 의학교육 평가인증 인증 획득 4년"></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- e_top_pop -->
			
			<!-- s_header -->
			<header class="header">
				<div class="header_top">
					<div class="top_wrap">       
						<div class="top_m">
						<!-- //TODO 임시로 제외시킴 -->
						<!-- <a href="javascript:void(0);" id="topPopupBtn" class="a_m a1" onclick="javascript:topPopupToggle();">팝업닫기</a> -->
							<a href="${HOME}" class="a_m a3">HOME</a>
							<!-- //TODO 임시로 제외시킴 -->
							<!-- <a href="#" class="a_m a4">사이트맵</a> -->
							<c:choose>
								<c:when test="${sessionScope.S_USER_ID ne null}">
									<a href="${HOME}/logout" class="a_m a5">로그아웃</a>
								</c:when>
								<c:otherwise>
									<a href="${HOME}/login" class="a_m a5">로그인</a>
								</c:otherwise>
							</c:choose>
							<a href="${HOME_URL }" class="a_m a6">기관Home</a>
						</div>
						<c:if test="${sessionScope.S_USER_ID ne null}">
							<div class="a_mp a7" onclick="myProfilePopup()">
								<c:choose>
									<c:when test='${sessionScope.S_USER_PICTURE_PATH ne null and sessionScope.S_USER_PICTURE_PATH ne ""}'>
										<span class="pt01"><img src="${sessionScope.S_USER_PICTURE_PATH}" alt="나의 대표 이미지" class="pt_img"></span>
									</c:when>
									<c:otherwise>
										<span class="pt01"><img src="${IMG}/profile_default.png" alt="나의 대표 이미지" class="pt_img"></span>
									</c:otherwise>
								</c:choose>
								
								<span class="ssp1">${sessionScope.S_USER_NAME} (${sessionScope.S_USER_DEPARTMENT_NAME})</span>
							</div>
							<!-- s_pop_pop9 -->
							<div id="pop_pop9" name="myProfilePopupDiv">
								<div class="spop9 show" id="spop9">
									<span class="tt">${sessionScope.S_USER_NAME} (${sessionScope.S_USER_DEPARTMENT_NAME})</span>
									<span class="tt">${sessionScope.S_USER_EMAIL}</span>
									<button onclick="location.href='${HOME}/common/my'" class="btn btn05">내 정보 수정</button>
									<span onclick="myProfilePopup()" class="pop_close_s" title="닫기">X</span>
								</div>
							</div>
							<!-- e_pop_pop9 --> 
						</c:if>
					</div>
				</div>
				<!-- s_gnbwrap -->			
				<div class="gnbwrap">
					<!-- s_gnb_swrap -->  
					<div class="gnb_swrap">
						<h1 class="logo"><a href="${HOME}" title="홈으로 가기"><img src="${TOP_LOGO}" alt="의과대학 로고"></a></h1>
					</div>
					<!-- e_gnb_swrap --> 
					
					<!-- s_gnb_fwrap -->  
					<div class="gnb_fwrap">
						<div class="gnb_swrap2">
							<ul id="gnb" class="gnb">
								<c:forEach items="${sessionScope.S_ALLOW_MENU_LIST}" var="allowMenuList" step="1">
									<li><a href="${HOME}${allowMenuList.url}" onclick="javascript:pageLinkCheck('${HOME}','${HOME}${allowMenuList.url}'); return false;" <c:if test='${allowMenuList.now_menu_yn eq "Y"}'>class="on"</c:if>>${allowMenuList.name}</a></li>
								</c:forEach>
							</ul>
						</div>
					</div>
					<!-- e_gnb_fwrap --> 
				</div>
				<!-- e_gnbwrap -->      
			</header>   
			
			<!-- s_container_table -->
			<div id="container" class="container_table">
				<!-- s_contents -->
				<div class="contents index">
				
					<!-- s_slidewrap -->
					<div class="slidewrap">    
				        <div id="slider001" class="flexslider_n1" style="display: block;">				
							<ul class="slides sd1">									
							    <li class="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
								    <a href="#" title="" target="_blank">
				                        <div class="sd_swrap bg1">
				                           
				                            <h3>2017-1학기 졸업예정자<br>취업심화교육 이수자<br>수강료지원 접수</h3>
				                            <p class="tt">대상</p>
				                            <p class="con">2017학년도 1학기 졸업연기자 중<br>교육기관(학원포함)에서<br>취업관련 교육을 받은 학생</p>
				                            
				                            <p class="tt">기간</p>
				                            <p class="con">2017.05.08.(월)~ 2017.05.12.(금)</p>
				                            
				                            <p class="tt">문의</p>
				                            <p class="con">해당학과 사무실</p>
				                            
				                        </div>
				                        <div class="sd_bwrap"></div>
				                        <div class="sd_img1"><img src="${IMG}/cku_slide01.png" alt="메인 비주얼 이미지" draggable="false" ></div>			    
								    </a>
							    </li>
							    <li class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">
								    <a href="#" title="" target="_blank">
				                        <div class="sd_swrap bg2">
				                            <h3>메인 비주얼 알림<br>대제목 두울<br>대제목 두울</h3>
				                            <p class="tt">소제목1</p>
				                            <p class="con">메인 비주얼 알림 내용들</p>
				                            <p class="tt">소제목2</p>
				                            <p class="con">메인 비주얼 알림 내용들</p>
				                            <p class="tt">소제목3</p>
				                            <p class="con">메인 비주얼 알림 내용들</p>
				                        </div>
				                        <div class="sd_bwrap"></div>
				                        <div class="sd_img1"><img src="${IMG}/cku_slide02.png" alt="메인 비주얼 이미지" draggable="false" ></div>			    
								    </a>
							    </li>
							    <li class="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
								    <a href="#" title="" target="_blank">
				                        <div class="sd_swrap bg3">
				                            <h3>메인 비주얼 알림<br>대제목 셋<br>대제목 셋</h3>
				                            <p class="tt">소제목1</p>
				                            <p class="con">메인 비주얼 알림 내용들</p>
				                            <p class="tt">소제목2</p>
				                            <p class="con">메인 비주얼 알림 내용들</p>
				                            <p class="tt">소제목3</p>
				                            <p class="con">메인 비주얼 알림 내용들</p>
				                        </div>
				                        <div class="sd_bwrap"></div>
				                        <div class="sd_img1"><img src="${IMG}/cku_slide03.png" alt="메인 비주얼 이미지" draggable="false" ></div>			    
								    </a>
							    </li>
							</ul>      
				        </div>
				    </div> 
					<!-- e_slidewrap -->
					<!-- s_main_table_wrap -->	
					<div class="main_table_wrap">
						<h3 class="main_tb_h3">학사일정</h3>
						<table class="yyschd">
							<caption>학사일정</caption>
							<thead>
								<tr>
									<th class="w1">월</th>
									<th class="w2">시작날짜</th>
									<th class="w2">종료날짜</th>
									<th class="w3">학사내용</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th rowspan="1">12월 (2017년)</th>
									<td>2017.12.19</td>
									<td>2018.01.11</td>
									<td><p>동계 계절학기개강 및 종강</p></td>
								</tr>
								<tr>
									<th rowspan="3">1월</th>
									<td>2018.01.11</td>
									<td>2018.01.11</td>
									<td><p>동계 계절학기 종강</p></td>
								</tr>
								<tr>
									<td>2018.01.11</td>
								    <td>2018.01.15</td>
								    <td><p>동계 계절학기 성적입력</p></td>
								</tr>
								<tr>	
									<td>2018.01.19</td>
									<td>2018.01.19</td>
									<td><p>2017학년도 전기 졸업 사정</p></td>
								</tr>
								<tr>	
									<th rowspan="4">2월</th>
									<td>2018.02.05</td>
									<td>2018.02.28</td>
									<td><p>재입학 기간</p></td>
								</tr>
								<tr>
									<td>2018.02.05</td>
									<td>2018.02.09</td>
									<td><p>일반휴학, 복학기간</p></td>
								</tr>
								<tr>
									<td>2018.02.20</td>
									<td>2018.02.23</td>
									<td><p>재학생 수강신청</p></td>
								</tr>
								<tr>
									<td>2018.02.23</td>
									<td>2018.02.23</td>
									<td><p>2017학년도 전기 학위수여식</p></td>
								</tr>
								<tr>
									<th rowspan="4">3월</th>
									<td>2018.03.05</td>
									<td>2018.03.05</td>
									<td><p>2018학년도 1학기 개강</p></td>
								</tr>
								<tr>
									<td>2018.03.05</td>
									<td>2018.03.09</td>
									<td><p>재수강 성적 포기</p></td>
								</tr>
								<tr>
									<td>2018.03.05</td>
									<td>2018.03.09</td>
									<td><p>조기 졸업 신청</p></td>
								</tr>
								<tr>
									<td>2018.03.26</td>
									<td>2018.03.30</td>
									<td><p>학점 인증제</p></td>
								</tr>
								<tr>
									<th rowspan="4">4월</th>
									<td>2018.04.16</td>
									<td>2018.04.20</td>
									<td><p>수강포기</p></td>
								</tr>
								<tr>
									<td>2018.04.16</td>
									<td>2018.04.20</td>
									<td><p>중간 설문 조사</p></td>
								</tr>
								<tr>
									<td>2018.04.23</td>
									<td>2018.04.27</td>
									<td><p>중간고사</p></td>
								</tr>
								<tr>
									<td>2018.04.30</td>
									<td>2018.05.04</td>
									<td><p>중간 성적 입력</p></td>
								</tr>
								<tr>
									<th rowspan="1">5월</th>
									<td>2018.05.14</td>
									<td>2018.05.18</td>
									<td><p>중간 성적 열람</p></td>
								</tr>
								<tr>
									<th rowspan="7">6월</th>
									<td>2018.06.04</td>
									<td>2018.06.08</td>
									<td><p>복수, 부전공 신청</p></td>
								</tr>
								<tr>
									<td>2018.06.11</td>
									<td>2018.06.15</td>
									<td><p>기말고사</p></td>
								</tr>
								<tr>
									<td>2018.06.18</td>
									<td>2018.06.22</td>
									<td><p>보강주간(개교기념일,어린이날 대체공휴일5.7, 석가탄신일5.22, 현충일 6.6, 지방선거6.13)</p></td>
								</tr>
								<tr>
									<td>2018.06.22</td>
									<td>2018.06.22</td>
									<td><p>2018학년도 1학기 종강</p></td>
								</tr>
								<tr>
									<td>2018.06.25</td>
									<td>2018.06.25</td>
									<td><p>하계 계절학기 개강</p></td>
								</tr>
								<tr>
									<td>2018.06.25</td>
									<td>2018.06.27</td>
									<td><p>계절학기 재수강 성적포기</p></td>
								</tr>
								<tr>
									<td>2018.06.25</td>
									<td>2018.06.29</td>
									<td><p>성적 열람 및 이의 신청</p></td>
								</tr>
								<tr>
									<th rowspan="2">7월</th>
									<td>2018.07.13</td>
									<td>2018.07.13</td>
									<td><p>하계 계절학기 종강</p></td>
								</tr>
								<tr>
									<td>2018.07.13</td>
									<td>2018.07.17</td>
									<td><p>하계 계절학기 성적입력</p></td>
								</tr>
								<tr>
									<th rowspan="7">8월</th>
									<td>2018.08.06</td>
									<td>2018.08.10</td>
									<td><p>일반휴학, 복학기간</p></td>
								</tr>
								<tr>
									<td>2018.08.06</td>
									<td>2018.08.24</td>
									<td><p>재입학 기간</p></td>
								</tr>
								<tr>
									<td>2018.08.16</td>
									<td>2018.08.16</td>
									<td><p>후기 학위 수여</p></td>
								</tr>
								<tr>
									<td>2018.08.17</td>
									<td>2018.08.23</td>
									<td><p>재학생 수강신청</p></td>
								</tr>
								<tr>
									<td>2018.08.20</td>
									<td>2018.08.24</td>
									<td><p>재학생 등록기간</p></td>
								</tr>
								<tr>
									<td>2018.08.27</td>
									<td>2018.08.27</td>
									<td><p>2018학년도 2학기 개강</p></td>
								</tr>
								<tr>
									<td>2018.08.27</td>
									<td>2018.08.31</td>
									<td><p>재수강 성적 포기</p></td>
								</tr>
								<tr>
									<th rowspan="1">9월</th>
									<td>2018.09.17</td>
									<td>2018.09.21</td>
									<td><p>학점 인증제</p></td>
								</tr>
								<tr>
									<th rowspan="4">10월</th>
									<td>2018.10.08</td>
									<td>2018.10.12</td>
									<td><p>수강포기</p></td>
								</tr>
								<tr>
									<td>2018.10.08</td>
									<td>2018.10.12</td>
									<td><p>중간 설문 조사</p></td>
								</tr>
								<tr>
									<td>2018.10.15</td>
									<td>2018.10.19</td>
									<td><p>중간고사</p></td>
								</tr>
								<tr>
									<td>2018.10.29</td>
									<td>2018.11.02</td>
									<td><p>중간 성적 입력</p></td>
								</tr>
								<tr>
									<th rowspan="2">11월</th>
									<td>2018.11.05</td>
									<td>2018.11.09</td>
									<td><p>중간 성적 열람</p></td>
								</tr>
								<tr>
									<td>2018.11.26</td>
									<td>2018.11.30</td>
									<td><p>교직, 전과, 복수, 부전공 신청</p></td>
								</tr>
								<tr>
									<th rowspan="6">12월</th>
									<td>2018.12.03</td>
									<td>2018.12.07</td>
									<td><p>기말고사</p></td>
								</tr>
								<tr>
									<td>2018.12.10</td>
									<td>2018.12.13</td>
									<td><p>보강주간(추석9.24-25, 개천절10.3, 한글날10.9)</p></td>
								</tr>									
								<tr>
									<td>2018.12.13</td>
									<td>2018.12.13</td>
									<td><p>2018학년도 2학기 종강</p></td>
								</tr>
								<tr>
									<td>2018.12.14</td>
									<td>2018.12.14</td>
									<td><p>동계 계절학기 개강</p></td>
								</tr>
								<tr>
									<td>2018.12.14</td>
									<td>2018.12.18</td>
									<td><p>동계 계절학기 재수강 성적포기</p></td>
								</tr>
								<tr>
									<td>2018.12.24</td>
									<td>2018.12.28</td>
									<td><p>성적 열람 및 이의 신청</p></td>
								</tr>
							</tbody>
						</table>	
					</div>	
				</div>
				<!-- e_contents -->
			</div>
			<!-- e_container_table -->
			
			<a href="#" id="backtotop" title="To top" class="totop"></a>
			
			<!-- s_footer -->
			<footer id="footer" class="ind">
		        <div class="footer_con">           
		            <div class="footer_scon">   
		                <ul class="f_menu">
						    <li><a href="#" title="새창" class="fmn1">개인정보처리방침</a></li>
		                    <li><a href="#" title="새창" class="fmn2"><span>|</span>이메일무단수집거부</a></li>                
					    </ul>   
		                <p class="addr">${ADDRESS }</p>
		                <a href="${HOME}" title="홈으로 가기" class="logo_wrap"><img src="${BOTTOM_LOGO}" alt="의과대학 로고" class="b_logo"></a>
		            </div>
		        </div>
			</footer>
			<!-- e_footer -->
		</div>
		<!-- e_wrap -->
	</body>
</html>