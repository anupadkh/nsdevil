<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="${CSS}/mpf_style.css" type="text/css"> 
		<style>			
			.gnb li a{display: inline-block;margin: 0;padding:0px;width:100%;height:40px;line-height: 40px;float: left;font-size:15px;color: rgba(255,255,255,1);border-bottom: solid 2px rgba(1, 96, 122, 1);text-align: center;text-indent: 0;box-sizing: border-box;}			
			.boardwrap {position: relative;top: 0;left: 7px;display: block;margin: 0;overflow:hidden;width: 236px;background-color:transparent;border:0px;height: auto;max-height: 800px;overflow-x: hidden;overflow-y: auto;border-top: dotted 1px rgb(91, 170, 233);border-bottom: dotted 1px rgb(91, 170, 233);}
			.boardwrap span.sp_tt{}
			.boardwrap span.sp_tt:hover{}
		</style>
        
    	<script src="${HOME}/resources/smarteditor_v2.9/js/service/HuskyEZCreator.js"></script>     
	<script type="text/javascript">
		var editor_object = [];

		$(document).ready(function() {	
			$(document).on( 'keyup', 'textarea', function (e){
				$(this).css('height', 'auto' );
				$(this).height( this.scrollHeight );
			});
			$(document).find( 'textarea' ).keyup();
			
			nhn.husky.EZCreator.createInIFrame({
		       oAppRef: editor_object,
		       elPlaceHolder: "lessonData",
               sSkinURI : "${HOME}/resources/smarteditor_v2.9/SmartEditor2Skin_ko_KR.html"     ,
               htParams : {
		           // 툴바 사용 여부 (true:사용/ false:사용하지 않음)
		           bUseToolbar : true,            
		           // 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
		           bUseVerticalResizer : true,    
		           // 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
		           bUseModeChanger : true
		       },
               fOnAppLoad:function(){
            	   editor_object.getById["lessonData"].exec("SE_FIT_IFRAME", [0,400]);	   
               }
		   });
			  
			//사진 여라장 등록
	        $(document).on("change", "input[name=imgMulti]", function(){
	        	handleMultiImgFile(this);			
	        });
			//파일 등록
	        $(document).on("change", "input[name=fileMulti]", function(){
	        	handleFileMulti(this);
	        });			
			//동영상 등록
	        $(document).on("change", "input[name=video]", function(){
	        	handleVideoMulti(this);
	        });			
			
	        getLessonData();

            $(document).on("click", ".del_btn", function(){               
                $(this).closest("div.box_n1").remove();              
            });

            $(document).on("click", ".sdel_btn", function(){               
                $(this).closest("div.box_n1").remove();              
            });

            $(document).on("click", ".fdel_btn", function(){               
                $(this).closest("div.wrap").remove();              
            });
            
            //수업계획서 타이틀 가져오기
            getLessonTitleInfo();
		});
								
		//사진 여러장 등록
		function handleMultiImgFile(input){
			var version = get_version_of_IE();

			if(version != -1 && version <= 9){
				var fileExt = $(input).val().split('.').pop().toLowerCase(); //마지막 . 위치로 확장자 자름
				if($.inArray(fileExt, ['gif','png','jpg','jpeg','bmp','zip']) != -1) {
					alert('gif,png,jpg,jpeg,bmp,zip 파일만 업로드 할수 있습니다.');
					$("#imgMulti"+imgIndex).remove();
					return;
				}
				
				htmls += '<div class="box_n1" name="multiImg">'
			        +'<div class="pt_wrap">' 
			        +'<div class="wrap_s">'
		            + '저장 후<br>미리보기가 제공 됩니다.';
			    	+'</div>'
			        //+'<span class="btn_num" title="등록된 파일 수">'+filesArr.length+'</span>'
			        +'<input type="hidden" name="imgGroupNum" value="1"/>'
			        //+'<button class="btn_mdf" title="이미지 수정하기">대표사진 설정</button>'
			        +'</div>'			
			        +'<div class="wrap">'
			    	+'<span class="sp1">'+$(input).val()+'</span>'
					+'<input class="ip_dsc" maxlength="15" name="multiImgExplan" placeholder="(선택입력)파일에 대한 간단한 코멘트를 남기실수 있어요 (최대15자)" type="text">'
			        //+'<button class="btn_save" title="저장하기">저장</button>'
			        +'</div>'			
			        +'<button class="btn_c" name="multiImg" onClick="imgMultiDelete('+imgIndex+', this);" title="삭제하기">X</button>'
			        +'</div>';	
			        
				
			}else{
				var files = $(input)[0].files;
				var filesArr = Array.prototype.slice.call(files);
				var ext = "";
				var img_reulst = "";
				var htmls = '';

				if(files.length > 1){
					for(var i=0; i<files.length; i++){
						var fileExt = files[i].name.split('.').pop().toLowerCase(); //마지막 . 위치로 확장자 자름
						if($.inArray(fileExt, ['zip']) != -1) {
							alert('zip 파일은 하나만 선택해주세요.');
							$("#imgMulti"+imgIndex).remove();
							return;
						}
					}
				}

				var i = 0;
				
				filesArr.forEach(function(f){
					var reader = new FileReader();
					reader.onload = function(e){
						var fileExt = f.name.split('.').pop().toLowerCase(); //마지막 . 위치로 확장자 자름
						if($.inArray(fileExt, ['gif','png','jpg','jpeg','bmp','zip']) == -1) {
							alert('gif,png,jpg,jpeg,bmp,zip 파일만 업로드 할수 있습니다.');
							$("#imgMulti"+imgIndex).remove();
							return;
						}
						if(i==0){
							htmls += '<div class="box_n1" name="multiImg">'
						        +'<div class="pt_wrap">' 
						        +'<div class="wrap_s">';
						        
						    if(fileExt == "zip")
					            htmls+= 'Zip 파일은 저장 후<br>미리보기가 제공 됩니다.';
						    else						    	
						        htmls+='<img src="'+e.target.result+'" alt="학습자료 이미지">';
						        
					    	htmls+='</div>';
					    	
				    		if(fileExt != "zip")
				    			htmls+='<span class="btn_num" title="등록된 파일 수">'+filesArr.length+'</span>';
						   						    
					        htmls+='<input type="hidden" name="imgGroupNum" value="'+filesArr.length+'"/>'
						        //+'<button class="btn_mdf" title="이미지 수정하기">대표사진 설정</button>'
						        +'</div>'			
						        +'<div class="wrap">';
						    if(filesArr.length == 1)
						    	htmls+='<span class="sp1">'+f.name+'</span>'				
					    	else
					    		htmls+='<span class="sp1">'+f.name+' 외 '+(filesArr.length-1)+'장</span>'
					    		htmls+='<input class="ip_dsc" maxlength="15" name="multiImgExplan" placeholder="(선택입력)파일에 대한 간단한 코멘트를 남기실수 있어요 (최대15자)" type="text">'
						        //+'<button class="btn_save" title="저장하기">저장</button>'
						        +'</div>'			
						        +'<button class="btn_c" name="multiImg" onClick="imgMultiDelete('+imgIndex+', this);" title="삭제하기">X</button>'
						        +'</div>';			     
						}					
						i++;

						if(filesArr.length == i){
				        	$("#imgListAdd").append(htmls);
						}
					}
					reader.readAsDataURL(f);
				});		
			}

		}
				
		//일반 파일 등록
		function handleFileMulti(input){
			
			var fileExt = $(input).val().split('.').pop().toLowerCase(); //마지막 . 위치로 확장자 자름			
			if($.inArray(fileExt, ["jpg","jpeg","gif","png","bmp","mp3","mp4","xls","xlsx","txt","hwp","pdf","zip"]) == -1) {
				alert("jpg,jpeg,gif,png,bmp,mp3,mp4,xls,xlsx,txt,hwp,pdf,zip 파일만 업로드 할수 있습니다.");
				$("#fileMulti"+fileIndex).remove();
				return;
			}
 				
			var htmls = '';
			var fileValue = $(input).val().split("\\");
			var fileName = fileValue[fileValue.length-1]; // 파일명
			htmls += '<div class="wrap">';                                           
			htmls += '<span class="sp1">'+fileName+'</span>';    
    		htmls += '<button class="btn_c" onClick="fileMultiDelete('+fileIndex+', this);" title="삭제하기">X</button> ';
    		htmls += '</div>';
   			$("#fileListAdd").append(htmls); 
    		
		}			
		
		//동영상 파일 등록
		function handleVideoMulti(input){
			
			var htmls = '';
			var fileValue = $(input).val().split("\\");
			var fileName = fileValue[fileValue.length-1]; // 파일명
			htmls+='<div class="box_n1">'
				+'<div class="video_wrap">'
				//+'<button class="btn_r" title="대표사진 등록">대표사진 등록</button>'
				+'</div>'	
				+'<div class="wrap">'                                           
				+'<span class="sp1">'+fileName+'</span>'				
				+'<input class="ip_dsc" maxlength="15" name="videoExplan" placeholder="(선택입력)파일에 대한 간단한 코멘트를 남기실수 있어요 (최대15자)" type="text">'
				//+'<button class="btn_save" title="저장하기">저장</button>'
				+'</div>'	
				+'<button class="btn_c" title="삭제하기">X</button>'
				+'</div>';
   			$("#videoListAdd").append(htmls); 
    		
		}
		
		function fileMultiDelete(index, obj){
			$(obj).closest("div.wrap").remove();
			$("#fileMulti"+index).remove();
		}
		
		function imgMultiDelete(index, obj){
			$(obj).closest("div.box_n1").remove();
			$("#imgMulti"+index).remove();
		}
		
		function imgDelete(index, obj){
			$(obj).closest("div.box_n1").remove();
			$("#img"+index).remove();
		}
		
		function videoDelete(index, obj){
			$(obj).closest("div.box_n1").remove();
			$("#video"+index).remove();
		}
		
		function youtubeDelete(obj){
			$(obj).closest("div.box_n1").remove();
		}
		
		function lessonDataSubmit(){
			editor_object.getById["lessonData"].exec("UPDATE_CONTENTS_FIELD", []);
			$("textarea[name=memo]").val($("textarea[name=memo]").val().replace(/\n/g, "<br/>"));
			$("#lessonDataForm").ajaxForm({
                type: "POST",
                url: "${HOME}/ajax/pf/lesson/lessonData/create",
                dataType: "json",
                success: function(data, status){
                    if (data.status == "200") {
                    	$("input[name=lesson_data_seq]").val(data.lesson_data_seq);
                        alert("저장이 완료되었습니다.");
                        pageMoveLessonData('${HOME}');
                    	//getLessonData();      
                    } else {
                        alert(data.status);
                        $.unblockUI();                          
                    }
                    
                },
                error: function(xhr, textStatus) {
                    document.write(xhr.responseText); 
                    $.unblockUI();                      
                },
    			uploadProgress: function(event, position, total, percentComplete) {
    			    $("#progressbar").width(percentComplete + '%');
    			    $("#statustxt").html(percentComplete + '%');
    			    if(percentComplete>50) {
    			    	$("#statustxt").css('color','#fff');
    			    }
    			},beforeSend:function() {
                    $.blockUI();                        
                },complete:function() {
                    $.unblockUI();                      
                }                       
            });     
			$("#lessonDataForm").submit();   			
		}
		
		function getYoutubeUrl(url) {
		    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
		    var match = url.match(regExp);

		    if (match && match[2].length == 11) {
		        return match[2];
		    } else {
		        return 'error';
		    }
		}
		
		function youtubeAdd(){
			if(isEmpty($("#youtubeUrl").val())){
				alert("유튜브 주소를 입력하세요");
				return false;
			}
			var regex = /^(((http(s?))\:\/\/)?)([0-9a-zA-Z\-]+\.)+[a-zA-Z]{2,6}(\:[0-9]+)?(\/\S*)?/;

			if(!regex.test($("#youtubeUrl").val())){
				alert("유효한 url을 입력해주세요.")
				return false;
			}
			
			
			var url = getYoutubeUrl($("#youtubeUrl").val())
			var htmls = '<div class="box_n1">'
		        +'<div class="video_wrap">'			
		        +'<div class="video-container">'
		        +'<iframe src="https://www.youtube.com/embed/'+url+'" width="170" allowfullscreen=""></iframe>'
		        +'</div><input type="hidden" name="youtubeUrl" value="https://www.youtube.com/embed/'+url+'"/>'
		        +'</div>'
		        +'<div class="wrap">'               
		        +'<input class="ip_dsc" maxlength="15" name="youtubeExplan" placeholder="(선택입력)파일에 대한 간단한 코멘트를 남기실수 있어요 (최대15자)" type="text">'
		        //+'<button class="btn_save" title="저장하기">저장</button>'
		        +'</div>'
		        +'<button class="btn_c" onClick="youtubeDelete(this);" title="삭제하기">X</button>'
		        +'</div>';
		        
			$("#youtubeAdd").append(htmls);
			$("#youtubeUrl").val("");
		}

        var fileIndex = 0;
		//파일 인풋 추가해서 해당 인풋 클릭하게 만든다.
		function fileMultiAdd(){
            fileIndex++;
			$("#fileInputAdd").append('<input type="file" name="fileMulti" id="fileMulti'+fileIndex+'" style="display:none;"/>');
			$("#fileInputAdd input").last().click();
		}

        var imgIndex = 0;
		function imgMultiAdd(){
			var version = get_version_of_IE();
			if(version != -1 && version <= 9){
				alert("이미지 여러장 등록은 익스플로러 10 이상버전에서 가능합니다.\n ZIP 파일로 등록해주세요.");
				imgIndex++;
				$("#imgInputAdd").append('<input type="file" id="imgMulti'+imgIndex+'" name="imgMulti" style="display:none;" accept="file_extension|image/*">');
				$("#imgInputAdd input").last().click();
			}else{
				imgIndex++;
				$("#imgInputAdd").append('<input type="file" id="imgMulti'+imgIndex+'" name="imgMulti" multiple style="display:none;" accept="file_extension|image/*">');
				$("#imgInputAdd input").last().click();
			}
				
		}
		
		var oneimgIndex = 0;
		function imgAdd(){
			oneimgIndex++;
			$("#imgAdd").append('<input type="file" id="img'+oneimgIndex+'" name="img" style="display:none;" accept="image/*">');
			$("#imgAdd input").last().click();
		}
		
		var videoIndex = 0;
		function videoAdd(){
			videoIndex++;
			$("#videoInputAdd").append('<input type="file" id="video'+videoIndex+'" name="video" style="display:none;" accept="video/mp4">');
			$("#videoInputAdd input").last().click();
		}
		
		function getLessonData(){
			if(isEmpty($("input[name=lesson_data_seq]").val()))
				return false;
			
			$.ajax({
                type: "POST",
                url: "${HOME}/ajax/pf/lesson/lessonData/list",
                data: {
                    "lesson_data_seq" : $("input[name=lesson_data_seq]").val()                   
                },
                dataType: "json",
                success: function(data, status) {                	
                	if(data.status == "200"){                		
                		if(!isEmpty(data.lessonDataList))
                			$("button[name=saveBtn]").text("수정");
                		else
                			$("button[name=saveBtn]").text("등록");
                		reset();
                		if(!isEmpty(data.lessonDataList))
							$("textarea[name=memo]").val(data.lessonDataList.memo.split('<br/>').join("\r\n"));
                		if(!isEmpty(data.lessonDataList))
							$("textarea[name=content]").val(data.lessonDataList.content);
						var htmls = '';
						var addDivId = "";
						$.each(data.lessonAttachList, function(index){
							if(this.attach_type =="I"){
						    	var filename = this.file_name.split("||");
						    	
						    	htmls = '<div class="box_n1" name="multiImg">'
							        +'<div class="pt_wrap"><input type="hidden" name="lesson_attach_seq" value="'+this.lesson_attach_seq+'"/>' 
							        +'<div class="wrap_s">'
							        +'<img src="${RES_PATH}'+this.file_path.split("||")[0]+'" alt="'+this.file_name.split("||")[0]+'">'
							        +'</div>';
							    if(filename.length > 1)							    	
							        htmls+='<span class="btn_num" title="등록된 파일 수">'+filename.length+'</span>';
							        //+'<button class="btn_mdf" title="이미지 수정하기">대표사진 설정</button>'
							    htmls+='</div>'			
							        +'<div class="wrap">';
							    if(filename.length == 1)
							    	htmls+='<span class="sp1">'+this.file_name.split("||")[0]+'</span>'				
						    	else
						    		htmls+='<span class="sp1">'+this.file_name.split("||")[0]+' 외 '+(filename.length-1)+'장</span>'
						    		htmls+='<input class="ip_dsc" maxlength="15" name="explan" placeholder="(선택입력)파일에 대한 간단한 코멘트를 남기실수 있어요 (최대15자)" value="'+this.explan+'" type="text">'						    		
							        +'<button class="btn_save" title="저장하기" onclick="saveComment(this);">저장</button>'
							        +'</div>'			
							        +'<button class="btn_c del_btn" name="multiImg" title="삭제하기">X</button>'
							        +'</div>';	
					            	$("#imgListAdd").append(htmls);

						    }else if(this.attach_type=="A"){	
						    	htmls = '<div class="wrap"><input type="hidden" name="lesson_attach_seq" value="'+this.lesson_attach_seq+'"/>';                                           
									+ '<span class="sp1">'+this.file_name+'</span>';    
						    		+ '<button class="btn_c fdel_btn" title="삭제하기">X</button> ';
						    		+ '</div>';
					   			$("#fileListAdd").append(htmls); 
					   			
			                }else if(this.attach_type=="V"){
			                	htmls='<div class="box_n1">'
			        				+'<div class="video_wrap"><input type="hidden" name="lesson_attach_seq" value="'+this.lesson_attach_seq+'"/>'
			        				+'<video width="170" controls>'
			        				+'<source src="${RES_PATH}'+this.file_path+'" type="video/mp4">'
			        				+'</video>'
			        				//+'<button class="btn_r" title="대표사진 등록">대표사진 등록</button>'
			        				+'</div>'	
			        				+'<div class="wrap">'                                           
			        				+'<span class="sp1">'+this.file_name+'</span>'				
			        				+'<input class="ip_dsc" maxlength="15" name="explan" placeholder="(선택입력)파일에 대한 간단한 코멘트를 남기실수 있어요 (최대15자)" value="'+this.explan+'" type="text">'			        				
			        				+'<button class="btn_save" title="저장하기" onclick="saveComment(this);">저장</button>'
			        				+'</div>'	
			        				+'<button class="btn_c del_btn" title="삭제하기">X</button>'
			        				+'</div>';
			           			$("#videoListAdd").append(htmls); 
		                    }else if(this.attach_type=="Y"){		
		                    	htmls ='<div class="box_n1">'
			        		        +'<div class="video_wrap"><input type="hidden" name="lesson_attach_seq" value="'+this.lesson_attach_seq+'"/>'			
			        		        +'<div class="video-container">'
			        		        +'<iframe src="'+this.youtube_url+'" width="170"  encrypted-media allowfullscreen=""></iframe>'
			        		        +'</div>'
			        		        +'</div>'
			        		        +'<div class="wrap">'               
			        		        +'<input class="ip_dsc" maxlength="15" name="explan" placeholder="(선택입력)파일에 대한 간단한 코멘트를 남기실수 있어요 (최대15자)" value="'+this.explan+'" type="text">'
			        		        +'<button class="btn_save" title="저장하기" onclick="saveComment(this);">저장</button>'
			        		        +'</div>'
			        		        +'<button class="btn_c del_btn" onClick="youtubeDelete(this);" title="삭제하기">X</button>'
			        		        +'</div>';
		        		        $("#youtubeAdd").append(htmls);
						    } else{
						    	htmls = '<div class="wrap"><input type="hidden" name="lesson_attach_seq" value="'+this.lesson_attach_seq+'"/>'                                          
									+ '<span class="sp1" style="cursor:pointer;" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');">'+this.file_name+'</span>'   
						    		+ '<button class="btn_c fdel_btn" title="삭제하기">X</button> '
						    		+ '</div>';
					    		
   	                            $("#fileListAdd").append(htmls);
		                    }
						});
						$.each($("textarea"),function(){
                        	$(this).height($(this).prop("scrollHeight"));
                        });
                	}
                },
                error: function(xhr, textStatus) {
                    //alert("오류가 발생했습니다.");
                    document.write(xhr.responseText);
                }
            }); 
		}
		
		function reset(){
   			$("#videoListAdd").empty();
			$("#imgListAdd").empty();
			$("#fileListAdd").empty();
			$("#youtubeAdd").empty();     
			$("#imgTitle").val("");
			$("#fileImgTitle").val("");
            $("input[name=imgMulti]").remove();
            $("input[name=fileMulti]").remove();
		}
		
	    function getLessonTitleInfo(){
            
            $.ajax({
                type: "POST",
                url: "${HOME}/ajax/pf/lesson/lessonPlanBasicInfo",
                data: {                  
                },
                dataType: "json",
                success: function(data, status) {
                	if(data.status == "200"){
	                    var lesson_date = data.lessonPlanBasicInfo.lesson_date.split("/");
	                    var aca_name = data.lessonPlanBasicInfo.year + " " + data.lessonPlanBasicInfo.l_aca_system_name + " " + data.lessonPlanBasicInfo.m_aca_system_name + " " + data.lessonPlanBasicInfo.aca_system_name;
	                    
	                    $("span[name=lesson_date]").text(data.lessonPlanBasicInfo.lesson_date);
	                    $("span[name=lesson_subject]").text("["+data.lessonPlanBasicInfo.period+"교시] "+data.lessonPlanBasicInfo.lesson_subject);                    
	                    $("span[name=curr_name]").text(" ("+data.lessonPlanBasicInfo.curr_name +" / "+ aca_name+")");                    
                	}
                },
                error: function(xhr, textStatus) {
                    //alert("오류가 발생했습니다.");
                    document.write(xhr.responseText);
                }
            }); 
        }
        
	    function saveComment(obj){
	    	var explan = $(obj).siblings("input[name=explan]").val();
	    	var lesson_attach_seq = $(obj).closest("div.box_n1").find("input[name=lesson_attach_seq]").val();
	    	var multiImgFlag = "N";
	    	if(lesson_attach_seq.split(",").length > 1)
	    		multiImgFlag = "Y";
	    	
	    	if(isEmpty(explan)){
	    		alert("코멘트를 입력 후 저장을 눌러주세요.");
	    		return;
	    	}
	    	
	    	if(isEmpty(lesson_attach_seq)){
	    		alert("잘못된 접근 입니다.");
	    		return;	    		
	    	}
	    	
	    	$.ajax({
                type: "POST",
                url: "${HOME}/ajax/pf/lesson/lessonData/comment/insert",
                data: {
                	"explan" : explan
                	,"lesson_attach_seq" : lesson_attach_seq
                	,"multiImgFlag" : multiImgFlag
                },
                dataType: "json",
                success: function(data, status) {
                	if(data.status == "200"){
	                    alert("코멘트 저장이 완료되었습니다.");	                    
                	}
                },
                error: function(xhr, textStatus) {
                    //alert("오류가 발생했습니다.");
                    document.write(xhr.responseText);
                }
            }); 
	    		
	    }
	</script>
	</head>
	<body>
		<!-- s_tt_wrap -->
		<div class="tt_wrap">
            <h3 class="am_tt">
                <span class="h_date" name="lesson_date"></span>
                <span class="tt" name="lesson_subject"></span>
                <span class="tt_s" name="curr_name"></span>
            </h3>
        </div>
		<!-- e_tt_wrap -->
	
		<!-- s_tab_wrap_cc -->
		<div class="tab_wrap_cc">
			<button class="tab01 tablinks" onclick="pageMoveLessonPlanView(${S_LP_SEQ},${S_CURRICULUM_SEQ});">수업계획서</button>
			<button class="tab01 tablinks active" onclick="pageMoveLessonData('${HOME}');">수업자료</button>
			<button class="tab01 tablinks" onclick="pageLinkCheck('${HOME}', '${HOME}/pf/lesson/formationEvaluation'); return false;">형성평가</button>
			<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/assignMent'">과제</button>
			<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/attendance/view'">출석</button>
		</div>
		<!-- e_tab_wrap_cc -->
	
		<!-- s_tabcontent -->
		<div id="tab001" class="tabcontent tabcontent3">
            <form id="lessonDataForm" onsubmit="return false;" enctype="multipart/form-data">
                <input type="hidden" name="lesson_data_seq" value="${lesson_data_seq }"/>
				<!-- s_stab_wrap  -->
				<table class="stab_wrap">
					<tbody>
						<!-- s_메모 -->
						<tr class="set">
							<td class="td_f">
		
								<div class="mm_wrap chng1">
									<div class="mm_ttx tarea free_textarea">
										<!-- <input type="text" class="tt01" value=""> -->
										<textarea class="tarea01 tt_t" name="memo" style="height: 17;" placeholder="여기에 메모를 입력하세요."></textarea>
									</div>
								</div>
		
							</td>
						</tr>
						<!-- e_메모 -->
					</tbody>
				</table>
				<!-- e_stab_wrap  -->
		
				<table class="tab_table edit">
					<tbody>
						<tr class="edit_wrap">
							<td class="tarea free_textarea">
								<textarea class="tarea01" style="height:400px;min-height:400px;" id="lessonData" name="content" ></textarea>
							</td>
						</tr>
					</tbody>
				</table>
		
				<!-- s_파일 첨부 -->
				<table class="tab_table ttb1">
					<tbody>
						<tr class="tr01">
				            <td rowspan="2" class="th02 w1">
				                <span class="sp01">파일</span>
				            </td>
				            <td class="w6">
				                <input type="text" class="ip_pt1" readonly value="(용량 200MB)">
				            </td>
				            <td class="w1" id="fileInputAdd">
								<button class="btn3" onClick="fileMultiAdd();">파일 선택</button>								
							</td>
				        </tr>				        
						<tr class="tr01">
							<td colspan="2" class="pt">
								<!-- s_set  -->
								<div class="set" id="fileListAdd">
									
								</div> <!-- e_set  --> <!-- s_set  -->						
							</td>
						</tr>
					</tbody>
				</table>
				<!-- e_파일 첨부-->
				
				<!-- s_사진 첨부 -->
				<table class="tab_table ttb1">
					<tbody>
						<tr class="tr01">
				            <td rowspan="2" class="th02 w1">
				                <span class="sp01">사진</span>
				            </td>
				            <td class="w6">
				                <input type="text" class="ip_pt1" readonly value="사진 한장, 여러 장 또는 zip 파일을 등록합니다 (용량 200MB)">
				            </td>
				            <td class="w1" id="imgInputAdd">
								<button class="btn3" onClick="imgMultiAdd();">파일 선택</button>
								
							</td>
				        </tr>				       
						<tr class="tr01">
							<td colspan="2" class="pt" id="imgListAdd">
									
							</td>
						</tr>
					</tbody>
				</table>
				<!-- e_사진 첨부 -->
				
				<!-- s_동영상 첨부 -->
				<table class="tab_table ttb1">
					<tr class="tr01">
						<td rowspan="2" class="th02 w1"><span class="sp01">동영상</span>
							<span class="sp_s">mp4</span></td>
						<td class="w6">
							<div class="tts_box">
								<span class="sp_c1">동영상</span>파일선택<br>(mp4만 등록)
							</div> <input type="text" class="ip_n2" value="동영상파일선택 (mp4만 등록)">
						</td>
						<td class="w1" id="videoInputAdd">
							<button class="btn3" onClick="videoAdd();">파일 선택</button>
						</td>
					</tr>					
					<tr class="tr01">
						<td colspan="2" class="pt" id="videoListAdd">			
	
						</td>
					</tr>
				</table>
				<!-- e_동영상 첨부 -->
	
	
				<!-- s_유튜브 영상 첨부 -->
				<table class="tab_table ttb1">
					<tbody>
						<tr class="tr01">
							<td rowspan="2" class="th02 w1"><span class="sp01">유튜브</span>
								<span class="sp_s">URL</span></td>
							<td class="w6">
								<div class="tts_box">
									<span class="sp_c1">URL</span><br>
								</div> <input type="text" class="ip_n2" id="youtubeUrl" placeholder="유튜브 영상 웹페이지에서 공유버튼을 눌러, 노출되는 URL을 입력해주세요.">
							</td>
							<td class="w1">
								<button class="btn3" onClick="youtubeAdd();">확인</button>
							</td>
				        </tr>
						
						<tr class="tr01">
							<td colspan="2" class="pt" id="youtubeAdd">
							</td>
						</tr>
					</tbody>
				</table>
				<!-- e_유튜브 영상 첨부-->
            </form>
			<div class="bt_wrap">
				<!-- <button class="bt_1">임시 저장</button> -->
				<button class="bt_2" name="saveBtn" onclick="lessonDataSubmit();">등록</button>
				<button class="bt_3" onclick="alert($('#lessonData').val());">취소</button>
			</div>
	
		</div>
	</body>
</html>