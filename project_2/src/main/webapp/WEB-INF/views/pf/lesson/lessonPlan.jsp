<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
	<link rel="stylesheet" href="${CSS}/mpf_style.css" type="text/css">
	<script type="text/javascript">
		$(document).ready(function() {
            getLessonPlanInfo();
            
            $("#yearList span:eq(0)").click();

            bindPopupEvent("#m_pop1", ".open1");
            bindPopupEvent("#m_popn1", ".openn1");
            
			$(document).on("click",".div_del", function(){
				$(this).closest("div").remove();
			});
						
			//강의 비강의 클릭
			$(document).on("change","input[name=lecture_yn]", function(){
				if($(this).val() == "Y"){
					if($("#lessonMethodAdd div").length != 0){
						if(confirm("기존 선택한 비강의를 삭제하고 변경하시겠습니까?")){
							$("#lessonMethodAdd").empty();
						}else{
							$("input[name=lecture_yn][value='Y']").prop("checked", true);
						}
					}
				}
			});
			
			//형성평가 아니 클릭 시
			$(document).on("change","input[name=fe_flag]", function(){
				if($(this).val()=="N"){
					if($("#feCodeAdd div").length != 0){
						$.ajax({
			                type: "POST",
			                url: "${HOME}/ajax/pf/lesson/lessonPlan/feDelChkAll",
			                data: {
			                },
			                dataType: "json",
			                success: function(data, status) {
			                	if(data.status == "200"){
			                		var cnt = 0;
			                		$.each(data.state, function(index){
			                			if(this.test_state != "WAIT")
			                				cnt++;
			                		});
			                		if(cnt != 0){
			                			alert("이미 진행중이거나 종료된 형성평가가 있습니다.");
		    							$("input[name=fe_flag][value='Y']").prop("checked", true);
			                		}else{
			                			if(confirm("기존 선택한 형성평가를 삭제하고 아니오로 변경하시겠습니까?")){
			    							$("#feCodeAdd").empty();
			    						}else{
			    							$("input[name=fe_flag][value='Y']").prop("checked", true);
			    						}
			                		}
			                	}else{
	    							$("input[name=fe_flag][value='Y']").prop("checked", true);
			                		alert("형성평가 상태값 가져오기 실패");
			                	}
			                },
			                error: function(xhr, textStatus) {
			                    //alert("오류가 발생했습니다.");
			                    document.write(xhr.responseText);
			                }
			            });						
					}
				}				
			});
			
			//진단명 추가 선택 체크, 해제 이벤트
            $("#diaCodeListAdd").on("change","input[name='diaChk']",function(){
                var dia_code = $(this).closest("tr").find("input[name=dia_code]").val();
                if($(this).prop("checked")){
                    var dia_name = $(this).closest("tr").find("td[name=dia_name_ko]").text();
                    var html = '<div class="c_wrap" data-name="'+dia_code+'"><div class="tt_c color06">'+dia_name+'</div>'
                    +'<input type="hidden" name="dia_code" value="'+dia_code+'"><button class="btn_c"  onClick="delDia(\''+dia_code+'\', this);" title="삭제하기">X</button></div>';            		
                    $("#selectDiaListAdd").append(html);
                }else{
                    $("#selectDiaListAdd div[data-name='"+dia_code+"']").remove();
                }

                selectDiaCount();
            });
		});
		
		//팝업에서 선택된 교수 삭제 X 클릭
        function delDia(dia_code, obj){
            $(obj).parent("div").remove();
            $("#diaCodeListAdd input[value="+dia_code+"]").closest("tr").find("input[name=diaChk]").prop("checked", false);
            selectDiaCount();
        }
		
		//선택된 진단명 카운트
        function selectDiaCount(){
            var cnt = $("#selectDiaListAdd div.c_wrap").length;
            $("span[data-name=selectDiaCnt]").text(cnt);
        }
		
		function getPreLesson(){

			if(isNaN($('input[name=lessonSelect]:checked').val())){
				alert("불러올 수업계획서를 선택해주세요");
				return false;
			}
			if(!confirm("이전 수업계획서로 대체하시겠습니까?")){
                return false;
            }
			getLessonPlanInfo($('input[name=lessonSelect]:checked').val());
		}
		
		function getLessonPlanInfo(){
			
            $.ajax({
                type: "POST",
                url: "${HOME}/ajax/pf/lesson/lessonPlanBasicInfo",
                data: {                  
                },
                dataType: "json",
                success: function(data, status) {
                	if(data.status=="200"){
                		if(!isEmpty(data.lessonPlanBasicInfo))
                			$("button[name=saveBtn]").text("수정");
                		else
                			$("button[name=saveBtn]").text("등록");
                		
                		var picturePath = "";
	                	var lesson_date = data.lessonPlanBasicInfo.lesson_date.split("/");
	                	var aca_name = data.lessonPlanBasicInfo.year + " " + data.lessonPlanBasicInfo.l_aca_system_name + " " + data.lessonPlanBasicInfo.m_aca_system_name + " " + data.lessonPlanBasicInfo.aca_system_name;
	                	if(isEmpty(data.lessonPlanBasicInfo.picture_path))
	                		picturePath = "${IMG}/ph.png";
                		else
                			picturePath = "${RES_PATH}"+data.lessonPlanBasicInfo.picture_path;
	                	
	                	$("#userImg").attr("src",picturePath);
	                	
	                	$("span[name=lesson_date]").text(data.lessonPlanBasicInfo.lesson_date);
	                	$("span[name=lesson_subject]").text("["+data.lessonPlanBasicInfo.period+"교시] "+data.lessonPlanBasicInfo.lesson_subject);                	
	                	$("span[name=curr_name]").text(" ("+data.lessonPlanBasicInfo.curr_name +" / "+ aca_name+")");
	                	
	                	$("textarea[name=lesson_subject]").val(data.lessonPlanBasicInfo.lesson_subject.split('<br/>').join("\r\n"));
	                	$("textarea[name=learning_outcome]").val(data.lessonPlanBasicInfo.learning_outcome.split('<br/>').join("\r\n"));
	                	$("td[name=lesson_code]").text(data.lessonPlanBasicInfo.lesson_code);
	                	$("span[name=detail_lesson_date]").text(data.lessonPlanBasicInfo.lesson_date + " - " + data.lessonPlanBasicInfo.period+"교시");
	                	$("span[name=lesson_time]").text(data.lessonPlanBasicInfo.start_time + " ~ " + data.lessonPlanBasicInfo.end_time);
	                	$("td[name=period_cnt]").text(data.lessonPlanBasicInfo.period_cnt);
	                	$("span[name=title_aca_name]").text(aca_name);
	                	$("input[name=classroom]").val(data.lessonPlanBasicInfo.classroom);
	                	
	                	if(isEmpty(data.lessonPlanBasicInfo.lesson_subject))
	                		$("span[name=title_lesson_subject]").text(data.lessonPlanBasicInfo.curr_name + " / [수업명 미등록] / " + lesson_date[0]+"월 " + lesson_date[1]+"일" + "("+data.lessonPlanBasicInfo.start_time + "~" + data.lessonPlanBasicInfo.end_time+")");
	                	else
	                		$("span[name=title_lesson_subject]").text(data.lessonPlanBasicInfo.curr_name + " / " + data.lessonPlanBasicInfo.lesson_subject + " / " + lesson_date[0]+"월 " + lesson_date[1]+"일" + "("+data.lessonPlanBasicInfo.start_time + "~" + data.lessonPlanBasicInfo.end_time+")");
	                		                	
	                	$("#domainCode").html('<input type="hidden" value="'+data.lessonPlanBasicInfo.domain_code+'"/>'+data.lessonPlanBasicInfo.domain_name);
	                		                	
	                	$("#levelCode").html('<input type="hidden" value="'+data.lessonPlanBasicInfo.level_code+'"/>'+data.lessonPlanBasicInfo.level_name);
	                		                	
	                	if(!isEmpty(data.lessonPlanBasicInfo.evaluation_method_code))
	                		$("#evaluationMethodCodeList input[value="+data.lessonPlanBasicInfo.evaluation_method_code+"]").closest("span").click();

	                	$("input[name=fe_flag][value="+data.lessonPlanBasicInfo.fe_flag+"]").prop("checked", true);
	                	$("input[name=lecture_yn][value="+data.lessonPlanBasicInfo.lecture_yn+"]").prop("checked", true);
	                	var ranNum = 0;
	                	var htmls='';
	                	 
	                	//핵심 임상표현
	                	$.each(data.lessonCoreClinicList,function(index){
	                        ranNum = Math.floor(Math.random()*7) + 1;
	                        
	                        htmls += '<div class="c_wrap">';
	                        htmls += '<span class="tt_c color0'+ranNum+'">'+this.code_name+'<input type="hidden" name="clinic_code" value="'+this.clinic_code+'"/></span>';
	                        htmls += '<button class="btn_c div_del" title="삭제하기">X</button>';
	                        htmls += '</div>';                   
	                    });
	                    $("#coreClinicListAdd").html(htmls);

	                	htmls = '';
	                	//임상표현
	                	$.each(data.lessonClinicList,function(index){
	                        ranNum = Math.floor(Math.random()*7) + 1;
	                        
	                        htmls += '<div class="c_wrap">';
	                        htmls += '<span class="tt_c color0'+ranNum+'">'+this.code_name+'<input type="hidden" name="clinic_code" value="'+this.clinic_code+'"/></span>';
	                        htmls += '<button class="btn_c div_del" title="삭제하기">X</button>';
	                        htmls += '</div>';                   
	                    });
	                    $("#clinicListAdd").html(htmls);
	                	
	                    htmls = '';
	                	//임상술기
	                	$.each(data.lessonOsceList,function(index){
	                        ranNum = Math.floor(Math.random()*7) + 1;
	                        
	                        htmls += '<div class="c_wrap">';
	                        htmls += '<span class="tt_c color0'+ranNum+'">'+this.osce_name+'<input type="hidden" name="osce_code" value="'+this.skill_code+'"/></span>';
	                        htmls += '<button class="btn_c div_del" title="삭제하기">X</button>';
	                        htmls += '</div>';                   
	                    });
	                    $("#osceCodeListAdd").html(htmls);
	                    
	                	htmls = '';
	                	//진단명
	                    $.each(data.lessonDiagnosisList,function(index){
	                        ranNum = Math.floor(Math.random()*7) + 1;
	                        
	                        htmls += '<div class="c_wrap">';
	                        htmls += '<span class="tt_c color0'+ranNum+'">'+this.code_name+'<input type="hidden" name="dia_code" value="'+this.dia_code+'"/></span>';
	                        htmls += '<button class="btn_c div_del" title="삭제하기">X</button>';
	                        htmls += '</div>';                    
	                    });
	                	$("#diaCodeAdd").html(htmls);
	                    
	                    //졸업역량
	                    htmls='';
	                    $.each(data.lessonFinishCapabilityList,function(index){
	                        ranNum = Math.floor(Math.random()*7) + 1;
	                        
	                        htmls += '<div class="c_wrap">';
	                        htmls += '<span class="tt_c color0'+ranNum+'">'+this.fc_name+'<input type="hidden" name="fc_seq" value="'+this.fc_seq+'"/></span>';
	                        htmls += '<button class="btn_c div_del" title="삭제하기">X</button>';
	                        htmls += '</div>';                   
	                    });
	                    $("#finishCapabilityCodeAdd").html(htmls); 
	                    
	                    //형성평가
	                    htmls='';                    
	                    $.each(data.formationEvaluationList,function(index){
	                    	
	                        ranNum = Math.floor(Math.random()*7) + 1;
	                        
	                        htmls += '<div class="c_wrap">';
	                        htmls += '<span class="tt_c color0'+ranNum+'">'+this.code_name+'<input type="hidden" name="fe_seq" value="'+this.fe_seq+'"/>';
	                        htmls += '<input type="hidden" name="fe_code" value="'+this.fe_cate_code+'"/></span>';
	                        htmls += '<button class="btn_c" onClick="feDel('+this.fe_seq+', this)" title="삭제하기">X</button>';
	                        htmls += '</div>';                   
	                    });
	                    $("#feCodeAdd").html(htmls); 
	                    
	                    //비강의
	                    htmls='';
	                    $.each(data.lessonMethodList, function(index){                    	
	                    	htmls += '<div class="c_wrap">';
	                        htmls += '<span class="tt_c color0'+ranNum+'">'+this.code_name+'<input type="hidden" name="lesson_method_code" value="'+this.lesson_method_code+'"/></span>';
	                        htmls += '<button class="btn_c div_del" title="삭제하기">X</button>';
	                        htmls += '</div>';                          
	                    });
	        			$("#lessonMethodAdd").html(htmls);
                	}else{
                		alert("수업계획서 가져오기 실패");
                	}
                },
                error: function(xhr, textStatus) {
                    //alert("오류가 발생했습니다.");
                    document.write(xhr.responseText);
                }
            }); 
        }
		
		var Color_arry = new Array("color01","color02","color03","color04","color05","color06","color07");
		
		function feDel(fe_seq, obj){
			$.ajax({
                type: "POST",
                url: "${HOME}/ajax/pf/lesson/lessonPlan/feDelChk",
                data: {
                	"fe_seq" : fe_seq
                },
                dataType: "json",
                success: function(data, status) {
                	if(data.status == "200"){
                		if(data.state[0].test_state != "WAIT"){
                			alert("이미 진행중이거나 종료된 형성평가입니다.");
                		}else{
                			$(obj).closest("div.c_wrap").remove();
                		}
                	}else{
                		alert("형성평가 상태값 가져오기 실패");
                	}
                },
                error: function(xhr, textStatus) {
                    //alert("오류가 발생했습니다.");
                    document.write(xhr.responseText);
                }
            });
		}		
		
		function osceAdd(){
			var ranNum = Math.floor(Math.random()*7) + 1;
			
			var code = $("#osce input").val();
			
			if(isEmpty(code)){
				alert("추가할 관련 ${code1.code_name }를 선택하세요");
				return false;
			}				
			
			if($("#osceCodeListAdd input[name=osce_code][value="+code+"]").length != 0){
				alert("이미 추가한 ${code1.code_name }입니다.");
				return false;
			}
			
			var htmls = '<div class="c_wrap">';
            htmls += '<span class="tt_c color0'+ranNum+'">'+$("#osce").text()+'<input type="hidden" name="osce_code" value="'+code+'"/></span>';
            htmls += '<button class="btn_c div_del" title="삭제하기">X</button>';
            htmls += '</div>';
            $("#osceCodeListAdd").append(htmls);
            
		}
		
		function clinicAdd(){
			var ranNum = Math.floor(Math.random()*7) + 1;
			
			var code = $("#clinic input").val();
			
			if(isEmpty(code)){
				alert("추가할 관련 ${code2.code_name }을 선택하세요");
				return false;
			}				
			
			if($("#clinicListAdd input[name=clinic_code][value="+code+"]").length != 0){
				alert("이미 추가한 관련 ${code2.code_name }입니다.");
				return false;
			}
			
			if($("#coreClinicListAdd").find("input[value="+code+"]").length != 0){
				alert("핵심 ${code2.code_name }에 추가된 항목은 관련 ${code2.code_name }에 추가할 수 없습니다.");
				return false;
			}
			
			var htmls = '<div class="c_wrap">';
            htmls += '<span class="tt_c color0'+ranNum+'">'+$("#clinic").text()+'<input type="hidden" name="clinic_code" value="'+code+'"/></span>';
            htmls += '<button class="btn_c div_del" title="삭제하기">X</button>';
            htmls += '</div>';
            $("#clinicListAdd").append(htmls);
            
		}
		
		function coreClinicAdd(){
			var ranNum = Math.floor(Math.random()*7) + 1;
			
			var code = $("#coreClinic input").val();
			
			if(isEmpty(code)){
				alert("추가할 핵심 ${code2.code_name }을 선택하세요");
				return false;
			}				
			
			if($("#coreClinicListAdd input[name=coreClinic_code][value="+code+"]").length != 0){
				alert("이미 추가한 핵심 ${code2.code_name }입니다.");
				return false;
			}
			
			if($("#clinicListAdd input[name=clinic_code][value="+code+"]").length != 0){
				alert("관련 ${code2.code_name }에 추가된 항목입니다.\n관련 ${code2.code_name }에 삭제 후 추가해주세요.");
				return false;
			}
			
			var htmls = '<div class="c_wrap">';
            htmls += '<span class="tt_c color0'+ranNum+'">'+$("#coreClinic").text()+'<input type="hidden" name="coreClinic_code" value="'+code+'"/></span>';
            htmls += '<button class="btn_c div_del" title="삭제하기">X</button>';
            htmls += '</div>';
            $("#coreClinicListAdd").append(htmls);
		}
		
		function diaCodeAdd(){
            var ranNum = Math.floor(Math.random()*7) + 1;
            
			var code = $("#diaCode input").val();
			
			if(isEmpty(code)){
                alert("추가할 ${code3.code_name }을 선택하세요");
                return false;
            }      
			
			if($("#clinicAnddiaCodeAdd input[name=dia_code][value="+code+"]").length != 0){
                alert("이미 추가한 ${code3.code_name }입니다.");
                return false;
            }
			
            var htmls = '<div class="c_wrap">';
            htmls += '<span class="tt_c color0'+ranNum+'">'+$("#diaCode").text()+'<input type="hidden" name="dia_code" value="'+code+'"/></span>';
            htmls += '<button class="btn_c div_del" title="삭제하기">X</button>';
            htmls += '</div>';
            $("#clinicAnddiaCodeAdd").append(htmls);
		}
		
		function feAdd(){
			if($("input[name=fe_flag]:checked").val() == "N"){
				alert("형성평가를 추가하려면 형성평가 여부를 예로 선택해주세요");
				return false;
			}
			
		    var ranNum = Math.floor(Math.random()*7) + 1;
            
            var code = $("#feCode input").val();
            
            if(isEmpty(code)){
                alert("추가할 형성평가를 선택하세요");
                return false;
            }      
            /* 
            if($("#feCodeAdd input[name=fe_code][value="+code+"]").length != 0){
                alert("이미 추가한 형성평가입니다.");
                return false;
            }
             */
            var htmls = '<div class="c_wrap">';
            htmls += '<span class="tt_c color0'+ranNum+'">'+$("#feCode").text()+'<input type="hidden" name="fe_code" value="'+code+'"/>';
            htmls += '<input type="hidden" name="fe_seq" value=""/></span>';            
            htmls += '<button class="btn_c div_del" title="삭제하기">X</button>';
            htmls += '</div>';
            $("#feCodeAdd").append(htmls);			
		}
		
		function finishCapabilityAdd(){
		    var ranNum = Math.floor(Math.random()*7) + 1;
            
            var code = $("#finishCapability input").val();
            
            if(isEmpty(code)){
                alert("추가할 졸업역량을 선택하세요");
                return false;
            }      
            
            if($("#finishCapabilityCodeAdd input[name=fc_seq][value="+code+"]").length != 0){
                alert("이미 추가한 졸업역량입니다.");
                return false;
            }
            
            var htmls = '<div class="c_wrap">';
            htmls += '<span class="tt_c color0'+ranNum+'">'+$("#finishCapability").text()+'<input type="hidden" name="fc_seq" value="'+code+'"/></span>';
            htmls += '<button class="btn_c div_del" title="삭제하기">X</button>';
            htmls += '</div>';
            $("#finishCapabilityCodeAdd").append(htmls);
		}
		
		function lessonMethodAdd(){
			if($("input[name=lecture_yn]:checked").val() == "Y"){
				alert("비강의식을 선택하셔야 추가할 수 있습니다.");
				return false;
			}
				
			var ranNum = Math.floor(Math.random()*7) + 1;
			
			var code = $("#lessonMethod input").val();
	            
			if(isEmpty(code)){
			    alert("추가할 비강의를 선택하세요");
			    return false;
			}      
			
			if($("#lessonMethodAdd input[name=lesson_method_code][value="+code+"]").length != 0){
			    alert("이미 추가한 비강의입니다.");
			    return false;
			}
			
			var htmls = '<div class="c_wrap">';
			htmls += '<span class="tt_c color0'+ranNum+'">'+$("#lessonMethod").text()+'<input type="hidden" name="lesson_method_code" value="'+code+'"/></span>';
			htmls += '<button class="btn_c div_del" title="삭제하기">X</button>';
			htmls += '</div>';
			$("#lessonMethodAdd").append(htmls);
		}
		
		function lessonPlanSubmit(){
			$("#domainCode input").attr("name","domain_code");
			$("#levelCode input").attr("name","level_code");
			$("#evaluationMethodCode input").attr("name","evaluation_method_code");
			$.each($("textarea"),function(){
        		$(this).val($(this).val().replace(/\n/g, "<br/>"));	
        	});
			
			if(!confirm("저장하시겠습니까?")){
				return false;
			}
			
			$("#lessonPlanForm").ajaxForm({
                type: "POST",
                url: "${HOME}/ajax/pf/lesson/lessonPlan/modify",
                dataType: "json",
                success: function(data, status){
                    if (data.status == "200") {
                        alert("저장이 완료되었습니다.");                     
                        getLessonPlanInfo();
                    } else {
                        alert("실패");
                        $.unblockUI();                          
                    }                        
                },
                error: function(xhr, textStatus) {
                    document.write(xhr.responseText); 
                    $.unblockUI();                      
                },beforeSend:function() {
                    $.blockUI();                        
                },complete:function() {
                    $.unblockUI();                      
                }                       
            });     
            $("#lessonPlanForm").submit();				
		}
		
		function preLessonPlan(){
            
            	
			$.ajax({
                type: "POST",
                url: "${HOME}/ajax/pf/lesson/lessonPlan/preLsessonPlanList",
                data: {
                	"year" : $("#yearList input").val(),     
                    "lesson_subject" : $("#lessonPlanSubject").val()
                },
                dataType: "json",
                success: function(data, status) {
                	var htmls = '';
                    $("#preLessonPlanListAdd tr").remove();
                    $.each(data.lessonList, function(){
                        htmls+='<tr>';           
                        htmls+='<td class="bd01 bd02 w1"><input type="radio" onClick="selectLesson(\''+this.lesson_subject+'\');" name="lessonSelect" value="'+this.lp_seq+'" class="radio01"></td>';
                        htmls+='<td class="bd01 bd02 ic_td w3">'+this.curr_name+'</td>';
                        htmls+='<td class="bd01 bd02 w2 name="preLessonSubject">'+this.lesson_subject+'</td>';
                        htmls+='<td class="bd01 bd02 w4_1">'+this.l_aca_system_name+' '+this.m_aca_system_name+' '+this.aca_system_name+'</td></tr>';
                    });
                    $("#preLessonPlanListAdd").append(htmls);
                },
                error: function(xhr, textStatus) {
                    //alert("오류가 발생했습니다.");
                    document.write(xhr.responseText);
                }
            }); 
        }
		
		function getDiaCode(page){
			
			var dia_code = '';
            $("#diaCodeAdd input[name=dia_code]").each(function(index){
                if(index == 0 )
                	dia_code+= "'"+$(this).val()+"'";
                else
                	dia_code+=",'"+$(this).val()+"'"; 
            });
            
			$.ajax({
	            type : "POST",
	            url : "${HOME}/ajax/pf/lesson/lessonPlan/diacode/list",
	            data : {
	                "page" : page
		            ,"dia_name" : $("#dia_name").val()
		            ,"dia_code" : dia_code
	            },
	            dataType : "json",
	            success : function(data, status) {
	            	var htmls = '';
	            	$("#selectDiaListAdd").empty();
	            	$.each(data.list, function(index){
	            		htmls+='<tr class="tr01"><input type="hidden" name="dia_code" value="'+this.dia_code+'">'
						+'<td class="w1">'+this.row_num+'</td>'
						+'<td class="ta_c w2" name="dia_name_ko">'+this.dia_name_ko+'</td>'
						+'<td class="ta_c w3" name="dia_name_en">'+this.dia_name_en+'</td>'
						+'<td class="ta_c w4"><label class="chk01">';
						if($("#selectDiaListAdd").find("input[value="+this.dia_code+"]").length==0)
							htmls+='<input type="checkbox" name="diaChk">';
						else
							htmls+='<input type="checkbox" name="diaChk" checked>';
						
						htmls+=' <span class="slider round">선택</span>'
						+'</label></td>'
						+'</tr>';
	            	});
	            	$("#diaCodeListAdd").html(htmls);
	            	
	            	$("#pagingBtnAdd").html(data.pageNav);
	            },
	            error : function(xhr, textStatus) {
	                //alert("오류가 발생했습니다.");
	                document.write(xhr.responseText);
	            },
	            beforeSend : function() {
	            },
	            complete : function() {
	            }
	        });
		}
				
		function getUnitCode(unitCode){
			
			$.ajax({
	            type : "POST",
	            url : "${HOME}/ajax/mpf/lesson/unit/unitCode",
	            data : {
	                "level" : "2"
	                ,"l_uc_code" : unitCode
	            },
	            dataType : "json",
	            success : function(data, status) {
	            	var level_code_html = '<span class="uoption u2open"><input type="hidden" value="">선택</span>';
	            	$.each(data.unitCode, function(index){
	            		level_code_html+='<span class="uoption u2open"><input type="hidden" value="'+this.uc_code+'">'+this.uc_name+'</span>';
	            	});
	            	$("#levelCodeList").html(level_code_html);
	            	$("#levelCodeList span:eq(0)").click();
	            },
	            error : function(xhr, textStatus) {
	                //alert("오류가 발생했습니다.");
	                document.write(xhr.responseText);
	            },
	            beforeSend : function() {
	            },
	            complete : function() {
	            }
	        });
		}
		
		function selectDiaCodeSubmit(){
			if($("#selectDiaListAdd div.c_wrap").length == 0){
				alert("추가할 진단명을 선택해주세요");				
			}else{
				var html = '';
				$.each($("#selectDiaListAdd div.c_wrap"), function(){
					var dia_code=$(this).find("input[name=dia_code]").val();
					var dia_name=$(this).find("div.tt_c").text();
										
					 html += '<div class="c_wrap"><div class="tt_c color06">'+dia_name+'</div>'
	                    +'<input type="hidden" name="dia_code" value="'+dia_code+'"><button class="btn_c div_del" title="삭제하기">X</button></div>';           		
						
				});	
				$("#diaCodeAdd").append(html);
				$(".closen1").click();
			}
		}
		
		function selectLesson(lesson_subject){			
			$("span[name=selectPreLesson]").text(lesson_subject);
			
		}
	</script>
	</head>
	<body>
		<!-- s_tt_wrap -->
		<div class="tt_wrap">
			<h3 class="am_tt">
				<span class="h_date" name="lesson_date"></span>
                <span class="tt" name="lesson_subject"></span>
                <span class="tt_s" name="curr_name"></span>
			</h3>
		</div>
		<!-- e_tt_wrap -->
	
		<!-- s_tab_wrap_cc -->
		<div class="tab_wrap_cc">
			<button class="tab01 tablinks active" onclick="pageMoveLessonPlanView(${S_LP_SEQ},${S_CURRICULUM_SEQ});">수업계획서</button>
			<button class="tab01 tablinks" onclick="pageMoveLessonData('${HOME}');">수업자료</button>
			<button class="tab01 tablinks" onclick="pageLinkCheck('${HOME}', '${HOME}/pf/lesson/formationEvaluation'); return false;">형성평가</button>
			<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/assignMent'">과제</button>
			<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/attendance/view'">출석</button>
		</div>
		<!-- e_tab_wrap_cc -->
	
		<!-- s_tabcontent -->
		<div id="tab001" class="tabcontent tabcontent2">		
            <form id="lessonPlanForm"  onSubmit="return false;">
				<div class="stab_wrap2">
					<div class="pt_wrap">
						<button class="btn1 open1" onclick="preLessonPlan();">기존 수업 계획서 불러오기</button>
		
						<div class="in_wrap">
							<div class="in_wrap_s">
								<img src="" alt="평가자 사진" id="userImg" class="pt1" style="width:100px;height:120px;">
							</div>
		
							<!-- <button class="btn06">사진 수정</button> -->
						</div>
		
						<div class="title_wrap">
							<textarea class="ip_ta1" rows="2" name="lesson_subject" placeholder="수업주제를 입력하세요"></textarea>
		
							<div class="tts">
								<span class="sp01" name="title_aca_name"></span> 
								<span class="sp01" name="title_lesson_subject"></span>
							</div>
						</div>
		
						<!-- <button class="btn2 open2">확장</button> -->
					</div>
				</div>
	
				<table class="tab_table tarea">
					<tbody>
						<tr class="tr02">
							<td class="tarea free_textarea"><textarea class="tarea01" style="height: 70px;" name="learning_outcome" placeholder="학습성과를 입력하세요."></textarea></td>
						</tr>
					</tbody>
				</table>
				<table class="tab_table ttb1">
					<tbody>
						<tr class="tr02">
							<td class="th01 w1">수업<br>코드 </td>
							<td class="w2" name="lesson_code"></td>
							<td class="th01 w1">일정</td>
							<td class="w5">
								<span class="sp07" name="detail_lesson_date"></span>
								<span class="sp08" name="lesson_time"></span>
							</td>
							<td class="th01 w1">시수</td>
							<td class="w4" name="period_cnt"></td>
							<td class="th01 w1">장소</td>
							<td class="w3">
								<input type="text" class="ip_tt" name="classroom" placeholder="* 강의실 입력" value=""/>								
							</td>
						</tr>
						<tr class="tr02">
							<td class="th01 w1">영역</td>
							<td class="w2">
								<!-- s_wrap2_lms_uselectbox -->
								<div class="wrap2_lms_uselectbox">
									<div class="uselectbox">
										<span class="uselected" id="domainCode"><input type="hidden" value=""/>선택</span><span class="uarrow">▼</span>
		
										<div class="uoptions" id="domainCodeList">
	                                       <span class="uoption" ><input type="hidden" value=""/>선택</span>
										   <c:forEach var="domainCodeList" items="${domainCodeList}">
	                                           <span class="uoption" onclick="getUnitCode('${domainCodeList.uc_code}');"><input type="hidden" value="${domainCodeList.uc_code}"/>${domainCodeList.uc_name}</span>
	                                       </c:forEach>  
										</div>
									</div>
								</div> <!-- e_wrap2_lms_uselectbox -->
							</td>
							<td class="th01 w1">수준</td>
							<td colspan="3" class="w2">
								<!-- s_wrap2_lms_uselectbox -->
								<div class="wrap2_lms_uselectbox">
									<div class="uselectbox">
										<span class="uselected" id="levelCode"><input type="hidden" value=""/>선택</span><span class="uarrow">▼</span>
		
										<div class="uoptions" id="levelCodeList">
	                                       <span class="uoption" ><input type="hidden" value=""/>선택</span>											
										</div>
									</div>
								</div> <!-- e_wrap2_lms_uselectbox -->
							</td>
							<td class="th01 w1">평가<br>방법
							</td>
							<td class="w3">
								<!-- s_wrap2_lms_uselectbox -->
								<div class="wrap2_lms_uselectbox">
									<div class="uselectbox">
										<span class="uselected" id="evaluationMethodCode"><input type="hidden" value=""/>선택</span> <span class="uarrow">▼</span>
		
										<div class="uoptions" id="evaluationMethodCodeList">
											<span class="uoption"><input type="hidden" value=""/>선택</span>
	                                        <c:forEach var="evaluationCode" items="${evaluationCode}">
	                                            <span class="uoption"><input type="hidden" value="${evaluationCode.code}"/>${evaluationCode.code_name}</span>
	                                        </c:forEach> 
										</div>
									</div>
								</div> <!-- e_wrap2_lms_uselectbox -->
							</td>
						</tr>
						<tr class="tr02">
	                        <td class="th01 w1">학습<br>방법 </td>
							<td colspan="7" class="">
								<input type="radio" name="lecture_yn" value="Y" class="ip01" id="Group1_0" checked> 						
								<span class="sp09">강의식</span> 
								<input type="radio" name="lecture_yn" value="N" class="ip01" id="Group1_1"> 
								<span class="sp09">비강의식</span> 
							    <!-- s_wrap3_lms_uselectbox -->
								<div class="wrap3_lms_uselectbox">
									<div class="uselectbox" name="lessonMethodDiv">
										<span class="uselected" id="lessonMethod"><input type="hidden" value=""/>선택</span> <span class="uarrow">▼</span>
		
										<div class="uoptions" id="lessonMethodList">
											<span class="uoption"><input type="hidden" value=""/>선택</span>
											<c:forEach var="lessonMethodCode" items="${lessonMethodCode}">
	                                            <span class="uoption"><input type="hidden" value="${lessonMethodCode.code}"/>${lessonMethodCode.code_name}</span>
	                                        </c:forEach> 
										</div>
									</div>
								</div> <!-- e_wrap3_lms_uselectbox --> <!-- s_wrap3_lms_uselectbox -->								
								<button class="btn3" onClick="lessonMethodAdd();">추가</button></td>
						</tr>
						
						<tr class="tr02">
							<td colspan="8" class="" id="lessonMethodAdd">
							</td>
						</tr>
						
						<tr class="tr02">
							<td class="th01 w1">핵심<br>${code2.code_name }</td>
							<td colspan="7" class="">
								<!-- s_wrap_nl_uselectbox -->
								<div class="wrap_nl_uselectbox">
									<div class="uselectbox">
										<span class="uselected" id="coreClinic"><input type="hidden" value=""/>${code2.code_name } 선택</span> <span class="uarrow">▼</span>
	
										<div class="uoptions" id="coreClinicList">		
										    <span class="uoption" ><input type="hidden" value=""/>${code2.code_name } 선택</span>
	                                        <c:forEach var="clinicCode" items="${clinicCode}">
	                                           <span class="uoption"><input type="hidden" value="${clinicCode.cpx_code}"/>${clinicCode.cpx_name}</span>
	                                      </c:forEach> 								
										</div>
									</div>
								</div> <!-- e_wrap_nl_uselectbox -->								
								<button class="btn3" onclick="coreClinicAdd();">추가</button>
							</td>
						</tr>
						
						<tr class="tr02">
			               <td colspan="8" class="" id="coreClinicListAdd">
							   
			               </td>
				        </tr>
	
						<tr class="tr02">
							<td class="th01 w1">관련<br>${code2.code_name }</td>
							<td colspan="7" class="">
								<!-- s_wrap_nl_uselectbox -->
								<div class="wrap_nl_uselectbox">
									<div class="uselectbox">
										<span class="uselected" id="clinic"><input type="hidden" value=""/>${code2.code_name } 선택</span> <span class="uarrow">▼</span>
	
										<div class="uoptions" id="clinicList">
											<span class="uoption" ><input type="hidden" value=""/>${code2.code_name } 선택</span>
	                                        <c:forEach var="clinicCode" items="${clinicCode}">
	                                           <span class="uoption"><input type="hidden" value="${clinicCode.cpx_code}"/>${clinicCode.cpx_name}</span>
	                                      </c:forEach> 		
										</div>
									</div>
								</div> <!-- e_wrap_nl_uselectbox -->
								<button class="btn3" onClick="clinicAdd();">추가</button>
							</td>
						</tr>

						<tr class="tr02">
			               <td colspan="8" class="" id="clinicListAdd">
			               </td>
				        </tr>	
				        					        	
				        <tr class="tr02">
							<td class="th01 w1">${code1.code_name }</td>
							<td colspan="7" class="">
								<!-- s_wrap_nl_uselectbox -->
								<div class="wrap_nl_uselectbox">
									<div class="uselectbox">
										<span class="uselected" id="osce"><input type="hidden" value=""/>${code1.code_name } 선택</span> <span class="uarrow">▼</span>
	
										<div class="uoptions" id="osceList">
											 <c:forEach var="osceCode" items="${osceCode}">
	                                           <span class="uoption"><input type="hidden" value="${osceCode.osce_code}"/>${osceCode.osce_name}</span>
	                                      </c:forEach> 
										</div>
									</div>
								</div> <!-- e_wrap_nl_uselectbox -->
								<button class="btn3" onClick="osceAdd();">추가</button>
							</td>
						</tr>

						<tr class="tr02">
			               <td colspan="8" class="" id="osceCodeListAdd">							  
			               </td>
				        </tr>	
				        
				        <tr class="tr02">
			               <td class="th01 w1">${code3.code_name }</td>
			               <td colspan="7" class=""><button class="btn4 openn1" onClick="getDiaCode(1);">팝업창에서 ${code3.code_name } 추가하기</button></td>
				        </tr>			 
							 
				        <tr class="tr02">
			               <td colspan="8" class="" id="diaCodeAdd">
			               </td>
				        </tr>
				        
						<tr class="tr02">
							<td class="th01 w1">형성<br>평가<br>여부
							</td>
							<td colspan="7" class="">
								<input type="radio" name="fe_flag" value="Y" class="ip01">
								<span class="sp09">예</span> <!-- s_wrap3_lms_uselectbox -->
								<div class="wrap3_lms_uselectbox">
									<div class="uselectbox">
										<span class="uselected" id="feCode"><input type="hidden" value=""/>선택</span> <span class="uarrow">▼</span>	
										<div class="uoptions" id="feCodeList">
										<span class="uoption" ><input type="hidden" value=""/>선택</span>
	                                        <c:forEach var="formationEvalution" items="${formationEvalution}">
	                                           <span class="uoption"><input type="hidden" value="${formationEvalution.code}"/>${formationEvalution.code_name}</span>
	                                      </c:forEach>
										</div>
									</div>
								</div> <!-- e_wrap3_lms_uselectbox -->
								<button class="btn3 mg_2" onClick="feAdd();">추가</button> 
								<input type="radio" name="fe_flag" value="N" class="ip01" checked>
								<span class="sp09">아니오 (진행안함)</span>
							</td>
						</tr>
						<tr class="tr02">
							<td colspan="8" class="" id="feCodeAdd">
								
							</td>
						</tr>
						<tr class="tr02">
							<td class="th01 w1">졸업<br>역량
							</td>
							<td colspan="7" class="">
								<!-- s_wrap4_lms_uselectbox -->
								<div class="wrap4_lms_uselectbox">
									<div class="uselectbox">
										<span class="uselected" id="finishCapability"><input type="hidden" value=""/>선택</span> <span class="uarrow">▼</span>
		
										<div class="uoptions" id="finishCapabilityList">
											<span class="uoption" ><input type="hidden" value=""/>선택</span>
	                                        <c:forEach var="currFinishCapability" items="${currFinishCapability}">
	                                           <span class="uoption"><input type="hidden" value="${currFinishCapability.fc_seq}"/>${currFinishCapability.fc_name}
	                                           <c:if test='${currFinishCapability.fc_code ne ""  }'> (${currFinishCapability.fc_code})</c:if></span>
	                                           
	                                           
	                                      </c:forEach>
										</div>
									</div>
								</div> <!-- e_wrap4_lms_uselectbox -->
								<button class="btn3" onClick="finishCapabilityAdd();">추가</button>
							</td>
						</tr>
						<tr class="tr02">
							<td colspan="8" class="bd01" id="finishCapabilityCodeAdd">
								
							</td>
						</tr>
					</tbody>
				</table>
            </form>
			<div class="bt_wrap">
				<!-- <button class="bt_1">임시 저장</button> -->
				<button class="bt_2" name="saveBtn" onclick="lessonPlanSubmit();">등록</button>
				<!-- <button class="bt_3">취소</button> -->
			</div>
	
		</div>
		<!-- e_tabcontent -->


	<!-- s_ 팝업 : 수업계획서 불러오기  -->
	<div id="m_pop1" class="pop_up1 mo1">
		<div class="pop_wrap">
			<button class="pop_close close1" type="button">X</button>

			<p class="t_title">수업계획서 불러오기</p>

			<!-- s_pop_table -->
			<div class="pop_table">
				<div class="pop_ipwrap1">
					<!-- s_wrap2_lms_uselectbox -->
					<div class="wrap2_lms_uselectbox">
						<div class="uselectbox" id="year">
							<span class="uselected">2017</span><span class="uarrow">▼</span>

							<div class="uoptions" id="yearList">
								<c:forEach var="yearList" items="${yearList}">
									<span class="uoption"><input type="hidden"
										value="${yearList.year}" />${yearList.year}</span>
								</c:forEach>
							</div>
						</div>
					</div>
					<!-- e_wrap2_lms_uselectbox -->

					<span class="ip_tt">수업주제</span>

					<div class="pop_date">
						<input type="text" class="ip_search" id="lessonPlanSubject"
							placeholder="수업주제를 입력하세요.">
						<button class="btn_search1" onClick="preLessonPlan();">검색</button>
					</div>

				</div>
			</div>
			<!-- e_pop_table -->

			<!-- s_table_b_wrap -->
			<div class="table_b_wrap">

				<!-- s_pop_table -->
				<div class="pop_table">
					<table class="tb_pop">
						<thead>
							<tr>
								<th scope="col" class="w1 style_7">선택</th>
								<th scope="col" class="w3">교육과정명</th>
								<th scope="col" class="w2">수업제목</th>
								<th scope="col" class="w4">학년</th>
							</tr>
						</thead>
					</table>
					<!-- s_pop_twrap1 -->
					<div class="pop_twrap1">
						<table class="tb_pop bd02">
							<tbody id="preLessonPlanListAdd">

							</tbody>
						</table>

					</div>
					<!-- e_pop_twrap1 -->

				</div>
				<!-- e_pop_table -->

				<!-- s_t_dd -->
				<div class="t_dd">
					<div class="c_subject">
						<span class="sp10">선택수업</span> <span class="sp11"
							name="selectPreLesson"></span>
					</div>
					<div class="pop_btn_wrap">
						<button class="btn01" type="button" onclick="getPreLesson();">수업계획서
							불러오기</button>
						<button class="btn02" type="button"
							onClick="$('.close1').click();">취소</button>
					</div>
				</div>
				<!-- e_t_dd -->
			</div>
			<!-- e_table_b_wrap -->
		</div>
	</div>
	<!-- e_ 팝업 : 수업계획서 불러오기  -->


	<!-- s_ 팝업 : 진단명 추가 -->
	<div id="m_popn1" class="pop_up_add_n1 mon1">
		<div class="pop_wrap">
			<button class="pop_close closen1" type="button">X</button>

			<p class="t_title">${code3.code_name } 추가</p>

			<!-- s_pop_ipwrap1 -->
			<div class="pop_ipwrap1">

				<div class="pop_date">
					<input type="text" class="ip_search" id="dia_name" placeholder="${code3.code_name }을 한글 또는 영어로 입력하세요."
						 onkeypress="javascript:if(event.keyCode==13){getDiaCode(1); return false;}">
					<button class="btn_search1" onClick="getDiaCode(1);">검색</button>
				</div>

			</div>
			<!-- e_pop_ipwrap1 -->

			<!-- s_table_b_wrap -->
			<div class="table_b_wrap">
				<!-- s_pop_twrap0 -->
				<div class="pop_twrap0">
					<!-- s_box_tt -->
					<div class="box_tt">
						<span class="th01 w1">NO</span> 
						<span class="th01 w2">${code3.code_name } 국문</span> 
						<span class="th01 w3">${code3.code_name } 영문</span> 
						<span class="th01 w4">선택</span>
					</div>
					<!-- s_box_tt -->
				</div>
				<!-- e_pop_twrap0 -->
				<!-- s_pop_twrap1 -->
				<div class="pop_twrap1">
					<table class="tab_table ttb1">
						<tbody id="diaCodeListAdd">
							
						</tbody>
					</table>

				</div>
				<!-- e_pop_twrap1 -->

				<!-- s_pagination -->
				<div class="pagination" id="pagingBtnAdd">
					
				</div>
				<!-- e_pagination -->

				<!-- s_pht -->
				<div class="pht">
					<div class="wrap2">
						<span class="tt_1">선택된 ${code3.code_name }</span><span class="tt_2" data-name="selectDiaCnt"></span>
					</div>

					<div class="con_wrap">
						<div class="con_s2" id="selectDiaListAdd">
						</div>
					</div>

				</div>
				<!-- e_pop_table -->

			</div>
			<!-- e_pht -->

			<div class="t_dd">

				<div class="pop_btn_wrap2">
					<button type="button" class="btn01"
						onclick="selectDiaCodeSubmit();">등록</button>
					<button type="button" class="btn02" onClick="$('.closen1').click();">취소</button>
				</div>

			</div>
		</div>
	</div>
	<!-- e_ 팝업 : 진단명 추가 -->
</body>
</html>