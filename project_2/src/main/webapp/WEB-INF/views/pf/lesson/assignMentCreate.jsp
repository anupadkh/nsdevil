<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
	<html>
	<head>
    	<script src="${HOME}/resources/smarteditor_v2.9/js/service/HuskyEZCreator.js"></script>       
		<script type="text/javascript">
			var editor_object = [];
						
			$(document).ready(function() {
				nhn.husky.EZCreator.createInIFrame({
                    oAppRef : editor_object,                
                    elPlaceHolder : "assignMent",
                    sSkinURI : "${HOME}/resources/smarteditor_v2.9/SmartEditor2Skin_ko_KR.html",
                    fOnAppLoad:function(){
						$("#tab2").hide();
					}
                });
								
				getAssignMentModifyList();
				
				//수업계획서 타이틀 가져오기
                getLessonTitleInfo();
				
			});	
					
			function getLessonTitleInfo(){
	            
	            $.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/pf/lesson/lessonPlanBasicInfo",
	                data: {                  
	                },
	                dataType: "json",
	                success: function(data, status) {
	                	if(data.status == "200"){
		                    var lesson_date = data.lessonPlanBasicInfo.lesson_date.split("/");
		                    var aca_name = data.lessonPlanBasicInfo.year + " " + data.lessonPlanBasicInfo.l_aca_system_name + " " + data.lessonPlanBasicInfo.m_aca_system_name + " " + data.lessonPlanBasicInfo.aca_system_name;
		                    
		                    $("span[name=lesson_date]").text(data.lessonPlanBasicInfo.lesson_date);
		                    $("span[name=lesson_subject]").text("["+data.lessonPlanBasicInfo.period+"교시] "+data.lessonPlanBasicInfo.lesson_subject);                    
		                    $("span[name=curr_name]").text(" ("+data.lessonPlanBasicInfo.curr_name +" / "+ aca_name+")");                    
	                	}
	                },
	                error: function(xhr, textStatus) {
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                }
	            }); 
	        }
			
			$(document).on("change", "input[name=file]", function(){
				var fileValue = $(this).val().split("\\");
			    var fileName = fileValue[fileValue.length-1]; // 파일명
			    
			    var fileLen = fileName.length; 
			    var lastDot = fileName.lastIndexOf('.'); //마지막 . 위치 가져온다.
			    var fileExt = fileName.substring(lastDot+1, fileLen).toLowerCase(); //마지막 . 위치로 확장자 자름
			    
			    if(fileExt.match(/(exe|sql|reg)$/)){
			    	alert("exe | sql | reg 파일은 등록할 수 없습니다.");
			    	$(this).remove();
			    	return false;
			    }
			    var htmls = '';
	            htmls+='<div class="set">';
	            htmls+='<div class="pt_wrap">';
	            htmls+='<span class="sp02">'+fileName+'</span>';
	            htmls+='<button class="btn_c" onClick="fileDelete(\''+fileIndex+'\', this);" title="삭제하기">X</button>';
	            htmls+='</div>';
	            htmls+='</div>';
			    $("#fileAddList").append(htmls);
			    
			});
			var fileIndex = 0;
	        //파일 인풋 추가해서 해당 인풋 클릭하게 만든다.
	        function fileMultiAdd(){
	            fileIndex++;
	            $("#fileInputAdd").append('<input type="file" name="file" id="file'+fileIndex+'" style="display:none;"/>');
	            $("#fileInputAdd input").last().click();
	        }
	        
	        //파일 삭제한다.
	        function fileDelete(index, obj){
	        	$(obj).closest("div.set").remove();
	        	$("#file"+index).remove();
	        }
	        
	        //날짜 체크한다. 마감날짜 시작날짜보다 작은지
	        function dateChk(){
	        	var start_date = new Date($("input[name=start_date]").val());
	        	var end_date = new Date($('input[name=end_date]').val());
	        	if( start_date > end_date){
	        		$('input[name=end_date]').val("");
	        	}
	        	/* $('input[name=end_date]').datetimepicker({
	                minDate: start_date
	            }); */
	        }
	        
	        //저장수정
	        function assignMentSubmit(){
	        	if(isEmpty($("input[name=start_date]").val())){
	                alert("과제 시작일을 선택해주세요");
	                return false;
	            }
	        	if(isEmpty($("input[name=end_date]").val())){
	                alert("과제 종료일을 선택해주세요");
	                return false;
	            }
	        	if(isEmpty($("input[name=asgmt_name]").val())){
	                alert("과제명을 입력해주세요");
	                return false;
	            }	        		
	        	
	            editor_object.getById["assignMent"].exec("UPDATE_CONTENTS_FIELD", []);
	            
	            $("#assignMentForm").ajaxForm({
	                type: "POST",
	                url: "${HOME}/ajax/pf/lesson/assignMent/insert",
	                dataType: "json",
	                success: function(data, status){
	                    if (data.status == "200") {
	                    	if(isEmpty($("input[name=asgmt_seq]").val())){
	                    		alert("등록이 완료되었습니다.");	
	                    	}else{
	                    		alert("수정이 완료되었습니다.");
	                    	}
	                    	AssignMenttabChange("tab1");         
	                    } else {
	                        alert("실패");
	                        $.unblockUI();                          
	                    }
	                    
	                },
	                error: function(xhr, textStatus) {
	                    document.write(xhr.responseText); 
	                    $.unblockUI();                      
	                },beforeSend:function() {
	                    $.blockUI();                        
	                },complete:function() {
	                    $.unblockUI();                      
	                }                       
	            });     
	            $("#assignMentForm").submit();      
	        	
	        }
	        
	        //등록화면 리셋
	        function assignMentFormReset(){
	        	$("input[name=start_date]").val("");
                $("input[name=end_date]").val("");
                $("#assignMent").val("");     
                //editor_object.getById["assignMent"].exec("UPDATE_CONTENTS_FIELD", []);
                editor_object.getById["assignMent"].exec("SET_IR",['']);
                $("input[name=asgmt_name]").val("");
                $("#fileAddList div").remove();
                $("#fileInputAdd input").remove();
                $("input[name=asgmt_seq]").val("");
	        }
	        
	        //과제 등록 가져오기
	        function getAssignMentData(asgmt_seq){
	        	AssignMenttabChange('tab2');
	            if(isEmpty(asgmt_seq))
	            	return false;
	            
	            $("button[name=saveBtn]").text("수정");
	            
                $("input[name=asgmt_seq]").val(asgmt_seq);
	            
	        	$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/pf/lesson/assignMent/oneList",
	                data: {
	                    "asgmt_seq" : asgmt_seq
	                },
	                dataType: "json",
	                success: function(data, status) {
	                	if(data.status=="200"){
		                	$("input[name=start_date]").val(data.assignMent.start_date);
		                	$("input[name=end_date]").val(data.assignMent.end_date);
                            $("input[name=group_flag][value="+data.assignMent.group_flag+"]").prop("checked", true);
                            $("input[name=asgmt_name]").val(data.assignMent.asgmt_name);
		                	$("#assignMent").val(data.assignMent.content);
		                	editor_object.getById["assignMent"].exec("LOAD_CONTENTS_FIELD");
		                	//editor_object.getById["assignMent"].exec("PASTE_HTML", [data.assignMent.content]);
		                	
		                    var htmls = '';
		                    $("#fileAddList div").remove();
		                    $.each(data.assignMentAttachList, function(){
		                    	htmls+='<div class="set"><input type="hidden" name="fileName" value="'+this.file_name+'"/>';
		                        htmls+='<div class="pt_wrap"><input type="hidden" name="asgmt_attach_seq" value="'+this.asgmt_attach_seq+'"/>';
		                        htmls+='<span class="sp02">'+this.file_name+'</span>';
		                        htmls+='<button class="btn_c" onClick="$(this).closest(\'div.set\').remove();" title="삭제하기">X</button>';
		                        htmls+='</div>';
		                        htmls+='</div>';                        
		                    });
		                    $("#fileAddList").append(htmls);
		                    
	                	}else{
	                		alert("실패");
	                	}
	                	
	                },
	                error: function(xhr, textStatus) {
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                }
	            }); 
	        }
	        
	        //탭 클릭
	        function AssignMenttabChange(name){
	        	if(name=='tab1'){
	        		$("#tab1").show();
	        		$("#tab2").hide();
	        		getAssignMentModifyList();
	        		$("button[name=saveBtn]").text("등록");
	        	}else if(name=='tab2'){
                    $("#tab2").show();
                    $("#tab1").hide();
                    assignMentFormReset();
	        	}
	        }
	        
	        //등록과제 리스트
            function getAssignMentModifyList(){
                $.ajax({
                    type: "POST",
                    url: "${HOME}/ajax/pf/lesson/assignMent/List",
                    data: {
                    	"lp_seq_yn" : "N",
                    	"asgmt_type" : "2"
                    },
                    dataType: "json",
                    success: function(data, status) {
                        if(data.status=="200"){
                            $("#assignMentModifyListAdd tr").remove();
                            var htmls = '';
                            $("span[data-name=assignCnt]").text(data.assignMentList.length);
                            $.each(data.assignMentList, function(index){
                                var group_YN="";
                                
                                if(this.group_flag=="Y") group_YN=="O";
                                else group_YN=="X";
                                
                                htmls='<tr>';
                                htmls+='<td class="td_1">'+(index+1)+'</td>';
                                htmls+='<td class="td_1">'+this.start_date+'</td>';
                                htmls+='<td class="td_1">'+this.end_date+'</td>';
                                htmls+='<td class="td_t open1" onClick="detailLessonPlan(\''+this.asgmt_seq+'\')">'+this.asgmt_name+'</td>';
                                htmls+='<td class="td_1">'+group_YN+'</td>';
                                htmls+='<td class="td_1">'+this.submit_cnt+'</td>';
                                htmls+='<td class="t_1">';
                                
                                //혹시 모르니 본인이 등록한거 맞는지 체크 한다.
                                if(this.feedback_cnt == "0" && this.user_chk=="Y"){
	                                htmls+='<button class="btn_tt1" onclick="getAssignMentData('+this.asgmt_seq+');">수정</button>';
	                                htmls+='<button class="btn_tt2" onclick="deleteAssignMent('+this.asgmt_seq+');">삭제</button>';
                                }else if(this.feedback_cnt != this.st_cnt)
                                	htmls+='평가중';                                		
                                else if(this.feedback_cnt == this.st_cnt)
                                	htmls+='평가완료';
                                
                                htmls+='</td></tr>';     
                                $("#assignMentModifyListAdd").append(htmls);     
                            });
                        }else{
                            alert("실패");
                        }
                        
                    },
                    error: function(xhr, textStatus) {
                        //alert("오류가 발생했습니다.");
                        document.write(xhr.responseText);
                    }
                }); 
            }
	        
	        function detailLessonPlan(asgmt_seq){
	        	$.ajax({
                    type: "POST",
                    url: "${HOME}/ajax/pf/lesson/assignMent/detailView",
                    data: {
                    	"asgmt_seq" : asgmt_seq
                    },
                    dataType: "json",
                    success: function(data, status) {
                        if(data.status=="200"){
                        	var htmls = "", start_day = "", end_day = "";
                            $("#m_pop1").show();
                            
                            switch(data.asgmt.start_day){
	                            case 0 : start_day="일";break;
	                            case 1 : start_day="월";break;
	                            case 2 : start_day="화";break;
	                            case 3 : start_day="수";break;
	                            case 4 : start_day="목";break;
	                            case 5 : start_day="금";break;
	                            case 6 : start_day="토";break;
                            }
                            
                            switch(data.asgmt.end_day){
	                            case 0 : end_day="일";break;
	                            case 1 : end_day="월";break;
	                            case 2 : end_day="화";break;
	                            case 3 : end_day="수";break;
	                            case 4 : end_day="목";break;
	                            case 5 : end_day="금";break;
	                            case 6 : end_day="토";break;
	                        }
                        
                            $("span[name=popupAsgmtName]").text(data.asgmt.asgmt_name);
                            $("span[name=popupAsgmtStartDate]").text(data.asgmt.start_date+"("+start_day+")");
                            $("span[name=popupAsgmtEndDate]").text(data.asgmt.end_date+"("+end_day+")");

                            if(data.asgmt.group_flag=="Y"){
                            	$("span[name=popupAsgmtGroupYN]").removeClass("tt_3_1");
                            	$("span[name=popupAsgmtGroupYN]").addClass("tt_3_2");
                            	$("span[name=popupAsgmtGroupYN]").text("그룹과제");
                            }
                            else{
                            	$("span[name=popupAsgmtGroupYN]").removeClass("tt_3_2");
                                $("span[name=popupAsgmtGroupYN]").addClass("tt_3_1");
                                $("span[name=popupAsgmtGroupYN]").text("개별과제");
                            }
                            $("div[name=popupAsgmtContent]").html(data.asgmt.content); 
                         
                            $.each(data.asgmtAttach, function(){
                            	htmls+='<li class="li_1" style="cursor:pointer;" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');">'+this.file_name+'</li>';
                            });
                            $("ul[name=asgmtAttachList]").html(htmls);
                            
                            
                        }else{
                            alert("실패");
                        }
                        
                    },
                    error: function(xhr, textStatus) {
                        //alert("오류가 발생했습니다.");
                        document.write(xhr.responseText);
                    }
                }); 
	        	
	        }
	        
	        function deleteAssignMent(asgmt_seq){
	        	if(!confirm("삭제하시겠습니까?"))
	        		return;
	        	
	        	$.ajax({
                    type: "POST",
                    url: "${HOME}/ajax/pf/lesson/assignMent/delete",
                    data: {
                    	"asgmt_seq" : asgmt_seq
                    },
                    dataType: "json",
                    success: function(data, status) {
                        if(data.status=="200"){
                        	alert("삭제 완료하였습니다.");
                        	getAssignMentModifyList();
                        }else if(data.status=="001"){
                            alert(data.msg);
                        }else{
                        	alert("삭제 실패하였습니다.");
                        }
                        
                    },
                    error: function(xhr, textStatus) {
                        //alert("오류가 발생했습니다.");
                        document.write(xhr.responseText);
                    }
                }); 
	        }
	        
            function pad(num) {
                num = num + '';
                return num.length < 2 ? '0' + num : num;
            }
		</script>
	</head>
	<body>
		
			<!-- s_tt_wrap -->
			<div class="tt_wrap">
				<h3 class="am_tt">
					<span class="h_date" name="lesson_date"></span>
	                <span class="tt" name="lesson_subject"></span>
	                <span class="tt_s" name="curr_name"></span>
				</h3>
			</div>
			<!-- e_tt_wrap -->
			
			<!-- s_tab_wrap_cc -->
			<div class="tab_wrap_cc">
				<button class="tab01 tablinks" onclick="pageMoveLessonPlanView(${S_LP_SEQ},${S_CURRICULUM_SEQ});">수업계획서</button>
				<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/lessonData'">수업자료</button>
				<button class="tab01 tablinks" onclick="pageLinkCheck('${HOME}', '${HOME}/pf/lesson/formationEvaluation'); return false;">형성평가</button>
				<button class="tab01 tablinks active">과제</button>
				<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/attendance/view'">출석</button>
			</div>
			<!-- e_tab_wrap_cc -->
			
			<!-- s_tabcontent -->
			<div id="tab001" class="tabcontent tabcontent5 rl">
			             
				<div class="stab_title">
					<button class="h_tt2 on" name="tabBtn1">과제 리스트</button>
                    <button onClick="location.href='${HOME}/pf/lesson/assignMent/view'" class="h_tt3" name="tabBtn2">과제제출내역조회</button>
                </div>
			
			<div id="tab1">
				<!-- s_hw_wrap-->
			<div class="hw_wrap_l">           
			         <button onClick="AssignMenttabChange('tab2');" class="h_tt1 on">과제 신규 등록</button>
			<div class="tt_wrap">
			   <span class="sp_tt">등록된 과제</span>
			   <span class="sp_n" data-name="assignCnt"></span>
			            <span class="sp_sign">건</span>
			         </div>
			</div>
			<!-- e_hw_wrap-->
			<div id="tab1">
				<table class="mlms_tb1">
			                 <thead>
			                     <tr>
			                         <th class="th01 bd01 w1">No.</th>
			                         <th class="th01 bd01 w2">과제오픈일</th>
			                         <th class="th01 bd01 w2">과제마감일</th>
			                         <th class="th01 bd01 w3">과제명</th>
			                         <th class="th01 bd01 w4">그룹<br>과제</th>
			                         <th class="th01 bd01 w4">제출<br>현황</th>
			                         <th class="th01 bd01 w5">관리<br>( 과제피드백 등록 후에는<br>수정 / 삭제 불가 )</th>
			                     </tr>
			                 </thead>
			                 <tbody id="assignMentModifyListAdd">  
			                     
				    </tbody>
				</table>
            </div>
			</div>    
			
			   <div id="tab2">
			    <form id="assignMentForm" onsubmit="return false;" enctype="multipart/form-data">
			        <input type="hidden" value="" name="asgmt_seq"/>
			        <input type="hidden" value="2" name="asgmt_type"/>
			        <!-- s_hw_wrap-->
			        <div class="hw_wrap">
			            <span class="tt">과제 오픈 일</span> 
			            <input type="text" class="dateyearpicker-input_1 ip_date" placeholder="과제 오픈 일" name="start_date" onChange="dateChk();"> 
			            <span class="tt">제출 마감 일</span> 
			            <input type="text" class="dateyearpicker-input_1 ip_date" placeholder="제출 마감 일" name="end_date" onChange="dateChk();">
			        </div>
			        <!-- e_hw_wrap-->
			
			        <!-- s_hw_wrap-->
			        <div class="hw_wrap">
			            <span class="tt">과제 명</span> 
			            <input type="text" class="ip_tt" placeholder="과제 명" name="asgmt_name">
			        </div>
			        <!-- e_hw_wrap-->
			
			        <!-- s_hw_wrap-->
			        <div class="hw_wrap">
			            <span class="tt">그룹 과제 여부</span> 
			            <input type="radio" name="group_flag" class="radio01" value="Y" checked> 
			            <span class="tt_s">그룹 과제</span>
			            <input type="radio" name="group_flag" class="radio01" value="N">
			            <span class="tt_s">개별과제</span>
			        </div>
			        <!-- e_hw_wrap-->
			
			        <div class="edit">
			            <table class="tab_table edit">
			                <tbody>
			                    <tr class="edit_wrap">
			                        <td class="tarea free_textarea">
			                            <textarea class="tarea01" style="width:100%;height:400px;min-height:400px;" id="assignMent" name="assignMent"></textarea>
			                        </td>
			                    </tr>
			                </tbody>
			            </table>
			        </div>
			
			        <!-- s_파일 첨부 -->
			        <div class="t_wrap">
			            <table class="tab_table ttb1">
			                <tbody>
			                    <tr class="tr01">
			                        <td rowspan="2" class="th02 w1">
			                        <span class="sp01">참고 파일</span></td>
			                        <td class="w2"><input type="text" class="ip_pt1" readonly value="파일을 등록합니다." onClick="fileMultiAdd();" style="cursor:pointer;"></td>
			                        <td class=" w4" id="fileInputAdd">
			                            <button class="btn3" onClick="fileMultiAdd();">파일 선택</button>
			                        </td>
			                    </tr>
			                    <tr class="tr01">
			                        <td colspan="2" class="w3" id="fileAddList">
			                                                        
			                        </td>
			                    </tr>
			                </tbody>
			            </table>
			        </div>
			        <!-- e_사진 첨부 -->
			    </form>
			    <div class="bt_wrap">
			        <button class="bt_2" name="saveBtn" onclick="assignMentSubmit();">등록</button>
			        <button class="bt_3" onclick="AssignMenttabChange('tab1');">취소</button>
			    </div>
			</div>
			
        </div>
        <!-- e_tabcontent -->    
        
        <!-- s_ 과제 등록 내역 팝업 -->
		<div id="m_pop1" class="pop_up_h pop_up_h_1 mo1">
		    
		     <div class="pop_wrap">
		                <p class="popup_title">과제 등록 내역</p>
		                
		                <button class="pop_close close1" type="button" onClick="$('#m_pop1').hide();">X</button>
		                
		                
		                  
		           <div class="t_title" style="margin-top:10px;">
		              <div class="wrap">
		                   <span class="tt_1">과제 오픈 일</span><span class="tt_2" name="popupAsgmtStartDate"></span>
		                   <span class="tt_1">제출 마감 일</span><span class="tt_2" name="popupAsgmtEndDate"></span>
		                   <span class="tt_3_1" name="popupAsgmtGroupYN" style="width:110px;"></span><!-- s_ 그룹과제 class tt_3_2 -->
		               </div>
		               <div class="wrap">
		                   <span class="tt_1" style="float:left;">과제 명</span>
		                   <span class="tt_2_1" name="popupAsgmtName"></span>                  
		               </div>
		          </div> 
		                    
		          <div class="con_wrap"> 
		                <div class="con_s" name="popupAsgmtContent" style="height:250px;"> 
		                        
		                </div>
		         </div>
		                <ul class="h_list" name="asgmtAttachList">                    
		           </ul>       
		                <!-- s_t_dd --> 
		                <div class="t_dd">
		                   
		                    <div class="pop_btn_wrap">
		                     <button type="button" class="btn01" onclick="$('#m_pop1').hide();">확인</button>                
		                    </div>
		                
		                </div>
		               <!-- e_t_dd -->                  
		                                 
		     </div>                 
		</div>
		<!-- e_ 과제 등록 내역 팝업 -->    
	</body>
</html>