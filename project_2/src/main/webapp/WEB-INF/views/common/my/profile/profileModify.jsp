<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).mouseup(function(e){
				layerPopupCloseInit(['div.uoptions']);
				$( ".boardwrap" ).accordion({
					collapsible: true,
					active: false
				});
			});
			
			function pwdChangeToggle(t) {
				var target = $(t);
				if ($(t).hasClass("open")) { //close
					$(t).removeClass("open");
					$(".table_my > tbody > tr >td").eq(0).attr("rowspan", "4");
					$("input[name='nowPwd']").prop("disabled", true).addClass("dis");
					$("tr[name='pwdChangeTr']").remove();
				} else { //open
					$(t).addClass("open");
					$(".table_my > tbody > tr >td").eq(0).attr("rowspan", "7");
					$("input[name='nowPwd']").prop("disabled", false).removeClass("dis");
					var html = "";
					html += "<tr name=\"pwdChangeTr\">";
					html += "    <td colspan=\"2\" class=\"td01 w04 bd05\">영문 + 숫자 + 기호 포함 최소 9자리--</td>";
					html += "    </tr>";
					html += "<tr name=\"pwdChangeTr\">";
					html += "    <td class=\"td01 w02 bd05\">변경 비밀번호</td>";
					html += "    <td class=\"td01 w03 bd05\"><input type=\"password\" class=\"ip_001\" id=\"newPwd1\" name=\"newPwd1\"></td>";
					html += "</tr>";
					html += "<tr name=\"pwdChangeTr\">";
					html += "    <td class=\"td01 w02 bd05\"><span class=\"stt\">변경 비밀번호</span><span class=\"stt\">확인</span></td>";
					html += "    <td class=\"td01 w03 bd05\"><input type=\"password\" class=\"ip_001\" id=\"newPwd2\" name=\"newPwd2\"></td>";
					html += "</tr>";
					$(".table_my").append(html);
				}
			}
			
			function profileSubmit() {
				var name = $("#name").val().trim();
				if (isBlank(name) || isEmpty(name)) {
					alert("성함을 입력해주세요.");
					$("#name").focus();
					return false;
				}
				
				var tel1 = $("#tel1").attr("value").trim();
				var tel2 = $("#tel2").val().trim();
				var tel3 = $("#tel3").val().trim();
				var tel = tel1 + "-" + tel2 + "-" + tel3;
				if (!isTelType(tel)) {
					alert("전화번호의 형식이 올바르지 않습니다.");
					$("#tel2").focus();
					return false;
				}
				$("#tel").val(tel);
				
				var email = $("#email").val().trim();
				if (isBlank(email) || isEmpty(email)) {
					alert("이메일을 입력해주세요.");
					$("#email").focus();
					return false;
				}
				
				if (!isEmailType(email)) {
					alert("이메일의 형식이 올바르지 않습니다.");
					$("#email").focus();
					return false;
				}
				
				if ($("#pwdChangBtn").hasClass("open")) {
					var nowPwd = $("#nowPwd").val();
					var newPwd1 = $("#newPwd1").val();
					var newPwd2 = $("#newPwd2").val();
					
					if (nowPwd == "") {
						alert("현재 비밀번호를 입력해주세요.");
						$("#nowPwd").focus();
						return false;
					}
					
					if (newPwd1 == "") {
						alert("변경 비밀번호를 입력해주세요.");
						$("#newPwd1").focus();
						return false;
					}
					
					if (!isPwdType(newPwd1)) {
						alert("변경 비밀번호 형식이 올바르지 않습니다.(영문+숫자+기호 포함 최소 9자리 최대 16자리)");
						$("#newPwd1").focus();
						return false;
					}
					
					if (newPwd1 != newPwd2) {
						alert("변경 비밀번호와 변경 비밀번호 확인의 입력 값이 다릅니다.");
						$("#newPwd2").focus();
						return false;
					}
				}

				$("#profileFrm").attr("action","${HOME}/ajax/common/my/profile/modify").ajaxForm({
					beforeSend: function () {
						$.blockUI();
					},
					type: "POST",
					dataType:"json",
					success:function(data){
						if (data.status == "200") {
							alert("프로필 정보가 수정되었습니다.");
							location.href="${HOME}/common/my/profile/detail";
						} else if (data.status == "101") {
							alert("현재 비밀번호가 일치하지 않습니다.");
							$("#nowPwd").focus();
						} else {
							alert("프로필 정보 수정이 실패 하였습니다.");
							location.reload();
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						document.write(xhr.responseText);
				    },
			        complete:function() {
			            $.unblockUI();
			        }
				}).submit();
			}
			
			function previewPicture1() {
				var target = $("#uploadFile");
				target.change(function(){
					var file = $(this).val();
					var previewPicture = $("#previewPicture");
					if (file != "") {
						var attachFileName = file.substring(file.lastIndexOf('\\') + 1);
						var ext = attachFileName.substring(attachFileName.lastIndexOf('.') + 1).toLowerCase();
						var fileMaxSize = 10; //10MB
						var fileSize = this.files[0].size/1024/1024;
						if (/(jpg|png|jpeg)$/i.test(ext) && (fileSize < fileMaxSize)) {
							previewPicture.attr("src", loadPreview(this, "previewPicture"));
						} else {
							alert("10MB이하 이미지 파일(jpg, png, jpeg)만 첨부 가능합니다.");
							previewPicture.attr("src", "${IMG}/profile_default.png");
							if (/msie/.test(navigator.userAgent.toLowerCase())) { // ie
								$("#uploadFile").replaceWith($("#uploadFile").clone(true)); 
							} else { // other browser 
								$("#uploadFile").val(""); 
							}
						}
					} else {
						previewPicture.attr("src", "${IMG}/profile_default.png");
					}
				});
				target.click();
			}
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
			<div class="contents sub">
				<div class="left_mcon my">   
					<div class="sub_menu st_my">
						<div class="title">My</div>   
						
						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt on" onClick="location.href='${HOME}/common/my/profile/detail'">프로필 수정</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>							
							<c:choose>
								<c:when test='${sessionScope.S_USER_LEVEL eq 1 or sessionScope.S_USER_LEVEL eq 2}'>
								</c:when>
								<c:when test='${sessionScope.S_USER_LEVEL eq 3}'>
									<div class="title1 wrapx">
										<span class="sp_tt" onClick="location.href='${HOME}/common/my/lessonData'">나의강의자료</span>
									</div>
									<ul class="panel">
										<li class="wrap xx"></li>
									</ul>	
								</c:when>
								<c:when test='${sessionScope.S_USER_LEVEL eq 4}'>
									<div class="title1"><span class="sp_tt">만족도 조사</span></div>
								    <ul class="panel">
								        <li class="wrap" data-name="lp"><a href="javascript:post_to_url('${HOME }/common/my/sfResearch/list', {'type':'1'});" class="tt_2">수업만족도 조사</a></li>
								        <li class="wrap" data-name="curr"><a href="javascript:post_to_url('${HOME }/common/my/sfResearch/list', {'type':'2'});" class="tt_2">과정만족도 조사</a></li>
								    </ul>
								
									<div class="title1 wrapx"><span class="sp_tt" onClick="location.href='${HOME}/common/my/asgmt/list'">과제 조회</span></div>
								    <ul class="panel">
								        <li class="wrap xx"></li>
								    </ul>
								    
									<div class="title1">
										<span class="sp_tt">성적조회</span>
									</div>
									<ul class="panel">
										<li class="wrap"><a href="${HOME }/common/my/feScore" class="tt_2">형성평가</a></li>
										<li class="wrap"><a href="${HOME }/common/my/soosiGrade" class="tt_2">수시성적</a></li>
										<li class="wrap"><a href="${HOME }/common/my/currGrade" class="tt_2">종합성적</a></li>
									</ul>
								</c:when>
							</c:choose>
							
						</div>
					</div>
				</div>
				<form id="profileFrm" name="profileFrm" onsubmit="return false;" enctype="multipart/form-data">
					<input type="hidden" id="tel" name="tel" value="">
					<input type="file" id="uploadFile" name="uploadFile" value="" class="blind-position">
					<div class="sub_con my">
						<div class="tt_wrap">
							<h3 class="am_tt"><span class="tt">프로필 수정</span></h3>                       
						</div>
						<div class="rfr_con my"> 
							<table class="table_my">
								<tbody>
									<tr>
										<td rowspan="4" class="td01 w01 bd04">
											<div class="ph_wrap">
												<c:choose>
													<c:when test='${picture_path ne null and picture_path ne ""}'>
														<div class="photo1"><img id="previewPicture" src="${RES_PATH}${picture_path}" alt="프로필 사진" ></div>
													</c:when>
													<c:otherwise>
														<div class="photo1"><img id="previewPicture" src="${IMG}/profile_default.png" alt="프로필 사진" ></div>
													</c:otherwise>
												</c:choose>
											</div> 
											<input type="button" class="delete" onclick="previewPicture1();" value="사진수정">
										</td>
										<td class="td01 w02">성함</td>
										<td class="td01 w03">
											<input type="text" class="ip_001" id="name" name="name" value="${name}" maxlength="20">
											<c:choose>
												<c:when test='${sessionScope.S_USER_LEVEL eq 4}'>
													<c:set var="position_name" value="${group_number}" />
												</c:when>
												<c:otherwise>
													<c:set var="position_name" value="${position_name}"/>
												</c:otherwise>
											</c:choose>
											 <c:choose>
								                <c:when test="${group_leader_flag eq 'Y' }">
								                	<span class="tt1 leader" title="직위명">${position_name}</span>
								                </c:when>
								                <c:otherwise>
													<span class="tt1" title="직위명">${position_name}</span>							                
								                </c:otherwise>
							                </c:choose>
										</td>
									</tr>
									<tr>
										<td class="td01 w02">전화번호</td>
										<td class="td01 td_wrap1 w03">   
											<div class="wrap2_2_uselectbox">
												<div class="uselectbox" value="${fn:split(tel,'-')[0]}" id="tel1">
													<span class="uselected">${fn:split(tel,'-')[0]}</span>
													<span class="uarrow">▼</span>
													<div class="uoptions">
														<span class="uoption" value="">선택</span>
														<c:forEach var="telCodeList" items="${telCodeList}">
															<span class="uoption" value="${telCodeList.code}">${telCodeList.code_name}</span>	
														</c:forEach>
													</div>
												</div>
											</div>
											<span class="sign1">-</span>
											<input type="text" class="ip_tel1" maxlength="4" id="tel2" value="${fn:split(tel,'-')[1]}" onkeyup="onlyNumber(this);">
											<span class="sign1">-</span>
											<input type="text" class="ip_tel1" maxlength="4" id="tel3" value="${fn:split(tel,'-')[2]}" onkeyup="onlyNumber(this);">
										</td>
									</tr>
									<tr>
										<td class="td01 w02">이메일</td>
										<td class="td01 w03 td_wrap1">
											<input type="text" class="ip_mail1" id="email" name="email" value="${email}" >
										</td>
									</tr>
									<tr>
										<td class="td01 w02 bd05">현재  비밀번호</td>
										<td class="td01 w03 bd05">
											<input type="password" class="ip_001 dis" id="nowPwd" name="nowPwd" disabled>
											<button class="mdf_pw" id="pwdChangBtn" onclick="pwdChangeToggle(this);">비밀번호 변경</button>
										</td>
									</tr>
								</tbody>
							</table>
							<div class="t_dd">
								<div class="pop_btn_wrap">
									<button type="button" class="btn01" onclick="profileSubmit();">저장</button>
									<button type="button" class="btn02" onclick="location.href='${HOME}/common/my/profile/detail'">취소</button>                    
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
</html>