<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="${CSS}/mpf_style.css" type="text/css">
<script type="text/javascript">
	$(document).mouseup(function(e) {
		layerPopupCloseInit([ 'div.uoptions' ]);
		bindPopupEvent("#m_pop1", ".open1");
		bindPopupEvent("#m_pop2", ".open2");
		bindPopupEvent("#m_pop3", ".open3");
		bindPopupEvent("#m_pop3_100", ".open100");
	});
	
	$(document).ready(function(){
		getMyLessonDataList(1);
		
		$('a.control_prev').click(function () {
	        moveLeft();
	    });

	    $('a.control_next').click(function () {
	        moveRight();
	    });

	    $('a[name=control_prev100]').click(function () {
	        moveLeftMax();
	    });

	    $('a[name=control_next100]').click(function () {
	        moveRightMax();
	    });
	});
	
	function zoomImg(){
		$("#zoomImgPopupListAdd").html($("#imgPopupListAdd").html());
		
		slideCount = $('#slider100 ul li').length;
		slideWidth = $('#slider100 ul li').width();
		slideHeight = $('#slider100 ul li').height();
		sliderUlWidth = slideCount * slideWidth;		
		$('#slider100').css({ width: '100%', height: slideHeight });		
		$('#slider100 ul').css({ width: '100%', marginLeft: - slideWidth });	
	    
	    if($('#slider100 ul li').length == 1){
			$("#m_pop3_100").find(".control_next").hide();
			$("#m_pop3_100").find(".control_prev").hide();
		}else{
			$("#m_pop3_100").find(".control_next").show();
			$("#m_pop3_100").find(".control_prev").show();
		}
	    
		$("#m_pop3").hide();
		$("#m_pop3_100").show();				
	}
	
	function zoomOutImg(){
		$("#imgPopupListAdd").html($("#zoomImgPopupListAdd").html());
		
		slideCount = $('#slider ul li').length;
		slideWidth = $('#slider ul li').width();
		slideHeight = $('#slider ul li').height();
		sliderUlWidth = slideCount * slideWidth;
		$('#slider').css({ width: slideWidth, height: slideHeight });			
		$('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
		
		if($('#slider ul li').length == 1){
			$("#m_pop3").find(".control_next").hide();
			$("#m_pop3").find(".control_prev").hide();
		}else{
			$("#m_pop3").find(".control_next").show();
			$("#m_pop3").find(".control_prev").show();
		}
		$("#m_pop3").show();
		$("#m_pop3_100").hide();
	}
	
	function moveLeft() {
        $('#slider ul').animate({
            left: + slideWidth
        }, 10, function () {
            $('#slider ul li:last-child').prependTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    function moveRight() {
        $('#slider ul').animate({
            left: - slideWidth
        }, 10, function () {
            $('#slider ul li:first-child').appendTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };   
	    
    function moveLeftMax() {
        $('#slider100 ul').animate({
            left: + slideWidth
        }, 10, function () {
            $('#slider100 ul li:last-child').prependTo('#slider100 ul');
            $('#slider100 ul').css('left', '');
        });
    };

    function moveRightMax() {
        $('#slider100 ul').animate({
            left: - slideWidth
        }, 10, function () {
            $('#slider100 ul li:first-child').appendTo('#slider100 ul');
            $('#slider100 ul').css('left', '');
        });
    };
    
	function getMyLessonDataList(page){
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/common/my/lessonData/list",
            data: {
         	   "page" : page
         	   ,"year" : $("#year span.uselected").attr("data-value")
         	   ,"lesson_subject" : $("#lesson_subject").val()
                },
            dataType: "json",
            success: function(data, status) {
            	$("#lessonDataList").empty();
                $("#pagingBtnAdd").html(data.pageNav);
            	var htmls = "";
            	var file_htmls = "";
            	var img_htmls = "";
            	var youtube_htmls = "";
            	
                $.each(data.list, function(index){
                	htmls = "";
                	file_htmls = "";
                	img_htmls = "";
                	youtube_htmls = "";
                	
                	var lesson_attach_seq = this.lesson_attach_seq.split("\|");
                	var file_path = this.file_path.split("\||");
                	var file_name = this.file_name.split("\||");
                	var youtube_url = this.youtube_url.split("\||");
                	var explan = this.explan.split("\||");
                	var attach_type = this.attach_type.split("\|");
                	
                	for(var i=0;i<lesson_attach_seq.length;i++){
                		var fileName = file_name[i]; // 파일명
	                    
	                    if(attach_type[i] == "I"){
	                    	if(file_name[i].split("\|").length > 1){
		                    	img_htmls+='<div class="l_wrap1">'
		                            +'<div class="pt_wrap">'                          
		                            +'<div class="imgs">'
		                            +'<img src="${RES_PATH}'+file_path[i].split("\|")[0]+'" onClick="popUpOpen(\'img\',\''+file_name[i]+'\',\''+file_path[i]+'\',\'${RES_PATH}\',\''+explan[i]+'\');" >'
		                            +'</div>'
		                            +'</div>'                        
		                            +'<div class="tt_wrap">'
		                            +'<span class="sp01">'+explan[i]+'</span>'                  
		                    		+'<span class="sp02">이미지</span>'
		                    		+'<span class="sp03">'+file_name[i].split("\|").length+'장</span>'
		                            +'</div>'
		                            +'<button class="dw" title="다운로드" onClick="multiFileDownload(\'${HOME}\',\''+file_path[i]+'\',\''+file_name[i]+'\');">다운로드</button>'
									+'<button class="fl open3" onClick="popUpOpen(\'img\',\''+file_name[i]+'\',\''+file_path[i]+'\',\'${RES_PATH}\',\''+explan[i]+'\');" title="파일">파일</button>'
		                            +'<input type="checkbox" name="checkgroup1" value="'+lesson_attach_seq[i]+'" class="chk_1">'
		                            +'</div>';
	                    	}else{
	                    		img_htmls+='<div class="l_wrap1">'
		                            +'<div class="pt_wrap">'                          
		                            +'<div class="imgs">'
		                            +'<img src="${RES_PATH}'+file_path[i]+'" onClick="popUpOpen(\'img\',\''+file_name[i]+'\',\''+file_path[i]+'\',\'${RES_PATH}\',\''+explan[i]+'\');" >'
		                            +'</div>'
		                            +'</div>'                        
		                            +'<div class="tt_wrap">'
		                            +'<span class="sp01">'+explan[i]+'</span>'                  
		                    		+'<span class="sp02">이미지</span>'
		                    		+'<span class="sp03"></span>'
		                            +'</div>'
		                            +'<button class="dw" title="다운로드" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+file_path[i]+'\',\''+file_name[i]+'\');">다운로드</button>'
									+'<button class="fl open3" onClick="popUpOpen(\'img\',\''+file_name[i]+'\',\''+file_path[i]+'\',\'${RES_PATH}\',\''+explan[i]+'\');" title="파일">파일</button>'
		                            +'<input type="checkbox" name="checkgroup1" value="'+lesson_attach_seq[i]+'" class="chk_1">'
		                            +'</div>';
	                    	}
	                    }else if(attach_type[i] == "A"){
	                    	img_htmls+='<div class="l_wrap1">'
	                    		+'<div class="pt_wrap"><br><br>'                                     
	                    		+'<audio width="220" height="140" controls>'
	                    		+'<source src="${RES_PATH}'+file_path[i]+'" type="audio/mp3">'
	                    		+'</audio>'	                       
	                    		+'</div>'	                        
	                    		+'<div class="tt_wrap">'
	                    		+'<span class="sp01">'+explan[i]+'</span>'                      
	                    		+'<span class="sp02">오디오</span>'
	                    		+'<span class="sp03">mp3</span>'
	                    		+'</div>'
	                    		+'<button class="dw" title="다운로드" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+file_path[i]+'\',\''+file_name[i]+'\');">다운로드</button>'
								+'<button class="fl open1" onClick="popUpOpen(\'mp3\',\''+file_name[i]+'\',\''+file_path[i]+'\',\'${RES_PATH}\',\''+explan[i]+'\');" title="파일">파일</button>'
								+'<input type="checkbox" name="checkgroup1" value="'+lesson_attach_seq[i]+'" class="chk_1">'
	                            +'</div>';
	                    }else if(attach_type[i] == "V"){
	                    	img_htmls+='<div class="l_wrap1">'
	                    		+'<div class="pt_wrap">'                                     
	                    		+'<video width="220" height="140" controls>'
	                    		+'<source src="${RES_PATH}'+file_path[i]+'" type="video/mp4">'
	                    		+'</video>'	                       
	                    		+'</div>'	                        
	                    		+'<div class="tt_wrap">'
	                    		+'<span class="sp01">'+explan[i]+'</span>'                      
	                    		+'<span class="sp02">동영상</span>'
	                    		+'<span class="sp03">mp4</span>'
	                    		+'</div>'
	                    		+'<button class="dw" title="다운로드" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+file_path[i]+'\',\''+file_name[i]+'\');">다운로드</button>'
								+'<button class="fl open1" onClick="popUpOpen(\'mp4\',\''+file_name[i]+'\',\''+file_path[i]+'\',\'${RES_PATH}\',\''+explan[i]+'\');" title="파일">파일</button>'
								+'<input type="checkbox" name="checkgroup1" value="'+lesson_attach_seq[i]+'" class="chk_1">'
	                            +'</div>';
	                	}else if(attach_type[i] == "Y"){
	                		youtube_htmls+='<div class="l_wrap1">'
	                    		+'<div class="pt_wrap">'
	                    		+'<iframe width="226" height="140" src="'+youtube_url[i]+'" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>'
	                    		//+'<iframe src="'+youtube_url[i]+'" frameborder="0" encrypted-media" allowfullscreen style="width:226px;height140px;"></iframe>'	                       
	                    		+'</div>'	                        
	                    		+'<div class="tt_wrap">'
	                    		+'<span class="sp01">'+explan[i]+'</span>'                      
	                    		+'<span class="sp02">동영상</span>'
	                    		+'<span class="sp03">youtube</span>'
	                    		+'</div>'
								+'<button class="fl open1" onClick="popUpOpen(\'youtube\',\''+youtube_url[i]+'\',\'\',\'${RES_PATH}\',\''+explan[i]+'\');" title="파일">파일</button>'
								+'<input type="checkbox" name="checkgroup1" value="'+lesson_attach_seq[i]+'" class="chk_1">'
	                            +'</div>';	
	                	}else{
	                    	file_htmls+='<li class="li_1">'
	                        	+'<input type="checkbox" name="checkgroup1" value="'+lesson_attach_seq[i]+'" class="chk_1">'
	                        	+'<span>'+file_name[i]+'</span>'
	                        	//+'<button class="fl open2" title="파일">파일</button>'
	                        	+'<button class="dw" title="다운로드" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+file_path[i]+'\',\''+file_name[i]+'\');">다운로드</button>';                  
	                        	+'</li>';
	                    }
                	}
                	
                	htmls = '<div class="myrfr_table">'		     		     		     		     		     		     		     		     		     		    
                		+'<table class="tb_pop"><thead><tr>'
                		+'<th scope="col" class="w1 style_7">'+this.curr_name
                		+'<span class="sign">&gt;</span>'+this.lesson_subject+' (총 '+(lesson_attach_seq.length)+'건)' 
                		+'<span class="sp_r">'+this.aca_system_name+'</span></th></tr></thead></table>'
              			+'<div class="myrfr_twrap1">'
                        +'<ul class="h_list m_tb">'
                        +file_htmls
                        +'</ul>'  
              			+'<table class="tb_pop bd02">'
              			+'<tbody>'
              			+'<tr>'          
              			+'<td class="bd01 bd02">'
                        +img_htmls
                        +youtube_htmls
                        +'</td>'
                        +'</tr>' 
                        +'</tbody>'
                        +'</table>'
                        +'</div>'
                        +'</div>';
                	
                    $("#lessonDataList").append(htmls);
                }); 
            },
            error: function(xhr, textStatus) {
         	   alert("실패");
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            },beforeSend:function() {
            },
            complete:function() {
            }
        }); 
		
	}
	
	function popUpOpen(type, file_name, file_path, res_path, explan){
		
		if(isEmpty(explan)){
			$("div[data-name=dataExplan]").hide();			
		}else{
			$("div[data-name=dataExplan]").show();
			$("div[data-name=dataExplan]").text(explan);
		}
		
		
		if(type=="img"){
			slideCount = $('#slider ul li').length;
			slideWidth = $('#slider ul li').width();
			slideHeight = $('#slider ul li').height();
			sliderUlWidth = slideCount * slideWidth;
			$('#slider').css({ width: slideWidth, height: slideHeight });			
			$('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });			
			$('#slider ul li:last-child').prependTo('#slider ul');
   
			var fileList = file_path.split("|");
			var htmls = "";
			for(var i=0;i<fileList.length;i++){
				htmls+='<li><img src="'+res_path+fileList[i]+'"></li>';
			}
			if(fileList.length == 1){
				$("#m_pop3").find(".control_next").hide();
				$("#m_pop3").find(".control_prev").hide();
			}else{
				$("#m_pop3").find(".control_next").show();
				$("#m_pop3").find(".control_prev").show();
			}
			$("#imgPopupListAdd").html(htmls);
			$("#m_pop3").show();
		}else if(type=="mp4"){
			var htmls = '<video width="894" controls><source src="'+res_path+file_path+'" type="video/mp4" id="videoSrc"></video>'; 
			$("#mediaAdd").html(htmls);
			$("#m_pop1").show();
		}else if(type=="mp3"){
			var htmls = '<audio width="894" controls><source src="'+res_path+file_path+'" type="audio/mpeg" id="videoSrc"></audio>';
			$("#mediaAdd").html(htmls);
			$("#m_pop1").show();
		}else if(type=="youtube"){
			
			var htmls = '<object data="'+file_name+'" width="894" height="500"></object>';
			$("#mediaAdd").html(htmls);
			$("#m_pop1").show();
		}
	}
</script>
</head>
<body>
	<div id="container" class="container_table">
		<div class="contents sub">
			<div class="left_mcon my">
				<div class="sub_menu st_my">
					<div class="title">My</div>
					<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/common/my/profile/detail'">프로필 수정</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>							
							<c:choose>
								<c:when test='${sessionScope.S_USER_LEVEL eq 1 or sessionScope.S_USER_LEVEL eq 2}'>
								</c:when>
								<c:when test='${sessionScope.S_USER_LEVEL eq 3}'>
									<div class="title1 wrapx">
										<span class="sp_tt on" onClick="location.href='${HOME}/common/my/lessonData'">나의강의자료</span>
									</div>
									<ul class="panel">
										<li class="wrap xx"></li>
									</ul>	
								</c:when>
								<c:when test='${sessionScope.S_USER_LEVEL eq 4}'>
									<div class="title1 wrapx">
										<span class="sp_tt">성적조회</span>
									</div>
									<ul class="panel on">
										<li class="wrap"><a href="${HOME }/common/my/feScore" class="tt_2">형성평가</a></li>
										<li class="wrap"><a href="${HOME }/common/my/soosiGrade" class="tt_2">수시성적</a></li>
										<li class="wrap"><a href="${HOME }/common/my/currGrade" class="tt_2">종합성적</a></li>
									</ul>
								</c:when>
							</c:choose>
							
						</div>
				</div>
			</div>

			<!-- s_sub_con -->
			<div class="sub_con my">

				<!-- s_tt_wrap -->
				<div class="tt_wrap">
					<h3 class="am_tt">
						<span class="tt">나의 강의 자료</span>
					</h3>

					<div class="wrap">
						<button class="ic_v4" onClick="alert('작업중입니다.');" title="자료 등록 하기">선택한 자료 학습자료실에 공유</button>
					</div>

				</div>
				<!-- e_tt_wrap -->


				<div class="sch_wrap">
					<span class="tt" style="letter-spacing:0px;">년도</span>
					<div class="wrap2_2_uselectbox" style="width:150px;">
						<div class="uselectbox" id="year">
							<span class="uselected" data-value="">전체</span> <span class="uarrow">▼</span>
							<div class="uoptions" style="display: none;">
								<span class="uoption firstseleted" data-value="">전체</span>
								 <c:forEach var="yearList" items="${yearList}">
                                      <span class="uoption" data-value="${yearList.year}">${yearList.year}</span>
                                 </c:forEach>
							</div>
						</div>
					</div>
					
					<span class="tt" style="letter-spacing:0px;">수업주제</span>
					<input type="text" class="ip_search" id="lesson_subject" placeholder="수업 주제 입력"  onkeypress="javascript:if(event.keyCode==13){getMyLessonDataList(1); return false;}">
					<button class="btn_search1"></button>
				</div>

				<!-- s_rfr_con -->
				<div class="rfr_con my" id="lessonDataList">

				</div>
				<!-- e_rfr_con my-->

				<!-- s_pagination -->
				<div class="pagination" id="pagingBtnAdd">
					<ul>
						<li><a href="#" title="처음" class="arrow bba"></a></li>
						<li><a href="#" title="이전" class="arrow ba"></a></li>
						<li><a href="#" title="다음" class="arrow na"></a></li>
						<li><a href="#" title="맨끝" class="arrow nna"></a></li>
					</ul>
				</div>
				<!-- e_pagination -->
			</div>
			<!-- e_sub_con -->
		</div>
	</div>
	
	<!-- s_ 팝업 : 강의자료 viewer1 -->
	<div id="m_pop1" class="pop_up_view1 mo1">    
		<div class="pop_wrap">
			<button class="pop_close close1" type="button" onClick="$('#m_pop1').hide();">X</button>	  
		
			<p class="t_title">[ 동영상 ]강의자료</p>       
	 		     		     		     		     		     		    
			<!-- s_table_b_wrap -->
			<div class="table_b_wrap">	
				<!-- s_pop_table -->	   
				<div class="pop_table">			     		     		     		     		     		     		     		     		     		    
					<div class="tt" ></div>   
					<!-- s_pop_twrap1 -->
					<div class="pop_twrap">   
					<!-- s_video_wrap -->
					<div class="video_wrap" id="mediaAdd">					                                     
				                  
					</div>
					<!-- e_video_wrap -->
					</div>        
				<!-- e_pop_twrap1 -->
				</div>		
				<!-- e_pop_table -->
			</div>               
		<!-- e_table_b_wrap -->   
		 </div>  
	</div>
	<!-- e_ 팝업 : 강의자료 viewer1 -->
	
	<!-- s_ 팝업 : 강의자료 viewer2 -->
	<div id="m_pop2" class="pop_up_view2 mo2" style="left:0px;">    
	          <div class="pop_wrap">
	 		        <button class="pop_close close2" type="button" onClick="$('#m_pop2').hide();$('#example1').empty();">X</button>	  
	
	                <p class="t_title">[ 파일 ]강의자료</p>       
	 		     		     		     		     		     		    
	 <!-- s_table_b_wrap -->
	<div class="table_b_wrap">	
	
	<!-- s_pop_table -->	   
	<div class="pop_table">			     		     		     		     		     		     		     		     		     		    
	            <div class="tt"></div>   
	<!-- s_pop_twrap1 -->
	<div class="pop_twrap">   
	<!-- s_f_output -->   
	    <div class="f_output" style="height:500px;"> 
	       <div class="file_con" id="example1" style="height:450px;">   
	        </div>
	    </div>
	<!-- e_f_output -->
	</div>        
	<!-- e_pop_twrap1 -->    
	
	 </div>		
	<!-- e_pop_table -->
	</div>               
	<!-- e_table_b_wrap -->   
	 </div>  
	</div>
	<!-- e_ 팝업 : 강의자료 viewer2 -->
	
	<!-- s_ 팝업 : 강의자료 viewer3 -->
	<div id="m_pop3" class="pop_up_view3 mo3">    
	          <div class="pop_wrap">
	 		        <button class="pop_close close3" type="button" onClick="$('#m_pop3').hide();">X</button>	  
	
	                <p class="t_title">[ 이미지 ]강의자료</p>       
	 		     	<button class="open3_100" type="button" title="크게보기" onClick="zoomImg();"></button>	     		     		     		     		    
	 <!-- s_table_b_wrap -->
	<div class="table_b_wrap">	
	
	<!-- s_pop_table -->	   
	<div class="pop_table">			     		     		     		     		     		     		     		     		     		    
	            <div class="tt"></div>   
	<!-- s_pop_twrap1 -->
	<div class="pop_twrap">   
	<!-- s_slider -->   
	    <div id="slider" >
	              <a href="javascript:return false;" class="control_next">&#8250;</a>
	                <a href="javascript:return false;" class="control_prev">&#8249;</a>
	                <ul id="imgPopupListAdd">                  
	               </ul>  
	         </div>
	<!-- e_slider -->
	</div>        
	<!-- e_pop_twrap1 -->    
	
	 </div>		
	<!-- e_pop_table -->
	</div>               
	<!-- e_table_b_wrap -->   
	 </div>  
	</div>
	<!-- e_ 팝업 : 강의자료 viewer3 -->
	
	<!-- s_ 팝업 : 강의자료 viewer3 .pop_up_view3_100 -->
	<div id="m_pop3_100" class="pop_up_view3_100 mo3_100">    
	          <div class="pop_wrap">
	 		        <button class="pop_close close3_100" type="button" title="이전 크기로 되돌아 가기" onClick="zoomOutImg();"></button>	  
	
	                <p class="t_title">[ 이미지 ]강의자료</p>       
	 		     		     		     		     		     		    
	 <!-- s_table_b_wrap -->
	<div class="table_b_wrap">	
	
	<!-- s_pop_table -->	   
	<div class="pop_table">			     		     		     		     		     		     		     		     		     		    
	            <div class="tt"></div>   
	<!-- s_pop_twrap1 -->
	<div class="pop_twrap">   
	<!-- s_slider100 -->   
	    <div id="slider100">
	              <a href="javascript:return false;" class="control_next" name="control_next100">&#8250;</a>
	                <a href="javascript:return false;" class="control_prev" name="control_prev100">&#8249;</a>
	                <ul id="zoomImgPopupListAdd">
	                                      
	               </ul>  
	         </div>
	<!-- e_slider100 -->
	</div>        
	<!-- e_pop_twrap1 -->    
	
	 </div>		
	<!-- e_pop_table -->
	</div>               
	<!-- e_table_b_wrap -->   
	 </div>  
	</div>
	<!-- e_ 팝업 : 강의자료 viewer3 .pop_up_view3_100 -->
</body>
</html>