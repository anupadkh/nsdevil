<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script>
			$(document).ready(function(){
				$( ".boardwrap" ).accordion({
					collapsible: true,
					active: false
				});
				
				if($.inArray("${aca_state}", ["03","04","05","06","07"]) > -1){
					getFeScore();	
				}else{
        			$("#scoreDiv").removeClass("class_x").addClass("class_0");
				}
			});
			
			function getFeScore(){
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/common/my/feScore/list",
		            data: {    
		            	"aca_seq" : "${aca_seq}"
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		var htmls="";
		            		
		            		$("#feListAdd").empty();
		            		
		            		if(data.feList.length == 0){
		            			$("#scoreDiv").removeClass("class_0").addClass("class_x");
		            		}
		            		
		            		$.each(data.feList , function(index){
		            			var state = "";
		            			var score = "";
		            			var avg_score = "";
		            			
		            			if(this.test_state == "WAIT" || this.test_state == "START"){
		            				score = "대기";
		            				avg_score = "대기";
		            			}else{
		            				score = this.fe_score;
		            				avg_score = this.avg_score;		            				
		            			}		            				
		            			
		            			htmls='<tr class="">'
									+'<td>'+this.lesson_date+'</td>'
									+'<td class="t_l">'+this.curr_name+'</td>'
									+'<td class="t_l">'+this.lesson_subject+'</td>'
									+'<td class="">'+this.name+'</td>'
									+'<td>'+score+'</td>'
									+'<td><span class="num1">'+avg_score+'</span><span class="sign">/</span>'
									+'<span class="num2">'+this.quiz_cnt+'</span></td>'
									+'</tr>';
								
								$("#feListAdd").append(htmls);
		            		});
		            	}else
		            		alert("실패");
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        });					
			}
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
			<div class="contents sub">
				<div class="left_mcon my">   
					<div class="sub_menu st_my">
						<div class="title">My</div>

						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/common/my/profile/detail'">프로필 수정</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>							
							<c:choose>
								<c:when test='${sessionScope.S_USER_LEVEL eq 1 or sessionScope.S_USER_LEVEL eq 2}'>
								</c:when>
								<c:when test='${sessionScope.S_USER_LEVEL eq 3}'>
									<div class="title1 wrapx">
										<span class="sp_tt" onClick="location.href='${HOME}/common/my/lessonData'">나의강의자료</span>
									</div>
									<ul class="panel">
										<li class="wrap xx"></li>
									</ul>	
								</c:when>
								<c:when test='${sessionScope.S_USER_LEVEL eq 4}'>
									<div class="title1"><span class="sp_tt">만족도 조사</span></div>
								    <ul class="panel">
								        <li class="wrap" data-name="lp"><a href="javascript:post_to_url('${HOME }/common/my/sfResearch/list', {'type':'1'});" class="tt_2">수업만족도 조사</a></li>
								        <li class="wrap" data-name="curr"><a href="javascript:post_to_url('${HOME }/common/my/sfResearch/list', {'type':'2'});" class="tt_2">과정만족도 조사</a></li>
								    </ul>
								
									<div class="title1 wrapx"><span class="sp_tt" onClick="location.href='${HOME}/common/my/asgmt/list'">과제 조회</span></div>
								    <ul class="panel">
								        <li class="wrap xx"></li>
								    </ul>
									<div class="title1 wrapx">
										<span class="sp_tt on">성적조회</span>
									</div>
									<ul class="panel on">
										<li class="wrap"><a href="${HOME }/common/my/feScore" class="tt_2 on">형성평가</a></li>
										<li class="wrap"><a href="${HOME }/common/my/soosiGrade" class="tt_2">수시성적</a></li>
										<li class="wrap"><a href="${HOME }/common/my/currGrade" class="tt_2">종합성적</a></li>
									</ul>
								</c:when>
							</c:choose>
							
						</div>
					</div>
				</div>

			<!-- s_sub_con -->
			<div class="sub_con rcard">

				<!-- s_tt_wrap -->
				<div class="tt_wrap grdcard">
					<h3 class="am_tt">
						<span class="tt_s">형성평가</span>
						<span class="tt">${aca_name }</span>
					</h3>
					<div class="w_r">
						<!-- <button class="btn_pdf" title="PDF다운로드"></button> -->
						<button class="btn_prt" title="인쇄하기"></button>
					</div>
				</div>
				<!-- e_tt_wrap -->

				<!-- s_rfr_con -->
				<!-- s_등록된 성적이 없을 때 class : class_x, 조회기간이 아닐 때 class : class_0 추가 -->
				<div class="rfr_con grd3" id="scoreDiv">
					<table class="mlms_tb card">
						<thead>
							<tr>
								<th class="th01 bd01 w4">날짜</th>
								<th class="th01 bd01 w5">교육과정명</th>
								<th class="th01 bd01 w6">수업명</th>
								<th class="th01 bd01 w3">교수명</th>
								<th class="th01 bd01 w1">내점수</th>
								<th class="th01 bd01 w2">평균</th>
							</tr>
						</thead>
						<tbody id="feListAdd">
							
						</tbody>
					</table>

				</div>
				<!-- e_rfr_con -->

			</div>
			<!-- e_sub_con -->

		</div>
		</div>
	</body>
</html>