<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <script src="${JS}/lib/jquery.ui.accordion.js"></script>
        <script type="text/javascript">
        	var ajaxTask;
            $(document).ready(function(){
                autosize($("textarea"));
                
                $("#answerUserSelectBox").hide();
				$("#answerUserSelectBox span.uselected").empty();
				$("input.qna_ip_tt").show();
            });
        
        
            function questionSubmit() {
                var title = $("#title").val();
                var content = $("#content").val();
                
                if (isEmpty(title) || isBlank(title)) {
                	alert("제목을 입력해 주세요.");
                	$("#title").focus();
                	return false;
                }
                
                if (isEmpty(content) || isBlank(content)) {
                	alert("질의 내용을 입력해 주세요.");
                	$("#content").focus();
                	return false;
                }
                
                $.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/common/SLife/qna/question/create",
			        data: $("#questionFrm").serialize(),
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
							alert("등록 되었습니다.");
							location.href = "${HOME}/common/SLife/qna/list";
						} else {
							alert("등록에 실패 하였습니다.");
							return false;
						}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
            }
            
            
            
			function searchOpen() {
				$("#answerUserSelectBox").hide();
				$("#answerUserSelectBox span.uselected").empty();
				$(".schwrap_uselectbox input.qna_ip_tt").show();
				
				$(":input[name='user_seq']").val("");
				$(":input[name='user_name_search']").val("");
				$(":input[name='user_name']").val("");
				$(":input[name='user_name']").focus();
			} 
            
			
			function selectAnswerUser(user_seq, element) {
				var uoption = $("span.a_mp", element);
				$("#answerUserSelectBox span.uselected").append(uoption);
				$("#answerUserSelectBox").show();
				$(".schwrap_uselectbox input.qna_ip_tt").hide();
				var user_name = $("span.userName", uoption).html();
				$(":input[name='user_seq']").val(user_seq);
				$(":input[name='user_name']").val(user_name);
				$(":input[name='user_name_search']").val(user_name);
				$("#answerUserSelectBox .uoptions").empty().hide();
			}
			
			
			function searchAnswerUser(event) {
				var user_name = $.trim($(":input[name='user_name']").val());
				
				//검색 키워드가 이전과 동일하면 검색 안함
				if (user_name == $(":input[name='user_name_search']").val()) {
					return;
				}
				
				//이전에 실행중인 ajax 작업이 있으면 검색 안함
				if(0 < $.active) {
					return;
		        }
				
				var uoptions = $("#answerUserSelectBox .uoptions");
				
				//검색값이 없으면 요청 취소하고 셀렉트박스 삭제
				if (user_name.length == 0) {
					uoptions.empty().hide();
					return;
				}
				
				clearTimeout(ajaxTask);
				
				ajaxTask = setTimeout(function() {
					$(":input[name='user_name_search']").val(user_name);
					$.ajax({
			            type: "POST",
			            url: "${HOME}/ajax/pf/lesson/getPfList",
			            data: {
			            	"user_name": $(":input[name='user_name_search']").val()
			            },
			            dataType: "json",
			            success: function(data, status) {
			            	uoptions.empty();
			            	if (0 < data.pfList.length) {
			            		uoptions.show();
			            	}
			            	
			            	$.each(data.pfList, function() {
			            		var userPic = "${DEFAULT_PICTURE_IMG}";
			                	if (this.picture_path != null) {
			                		userPic = this.picture_path;
			                	}
			            		var html = '';
			            		html += '<a onclick="selectAnswerUser(\'' + this.user_seq + '\', this);">';
			            		html += '	<span class="uoption">';
								html += '		<span class="a_mp">';
								html += '			<span class="pt01"><img src="' + userPic + '" alt="등록된 사진 이미지" class="pt_img"></span>';
								html += '			<span class="ssp1"><span class="userName">' + this.name + '</span> (' + this.department + ')</span>';
								html += '		</span>';
								html += '	</span>';
								html += '</a>';
			                    uoptions.append(html);
			            	});
			            	$("#answerUserSelectBox").show();
			            },
				        error: function(xhr, textStatus) {
				            document.write(xhr.responseText);
				        },
				        beforeSend:function() {
				            $.blockUI();
				        },
				        complete:function() {
				            $.unblockUI();
				        }
			        });
				}, 100);
			}
            
        </script>
    </head>
    <body>
        <div id="container" class="container_table">
            <div class="contents sub">
                <div class="left_mcon">
                    <div class="sub_menu slife ">
                        <div class="title">학교생활</div>
                        <ul class="sub_panel" id="sLifeMenuArea"></ul>
                    </div>
                </div>
                <div class="sub_con slife st">
                    <div class="tt_wrap">
                        <h3 class="am_tt"><span class="tt" id="boardTitleValue"> 글 등록</span></h3>
                        <div class="wrap">
                            <button class="ic_v3" onclick="javascript:location.href='${HOME}/common/SLife/qna/list';" title="목록 보기">목록</button>
                        </div>
                    </div>
                    <form id="questionFrm" name="questionFrm" onsubmit="return false;" autocomplete="off">
                    	<input type="hidden" id="answerUserSeq" name="answerUserSeq">
                        <div class="rfr_con">   
                            <div class="qna_wrap">
                            	<div class="rwrap">
								    <span class="tt2">제목</span>
								    <input type="text" class="ip_tt" id="title" name="title" placeholder="제목을 입력하세요.">
							    </div>
                            	 <div class="rwrap">
									<span class="tt1">교수님 지정</span>
								    <div class="schwrap_uselectbox">
										<input type="hidden" name="user_seq">
		                        		<input type="hidden" name="user_name_search">
										<input type="text" class="qna_ip_tt" name="user_name" placeholder="교수검색 (선택입력)" onkeyup="searchAnswerUser();">
								    	<div id="answerUserSelectBox" class="uselectbox">
											<span class="uselected"></span>
											<span class="uarrow" onclick="searchOpen();">검색</span>
											<div class="uoptions" style="display: none;">
												<span class="uoption schopt1 firstseleted">교수검색 (선택입력)</span>
												<span class="uoption">
												    <span class="a_mp"><span class="pt01"><img src="../img/ph_r1.png" alt="등록된 사진 이미지" class="pt_img"></span><span class="ssp1">가교수 (진단검사의학)</span></span>
												</span>
											</div>
										</div>  								
									</div>
									<span class="tt4">교수님을  선택하지 않으면 행정직원만 볼 수 있습니다.</span>
								</div>
                                <div class="rwrap tarea free_textarea">
                                    <span class="tt3">질의 내용을 작성해 주세요.</span>
                                    <textarea class="tarea01" id="content" name="content"></textarea> 
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="btn_wrap_r">
                        <button class="bt_2" onclick="questionSubmit();">등록</button>
                        <button class="bt_3" onclick="location.href='${HOME}/common/SLife/qna/list'">취소</button>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>