<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script src="${JS}/lib/jquery.ui.accordion.js"></script>
		<script type="text/javascript">
			function faqListView(page) {
				
				if (typeof page == "undefined") {
					page = 1;
				}
				
				$("input[name='page']").val(page);
				
				var searchText = $("#searchText").val();
				if (!isEmpty(searchText)) {
					$("#searchType").val($("#searchSelectBox").attr("value"));
				}
				
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/common/SLife/faq/list",
			        data: $("#searchFrm").serialize(),
			        dataType: "json",
			        success: function(data, status) {
			            $("#faqList").empty();
			            var html = "";
		        		$.each(data.list, function() {
		        			html += "<div class=\"title\"><span class=\"sp_tt\">" + this.title + "</span></div>";
		        			html += "<ul class=\"panel\">";
		        			html += "	<li class=\"wrap\">" + this.content + "</li>";
		        			html += "</ul>";
		        		});
		        		if(data.list.length > 0) {
				            $("#faqList").html(html);
				            $(".pagination").html(data.pageNav);
		        		} else {
// 		        			alert("//TODO 게시글 없을때");
		        			//TODO 등록된 게시글 없을때 표기
// 		        			html = "<ul><li class=\"faq-list-content-li\"><div class=\"question\">등록된 게시글이 없습니다.</div></li></ul>";
		        		}
	        			
			            accordionInit();
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			$(document).ready(function(){
			    faqListView(1);
			});

			function accordionInit() {
				$("#faqList").accordion({collapsible: true, active: false}).accordion("refresh");
			}
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
			<div class="contents sub">
				<div class="left_mcon">   
					<div class="sub_menu slife">
					    <div class="title">학교생활</div>   
					    <ul class="sub_panel" id="sLifeMenuArea"></ul>
					</div>
				</div>
				<div class="sub_con slife">
				    <form id="searchFrm" name="searchFrm" method="get" onsubmit="return false;">
				    	<input type="hidden" id="page" name="page">
				    	<input type="hidden" id="searchType" name="searchType">
						<div class="tt_wrap">
						    <h3 class="am_tt"><span class="tt" id="boardTitleValue"></span></h3>
						    <div class="wrap">
								<div class="wrap2_2_uselectbox">
									<div class="uselectbox" id="searchSelectBox" value="">
										<span class="uselected">전체</span>
										<span class="uarrow">▼</span>			
										<div class="uoptions">
								        	<span class="uoption" value="">전체</span>
								        	<span class="uoption" value="00">제목</span>
								        	<span class="uoption" value="01">내용</span>
										</div>
									</div>  								
								</div>
						    	<input type="text" class="ip_search" id="searchText" name="searchText" value="" onkeypress="javascript:if(event.keyCode==13){faqListView(1); return false;}">
						    	<button type="button" class="btn_search1" onclick="faqListView(1);"></button>   
						    </div>
						</div>
				    </form>
					<div class="rfr_con">   
						<div class="faq_wrap">
							<div class="boardwrap" id="faqList"></div>
						</div>
					</div> 
					<div class="pagination"></div>
				</div>
			</div>
		</div>
	</body>
</html>