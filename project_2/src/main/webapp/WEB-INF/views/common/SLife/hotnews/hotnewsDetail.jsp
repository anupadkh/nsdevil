<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function(){
				getBoardDetail();
			});
			
			function getBoardDetail() {
				$.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/common/SLife/hotnews/detail",
			        data: {
			        	"board_seq":"${seq}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var boardInfo = data.board_info;
			        		var attachList = data.attach_list;
			        		var nextBoardInfo = data.next_board_info;
			        		var prevBoardInfo = data.prev_board_info;
			        		var userPicturePath = boardInfo.user_picture_path;
			        		var regUserDepart = boardInfo.user_department_name;
			        		if (userPicturePath != "") {
			        			$("#regUserPicture").attr("src", "${RES_PATH}"+userPicturePath);
			        		}
			        		if (regUserDepart != "") {
				        		$("#regUserDepart").html("("+boardInfo.user_department_name+")");
			        		}
			        		$("#regUserName").html(boardInfo.name);
			        		
			        		$("#title").html(boardInfo.title);
			        		$("#regDate").html(boardInfo.reg_date);
			        		$("#content").html(boardInfo.content);
			        		
			        		if (attachList.length > 0) {
			        			var attachHtml = '<ul class="h_list m_tb">';
				        		$(attachList).each(function() {
				        			attachHtml += '<li class="li_1"><span>' + this.file_name + '</span><button class="dw" title="다운로드" onclick="fileDownload(\'${HOME}\', \'${RES_PATH}'+this.file_path+'\', \'\', \''+this.file_name+'\')">다운로드</button></li>';
				        		});
				        		attachHtml += '</ul>';
				        		$("#attachListArea").html(attachHtml);
			        		} else {
			        			$("#attachListArea").html("");
			        		}
			        		
			        		if (nextBoardInfo != null) {
			        			$("#nextBtn").attr("onclick", "javascript:location.href='${HOME}/common/learning/learning/detail?seq=" + nextBoardInfo.next_board_seq + "'");
			        		}
			        		
			        		if (prevBoardInfo != null) {
			        			$("#prevBtn").attr("onclick", "javascript:location.href='${HOME}/common/learning/learning/detail?seq=" + prevBoardInfo.prev_board_seq + "'");
			        		}
			        	} else {
			        		alert("불러오기에 실패하였습니다. [" + data.status + "]");
			        		location.href="${HOME}/common/SLife/hotnews/list";
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
			<div class="contents sub">
			<div class="left_mcon">   
				<div class="sub_menu slife">
				    <div class="title">학교생활</div>   
				    <ul class="sub_panel" id="sLifeMenuArea"></ul>
				</div>
			</div>
			<div class="sub_con slife">
				<div class="tt_wrap">
				    <h3 class="am_tt"><span class="tt" id="boardTitleValue"></span></h3>
				    <div class="wrap">
				        <button class="ic_v3" onClick="location.href='${HOME}/common/SLife/hotnews/list'" title="목록 보기">목록</button>
				    </div>
				</div>
				<div class="tt_wrap2">                        
				    <span class="tt_con" id="title"> </span>  
				    <span class="tt_date" id="regDate"></span>    
				</div>	
				<div class="rfr_con">
					<div id="attachListArea"></div>
					<div class="h_list_custom mp_2">
						<div class="wrap">
							<span class="a_mp"><span class="pt01"><img id="regUserPicture" src="${DEFAULT_PICTURE_IMG}" alt="사진" class="pt_img"></span><span class="ssp1"><span id="regUserName"></span><span class="ssp2" id="regUserDepart"></span></span></span>
						</div>
					</div>
					<div class="con_wrap">
						<div class="con_s con_s_h" id="content"></div>
					</div>
				</div> 
				<div class="btn_wrap">
				    <button class="btn1" id="nextBtn" onclick="javascript:alert('다음 글이 없습니다.'); return false;"><span><span class="ic">▲</span>다음 글</span></button>
				    <button class="btn2" id="prevBtn" onclick="javascript:alert('이전 글이 없습니다.'); return false;"><span><span class="ic">▼</span>이전 글</span></button>
				</div>
			</div>
			
			</div>
		</div>
	</body>
</html>