<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script src="${JS}/lib/datetimep_1.js"></script>
		<script src="${JS}/lib/monthdatepicker_ui.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				
				noticeListView(1);
				
				$.datetimepicker.setLocale('kr');
			    $('#startDate').datetimepicker({
			        timepicker:false,
			        datepicker:true,
			        format:'y-m-d',
			        formatDate:'y-m-d'
			    });
			    
			    $('#endDate').datetimepicker({
			        timepicker:false,
			        datepicker:true,
			        format:'y-m-d',
			        formatDate:'y-m-d'
			    });
			    
			    $("#startDate").on("change", function(e){
			    	var startDate = $("#startDate").val();
			    	if (startDate != "") {
				    	$('#endDate').datetimepicker({minDate: startDate});
			    	}
			    });
			    
			    $("#endDate").on("change", function(e){
			    	var endDate = $("#endDate").val();
			    	if (endDate != "") {
				    	$('#startDate').datetimepicker({maxDate: endDate});
			    	}
			    });
			});
			
			function noticeListView(page) {
				if (typeof page == "undefined") {
					page = 1;
				}
				$("input[name='page']").val(page);
				
				var startDate = $("#startDate").val();
				var endDate = $("#endDate").val();
				
				if (!isEmpty(startDate)) {
					if (!isValidDate(startDate)) {
						alert("시작일의 날짜형식이 올바르지않습니다.(YYYY-MM-DD)")
						return false;
					}
				}
				
				if (!isEmpty(endDate)) {
					if (!isValidDate(endDate)) {
						alert("종료일의 날짜형식이 올바르지않습니다.(YYYY-MM-DD)")
						return false;
					}
				}
				
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/common/SLife/notice/list",
			        data: $("#searchFrm").serialize(),
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var list = data.list;
				        	var listHtml = '';
				        	var totalCnt = data.totalCnt;
				        	$(list).each(function(){
				        		var boardSeq = this.board_seq;
				        		var rowNum = this.row_num;
				        		var hits = this.hits;
				        		var title = this.title;
				        		var content = this.content;
				        		content = content.replace(/(<([^>]+)>)/ig,"");
								content = content.replace(/\n/g, "");//행 바꿈 제거
				            	content = content.replace(/\s+/, "");//왼쪽 공백 제거
				            	content = content.replace(/\r/g, "");//엔터 제거
				        		var fileCnt = this.file_cnt;
				        		var regDate = this.reg_date;
				        		var cateCode = this.board_cate_code;
				        		var showTarget = this.show_target;
				        		var targetList = this.target_list;
				        		
				        		var noText = (totalCnt+1) - rowNum;
				        		var noClass = "s1_2";
				        		if (cateCode == "97") {
				        			noText = "기타";
				        			noClass = "s1_1_notice";
				        		} else if(cateCode == "98") {
				        			noText = "시스템 점검";
				        			noClass = "s1_1_notice";
				        		} else if(cateCode == "99") {
				        			noText = "긴급 공지";
				        			noClass = "s1_1_notice";
				        		}
				        		listHtml += '<tr>';
				        		listHtml += '    <td><span class="' + noClass + '">' + noText + '</span></td>';
				        		listHtml += '    <td class="t_l lnk" onclick="javascript:noticeDetail('+boardSeq+')" title="상세보기">';
				        		listHtml += '    	<span class="sp1">' + title + ' </span>';
				        		listHtml += '		<span class="sp2">' + content + ' </span>';
				        		listHtml += '	</td>';
				        		listHtml += '	<td>';
				        		if (fileCnt > 0) {
				        			listHtml += '		<button class="dw" title="다운로드" onclick="javascript:redirectZipfileDownload(\'${HOME}\','+boardSeq+', '+fileCnt+', \'' + title + '\', \'/common/SLife/notice/list\');">다운로드</button>';
				        		}
			        			listHtml += '	</td>';
				        		listHtml += '   <td>' + regDate + '</td>';
				        		listHtml += '   <td>' + hits + '</td>';
				        		listHtml += '</tr>';
				        	});
				        	if (list.length > 0) {
				        		$("#noticeList").html(listHtml);
					        	$("#pageNationArea").html(data.pageNav);
				        	} else {
				        		$("#noticeList").html('<tr><td colspan="5">등록된 게시글이 없습니다.</td></tr>');
				        	}
			        	} else {
			        		//TODO
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function noticeDetail(boardSeq) {
				if (typeof boardSeq !=  "undefined" && boardSeq != "") {
					location.href = '${HOME}/common/SLife/notice/detail?seq='+boardSeq
				}
			}
			
			function fileDownload(boardSeq, fileCnt, title) {
				var zipFileName = "";
				if (fileCnt > 1) {
					zipFileName = title;
				}
				post_to_url('${HOME}/common/SLife/notice/download', {"board_seq":boardSeq, "zip_file_name":zipFileName});
			}
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
			<div class="contents sub">
				<div class="left_mcon">   
					<div class="sub_menu slife">
						<div class="title">학교생활</div>   
						<ul class="sub_panel" id="sLifeMenuArea"></ul>
					</div>
				</div>
			
				<div class="sub_con slife">
				 
					<div class="tt_wrap">
						<h3 class="am_tt"><span class="tt" id="boardTitleValue"></span></h3>
					</div>
					<form id="searchFrm" name="searchFrm" method="get" onsubmit="return false;">
							<input type="hidden" id="page" name="page">
						<div class="sch_wrap">
						    <span class="tt">등록일구간</span>
						    <input type="text" class="ip_date" id="startDate" name="search_start_date" placeholder="시작일">
						    <span class="tt">~</span>
						    <input type="text" class="ip_date" id="endDate" name="search_end_date" placeholder="종료일">
						    <input type="text" class="ip_search" name="search_text" placeholder="제목">
						    <button class="btn_search1" onclick="javascript:noticeListView(1);"></button>
						</div>
					</form>	
					<div class="rfr_con">   
						<table class="mlms_slife1">
							<thead>
								<tr>
									<th class="th01 bd01 w1">No.</th>
									<th class="th01 bd01 w3">제목</th>
									<th class="th01 bd01 w1">파일</th>
									<th class="th01 bd01 w2">등록일시</th>
									<th class="th01 bd01 w1">조회수</th>
								</tr>
							</thead>
							<tbody id="noticeList">    
								<tr>
									<td colspan="5">등록된 게시글이 없습니다.</td>
								</tr>
							</tbody>
						</table>
					</div> 
					<div class="pagination" id="pageNationArea"></div>
				</div>
			</div>
		</div>
	</body>
</html>