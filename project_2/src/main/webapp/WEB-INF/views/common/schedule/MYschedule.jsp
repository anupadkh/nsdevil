<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<script src="${JS}/schedule.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("body").addClass("full_schd");
		
		$('.free_textarea').on( 'keyup', 'textarea', function (e){
			$(this).css('height', 'auto' );
			$(this).height( this.scrollHeight );
		});
		$('.free_textarea').find( 'textarea' ).keyup();
		
		$(".btn_l").bind("click", function(){
	        $(".btn_l").toggleClass("move-trigger_l");
	        $(".aside_l").toggleClass("folding");
	        $(".main_con").toggleClass("");
	    });
		
		getMYSchedule("${curr_seq}","${RES_PATH}");
	});
	
	//삭제 후 재등록
	function deleteAndRegister(curr_seq) {
		if(!confirm("입력한 시간표가 삭제됩니다.\n계속 진행하시겠습니까?")) {
			return;
		}
		$.ajax({
	        type: "POST",
	        url: "${HOME}/ajax/pf/lesson/deleteAllSchedule",
	        data: {
	        	"curr_seq": curr_seq
	        },
	        dataType: "json",
	        success: function(data, status) {
	        	if (data.status == "200") {
	        		location.href="./scheduleMod";
				} else {
					alert("오류가 발생했습니다.");
				}
	        },
	        error: function(xhr, textStatus) {
	            alert("오류가 발생했습니다.");
	        }
	    });
	}

	function SearchVer(){
		post_to_url("${HOME}/aca/MYscheduleSearch", {"curr_seq": "${curr_seq}"});
	}
</script>
</head>

<body class="full_schd">
 <!-- s_container_table -->
<div id="container" class="container_table">
<!-- s_contents -->
<div class="contents main">

<!-- s_left_mcon -->
<div class="left_mcon aside_l">    
   
<div class="sub_menu st_am">
    <div class="title">학사일정</div>   
    <ul class="sub_panel">
        <li class="mn"><a href="${HOME }/aca/academicM" class="">학 사 력</a></li>
		<li class="mn"><a href="${HOME }/aca/MYscheduleM" class="on">MY 시간표</a></li>		
		<li class="mn"><a href="${HOME }/aca/MYscheduleMemo" class="">개인일정</a></li>
		<c:choose>
			<c:when test="${S_USER_LEVEL == 4}">
				<li class="mn"><a href="" class="">수강신청</a></li>
			</c:when>
		</c:choose>
    </ul>
</div>	
	
</div>
<!-- e_left_mcon -->

<!-- s_main_con -->
<div class="main_con cld">
<!-- s_메뉴 접기 버튼 -->	
<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
<!-- e_메뉴 접기 버튼 -->	

<!-- s_mpf_tabcontent2 --> 
 <div class="mpf_tabcontent2">
<!-- s_tt_wrap -->                
<div class="tt_wrap">
    <button class="btn_dw1" onclick="location.href='${HOME}/pf/lesson/scheduleExcelDown'">시간표 다운로드</button>
<!-- s_sch_wrap -->
<div class="sch_wrap">
<!-- s_wrap_p1_uselectbox -->
<!--
                     <div class="wrap_p1_uselectbox">
		                        <div class="uselectbox">
		                                <span class="uselected">전체 교육과정</span>
		                                <span class="uarrow">▼</span>
			
		                            <div class="uoptions">
		                                    <span class="uoption opt1 firstseleted">전체 교육과정</span>
		                                    <span class="uoption opt2 ">인체의 구조 Ⅰ</span>
		                                    <span class="uoption opt3 ">호흡기학</span>
		                                    <span class="uoption opt4 ">소화기학 Ⅱ</span>
		                                    <span class="uoption opt5 ">교육과정</span>
		                                    <span class="uoption opt6 ">교육과정</span>
		                                    <span class="uoption opt7 ">교육과정</span>
		                                    <span class="uoption opt8 ">교육과정</span>
		                            </div>
		                        </div>  								
                       </div>
-->
<!-- e_wrap_p1_uselectbox -->

<button onclick="location.href='${HOME}/aca/MYscheduleM'" class="btn_view full">달력 보기</button>
<button onclick="javascript:SearchVer();" class="btn_search1" title="검색"></button>                
            
</div>
<!-- e_sch_wrap -->
</div>
<!-- e_tt_wrap -->

    
<!-- s_mlms_tb1 -->                                                          
<table class="mlms_tb1">
                    <thead>
                        <tr>
                            <th class="th01 bd01 w1">차시</th>
                            <th class="th01 bd01 w2">날짜</th>
                            <th class="th01 bd01 w2">교시</th>
                            <th class="th01 bd01 w4">교수명</th>
                            <th class="th01 bd01 w4_1">수업주제</th>
                            <th class="th01 bd01 w3">수업방법</th>
                            <th class="th01 bd01 w2">관리</th>
                        </tr>
                    </thead>
                    <tbody id="scheduleList"></tbody>
<%--
                        <tr>
                            <td class="td_1"><span class="tt01">1</span></td>
                            <td class="td_1"><span class="tt_t">8 / 25</span></td>
                            <td class="td_1"><span class="tt01">1</span></td>
                            <td class="td_1 t_l"><span class="a_mp"><span class="pt01"><img src="${IMG}/ph_r1.png" alt="사진" class="pt_img"></span><span class="ssp1">교수명 (내과)</span></span></td>
                            <td class="td_1 t_l">해부학 수업</td>
<!-- 수업방법 - sp01 : 강의, sp02 : 형성, sp03 : 실습 --> 
                            <td class="td_1"><span class="sp sp01">강의</span></td>
                            <td class="td_1"><button class="btn_mdf open1">수정</button></td>
                        </tr>
                        <tr>
                            <td class="td_1"><span class="tt01">5</span></td>
                            <td class="td_1"><span class="tt_t">8 / 25</span></td>
                            <td class="td_1"><span class="tt01">1</span></td>
                            <td class="zero01 bd01 t_l"><span class="a_mp"><span class="pt01"><img src="${IMG}/ph_3.png" alt="사진" class="pt_img"></span><span class="ssp1">홍길동 (내과)</span></span></td>
<!-- 미등록 시  class : nonregi 추가 -->
                            <td class="td_1 t_l nonregi">호흡기학 (미등록)</td>
                            <td class="td_1"></td>
                            <td class="td_1"><button class="btn_mdf open1">수정</button></td>
                        </tr>
--%>
</table>
<!-- e_mlms_tb1 -->

<%-- 
<div class="bt_wrap" id="regButton">
    <!--<button class="bt_3a" onclick="location.href='./scheduleMod'">등록</button>-->
    <button class="bt_3" onclick="javascript:deleteAndRegister('${S_CURRICULUM_SEQ}');">전체삭제 - 재등록</button>
</div> --%>
</div>
<!-- e_mpf_tabcontent2 -->
<%-- <jsp:include page="schedulePopup.jsp">
	<jsp:param name="pageName" value="schedule" />
</jsp:include> --%>
</div>
</div>
</div>
</body>
</html>