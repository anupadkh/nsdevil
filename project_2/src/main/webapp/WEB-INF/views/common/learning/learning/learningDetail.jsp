<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function(){
				getBoardDetail();
			});
			
			function getBoardDetail() {
				$.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/common/learning/learning/detail",
			        data: {
			        	"board_seq":"${seq}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var boardInfo = data.board_info;
			        		var attachList = data.attach_list;
			        		var nextBoardInfo = data.next_board_info;
			        		var prevBoardInfo = data.prev_board_info;
			        		var userPicturePath = boardInfo.user_picture_path;
			        		var regUserDepart = boardInfo.user_department_name;
			        		var cateCode = boardInfo.board_cate_code;
			        		var notiSpan = "";
			        		if (cateCode == "96") {
			        			notiSpan = "공지";
			        		}
			        		if (notiSpan != "") {
			        			$("#notiSpan").html(notiSpan);
			        			$("#notiSpan").addClass("s1_2");
			        		}
			        		if (userPicturePath != "") {
			        			$("#regUserPicture").attr("src", "${RES_PATH}"+userPicturePath);
			        		}
			        		if (regUserDepart != "") {
				        		$("#regUserDepart").html("("+boardInfo.user_department_name+")");
			        		}
			        		$("#regUserName").html(boardInfo.name);
			        		
			        		$("#title").html(boardInfo.title);
			        		$("#regDate").html(boardInfo.reg_date);
			        		$("#content").html(boardInfo.content);
			        		
			        		if (attachList.length > 0) {
			        			var attachHtml = '<ul class="h_list m_tb">';
				        		$(attachList).each(function() {
				        			attachHtml += '<li class="li_1"><span>' + this.file_name + '</span><button class="dw" title="다운로드" onclick="fileDownload(\'${HOME}\', \'${RES_PATH}'+this.file_path+'\', \''+this.file_name+'\')">다운로드</button></li>';
				        		});
				        		attachHtml += '</ul>';
				        		$("#attachListArea").html(attachHtml);
			        		} else {
			        			$("#attachListArea").html("");
			        		}
			        		
			        		if (nextBoardInfo != null) {
			        			$("#nextBtn").attr("onclick", "javascript:location.href='${HOME}/common/learning/learning/detail?seq=" + nextBoardInfo.next_board_seq + "'");
			        		}
			        		
			        		if (prevBoardInfo != null) {
			        			$("#prevBtn").attr("onclick", "javascript:location.href='${HOME}/common/learning/learning/detail?seq=" + prevBoardInfo.prev_board_seq + "'");
			        		}
			        		
			        		if (boardInfo.reg_user_seq == "${sessionScope.S_USER_SEQ}" || "${sessionScope.S_USER_LEVEL}" < 3) {
			        			var modifyViewHtml = "";
			        			modifyViewHtml += '<div class="board btn_wrap_v">';
			        			modifyViewHtml += '	<button class="bt_2" onclick="getBoardModifyView();">수정</button>';
			        			modifyViewHtml += '	<button class="bt_3" onclick="removeBoard();">삭제</button>';
			        			modifyViewHtml += '</div>';
			        			$("#modifyViewArea").html(modifyViewHtml);
			        			
			        		}
			        	} else {
			        		alert("불러오기에 실패하였습니다. [" + data.status + "]");
			        		location.href="${HOME}/common/learning/learning/list";
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function removeBoard() {
				if(confirm("삭제하시겠습니까?")){
				   	$.ajax({
				        type: "POST",
				        url: "${HOME}/ajax/common/board/remove",
				        data: {"removeBoardSeqs":"${seq}"},
				        dataType: "json",
				        success: function(data, status) {
				        	if (data.status == "200") {
				        		alert("삭제 완료되었습니다.");
				        		location.href="${HOME}/common/learning/learning/list";
				        	} else {
				        		alert("삭제에 실패하였습니다. [" + data.status + "]");
				        		location.reload();
				        	}
				        },
				        error: function(xhr, textStatus) {
				            document.write(xhr.responseText);
				        },
				        beforeSend:function() {
				            $.blockUI();
				        },
				        complete:function() {
				            $.unblockUI();
				        }
				    });
				}
			}
			
			function getBoardModifyView() {
				post_to_url('${HOME}/common/learning/learning/modify', {"seq":"${seq}"});
			}
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
			<div class="contents sub">
				<div class="left_mcon"> 
					<div class="sub_menu">
						<div class="title">학습자료실</div>   
						<ul class="sub_panel" id="learningMenuArea"></ul>
					</div>
				</div>
				<div class="sub_con">
					<div class="tt_wrap">
						<h3 class="am_tt"><span class="tt" id="boardTitleValue"></span></h3>
						<div class="wrap">
							<button class="ic_v3" onclick="location.href='${HOME}/common/learning/learning/list'" title="목록 보기">목록</button>
						</div>               
					</div>
					<div class="tt_wrap2">
						<span id="notiSpan"></span>
						<span class="tt_con" id="title"></span>  
						<span class="tt_date" id="regDate"></span>    
					</div>	
					<div class="rfr_con">
						<div id="attachListArea"></div>
						<div class="h_list_custom mp_1">
							<div class="wrap">
								<span class="a_mp"><span class="pt01"><img id="regUserPicture" src="${DEFAULT_PICTURE_IMG}" alt="사진" class="pt_img"></span><span class="ssp1"><span id="regUserName"></span><span class="ssp2" id="regUserDepart"></span></span></span>
							</div>
						</div>
						<div class="con_wrap"> 
							<div class="con_s con_s_h" id="content"></div>
						</div>
						<div id="modifyViewArea"></div>
					</div> 
					<div class="btn_wrap">
						<button class="btn1" id="nextBtn" onclick="javascript:alert('다음 글이 없습니다.'); return false;"><span><span class="ic">▲</span>다음 글</span></button>
						<button class="btn2" id="prevBtn" onclick="javascript:alert('이전 글이 없습니다.'); return false;"><span><span class="ic">▼</span>이전 글</span></button>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>