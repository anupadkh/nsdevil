<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
    	<script src="${HOME}/resources/smarteditor_v2.9/js/service/HuskyEZCreator.js"></script> 
		<script type="text/javascript">
			var editor_object = [];
			$(document).ready(function(){
				getOSCECodeList();
				getBoardDetail();
				nhn.husky.EZCreator.createInIFrame({
					oAppRef: editor_object,
				    elPlaceHolder: "content",
		            sSkinURI : "${HOME}/resources/smarteditor_v2.9/SmartEditor2Skin_ko_KR.html"     ,
		            htParams : {
	                    // 툴바 사용 여부 (true:사용/ false:사용하지 않음)
	                    bUseToolbar : true,            
	                    // 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
	                    bUseVerticalResizer : true,    
	                    // 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
	                    bUseModeChanger : true
				    },
		            fOnAppLoad:function(){
		            	editor_object.getById["content"].exec("SE_FIT_IFRAME", [0,400]);	   
					}
				 });
			});
			
			function getOSCECodeList() {
				$.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/common/learning/osce/code/list",
			        data: {},
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		osceCodeSelectBoxSetting(data.code_list);
			        	} else {
			        		alert("임상술기 항목 불러오기에 실패하였습니다. [" + data.status + "]");
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			
			function osceCodeSelectBoxSetting(list) {
				var target = $("#osceCodeSelectBox").find("div.uoptions");
				var listHtml = '<span class="uoption" value="">선택</span>';
				$(list).each(function() {
					listHtml += '<span class="uoption" value="' + this.osce_code + '">' + this.osce_name + '</span>';
				});
				$(target).html(listHtml);
			}
			
			function getBoardDetail() {
				$.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/common/learning/osce/modify/detail",
			        data: {
			        	"board_seq":"${seq}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var boardInfo = data.board_info;
			        		var attachList = data.attach_list;
			        		var boardCateCode = boardInfo.board_cate_code;
			        		var osceName = boardInfo.osce_name;
			        		if (boardCateCode != "") {
								$("#osceCodeSelectBox").attr("value", boardCateCode);
								$("#osceCodeSelectBox > .uselected").html(osceName);
			        		}			        		
			        		$("#title").val(boardInfo.title);
			        		$("#content").val(boardInfo.content);
			        		if (attachList.length > 0) {
			        			$(attachList).each(function() {
				        			var attachSeq = this.board_attach_seq;
				        			var fileName = this.file_name;
				        			$("#fileUploadArea").append('<div class="wrap"><span class="sp1">'+fileName+'</span><button class="btn_c" title="삭제하기" onclick="removeAttachFile(this,'+(fileCnt++)+', ' + attachSeq + ')">X</button></div>');
			        			});
			        		} else {
			        			$("#attachListArea").html("");
			        		}
			        	} else {
			        		alert("임상술기 불러오기에 실패하였습니다. [" + data.status + "]");
			        		location.href="${HOME}/common/learning/osce/list";
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			var fileCnt = 0;
			function fileSelect() {
				var div = $("<div>").addClass(".wrap");
				var fileInput = $("<input>").attr("type", "file").attr("id", "uploadFile"+fileCnt).attr("name", "uploadFile").addClass("blind-position");
				$("#fileUploadArea").append(fileInput);
				
				$(fileInput).change(function(){
					var file = $(this).val();
					if (file != "") {
						var attachFileName = file.substring(file.lastIndexOf('\\') + 1); //파일명
						var ext = attachFileName.substring(attachFileName.lastIndexOf('.') + 1).toLowerCase(); //파일 확장자
						var fileMaxSize = 10; //10MB
						var fileSize = this.files[0].size/1024/1024;
						if (fileSize > fileMaxSize) {
							alert("10MB이하의 파일만 업로드 가능합니다.");
							fileValueInit(fileInput);
							return false;
						}
						
						//파일 확장자 유효성검사
						if (/(mp4)$/i.test(ext) == false) {
							alert("파일의 확장자를 확인해주세요.(mp4)");
							fileValueInit(fileInput);
							return false;
						}
						$("#fileUploadArea").append('<div class="wrap"><span class="sp1">'+attachFileName+'</span><button class="btn_c" title="삭제하기" onclick="removeAttachFile(this,'+(fileCnt++)+')">X</button></div>');
					}
				});
				$(fileInput).click();
			}
			
			function removeAttachFile(t, num, removeAttachSeq) {
				$(t).parent().remove();
				$("#uploadFile"+num).remove();
				if (removeAttachSeq != "") {
					var removeAttachSeqStr = $("#removeAttachSeqStr").val();
					if (removeAttachSeqStr == "") {
						removeAttachSeqStr = removeAttachSeq;
					} else {
						removeAttachSeqStr += "," + removeAttachSeq;
					}
					$("#removeAttachSeqStr").val(removeAttachSeqStr);
				}
			}
			
			function submitBoard() {
				editor_object.getById["content"].exec("UPDATE_CONTENTS_FIELD", []);
				
				var title = $("#title").val();
				if (isEmpty(title) || isBlank(title)){
					alert("제목을 입력해주세요");
					return false;
				}
				
				var content = $("#content").val();
				if (content == "<p><br></p>") {
					alert("내용을 입력해주세요");
					return false;
				}
				
				var cateCodeValue = $("#osceCodeSelectBox").attr("value");
				$("#boardCateCode").val(cateCodeValue);
				
				if ($("#fileUploadArea").find("div.wrap").length < 1) {
					alert("동영상 파일을 첨부해주세요.");
					return false;
				}
				
				$("#boardFrm").attr("action", "${HOME}/ajax/common/learning/osce/modify").ajaxForm({
					beforeSend: function () {
						$.blockUI();
					},
					type: "POST",
					dataType:"json",
					success:function(data){
						if (data.status == "200") {
							alert("수정이 완료되었습니다.");
							location.href='${HOME}/common/learning/osce/list';
						} else {
							alert("수정에 실패하였습니다. [" + data.status + "]");
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						document.write(xhr.responseText);
				    },
			        complete:function() {
			            $.unblockUI();
			        }
				}).submit();
			}
		</script>
	</head>
	<body>
		<form id="boardFrm" name="boardFrm" onsubmit="return false;" enctype="multipart/form-data">
		<input type="hidden" id="removeAttachSeqStr" name="removeAttachSeqStr">
		<input type="hidden" id="boardCateCode" name="boardCateCode" value="">
		<input type="hidden" id="boardSeq" name="boardSeq" value="${seq}">
		    <div id="container" class="container_table">
		        <div class="contents sub">
		            <div class="left_mcon"> 
		                <div class="sub_menu">
		                	<div class="title">학습자료실</div>
		                    <ul class="sub_panel" id="learningMenuArea"></ul>
		                </div>
		            </div>
		            <div class="sub_con">
		                <div class="tt_wrap"><h3 class="am_tt"><span class="tt" id="boardTitleValue"> 글 수정</span></h3></div>
		                <div class="rfr_con">
		                    <table class="mlms_tb4">
		                        <tbody>
		                        	<tr>
										<th scope="row" class="w1"><span class="sp1">${code_name}</span></th>
										<td class="w2">
											<div class="wrap1_lms_uselectbox">
												<div class="uselectbox" id="osceCodeSelectBox" value="">
													<span class="uselected">선택</span>
													<span class="uarrow">▼</span>
													<div class="uoptions">
														<span class="uoption">선택</span>
													</div>
												</div>
											</div>
										</td>
									</tr>
		                            <tr>
		                                <th scope="row" class="w1"><span class="sp1">제목</span></th>
		                                <td><input type="text" id="title" name="title" class="ip_pt1" placeholder="제목을 입력해주세요."></td>
		                            </tr>
		                            <tr>
		                                <th scope="row"><span class="sp1">내용</span></th>
		                                <td class="tarea free_textarea">
		                                    <textarea class="tarea01" style="height: 100px;" id="content" name="content"></textarea>
		                                </td>
		                            </tr>
		                            <tr>
		                                <th rowspan="2" scope="row"><span class="sp1">파일첨부</span></th>
		                                <td>
		                                    <input type="text" class="ip_pt2" value="동영상 파일을 첨부해주세요. ( mp4 )" readonly="readonly">
		                                    <button class="btn1" onclick="fileSelect();">선택</button>
		                                </td>
		                            </tr>
		                            <tr>
										<td class="f_add1" id="fileUploadArea"></td>
									</tr>
		                        </tbody>
		                    </table>
		                </div> 
		
		                <div class="btn_wrap_r">
		                    <button class="bt_2" onclick="submitBoard();">수정</button>
							<button class="bt_3" onclick="location.href='${HOME}/common/learning/learning/list'">취소</button>
		                </div>
		            </div>
		        </div>
		    </div>
		</form>
	</body>
</html>