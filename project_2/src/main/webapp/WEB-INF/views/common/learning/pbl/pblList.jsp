<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script src="${JS}/lib/datetimep_1.js"></script>
		<script src="${JS}/lib/monthdatepicker_ui.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				
				pblListView(1);
				
				$.datetimepicker.setLocale('kr');
			    $('#startDate').datetimepicker({
			        timepicker:false,
			        datepicker:true,
			        format:'y-m-d',
			        formatDate:'y-m-d'
			    });
			    
			    $('#endDate').datetimepicker({
			        timepicker:false,
			        datepicker:true,
			        format:'y-m-d',
			        formatDate:'y-m-d'
			    });
			    
			    $("#startDate").on("change", function(e){
			    	var startDate = $("#startDate").val();
			    	if (startDate != "") {
				    	$('#endDate').datetimepicker({minDate: startDate});
			    	}
			    });
			    
			    $("#endDate").on("change", function(e){
			    	var endDate = $("#endDate").val();
			    	if (endDate != "") {
				    	$('#startDate').datetimepicker({maxDate: endDate});
			    	}
			    });
			});

			function pblListView(page) {
				if (typeof page == "undefined") {
					page = 1;
				}
				$("input[name='page']").val(page);
				
				var startDate = $("#startDate").val();
				var endDate = $("#endDate").val();
				
				if (!isEmpty(startDate)) {
					if (!isValidDate(startDate)) {
						alert("시작일의 날짜형식이 올바르지않습니다.(YYYY-MM-DD)")
						return false;
					}
				}
				
				if (!isEmpty(endDate)) {
					if (!isValidDate(endDate)) {
						alert("종료일의 날짜형식이 올바르지않습니다.(YYYY-MM-DD)")
						return false;
					}
				}
				
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/common/learning/pbl/list",
			        data: $("#searchFrm").serialize(),
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var list = data.list;
				        	var listHtml = '';
				        	var totalCnt = data.totalCnt;
				        	$(list).each(function(){
				        		var boardSeq = this.board_seq;
				        		var rowNum = this.row_num;
				        		var hits = this.hits;
				        		var title = this.title;
				        		var content = this.content;
				        		var start_date = this.start_date;
				        		var end_date = this.end_date;
				        		content = content.replace(/(<([^>]+)>)/ig,"");
								content = content.replace(/\n/g, "");//행 바꿈 제거
				            	content = content.replace(/\s+/, "");//왼쪽 공백 제거
				            	content = content.replace(/\r/g, "");//엔터 제거
				        		var fileCnt = this.file_cnt;
				        		var regDate = this.reg_date;
				        		var cateCode = this.board_cate_code;
				        		var showTarget = this.show_target;
				        		var targetList = this.target_list;
				        		
				        		var noText = (totalCnt+1) - rowNum;
				        		var noClass = "";
				        		if (cateCode == "96") {
				        			noText = "공지";
				        			noClass = "s1_1";
				        		}
				        		var targetSpan = "";
				        		if (showTarget == "00") {
				        			targetSpan = '<span class="s2_9_all">전체</span>';
				        		} else if (showTarget == "02") {
			        				targetSpan = '<span class="s2_9_all">학생 - 전체</span>';
				        		} else if (showTarget == "03") {
				        			$(targetList).each(function(idx) {
			        					targetSpan += '<span class="s2_7_st">' + this.target_name + '</span>';
			        				});
				        		}
				        		listHtml += '<tr>';
				        		listHtml += '    <td><span class="' + noClass + '">' + noText + '</span></td>';
				        		listHtml += '    <td>' + targetSpan + '</td>';
				        		listHtml += '    <td class="t_l lnk" onclick="javascript:pblDetail('+boardSeq+')" title="상세보기">';
				        		listHtml += '    	<span class="sp1">' + title + ' </span>';
				        		listHtml += '		</td>';	
				        		listHtml += '    <td>' + start_date +'<br>' + end_date + '</td>';
				        		listHtml += '	<td>';
				        		if (fileCnt > 0) {
				        			listHtml += '		<button class="dw" title="다운로드" onclick="javascript:redirectZipfileDownload(\'${HOME}\','+boardSeq+', '+fileCnt+', \'' + title + '\', \'/common/learning/pbl/list\');">다운로드</button>';
				        		}
				        		listHtml += '	</td>';
				        		listHtml += '   <td>' + hits + '</td>';
				        		listHtml += '</tr>';
				        	});
				        	if (list.length > 0) {
				        		$("#boardList").html(listHtml);
					        	$("#pageNationArea").html(data.pageNav);
				        	} else {
				        		$("#boardList").html('<tr><td colspan="6">등록된 게시글이 없습니다.</td></tr>');
				        	}
			        	} else {
			        		//TODO
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function pblDetail(boardSeq) {
				if (typeof boardSeq !=  "undefined" && boardSeq != "") {
					location.href = '${HOME}/common/learning/pbl/detail?seq='+boardSeq
				}
			}
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
		    <div class="contents sub">
		        <div class="left_mcon">
		            <div class="sub_menu">
		            	<div class="title">학습자료실</div>
		                <ul class="sub_panel" id="learningMenuArea"></ul>
		            </div>
		        </div>
		
		        <div class="sub_con">
		            <div class="tt_wrap">
		                <h3 class="am_tt"><span class="tt" id="boardTitleValue"></span></h3>
		            </div>
		
		            <form id="searchFrm" name="searchFrm" method="get" onsubmit="return false;">
						<div class="sch_wrap">
							<input type="hidden" id="page" name="page">
						    <span class="tt">기한</span>
						    <input type="text" class="ip_date" id="startDate" name="search_start_date" placeholder="시작일">
						    <span class="tt">~</span>
						    <input type="text" class="ip_date" id="endDate" name="search_end_date" placeholder="종료일">
						    <input type="text" class="ip_search" name="search_text" placeholder="제목">
						    <button class="btn_search1" onclick="javascript:pblListView(1);"></button>
						</div>
					</form>	
		
		            <div class="rfr_con">
		                <table class="mlms_tb2">
		                    <thead>
		                        <tr>
		                            <th class="th01 bd01 w1">구분</th>
		                            <th class="th01 bd01 w2">대상</th>
		                            <th class="th01 bd01 w4">제목</th>
		                            <th class="th01 bd01 w2">기한</th>
		                            <th class="th01 bd01 w1">파일</th>
		                            <th class="th01 bd01 w1">조회수</th>
		                        </tr>
		                    </thead>
		                    <tbody id="boardList"></tbody>
		                </table>
		            </div> 
		            <div class="pagination" id="pageNationArea"></div>
		        </div>
		    </div>
		</div>
	</body>
</html>