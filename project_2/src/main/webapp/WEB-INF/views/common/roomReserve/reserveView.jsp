<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript">
		$(document).ready(function() {
			
			setWeekDate("");
			
			bindPopupEvent("#m_pop1", ".open1");
			
			$( ".boardwrap" ).accordion({
				collapsible: true
			});
			
			$(document).on( 'keyup', 'textarea', function (e){
				$(this).css('height', 'auto' );
				$(this).height( this.scrollHeight );
			});		
			$(document).find( 'textarea' ).keyup();
	        
			$('.timepicker1').timepicki();
			$('.timepicker2').timepicki({custom_classes:"time2"});
			
			$(document).on("change","input[name=all_day_flag]",function(){
				if($(this).is(":checked")){
					$("input[name=start_time]").prop("disabled",true);
					$("input[name=end_time]").prop("disabled",true);
					$("input[name=start_time]").val("");
					$("input[name=end_time]").val("");
				}else{
					$("input[name=start_time]").prop("disabled",false);
					$("input[name=end_time]").prop("disabled",false);
				}
			});			
		});
	
		function setWeekDate(type){
			var currentDay;
			if(type=="")
				currentDay = new Date();
			else if(type=="pre"){
				currentDay = new Date($("input[name=sDate]").val());
				currentDay.setDate(currentDay.getDate()-7);
			}else{
				currentDay = new Date($("input[name=sDate]").val());
				currentDay.setDate(currentDay.getDate()+7);
			}
			var theYear = currentDay.getFullYear();
			var theMonth = currentDay.getMonth();
			var theDate  = currentDay.getDate();
			var theDayOfWeek = currentDay.getDay();
			 
			var thisWeek = [];
			
			var weekName = ["sun","mon","tue","wnd","thu","fri","sat"];
			
			var weekDayText = "";
			var this_week_flag = false;
			for(var i=0; i<7; i++) {
				var resultDay = new Date(theYear, theMonth, theDate + (i - theDayOfWeek));
				var yyyy = resultDay.getFullYear();
				var mm = Number(resultDay.getMonth()) + 1;
				var dd = resultDay.getDate();
				
				mm = String(mm).length === 1 ? '0' + mm : mm;
				dd = String(dd).length === 1 ? '0' + dd : dd;
				
				thisWeek[i] = yyyy + '-' + mm + '-' + dd;

				$("span[data-name="+weekName[i]+"]").text(dd);
				$("span[data-name="+weekName[i]+"]").attr("data-value",thisWeek[i]);
								
				if(theYear == yyyy && theMonth == resultDay.getMonth() && theDate == dd){
					this_week_flag = true;
					$("td").removeClass("t_day");
					$("span[data-value="+thisWeek[i]+"]").closest("td").addClass("t_day");					
				}	
					
				if(i==0)
					weekDayText = yyyy + "년 " + mm+"월 " + dd+"일 ~ ";
				else if(i==6)
					weekDayText += mm+"월 " + dd+"일";
			}
			
			$("#date").text(weekDayText);
			$("input[name=sDate]").val(thisWeek[0]);
			$("input[name=eDate]").val(thisWeek[6]);
			
			if($("input[name=day_flag]").val() == "Y"){
				if(this_week_flag == true){
					theMonth = Number(currentDay.getMonth())+1;
					theMonth = String(theMonth).length == 1? '0' + theMonth : theMonth;
					var this_day = theYear + '-' + theMonth + '-' + theDate;				
					getReserveList(this_day);
				}else{
					$("td").removeClass("t_day");
					$("span[data-value="+thisWeek[6]+"]").closest("td").addClass("t_day");			
					getReserveList(thisWeek[6]);
				}
				
			}else
				getReserveList("");
		}
		
		//교육과정 가져오기
		function getReserveList(day) {
			$.ajax({
				type : "POST",
				url : "${HOME}/ajax/common/roomReserve/list",
				data : {
					"start_date" : $("input[name=sDate]").val()
					,"end_date" : $("input[name=eDate]").val()
					,"my_flag" : $("input[name=my_flag]").val()
					,"facility_code" : $("input[name=facility]").val()	
					,"day_flag" : $("input[name=day_flag]").val()
					,"this_day" : day
				},
				dataType : "json",
				success : function(data, status) {
					if (data.status == "200") {
						var htmls = "";
						var obj = {};
						$.each(data.facilityList, function(index){
							//주별
							if($("input[name=day_flag]").val()=="N"){
								htmls += '<tr>'
									+'<td class="bd02 tt_t1"><span class="sp1">'+this.code_name+'</span></td>'
									+'<td class="" data-name="'+this.code+'_0"></td>'
									+'<td class="" data-name="'+this.code+'_1"></td>'
									+'<td class="" data-name="'+this.code+'_2"></td>'
									+'<td class="" data-name="'+this.code+'_3"></td>'
									+'<td class="" data-name="'+this.code+'_4"></td>'
									+'<td class="" data-name="'+this.code+'_5"></td>'
									+'<td class="" data-name="'+this.code+'_6"></td>'
									+'</tr>';
							}else{
								//일별
								htmls += '<tr><td class="bd02 tt_t1">'
	                                +'<span class="sp1">'+this.code_name+'</span>'                
	                                +'</td>'
	                                +'<td colspan="7" class="bd02" data-name="'+this.code+'"></td></tr>';
							}
						});
						$("#facilityList").html(htmls);
						
						htmls = "";
						$.each(data.list, function(index){
							var width = 100;							
							var margin = "";
							var all_day_class = "bk_tt";
							var time = this.start_time+'-'+this.end_time;
							var state = "";
							var onClick_htmls = "";
							//대기 = WAIT, 승인 = APPROVAL, 비허가 = RETURN
							if(this.reserve_state_code == "WAIT")
								state = "ss";
							if(this.reserve_state_code == "RETURN")
								state = "xx";
							
							//해당 일에 종일 이미 있을 경우 margin-top 값
							if(obj[this.facility_code+"_"+this.start_day] != null && obj[this.facility_code+"_"+this.start_day] != 0){
								if(this.all_day_flag=="Y")
									margin = "margin-top:"+(obj[this.facility_code+"_"+this.start_day]*80)+"px;";
								else{
									margin = "margin-top:"+(80+(obj[this.facility_code+"_"+this.start_day]-1)*60)+"px;";
									obj[this.facility_code+"_"+this.start_day]=0;
								}
							}
							
							//종일 일때
							if(this.all_day_flag == "Y"){
								width += (this.end_day-this.start_day)*100;
								all_day_class = "bk_ttx";
								if(this.day == 0)
									time = "종일("+this.start_date_mmdd+")";
								else
									time = "종일("+this.start_date_mmdd+"~"+this.end_date_mmdd+")";
								
								for(var i=this.start_day;i<this.end_day;i++){
									if(obj[this.facility_code+"_"+i] == null)
										obj[this.facility_code+"_"+i] = 1;
									else
										obj[this.facility_code+"_"+i] += 1;									
								}
							}
							if(this.my_check == 'Y' || '${S_USER_LEVEL}' == 1)
								onClick_htmls =  'onClick="getReserveDetail('+this.reserve_seq+');"';
											
							if($("input[name=day_flag]").val()=="N"){
								htmls = '<a class="'+all_day_class+' open1 '+state+'" style="width:'+width+'px;'+margin+'" '+onClick_htmls+'>'
	                           	+'<span class="sp_t1">'+time+'</span>'
	                           	+'<span class="sp_t2">'+this.title+'</span>'
	                           	+'<span class="vicon"></span>'
	                           	+'</a>';
								$("td[data-name="+this.facility_code+"_"+this.start_day+"]").append(htmls);
							}else{
								htmls = '<a class="'+all_day_class+' open1 '+state+'" '+onClick_htmls+'>'
	                           	+'<span class="sp_t1">'+time+'</span>'
	                           	+'<span class="sp_t2">'+this.title+'</span>'
	                           	+'<span class="vicon"></span>'
	                           	+'</a>';
								$("td[data-name="+this.facility_code+"]").append(htmls);
							}
						});
					} else {
	
					}
				},
				error : function(xhr, textStatus) {
					document.write(xhr.responseText);
				},
				beforeSend : function() {
					$.blockUI();
				},
				complete : function() {
					$.unblockUI();
				}
			});
		}
		
		function submitReserve(){	
			var all_day_flag = "Y";
			if($("input[name=all_day_flag]").is(":checked") == false){
				all_day_flag='N';
				
				if(isEmpty($("input[name=start_time]").val())){
					alert("시설 예약 시작 시간을 입력하세요");
					$("input[name=start_time]").focus();
					return;
				}
				
				if(isEmpty($("input[name=end_time]").val())){
					alert("시설 예약 종료 시간을 입력하세요");
					$("input[name=end_time]").focus();
					return;
				}	
			}
			
			if(isEmpty($("input[name=start_date]").val())){
				alert("시설 예약 시작일을 입력하세요"); 
				$("input[name=start_date]").focus();
				return;
			}
			
			if(isEmpty($("input[name=end_date]").val())){
				alert("시설 예약 종료일을 입력하세요"); 
				$("input[name=end_date]").focus();
				return;
			}
			
			if(isEmpty($("input[name=title]").val())){
				alert("제목을 입력해주세요");
				return;				
			}
			
			var regExp = /^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/;
			var tel = $("input[name=tel1]").val()+"-"+$("input[name=tel2]").val()+"-"+$("input[name=tel3]").val();
			
			if(!regExp.test(tel)){
				alert("잘못된 전화번호 입니다.");
				return;
			}
			
			$.ajax({
				type : "POST",
				url : "${HOME}/ajax/common/roomReserve/create",
				data : {
					"start_date" : $("input[name=start_date]").val()
					, "end_date" : $("input[name=end_date]").val()
					, "all_day_flag" : all_day_flag
					, "title" : $("input[name=title]").val()
					, "content" : $("textarea[name=content]").val().replace(/\n/g, "<br/>")
					, "facility_code" : $("#facility_code span.uselected").attr("data-value")
					, "important_level" : $("#important_level span.uselected").attr("data-value")
					, "tel" : tel
					, "start_time" : $("input[name=start_time]").val()
					, "end_time" : $("input[name=end_time]").val()
					, "reserve_seq" : $("input[name=reserve_seq]").val()
				},
				dataType : "json",
				success : function(data, status) {
					if (data.status == "200") {
						alert("저장을 완료하였습니다.");
						$("input[name=start_time]").prop("disabled",false);
						$("input[name=end_time]").prop("disabled",false);						
						var this_day = "";
						
						if($("input[name=day_flag]").val() == "Y"){
							this_day = $("td.t_day").find("span.sp_date").attr("data-value");
						}
						getReserveList(this_day);
						$("#m_pop1").hide();
						formReset();
					} else {
						alert("저장을 실패하였습니다.");
					}
				},
				error : function(xhr, textStatus) {
					document.write(xhr.responseText);
				},
				beforeSend : function() {
					$.blockUI();
				},
				complete : function() {
					$.unblockUI();
				}
			});
		}
		
		function getReserveDetail(reserve_seq){
			$.ajax({
				type : "POST",
				url : "${HOME}/ajax/common/roomReserve/detail",
				data : {
					"reserve_seq" : reserve_seq
				},
				dataType : "json",
				success : function(data, status) {
					if (data.status == "200") {
						var tel = data.reserveInfo.tel.split("-");
						$("input[name=reserve_seq]").val(reserve_seq);
						$("input[name=start_date]").val(data.reserveInfo.start_date);
						$("input[name=end_date]").val(data.reserveInfo.end_date);
						$("input[name=title]").val(data.reserveInfo.title);
						$("textarea[name=content]").val(data.reserveInfo.content.split('<br/>').join("\r\n"));
						$("#facility_code div.uoptions span[data-value="+data.reserveInfo.facility_code+"]").click();
						$("#important_level div.uoptions span[data-value="+data.reserveInfo.important_level+"]").click();
						$("input[name=tel1]").val(tel[0]);			
						$("input[name=tel2]").val(tel[1]);
						$("input[name=tel3]").val(tel[2]);
						$("input[name=start_time]").val(data.reserveInfo.start_time_12h);
						$("input[name=end_time]").val(data.reserveInfo.end_time_12h);
						if(data.reserveInfo.all_day_flag == "Y")
							$("input[name=all_day_flag]").prop("checked", true).change();
						else
							$("input[name=all_day_flag]").prop("checked", false).change();
						if(data.reserveInfo.reserve_state_code == "WAIT")
							$("button[name=saveBtn]").show();
						else
							$("button[name=saveBtn]").hide();
						
						$("#m_pop1").show();
					} else {
						alert("불러오기 실패하였습니다.");
					}
				},
				error : function(xhr, textStatus) {
					document.write(xhr.responseText);
				},
				beforeSend : function() {
					$.blockUI();
				},
				complete : function() {
					$.unblockUI();
				}
			});
		}
		
		function formReset(){
			$("input[name=start_date]").val("");
			$("input[name=end_date]").val("");
			$("input[name=title]").val("");
			$("textarea[name=content]").val("");
			$("#facility_code div.uoptions span:ep(0)").click();
			$("#important_level div.uoptions span:ep(0)").click();
			$("input[name=tel1]").val("");			
			$("input[name=tel2]").val("");
			$("input[name=tel3]").val("");
			$("input[name=start_time]").val("");
			$("input[name=end_time]").val("");
			$("input[name=all_day_flag]").prop("checked", false).change();
		}
		
		//전체보기, MY 보기 변경
		function viewType(obj_name){
			$("span.type").removeClass("on");
			$("span[data-name="+obj_name+"]").addClass("on");
			if(obj_name=="my"){
				$("span[data-name=fcName]").text("My 예약");
				$("input[name=my_flag]").val("Y");
				$("input[name=facility]").val("");
				$("a.detailType").removeClass("on");	
			} else{
				$("span[data-name=fcName]").text("전체");
				$("input[name=my_flag]").val("N");
				$("input[name=facility]").val("");
				$("a.detailType").removeClass("on");				
			}
			
			var this_day = "";
			
			if($("input[name=day_flag]").val() == "Y"){
				this_day = $("td.t_day").find("span.sp_date").attr("data-value");
			}
			getReserveList(this_day);
		}
		
		//시설 별
		function setFacility(obj_name){
			$("a.detailType").removeClass("on");
			$("a[data-name="+obj_name+"]").addClass("on");
			$("span[data-name=fcName]").text($("a[data-name="+obj_name+"]").text());
			$("input[name=facility]").val(obj_name);
			var this_day = "";
			
			if($("input[name=day_flag]").val() == "Y"){
				this_day = $("td.t_day").find("span.sp_date").attr("data-value");
			}
			getReserveList(this_day);
		}
		
		//주별, 일별 변경
		function weekDayChange(flag){
			$("input[name=day_flag]").val(flag);
			if(flag=="Y"){
				$("button[name=weekBtn]").show();
				$("button[name=dayBtn]").hide();
				$("#mainTable").removeClass("bk_table_w").addClass("bk_table_d");				
			}
			else{
				$("button[name=weekBtn]").hide();
				$("button[name=dayBtn]").show();
				$("#mainTable").removeClass("bk_table_d").addClass("bk_table_w");
			}			
			setWeekDate("");
		}
		
		//일별일때 일자별 
		function getReserveThisDay(obj){
			if($("input[name=day_flag]").val() == "Y"){
				$("td").removeClass("t_day");
				$(obj).addClass("t_day");
				getReserveList($(obj).children('span:eq(1)').attr('data-value'));
			}
		}
	</script>
	</head>
	<body>
		<input type="hidden" name="reserve_seq" value="">
		<input type="hidden" name="my_flag" value="N">
		<input type="hidden" name="day_flag" value="N">
		<input type="hidden" name="facility" value="">
		
		<!-- s_container_table -->
		<div id="container" class="container_table">
			<!-- s_contents -->
			<div class="contents sub">
	
				<!-- s_left_mcon -->
				<div class="left_mcon">
					<!-- s_학습자료 메뉴 -->
					<div class="sub_menu bk">
						<div class="title">시설예약</div>
						<div class="twrap" style="margin-bottom:5px;">						
							<a href="#" style="width:100%;"><span class="tt on type open1" data-name="all">예약신청</span></a>
						</div>
						<div class="twrap">
							<a href="javascript:viewType('all');"><span class="tt on type" data-name="all">전체보기</span></a>
							<a href="javascript:viewType('my');"><span class="tt type" data-name="my">My 예약</span></a>
						</div>
						<ul class="sub_panel">
							<c:forEach var="facilityList" items="${facilityList}">
								<li class="mn"><a href="javascript:setFacility('${facilityList.code}');" data-name="${facilityList.code}" class="detailType"> ${facilityList.code_name}</a></li>
							</c:forEach>
						</ul>
					</div>
					<!-- e_학습자료 메뉴 -->
				</div>
				<!-- e_left_mcon -->
	
				<!-- s_sub_con -->
				<div class="sub_con bk">
	
					<!-- s_tt_wrap -->
					<div class="tt_wrap">
						<h3 class="am_tt">
							<span class="tt" data-name="fcName">전체</span>
						</h3>
	
						<div class="wrap_l">
							<button class="ic_v1 open1" title="예약신청">예약신청</button>
							<div class="rwrap">
								<span class="sicon">_</span> <span class="vtt">대 기</span>
							</div>
							<div class="rwrap">
								<span class="vicon">V</span> <span class="vtt">예약됨</span>
							</div>
							<div class="rwrap">
								<span class="xicon">X</span> <span class="vtt">비허가</span>
							</div>
						</div>
	
						<div class="wrap_r">
							<div class="cwrap">
								<input type="hidden" name="sDate"><input type="hidden" name="eDate">
								<a class="ar1" href="javascript:setWeekDate('pre');">◀</a> 
								<span class="tt" id="date"></span> 
								<a class="ar2" href="javascript:setWeekDate('next');">▶</a>
							</div>
							<button class="ic_v2" name="dayBtn" onClick="weekDayChange('Y');" title="일별 보기">일별</button>
							<button class="ic_v2" name="weekBtn" onClick="weekDayChange('N');" title="주별 보기" style="display:none;">주별</button>
						</div>
	
					</div>
					<!-- e_tt_wrap -->
	
					<!-- s_rfr_con -->
					<div class="rfr_con">
	
						<table class="bk_table_w" id="mainTable">
							<thead>
								<tr>
									<td class="th01 bd01 w0"></td>
									<td class="th01 bd01 w1 bd02" onClick="getReserveThisDay(this);" style="cursor:pointer;">
										<span class="sp_tt color05">일</span>
										<span class="sp_date color05" data-name="sun" data-value="">15</span>
									</td>
									<td class="th01 bd01 w1" onClick="getReserveThisDay(this);" style="cursor:pointer;">
										<span class="sp_tt">월</span> 
										<span class="sp_date" data-name="mon" data-value="">16</span>
									</td>
									<td class="th01 bd01 w1" onClick="getReserveThisDay(this);" style="cursor:pointer;">
										<span class="sp_tt">화</span> 
										<span class="sp_date" data-name="tue" data-value="">17</span>
									</td>
									<td class="th01 bd01 w1" onClick="getReserveThisDay(this);" style="cursor:pointer;">
										<span class="sp_tt">수</span> 
										<span class="sp_date" data-name="wnd" data-value="">18</span>
									</td>
									<td class="th01 bd01 w1" onClick="getReserveThisDay(this);" style="cursor:pointer;">
										<span class="sp_tt">목</span> 
										<span class="sp_date" data-name="thu" data-value="">19</span>
									</td>
									<td class="th01 bd01 w1" onClick="getReserveThisDay(this);" style="cursor:pointer;">
										<span class="sp_tt">금</span> 
										<span class="sp_date" data-name="fri" data-value="">20</span>
									</td>
									<td class="th01 bd01 w1 bd03" onClick="getReserveThisDay(this);" style="cursor:pointer;">
										<span class="sp_tt color06">토</span>
										<span class="sp_date color06" data-name="sat" data-value="">21</span>
									</td>
								</tr>
							</thead>
							<tbody id="facilityList">													
							</tbody>
						</table>
					</div>
					<!-- e_rfr_con -->
	
				</div>
				<!-- e_sub_con -->
	
				<!-- s_right_m -->
				<div class="right_m hidden">
					<button type="button" onclick="location.href='pf_schd_m.html'" title="일정" class="ic_list"></button>
				</div>
				<!-- e_right_m -->
	
			</div>
			<!-- e_contents -->
		</div>
		<!-- e_container_table -->
	
		<!-- s_ 팝업 : 예약신청 -->
		<div id="m_pop1" class="pop_up_bk mo1">
			<!-- s_pop_wrap -->
			<div class="pop_wrap">
				<button class="pop_close close1" type="button">X</button>
	
				<p class="t_title">예약신청</p>
	
				<!-- s_pop_swrap -->
				<div class="pop_swrap">
	
					<div class="swrap">
						<span class="tt">기간<span class="t_1">( 필수 )</span></span>
						<div class="twrap1">
							<input type="text" class="dateyearpicker-input_1 ip_date" name="start_date" value="">
						</div>
						<div class="twrap2">
							<span class="sign2">~</span>
						</div>
						<input type="text" class="dateyearpicker-input_1 ip_date" name="end_date" value="">
					</div>
	
					<div class="swrap">
						<span class="tt">시간<span class="t_1">( 필수 )</span></span>
	
						<div class="cwrap">
							<span class="tt2">종일</span> <input type="checkbox" class="chk" name="all_day_flag">
						</div>
	
						<input type="text" class="timepicker1 ip_time" value="" name="start_time"> 
						<span class="sign">~</span> 
						<input type="text" class="timepicker2 ip_time" value="" name="end_time">
					</div>
	
					<div class="swrap">
						<span class="tt">중요도<span class="t_1">( 필수 )</span></span>
	
	
						<div class="uselectbox" id="important_level">
							<span class="uselected" data-value="">선택</span> <span class="uarrow">▼</span>
	
							<div class="uoptions">
								<span class="uoption firstseleted" data-value="1">보통</span>
								<span class="uoption" data-value="2">중요</span>
								<span class="uoption" data-value="3">매우 중요</span>
							</div>
						</div>
	
					</div>
	
					<div class="swrap">
						<span class="tt">시설<span class="t_1">( 필수 )</span></span>
	
						<div class="uselectbox" id="facility_code">
							<span class="uselected" data-value="">선택</span> <span class="uarrow">▼</span>	
							<div class="uoptions">
								<span class="uoption firstseleted" data-value="">선택</span>
                                <c:forEach var="facilityList" items="${facilityList}">
                                    <span class="uoption" data-value="${facilityList.code}">${facilityList.code_name}</span>
                                </c:forEach>
							</div>
						</div>	
					</div>
	
					<div class="swrap">
						<span class="tt">제목<span class="t_1">( 필수 )</span></span> 
						<input type="text" class="ip_tt" placeholder="제목을 입력하세요." name="title">
					</div>
	
					<div class="swrap tarea free_textarea">
						<span class="tt t_a">내용 및 비고 사항<br>( 선택 )</span>
						<textarea class="tarea1" style="height: 40px;" name="content"></textarea>
					</div>
	
					<div class="swrap">
						<span class="tt">연락처<span class="t_1">( 필수 )</span></span> 
						<input type="text" class="ip_tt_s1" maxlength="3" name="tel1"> 
						<span class="sign2">-</span> 
						<input type="text" class="ip_tt_s1" maxlength="4" name="tel2"> 
						<span class="sign2">-</span> 
						<input type="text" class="ip_tt_s1" maxlength="4" name="tel3">
					</div>
	
					<span class="tts">대관신청을 하였더라도 <span class="t_1">관리자의
							승인 절차가 없을 경우 그 효력이 없으므로,</span> <br>승인 여부를 <span class="t_2">반드시
							확인</span>하시기 바랍니다.
					</span>
	
				</div>
				<!-- e_pop_swrap -->
				<div class="t_dd">
	
					<div class="pop_btn_wrap">
						<button type="button" class="btn01" name="saveBtn" onclick="submitReserve();">신청등록</button>
						<button type="button" class="btn02" onclick="$('.close1').click();">취소</button>
					</div>
	
				</div>
			</div>
		</div>
		<!-- e_pop_wrap -->
		<!-- e_ 팝업 : 예약신청 -->	
	</body>
</html>