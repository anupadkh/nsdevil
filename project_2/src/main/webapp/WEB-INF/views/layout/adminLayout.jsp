<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="Description" content="의과대학 LMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="width=1100">
		
		<title>MLMS</title>
		
		<link rel="icon" type="img/png" href="${IMG}/favicon.png">
		<link rel="shortcut icon" href="${IMG}/favicon.png">
		<link rel="icon" href="${IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${IMG}/favicon.ico">
		<link rel="stylesheet" href="${CSS}/admin_style.css" type="text/css">
		<link rel="stylesheet" href="${CSS}/datetime_1.css" type="text/css" >
		<link rel="stylesheet" href="${CSS}/jquery.ui.datepicker.min.css" type="text/css" >
		<link rel="stylesheet" href="${CSS}/dev_admin_style.css" type="text/css">
		<script src="${JS}/lib/jquery-1.11.1.min.js"></script>
        <script src="${JS}/lib/datetimep_1.js"></script>
		<script src="${JS}/lib/timepicki.js"></script>
		<script src="${JS}/lib/monthdatepicker_ui.js"></script>
        <script src="${JS}/lib/jquery.form.min.js"></script>
		<script src="${JS}/lib/autosize.min.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/lib/jquery.ui.accordion.js"></script>
        <script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.ui-1.10.4.datepicker.min.js"></script>
		<script src="${JS}/common.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				topPopupInit();
				initPageTopButton();
				customSelectBoxInit();
				adminNoticeLeftMenuInit();
				$(document).mouseup(function(e){
					layerPopupCloseInit(['div[name="myProfilePopupDiv"]']);
				});

            	$('.timepicker1').timepicki();
		    	$('.timepicker2').timepicki({custom_classes:"time2"});
		    	
				$.datetimepicker.setLocale('kr');
			    $('.dateyearpicker-input_1').datetimepicker({
			        timepicker:false,
			        datepicker:true,
			        format:'y-m-d',
			        formatDate:'y-m-d'
			    });
			    
			    $(".btn_l").bind("click", function(){
			        $(".btn_l").toggleClass("move-trigger_l");
			        $(".aside_l").toggleClass("folding");
			        $(".main_con").toggleClass("");
			    });
			    
			    boardMenuSetting('${sessionScope.S_BOARD_MENU_LIST}');
			    $(function() {
					$( ".boardwrap" ).accordion({
						collapsible: true,
						active: false
					});
				});
			});
			
			function boardMenuSetting(list) {
				var boardMenuAreaHtml = "";
				boardMenuAreaHtml += '<div class="tt">게시판 관리</div>';
				boardMenuAreaHtml += '<a href="${HOME}/admin/board/list" class="tt_s">전체게시판 관리</a>';
				boardMenuAreaHtml += '<div class="boardwrap">';
				boardMenuAreaHtml += '    <div class="title adiv1"><span class="sp_tt">주요관리게시판</span></div>   ';
				boardMenuAreaHtml += '    <ul class="panel panel1" id="mainMenuArea"></ul>';
				boardMenuAreaHtml += '    <div class="title adiv2"><span class="sp_tt">학습자료실</span></div>';
				boardMenuAreaHtml += '    <ul class="panel panel2" id="learningMenuArea"></ul>';
				boardMenuAreaHtml += '    <div class="title adiv3"><span class="sp_tt">학교생활</span></div>';
				boardMenuAreaHtml += '    <ul class="panel panel3" id="sLifeMenuArea"></ul>';
				boardMenuAreaHtml += '</div>';
				
				$("#boardMenuArea").html(boardMenuAreaHtml);
				var boardMenuList = $.parseJSON(list);
			    $(boardMenuList).each(function() {
			    	var code = this.board_menu_code;
			    	var boardName = this.board_name;
			    	var mainBoardFlag = this.main_board_flag;
			    	var nowMenuYN = this.now_menu_yn;
			    	var url = this.url;
			    	var onTag = "";
			    	if (nowMenuYN == "Y") {
			    		onTag = " on";
			    		$("#boardTitleValue").html(boardName+$("#boardTitleValue").text());
			    	}
			    		
			    	var menuHtml = '<li class="wrap"><a href="${HOME}' + url + '" class="tt_1' + onTag + '">' + boardName + '</a></li>';
			    	if (mainBoardFlag == "Y") {
			    		$("#mainMenuArea").append(menuHtml);
			    	}
			    	
			    	if (code == "00") {
			    		$("#learningMenuArea").append(menuHtml);
			    	}
			    	
			    	if (code == "01") {
			    		$("#sLifeMenuArea").append(menuHtml);
			    	}
			    });
			}
			
			//이용자관리
			function searchUser() {
				var type = $("#userTypeSelectBox").attr("value");
				var searchUserName = $("#searchUserName").val();
				if (type == "PROFESSOR") {
					location.href="${HOME}/admin/user/pf/list?user_name="+searchUserName;
				} else if (type == "STUDENT") {
					location.href="${HOME}/admin/user/st/list?user_name="+searchUserName;
				} else if (type == "STAFF") {
					location.href="${HOME}/admin/user/staff/list?user_name="+searchUserName;
				}
			}
		</script>
		<sitemesh:write property='head'/>
	</head>

	<body>
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
		
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a>
			<a href="#container">컨텐츠 바로가기</a>
		</div>
		
		<div id="wrap">
			<div id="top_pop">
				<div class="wrap">
					<div class="slide-wrap">
						<ul>
							<li class="tt"><img src="${IMG}/pop001.png" alt="의과대학 의학교육 평가인증 인증 획득 4년"></li>
						</ul>
					</div>
				</div>
			</div>
			
			<header class="header">
				<div class="header_top">
					<div class="top_wrap">       
						<div class="top_m">
<!-- 							<a href="javascript:void(0);" id="topPopupBtn" class="a_m a1" onclick="javascript:topPopupToggle();">팝업닫기</a> -->
							<c:choose>
								<c:when test='${HOME ne ""}'>
									<a href="${HOME}" class="a_m a3">HOME</a>
								</c:when>
								<c:otherwise>
									<a href="/" class="a_m a3">HOME</a>
								</c:otherwise>
							</c:choose>
<!-- 							<a href="#" class="a_m a4">사이트맵</a> -->
							<c:choose>
								<c:when test="${sessionScope.S_USER_ID ne null}">
									<a href="${HOME}/logout" class="a_m a5">로그아웃</a>
								</c:when>
								<c:otherwise>
									<a href="${HOME}/login" class="a_m a5">로그인</a>
								</c:otherwise>
							</c:choose>
							<a href="${HOME_URL }" class="a_m a6">기관Home</a>
						</div>
						<c:if test="${sessionScope.S_USER_ID ne null}">
							<div class="a_mp a7" onclick="myProfilePopup()">
								<c:choose>
									<c:when test='${sessionScope.S_USER_PICTURE_PATH ne null and sessionScope.S_USER_PICTURE_PATH ne ""}'>
										<span class="pt01"><img src="${sessionScope.S_USER_PICTURE_PATH}" alt="나의 대표 이미지" class="pt_img"></span>
									</c:when>
									<c:otherwise>
										<span class="pt01"><img src="${DEFAULT_PICTURE_IMG}" alt="나의 대표 이미지" class="pt_img"></span>
									</c:otherwise>
								</c:choose>
								<span class="ssp1">${sessionScope.S_USER_NAME} (${sessionScope.S_USER_DEPARTMENT_NAME})</span>
							</div>
							<div id="pop_pop9" name="myProfilePopupDiv">
								<div class="spop9 show" id="spop9">
									<span class="tt">${sessionScope.S_USER_NAME} (${sessionScope.S_USER_DEPARTMENT_NAME})</span>
									<span class="tt">${sessionScope.S_USER_EMAIL}</span>
									<button onclick="location.href='${HOME}/common/SLife/qna/list'" class="btn btn05">1:1 문의</button>
									<span onclick="$('#pop_pop9').hide();" class="pop_close_s" title="닫기">X</span>
								</div>
							</div>
						</c:if>
					</div>
				</div>     
				
				<div class="gnbwrap">
					<div class="gnb_swrap">
						<h1 class="logo">
						<c:choose>
							<c:when test='${HOME ne ""}'>
								<a href="${HOME}" title="홈으로 가기"><img src="${TOP_LOGO}" alt="의과대학 로고"></a>
							</c:when>
							<c:otherwise>
								<a href="/" title="홈으로 가기"><img src="${TOP_LOGO}" alt="의과대학 로고">	</a>
							</c:otherwise>
						</c:choose>
						</h1>
					</div>
					
					<div class="gnb_fwrap">
						<div class="gnb_swrap2">
							<ul id="gnb" class="gnb">
								<c:forEach items="${sessionScope.S_ALLOW_MENU_LIST}" var="allowMenuList" step="1">
									<li><a href="${HOME}${allowMenuList.url}" <c:if test='${allowMenuList.now_menu_yn eq "Y" && allowMenuList.url ne ""}'>class="on"</c:if>>${allowMenuList.name}</a></li>
								</c:forEach>
							</ul>
						</div>
					</div>
				</div>
			</header>
			
			<sitemesh:write property='body'/>
			
			<a href="../#" id="backtotop" title="To top" class="totop" ></a>
			
			<footer id="footer">
		        <div class="footer_con">           
		            <div class="footer_scon">   
		                <ul class="f_menu">
						    <li><a href="#" title="새창" class="fmn1">개인정보처리방침</a></li>
		                    <li><a href="#" title="새창" class="fmn2"><span>|</span>이메일무단수집거부</a></li>                
					    </ul>   
		                <p class="addr">${ADDRESS }</p>
		                <a href="${HOME}" title="홈으로 가기" class="logo_wrap"><img src="${BOTTOM_LOGO}" alt="의과대학 로고" class="b_logo"></a>
		            </div>
		        </div>
			</footer>
			
		</div>
	</body>
</html>