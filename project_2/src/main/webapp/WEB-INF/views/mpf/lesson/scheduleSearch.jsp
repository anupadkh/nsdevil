<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<script type="text/javascript">
	$(document).ready(function() {
		$("body").addClass("search");
		getScheduleSearch("${S_CURRICULUM_SEQ}");
	});
	
	//시간표 목록
	function getScheduleSearch(curr_seq) {
		
		if (curr_seq == "") {
			alert("교육과정을 선택해 주세요.");
			history.back(-1);
			return;
		}
		
	    $.ajax({
	        type: "POST",
	        url: "${HOME}/ajax/pf/lesson/getSchedule",
	        data: {
	        	"curr_seq": curr_seq,
	        	"search_keyword": $.trim($(":input[name='search_keyword']").val())
	        },
	        dataType: "json",
	        success: function(data, status) {
	        	var html = "";
				
	        	$("span[data-name='curr_name']").html(data.basicInfo.curr_name);
	        	$("span[data-name='aca_system_name']").html("("+ data.basicInfo.aca_system_name + ")");
	        	$("#searchCount").html(data.schedule.length);
	        	var prevTitleDay = "";
	        	
	            $.each(data.schedule, function() {
	            	
	            	if (prevTitleDay != this.lesson_date) {
	            		prevTitleDay = this.lesson_date.toLocaleString("ko-KR", {  weekday: "long" });
	            		html += '<tr><td colspan="3" class="th02 bd01">' + this.lesson_date + ' (' + this.lesson_date.toLocaleString("ko-KR", {  weekday: "long" }) + ')</td></tr>';
	            	}
	            	
	            	var userPic = "${IMG}/ph_3.png";
	            	
	            	if (this.picture_path != null) {
	            		userPic = "${RES_PATH}"+this.picture_path;
	            	}
	            	
	            	var lesson_subject = this.lesson_subject;
	            	var nonregiClass = "";
	            	
	            	if (lesson_subject == "") {
	            		nonregiClass = "nonregi";
	            		lesson_subject = "(미등록)";
	            	}
	            	
	            	var department = "";
	            	if (this.department != "") {
	            		department = "(" + this.department + ")";
	            	}
	            	
	            	var lessonTime = "";
	            	
	            	if (this.start_time_12h != null) {
	            		lessonTime = this.start_time_12h;
	            		if (this.end_time_12h != null) {
	            			lessonTime += lessonTime + " ~ " + this.end_time_12h;
	            		}
	            	}
	            	
	            	html += '<tr>'
						+ '<td class="td_1 w1">'
						+ '	<span class="wrap_num">' + this.period_seq + '</span>'
						+ '	<span class="tt01">' + this.period + '교시</span>'
						+ '</td>'
						+ '<td class="td_1 w2">'
						+ '	<div class="tt_wrap_s">'
						+ '	<span class="tt ' + nonregiClass + '">' + lesson_subject + '</span>'
						+ '	<span class="tt_dt">' + lessonTime + '</span>'
						+ '	</div>'
						+ '</td>'
						+ '<td class="td_1 w3"><span class="a_mp"><span class="pt01"><img src="' + userPic + '" alt="사진" class="pt_img"></span><span class="ssp1">' + this.pf_name + '</span><span class="ssp2">' + department + '</span></span></td>'
	                	'</tr>';
	            });
	            $("#scheduleSearchList").html(html);
	            
	            if (data.schedule.length == 0) {
		        	$(".mlms_tb_wrap .mlms_tb1").addClass("class_x");
		        	$(".mlms_tb_wrap .mlms_tb1").html("<tr><td></td></tr>");
		        	$("#regButton").html("<button class=\"bt_3a\" onclick=\"location.href='./scheduleMod'\">등록</button>");
	            }
	            
	            bindPopupEvent("#m_pop1", ".open1");
	        },
	        error: function(xhr, textStatus) {
	            alert("오류가 발생했습니다.");
	            //document.write(xhr.responseText);
	        }
	    });
	}
	
	//엔터키 검색
	function startSearch(event) {
		if (event.keyCode == 13) {
			getScheduleSearch("${S_CURRICULUM_SEQ}");
		}
	}
</script>
</head>

<body class="search">

 <!-- s_tt_wrap -->                
<div class="tt_wrap">
    <h3 class="am_tt">
    <span class="tt" data-name="curr_name"></span><span class="tt_s" data-name="aca_system_name"></span>
    </h3>                    
</div>
<!-- e_tt_wrap -->
    
<!-- s_tab_wrap_cc --> 
<div class="tab_wrap_cc">
<!-- s_해당 탭 페이지 class에 active 추가 --> 
	    <button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson'">교육과정계획서</button>	         
		<button class="tab01 tablinks active" onclick="location.href='${HOME}/pf/lesson/schedule'">시간표관리</button>	
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/unit'">단원관리</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/lessonPlanRegCs'">단위 수업계획서</button>
		<button class="tab01 tablinks" onclick="">만족도 조사</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/currAssignMent'">종합 과제</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/soosiGrade'">수시 성적</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/grade'">종합 성적</button>	
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/report'">운영보고서</button>
<!-- e_해당 탭 페이지 class에 active 추가 -->
</div>
<!-- e_tab_wrap_cc -->  
 
<!-- s_mpf_tabcontent2 --> 
 <div class="mpf_tabcontent2">
<!-- s_tt_wrap -->                
<div class="tt_wrap">
    <button class="btn_dw1 schd_mv" onclick="location.href='./scheduleM'">시간표 월별 보기</button>
<!-- s_sch_wrap -->
<div class="sch_wrap">

<input type="search" name="search_keyword" class="ip_search" value="" onkeyup="startSearch(event)">
<button class="btn_search2" onclick="getScheduleSearch(${S_CURRICULUM_SEQ})">검색</button>
            
</div>
<!-- e_sch_wrap -->                       
</div>
<!-- e_tt_wrap -->
 
<!-- s_mlms_tb1 -->                                                          
                   <table class="mlms_tb1 sch top">
                        <tr>
                            <td class="th01 bd01">
                            <div class="sch_wrap_s">총<span id="searchCount" class="num">15</span>건 검색</div>
                            </td>
                        </tr>
                   </table>
                   
             <div class="wrap_schtb">      
                   <table class="mlms_tb1 sch">
                   <tbody id="scheduleSearchList"></tbody>
                   </table>
             </div>
<!-- e_mlms_tb1 -->

</div>
<!-- e_mpf_tabcontent2 -->

</body>
</html>