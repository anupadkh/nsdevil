<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<script src="${JS}/schedule.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("body").addClass("full_schd");
		
		$('.free_textarea').on( 'keyup', 'textarea', function (e){
			$(this).css('height', 'auto' );
			$(this).height( this.scrollHeight );
		});
		$('.free_textarea').find( 'textarea' ).keyup();

		getSchedule("${S_CURRICULUM_SEQ}","${RES_PATH}");
	});
	
	//삭제 후 재등록
	function deleteAndRegister(curr_seq) {
		if($.inArray("${acaState.aca_state}",["01","02"]) == -1){
    		alert("이미 학기가 시작된 교육과정입니다.\n관리자에게 문의해주세요.");
    		return;            		
    	}
    	
    	if("${currFlagInfo.curr_confirm_flag}"=="Y"){
    		alert("해당 교육과정은 확정 상태입니다.\n관리자에게 문의해주세요.");
    		return;
    	}
    	
		if(!confirm("입력한 시간표가 삭제됩니다.\n계속 진행하시겠습니까?")) {
			return;
		}
		$.ajax({
	        type: "POST",
	        url: "${HOME}/ajax/pf/lesson/deleteAllSchedule",
	        data: {
	        	"curr_seq": curr_seq
	        },
	        dataType: "json",
	        success: function(data, status) {
	        	if (data.status == "200") {
	        		location.href="./scheduleMod";
				} else {
					alert("오류가 발생했습니다.");
				}
	        },
	        error: function(xhr, textStatus) {
	            alert("오류가 발생했습니다.");
	        }
	    });
	}
	
</script>
</head>

<body class="full_schd">

 <!-- s_tt_wrap -->                
<div class="tt_wrap">
    <h3 class="am_tt">
    <span class="tt" data-name="curr_name"></span><span class="tt_s" data-name="aca_system_name"></span>
    </h3>                    
</div>
<!-- e_tt_wrap -->
    
<!-- s_tab_wrap_cc --> 
<div class="tab_wrap_cc">
<!-- s_해당 탭 페이지 class에 active 추가 --> 
			<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson'">교육과정계획서</button>	         
		<button class="tab01 tablinks active" onclick="location.href='${HOME}/pf/lesson/schedule'">시간표관리</button>	
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/unit'">단원관리</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/lessonPlanRegCs'">단위 수업계획서</button>
		<button class="tab01 tablinks" onclick="">만족도 조사</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/currAssignMent'">종합 과제</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/soosiGrade'">수시 성적</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/grade'">종합 성적</button>	
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/report'">운영보고서</button>
<!-- e_해당 탭 페이지 class에 active 추가 -->
</div>
<!-- e_tab_wrap_cc -->  
 
<!-- s_mpf_tabcontent2 --> 
 <div class="mpf_tabcontent2">
<!-- s_tt_wrap -->                
<div class="tt_wrap">
    <button class="btn_dw1" onclick="location.href='${HOME}/pf/lesson/scheduleExcelDown'">시간표 다운로드</button>
<!-- s_sch_wrap -->
<div class="sch_wrap">
<!-- s_wrap_p1_uselectbox -->
<!--
                     <div class="wrap_p1_uselectbox">
		                        <div class="uselectbox">
		                                <span class="uselected">전체 교육과정</span>
		                                <span class="uarrow">▼</span>
			
		                            <div class="uoptions">
		                                    <span class="uoption opt1 firstseleted">전체 교육과정</span>
		                                    <span class="uoption opt2 ">인체의 구조 Ⅰ</span>
		                                    <span class="uoption opt3 ">호흡기학</span>
		                                    <span class="uoption opt4 ">소화기학 Ⅱ</span>
		                                    <span class="uoption opt5 ">교육과정</span>
		                                    <span class="uoption opt6 ">교육과정</span>
		                                    <span class="uoption opt7 ">교육과정</span>
		                                    <span class="uoption opt8 ">교육과정</span>
		                            </div>
		                        </div>  								
                       </div>
-->
<!-- e_wrap_p1_uselectbox -->

<button onclick="createLesson();" class="btn_view full">수업 등록</button>
<button onclick="location.href='./scheduleM'" class="btn_view full">달력 보기</button>
<button onclick="location.href='./scheduleSearch'" class="btn_search1" title="검색"></button>                
            
</div>
<!-- e_sch_wrap -->
</div>
<!-- e_tt_wrap -->


 <div class="mlms_tb_wrap">         
<!-- s_mlms_tb1 -->                                                          
<table class="mlms_tb1">
                    <thead>
                        <tr>
                            <th class="th01 bd01 w1">차시</th>
                            <th class="th01 bd01 w2">날짜</th>
                            <th class="th01 bd01 w2">교시</th>
                            <th class="th01 bd01 w4">교수명</th>
                            <th class="th01 bd01 w4_1">수업주제</th>
                            <!-- <th class="th01 bd01 w3">수업방법</th> -->
                            <th class="th01 bd01 w2">관리</th>
                        </tr>
                    </thead>
                    <tbody id="scheduleList"></tbody>
<%--
                        <tr>
                            <td class="td_1"><span class="tt01">1</span></td>
                            <td class="td_1"><span class="tt_t">8 / 25</span></td>
                            <td class="td_1"><span class="tt01">1</span></td>
                            <td class="td_1 t_l"><span class="a_mp"><span class="pt01"><img src="${IMG}/ph_r1.png" alt="사진" class="pt_img"></span><span class="ssp1">교수명 (내과)</span></span></td>
                            <td class="td_1 t_l">해부학 수업</td>
<!-- 수업방법 - sp01 : 강의, sp02 : 형성, sp03 : 실습 --> 
                            <td class="td_1"><span class="sp sp01">강의</span></td>
                            <td class="td_1"><button class="btn_mdf open1">수정</button></td>
                        </tr>
                        <tr>
                            <td class="td_1"><span class="tt01">5</span></td>
                            <td class="td_1"><span class="tt_t">8 / 25</span></td>
                            <td class="td_1"><span class="tt01">1</span></td>
                            <td class="zero01 bd01 t_l"><span class="a_mp"><span class="pt01"><img src="${IMG}/ph_3.png" alt="사진" class="pt_img"></span><span class="ssp1">홍길동 (내과)</span></span></td>
<!-- 미등록 시  class : nonregi 추가 -->
                            <td class="td_1 t_l nonregi">호흡기학 (미등록)</td>
                            <td class="td_1"></td>
                            <td class="td_1"><button class="btn_mdf open1">수정</button></td>
                        </tr>
--%>
</table>
<!-- e_mlms_tb1 -->
</div>


<div class="bt_wrap" id="regButton">
    <!--<button class="bt_3a" onclick="location.href='./scheduleMod'">등록</button>-->
    <button class="bt_3" onclick="javascript:deleteAndRegister('${S_CURRICULUM_SEQ}');">전체삭제 - 재등록</button>
</div>
</div>
<!-- e_mpf_tabcontent2 -->
<jsp:include page="schedulePopup.jsp">
	<jsp:param name="pageName" value="schedule" />
</jsp:include>
</body>
</html>