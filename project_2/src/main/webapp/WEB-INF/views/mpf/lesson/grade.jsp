<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<style>
.main .main_con .mpf_tabcontent4 .tab_table.ttb1 .w9_1 {
	width: 9%;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		bindPopupEvent("#m_pop1", ".open1");
		bindPopupEvent("#m_pop2", ".open2");
		getStList("id");	
	});
	
	function gradeNumCheck(obj){
		
		
		if(isNaN(obj.value) || isEmpty(obj.value)){
			$(obj).val("");
			$(obj).focus("");
		}else{			
			$(obj).val($(obj).val().replace(/ /g,""));
		}

		var total = 0;
		
		$.each($(obj).closest("tr").find("td[name=scoreTD]"), function(){
			if(isNaN($(this).find("input").val()) || isEmpty($(this).find("input").val())){
				total += 0;
			}else{			
				total += parseFloat($(this).find("input").val());
			}
		});
		
		if(total > 100){
			alert("합계 점수가 100점을 넘을 수 없습니다.");
			$(obj).val("");
			$(obj).focus("");
			return;
		}
		$(obj).closest("tr").find("td[name=totalScore]").text(total);
	}
	
	function getStList(order_column) {

		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/grade/getStList",
            data: {
            	"order" : order_column
            	,"curr_seq" : "${S_CURRICULUM_SEQ}"
            	,"st_name" : $("#st_name").val()            	
            },
            dataType: "json",
            success: function(data, status) {
            	
            	var st_cnt = data.gradeCount.st_total_cnt;
            	if(data.subjectList.length!=0){
            		$("button[name=saveBtn]").text("수정");
            	}
            	var htmls1 = '<tr class="tr01"><td class="th01 w9_1" style="width:10%;"></td>';
            	var htmls2 = '<tr class="tr01"><td class="bd01 ta_c">가이드<br>비중(%)</td>';
            	var htmls3 = '<tr class="tr01" data-name="guideTr"><td class="bd01 ta_c">가이드<br>명수</td>';
            	var htmls4 = '<tr class="tr01" data-name="realTr"><td class="bd01 ta_c">실제<br>학생 수</td>';
            	var htmls5 = '<tr class="tr01" data-name="realPercentTr"><td class="bd01 ta_c">실제<br>비중(%)</td>';
            	var percent = 0;
            	var guide_stcnt = 0;
            	
            	$.each(data.gradeCodeList, function(index){
            		htmls1 += '<td class="th01 w9_1">'+this.grade_code+'</td>';
            		htmls2 += '<td class="bd01 ta_c" data-name="guidePercentTd"><input type="text" name="'+this.grade_code+'" value="'+this.grade_percent+'" onkeyup="percentCnt(this);" maxlength="4" style="width:95%;text-align:center;"></td>';
            		htmls3 += '<td class="bd01 ta_c" data-name="guideTd"><input type="text" name="'+this.grade_code+'" value="'+st_cnt*this.grade_percent/100+'" style="width:95%;text-align:center;" maxlength="4"></td>';
            		htmls4 += '<td class="bd01 ta_c" data-name="realTd"><span class="sp02_1 open2" onClick="getGradeStList(\''+this.grade_code+'\');" data-name="'+this.grade_code+'"></span></td>';
            		htmls5 += '<td class="bd01 ta_c" data-name="'+this.grade_code+'"></td>';
            		percent += this.grade_percent;
            		guide_stcnt += st_cnt*this.grade_percent/100;
            	});
            	
            	htmls1 += '<td class="th01 w9_2">계</td></tr>';
        		htmls2 += '<td class="bd01 ta_c" data-name="guidePercentTotal">'+percent+'</td></tr>';
        		htmls3 += '<td class="bd01 ta_c">'+guide_stcnt+'</td></tr>';
        		htmls4 += '<td class="bd01 ta_c" data-name="total"></td></tr>';
        		htmls5 += '<td class="bd01 ta_c">'+Math.round(data.gradeCount.submit_grade_cnt/data.gradeCount.st_total_cnt*10000)/100+'</td></tr>';
            	
        		$("#guideTrAdd").html(htmls1+htmls2+htmls3+htmls4+htmls5);
        		
            	$("span[data-name='curr_name']").html(data.basicInfo.curr_name);
            	$("span[data-name='aca_system_name']").html("("+ data.basicInfo.aca_system_name + ")");
            	
            	$("span[data-name=A\\+]").text(data.gradeCount.ap);
            	$("td[data-name=A\\+]").text(Math.round(data.gradeCount.ap/st_cnt*10000)/100);
            	
            	$("span[data-name=A]").text(data.gradeCount.a);
            	$("td[data-name=A]").text(Math.round(data.gradeCount.a/st_cnt*10000)/100);
            	
            	$("span[data-name=B\\+]").text(data.gradeCount.bp);
            	$("td[data-name=B\\+]").text(Math.round(data.gradeCount.bp/st_cnt*10000)/100);
            	
            	$("span[data-name=B]").text(data.gradeCount.b);
            	$("td[data-name=B]").text(Math.round(data.gradeCount.b/st_cnt*10000)/100);
            	
            	$("span[data-name=C\\+]").text(data.gradeCount.cp);
            	$("td[data-name=C\\+]").text(Math.round(data.gradeCount.cp/st_cnt*10000)/100);
            	
            	$("span[data-name=C]").text(data.gradeCount.c);
            	$("td[data-name=C]").text(Math.round(data.gradeCount.c/st_cnt*10000)/100);
            	
            	$("span[data-name=D\\+]").text(data.gradeCount.dp);
            	$("td[data-name=D\\+]").text(Math.round(data.gradeCount.dp/st_cnt*10000)/100);
            	
            	$("span[data-name=D]").text(data.gradeCount.d);
            	$("td[data-name=D]").text(Math.round(data.gradeCount.d/st_cnt*10000)/100);
            	
            	$("span[data-name=F]").text(data.gradeCount.f);
            	$("td[data-name=F]").text(Math.round(data.gradeCount.f/st_cnt*10000)/100);
            	
            	$("td[data-name=total]").html('<span class="sp_t">'+data.gradeCount.submit_grade_cnt+'</span><span>/</span><span class="sp_t">'+data.gradeCount.st_total_cnt+'</span>');
            	
            	
            	
            	var htmls = '<tr>'            	
            		+'<th class="color1 th01 b_2 w2">No.</th>'
            		+'<th class="color1 th01 bd01 w4">학번</th>'
            		+'<th class="color1 th01 bd01 w5">학생</th>'
            		+'<th class="color1 th01 bd01 w1">조</th>';
				$.each(data.subjectList,function(index){
					htmls+= '<th class="color2 th01 bd01 w3">'+this.subject+'</th>';
				});
    	        
				htmls+='<th class="color3 th01 bd01 w2">총점</th>'
	    	        +'<th class="color3 th01 bd01 w2">석차</th>'
	    	        +'<th class="color4 th01 bd01 w3">확정<br>등급</th>'
	    	        +'<th class="color5 th01 b_1 w3">변환<br>점수</th>'
	    	        +'</tr>';
            	
	    	    $("#table_header").html(htmls);
	    	   	
	    	    htmls = "";
	    	        
	    	    $.each(data.stList, function(index){
            		var score = this.score.split("\|");
            		var subject_seq = this.subject_seq.split("\|");
            		var totalScore = 0;
            		var picture_path = this.picture_path ;
            		var group_number = this.group_number;
            		if(group_number == 0){
            			group_number = "-";
            		}
            		if(isEmpty(picture_path))
            			picture_path = "${IMG}/ph_3.png";
            			
            			htmls += '<tr data-name="'+this.id+'">'
	                    + '<td class="td_1">' + (index + 1) + '</td>'
	                    + '<td class="td_1">' + this.id + '<input type="hidden" name="id" value="'+this.id+'"/></td>'
	                    + '<td class="td_1 t_l"><span class="a_mp"><span class="pt01"><img src="'+picture_path+'" alt="등록된 사진 이미지" class="pt_img"></span><span class="ssp1">' + this.name + '</span></span></td>'
	                    + '<td class="td_1">'+group_number+'</td>';
	                    
					if(!isEmpty(score)){
						for(var i=0; i<score.length;i++){
							htmls +='<td class="td_1" name="scoreTD"><input type="text" class="ip01 keyupInput" onKeyup="gradeNumCheck(this);" name="'+subject_seq[i]+'" value="'+score[i]+'"></td>';
							totalScore += parseFloat(score[i]);							
						}							               
					}else{
						for(var i=0; i<subject_seq.length;i++){
							htmls +='<td class="td_1" name="scoreTD"><input type="text" class="ip01 keyupInput" onKeyup="gradeNumCheck(this);" name="'+subject_seq[i]+'" value="0"></td>';
							totalScore = 0;				
						}
					}
	                   
	               htmls += '<td class="td_1" name="totalScore">'+this.total_score+'</td>'
	                    + '<td class="td_1" name="rank">'+this.rank+'</td>'
	                    + '<td class="td_1"><input type="text" class="ip02" name="final_grade" value="'+this.final_grade+'" onkeyup="setStGrade(this);"></td>'
	                    + '<td class="td_1" name="change_score">'+this.change_score+'</td>'
	                    + '</tr>';
            	});
            	$("#stList").html(htmls);
            },
            error: function(xhr, textStatus) {
                alert("오류가 발생했습니다.");
            }
        });
	}
	
	function percentCnt(obj){
		if($.isNumeric($(obj).val())){
			$(obj).val($(obj).val());
		}else if(isEmpty($(obj).val())){
				
		}else{
			$(obj).val("");
			alert("숫자만 입력해주세요.");			
			return;
		}
		
		var total = 0;
		
		$.each($("td[data-name=guidePercentTd]"), function(index){
			var percent = $(this).find("input").val();
			if($.isNumeric(percent)){
				total+=parseFloat(percent);			
			}
			var guideStCnt = $("#stList tr").length*percent/100;
			$("td[data-name=guideTd]").eq(index).find("input").val(guideStCnt);
		});
		
		if(total > 100){
			alert("비율의 합이 100이 넘습니다.");
			$(obj).val("");
			
			total = 0;
			
			$.each($("td[data-name=guidePercentTd]"), function(index){
				var percent = $(this).find("input").val();
				if($.isNumeric(percent)){
					total+=parseFloat(percent);			
				}
				var guideStCnt = $("#stList tr").length*percent/100;
				$("td[data-name=guideTd]").eq(index).find("input").val(guideStCnt);
			});
		}
		
		$("td[data-name=guidePercentTotal]").text(total);	
	}	
	
	function setStGrade(obj){
		var grade = ["A+","A","B+","B","C+","C","D+","D","F"];
		if(isEmpty($(obj).val())){
			
		}else if($.inArray($(obj).val(), grade) > -1){
					
		}else{
			$(obj).val("");
			alert("A+,A,B+,B,C+,C,D+,D,F 중에 입력해주세요");
		}
		
		for(var i=0;i<grade.length;i++){
			var thisGradeCnt = 0;
			$.each($("#stList input[name=final_grade]"), function(index_input){
				if($(this).val() == grade[i]){
					thisGradeCnt++;
				}						
			});				
			
			$("tr[data-name=realTr]").find("span[data-name='"+grade[i]+"']").text(thisGradeCnt);
		}	
		//비율 계산.
		setRealStCnt();	
	}
	
	function submitGrade(){
		if(!confirm("저장하시겠습니까?"))
			return;
		
		var arrayList = new Array();
		
		$.each($("#stList tr"), function(index){
			var listInfo = new Object();
			var id = $(this).find("input[name=id]").val();
			var subject_seq_list = new Array();
			var final_grade = $(this).find("input[name=final_grade]").val();
			$.each($(this).find("td[name=scoreTD]"), function(index){
				var subject = new Object();
				
				if(!isEmpty($(this).find("input").val())){
					subject.subject_seq = $(this).find("input").attr("name");
					subject.score = $(this).find("input").val();
					subject_seq_list.push(subject);
				}
			});
			listInfo.id = id;
			listInfo.subject_seq_list = subject_seq_list;
			listInfo.final_grade = final_grade;
			arrayList.push(listInfo);
		});
		
		var scoreList = new Object();
		scoreList.list = arrayList;
		
		var jsonInfo = JSON.stringify(scoreList);

		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/grade/getStList/insert",
            data: {   
            	"scorelist" : jsonInfo
            	,"curr_seq" : "${S_CURRICULUM_SEQ}"
            	,"confirm" : "N"
            },
            dataType: "json",
            success: function(data, status) {
            	if(data.status == "200"){
            		getStList($("#sortVal span:eq(0)").attr("data-value"));
	            	alert("저장 완료 되었습니다.");	            	
            	}else{
            		alert("저장 실패 하였습니다.");
            	}
            },
	        beforeSend:function() {
	            $.blockUI();
	        },
	        complete:function() {
	            $.unblockUI();
	        },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                //document.write(xhr.responseText);
            }
        }); 
	}
	
	function getGradeStList(grade){
		
		$("span.sp_nn").text(grade);
		var cnt = 0;
		var htmls = "";
		$.each($("#stList input[name=final_grade]"), function(index_input){
			if($(this).val() == grade){
				htmls += $(this).closest("tr").find("td:eq(2)").html();
				cnt ++;
			}						
		});		
		
		$("span.sp_num").text(cnt);
		$("#gradeStListAdd").html(htmls);        
		
		/* 
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/grade/getGradeStList",
            data: {   
            	"grade" : grade
            	,"curr_seq" : "${S_CURRICULUM_SEQ}"
            },
            dataType: "json",
            success: function(data, status) {
            	if(data.status == "200"){
            		var htmls = "";
            		$.each(data.stList,function(index){
            			var picture_path = this.picture_path;
            			if(isEmpty(picture_path))
            				picture_path = "${IMG}/ph_3.png";
            				
            			htmls += '<div class="a_mp">'
							+'<span class="pt01"><img src="'+picture_path+'" alt="사진" class="pt_img"></span><span class="ssp1">'+this.name+'</span>'
							+'</div>';
            		});
            		$("span.sp_num").text(data.stList.length);
            		$("#gradeStListAdd").html(htmls);           	
            	}else{
            		alert("등급별 학생 불러오기 실패 하였습니다.");
            	}
            },
	        beforeSend:function() {
	            $.blockUI();
	        },
	        complete:function() {
	            $.unblockUI();
	        },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                //document.write(xhr.responseText);
            }
        });		 */
	}
	
	function file_nameChange(){    
	    if($("#xlsFile").val() != ""){
	        var fileValue = $("#xlsFile").val().split("\\");
	        var fileName = fileValue[fileValue.length-1]; // 파일명
	        
	        var fileLen = fileName.length; 
	        var lastDot = fileName.lastIndexOf('.'); //마지막 . 위치 가져온다.
	        var fileExt = fileName.substring(lastDot+1, fileLen).toLowerCase(); //마지막 . 위치로 확장자 자름

	        if(fileExt != "xlsx") {
	        	alert("xlsx 파일을 등록해주세요");
	        	$("#xlsFile").val("");
	        	return;			        	
	        }
	        
	        $("#xls_filename").val(fileName);
	    }
	}
	
	
	function gradeXlsxUp() {
		if (isEmpty($("#xlsFile").val())) {
			alert("파일을 선택해주세요");
			return;
		}

		var form = document.getElementById("xlsForm");
		
		var hiddenField = document.createElement("input");
		
		hiddenField.type = "hidden";
		hiddenField.name = "curr_seq";
		hiddenField.value ="${S_CURRICULUM_SEQ}";
		
		form.appendChild(hiddenField);
		
		$("#xlsForm").ajaxForm({
			type : "POST",
			url : "${HOME}/ajax/pf/lesson/grade/excel/create",
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {
					alert("엑셀업로드가 완료 되었습니다.");
					$("#xlsFile").val("");
					$("#xls_filename").val("");
					getStList($("#sortVal span:eq(0)").attr("data-value"));
				} else {
					alert("엑셀업로드를 실패 하였습니다.\n"+data.msg);
				}
			},
			error : function(xhr, textStatus) {
				alert(textStatus);
				//document.write(xhr.responseText);
				$.unblockUI();
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
		$("#xlsForm").submit();
	}
	
	function excelTmplDown(){
		location.href='${HOME}/pf/lesson/grade/excelTmplDown?curr_seq=${S_CURRICULUM_SEQ}';
	}
	
	function excelDown(){
		location.href='${HOME}/pf/lesson/grade/excelDown?order='+$("#sortVal span:eq(0)").attr("data-value")+'&st_name='+$("#st_name").val()+'&curr_seq=${S_CURRICULUM_SEQ}';	
	}
	
	function stListSort(order){
		var sortData = [];

		$.each($("#stList tr"), function(index){
			
			
			
			var html = $(this);	
			
			var name = "";
			var id = $(this).find("input[name=id]").val();
			var img = $(this).find("img").attr("src");
			var st_name = $(this).find("td:eq(2)").text();
			var group_num = $(this).find("td:eq(3)").text();
			var score = "";
			var subject_seq = "";
			var totalScore = $(this).find("td[name=totalScore]").text();
			var rank = $(this).find("td[name=rank]").text();
			var final_grade = $(this).find("input[name=final_grade]").val();
			var change_score = $(this).find("td[name=change_score]").text();
				
			$.each($(this).find("td[name=scoreTD]"), function(td_index){
				if(td_index != 0){
					score+=",";
					subject_seq+=",";
				}
				
				score+=$(this).find("input").val();
				subject_seq+=$(this).find("input").attr("name");
			});
									
			if(order == "id")
				name = $(this).find("input[name=id]").val();
			else if(order == "group_number"){
				if($(this).find("td:eq(3)").text() == "-")
					name = 9999;
				else
					name = parseInt($(this).find("td:eq(3)").text());
			}else if(order == "total_score"){
				name = parseFloat($(this).find("td[name=totalScore]").text());
			}else if(order == "rank"){
				name = parseInt($(this).find("td[name=rank]").text());
			}else if(order == "grade_order"){
				if(!isEmpty($(this).find("input[name=final_grade]").val()))
					name = $(this).find("input[name=final_grade]").val();
				else
					name = "ZZ";
			}else if(order == "change_score"){
				name = parseFloat($(this).find("td[name=change_score]").text());
			}
			sortData.push({
				key:name
				,st_id:id
				,img_src:img
				,st_name:st_name
				,group_num:group_num
				,score:score
				,subject_seq:subject_seq
				,totalScore:totalScore
				,rank:rank
				,final_grade:final_grade
				,change_score:change_score
			});			
		});

		sortData.sort(function(a, b) {
			var nameA = a.key 
			var nameB = b.key 
			
			if(order == "id" || order == "group_number" || order == "rank"){
				if (nameA < nameB) {
				  return -1;
				}
				if (nameA > nameB) {
				  return 1;
				}
				if (a.totalScore < b.totalScore)
					return 1;
				
				// 이름이 같을 경우
				return 0;
			}else if(order == "grade_order"){
				var grades = ['A+', 'A', 'B+', 'B', 'C+', 'C', 'D+', 'D', 'F'];
				var a_index = grades.indexOf(a.key.toUpperCase());
				var b_index = grades.indexOf(b.key.toUpperCase());
				if (a_index < b_index) {
					return -1;
				}
				if (a_index > b_index) {
					return 1;
				}		
				if (a.totalScore < b.totalScore)
					return -1;	
				
				// 이름이 같을 경우
				return 0;
			} else{
				if (nameA < nameB) {
					return 1;
				}
				if (nameA > nameB) {
					return -1;
				}		
				if (a.totalScore < b.totalScore)
					return 1;	
				
				// 이름이 같을 경우
				return 0;
			}
		});

		$("#stList").empty();
		
		var htmls = '';
		$.each(sortData, function(index){
			var st_score = this.score.split(",");
			var subject_seq = this.subject_seq.split(",");
			
			htmls = '<tr data-name="'+ this.st_id +'">'
                + '<td class="td_1">' + (index + 1) + '</td>'
                + '<td class="td_1">' + this.st_id + '<input type="hidden" name="id" value="'+this.st_id+'"/></td>'
                + '<td class="td_1 t_l"><span class="a_mp"><span class="pt01"><img src="'+this.img_src+'" alt="등록된 사진 이미지" class="pt_img"></span><span class="ssp1">' + this.st_name + '</span></span></td>'
                + '<td class="td_1">'+this.group_num+'</td>';
                
			if(!isEmpty(st_score)){
				for(var i=0; i<st_score.length;i++){
					htmls +='<td class="td_1" name="scoreTD"><input type="text" class="ip01 keyupInput" onKeyup="gradeNumCheck(this);" name="'+subject_seq[i]+'" value="'+st_score[i]+'"></td>';												
				}							               
			}else{
				for(var i=0; i<subject_seq.length;i++){
					htmls +='<td class="td_1" name="scoreTD"><input type="text" class="ip01 keyupInput" onKeyup="gradeNumCheck(this);" name="'+subject_seq[i]+'" value="0"></td>';
					totalScore = 0;				
				}
			}
               
           htmls += '<td class="td_1" name="totalScore">'+this.totalScore+'</td>'
                + '<td class="td_1" name="rank">'+this.rank+'</td>'
                + '<td class="td_1"><input type="text" class="ip02" name="final_grade" value="'+this.final_grade+'" onkeyup="setStGrade(this);"></td>'
                + '<td class="td_1" name="change_score">'+this.change_score+'</td>'
                + '</tr>';
           $("#stList").append(htmls);
		});
	}
	
	function setGuide(){
		
		var total = 0;
		
		$.each($("td[data-name=guidePercentTd]"), function(index){
			var percent = $(this).find("input").val();
			if($.isNumeric(percent)){
				total+=parseFloat(percent);			
			}
		});
		
		if(total != 100){
			alert("가이드 비중의 합이 100%가 아닙니다.");
			return;
		}
		
		var sortData = [];
		
		//정렬을 위해 배열에 담는다.
		$.each($("#stList tr"), function(index){
			
			var id = $(this).find("input[name=id]").val();
			var totalScore = $(this).find("td[name=totalScore]").text();
			
			sortData.push({
				key : id
				,total_score : totalScore
			});			
		});

		//점수별로 정렬
		sortData.sort(function(a, b) {
			var nameA = a.total_score 
			var nameB = b.total_score 
			
			if (nameA < nameB) {
				return 1;
			}
			if (nameA > nameB) {
				return -1;
			}		
			if (a.totalScore < b.totalScore)
				return 1;	
			
			// 이름이 같을 경우
			return 0;
		});
				
		var guideTd = [];
		
		//등급별 가이드 명수 넣기
		$.each($("#guideTrAdd tr[data-name=guideTr] td[data-name=guideTd]"), function(index){
			guideTd.push({
				cnt : Math.round($(this).find("input").val())
				,grade : $(this).find("input").attr("name")
			})			
		});
		
		//기존 입력된 등급 다 공백처리.
		$("input[name=final_grade]").val("");
		
		var guideIndex = 0;		
		var preTotalScore = 0;
		
		$("tr[data-name=realTr] td[data-name=realTd]").find("span").text("0");
		
		for(var i=0;i<sortData.length;i++){
			//등급 명수가 0일 때
			if(guideTd[guideIndex].cnt == 0){
				if(guideTd.length > (guideIndex+1)){
					guideIndex++;
					i-=1;
				}
			}
			var guideCnt = guideTd[guideIndex].cnt;
			var realCnt =  parseInt($("tr[data-name=realTr]").find("span[data-name='"+guideTd[guideIndex].grade+"']").text());
				
			//0점이면 F 
			if(sortData[i].total_score == 0){
				$("#stList tr[data-name="+sortData[i].key+"]").find("input[name=final_grade]").val("F");
				var thisGradeCnt = 0;
				
				$.each($("#stList input[name=final_grade]"), function(index_input){
					if($(this).val() == "F"){
						thisGradeCnt++;
					}						
				});			
				
				$("tr[data-name=realTr]").find("span[data-name='F']").text(thisGradeCnt);
			}
			//가이드 보다 실제명수가 적거나 이전사람과 동점이면
			else if(guideCnt > realCnt || preTotalScore == sortData[i].total_score){				
				$("#stList tr[data-name="+sortData[i].key+"]").find("input[name=final_grade]").val(guideTd[guideIndex].grade);
				var thisGradeCnt = 0;
				
				$.each($("#stList input[name=final_grade]"), function(index_input){
					if($(this).val() == guideTd[guideIndex].grade){
						thisGradeCnt++;
					}						
				});				
				
				$("tr[data-name=realTr]").find("span[data-name='"+guideTd[guideIndex].grade+"']").text(thisGradeCnt);

			}else{
				if(guideTd.length > (guideIndex+1)){
					guideIndex++;
				}
				$("#stList tr[data-name="+sortData[i].key+"]").find("input[name=final_grade]").val(guideTd[guideIndex].grade);
				var thisGradeCnt = 0;
				
				$.each($("#stList input[name=final_grade]"), function(index_input){
					if($(this).val() == guideTd[guideIndex].grade){
						thisGradeCnt++;
					}						
				});				
				
				$("tr[data-name=realTr]").find("span[data-name='"+guideTd[guideIndex].grade+"']").text(thisGradeCnt);
			}
			
			preTotalScore = sortData[i].total_score;
		}
		//비율 계산.
		setRealStCnt();
	}
	
	function setRealStCnt(){
		var stCnt = $("#stList tr").length;
		
		$.each($("tr[data-name=realTr] td[data-name=realTd]"), function(index){
			var grade = $(this).find("span").attr("data-name");
			var gradeCnt = parseInt($(this).find("span").text());
			
			var percentage = Math.round(gradeCnt/stCnt*10000)/100;
			$("tr[data-name=realPercentTr] td[data-name='"+grade+"']").text(percentage);			
		});
		
		$("tr[data-name=realTr] td[data-name=total]").html('<span class="sp_t">'+stCnt+'</span><span>/</span><span class="sp_t">'+stCnt+'</span>');
	}
	
	function confirmGrade(){
		if(confirm("저장된 성적으로 확정하시겠습니까?")){
			return;			
		}		
		
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/grade/getStList/confirm",
            data: {   
            	"curr_seq" : "${S_CURRICULUM_SEQ}"
            },
            dataType: "json",
            success: function(data, status) {
            	if(data.status == "200"){            		
	            	alert("확정 완료 되었습니다.");	        
	            	$("button[name=confirmBtn]").hide();
	            	$("button[name=saveBtn]").hide();
            	}else{
            		alert("저장 실패 하였습니다.");
            	}
            },
	        beforeSend:function() {
	            $.blockUI();
	        },
	        complete:function() {
	            $.unblockUI();
	        },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                //document.write(xhr.responseText);
            }
        });		
	}
	
	function confirmGrade(){
		if(!confirm("저장된 성적으로 확정하시겠습니까?")){
			return;			
		}		
		
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/grade/getStList/confirm",
            data: {   
            	"curr_seq" : "${S_CURRICULUM_SEQ}"
            },
            dataType: "json",
            success: function(data, status) {
            	if(data.status == "200"){            		
	            	alert("확정 완료 되었습니다.");	          
	            	window.location.reload();
            	}else{
            		alert("확정 실패 하였습니다.");
            	}
            },
	        beforeSend:function() {
	            $.blockUI();
	        },
	        complete:function() {
	            $.unblockUI();
	        },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                //document.write(xhr.responseText);
            }
        });		
	}
	
	function confirmCancelGrade(){
		if(!confirm("확정 취소 하시겠습니까?")){
			return;			
		}		
		
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/grade/getStList/cancelConfirm",
            data: {   
            	"curr_seq" : "${S_CURRICULUM_SEQ}"
            },
            dataType: "json",
            success: function(data, status) {
            	if(data.status == "200"){            		
	            	alert("확정 취소 완료 되었습니다.");	    
	            	window.location.reload();
            	}else{
            		alert("확정 취소 실패 하였습니다.");
            	}
            },
	        beforeSend:function() {
	            $.blockUI();
	        },
	        complete:function() {
	            $.unblockUI();
	        },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                //document.write(xhr.responseText);
            }
        });		
	}
</script>
</head>

<body>

	<!-- s_tt_wrap -->
	<div class="tt_wrap">
		<h3 class="am_tt">
			<span class="tt" data-name="curr_name"></span> <span class="tt_s"
				data-name="aca_system_name"></span>
		</h3>
	</div>
	<!-- e_tt_wrap -->

	<!-- s_tab_wrap_cc -->
	<div class="tab_wrap_cc">
		<!-- s_해당 탭 페이지 class에 active 추가 -->
		<button class="tab01 tablinks"
			onclick="location.href='${HOME}/pf/lesson'">교육과정계획서</button>
		<button class="tab01 tablinks"
			onclick="location.href='${HOME}/pf/lesson/schedule'">시간표관리</button>
		<button class="tab01 tablinks"
			onclick="location.href='${HOME}/pf/lesson/unit'">단원관리</button>
		<button class="tab01 tablinks"
			onclick="location.href='${HOME}/pf/lesson/lessonPlanRegCs'">단위
			수업계획서</button>
		<button class="tab01 tablinks" onclick="">만족도 조사</button>
		<button class="tab01 tablinks"
			onclick="location.href='${HOME}/pf/lesson/currAssignMent'">종합
			과제</button>
		<button class="tab01 tablinks"
			onclick="location.href='${HOME}/pf/lesson/soosiGrade'">수시 성적</button>
		<button class="tab01 tablinks active"
			onclick="location.href='${HOME}/pf/lesson/grade'">종합 성적</button>
		<button class="tab01 tablinks"
			onclick="location.href='${HOME}/pf/lesson/report'">운영보고서</button>
		<!-- e_해당 탭 페이지 class에 active 추가 -->
	</div>
	<!-- e_tab_wrap_cc -->

	<!-- s_mpf_tabcontent4 -->
	<div class="mpf_tabcontent4">

		<!-- s_tt_wrap -->
		<div class="tt_wrap3">
			<button class="btn_up1 open1">엑셀일괄등록</button>
			<button class="btn_dw3" onClick="excelDown();">종합성적 다운로드</button>

			<div class="sch_wrap">
				<div class="wrap1_uselectbox1">
					<div class="uselectbox" id="sortVal">
						<span class="uselected" data-value="id">학번순</span> <span
							class="uarrow">▼</span>
						<div class="uoptions" style="display: none;">
							<span class="uoption firstseleted" data-value="id" onClick="stListSort($(this).attr('data-value'));">학번 순</span> 
							<span class="uoption" data-value="group_number" onClick="stListSort($(this).attr('data-value'));">조별 순</span> 
							<span class="uoption" data-value="total_score" onClick="stListSort($(this).attr('data-value'));">총점 순</span> 
							<span class="uoption" data-value="rank" onClick="stListSort($(this).attr('data-value'));">석차 순</span> 
							<span class="uoption" data-value="grade_order" onClick="stListSort($(this).attr('data-value'));">확정등급 순</span>
							<span class="uoption" data-value="change_score" onClick="stListSort($(this).attr('data-value'));">변환점수 순</span>
						</div>
					</div>
				</div>

				<input type="text" class="ip_search1" placeholder="학생 이름"
					id="st_name"
					onkeypress="javascript:if(event.keyCode==13){stListSort($('#sortVal span:eq(0)').attr('data-value')); return false;}">
				<button class="btn_search1"
					onClick="stListSort($('#sortVal span:eq(0)').attr('data-value'));"></button>
			</div>
		</div>
		<!-- e_tt_wrap -->

		<!-- s_tt_wrap -->
		<div class="tt_wrap1">
			<span class="tts5">종합성적 최초 등록은 엑셀일괄등록으로만 가능합니다.</span>
			<c:if test='${acaState.aca_state != "05" && acaState.aca_state != "06" && acaState.aca_state != "07" }'>
				<br>
				<span class="tts5" style="color: red; font-weight: 900;">성적을 등록할 수 없습니다. 학사 상태를 확인하세요.</span>
			</c:if>

			<table class="tab_table ttb1">
				<tbody id="guideTrAdd">

				</tbody>
			</table>

		</div>

		<div class="bt_wrap">
			<c:if test='${acaState.aca_state == "05" || acaState.aca_state == "06" || acaState.aca_state == "07" }'>			
				<c:if test='${currFlagInfo.grade_confirm_flag == "N"}'>
					<button class="bt_2" onclick="setGuide();">가이드 적용</button>
					<button class="bt_2" onclick="getStList('id');">가이드 적용 해제</button>
				</c:if>
			</c:if>
		</div>

		<!-- e_tt_wrap -->
		<form id="gradeForm" onSubmit="return false">
			<table class="mlms_tb1">
				<thead id="table_header">

				</thead>
				<tbody id="stList"></tbody>
			</table>
		</form>


		<div class="bt_wrap">
			<c:if test='${acaState.aca_state == "05" || acaState.aca_state == "06" || acaState.aca_state == "07" }'>
				<c:choose>
					<c:when test='${currFlagInfo.grade_confirm_flag == "N"}'>
						<button class="bt_2" name="saveBtn" onclick="submitGrade();">등록</button>				
						<button class="bt_2" name="confirmBtn" onclick="confirmGrade();">확정</button>
					</c:when>
					<c:otherwise>
						<button class="bt_2" name="cancelBtn" onclick="confirmCancelGrade();">확정 취소</button>
					</c:otherwise>
				</c:choose>
			</c:if>
			<!-- <button class="bt_3">취소</button> -->
		</div>

	</div>
	<!-- e_mpf_tabcontent4 -->

	<!-- s_ 엑셀 일괄 등록 팝업 -->
	<div id="m_pop1" class="pop_up_ex pop_up_ex_1 mo1">

		<div class="pop_wrap">
			<p class="popup_title">교육과정 성적관리 엑셀업로드</p>

			<button class="pop_close close1" type="button">X</button>

			<button class="btn_exls_down_2" onclick="javascript:excelTmplDown();">엑셀
				양식 다운로드</button>

			<div class="t_title_1">
				<span class="sp_wrap_1">엑셀 양식을 다운로드 하시면, 해당 교육과정의 학생 리스트가
					자동으로 기입된 양식을 확인하실 수 있습니다.<br>해당 학생의 과목별 점수를 기입하셔서 업로드 해 주세요
				</span>
			</div>

			<div class="pop_ex_wrap">
				<div class="sub_fwrap_ex_1">
					<form id="xlsForm" onSubmit="return false;">
						<span class="ip_tt">엑셀 파일</span> <input type="text" readonly
							class="ip_sort1_1" id="xls_filename" value=""
							onClick="$('#xlsFile').click();">
						<button class="btn_r1_1" onClick="$('#xlsFile').click();">파일선택</button>
						<input type="file" style="display: none;" id="xlsFile"
							name="xlsFile" onChange="file_nameChange();">
					</form>
				</div>

				<span class="t_title_2">등록 가능한 파일확장자 : xlsx</span>

				<!-- s_t_dd -->
				<div class="t_dd">

					<div class="pop_btn_wrap">
						<c:if test='${acaState.aca_state == "05" || acaState.aca_state == "06" || acaState.aca_state == "07" }'>
							<c:if test='${currFlagInfo.grade_confirm_flag == "N"}'>
								<button type="button" class="btn01" onclick="gradeXlsxUp();">등록</button>
							</c:if>
						</c:if>
						<button type="button" class="btn02"
							onClick="$('.close1').click();">취소</button>
					</div>

				</div>
				<!-- e_t_dd -->

			</div>
		</div>
	</div>
	<!-- e_ 엑셀 일괄 등록 팝업 -->

	<!-- s_ 확정등급 팝업 -->
	<div id="m_pop2" class="pop_up_grade mo2">
		<div class="pop_wrap">

			<p class="t_title">
				<span class="sp_un">확정등급 :</span><span class="sp_nn"></span>
			</p>

			<!-- s_table_b_wrap -->
			<div class="table_b_wrap">
				<p class="t_title2">
					<span class="sp_un">확정등급</span><span class="sp_nn"></span><span
						class="sp_un">-</span><span class="sp_num"></span><span
						class="sp_un">명</span>
				</p>

				<!-- s_pht -->
				<div class="pht">
					<div class="con_wrap">
						<div class="con_s2" id="gradeStListAdd"></div>
					</div>
				</div>
				<!-- e_pop_table -->

			</div>
			<!-- e_pht -->

			<div class="t_dd">

				<div class="pop_btn_wrap2">
					<button type="button" class="btn01 close2">닫기</button>
				</div>

			</div>
		</div>
	</div>
	<!-- e_ 확정등급 팝업 -->

	<!-- s_팝업 -->

</body>
</html>