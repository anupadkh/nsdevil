<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
	<link rel="stylesheet" href="${CSS}/nwagon.css" type="text/css">
	<script src="${JS}/lib/nwagon.js"></script>	
	<script>
	
		
		$(document).ready(function() {
			getCurrList();
		});
		
		function getCurrList(){
			$.ajax({
	            type: "POST",
	            url: "${HOME}/ajax/admin/lesson/SFSurvey/list",
	            data: {      
	            },
	            dataType: "json",
	            success: function(data, status) {
	            	var htmls = "";
	            	            	
	            	$("#listAdd").empty();
	            	if(data.status=="200"){
	            		if(data.currInfo.end_chk == "Y"){
		            		htmls='<tr>'
		            		+'<td class="td_1 bd01 ta_l go1" onclick=""><div class="t_wrap3">'+data.currInfo.curr_name+'</div></td>'
							+'<td class="td_1 bd01"></td><td class="td_1 bd01">';
							if(data.stCnt.submit_cnt == data.stCnt.st_cnt)
								htmls+='<div class="sp_wrap3">전원제출</div>';
							else
								htmls+='<div class="sp_wrap2 open1b" onclick="getCurrSTList();"><span class="sp_n">'+data.stCnt.submit_cnt+'</span><span class="sign">/</span><span class="sp_n">'+data.stCnt.st_cnt+'</span>';
							htmls+='</td><td class="td_1 bd01"><button class="btn_sv1 open3" onClick="getCurrResultList();">조회</button></td>'
							+'</tr>';
							$("#listAdd").html(htmls);							
	            		}else{
	            			$("#tab001").addClass("class_x");
	            			$('head').append('<style>.tabcontent5.survey.cc.class_x .tb_wrap_n:before{content: "'+data.currInfo.academic_name+'";}</style>');
	            		}
	            		$("span[data-name='curr_name']").html(data.currInfo.curr_name);
	                	$("span[data-name='aca_system_name']").html("("+ data.currInfo.aca_system_name + ")");
	            	}
	            	
	            },
	            error: function(xhr, textStatus) {
	                document.write(xhr.responseText); 
	                $.unblockUI();                        
	            },beforeSend:function() {
	                $.blockUI();                        
	            },complete:function() {
	                $.unblockUI();                         
	            }                   
	        });
		}
		
		//과정만족도 조회
		function getCurrResultList(){
			
			$.ajax({
	            type: "POST",
	            url: "${HOME}/ajax/admin/academic/SFSurvey/currResultList",
	            data: {       
	            	"curr_seq" : "${S_CURRICULUM_SEQ}"
	            },
	            dataType: "json",
	            success: function(data, status) {
	            	var htmls = "";
	            	$("#currSurveyListAdd").empty();
	            	if(data.status=="200"){
						$("#m_pop3").show();
	            		var mpfList = "";
	            		
	            		$.each(data.mpfList, function(index){
	            			if(index != 0)
	            				mpfList += "<br>";
	            			
	            			if(isEmpty(this.department_name))
	            				mpfList += this.name;
	            			else
	            				mpfList += this.name+"("+this.department_name+")";
	            		});
	            		
						var dpfList = "";
	            		
	            		$.each(data.dpfList, function(index){
	            			if(index != 0)
	            				dpfList += "<br>";
	            			
	            			if(isEmpty(this.department_name))
	            				dpfList += this.name;
	            			else
	            				dpfList += this.name+"("+this.department_name+")";
	            		});
	            		
	            		$("td[data-name=pop_mpfList]").html(mpfList);
	            		$("td[data-name=pop_dpfList]").html(dpfList);
	            		$("td[data-name=pop_curr_name]").text(data.currInfo.curr_name);
	            		$("td[data-name=pop_curr_date]").text(data.currInfo.curr_start_date+"~"+data.currInfo.curr_end_date+" "+data.currInfo.curr_week+"주"+" "+data.currInfo.period_cnt+"차시");
	            		$("span[data-name=pop_complete_name]").text(data.currInfo.complete_name);
	            		$("span[data-name=pop_administer_name]").text(data.currInfo.administer_name);
	            		$("span[data-name=pop_grade]").text(data.currInfo.grade);
	            				            		            		
	            		if(data.currInfo.req_charge == 0)
	            			$("span[data-name=pop_req_charge]").text("");
	            		else
	            			$("span[data-name=pop_req_charge]").text(numberWithCommas(data.currInfo.req_charge+""));
	            		
	            		$("span[data-name=pop_submitStCnt]").text(data.stCnt.submit_cnt);
	            		$("span[data-name=pop_totalStCnt]").text(data.stCnt.st_cnt);
	            		$("span[data-name=pop_unSubmitStCnt]").text(data.stCnt.st_cnt-data.stCnt.submit_cnt);
	            			            		
	            		
	            		$.each(data.resultList, function(index){
	            			htmls = "";
	            			if(this.srh_type=="1"){
	            				var sri_item_explan = this.sri_item_explan.split("\|");
	            				var sri_seq =  this.sri_seq.split("\|");
	            				var answer_sri_seq = this.answer_sri_seq.split("\|");
	            				
	            				var answer_cnt = [];
	            				
	            				var color = ["#63B2E6","#78BFA3","#E9D25A","#BDBDBD","#7A7A7A","#E2FCFF","#FF7171"];
	            				
	            				for(var i=0; i<sri_item_explan.length;i++){
	            					answer_cnt[i] = 0;
								}
	            				
	            				for(var i=0; i<sri_seq.length;i++){
	            					for(var j=0; j<answer_sri_seq.length;j++){
	            						if(sri_seq[i] == answer_sri_seq[j])
	            							answer_cnt[i]++;
	            					}
								}
	            				
	            				var print_class = '';
	            				
	            				if(index != 0 && index%2==0)
	            					print_class = 'style="page-break-before:always;"';
	            					
	            				htmls = '<div class="tb_wrap" '+print_class+'>'
			    					+'<div class="wrap_wrap">'
			    					+'<p class="tt"><span class="num">'+this.srh_num+'.</span> '+this.srh_subject+'</p>';

	        					if(!isEmpty(this.srh_explan))
			    					htmls+='<p class="tt_s">'+this.srh_explan+'</p>';
			    					
		    					htmls+='<div class="wrap_s1">'
			    					+'<div class="cht1">'
			    					+'<div class="wraps">'
			    					+'<div id="chart'+index+'" class="chart100"></div>'
			    					+'<div class="fields1">';
			    				//챠트 아래 항목
			    				for(var i=0; i<sri_item_explan.length;i++){
			    					htmls+='<div class="wrap"><span class="rt'+(i+1)+'" style="background:'+color[i]+';"></span><span class="tt1">'+sri_item_explan[i]+'</span></div>'
								}
			    				
			    				var answerCnt = 0;
			    				
			    				if(!isEmpty(answer_sri_seq))
			    					answerCnt = answer_sri_seq.length;
			    						
			    			
			    				htmls += '</div></div></div></div>'
			    					+'<div class="wrap_s2">'
			    					+'<table class="tab_table_u ttb1">'
			    					+'<tr class="">'
			    					+'<td class="th01 w1">점수</td>'
			    					+'<td class="th01 w2 td_l">항목</td>'
			    					+'<td class="th01 w3"><span class="sp_sign">응답</span>'
			    					+'<span class="sp_n">'+answerCnt+'</span><span class="sp_sign">명</span></td>'
			    					+'</tr>'
			    					
			    				//답가지 응답 수
		    					for(var i=0; i<sri_item_explan.length;i++){
			    					htmls+='<tr class="">'
			    						+'<td class="th01"></td>'
			    						+'<td class="td1 td_l">'+sri_item_explan[i]+'</td>'
			    						+'<td class="td1"><span class="sp_n">'+answer_cnt[i]+'</span>'
			    						+'<span class="sp_sign">명</span></td>'
			    						+'</tr>'				    						
								}
			    				
			    				htmls += '</table></div></div></div>';
			    				
			    				$("#currSurveyListAdd").append(htmls);
			    				var options = {
		    						'dataset': {
		    							title: '',
		    							values:answer_cnt,
		    							colorset:color,
		    							fields:sri_item_explan 
		    						},
		    						'donut_width' : 100, 
		    						'core_circle_radius':0,
		    						'chartDiv': 'chart'+index,
		    						'chartType': 'pie',
		    						'chartSize': {width:200, height:400}
		    					};
		    					Nwagon.chart(options);
	            			}else if(this.srh_type=="2"){
	            				var answer = this.answer.split("\|\|");
	            				
	            				htmls += '<div class="tb_wrap">'
	        						+'<div class="wrap_wrap">'
	        						+'<p class="tt">'+this.srh_num+'. '+this.srh_subject+'</p>';
	        						
	        					if(!isEmpty(this.srh_explan))
	        						htmls+='<p class="tt_s">'+this.srh_explan+'</p>';
	        						
	    						htmls+='<div class="wrap_s3">'
	        						+'<table class="tab_table_u ttb2">'
	        						+'<tr class="">'
	        						+'<td class="th01 w4">no</td>'
	        						//+'<td class="th01 w5">응답자</td>'
	        						+'<td class="th01 w6" colspan="2">응답 내용</td>'
	        						+'</tr>';
	        					if(!isEmpty(answer)){
		        					for(var i=0; i<answer.length; i++){
		        						var answer_detail = answer[i].split("\|");
		        						htmls+='<tr class="">'
		        						+'<td class="th01">'+(i+1)+'</td>'
		        						/* +'<td class="td1 td_l"><span class="sp_1">'+answer_detail[0]+'</span>'
		        						+'<span class="sign">(</span><span class="sp_2">'+this.m_aca_name+' '+this.aca_name+'</span>'
		        						+'<span class="sign">)</span></td>' */
		        						+'<td class="td1 td_l" colspan="2">'+answer_detail[1]+'</td>'
		        						+'</tr>';
		        					}
	        					}
	        					
	        					htmls +='</table></div></div></div>';
	        					$("#currSurveyListAdd").append(htmls);
	            			}		
	            		});	
	            	}
	            },
	            error: function(xhr, textStatus) {
	                document.write(xhr.responseText); 
	                $.unblockUI();                        
	            },beforeSend:function() {
	                $.blockUI();                        
	            },complete:function() {
	                $.unblockUI();                         
	            }                   
	        });					
		}
		
		//과정만족도 미제출 학생 리스트
		function getCurrSTList(){
			
			$.ajax({
	            type: "POST",
	            url: "${HOME}/ajax/admin/academic/SFSurvey/curStList",
	            data: {       
	            	"curr_seq" : "${S_CURRICULUM_SEQ}"
	            },
	            dataType: "json",
	            success: function(data, status) {
	            	var htmls = "";
	            	if(data.status=="200"){
	            		$("span[data-name=pop_curr_name]").text(data.currInfo.curr_name);
	            		$("span[data-name=curr_st_cnt]").text(data.stCnt);
	            		$.each(data.stList, function(index){
	            			htmls +='<li class="">'
								+'<span class="sp_1">'+this.name+'</span>'
								+'<span class="sp_3">'+this.tel+'</span>'
								+'</li>';								
	            		});	
	            		$("#currStListAdd").html(htmls);
	            		$("#m_pop1b").show();
	            	}
	            },
	            error: function(xhr, textStatus) {
	                document.write(xhr.responseText); 
	                $.unblockUI();                        
	            },beforeSend:function() {
	                $.blockUI();                        
	            },complete:function() {
	                $.unblockUI();                         
	            }                   
	        });
		}
		
		function currSurveyPrint() {
			var initBody = document.body.innerHTML;
			document.body.innerHTML = "<div class='class_survey' style='display:block;position:relative;'><div class='content' style='margin:0px;'>"+document.getElementById("printDivCurr").innerHTML+"</div></div>";
			 window.onbeforeprint = function () {
				document.body.innerHTML = "<div class='class_survey' style='display:block;'><div class='content'>"+document.getElementById("printDivCurr").innerHTML+"</div></div>";
				$(".btn_prt").hide();
			}
			window.onafterprint = function () {

				document.body.innerHTML = initBody;
				$(".btn_prt").show();
			}
			window.print();
		}
	</script>
	</head>
	
	<body>
		<!-- s_tt_wrap -->
		<div class="tt_wrap">
            <h3 class="am_tt">
                <span class="tt" data-name="curr_name"></span>
                <span class="tt_s" data-name="aca_system_name"></span>
            </h3>
        </div>
		      <!-- e_tt_wrap -->
		
		<!-- s_tab_wrap_cc -->
		<div class="tab_wrap_cc">
		    <!-- s_해당 탭 페이지 class에 active 추가 -->
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson'">교육과정계획서</button>	         
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/schedule'">시간표관리</button>	
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/unit'">단원관리</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/lessonPlanRegCs'">단위 수업계획서</button>
		<button class="tab01 tablinks active" onclick="">만족도 조사</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/currAssignMent'">종합 과제</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/soosiGrade'">수시 성적</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/grade'">종합 성적</button>	
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/report'">운영보고서</button>
		<!-- e_해당 탭 페이지 class에 active 추가 -->
		</div>
		<!-- e_tab_wrap_cc -->
		
		<!-- s_조사기간이 아닙니다. class : class_x 추가 -->
		<div id="tab001" class="tabcontent tabcontent5 survey cc">
		
			<!-- s_tab_wraps-->
		<div class="tab_wraps">
			<button onclick="location.href='${HOME}/pf/lesson/currSurvery'" class="h_tt n1 on">교과정 만족도조사 조회</button>
			<button onclick="location.href='${HOME}/pf/lesson/lessonPlanSurvery'" class="h_tt n2">수업 만족도조사 조회</button>
		</div>
		<!-- s_tab_wraps-->
		
		<!-- s_btnwrap_s2_s -->
		<!-- <div class="btnwrap_s2_s result">
			<button class="btn_down2">과정만족도 엑셀 전체 다운로드</button>
		</div> -->
		<!-- e_btnwrap_s2_s -->
		
		<!-- s_tb_wrap -->
		<div class="tb_wrap_n">
			<table class="mlms_tb survey cc">
				<thead>
					<tr>
						<td class="th01 w5">교육과정명</td>
						<td class="th01 w2">평균점수</td>
						<td class="th01 w3">제출인원</td>
						<td class="th01 w3">결과보기</td>
					</tr>
				</thead>
				<tbody id="listAdd">
					
				</tbody>
			</table>
		</div>
		<!-- e_tb_wrap -->
		
		</div>
		<!-- e_tabcontent rl -->
	<!-- s_과정만족도 팝업 -->
	<div id="m_pop3" class="class_survey cc mo3" style="left:0;">
		<!-- s_pop_wrap -->
		<div class="pop_wrap">
			<button class="pop_close close3" type="button" onClick="$('#m_pop3').hide();">X</button>

			<p class="t_title">과정만족도 결과</p>


			<!-- s_content -->
			<div class="content" id="printDivCurr">
				<div class="title_wrap">

					<span class="title1">과정만족도 조사</span>
					<button class="btn_prt" title="인쇄하기" onClick="currSurveyPrint()">인쇄</button>

				</div>

				<div class="top_wrap">

					<table class="top_tb">
						<tr>
							<td rowspan="2" class="w1a">책임교수</td>
							<td rowspan="2" class="w1b" data-name="pop_mpfList"></td>
							<td class="w2">교육과정명</td>
							<td class="w3" data-name="pop_curr_name"></td>
						</tr>
						<tr>
							<td class="w2">교육과정기간</td>
							<td class="w3" data-name="pop_curr_date">2018.03.01 ~ 2018.05.31 8주 45차시</td>
						</tr>
						<tr>
							<td rowspan="2" class="w1a">부책임교수</td>
							<td rowspan="2" class="w1b" data-name="pop_dpfList"></td>
							<td colspan="2" class="w4">
								<div class="title_wrap">
									<div class="tt_wrap_s">
										<span class="tt_s4" data-name="pop_complete_name"></span>
									</div>
									<div class="tt_wrap_s">
										<span class="tt_s4" data-name="pop_administer_name"></span>
									</div>
									<div class="tt_wrap_s">
										<span class="sp_n2" data-name="pop_grade"></span>
										<span class="tt_s3">학점</span>
									</div>
									<div class="tt_wrap_s">
										<span class="tt_st">비용</span>
										<span class="sp_n2" data-name="pop_req_charge"></span>
										<span class="tt_s3">원</span>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="w4">
								<div class="title_wrap">
									<div class="tt_wrap_s pop open1b" onclick="getCurrSTList();">
										<span class="sp_n" data-name="pop_submitStCnt"></span>
										<span class="sign">/</span>
										<span class="sp_n" data-name="pop_totalStCnt"></span>
										<span class="sign">(</span>
										<span class="sp_n" data-name="pop_unSubmitStCnt"></span>
										<span class="tt_s1">명 미제출</span>
										<span class="sign">)</span>
									</div>
									<div class="tt_wrap_s">
										<span class="tt_s">최고점</span>
										<span class="sp_n"></span>
										<span class="tt_s">최저점</span>
										<span class="sp_n"></span>
										<span class="tt_s">평균점</span>
										<span class="sp_n"></span>
									</div>
								</div>
							</td>
						</tr>
					</table>

				</div>

				<div id="currSurveyListAdd">
								
				</div>
			</div>
			<!-- e_content -->
		</div>
		<!-- e_pop_wrap -->
	</div>
	<!-- e_과정만족도 팝업 -->
	
	<!-- s_과정만족도 미제출자 리스트 -->
	<div id="m_pop1b" class="pop_up_nonsub mo1b" style="left:0;">
		<div class="pop_wrap">
			<button class="pop_close close1b" type="button" onClick="$('#m_pop1b').hide();">X</button>

			<p class="t_title">과정만족도 미제출자 리스트</p>

			<!-- s_list_wrap -->
			<div class="list_wrap">

				<!-- s_wrap -->
				<div class="wrap">
					<div class="wrap_s">
						<span class="sp_1">교육과정명</span>
						<span class="sp_2" data-name="pop_curr_name"></span> 
						<span class="sp_1">미제출자</span>
						<span class="sp_2">
							<span class="tt_s">총</span>
							<span class="sp_n" data-name="curr_st_cnt"></span>
							<span class="tt_s">명</span>
						</span>
					</div>

					<ul class="wrap_s" id="currStListAdd">
						
					</ul>
				</div>
				<!-- e_wrap -->

			</div>
			<!-- e_list_wrap -->

		</div>
		<!-- e_pop_wrap -->
	</div>
	<!-- e_과정만족도 미제출자 리스트 -->
	</body>
</html>