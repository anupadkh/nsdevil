import pandas as pd
import os
import shutil
import sys, getopt
import re



def main(argv):
    file='translation.xlsx'
    try:
        opts, args = getopt.getopt(argv,"hd:i:",["directory=","input="])
    except getopt.GetoptError:
        print ('spring_replace.py -d <directory>')
        sys.exit(2)
    if opts == []:
        print ('\n\t !!! Error !!! No arguments supplied \n\nspring_replace.py -d <directory> -i <input-file>\n\n example: spring_replace.py -d "src_2/test" -i "translation.xlsx" \n')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print ('\nspring_replace.py -d <directory> -i <input-file>\n\n example: spring_replace.py -d "src_2/test" -i "translation.xlsx" \n')
            sys.exit()
        elif opt in ("-d", "--directory"):
            directory = arg
        elif opt in ("-i", "--input"):
            file = arg



    file2 = file
    xl = pd.ExcelFile(file)

    # 1. Loading the translation file to dataframe memory
    df = pd.DataFrame()
    # df.columns=['korean','english','indonesian']
    df = pd.read_excel(file2,header=[0],sheet_name = xl.sheet_names[0])
    df = df.iloc[:,range(3)].copy()
    df.columns = ['code','korean','english']
    for i in range(1,len(xl.sheet_names)):
        tempdf = pd.read_excel(file2,header=[0],sheet_name = xl.sheet_names[i])
        tempdf = tempdf.iloc[:,range(3)].copy()
        tempdf.columns = ['code','korean','english']
        df = df.append(tempdf,ignore_index=True)

    convert_axis =  1 # Column containing translation

    def create_properdf(df):
        columns = df.columns
        a = df.values
        a = {y[0]:y[1:] for y in a}
        for x in a:
          for y in a:
            if (a[y][0] in a[x][0]) & (a[y][0] != a[x][0]):
                a[y][-1] = a[y][-1] - 1
        df = pd.DataFrame(a)
        df = df.T
        df.columns = columns[1:]
        df[columns[0]] = df.index
        return df

    find_under_single_quotes = re.compile(r"\'(.*?)\'")
    find_comments = re.compile(r"//(.*?)")
    find_inside_double_quotes = re.compile(r"\"(.*?)\"")
    find_inside_angles = re.compile(r">(.*?)<")
    p = re.compile('[ㄱ-힣 ]+')


    def make_replace(line, dataframe, type='normal'):
        for a in dataframe.values.astype(str):
            # find_under_single_quotes = re.compile(r"\'(.*?)\'")
            # find_under_single_quotes.split(line)
            # find_under_single_quotes.findall(line)
            # str1 = ''.join(str(e) for e in list1)
    #         line = line_arrays[0]
            a[1] = a[1].strip()
            all_finds = find_under_single_quotes.split(line)
            quotes_find = find_under_single_quotes.findall(line)

    #         str1 = ''.join(str(e) for e in list1)
            y =[]

            for line in all_finds:
                if line in quotes_find:
                    line = line.replace(a[1],a[0])
                    y.append('\''+line+'\'')
                    continue
                if (type == 'normal'):
                    line = line.replace(a[1],a[0])
                elif (type == 'java'):
                    quotes_found = find_inside_double_quotes.findall(line)
                    for quote in quotes_found:
                        qfy = p.findall(quote)
                        for part_quote in qfy:
                            if a[1] == part_quote.strip():
                                line = line.replace('%s' % a[1], 'messageSource.getMessage("%s", null,LocaleContextHolder.getLocale())' % a[0])

    #                 line = line.replace('"%s"' % a[1], 'messageSource.getMessage("%s", null,LocaleContextHolder.getLocale())' % a[0])
    #                 line = line.replace('\'%s\'' % a[1], 'messageSource.getMessage("%s", null,LocaleContextHolder.getLocale())' % a[0])
                elif (type=='jsp'):
                    quotes_found = find_inside_double_quotes.findall(line)
                    for quote in quotes_found:
                        qfy = p.findall(quote)
                        for part_quote in qfy:
                            if part_quote.strip() == a[1]:
                                line = line.replace('%s' % a[1] , '<spring:message code="%s"/>' % a[0])
    #                 line = line.replace('\'%s\'' % a[1] , '<spring:message code="%s"/>' % a[0])
                    # Similarly for angle brackets
                    brackets_found = find_inside_angles.findall(line)
                    for bracket in brackets_found:
                        qay = p.findall(bracket)
                        for part_bracket in qay:
                            if part_bracket.strip() == a[1]:
                                line = line.replace('%s' % a[1] , '<spring:message code="%s"/>' % a[0])
                elif (type=='js'):
                    quotes_found = find_inside_double_quotes.findall(line)
                    for quote in quotes_found:
                        qfy = p.findall(quote)
                        for part_quote in qfy:
                            if a[1] == part_quote.strip():
                                line = line.replace ('%s' % a[1] , '$.i18n.prop("%s")' % a[0])
                y.append(line)
            line = ''.join(str(e) for e in y)
        return line




    df['Priority'] = 1
    df = df.reset_index(drop=True)
    df['string_len'] = df.iloc[:,1].str.len()
    newdf = create_properdf(df)


    # print(newdf.iloc[:,1].str.len())
    # print(newdf)
    df = newdf.sort_values(by=['Priority','string_len','korean'], ascending=False)
    df.to_excel('sarkar.xlsx')

    df = df.loc[:,['code', 'korean' ]]
    df = df.dropna()
    df = df.drop_duplicates(subset = 'korean', keep='first')

    inputs=[]

    for ls in os.walk(directory):
        for myfile in ls[2]:
            if myfile.endswith('java') |  myfile.endswith('jsp'):
                inputs.append(ls[0]+ os.sep + myfile)

    file = ['js', 'jsp', 'java']
    import time

    for file_d in inputs:
        start = time.process_time()
        with open(file_d, "rt", encoding='UTF-8') as fin:
            file_type = file_d.split('.')[-1]
            if file_type == 'jsp':
                mode = 1
            else:
                mode = 0
            with open("out.txt", "w", encoding='UTF-8') as fout:
                for line in fin:
                    if mode:
                        if '<script' in line:
                            file_type = 'js'
                        elif '</script>' in line:
                            file_type = 'jsp'

                    if find_comments.search(line):
                        if find_comments.search(line.strip()).start!=0:
                            line_arrays = find_comments.split(line)
                            line_arrays[0] = make_replace(line_arrays[0], dataframe = df, type = file_type)
                            line_return = '//'.join(str(e) for e in line_arrays)
                        else:
                            line_return = line
                    else:
                        line_return = make_replace(line, dataframe = df, type = file_type)
                    # if line_return != line:
                    #     print (line_return)
                    fout.write(line_return)
        shutil.move('out.txt',file_d)
        print (time.process_time() - start, file_d)


if __name__ == "__main__":
   main(sys.argv[1:])
