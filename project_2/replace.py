import pandas as pd
import os
import shutil

file='translation.xlsx'
xl = pd.ExcelFile(file)

# 1. Loading the translation file to dataframe memory
df = pd.DataFrame()
# df.columns=['korean','english','indonesian']
df = pd.read_excel(file,header=[0],sheet_name = xl.sheet_names[0])
df = df.iloc[:,range(3)].copy()
df.columns = ['korean','english','indonesian']
for i in range(1,len(xl.sheet_names)):
    tempdf = pd.read_excel(file,header=[0],sheet_name = xl.sheet_names[i])
    tempdf = tempdf.iloc[:,range(3)].copy()
    tempdf.columns = ['korean','english','indonesian']
    df = df.append(tempdf,ignore_index=True)

convert_axis = 2 # Column containing translation

def create_properdf(df):
    a = dict(df.iloc[:,[0,2]].values)
    for x in a:
      for y in a:
        if (y in x) & (y!=x):
            a[y] = a[y] - 1
    return pd.DataFrame(list(a.items()))

df = df.iloc[:,[0,convert_axis]]
df = df.dropna()
df = df.drop_duplicates(subset='korean',keep='first')


df['Priority'] = 1
df = df.reset_index(drop=True)
newdf = create_properdf(df)
newdf.columns = ['korean', 'Priority']
df['Priority'] = newdf['Priority']
df = df.sort_values(by=['Priority','korean'], ascending=False)

# df = df.iloc[df['korean'].dropna().index,:]


# 2. Function that makes a direct replacement to each lines in a file
def make_replace(line, dataframe):
    # x = line
    for a in dataframe.values.astype(str):
        line = line.replace(a[0],a[1])
        # if line != x:
        #     print('Replaced %s with %s' % (a[0],a[1]))
        #     break
    return line

# 3. Open all the files inside of src_2 folder and populate inputs array with necessary file types
inputs=[]
for ls in os.walk('src_2'):
    for myfile in ls[2]:
        if myfile.endswith('java') |  myfile.endswith('jsp') | myfile.endswith('xml') | myfile.endswith('html') | myfile.endswith('txt'):
            inputs.append(ls[0]+ os.sep + myfile)

# print (len(inputs))
# 4. Make a replacement with files
for file_d in inputs:
    with open(file_d, "rt", encoding='UTF-8') as fin:
        with open("out.txt", "w", encoding='UTF-8') as fout:
            for line in fin:
                fout.write(make_replace(line, dataframe = df))
    shutil.move('out.txt',file_d)

# line.replace('Orangedhikari', 'Orange')
