import os
import numpy as np
import openpyxl as oxl


import sys, getopt

label = ''
replacecolumn = 2
output_file = 'Result.xlsx'
directory = 'src_2'
def main(argv):
    fields = ['Korean', 'English', 'Indonesian']
    global label, output_file, directory
    try:
        opts, args = getopt.getopt(argv,"ho:l:d:",["output=","label=","directory="])
    except getopt.GetoptError:
        print ('convert_spring.py -o <output-file> -l <chosen_code_label> -d <from_directory>')
        sys.exit()
    if opts == []:
        print ('Error !!! No arguments supplied\n !!!\nconvert_spring.py -o <output-file> -l <chosen_code_label>  -d <from_directory>\n\t Output File = translation.xlsx \n\t Label = code \n\n\t\t\t example: convert_spring -o translation.xlsx -l code -d "src"\n\n\n\n')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print ('\n  convert_spring.py -o <output-file> -l <chosen_code_label>  -d <from_directory>\n\n\t Output File = translation.xlsx \n\t Label = code \n\n\t\t\t example: convert_spring -o "translation.xlsx" -l "code" -d "src_2"\n\n')
            sys.exit()
        elif opt in ("-o", "--output"):
            output_file = arg
            print (output_file)
        elif opt in ("-l", "--label"):
            label = arg
        elif opt in ("-d", "--directory"):
            directory = arg
    print ('Replacing column is %s' % fields[replacecolumn-1])
    print ('Label is "%s"' % label)

if __name__ == "__main__":
   main(sys.argv[1:])



'''
os.listdir('..')
os.path.isfile('myfile.txt')

'''
import re
a = ord('ㄱ')
z = ord('힣')


def get_all(all_lines, debug = False):
    p = re.compile('[ㄱ-힣 ]+')
    find_under_quotes = re.compile(r'"(.*?)"')
    find_under_single_quotes = re.compile(r"\'(.*?)\'")
    find_under_brackets = re.compile(r'>(.*?)<')
    find_comments = re.compile(r'//(.*?)')
    find_comments_end = re.compile(r'\*/|-->')
    find_comments_start = re.compile(r'/\*|<!--')
    all_quotes = []
    all_angles = []
    all_single_qoutes = []
    skip = False
    skipcurrentLine = False
    # spaces_check = re.compile('^[ ]+')
    everything = []
    i=0
    for myline in all_lines:
        i = i+1
        if find_comments_start.search(myline):
            if debug: print("%d \t starting long skip\t %s"%(i,myline))
            skip = True
            skipcurrentLine = True
        if find_comments_end.search(myline):
            skip = False
            if debug: print('%d\t end long skips \t %s'%(i, myline))
        xx = re.compile('[ㄱ-힣]+').findall(myline)
        if debug:
            if len(xx) == 0 :
                continue
            else:
                print("%s\t found\t%s"%(str(i),myline))

        if skip == True:
            if debug:print('skipping %s \t long skip \t %s'%(str(i), myline))
            continue
        if skipcurrentLine == True:
            skipcurrentLine = False
            if debug:print('skipping %s \t onelineskip\t %s'%(str(i), myline))
            continue
        if find_comments.search(myline):
            if find_comments.search(myline.strip()).start != 0:
                line_arrays = find_comments.split(myline)
                myline = line_arrays[0]
            else:
                if debug:print('skipping %s \t comments skip \t %s'%(str(i), myline))
                continue
        x = find_under_quotes.findall(myline)
        y = find_under_brackets.findall(myline)
        z = find_under_single_quotes.findall(myline)

        for thing in p.findall(myline):
            if thing.strip() != '':
                everything.append(thing.strip())

        for values in x:
            if len(p.findall(values)) != 0:
                for a in p.findall(values):
                    if len(a.strip()) != 0:
                        all_quotes.append(a.strip())

        for values in y:
            if len(p.findall(values)) != 0:
                for a in p.findall(values):
                    if len(a.strip()) != 0:
                        all_angles.append(a.strip())
        for values in z:
            if len(p.findall(values)) != 0:
                for a in p.findall(values):
                    if len(a.strip()) != 0:
                        all_single_qoutes.append(a.strip())

    if debug:print('\n Single Quotes: \n %s \n Angles: \n %s \n %s \n %s' % (all_quotes, all_angles, all_single_qoutes, everything))

    return all_quotes, all_angles, all_single_qoutes, everything

wb = oxl.Workbook()
ws = wb.active
ws['A1'] = 'S.No.'
ws['B1'] = 'Quoted'
ws['C1'] = 'Angular Brackets'
ws['D1'] = 'Single Quotes'
ws['G1'] = 'Single Quotes'


all_quotes = []
all_angles = []
all_single_qoutes = []
all_korean = []
if 0 : # directory.rfind('.'):
    directory = '/Users/anupadkh/Bitrix24/nsdevil/project_2/Archive/main/webapp/WEB-INF/views/mpf/lesson/grade.jsp'
    myfile = "Great"
    ls=directory
    print(directory)
    if directory.endswith('java') |  directory.endswith('jsp') | directory.endswith('txt'):
        # print ('Thank god')
        a = open(directory,encoding='utf-8', errors='ignore')
        all_lines = a.readlines()

        quotes , angles, single_quotes, korean_find = (get_all(all_lines))

        print("\n\n\n finds \n ")
        print(quotes , angles, single_quotes, korean_find)
        if len(quotes) == 0 & len(angles) == 0 & len(single_quotes) == 0 :
            "No thing to learn"
        ws2 = wb.copy_worksheet(ws)
        ws2.title = myfile[-30:-1]
        all_quotes.extend(quotes)
        all_angles.extend(angles)
        all_single_qoutes.extend(single_quotes)
        all_korean.extend(korean_find)
        ws2.cell(row=1, column = 6).value = ls[0]+'/'+myfile
        for i in range(max([len(quotes), len(angles), len(single_quotes), len(all_korean)])):
            ws2.cell(row = 2 + i, column = 1).value = i + 1
            if i < len(quotes):
                ws2.cell(row = 2 + i, column = 2).value = quotes[i]
            if i < len(angles):
                ws2.cell(row = 2 + i, column = 3).value = angles[i]
            if i < len(single_quotes):
                ws2.cell(row = 2 + i, column = 4).value = single_quotes[i]
            if i < len(all_korean):
                ws2.cell(row = 2 + i, column = 5).value = all_korean[i]
        print('From %s got total of %s single_quotes %s double_quotes and %s angles' % (myfile,len(single_quotes), len(quotes), len(angles)))

else:
    for ls in os.walk(directory):
        for myfile in ls[2]:
            if myfile.endswith('java') |  myfile.endswith('jsp'):
                a = open(ls[0] + '/' + myfile,encoding='utf-8', errors='ignore')
                all_lines = a.readlines()
                quotes , angles, single_quotes, korean_find = (get_all(all_lines))
                if len(quotes) == 0 & len(angles) == 0 & len(single_quotes) == 0 :
                    continue
                ws2 = wb.copy_worksheet(ws)
                ws2.title = myfile[-30:-1]
                all_quotes.extend(quotes)
                all_angles.extend(angles)
                all_single_qoutes.extend(single_quotes)
                all_korean.extend(korean_find)
                ws2.cell(row=1, column = 6).value = ls[0]+'/'+myfile
                for i in range(max([len(quotes), len(angles), len(single_quotes), len(all_korean)])):
                    ws2.cell(row = 2 + i, column = 1).value = i + 1
                    if i < len(quotes):
                        ws2.cell(row = 2 + i, column = 2).value = quotes[i]
                    if i < len(angles):
                        ws2.cell(row = 2 + i, column = 3).value = angles[i]
                    if i < len(single_quotes):
                        ws2.cell(row = 2 + i, column = 4).value = single_quotes[i]
                    if i < len(all_korean):
                        ws2.cell(row = 2 + i, column = 5).value = all_korean[i]
                print('From %s got total of %s single_quotes %s double_quotes and %s angles %s korean_akram' % (myfile,len(single_quotes), len(quotes), len(angles), len(all_korean)))


all_quotes = list(set(all_quotes))
all_angles = list(set(all_angles))
all_single_qoutes = list(set(all_single_qoutes))
all_korean = list(set(all_korean))
for i in range(max([len(all_quotes), len(all_angles)])):
    ws.cell(row = 2 + i, column = 1).value = i + 1
    if i < len(all_quotes):
        ws.cell(row = 2 + i, column = 2).value = all_quotes[i]
        if i < len(all_angles):
            ws.cell(row = 2 + i, column = 3).value = all_angles[i]
        if i < len(all_single_qoutes):
            ws.cell(row = 2 + i, column = 7).value = all_single_qoutes[i]
    else:
        ws.cell(row = 2 + i, column = 3).value = all_angles[i]




all_entries = all_quotes
all_entries.extend(all_angles)
# all_entries.extend(all_single_qoutes)
all_entries = list(set(all_entries))
all_entries.sort(reverse=True)

import numpy as np
all_korean = np.concatenate((all_korean,all_entries))
all_entries = np.unique(all_korean)

ws.cell(row = 1, column = 4).value = 'All Translation Dictionary'
ws.cell(row = 1, column = 5).value = 'All Possible Finds'
for i in range(len(all_entries)):
    ws.cell(row=2+i, column=4).value = all_entries[i]
for i in range(len(all_korean)):
    ws.cell(row=2+i, column = 5).value = all_korean[i]
wb.save('sudo.xlsx')
# Reverse engineering the partial translations
import pandas as pd

file='translation.xlsx'
xl = pd.ExcelFile(file)
df = pd.DataFrame()
# df.columns=['korean','english','indonesian']
df = pd.read_excel(file,header=[0],sheet_name = xl.sheet_names[0])
df = df.iloc[:,range(4)].copy()
df.columns = ['code','korean','english','indonesian']
for i in range(1,len(xl.sheet_names)):
    tempdf = pd.read_excel(file,header=[0],sheet_name = xl.sheet_names[i])
    tempdf = tempdf.iloc[:,range(4)].copy()
    tempdf.columns = ['code','korean','english','indonesian']
    df = df.append(tempdf, ignore_index=True)

convert_axis =  replacecolumn # Column containing translation

def create_properdf(df):
    global convert_axis
    a = dict(df.iloc[:,[1,2]].values)
    for x in a:
     for y in a:
        if (y in x) & (y!=x):
            a[y] = a[y] - 1
    return pd.DataFrame(list(a.items()))

df.index = df['code']
df = df.iloc[:,[1,convert_axis]]
df = df.dropna()
df = df.drop_duplicates(subset='korean',keep='first')
# print(df.T)
newdf = pd.DataFrame(data=all_entries, columns = ['korean'])
newdf['length'] = newdf.korean.str.len()
newdf = newdf.sort_values(by=['length'], ascending=False)
all_entries = newdf.korean.values
# df = df.append(pd.DataFrame(data=all_entries, columns = ['korean']))
# print(df.columns)
# print(df.T)
df['Priority'] = 1
df = df.reset_index(drop=True)
newdf = create_properdf(df)
# print(df.columns)
newdf.columns = ['korean', 'Priority']
# newdf.append(pd.DataFrame(data=all_entries, columns=['korean']))
df['Priority'] = newdf['Priority']
df = df.sort_values(by=['Priority','korean'], ascending=False)


# df = df.iloc[df['korean'].dropna().index,:]



def make_replace(line, dataframe):
    # x = line
    for a in dataframe.values.astype(str)[::-1]:
        line = line.replace(a[1],a[0])
        # if line != x:
        #     print('Replaced %s with %s' % (a[0],a[1]))
        #     break
    return line

wb = oxl.load_workbook('translation.xlsx')
# wbnew = oxl.Workbook()
ws = wb.active
ws_rev_eng = wb.copy_worksheet(ws)
wb.remove_sheet(ws)
ws_rev_eng.title = "Conversion"
# for all_ws in wb.sheetnames:
#     if ('Conversion' in all_ws) != 1:
#         std = wb[all_ws]
#         wb.remove(std)

row_count = ws_rev_eng.max_row
column_count = ws_rev_eng.max_column
for i in range(row_count):
    for j in range(column_count):
        ws_rev_eng.cell(row = i+1, column=j+1).value = None

korean_offset_column = 1
ws_rev_eng['A1'] = 'Code'
ws_rev_eng['B1'] = 'Korean Text'
ws_rev_eng['C1'] = 'English Text'
ws_rev_eng['D1'] = 'Indonesian text'
ws_rev_eng['E1'] = 'Partial Conversion Text (if Present)'
for i in range(len(all_korean)):
    if type(all_korean[i]) == type(None):
        continue
    result = make_replace(all_korean[i], df)
    # ws_rev_eng.cell(row=(2+i), column=1).value = i

    ws_rev_eng.cell(row = 2+ i, column = 1 + korean_offset_column).value = all_korean[i].strip()
    ws_rev_eng.cell(row = 2 + i, column = korean_offset_column - 1 + 1 ).value = label + str(i)
    if result != all_korean[i]:
        ws_rev_eng.cell(row=2+i, column=1 + korean_offset_column).value = result
        ws_rev_eng.cell(row = 2+ i, column = 4 + korean_offset_column).value = all_entries[i]
    # print("(%s , %s )"%(result, all_entries[i]))

print("Writing to " + output_file)
wb.save(output_file)
