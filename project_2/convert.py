import os
import openpyxl as oxl

'''
os.listdir('..')
os.path.isfile('myfile.txt')

'''
import re
a = ord('ㄱ')
z = ord('힣')

def get_all(all_lines):
    p = re.compile('[ㄱ-힣]+')
    find_under_quotes = re.compile(r'"(.*?)"')
    find_under_brackets = re.compile(r'>(.*?)<')
    all_quotes = []
    all_angles = []

    for myline in all_lines:
        x = find_under_quotes.findall(myline)
        y = find_under_brackets.findall(myline)
        for values in x:
            if len(p.findall(values)) != 0:
                # print(values)
                all_quotes.append(values)
        for values in y:
            if len(p.findall(values)) != 0:
                # print('voila')
                all_angles.append(values)
                # print(values)

    return all_quotes, all_angles

wb = oxl.Workbook()
ws = wb.active
ws['A1'] = 'S.No.'
ws['B1'] = 'Quoted'
ws['C1'] = 'Angular Brackets'


all_quotes = []
all_angles = []
for ls in os.walk('src_2'):
    for myfile in ls[2]:
        if myfile.endswith('java') |  myfile.endswith('jsp'):
            a = open(ls[0] + '/' + myfile,encoding='utf-8', errors='ignore')
            all_lines = a.readlines()
            ws2 = wb.copy_worksheet(ws)
            ws2.title = myfile[-30:-1]
            quotes , angles = (get_all(all_lines))
            all_quotes.extend(quotes)
            all_angles.extend(angles)
            for i in range(max([len(quotes), len(angles)])):
                ws2.cell(row = 2 + i, column = 1).value = i + 1
                if i < len(quotes):
                    ws2.cell(row = 2 + i, column = 2).value = quotes[i]
                    if i < len(angles):
                        ws2.cell(row = 2 + i, column = 3).value = angles[i]
                else:
                    ws2.cell(row = 2 + i, column = 3).value = angles[i]

all_quotes = list(set(all_quotes))
all_angles = list(set(all_angles))
for i in range(max([len(all_quotes), len(all_angles)])):
    ws.cell(row = 2 + i, column = 1).value = i + 1
    if i < len(all_quotes):
        ws.cell(row = 2 + i, column = 2).value = all_quotes[i]
        if i < len(all_angles):
            ws.cell(row = 2 + i, column = 3).value = all_angles[i]
    else:
        ws.cell(row = 2 + i, column = 3).value = all_angles[i]



all_entries = all_quotes
all_entries.extend(all_angles)
all_entries = list(set(all_entries))
all_entries.sort(reverse=True)
ws.cell(row = 1, column = 4).value = 'All Translation Dictionary'
for i in range(len(all_entries)):
    ws.cell(row=2+i, column=4).value = all_entries[i]

wb.save('sudo.xlsx')
# Reverse engineering the partial translations
import pandas as pd

file='translation.xlsx'
xl = pd.ExcelFile(file)
df = pd.DataFrame()
# df.columns=['korean','english','indonesian']
df = pd.read_excel(file,header=[0],sheet_name = xl.sheet_names[0])
df = df.iloc[:,range(3)].copy()
df.columns = ['korean','english','indonesian']
for i in range(1,len(xl.sheet_names)):
    tempdf = pd.read_excel(file,header=[0],sheet_name = xl.sheet_names[i])
    tempdf = tempdf.iloc[:,range(3)].copy()
    tempdf.columns = ['korean','english','indonesian']
    df = df.append(tempdf,ignore_index=True)

convert_axis = 2 # Column containing translation

def create_properdf(df):
    a = dict(df.iloc[:,[0,2]].values)
    for x in a:
      for y in a:
        if (y in x) & (y!=x):
            a[y] = a[y] - 1
    return pd.DataFrame(list(a.items()))

df = df.iloc[:,[0,convert_axis]]
df = df.dropna()
df = df.drop_duplicates(subset='korean',keep='first')


df['Priority'] = 1
df = df.reset_index(drop=True)
newdf = create_properdf(df)
newdf.columns = ['korean', 'Priority']
df['Priority'] = newdf['Priority']
df = df.sort_values(by=['Priority','korean'], ascending=False)

# df = df.iloc[df['korean'].dropna().index,:]



def make_replace(line, dataframe):
    # x = line
    for a in dataframe.values.astype(str)[::-1]:
        line = line.replace(a[1],a[0])
        # if line != x:
        #     print('Replaced %s with %s' % (a[0],a[1]))
        #     break
    return line

wb = oxl.load_workbook('translation.xlsx')
# wbnew = oxl.Workbook()
ws = wb.active
ws_rev_eng = wb.copy_worksheet(ws)
ws_rev_eng.title = "Conversion"
for all_ws in wb.sheetnames:
    if ('Conversion' in all_ws) != 1:
        std = wb[all_ws]
        wb.remove(std)

row_count = ws_rev_eng.max_row
column_count = ws_rev_eng.max_column
for i in range(row_count):
    for j in range(column_count):
        ws_rev_eng.cell(row = i+1, column=j+1).value = None

ws_rev_eng['A1'] = 'Korean Text'
ws_rev_eng['B1'] = 'English Text'
ws_rev_eng['C1'] = 'Indonesian text'
ws_rev_eng['D1'] = 'Partial Conversion Text (if Present)'
for i in range(len(all_entries)):
    result = make_replace(all_entries[i], df)
    # ws_rev_eng.cell(row=(2+i), column=1).value = i
    ws_rev_eng.cell(row = 2+ i, column = 1).value = all_entries[i]
    if result != all_entries[i]:
        ws_rev_eng.cell(row=2+i, column=1).value = result
        ws_rev_eng.cell(row = 2+ i, column = 4).value = all_entries[i]
    # print("(%s , %s )"%(result, all_entries[i]))


wb.save('Result.xlsx')



# os.path.isfile('myfile.txt')

# a = open('raw.txt')
# file_lines = a.readlines()
# print (get_all(file_lines))



    # print(p.findall())
