import os
os.chdir('C:\\Users\\Dell\\Documents\\anup_python_codelab\\nsdevil\\project_2')
import pandas as pd
import shutil
import sys, getopt
file2 ="Result_again.xlsx"
xl = pd.ExcelFile(file2)



# 1. Loading the translation file to dataframe memory
df = pd.DataFrame()
# df.columns=['korean','english','indonesian']
df = pd.read_excel(file2,header=[0],sheet_name = xl.sheet_names[0])
df = df.iloc[:,range(3)].copy()
df.columns = ['code','korean','english']
for i in range(1,len(xl.sheet_names)):
    tempdf = pd.read_excel(file,header=[0],sheet_name = xl.sheet_names[i])
    tempdf = tempdf.iloc[:,range(3)].copy()
    tempdf.columns = ['code','korean','english']
    df = df.append(tempdf,ignore_index=True)

convert_axis =  1 # Column containing translation

def create_properdf(df):
    columns = df.columns
    a = df.values
    a = {y[0]:y[1:] for y in a}
    for x in a:
      for y in a:
        if (a[y][0] in a[x][0]) & (a[y][0] != a[x][0]):
            a[y][-1] = a[y][-1] - 1
    df = pd.DataFrame(a)
    df = df.T
    df.columns = columns[1:]
    df[columns[0]] = df.index
    return df

find_under_single_quotes = re.compile(r"\'(.*?)\'")
find_comments = re.compile(r"//(.*?)")
    
    
def make_replace(line, dataframe, type='normal'):
    for a in dataframe.values.astype(str):
        # find_under_single_quotes = re.compile(r"\'(.*?)\'")
        # find_under_single_quotes.split(line)
        # find_under_single_quotes.findall(line)
        # str1 = ''.join(str(e) for e in list1)
#         line = line_arrays[0]
        all_finds = find_under_single_quotes.split(line)
        quotes_find = find_under_single_quotes.findall(line)

#         str1 = ''.join(str(e) for e in list1)
        y =[]

        for line in all_finds:
            if line in quotes_find:
                line = line.replace(a[1],a[0])
                y.append('\''+line+'\'')
                continue
            if (type == 'normal'):
                line = line.replace(a[1],a[0])
            elif (type == 'java'):
                line = line.replace('"%s"' % a[1], 'messageSource.getMessage("%s", null,LocaleContextHolder.getLocale())' % a[0])
                line = line.replace('\'%s\'' % a[1], 'messageSource.getMessage("%s", null,LocaleContextHolder.getLocale())' % a[0])
            elif (type=='jsp'):
                line = line.replace('"%s"' % a[1] , '<spring:message code="%s"/>' % a[0])
                line = line.replace('\'%s\'' % a[1] , '<spring:message code="%s"/>' % a[0])
                # Similarly for angle brackets
                line = line.replace('>%s<' % a[1] , '><spring:message code="%s"/><' % a[0])
            elif (type=='js'):
                line = line.replace ('"%s"' % a[1] , '$.i18n.prop("%s")' % a[0])
            y.append(line)
        line = ''.join(str(e) for e in y)
    return line


df['Priority'] = 1
df = df.reset_index(drop=True)
newdf = create_properdf(df)
df = df.sort_values(by=['Priority','korean'], ascending=False)

df = df.loc[:,['code', 'korean' ]]
df = df.dropna()
df = df.drop_duplicates(subset = 'korean', keep='first')

inputs=[]
directory = 'Archive'
for ls in os.walk(directory):
    for myfile in ls[2]:
        if myfile.endswith('java') |  myfile.endswith('jsp'):
            inputs.append(ls[0]+ os.sep + myfile)
            
file = ['js', 'jsp', 'java']
import time

for file_d in inputs:
    start = time.process_time()
    with open(file_d, "rt", encoding='UTF-8') as fin:
        file_type = file_d.split('.')[-1]
        if file_type == 'jsp':
            mode = 1
        else:
            mode = 0
        with open("out.txt", "w", encoding='UTF-8') as fout:
            for line in fin:
                if mode:
                    if '<script' in line:
                        file_type = 'js'
                    elif '</script>' in line:
                        file_type = 'jsp'
                
                if find_comments.search(line):
                    if find_comments.search(line.strip()).start!=0:
                        line_arrays = find_comments.split(line)
                        line_arrays[0] = make_replace(line_arrays[0], dataframe = df, type = file_type)
                        line_return = '//'.join(str(e) for e in line_arrays)
                    else:
                        line_return = line
                else:
                    line_return = make_replace(line, dataframe = df, type = file_type)
                # if line_return != line:
                #     print (line_return)
                fout.write(line_return)
    shutil.move('out.txt',file_d)
    print (time.process_time() - start, file_d)