package com.nsdevil.medlms.web.admin.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.exception.RuntimeLogicException;
import com.nsdevil.medlms.common.util.FileUploader;
import com.nsdevil.medlms.common.util.PagingHelper;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.admin.dao.StUserDao;

@Service
public class StUserService {
	
	@Value("#{config['config.rootPath']}")
	private String ROOT_PATH;

	@Value("#{config['config.resPath']}")
	private String RES_PATH;

	@Value("#{config['config.profilePath']}")
	private String PROFILE_PATH;

	@Value("#{config['config.initPassword']}")
	private String INIT_PASSWORD;
	
	@Autowired
	private StUserDao stUserDao;

	public ResultMap getStList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		PagingHelper pagingHelper = new PagingHelper();
		//카운트 취득
		int totalCount = stUserDao.getSudentListCount(param);
		
		List<HashMap<String, Object>> list = stUserDao.getSudentList(pagingHelper.getPagingParam(param, totalCount));
		resultMap.putAll(pagingHelper.getPageList(list, "getStList", totalCount));
		return resultMap;
	}	
	
	public ResultMap stCreate(HttpServletRequest request, HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		String profileImgUploadPath = ROOT_PATH + RES_PATH + PROFILE_PATH;
		boolean isPictureChange = false;
		param.put("id", param.get("userId"));
		
		HashMap<String, Object> overLapMap = stUserDao.getOverLapChk(param);
				
		//학번
		String id = String.valueOf(request.getParameter("id"));
		if (id != null && !id.equals("")) {			
			if (!overLapMap.get("id_chk").toString().equals("0")) {
				throw new RuntimeLogicException("STUDENT ID OVERLAP", "301");
			}
		}else {
			throw new RuntimeLogicException("STUDENT ID NULL", "201");
		}
		
		//이메일 중복검사
		String email = String.valueOf(request.getParameter("email"));
		if (email != null && !email.equals("")) {			
			if (!overLapMap.get("email_chk").toString().equals("0")) {
				throw new RuntimeLogicException("STUDENT E-MAIL OVERLAP", "302");
			}
		}else {
			throw new RuntimeLogicException("STUDENT E-MAIL NULL", "202");
		}
			
		param.put("pwd", Util.getSHA256(INIT_PASSWORD)); //초기비밀번호

		
		// 파일업로드
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		MultipartFile profileImgFile = mRequest.getFile("uploadFile");
		if (profileImgFile != null && profileImgFile.getSize() > 0) {
			String realName = FileUploader.uploadFile(profileImgUploadPath, profileImgFile);
			if (realName == null || realName.equals("")) {
				throw new RuntimeLogicException("FILE_UPLOAD_FAIL [ profile image ]", "003");
			}
			profileImgUploadPath = PROFILE_PATH + "/" + realName;
			isPictureChange = true;
		}
		
		if (isPictureChange) {
			param.put("picture_path", profileImgUploadPath);
			param.put("picture_name", profileImgFile.getOriginalFilename());
		}
		
		if (stUserDao.insertUsertSt(param) < 1) {
			throw new RuntimeLogicException("QUERY_FAIL [ users student insert query fail]", "004");
		}
		
		
		return resultMap;
	}

	public ResultMap getStInfo(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("stInfo", stUserDao.stUserDetail(param));
		resultMap.put("stAcaInfo", stUserDao.getSTAcaInfo(param));
		resultMap.put("stNowAcaInfo", stUserDao.getStNowAcaInfo(param));
		return resultMap;
	}

	public ResultMap stModify(HttpServletRequest request, HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		String profileImgUploadPath = ROOT_PATH + RES_PATH + PROFILE_PATH;
		
		HashMap<String, Object> overLapMap = stUserDao.getOverLapChk(param);
						
		//이메일 중복검사
		String email = String.valueOf(request.getParameter("email"));
		if (email != null && !email.equals("")) {			
			if (!overLapMap.get("email_chk").toString().equals("0")) {
				throw new RuntimeLogicException("STUDENT E-MAIL OVERLAP", "302");
			}
		}else {
			throw new RuntimeLogicException("STUDENT E-MAIL NULL", "202");
		}
					
		// 파일업로드
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		MultipartFile profileImgFile = mRequest.getFile("uploadFile");
		if (profileImgFile != null && profileImgFile.getSize() > 0) {
			String realName = FileUploader.uploadFile(profileImgUploadPath, profileImgFile);
			if (realName == null || realName.equals("")) {
				throw new RuntimeLogicException("FILE_UPLOAD_FAIL [ profile image ]", "003");
			}
			profileImgUploadPath = PROFILE_PATH + "/" + realName;
			param.put("picture_path", profileImgUploadPath);
			param.put("picture_name", profileImgFile.getOriginalFilename());
		}
		
		
		if (stUserDao.updateUsertSt(param) < 1) {
			throw new RuntimeLogicException("QUERY_FAIL [ users student update query fail]", "004");
		}
		
		
		return resultMap;
	}
	
	@Transactional
	public ResultMap stExcelCreate(MultipartHttpServletRequest request, HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();

		List<HashMap<String, Object>> stInfoList = new ArrayList<HashMap<String, Object>>();
		MultipartFile uploadFile = request.getFile("xlsFile");
		if (uploadFile != null) {
			@SuppressWarnings("resource")
			XSSFWorkbook workBook = new XSSFWorkbook(uploadFile.getInputStream());
			XSSFSheet curSheet = null;
			XSSFRow curRow = null;
			XSSFCell curCell = null;
			DataFormatter formatter = new DataFormatter();
			HashMap<String, Object> stInfo = null;
			//sheet 탐색 for문
			for (int sheetIndex = 0; sheetIndex < workBook.getNumberOfSheets(); sheetIndex++) {
				//현재 sheet 반환
				curSheet = workBook.getSheetAt(sheetIndex);
				//row 탐색 for문
				for (int rowIndex = 0; rowIndex < curSheet.getPhysicalNumberOfRows(); rowIndex++) {
					//0번째 row는 Header 정보이기 때문에 pass
					if (rowIndex != 0) {
						//현재 row 반환
						curRow = curSheet.getRow(rowIndex);
						stInfo = new HashMap<String, Object>();
						for (int cellIndex = 0; cellIndex < 6; cellIndex++) {
							curCell = curRow.getCell(cellIndex);
							if (curCell != null) {
								String value = formatter.formatCellValue(curCell);
								switch (cellIndex) {
									case 1:
										stInfo.put("user_id", value);
										break;
									case 2:
										stInfo.put("user_name", value);
										break;
									case 3:
										stInfo.put("tel", value);
										break;
									case 4:
										stInfo.put("email", value);
										break;
									case 5:
										stInfo.put("pwd", Util.getSHA256(value));
										break;
									default:
										break;
								}
							}
						}
						stInfoList.add(stInfo);
					}
				}
			}
		}
		
		int index = 0;
		for (HashMap<String, Object> map : stInfoList) {
			index++;
			map.put("user_seq", param.get("user_seq"));
			
			int IdChk = stUserDao.idOverlapChk(map);
			int emailChk = stUserDao.emailOverlapChk(map);
			if(IdChk > 0) {
				throw new RuntimeLogicException(String.valueOf(index), "201");
			}
			
			if(emailChk > 0) {
				throw new RuntimeLogicException(String.valueOf(index), "202");
			}
			
			if (stUserDao.insertUsertSt(map) != 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ users insert query fail ]", "004");
			}
		}
		
		return resultMap;
	}		
}
