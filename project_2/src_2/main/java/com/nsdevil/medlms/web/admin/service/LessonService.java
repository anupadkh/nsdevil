package com.nsdevil.medlms.web.admin.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.exception.RuntimeLogicException;
import com.nsdevil.medlms.common.util.PagingHelper;
import com.nsdevil.medlms.web.admin.dao.LessonDao;

@Service
public class LessonService {

	@Autowired
	private LessonDao lessonDao;
	
	public ResultMap getAcaYear() throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("yearList", lessonDao.getAcaYearList());
		return resultMap;
	}

	public ResultMap getAcaList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("academicList", lessonDao.getAcaDemicList(param));
		return resultMap;
	}

	public ResultMap getLeftCurrList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		List<HashMap<String, Object>> curriculumList = lessonDao.getCurrList(param);
		
		//교육과정에 수업계획서 리스트 insert
		for (HashMap<String, Object> curriculumInfo : curriculumList) {
			param.put("curr_seq", curriculumInfo.get("curr_seq"));
			param.put("curr_represent_yn", curriculumInfo.get("curr_represent_yn"));

			PagingHelper pagingHelper = new PagingHelper(5, 1);
						
			//카운트 취득
			int totalCount = lessonDao.getLpListCount(param);
			
			List<HashMap<String, Object>> list = lessonDao.getLpList(pagingHelper.getPagingParam(param, totalCount));
			
			curriculumInfo.put("lpList", list);
			curriculumInfo.put("countInfo", pagingHelper.getPageCnt(totalCount));			

		}
		resultMap.put("currList", curriculumList);
		return resultMap;
	}

	public ResultMap getLeftLpList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		//카운트 취득
		int totalCount = lessonDao.getLpListCount(param);

		PagingHelper pagingHelper = new PagingHelper(5, 1);
		
		List<HashMap<String, Object>> list = lessonDao.getLpList(pagingHelper.getPagingParam(param, totalCount));
		
		resultMap.put("lpList", list);
		resultMap.put("countInfo", pagingHelper.getPageCnt(totalCount));		
		return resultMap;
	}

	public ResultMap confirmCurriculum(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		if(lessonDao.confirmFlagCurr(param) == 0) {
			throw new RuntimeLogicException("QUERY_FAIL [ admin - curriculum confirm query fail ]", "004");
		}	
		
		return resultMap;
	}

	public ResultMap monitoringCurrList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		//카운트 취득
		int totalCount = lessonDao.curriculumMonitoringCount(param);

		PagingHelper pagingHelper = new PagingHelper();
		
		List<HashMap<String, Object>> list = lessonDao.curriculumMonitoring(pagingHelper.getPagingParam(param, totalCount));
		
		resultMap.putAll(pagingHelper.getPageList(list, "getCurrList", totalCount));
		return resultMap;
	}
}
