package com.nsdevil.medlms.web.common.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.common.service.RoomReserveService;

@Controller
public class RoomReserveController extends ExceptionController {
	
	@Autowired
	private RoomReserveService roomReserveService;

	@RequestMapping(value = "common/roomReserve", method = RequestMethod.GET)
	public String roomReserveView( Model model, HttpServletRequest request ,HttpServletResponse response, @RequestParam HashMap<String, Object> param) throws Exception {

		model.addAllAttributes(roomReserveService.getFacilityCode(param)); //시설코드
		return "common/roomReserve/reserveView";
	}
	
	@RequestMapping(value ="/ajax/common/roomReserve/create", method = RequestMethod.POST)
	public String roomReserveCreate(HttpServletRequest request, Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		Util.requiredCheck(param, new String[]{"start_date", "end_date", "all_day_flag", "title", "facility_code", "important_level", "tel"});		
		model.addAllAttributes(roomReserveService.insertReserve(param));
		return JSON_VIEW;
	}
	
	@RequestMapping(value ="/ajax/common/roomReserve/list", method = RequestMethod.POST)
	public String roomReserveList(HttpServletRequest request, Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		model.addAllAttributes(roomReserveService.getReserveList(param));
		model.addAllAttributes(roomReserveService.getFacilityCode(param)); //시설코드
		return JSON_VIEW;
	}
	
	@RequestMapping(value ="/ajax/common/roomReserve/detail", method = RequestMethod.POST)
	public String roomReserveDetail(HttpServletRequest request, Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		Util.requiredCheck(param, new String[]{"reserve_seq"});
		model.addAllAttributes(roomReserveService.getReserveDetailInfo(param));
		return JSON_VIEW;
	}
}
