package com.nsdevil.medlms.web.common.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.web.common.dao.CarteDao;

@Service
public class CarteService {
	@Autowired
	private CarteDao dao;
	
	public ResultMap getCarteDetail() throws Exception {
		ResultMap resultMap = new ResultMap();
		HashMap<String, Object> carteInfo = new HashMap<String, Object>();
		carteInfo = dao.getCarteInfo();
		if (carteInfo != null && !carteInfo.isEmpty()) {
			String [] keyArr = {"lunch_a", "lunch_b", "diner_a", "diner_b","add_food"};
			for (int i=0; i<keyArr.length; i++) {
				String [] menuArr = String.valueOf(carteInfo.get(keyArr[i])).split("\\|ns\\|");
				List<String> list = new ArrayList<String>();
				for (int k=0; k<5; k++) {
					list.add(menuArr[k]);
				}
				carteInfo.put(keyArr[i]+"_list", list);
				carteInfo.remove(keyArr[i]);
			}
			resultMap.putAll(carteInfo);
		} else {
			resultMap.setStatus("009");
			resultMap.setMsg("EMPTY_FOOD_MENU");
		}
		return resultMap;
		
	}
	
}
