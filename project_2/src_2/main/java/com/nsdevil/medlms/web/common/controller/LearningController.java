package com.nsdevil.medlms.web.common.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nsdevil.medlms.common.constants.Constants;
import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.common.exception.LogicException;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.common.service.CommonService;
import com.nsdevil.medlms.web.common.service.LearningService;

@Controller
public class LearningController extends ExceptionController {
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private LearningService service;
	
	//게시판 글 삭제
	@RequestMapping(value = "/ajax/common/board/remove", method = RequestMethod.POST)
	public String removeBoard(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("remove_board_seqs", param.get("removeBoardSeqs"));
		int sUserLevel = (int)request.getSession().getAttribute("S_USER_LEVEL");
		if (sUserLevel > 3) {
			throw new LogicException("USER_PERMISSION_DENIED", "003");
		}
		Util.requiredCheck(param, new String[] {"s_user_seq", "remove_board_seqs"});
		model.addAllAttributes(commonService.removeBoard(param));
		return JSON_VIEW;
	}
	
	@RequestMapping(value = "/common/learning", method = RequestMethod.GET)
	public String learningView() throws Exception {
		return "redirect:/common/learning/learning/list";
	}
	
	/************************************************************************************************/
	/*                                        학습자료실                                                                                  */
	/************************************************************************************************/
	
	//학습자료실 목록 화면
	@RequestMapping(value = "/common/learning/learning/list", method = RequestMethod.GET)
	public String learningListView (HttpServletRequest request) throws Exception {
		return "common/learning/learning/learningList";
	}
	
	//학습자료실 목록
	@RequestMapping(value = "/ajax/common/learning/learning/list", method = RequestMethod.GET)
	public String learningList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.LEARNING_BOARD_CODE);
		param.put("listFunctionName", "boardListView");
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		model.addAllAttributes(commonService.getBoardList(param));
		return JSON_VIEW;
	}
	
	//학습자료실  registrasi 화면
	@RequestMapping(value = "/common/learning/learning/create", method = RequestMethod.GET)
	public String learningCreateView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		return "common/learning/learning/learningCreate";
	}
	
	//학습자료실 registrasi
	@RequestMapping(value = "/ajax/common/learning/learning/create", method = RequestMethod.POST)
	public String learningCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.LEARNING_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(commonService.boardCreate(param, request));
		return JSON_VIEW;
	}
	
	//학습자료실 상세화면
	@RequestMapping(value = "/common/learning/learning/detail", method = RequestMethod.GET)
	public String learningDetailView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "common/learning/learning/learningDetail";
	}
	
	//학습자료실 상세
	@RequestMapping(value = "/ajax/common/learning/learning/detail", method = RequestMethod.GET)
	public String learningDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("s_board_seq", request.getSession().getAttribute("S_BOARD_SEQ"));
		param.put("board_code", Constants.LEARNING_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(commonService.getBoardDetail(param));
		String sBoardSeq = (String)param.get("s_board_seq");
		String boardSeq = (String)param.get("board_seq");
		if (sBoardSeq == null || !sBoardSeq.equals(boardSeq)) {
			commonService.updateBoardHits(boardSeq);
			request.getSession().setAttribute("S_BOARD_SEQ", param.get("board_seq"));
		}
		return JSON_VIEW;
	}
	
	
	//학습자료실 perbaiki화면
	@RequestMapping(value = "/common/learning/learning/modify", method = RequestMethod.POST)
	public String learningModifyView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		int sUserLevel = (int)request.getSession().getAttribute("S_USER_LEVEL");
		if (sUserLevel > 3) {
			return "redirect:/";
		}
		model.addAllAttributes(param);
		return "common/learning/learning/learningModify";
	}
	
	//학습자료실 perbaiki상세
	@RequestMapping(value = "/ajax/common/learning/learning/modify/detail", method = RequestMethod.GET)
	public String learningModifyDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.LEARNING_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(commonService.getBoardDetail(param));
		return JSON_VIEW;
	}
	
	//학습자료실 perbaiki
	@RequestMapping(value = "/ajax/common/learning/learning/modify", method = RequestMethod.POST)
	public String learningModify(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.LEARNING_BOARD_CODE);
		param.put("board_seq", param.get("boardSeq"));
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(commonService.boardModify(param, request));
		return JSON_VIEW;
	}

	/************************************************************************************************/
	/*                                         임상술기(OSCE) 동영상                                                               */
	/************************************************************************************************/
	
	//OSCE
	@RequestMapping(value = "/common/learning/osce/list", method = RequestMethod.GET)
	public String osceListView(HttpServletRequest request) throws Exception {
		return "common/learning/osce/osceList";
	}
	
	//OSCE 목록
	@RequestMapping(value = "/ajax/common/learning/osce/list", method = RequestMethod.GET)
	public String osceList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.OSCE_BOARD_CODE);
		param.put("search_osce_code", param.get("searchOSCECode"));
		param.put("listFunctionName", "boardListView");
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		model.addAllAttributes(service.getOSCEList(param));
		return JSON_VIEW;
	}
	
	//OSCE CODE 목록
	@RequestMapping(value = "/ajax/common/learning/osce/code/list", method = RequestMethod.GET)
	public String osceCodeList(Model model) throws Exception {
		model.addAllAttributes(service.getOSCECodeList());
		return JSON_VIEW;
	}
	
	//OSCE  registrasi 화면
	@RequestMapping(value = "/common/learning/osce/create", method = RequestMethod.GET)
	public String osceCreateView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAttribute("code_name", commonService.getCodeName("osce_code_name"));
		return "common/learning/osce/osceCreate";
	}
	
	//OSCE registrasi
	@RequestMapping(value = "/ajax/common/learning/osce/create", method = RequestMethod.POST)
	public String osceCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.OSCE_BOARD_CODE);
		param.put("board_cate_code", param.get("boardCateCode"));
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(commonService.boardCreate(param, request));
		return JSON_VIEW;
	}
	
	//OSCE 상세화면
	@RequestMapping(value = "/common/learning/osce/detail", method = RequestMethod.GET)
	public String osceDetailView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "common/learning/osce/osceDetail";
	}
	
	//OSCE 상세
	@RequestMapping(value = "/ajax/common/learning/osce/detail", method = RequestMethod.GET)
	public String osceDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("s_board_seq", request.getSession().getAttribute("S_BOARD_SEQ"));
		param.put("board_code", Constants.OSCE_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(service.getOSCEDetail(param));
		String sBoardSeq = (String)param.get("s_board_seq");
		String boardSeq = (String)param.get("board_seq");
		if (sBoardSeq == null || !sBoardSeq.equals(boardSeq)) {
			commonService.updateBoardHits(boardSeq);
			request.getSession().setAttribute("S_BOARD_SEQ", param.get("board_seq"));
		}
		return JSON_VIEW;
	}
	
	//OSCE perbaiki화면
	@RequestMapping(value = "/common/learning/osce/modify", method = RequestMethod.POST)
	public String osceModifyView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		int sUserLevel = (int)request.getSession().getAttribute("S_USER_LEVEL");
		if (sUserLevel > 3) {
			return "redirect:/";
		}
		model.addAllAttributes(param);
		model.addAttribute("code_name", commonService.getCodeName("osce_code_name"));
		return "common/learning/osce/osceModify";
	}
	
	//OSCE perbaiki상세
	@RequestMapping(value = "/ajax/common/learning/osce/modify/detail", method = RequestMethod.GET)
	public String osceModifyDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.OSCE_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(service.getOSCEDetail(param));
		return JSON_VIEW;
	}
	
	//OSCE perbaiki
	@RequestMapping(value = "/ajax/common/learning/osce/modify", method = RequestMethod.POST)
	public String osceModify(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.OSCE_BOARD_CODE);
		param.put("board_cate_code", param.get("boardCateCode"));
		param.put("board_seq", param.get("boardSeq"));
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(commonService.boardModify(param, request));
		return JSON_VIEW;
	}
	
	/************************************************************************************************/
	/*                                         임상표현(CPX) 동영상                                                                 */
	/************************************************************************************************/
	
	//CPX
	@RequestMapping(value = "/common/learning/cpx/list", method = RequestMethod.GET)
	public String cpxListView(HttpServletRequest request) throws Exception {
		return "common/learning/cpx/cpxList";
	}
	
	//CPX 목록
	@RequestMapping(value = "/ajax/common/learning/cpx/list", method = RequestMethod.GET)
	public String cpxList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.CPX_BOARD_CODE);
		param.put("search_cpx_code", param.get("searchCPXCode"));
		param.put("listFunctionName", "boardListView");
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		model.addAllAttributes(service.getCPXList(param));
		return JSON_VIEW;
	}
	
	//CPX CODE 목록
	@RequestMapping(value = "/ajax/common/learning/cpx/code/list", method = RequestMethod.GET)
	public String cpxCodeList(Model model) throws Exception {
		model.addAllAttributes(service.getCPXCodeList());
		return JSON_VIEW;
	}
	
	//CPX  registrasi 화면
	@RequestMapping(value = "/common/learning/cpx/create", method = RequestMethod.GET)
	public String cpxCreateView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAttribute("code_name", commonService.getCodeName("cpx_code_name"));
		return "common/learning/cpx/cpxCreate";
	}
	
	//CPX registrasi
	@RequestMapping(value = "/ajax/common/learning/cpx/create", method = RequestMethod.POST)
	public String cpxCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.CPX_BOARD_CODE);
		param.put("board_cate_code", param.get("boardCateCode"));
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(commonService.boardCreate(param, request));
		return JSON_VIEW;
	}
	
	//CPX 상세화면
	@RequestMapping(value = "/common/learning/cpx/detail", method = RequestMethod.GET)
	public String cpxDetailView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "common/learning/cpx/cpxDetail";
	}
	
	//CPX 상세
	@RequestMapping(value = "/ajax/common/learning/cpx/detail", method = RequestMethod.GET)
	public String cpxDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("s_board_seq", request.getSession().getAttribute("S_BOARD_SEQ"));
		param.put("board_code", Constants.CPX_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(service.getCPXDetail(param));
		String sBoardSeq = (String)param.get("s_board_seq");
		String boardSeq = (String)param.get("board_seq");
		if (sBoardSeq == null || !sBoardSeq.equals(boardSeq)) {
			commonService.updateBoardHits(boardSeq);
			request.getSession().setAttribute("S_BOARD_SEQ", param.get("board_seq"));
		}
		return JSON_VIEW;
	}
	
	//CPX perbaiki화면
	@RequestMapping(value = "/common/learning/cpx/modify", method = RequestMethod.POST)
	public String cpxModifyView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		int sUserLevel = (int)request.getSession().getAttribute("S_USER_LEVEL");
		if (sUserLevel > 3) {
			return "redirect:/";
		}
		model.addAllAttributes(param);
		model.addAttribute("code_name", commonService.getCodeName("cpx_code_name"));
		return "common/learning/cpx/cpxModify";
	}
	
	//CPX perbaiki상세
	@RequestMapping(value = "/ajax/common/learning/cpx/modify/detail", method = RequestMethod.GET)
	public String cpxModifyDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.CPX_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(service.getCPXDetail(param));
		return JSON_VIEW;
	}
	
	//CPX perbaiki
	@RequestMapping(value = "/ajax/common/learning/cpx/modify", method = RequestMethod.POST)
	public String cpxModify(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.CPX_BOARD_CODE);
		param.put("board_cate_code", param.get("boardCateCode"));
		param.put("board_seq", param.get("boardSeq"));
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(commonService.boardModify(param, request));
		return JSON_VIEW;
	}

	/************************************************************************************************/
	/*                                         PBL                                                  */
	/************************************************************************************************/
	
	//PBL
	@RequestMapping(value = "/common/learning/pbl/list", method = RequestMethod.GET)
	public String pblListView(HttpServletRequest request) throws Exception {
		return "common/learning/pbl/pblList";
	}
	
	//PBL 목록
	@RequestMapping(value = "/ajax/common/learning/pbl/list", method = RequestMethod.GET)
	public String pblList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.PBL_BOARD_CODE);
		param.put("listFunctionName", "pblListView");
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		model.addAllAttributes(service.getPBLList(param));
		return JSON_VIEW;
	}

	//PBL 상세화면
	@RequestMapping(value = "/common/learning/pbl/detail", method = RequestMethod.GET)
	public String pblDetailView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "common/learning/pbl/pblDetail";
	}
	
	//PBL 상세
	@RequestMapping(value = "/ajax/common/learning/pbl/detail", method = RequestMethod.GET)
	public String pblDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("s_board_seq", request.getSession().getAttribute("S_BOARD_SEQ"));
		param.put("board_code", Constants.PBL_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq"});
		model.addAllAttributes(service.getPBLDetail(param));
		
		String sBoardSeq = (String)param.get("s_board_seq");
		String boardSeq = (String)param.get("board_seq");
		if (sBoardSeq == null || !sBoardSeq.equals(boardSeq)) {
			commonService.updateBoardHits(boardSeq);
			request.getSession().setAttribute("S_BOARD_SEQ", param.get("board_seq"));
		}
		return JSON_VIEW;
	}
	
}
