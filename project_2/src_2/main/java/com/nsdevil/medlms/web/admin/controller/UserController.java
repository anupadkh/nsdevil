package com.nsdevil.medlms.web.admin.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.admin.service.UserService;
import com.nsdevil.medlms.web.common.service.CommonService;

@Controller
public class UserController extends ExceptionController {
	
	@Autowired
	private UserService service;
	
	@Autowired
	private CommonService commonService;
	
	@RequestMapping(value = "/admin/user", method = RequestMethod.GET)
	public String userView (HttpServletRequest request, @RequestParam HashMap<String, Object> param) {
		return "redirect:/admin/user/pf/list";
	}	
	
	/************************************************************************************************/
	/*                                    이용자 registrasi정보 manajemen                                                                             */
	/************************************************************************************************/
	
	//재직 목록manajemen
	@RequestMapping(value = "/admin/user/code/attend/list", method = RequestMethod.GET)
	public String attendListView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "admin/user/code/attendList";
	}
	
	//재직 목록
	@RequestMapping(value = "/ajax/admin/user/code/attend/list", method = RequestMethod.GET)
	public String attendList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("code_cate", "attend");
		param.put("code_column_name", "attend_code");
		model.addAllAttributes(service.getAttendCountList(param));
		return JSON_VIEW;
	}
	
	//재직 perbaiki 목록
	@RequestMapping(value = "/admin/user/code/attend/modify", method = RequestMethod.GET)
	public String attendModifyView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "admin/user/code/attendModify";
	}
	
	//posisi perbaiki
	@RequestMapping(value = "/ajax/admin/user/code/attend/modify", method = RequestMethod.POST)
	public String attendModify (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("code_cate", "attend");
		param.put("remove_code_column", "attend_code");
		param.put("remove_user_level", 2);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.codeListModify(param));
		return JSON_VIEW;
	}
	
	//posisi 목록manajemen
	@RequestMapping(value = "/admin/user/code/position/list", method = RequestMethod.GET)
	public String postionListView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "admin/user/code/positionList";
	}
	
	//posisi 목록
	@RequestMapping(value = "/ajax/admin/user/code/position/list", method = RequestMethod.GET)
	public String positionList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("code_cate", "position");
		param.put("code_column_name", "position_code");
		param.put("user_level", 3);
		model.addAllAttributes(service.getCodeCountList(param));
		return JSON_VIEW;
	}
	
	//posisi 목록manajemen
	@RequestMapping(value = "/admin/user/code/position/modify", method = RequestMethod.GET)
	public String postionModifyView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "admin/user/code/positionModify";
	}
	
	//posisi perbaiki
	@RequestMapping(value = "/ajax/admin/user/code/position/modify", method = RequestMethod.POST)
	public String positionModify (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("code_cate", "position");
		param.put("remove_code_column", "position_code");
		param.put("remove_user_level", 3);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.codeListModify(param));
		return JSON_VIEW;
	}
	
	//교수 departement 목록manajemen
	@RequestMapping(value = "/admin/user/code/department/list", method = RequestMethod.GET)
	public String departmentListView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "admin/user/code/departmentList";
	}
	
	//교수 departement 목록
	@RequestMapping(value = "/ajax/admin/user/code/department/list", method = RequestMethod.GET)
	public String departmentList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("code_cate", "department");
		param.put("code_column_name", "department_code");
		param.put("user_level", 3);
		model.addAllAttributes(service.getCodeCountList(param));
		return JSON_VIEW;
	}
	
	//교수 departement 목록manajemen
	@RequestMapping(value = "/admin/user/code/department/modify", method = RequestMethod.GET)
	public String departmentModifyView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "admin/user/code/departmentModify";
	}
	
	//교수 departement perbaiki
	@RequestMapping(value = "/ajax/admin/user/code/department/modify", method = RequestMethod.POST)
	public String departmentModify (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("code_cate", "department");
		param.put("remove_code_column", "department_code");
		param.put("remove_user_level", 3);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.codeListModify(param));
		return JSON_VIEW;
	}
	
	//직원 departement 목록manajemen
	@RequestMapping(value = "/admin/user/code/staffDepartment/list", method = RequestMethod.GET)
	public String staffDepartmentListView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "admin/user/code/staffDepartmentList";
	}
	
	//직원 departement 목록
	@RequestMapping(value = "/ajax/admin/user/code/staffDepartment/list", method = RequestMethod.GET)
	public String staffDepartmentList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("code_cate", "staff_department");
		param.put("code_column_name", "department_code");
		param.put("user_level", 2);
		model.addAllAttributes(service.getCodeCountList(param));
		return JSON_VIEW;
	}
	
	//직원 departement 목록manajemen
	@RequestMapping(value = "/admin/user/code/staffDepartment/modify", method = RequestMethod.GET)
	public String staffDepartmentModifyView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "admin/user/code/staffDepartmentModify";
	}
	
	//직원 departement perbaiki
	@RequestMapping(value = "/ajax/admin/user/code/staffDepartment/modify", method = RequestMethod.POST)
	public String staffDepartmentModify (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("code_cate", "staff_department");
		param.put("remove_code_column", "department_code");
		param.put("remove_user_level", 2);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.codeListModify(param));
		return JSON_VIEW;
	}
	
	//jurusan khusus 목록manajemen
	@RequestMapping(value = "/admin/user/code/specialty/list", method = RequestMethod.GET)
	public String specialtyListView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "admin/user/code/specialtyList";
	}
	
	//교수manajemen jurusan khusus 목록
	@RequestMapping(value = "/ajax/admin/user/code/specialty/list", method = RequestMethod.GET)
	public String specialtyList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("code_cate", "specialty");
		param.put("code_column_name", "specialty");
		param.put("user_level", 3);
		model.addAllAttributes(service.getCodeCountList(param));
		return JSON_VIEW;
	}
	
	//교수manajemen jurusan khusus 목록manajemen
	@RequestMapping(value = "/admin/user/code/specialty/modify", method = RequestMethod.GET)
	public String specialtyModifyView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "admin/user/code/specialtyModify";
	}
	
	//교수manajemen jurusan khusus perbaiki
	@RequestMapping(value = "/ajax/admin/user/code/specialty/modify", method = RequestMethod.POST)
	public String specialtyModify (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("code_cate", "specialty");
		param.put("remove_code_column", "specialty");
		param.put("remove_user_level", 3);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.codeListModify(param));
		return JSON_VIEW;
	}
	
	/************************************************************************************************/
	/*                                        교수 manajemen                                                                                   */
	/************************************************************************************************/
	
	//교수manajemen 목록 화면
	@RequestMapping(value = "/admin/user/pf/list", method = RequestMethod.GET)
	public String pfListView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "admin/user/pf/pfList";
	}
	
	//sistem akademik
	@RequestMapping(value = "/ajax/admin/user/academic/list", method = RequestMethod.GET)
	public String userAcademicList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(service.getAcademicList(param));
		return JSON_VIEW;
	}
	
	//교수manajemen 목록
	@RequestMapping(value = "/ajax/admin/user/pf/list", method = RequestMethod.GET)
	public String pfList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		param.put("search_year", param.get("searchYear"));
		param.put("search_l_seq", param.get("searchLSeq"));
		param.put("search_m_seq", param.get("searchMSeq"));
		param.put("search_s_seq", param.get("searchSSeq"));
		param.put("search_p_seq", param.get("searchPSeq"));
		param.put("search_curr_name", param.get("searchCurrName"));
		param.put("search_department_name", param.get("searchDepartmentName"));
		param.put("search_user_name", param.get("searchUserName"));
		param.put("search_professor_id", param.get("searcPropessorId"));
		
		String searchFlag = "";//search_flag : Y JOIN 나머지 LEFT JOIN
		//sistem akademik
		String searchYear = Util.nvl(String.valueOf(param.get("search_year"))); //tahun
		String searchLSeq = Util.nvl(String.valueOf(param.get("search_l_seq"))); //fakultas
		String searchMSeq = Util.nvl(String.valueOf(param.get("search_m_seq"))); //jurusan
		String searchSSeq = Util.nvl(String.valueOf(param.get("search_s_seq"))); //tahun ajaran
		String searchPSeq = Util.nvl(String.valueOf(param.get("search_p_seq"))); //periode
		String searchCurrName = Util.nvl(String.valueOf(param.get("search_curr_name")));
		if (!searchYear.equals("") ||
				!searchLSeq.equals("") ||
				!searchMSeq.equals("") ||
				!searchSSeq.equals("") ||
				!searchPSeq.equals("") ||
				!searchCurrName.equals("")
				) {
			searchFlag = "Y";
		}
		param.put("search_flag", searchFlag);
		
		param.put("listFunctionName", "pfListView");
		model.addAllAttributes(service.getPFList(param));
		return JSON_VIEW;
	}
	
	@RequestMapping(value = "/ajax/admin/user/code/list", method = RequestMethod.GET)
	public String getPFCodeList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("position_code_list", commonService.getCodeList("position"));//posisi
		resultMap.put("department_code_list", commonService.getCodeList("department"));//departement
		resultMap.put("specialty_code_list", commonService.getCodeList("specialty"));//jurusan khusus
		resultMap.put("attend_code_list", commonService.getCodeList("attend"));//재직상태
		resultMap.put("staff_department_code_list", commonService.getCodeList("staff_department"));//직원 departement
		model.addAllAttributes(resultMap);
		return JSON_VIEW;
	}
	
	//교수manajemen registrasi 화면
	@RequestMapping(value = "/admin/user/pf/create", method = RequestMethod.GET)
	public String pfCreateView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		return "admin/user/pf/pfCreate";
	}
	
	//교수manajemen registrasi
	@RequestMapping(value = "/ajax/admin/user/pf/create", method = RequestMethod.POST)
	public String pfCreate (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"userName", "userEmail"});
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.pfCreate(request, param));
		return JSON_VIEW;
	}
	
	//교수manajemen 상세 화면
	@RequestMapping(value = "/admin/user/pf/detail", method = RequestMethod.POST)
	public String pfDetailView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", param.get("userSeq"));
		model.addAllAttributes(param);
		return "admin/user/pf/pfDetail";
	}
	
	//교수manajemen 상세
	@RequestMapping(value = "/ajax/admin/user/pf/detail", method = RequestMethod.GET)
	public String getPFDetail (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"user_seq"});
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.getPFDetail(param));
		return JSON_VIEW;
	}
	
	//비밀번호 pengaturan ulang
	@RequestMapping(value = "/ajax/admin/user/pwd/init", method = RequestMethod.GET)
	public String userPwdInitModify (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"user_seq"});
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.userPwdInitModify(param));
		return JSON_VIEW;
	}
	
	//교수manajemen perbaiki 화면
	@RequestMapping(value = "/admin/user/pf/modify", method = RequestMethod.POST)
	public String pfModifyView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", param.get("userSeq"));
		model.addAllAttributes(param);
		return "admin/user/pf/pfModify";
	}
	
	//교수manajemen perbaiki
	@RequestMapping(value = "/ajax/admin/user/pf/modify", method = RequestMethod.POST)
	public String pfModify (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"userName", "userEmail", "userSeq"});
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("user_seq", param.get("userSeq"));
		model.addAllAttributes(service.pfModify(request, param));
		return JSON_VIEW;
	}
	
	//교수manajemen 교수 registrasi 엑셀 양식 다운로드
	@RequestMapping(value = "/admin/user/pf/excel/template/down", method = RequestMethod.GET)
	public String pfExcelTemplateDown(HttpServletRequest request, Model model, @RequestParam HashMap<String, Object> param, HttpServletResponse response) throws Exception {
		String curDate = (new SimpleDateFormat("yyyyMMdd")).format(new Date());
		model.addAttribute("excel_view_type", "professorUploadTemplate");
		model.addAttribute("position_list", commonService.getCodeList("position")); //posisi
		model.addAttribute("department_list", commonService.getCodeList("department")); //departement
		model.addAttribute("specialty_list", commonService.getCodeList("specialty")); //jurusan khusus
		response.setHeader("Content-disposition", "attachment; filename=" + "professorUploadTemplate_"+curDate+".xlsx");
		return "excelDownloadView";
	}
	
	//교수manajemen 교수 엑셀 일괄 registrasi
	@RequestMapping(value = "/ajax/admin/user/pf/excel/create", method = RequestMethod.POST)
	public String pfExcelCreate (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"uploadFile"});
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.pfExcelCreate(request, param));
		return JSON_VIEW;
	}
	
	/************************************************************************************************/
	/*                                        직원 manajemen                                                                                   */
	/************************************************************************************************/
	
	//직원manajemen 목록 화면
	@RequestMapping(value = "/admin/user/staff/list", method = RequestMethod.GET)
	public String staffListView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "admin/user/staff/staffList";
	}
	
	
	//직원manajemen registrasi 화면
	@RequestMapping(value = "/admin/user/staff/create", method = RequestMethod.GET)
	public String staffCreateView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		return "admin/user/staff/staffCreate";
	}
	
	//직원manajemen registrasi
	@RequestMapping(value = "/ajax/admin/user/staff/create", method = RequestMethod.POST)
	public String staffCreate (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"userName", "userEmail"});
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.staffCreate(request, param));
		return JSON_VIEW;
	}
	
	//직원manajemen 목록
	@RequestMapping(value = "/ajax/admin/user/staff/list", method = RequestMethod.GET)
	public String staffList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("search_type", param.get("searchType"));
		param.put("search_order_type", param.get("searchOrderType"));
		param.put("search_order", param.get("searchOrder"));
		param.put("search_account_use_state", param.get("searchAccountUseState"));
		param.put("search_text", param.get("searchText"));
		param.put("listFunctionName", "staffListView");
		model.addAllAttributes(service.getStaffList(param));
		return JSON_VIEW;
	}
	
	//직원manajemen 상세 화면
	@RequestMapping(value = "/admin/user/staff/detail", method = RequestMethod.POST)
	public String staffDetailView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", param.get("userSeq"));
		model.addAllAttributes(param);
		return "admin/user/staff/staffDetail";
	}
	
	//직원manajemen 상세
	@RequestMapping(value = "/ajax/admin/user/staff/detail", method = RequestMethod.GET)
	public String getStaffDetail (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"user_seq"});
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.getStaffDetail(param));
		return JSON_VIEW;
	}
	
	//교수manajemen perbaiki 화면
	@RequestMapping(value = "/admin/user/staff/modify", method = RequestMethod.POST)
	public String staffModifyView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", param.get("userSeq"));
		model.addAllAttributes(param);
		return "admin/user/staff/staffModify";
	}
	
	@RequestMapping(value = "/ajax/admin/user/staff/modify", method = RequestMethod.POST)
	public String staffModify (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"userName", "userEmail", "userSeq"});
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("user_seq", param.get("userSeq"));
		model.addAllAttributes(service.staffModify(request, param));
		return JSON_VIEW;
	}
	
	
}

