package com.nsdevil.medlms.web.common.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.constants.Constants;
import com.nsdevil.medlms.common.exception.ResourceNotFoundException;
import com.nsdevil.medlms.common.util.PagingHelper;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.common.dao.CommonDao;
import com.nsdevil.medlms.web.common.dao.LearningDao;

@Service
public class LearningService {
	
	@Value("#{config['config.rootPath']}")
	private String ROOT_PATH;
	
	@Value("#{config['config.resPath']}")
	private String RES_PATH;
	
	@Value("#{config['config.boardPath']}")
	private String BOARD_PATH;
	
	@Autowired
	private LearningDao dao;
	
	@Autowired
	private CommonDao commonDao;
	
	public ResultMap getCPXList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		int totalCnt = dao.getCPXContentsCount(param);
		PagingHelper pagingHelper = new PagingHelper(6 ,10);
		List<HashMap<String, Object>> ContentsList = dao.getCPXContentsList(pagingHelper.getPagingParam(param, totalCnt));
		resultMap.putAll(pagingHelper.getPageList(ContentsList, (String)param.get("listFunctionName"), totalCnt));
		return resultMap;
	}
	
	public ResultMap getOSCEList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		int totalCnt = dao.getOSCEContentsCount(param);
		PagingHelper pagingHelper = new PagingHelper(6 ,10);
		List<HashMap<String, Object>> ContentsList = dao.getOSCEContentsList(pagingHelper.getPagingParam(param, totalCnt));
		resultMap.putAll(pagingHelper.getPageList(ContentsList, (String)param.get("listFunctionName"), totalCnt));
		return resultMap;
	}
	
	public ResultMap getOSCECodeList() throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("code_list", dao.getOSCECodeList());
		return resultMap;
	}
	
	public ResultMap getCPXCodeList() throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("code_list", dao.getCPXCodeList());
		return resultMap;
	}
	
	public ResultMap getOSCEDetail(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		HashMap<String, Object> boardDetailInfo = new HashMap<String, Object>();
		boardDetailInfo = dao.getOSCEDetailInfo(param);
		if (boardDetailInfo == null || boardDetailInfo.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_NOTICE_INFO", "007");
		}
		List<HashMap<String, Object>> attachList = new ArrayList<HashMap<String, Object>>();
		attachList = commonDao.getBoardAttachFileList(param);
		HashMap<String, Object> nextBoardInfo = new HashMap<String, Object>();
		nextBoardInfo = commonDao.getNextBoardInfo(param);
		HashMap<String, Object> prevBoardInfo = new HashMap<String, Object>();
		prevBoardInfo = commonDao.getPrevBoardInfo(param);
		resultMap.put("board_info", boardDetailInfo);
		resultMap.put("attach_list", attachList);
		resultMap.put("next_board_info", nextBoardInfo);
		resultMap.put("prev_board_info", prevBoardInfo);
		return resultMap;
	}
	
	public ResultMap getCPXDetail(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		HashMap<String, Object> boardDetailInfo = new HashMap<String, Object>();
		boardDetailInfo = dao.getCPXDetailInfo(param);
		if (boardDetailInfo == null || boardDetailInfo.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_NOTICE_INFO", "007");
		}
		
		List<HashMap<String, Object>> attachList = new ArrayList<HashMap<String, Object>>();
		attachList = commonDao.getBoardAttachFileList(param);
		HashMap<String, Object> nextBoardInfo = new HashMap<String, Object>();
		nextBoardInfo = commonDao.getNextBoardInfo(param);
		HashMap<String, Object> prevBoardInfo = new HashMap<String, Object>();
		prevBoardInfo = commonDao.getPrevBoardInfo(param);
		resultMap.put("board_info", boardDetailInfo);
		resultMap.put("attach_list", attachList);
		resultMap.put("next_board_info", nextBoardInfo);
		resultMap.put("prev_board_info", prevBoardInfo);
		return resultMap;
	}
	
	@SuppressWarnings("unchecked")
	public ResultMap getPBLList(HashMap<String, Object> param) throws Exception {
		
		ResultMap resultMap = new ResultMap();
		
		HashMap<String, Object> academicSystemInfo = new HashMap<String, Object>();
		if (param.get("s_user_level") != null ) {
			int sUserLevel = (int) param.get("s_user_level");
			if (sUserLevel == Constants.STUDENT_LEVEL) {
				academicSystemInfo = commonDao.getAcademicSystemInfo(param);
			}
			
			if (academicSystemInfo != null && !academicSystemInfo.isEmpty()) {
				param.put("l_seq", academicSystemInfo.get("l_seq"));
				param.put("m_seq", academicSystemInfo.get("m_seq"));
				param.put("s_seq", academicSystemInfo.get("s_seq"));
				
				if (academicSystemInfo.get("s_seq") == null) {
					param.put("s_seq", academicSystemInfo.get("ss_seq"));
				}
			}
		}
		
		int totalCnt = dao.getPBLContentsCount(param);
		PagingHelper pagingHelper = new PagingHelper();
		List<HashMap<String, Object>> contentsList = dao.getPBLContentsList(pagingHelper.getPagingParam(param, totalCnt));
		for (HashMap<String, Object> map : contentsList) {
			List<HashMap<String, Object>> targetList = new ArrayList<HashMap<String, Object>>();
			//공지 Target이 학생(02)이면서 학생 Keseluruhan가 아닌 tahun ajaran 별일때
			if (map.get("aca_seq_str") != null) {
				String acaSeqStr = String.valueOf(map.get("aca_seq_str"));
				if (!acaSeqStr.equals("")) {
					String[] acaSeqArr = acaSeqStr.split("\\|\\|");
					
					if (acaSeqArr.length > 1) { //정렬
						Arrays.sort(acaSeqArr);
					}
					
					for (String acaSeq : acaSeqArr) {
						HashMap<String, Object> tmp = new HashMap<String, Object>();
						tmp = commonDao.getAcademicSystemInfoForSeq(acaSeq);
						if (tmp != null && !tmp.isEmpty()) {
							String targetName = "";
							String lAcaName = Util.nvl(String.valueOf(tmp.get("l_aca_name")));
							String mAcaName = Util.nvl(String.valueOf(tmp.get("m_aca_name")));
							String currAcaName = Util.nvl(String.valueOf(tmp.get("curr_aca_name")));
							int level = (int) tmp.get("level"); //1:대, 2:중, 3:소, 4:periode
							if (level == 1) {
								targetName = currAcaName;
							} else if (level == 2) {
								targetName = lAcaName + " " +currAcaName;
							} else if (level == 3) {
								targetName = mAcaName + " " + currAcaName;
							}
							
							if (!targetName.equals("")) {
								tmp.put("target_name", targetName);
								targetList.add((HashMap<String, Object>)tmp.clone());
							}
						}
					}
				}
			}
			map.put("target_list", targetList);
		}
		resultMap.putAll(pagingHelper.getPageList(contentsList, (String)param.get("listFunctionName"), totalCnt));
		return resultMap;
	}
	
	@SuppressWarnings("unchecked")
	public ResultMap getPBLDetail(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		HashMap<String, Object> academicSystemInfo = new HashMap<String, Object>();
		if (param.get("s_user_level") != null ) {
			int sUserLevel = (int) param.get("s_user_level");
			if (sUserLevel == Constants.STUDENT_LEVEL) {
				academicSystemInfo = commonDao.getAcademicSystemInfo(param);
			}
			
			if (academicSystemInfo != null && !academicSystemInfo.isEmpty()) {
				param.put("l_seq", academicSystemInfo.get("l_seq"));
				param.put("m_seq", academicSystemInfo.get("m_seq"));
				param.put("s_seq", academicSystemInfo.get("s_seq"));
				
				if (academicSystemInfo.get("s_seq") == null) {
					param.put("s_seq", academicSystemInfo.get("ss_seq"));
				}
			}
		}
		HashMap<String, Object> pblDetailInfo = new HashMap<String, Object>();
		pblDetailInfo = dao.getPBLDetailInfo(param);
		if (pblDetailInfo == null || pblDetailInfo.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_NOTICE_INFO", "007");
		}
		

		List<HashMap<String, Object>> targetList = new ArrayList<HashMap<String, Object>>();
		//공지 Target이 학생(03)이면서 학생 Keseluruhan가 아닌 tahun ajaran 별일때
		if (pblDetailInfo.get("aca_seq_str") != null) {
			String acaSeqStr = String.valueOf(pblDetailInfo.get("aca_seq_str"));
			if (!acaSeqStr.equals("")) {
				String[] acaSeqArr = acaSeqStr.split("\\|\\|");
				
				if (acaSeqArr.length > 1) { //정렬
					Arrays.sort(acaSeqArr);
				}
				
				for (String acaSeq : acaSeqArr) {
					HashMap<String, Object> tmp = new HashMap<String, Object>();
					tmp = commonDao.getAcademicSystemInfoForSeq(acaSeq);
					if (tmp != null && !tmp.isEmpty()) {
						String targetName = "";
						String lAcaName = Util.nvl(String.valueOf(tmp.get("l_aca_name")));
						String mAcaName = Util.nvl(String.valueOf(tmp.get("m_aca_name")));
						String currAcaName = Util.nvl(String.valueOf(tmp.get("curr_aca_name")));
						int level = (int) tmp.get("level"); //1:대, 2:중, 3:소, 4:periode
						if (level == 1) {
							targetName = currAcaName;
						} else if (level == 2) {
							targetName = lAcaName + " " +currAcaName;
						} else if (level == 3) {
							targetName = mAcaName + " " + currAcaName;
						}
						
						if (!targetName.equals("")) {
							tmp.put("target_name", targetName);
							targetList.add((HashMap<String, Object>)tmp.clone());
						}
					}
				}
			}
		}
		
		List<HashMap<String, Object>> attachList = new ArrayList<HashMap<String, Object>>();
		attachList = dao.getPBLAttachFileList(param);
		for (HashMap<String, Object> map : attachList) {
			map.put("file_path", RES_PATH + map.get("file_path"));
		}
		
		HashMap<String, Object> nextPBLInfo = new HashMap<String, Object>();
		nextPBLInfo = dao.getNextPBLInfo(param);
		HashMap<String, Object> prevPBLInfo = new HashMap<String, Object>();
		prevPBLInfo = dao.getPrevPBLInfo(param);
		
		resultMap.put("pbl_info", pblDetailInfo);
		resultMap.put("attach_list", attachList);
		resultMap.put("next_pbl_info", nextPBLInfo);
		resultMap.put("target_list", targetList);
		resultMap.put("prev_pbl_info", prevPBLInfo);
		return resultMap;
	}
	
}
