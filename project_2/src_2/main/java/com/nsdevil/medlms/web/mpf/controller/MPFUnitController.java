package com.nsdevil.medlms.web.mpf.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.web.common.service.CommonService;
import com.nsdevil.medlms.web.mpf.service.MPFUnitService;

@Controller
public class MPFUnitController extends ExceptionController {
	
	@Autowired
	private MPFUnitService unitService;
	
	@Autowired
	private CommonService commonService;

	@RequestMapping(value = "/pf/lesson/unit", method = RequestMethod.GET)
	public String unitView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ"));
		model.addAllAttributes(commonService.getAcaState(param));
		model.addAllAttributes(commonService.getCurrFlagInfo(param));
		return "mpf/lesson/unit";
	}
	
	//수업계획 리스트 가져오기
	@RequestMapping(value = "/ajax/mpf/lesson/unit/lessonScheduleList", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String lessonScheduleList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		model.addAttribute("lessonList", unitService.lessonScheduleList(param));
		return JSON_VIEW;
	}	
	
	//단원 simpan
	@RequestMapping(value = "/ajax/mpf/lesson/unit/create", method = RequestMethod.POST)
	public String unitCreate(HttpServletRequest request,  Model model) throws Exception {
		String[] unit_seq = request.getParameterValues("unit_seq");
		String[] unit_name = request.getParameterValues("unit_name");
		String[] lp_seq = request.getParameterValues("lp_seq");
		String[] tlo_seq = request.getParameterValues("tlo_seq");
		String[] skill = request.getParameterValues("skill");
		String[] teaching_method = request.getParameterValues("teaching_method");
		String[] evaluation_method = request.getParameterValues("evaluation_method");
		String[] elo_seq = request.getParameterValues("elo_seq");
		String[] domain_code = request.getParameterValues("domain_code");
		String[] level_code = request.getParameterValues("level_code");
		String[] content = request.getParameterValues("content");
		
		model.addAllAttributes(unitService.createUnit(request.getSession().getAttribute("S_USER_SEQ").toString(), request.getSession().getAttribute("S_CURRICULUM_SEQ").toString(),
				unit_seq, unit_name, lp_seq, skill, teaching_method, evaluation_method, domain_code, level_code, content, tlo_seq, elo_seq
				));
		return JSON_VIEW;
	}
	
	//단원 리스트 가져오기
	@RequestMapping(value = "/ajax/mpf/lesson/unit/unitList", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String unitList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception{
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		model.addAllAttributes(unitService.unitList(param));
		return JSON_VIEW;
	}	
	
	//단원 리스트 가져오기
	@RequestMapping(value = "/ajax/mpf/lesson/unit/unitCode", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String unitCode(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception{
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		model.addAllAttributes(unitService.unitCode(param));
		return JSON_VIEW;
	}	
	
	//수업tambahkan현황 가져오기
	@RequestMapping(value = "/ajax/mpf/lesson/unit/lessonAddPC", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String lessonAddPC(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		model.addAttribute("AddPCList", unitService.lessonAddPCList(param));
		return JSON_VIEW;
	}	
}
