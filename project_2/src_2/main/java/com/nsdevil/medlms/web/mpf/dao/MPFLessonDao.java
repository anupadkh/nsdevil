package com.nsdevil.medlms.web.mpf.dao;

import java.util.List;
import java.util.HashMap;


public interface MPFLessonDao {

	public List<HashMap<String, Object>> getprofessor(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getLessonPlanRegCs(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getLessonPlanTlo(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getLessonPlanElo(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getAbsenceSTList(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> preNextLpSeq(HashMap<String, Object> param) throws Exception;
}
