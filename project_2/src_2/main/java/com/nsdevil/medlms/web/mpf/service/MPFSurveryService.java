package com.nsdevil.medlms.web.mpf.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.web.admin.dao.AcademicDao;
import com.nsdevil.medlms.web.mpf.dao.MPFSurveryDao;
import com.nsdevil.medlms.web.pf.dao.PFLessonDao;

@Service
public class MPFSurveryService {
	
	@Autowired
	private MPFSurveryDao mpfSurveryDao;
	
	@Autowired
	private PFLessonDao pfLessonDao;
	
	@Autowired
	private AcademicDao academicDao;
	
	public ResultMap getCurrSFSurveyInfo(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("currInfo", pfLessonDao.getCurriculumBasicInfo(param));
		resultMap.put("stCnt", academicDao.getCurrSRSurveyInfo(param));
		return resultMap;
	}

	public ResultMap getlessonPlanSFSurveyInfo(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("surveyList", mpfSurveryDao.getLessonPlanSFSurveyCS(param));
		return resultMap;
	}
	
	
}