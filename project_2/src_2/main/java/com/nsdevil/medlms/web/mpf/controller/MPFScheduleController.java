package com.nsdevil.medlms.web.mpf.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.common.service.CommonService;
import com.nsdevil.medlms.web.mpf.service.MPFScheduleService;

@Controller
public class MPFScheduleController extends ExceptionController {
	
	@Autowired
	private MPFScheduleService mpfScheduleService;

	@Autowired
	private CommonService commonService;
	
	@RequestMapping(value = "/pf/lesson/schedule", method = RequestMethod.GET)
	public String scheduleView(HttpServletRequest request, Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		model.addAllAttributes(commonService.getAcaState(param));
		model.addAllAttributes(commonService.getCurrFlagInfo(param));
		return "mpf/lesson/schedule";
	}
	
	@RequestMapping(value = "/pf/lesson/scheduleM", method = RequestMethod.GET)
	public String scheduleMonthView(HttpServletRequest request) throws Exception {
		return "mpf/lesson/scheduleM";
	}
	
	@RequestMapping(value = "/pf/lesson/scheduleMod", method = RequestMethod.GET)
	public String scheduleModView(HttpServletRequest request, Model model, @RequestParam HashMap<String, Object> param) throws Exception {
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		model.addAllAttributes(commonService.getAcaState(param));
		model.addAllAttributes(commonService.getCurrFlagInfo(param));
		return "mpf/lesson/scheduleModify";
	}
	
	@RequestMapping(value = "/pf/lesson/scheduleSearch", method = RequestMethod.GET)
	public String scheduleSearchView(HttpServletRequest request) throws Exception {
		return "mpf/lesson/scheduleSearch";
	}
	
	@RequestMapping(value = "/pf/lesson/academicM", method = RequestMethod.GET)
	public String academicMonthView(HttpServletRequest request) throws Exception {
		return "mpf/lesson/academicM";
	}
	
	@RequestMapping(value = "/ajax/pf/lesson/getSchedule", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String getSchedule(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		
		HashMap<String, Object> map = mpfScheduleService.getSchedule(param);
		model.addAllAttributes(map);
		return JSON_VIEW;
	}		
	
	@RequestMapping(value = "/ajax/pf/lesson/getSelectedDaySchedule", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String getSelectedDaySchedule(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[]{"curr_seq"});
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAttribute("schedule", mpfScheduleService.getSelectedDaySchedule(param));
		return JSON_VIEW;
	}
	
	/**
	 * 엑셀 업로드
	 * @param request
	 * @param param
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/ajax/pf/lesson/uploadScheduleXls", method = RequestMethod.POST)
	public String uploadScheduleXls(MultipartHttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		
		MultipartFile uploadFile = request.getFile("xlsFile");
		
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		
		model.addAllAttributes(mpfScheduleService.uploadScheduleXls(uploadFile, param));
		
		return JSON_VIEW;
	}
	
	/**
	 * 일정 상세
	 * @param request
	 * @param param
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/ajax/pf/lesson/getScheduleDetail", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String getScheduleDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
				
		HashMap<String, Object> map = mpfScheduleService.getScheduleDetail(param);
		model.addAllAttributes(map);
		return JSON_VIEW;
	}
	
	@RequestMapping(value = "/ajax/pf/lesson/saveSchedule", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String saveSchedule(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[]{"lesson_subject", "lesson_date", "period", "start_time", "end_time","curr_seq"});
		
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		
		model.addAllAttributes(mpfScheduleService.saveSchedule(param));
		return JSON_VIEW;
	}
	
	@RequestMapping(value = "/ajax/pf/lesson/changeEventDate", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String changeEventDate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[]{"event_seq", "event_type", "event_date", "start_time", "end_time"});
		
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		param.put("userLevel", request.getSession().getAttribute("S_USER_LEVEL"));
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		
		model.addAllAttributes(mpfScheduleService.changeEventDate(param));
		return JSON_VIEW;
	}
	
	@RequestMapping(value = "/ajax/pf/lesson/getPfList", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String getPfList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[]{"user_name"});
		HashMap<String, Object> map = mpfScheduleService.getPfList(param);
		map.put("user_name", param.get("user_name"));
		model.addAllAttributes(map);
		return JSON_VIEW;
	}
	
	@RequestMapping(value = "/ajax/pf/lesson/deleteAllSchedule", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String deleteAllSchedule(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[]{"curr_seq"});
		HashMap<String, Object> map = mpfScheduleService.deleteAllSchedule(param);
		model.addAllAttributes(map);
		return JSON_VIEW;
	}
	
	/**
	 * waktu표 다운로드
	 * @param model
	 * @param param
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/pf/lesson/scheduleExcelDown", method = RequestMethod.GET)
	public String scheduleExcelDown(HttpServletRequest request, Model model, @RequestParam HashMap<String, Object> param, HttpServletResponse response)  throws Exception {
		
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		
		//엑셀 제목 크기
		int[] sheetWidth = {2000, 2500, 2000, 2000, 2000, 3000, 2000, 4000, 8000, 10000, 2000, 4000, 2000, 3000, 3000, 3000, 3000};

		//제목
		String[] sheetTitle = {
				"waktu", "hari dan tanggal", "시작waktu", "종료waktu", " pengajar", "nama dosen pengajar", "departement", "장소", "수업명(topik pembelajaran)", "hasil pembelajaran", "cara pengajaran",
				"학습방법", "형성penilaian", "수업코드", "Area코드", "tingkat코드", "Metode evaluasi코드"
			};

		String curDate = (new SimpleDateFormat("yyyy-MM-dd")).format(new Date());
		
		model.addAttribute("excel_view_type", "schedule");
		model.addAttribute("filename","schedule_"+curDate);
		model.addAttribute("sheetName","sheet1");
		model.addAttribute("sheetTitle", sheetTitle);
		model.addAttribute("sheetWidth",sheetWidth);
		model.addAttribute("scheduleList", mpfScheduleService.scheduleExcelDown(param));
		
		response.setHeader("Content-disposition", "attachment; filename=" + "schedule_"+curDate+".xlsx"); 
		
		return "excelDownloadView";
	}
	
	@RequestMapping(value = "/ajax/pf/lesson/getAcademicList", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String getAcademicList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[]{"start_date", "end_date"});
		model.addAttribute("list", mpfScheduleService.getAcademicList(param));
		return JSON_VIEW;
	}
	
	/**
	 * waktu표 업로드 양식 다운로드
	 * @param model
	 * @param param
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/pf/lesson/scheduleExcelTmplDown", method = RequestMethod.GET)
	public String scheduleExcelTmplDown(HttpServletRequest request, Model model, @RequestParam HashMap<String, Object> param, HttpServletResponse response)  throws Exception {
		
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		
		//엑셀 제목 크기
		int[] sheetWidth = {2000, 3000, 3000, 2000, 2000
				, 3000, 3000, 7000, 8000, 3000, 4000
				, 3000, 3000, 3000, 3000, 3000};

		//제목
		String[] sheetTitle = {
				"hari dan tanggal(*)", "시작waktu(*)", "종료waktu(*)", " pengajar(*)", "nama dosen pengajar"
				, "사번(*)", "장소(*)", "수업명(수업 주제)(*)", "학습 성과", "학습방법", "학습방법 코드"
				, "형성penilaian", "수업코드", "Area코드", "tingkat코드", "Metode evaluasi코드"				
			};

		model.addAttribute("unitCodeList", commonService.getUnitCode()); //Area tingkat 코드
		model.addAttribute("formationEvalution", commonService.getCodeList("formation_evaluation")); //형성penilaian
		model.addAttribute("evaluationCode", commonService.getCodeList("evaluation_code")); //Metode evaluasi
		model.addAttribute("lessonMethodCode", commonService.getCodeList("lesson_method_code")); //학습방법
		model.addAttribute("pfList", mpfScheduleService.getCurrPfList(param));
		
		String curDate = (new SimpleDateFormat("yyyy-MM-dd")).format(new Date());
		
		model.addAttribute("excel_view_type", "scheduleTmplDown");
		model.addAttribute("filename","scheduleTmpl_"+curDate);
		model.addAttribute("sheetName","sheet1");
		model.addAttribute("sheetTitle", sheetTitle);
		model.addAttribute("sheetWidth",sheetWidth);
		model.addAttribute("scheduleList", mpfScheduleService.scheduleExcelDown(param));
		
		response.setHeader("Content-disposition", "attachment; filename=" + "scheduleTmpl_"+curDate+".xlsx"); 
		
		return "excelDownloadView";
	}
}
