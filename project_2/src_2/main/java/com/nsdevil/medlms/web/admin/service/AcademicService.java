package com.nsdevil.medlms.web.admin.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.exception.RuntimeLogicException;
import com.nsdevil.medlms.common.util.FileUploader;
import com.nsdevil.medlms.common.util.PagingHelper;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.admin.dao.AcademicDao;
import com.nsdevil.medlms.web.mpf.dao.MPFUnitDao;
import com.nsdevil.medlms.web.pf.dao.PFLessonDao;


@Service
public class AcademicService {
	
	@Autowired
	private AcademicDao academicDao;

	@Autowired
	private PFLessonDao pfLessonDao;
	
	@Autowired
	private MPFUnitDao unitDao;
	
	@Value("#{config['config.rootPath']}")
	private String ROOT_PATH;
	
	@Value("#{config['config.resPath']}")
	private String RES_PATH;
	
	public ResultMap academicSystemCreate(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		if (academicDao.insertAcademicSystem(param) != 1) {
			throw new RuntimeLogicException("QUERY_FAIL [ academic_system insert query fail ]", "004");
		}
		return resultMap;
	}

	public List<HashMap<String, Object>> academicSystemListNoAdd(HashMap<String, Object> param) throws Exception {
		return academicDao.getAcademicSystemListNoAdd(param);
	}

	public ResultMap academicSystemAcaNameModify(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		//해당 시퀀스 레벨 확인
		int level = academicDao.getAcaLevel(param);
		param.put("level", level);
		
		//해당 시퀀스로 registrasi perkuliahan 되어 있는지 확인
		if(academicDao.academicCntChk(param) > 0) {
			resultMap.put("status", "201");
			resultMap.put("level", level);
			return resultMap;
		}
		if (academicDao.updateAcademicSystemAcaName(param) != 1) {
			throw new RuntimeLogicException("QUERY_FAIL [ academic_system aca_name modify query fail ]", "004");
		}
		return resultMap;
	}

	public ResultMap academicSystemAcaOrderModify(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		if (academicDao.updateAcademicSystemAcaOrder(param) != 1) {
			throw new RuntimeLogicException("QUERY_FAIL [ academic_system aca_system_order modify query fail ]", "004");
		}
		return resultMap;
	}

	public ResultMap academicSystemDelete(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();	
		//해당 시퀀스 레벨 확인
		int level = academicDao.getAcaLevel(param);
		param.put("level", level);
		
		//해당 시퀀스로 registrasi perkuliahan 되어 있는지 확인
		if(academicDao.academicCntChk(param) > 0) {
			resultMap.put("status", "201");
			resultMap.put("level", level);
			return resultMap;
		}
		if(academicDao.deleteAcademicSystem(param) == 0) {
			throw new RuntimeLogicException("QUERY_FAIL [ academic_system delete query fail ]", "004");
		}
		return resultMap;
	}

	public List<HashMap<String, Object>> academicSystemSategeOfList(HashMap<String, Object> param) throws Exception {
		return academicDao.getAcademicSystemSategeOfList(param);
	}

	public HashMap<String, Object> curriculumList(HashMap<String, Object> param) throws Exception {
		int totalCount = academicDao.getCurriculumCount(param);
		
		PagingHelper pagingHelper = new PagingHelper();
		List<HashMap<String, Object>> list = academicDao.getCurriculumList(pagingHelper.getPagingParam(param, totalCount));
		
		return pagingHelper.getPageList(list, "getCurriculumList", totalCount);
	}
	
	//엑셀다운용
	public List<HashMap<String, Object>> curriculumListExcelDown(HashMap<String, Object> param) throws Exception {
				
		return academicDao.getCurriculumList(param);
	}

	public int curriculumCodeCount(HashMap<String, Object> param) throws Exception {

		return academicDao.getCurriculumCodeCount(param);
	}

	@Transactional
	public ResultMap curriculumDelete(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();	
		String[] curr_seq = param.get("curr_seqs").toString().split(",");
		
		for(int i=0; i<curr_seq.length; i++){
			HashMap<String, Object> map = academicDao.getCurriculumInfo(curr_seq[i]);
			if(map.get("confirm_flag").toString().equals("Y")) {
				throw new RuntimeLogicException(map.get("curr_name").toString(), "004");
			}
			if (academicDao.deleteCurriculum(curr_seq[i]) == 0) {		
				throw new RuntimeLogicException("QUERY_FAIL [ admin - curriculum delete query fail ]", "004");
			}
		}
		return resultMap;
	}

	public HashMap<String, Object> curriculumOneList(HashMap<String, Object> param) throws Exception {
		List<HashMap<String, Object>> list = academicDao.getCurriculumList(param); 
		return list.get(0);
	}

	public List<HashMap<String, Object>> curriculumMpAssignPfList(HashMap<String, Object> param) throws Exception {
		// TODO Auto-generated method stub
		return academicDao.getCurriculumMpAssignPf(param);
	}
	
	public List<HashMap<String, Object>> academicSystemTemplateList() throws Exception {
		return academicDao.getAcamedicSystemTemplateList();
	}	

	@Transactional
	public ResultMap curriculumExcelCreate(MultipartFile uploadFile, HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		String reg_user_seq = param.get("reg_user_seq").toString();
		if (uploadFile != null) {

			@SuppressWarnings("resource")
			Workbook workbook = new XSSFWorkbook(uploadFile.getInputStream());
			//int sheetNum = workbook.getNumberOfSheets(); // 촟 Sheet 갯수
			List<HashMap<String, Object>> arryHash = new ArrayList<HashMap<String, Object>>();
	
			for (int k = 0; k < 1; k++) {
				Sheet sheet = workbook.getSheetAt(k); // Sheet의 데이터 가져옴
				
	
				int rows = sheet.getPhysicalNumberOfRows(); // 총 Row 갯수
				for (int r = 1; r < rows; r++) {
					Row row = sheet.getRow(r);
					HashMap<String, Object> curriculumMap = new HashMap<String, Object>();
					
					if (row != null) { // Row가 null이 아닐때
	
						int cells = 12; // row.getLastCellNum(); //총 cell 갯수
	
						for (int c = 0; c < cells; c++) {
							Cell cell = row.getCell(c);
							String value = "";
							if (cell != null) {
								switch (cell.getCellTypeEnum()) {
									case NUMERIC: // 숫자형								
										if (c == 1 || c == 2 || c == 3 || c == 9 || c == 10 || c == 11)
											value = "" + (int) cell.getNumericCellValue();
										else
											value = "" + (double) cell.getNumericCellValue();
										break;
									case STRING: // 문자형
										value = "" + cell.getStringCellValue();
										break;
									case BLANK: // 값이 없을때
										value = "";
										break;
									case ERROR:
										value = "" + cell.getErrorCellValue(); // byte
										break;
									default:
								}
							}
							switch (c) {
							case 0: //No 
								break;
							case 1: //kode kurikulum Akademik
								if(!value.isEmpty() || !value.equals(""))
									curriculumMap.put("auto_flag", "Y");									
								else
									curriculumMap.put("auto_flag", "N");
								curriculumMap.put("curr_code", value);
								break;
							case 2: //Nama Mata Kuliah
								if(!value.isEmpty() || !value.equals("")){
									
								}
								curriculumMap.put("curr_name", value);
								break;
							case 3: //sistem akademik분류코드
								if(!value.isEmpty() || !value.equals("")){
									
								}
								curriculumMap.put("aca_system_seq", value);
								break;
							case 4: //kategori penuntasan코드
								curriculumMap.put("complete_code", value);
								break;
							case 5: //nilai코드
								curriculumMap.put("grade_code", value);
								break;
							case 6: //nilai
								curriculumMap.put("grade", value);
								break;
							case 7: //manajemen Klasifikasi코드
								curriculumMap.put("administer_code", value);
								break;
							case 8: //Target코드
								curriculumMap.put("target_code", value);
								break;
							case 9: //신청비용
								curriculumMap.put("req_charge", value);
								break;
							case 10: //Dosen Penanggung jawab코드
								curriculumMap.put("mpf", value);
								break;
							case 11: //부Dosen Penanggung jawab코드
								curriculumMap.put("pf", value);
								break;
								
							}
						}
					}
					curriculumMap.put("reg_user_seq", reg_user_seq);	
					curriculumMap.put("curr_seq", "");
					arryHash.add(curriculumMap);
				}			
			}
	
			for (int i = 0; i < arryHash.size(); i++) {
				HashMap<String, Object> maps = arryHash.get(i);
				//유효성 체크한다.
				HashMap<String, Object> chkMap = academicDao.getCurriculumExcelUpChk(maps);
				boolean values_chk = true;
				String message = (i+2)+" 번째 줄 ";
				if(chkMap.get("aca_system_seq_chk").toString().equals("N")){
					message += "없는 학사쳬계분류코드 입니다.";
					values_chk = false;
				}
				if(chkMap.get("curr_chk").toString().equals("N")){
					message += "\n이미 registrasi된 교육과정 입니다.";
					values_chk = false;
				}
				if(chkMap.get("complete_chk").toString().equals("N")){
					message += "\n없는 kategori penuntasan코드 입니다.";
					values_chk = false;
				}
				if(chkMap.get("grade_chk").toString().equals("N")){
					message += "\n없는 nilai코드 입니다.";
					values_chk = false;
				}
				if(chkMap.get("administer_chk").toString().equals("N")){
					message += "\n없는 manajemen Klasifikasi코드 입니다.";
					values_chk = false;
				}
				if(chkMap.get("target_chk").toString().equals("N")){
					message += "\n없는 Target코드 입니다.";
					values_chk = false;
				}
										
				academicDao.insertCurriculum(maps);	
				String mpf_chk = "";
				String pf_chk = "";
				boolean mpf_bool = true;
				boolean pf_bool = true;
				
				//Dosen Penanggung jawab 인서트한다.
				if(!maps.get("mpf").toString().equals("")){
					String[] mpfList = maps.get("mpf").toString().split(",");
					for(int mpf_index=0;mpf_index<mpfList.length;mpf_index++){							 
						maps.put("user_seq", mpfList[mpf_index]);
						maps.put("professor_level", "1");
						
						//교수 시퀀스 users 테이블에 있는지 체크
						int mpf_cnt = academicDao.professorChk(maps);
						if(mpf_cnt == 0){
							mpf_chk +=" | "+mpfList[mpf_index];
							mpf_bool = false;
						}else{
							academicDao.insertMpAssignPf(maps);
						}
					}
				}

				//부 Dosen Penanggung jawab 인서트한다.
				if(!maps.get("pf").toString().equals("")){
					String[] pfList = maps.get("pf").toString().split(",");
					for(int pf_index=0;pf_index<pfList.length;pf_index++){
						maps.put("user_seq", pfList[pf_index]);
						maps.put("professor_level", "2");
						
						//교수 시퀀스 users 테이블에 있는지 체크
						int pf_cnt = academicDao.professorChk(maps);
						if(pf_cnt == 0){
							pf_chk +=" | "+pfList[pf_index];
							pf_bool = false;
						}else
							academicDao.insertMpAssignPf(maps);
					}
				}
				
				if(mpf_bool == false)
					message += "\n Dosen Penanggung jawab 코드 " + mpf_chk + " 없는 교수 코드 입니다.";
				if(pf_bool == false)
					message += "\n 부Dosen Penanggung jawab 코드 "+ pf_chk + " 없는 교수 코드 입니다.";
				
				if(mpf_bool == false || pf_bool == false) {
					throw new RuntimeLogicException(message, "002");
/*						throw new LogicException("VALIDATION_CHECK_FAIL [ type ]", "002");*/
				}
			}
		}

		/*
		 * for(int i=0; i<patientVoArray.size();i++){
		 * patientDao.addPatient(patientVoArray.get(i)); }
		 */
		return resultMap;
	}

	public HashMap<String, Object> academicList(HashMap<String, Object> param) throws Exception {

		int totalCount = academicDao.getAcademicListCount(param);
		
		PagingHelper pagingHelper = new PagingHelper();
		
		List<HashMap<String, Object>> list = academicDao.getAcademicList(pagingHelper.getPagingParam(param, totalCount));
		
		return pagingHelper.getPageList(list, "getAcademicList", totalCount);
	}


	public ResultMap academicExcelList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("list", academicDao.getAcademicList(param));
		
		return resultMap;
	}
	
	public List<HashMap<String, Object>> gradCapabilityVersionList() throws Exception {
		List<HashMap<String, Object>> list = academicDao.gradCapabilityGetVersion();
		
		return list;		
	}

	public ResultMap graduationCapabilityExcelCreate(MultipartFile uploadFile, HashMap<String, Object> param) throws Exception {
		String reg_user_seq = param.get("reg_user_seq").toString();
		ResultMap reulstMap = new ResultMap();
		if (uploadFile != null) {
		
			@SuppressWarnings("resource")
			Workbook workbook = new XSSFWorkbook(uploadFile.getInputStream());
			//int sheetNum = workbook.getNumberOfSheets(); // 촟 Sheet 갯수
			List<HashMap<String, Object>> arryHash = new ArrayList<HashMap<String, Object>>();
			String version = "";

			int versionCount = academicDao.getGCVersionCount();
			if(versionCount == 0)
				version = "1.0";
			else
				version = academicDao.getGCMaxVersion();
			
			for (int k = 0; k < 1; k++) {
				Sheet sheet = workbook.getSheetAt(k); // Sheet의 데이터 가져옴
				
	
				int rows = sheet.getPhysicalNumberOfRows(); // 총 Row 갯수
				for (int r = 1; r < rows; r++) {
					Row row = sheet.getRow(r);
					HashMap<String, Object> graduationCapabilityMap = new HashMap<String, Object>();
					
					if (row != null) { // Row가 null이 아닐때
	
						int cells = 4; // row.getLastCellNum(); //총 cell 갯수
						
						for (int c = 0; c < cells; c++) {
							Cell cell = row.getCell(c);
							String value = "";
							if (cell != null) {
								switch (cell.getCellTypeEnum()) {
								case NUMERIC: // 숫자형								
									if (c == 1)
										value = "" + (int) cell.getNumericCellValue();
									else
										value = "" + (double) cell.getNumericCellValue();
									break;
								case STRING: // 문자형
									value = "" + cell.getStringCellValue();
									break;
								case BLANK: // 값이 없을때
									value = "";
									break;
								case ERROR:
									value = "" + cell.getErrorCellValue(); // byte
									break;
								default:
								}
							}
							switch (c) {
							case 0: //No 
								break;
								
							case 1: //syarat kelulusan코드														
								graduationCapabilityMap.put("fc_code", value);
								break;
							case 2: //syarat kelulusan							
								graduationCapabilityMap.put("fc_name", value);
								break;
							case 3: //isi							
								graduationCapabilityMap.put("content", value);
								break;
								
							}
						}
					}
					
					graduationCapabilityMap.put("version", version);
					graduationCapabilityMap.put("reg_user_seq", reg_user_seq);	
					arryHash.add(graduationCapabilityMap);
				}
			}

			for (int i = 0; i < arryHash.size(); i++) {
				HashMap<String, Object> maps = arryHash.get(i);
				//유효성 체크한다.
				for(int j = 0; j < arryHash.size(); j++){
					if(i==j)
						continue;
					else if(arryHash.get(j).get("fc_code").toString().equals(maps.get("fc_code").toString()))
						throw new RuntimeLogicException("역량코드 : " + arryHash.get(j).get("fc_code").toString() + " 파일내 중복되는 값입니다.","002");
				}
			}
			
			for (int i = 0; i < arryHash.size(); i++) {
				HashMap<String, Object> maps = arryHash.get(i);
				academicDao.insertGraduationCapability(maps);						
			}
		}

		return reulstMap;
	}

	public List<HashMap<String, Object>> graduationCapabilityList(HashMap<String, Object> param) throws Exception {

		return academicDao.getGraduationCapabilityList(param);
	}

	public HashMap<String, Object> gradCapabilityLastVersion() throws Exception {

		return academicDao.getGraduationCapabilityLastVersion();
	}

	public ResultMap getAcademicYearList() throws Exception{
		ResultMap resultMap = new ResultMap();	
		resultMap.put("academicYearList", academicDao.getAcademicYearList());
		return resultMap;
	}

	public ResultMap academicStudentCount(HashMap<String, Object> param) throws Exception{
		ResultMap resultMap = new ResultMap();	
		resultMap.put("studentCount", academicDao.getAcademicStudentCount(param));
		return resultMap;
	}

	public ResultMap academicInsert(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();	
		if(param.get("aca_seq").toString().isEmpty()) {
			int overlapchk = academicDao.checkAcademicOverlap(param);
			if(overlapchk > 0)
				resultMap.put("status", "201");
			else {
				if(academicDao.insertAcademic(param) != 1)
					throw new RuntimeLogicException("QUERY_FAIL [ academic insert query fail ]", "004");
			}
		}else {
			if(academicDao.updateAcademic(param) != 1)
				throw new RuntimeLogicException("QUERY_FAIL [ academic update query fail ]", "004");
		}
		resultMap.put("aca_seq", param.get("aca_seq").toString());
		
		return resultMap;
	}

	public ResultMap academicGetBaseInfoList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("acaInfo", academicDao.getAcademicOne(param));
		return resultMap;
	}

	public ResultMap curriculumAssignList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("currList", academicDao.getAssignCurrList(param));
		resultMap.put("confirmFlag", academicDao.getAcademicConfirmFlag(param));
		resultMap.put("currCount", academicDao.getAssignCurrCount(param));
		return resultMap;
	}

	public ResultMap academicConfirm(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		//확정 batal일경우 status akademik 체크.
		if(param.get("flag").toString().equals("N")) {
			String aca_state = academicDao.getAcademicState(param);
			if(!aca_state.equals("01") && !aca_state.equals("02")) {
				resultMap.put("status", "201");
				return resultMap;
			}				
		}
		
		List<HashMap<String, Object>> curr_not_confirm_list = academicDao.getCurrNotConfirmList(param);
		if(curr_not_confirm_list.size() > 0) {
			resultMap.put("status", "202");			
			String msg = "";
			for(HashMap<String, Object> map : curr_not_confirm_list) {
				msg += map.get("curr_name").toString()+"\n";
			}
			resultMap.put("msg", msg);
			return resultMap;
		}
			
		if(academicDao.academicConfirm(param) != 1)
			throw new RuntimeLogicException("QUERY_FAIL [ academic confirm update query fail ]", "004");
		return resultMap;
	}

	@Transactional
	public ResultMap curriculumAssignInsert(HashMap<String, Object> param, String[] curr_seq) throws Exception{
		ResultMap resultMap = new ResultMap();
		for(int i=0;i<curr_seq.length;i++) {
			param.put("curr_seq", curr_seq[i]);
			if(academicDao.curriculumAssignUpdate(param) != 1)
				throw new RuntimeLogicException("QUERY_FAIL [ curriculum assign update query fail ]", "004");
		}
		
		return resultMap;
	}

	public ResultMap academicStateUpdate(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		//개설 이후 상태로 변경할려고 할때
		if(!param.get("aca_state").toString().equals("01") && !param.get("aca_state").toString().equals("02")) {
			//informasi dasar, Penetapan Kurikulum, waktu표 확정되어있는지 체크
			if(academicDao.academicConfirmChk(param) != 1) {
				resultMap.put("status", "201");
				return resultMap;
			}			
		}
		
		if(academicDao.updateAcademic(param) != 1)
			throw new RuntimeLogicException("QUERY_FAIL [ academic aca_state update query fail ]", "004");
		return resultMap;
	}

	public ResultMap getSchdCurrList(HashMap<String, Object> param) throws Exception{
		ResultMap resultMap = new ResultMap();
		resultMap.put("currList", academicDao.getCurrSchdList(param));
		resultMap.put("confirmFlag", academicDao.getAcademicConfirmFlag(param));
		return resultMap;
	}

	public ResultMap getSFSurveyCSList(HashMap<String, Object> param) throws Exception{
		ResultMap resultMap = new ResultMap();
		resultMap.put("surveyList", academicDao.getSFSurveyCS(param));
		resultMap.put("acaInfo", academicDao.getAcademicOne(param));
		resultMap.put("stCnt", academicDao.getCurrSRSurveyInfo(param));
		resultMap.put("currInfo", pfLessonDao.getCurriculumBasicInfo(param));
		//Dosen Penanggung jawab
		param.put("professor_level", "1");
		resultMap.put("mpfList", academicDao.getCurriculumMpAssignPf(param));
		
		//부Dosen Penanggung jawab
		param.put("professor_level", "2");
		resultMap.put("dpfList", academicDao.getCurriculumMpAssignPf(param));
		return resultMap;
	}

	public ResultMap getCurrNameList(HashMap<String, Object> param) throws Exception{
		ResultMap resultMap = new ResultMap();
		resultMap.put("currList", academicDao.getCurrNameList(param));
		return resultMap;
	}

	public ResultMap getLessonList(HashMap<String, Object> param) throws Exception{
		ResultMap resultMap = new ResultMap();
		resultMap.put("lessonList", academicDao.getCurrLPList(param));
		return resultMap;
	}

	public ResultMap getPFList(HashMap<String, Object> param) throws Exception{
		ResultMap resultMap = new ResultMap();
		resultMap.put("pfList", academicDao.getLessonPFList(param));
		return resultMap;
	}

	public ResultMap getUnsubmissionSTList(HashMap<String, Object> param) throws Exception{
		ResultMap resultMap = new ResultMap();
		List<HashMap<String, Object>> hashMap = academicDao.unsubmissionSTList(param);
		
		resultMap.put("stList", hashMap);
		resultMap.put("stCnt", hashMap.size());
		resultMap.put("lpSubject", academicDao.getLessonPlanSubject(param));
		return resultMap;
	}

	public ResultMap getUnsubmissionCurrSTList(HashMap<String, Object> param) throws Exception{
		ResultMap resultMap = new ResultMap();
		List<HashMap<String, Object>> hashMap = academicDao.unsubmissionCurrSTList(param);
		
		resultMap.put("stList", hashMap);
		resultMap.put("stCnt", hashMap.size());
		resultMap.put("currInfo", pfLessonDao.getCurriculumBasicInfo(param));
		return resultMap;
	}
	
	public ResultMap getSRSurveyResult(HashMap<String, Object> param) throws Exception{
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("resultList", academicDao.getSRSurveyResult(param));
		resultMap.put("titleInfo", academicDao.getSRSurveyInfo(param));
		return resultMap;
	}

	public ResultMap getSRSurveyCurrResult(HashMap<String, Object> param) throws Exception{
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("resultList", academicDao.getSRSurveyCurrResult(param));
		resultMap.put("stCnt", academicDao.getCurrSRSurveyInfo(param));
		resultMap.put("currInfo", pfLessonDao.getCurriculumBasicInfo(param));
		//Dosen Penanggung jawab
		param.put("professor_level", "1");
		resultMap.put("mpfList", academicDao.getCurriculumMpAssignPf(param));
		
		//부Dosen Penanggung jawab
		param.put("professor_level", "2");
		resultMap.put("dpfList", academicDao.getCurriculumMpAssignPf(param));
		return resultMap;
	}
	
	public ResultMap curriculumInsert(HashMap<String, Object> param, String[] mpf_user_seq, String[] pf_user_seq) throws Exception {
		ResultMap resultMap = new ResultMap();

		if(param.get("curr_seq").toString().equals("") || param.get("curr_seq").toString().isEmpty()) {
			if (academicDao.insertCurriculum(param) == 0) {
				throw new RuntimeLogicException("QUERY_FAIL [ admin - curriculum insert query fail ]", "004");
			}
		}
		else {
			if (academicDao.updateCurriculum(param) == 0) {
				throw new RuntimeLogicException("QUERY_FAIL [ admin - curriculum update query fail ]", "004");
			}
		}
		
		//교육과정과 교수 매핑된거 삭제함. Dosen Penanggung jawab, 부Dosen Penanggung jawab만(교수 레벨 1,2)
		param.put("deleteType", "mpf");
		academicDao.deleteMpAssignPf(param);
		
		param.put("cnt", 0);
		if(mpf_user_seq != null ){
			param.put("professor_level", 1);
			for(int i=0;i<mpf_user_seq.length;i++){
				param.put("user_seq", mpf_user_seq[i]);
				if (academicDao.insertMpAssignPf(param) == 0) {
					throw new RuntimeLogicException("QUERY_FAIL [ admin - mp_assign_professor insert query fail ]", "004");
				}				
			}
		}
		
		param.put("deleteType", "pf");
		academicDao.deleteMpAssignPf(param);
		
		if(pf_user_seq != null){
			param.put("professor_level", 2);
			for(int i=0;i<pf_user_seq.length;i++){
				param.put("user_seq", pf_user_seq[i]);
				if (academicDao.insertMpAssignPf(param) == 0) {
					throw new RuntimeLogicException("QUERY_FAIL [ admin - mp_assign_professor insert query fail ]", "004");
				}
			}
		}
		resultMap.put("curr_seq", param.get("curr_seq").toString());
		return resultMap;
	}

	public ResultMap academicDelete(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		String[] aca_seq = param.get("aca_seq").toString().split(",");
		
		List<HashMap<String, Object>> listMap = new ArrayList<HashMap<String, Object>>(); 
		for(int i=0; i<aca_seq.length; i++) {
			param.put("aca_seq", aca_seq[i]);			
			HashMap<String, Object> map = academicDao.getAcaStateInfo(param);
			
			//교육과정 배정된 것과, status akademik가 개설(비공개) 아닌것 골라낸다.
			if(!map.get("curr_cnt").toString().equals("0") || !map.get("aca_state").toString().equals("01"))
				listMap.add(map);
			else
				academicDao.deleteAcademic(param);
		}
		
		if(listMap.size() != 0) {
			resultMap.put("status", "001");
			resultMap.put("list", listMap);
		}
		
		return resultMap;
	}

	public ResultMap getAcaConfirmInfo(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("aca_state", academicDao.getAcaConfirmInfo(param));
		
		return resultMap;
	}

	public ResultMap getAcaCurrList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("currList", academicDao.getAcaCurrList(param));
		
		return resultMap;
	}
	

	public ResultMap getGradeScoreList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("stList", academicDao.getGradeStList(param));
		resultMap.put("scoreList", academicDao.getGradeScoreList(param));
		resultMap.put("currList", academicDao.getAcaCurrList(param));
		resultMap.put("currCntInfo", academicDao.getAcaCurrCount(param));
		resultMap.put("stGradeInfo", academicDao.getAcaStGradeInfo(param));
		
		return resultMap;
	}

	public ResultMap getSooSiGradeScoreList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("stList", academicDao.getGradeStList(param));
		resultMap.put("currList", academicDao.getAcaSooSiCurrList(param));
		resultMap.put("currCntInfo", academicDao.getAcaSooSiCurrCount(param));
		resultMap.put("scoreList", academicDao.getAcaSooSiScoreList(param));
		resultMap.put("stGradeInfo", academicDao.getAcaSooSiScoreRank(param));
		return resultMap;
	}	

	public List<HashMap<String, Object>> getGradeStList(HashMap<String, Object> param) throws Exception {
		return academicDao.getGradeStList(param);
	}

	@Transactional
	public ResultMap sooSiScoreExcelCreate(MultipartFile uploadFile, HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		String reg_user_seq = param.get("reg_user_seq").toString();
		if (uploadFile != null) {

			@SuppressWarnings("resource")
			Workbook workbook = new XSSFWorkbook(uploadFile.getInputStream());
			//int sheetNum = workbook.getNumberOfSheets(); // 촟 Sheet 갯수
			List<HashMap<String, Object>> arryHash = new ArrayList<HashMap<String, Object>>();
	
			String src_name = "";
			String src_date = "";
			String src_cnt = "";
			
			for (int k = 0; k < 1; k++) {
				Sheet sheet = workbook.getSheetAt(k); // Sheet의 데이터 가져옴
				
	
				int rows = sheet.getPhysicalNumberOfRows(); // 총 Row 갯수
				for (int r = 0; r < rows; r++) {
					Row row = sheet.getRow(r);
					HashMap<String, Object> gradeMap = new HashMap<String, Object>();
					
					if(r==3)
						continue;
					
					if (row != null) { // Row가 null이 아닐때
	
						int cells = 5; // row.getLastCellNum(); //총 cell 갯수
	
						for (int c = 0; c < cells; c++) {
							Cell cell = row.getCell(c);
							String value = "";
							if (cell != null) {
								switch (cell.getCellTypeEnum()) {
									case NUMERIC: // 숫자형				
										if( DateUtil.isCellDateFormatted(cell)) {
											Date date = cell.getDateCellValue();
											value = new SimpleDateFormat("yyyy-MM-dd").format(date);
										}else {
											if (c == 1 || c == 2)
												value = "" + (int) cell.getNumericCellValue();
											else
												value = "" + (double) cell.getNumericCellValue();
										}
										break;
									case STRING: // 문자형
										value = "" + cell.getStringCellValue();
										break;
									case BLANK: // 값이 없을때
										value = "";
										break;
									case ERROR:
										value = "" + cell.getErrorCellValue(); // byte
										break;
									default:
								}
							}
							switch (c) {
							case 0: //No 
								break;
							case 1: //학번
								if(value.isEmpty() || value.equals(""))
									break;
								
								if(r==0)
									src_name = value;
								else if(r==1)
									src_date = value;
								else if(r==2)
									src_cnt = value;
								else
									gradeMap.put("id", value);
								break;
							case 2: //학생이름								
								break;
							case 3: //정답수
								gradeMap.put("answer_cnt", value);
								break;
							case 4: //점수
								gradeMap.put("score", value);
								break;															
							}
						}
					}
					
					if(r<=3)
						continue;
					gradeMap.put("reg_user_seq", reg_user_seq);	
					gradeMap.put("curr_seq", param.get("curr_seq").toString());
					arryHash.add(gradeMap);
				}			
			}			
			if(src_name.isEmpty())
				throw new RuntimeLogicException("시험명을 입력해주세요.", "002");
			
			if(src_date.isEmpty())
				throw new RuntimeLogicException("시험일자를 입력해주세요.", "002");
			
			if(Util.DateCheck(src_date) == false)
				throw new RuntimeLogicException("시험일자를 정확히 입력해주세요.", "002");
			
			if(src_cnt.isEmpty())
				throw new RuntimeLogicException("총문항 수를 입력해주세요.", "002");
	
			HashMap<String, Object> srcMap = new HashMap<String, Object>();
			
			srcMap.put("src_seq", param.get("src_seq").toString());
			srcMap.put("src_name", src_name);
			srcMap.put("src_date", src_date);
			srcMap.put("question_cnt", src_cnt);
			srcMap.put("reg_user_seq", reg_user_seq);
			srcMap.put("curr_seq", param.get("curr_seq").toString());
			
			if(param.get("src_seq").toString().isEmpty())			
				academicDao.insertSooSiScore(srcMap);
			else
				academicDao.updateSooSi(srcMap);
			
			for (int i = 0; i < arryHash.size(); i++) {
				HashMap<String, Object> maps = arryHash.get(i);

				//유효성 체크한다.
				HashMap<String, Object> chkMap = academicDao.getStCheck(maps);
				boolean values_chk = true;
				
				String message = (i+5)+" 번째 줄 ";
				if(chkMap.get("st_cnt").toString().equals("0")){
					message += "없는 학번 입니다.";
					values_chk = false;
				}else if(chkMap.get("curr_st_cnt").toString().equals("0")){
					message += "해당 교육과정에 registrasi 되지 않은 학생 입니다.";
					values_chk = false;
				}else{					
					maps.put("user_seq", chkMap.get("user_seq").toString());					
				}
				
				if(Util.isNumeric(maps.get("score").toString()) == false){
					message += "\n 점수는 숫자로 입력해주세요.(공백->0으로 입력)";
					values_chk = false;
				}
				
				if(Util.isNumeric(maps.get("answer_cnt").toString()) == false){
					message += "\n 정답 수는 숫자로 입력해주세요.(공백->0으로 입력)";
					values_chk = false;
				}
				
				if(values_chk==false) {
					throw new RuntimeLogicException(message, "002");
				}
				maps.put("src_seq", srcMap.get("src_seq").toString());
										
				academicDao.insertSooSiScoreItem(maps);
			}
		}
		return resultMap;
	}

	public ResultMap getSooSiScore(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("stList", academicDao.getGradeStList(param));
		resultMap.put("soosiList", academicDao.getCurrSooSiList(param));
		resultMap.put("scoreList", academicDao.getCurrSooSiScoreList(param));
		return resultMap;
	}

	@Transactional
	public ResultMap insertSooSiScore(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		JSONParser jsonParser = new JSONParser();
		
		JSONObject jsonObject = (JSONObject) jsonParser.parse(param.get("scorelist").toString());
		
		JSONArray scoreList = (JSONArray) jsonObject.get("list");
		
		for(int i=0; i<scoreList.size(); i++) {	
			JSONObject scoreInfo = (JSONObject) scoreList.get(i);
			
			param.put("user_seq", scoreInfo.get("user_seq"));
			String[] src_seq = scoreInfo.get("src_seq").toString().split("\\|");
			String[] answer_cnt = scoreInfo.get("answer_cnt").toString().split("\\|");
			String[] score = scoreInfo.get("score").toString().split("\\|");
			for(int j=0; j<src_seq.length; j++) {
				param.put("src_seq", src_seq[j]);
				param.put("answer_cnt", answer_cnt[j]);
				param.put("score", score[j]);
				
				if(academicDao.updateSooSiScore(param) != 1)
					throw new RuntimeLogicException("QUERY_FAIL [ soosi_report_card_score update query fail ]", "004");
			}		
		}
		return resultMap;
	}

	public ResultMap getFeScore(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("stList", academicDao.getGradeStList(param));
		resultMap.put("feCurrList", academicDao.getAcaFeCurrList(param));
		//resultMap.put("scoreList", academicDao.getCurrSooSiScoreList(param));
		return resultMap;
	}

	@Transactional
	public ResultMap insertCode(HttpServletRequest request, HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		String[] code_name = request.getParameterValues("code_name");
		String[] code = request.getParameterValues("code");
		String[] new_code_name = request.getParameterValues("new_code_name");
		
		academicDao.updateCode(param);
		
		if(code_name != null) {
			for(int i=0; i<code_name.length; i++) {
				param.put("code", code[i]);
				param.put("code_name", code_name[i]);
				
				academicDao.insertCode(param);
			}
		}
		
		if(new_code_name != null) {
			for(int i=0;i<new_code_name.length;i++) {
				param.put("code", "");
				param.put("code_name", new_code_name[i]);
				
				academicDao.insertCode(param);
			}
		}
		
		return resultMap;
	}

	public ResultMap getSeCodeList() throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("seCodeList", academicDao.getSeCode());
		return resultMap;
	}

	@Transactional
	public ResultMap seCodeInsert(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		JSONParser jsonParser = new JSONParser();
		
		JSONObject jsonObject = (JSONObject) jsonParser.parse(param.get("list").toString());
		
		JSONArray l_se_list = (JSONArray) jsonObject.get("l_se_list");
		
		academicDao.updateSeCode();
		
		param.clear();
		
		for(int i=0; i<l_se_list.size();i++) {
			JSONObject list = (JSONObject) l_se_list.get(i);
			
			param.put("se_code", list.get("l_se_code").toString());
			param.put("se_name", list.get("l_se_name").toString());
			academicDao.insertLSeCode(param);
		}
		
		JSONArray se_list = (JSONArray) jsonObject.get("se_list");
		
		param.clear();
		for(int i=0;i<se_list.size();i++) {
			JSONObject list = (JSONObject) se_list.get(i);
			
			param.put("l_se_code", list.get("l_se_code").toString());
			param.put("se_code", list.get("se_code").toString());
			param.put("se_name", list.get("se_name").toString());
			academicDao.insertSeCode(param);
		}
		
		
		return resultMap;
	}

	public ResultMap getUnitCodeList() throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("unitCodeList", academicDao.getUnitCode());
		return resultMap;
	}

	public ResultMap unitCodeInsert(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		JSONParser jsonParser = new JSONParser();
		
		JSONObject jsonObject = (JSONObject) jsonParser.parse(param.get("list").toString());
		
		JSONArray l_uc_list = (JSONArray) jsonObject.get("l_uc_list");
		
		academicDao.updateUnitCode();
		
		param.clear();
		
		for(int i=0; i<l_uc_list.size();i++) {
			JSONObject list = (JSONObject) l_uc_list.get(i);
			
			param.put("uc_code", list.get("l_uc_code").toString());
			param.put("uc_name", list.get("l_uc_name").toString());
			academicDao.insertLUnitCode(param);
		}
		
		JSONArray uc_list = (JSONArray) jsonObject.get("uc_list");
		
		param.clear();
		for(int i=0;i<uc_list.size();i++) {
			JSONObject list = (JSONObject) uc_list.get(i);
			
			param.put("l_uc_code", list.get("l_uc_code").toString());
			param.put("uc_code", list.get("uc_code").toString());
			param.put("uc_name", list.get("uc_name").toString());
			academicDao.insertUnitCode(param);
		}
		
		
		return resultMap;
	}

	public ResultMap deleteCode(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		int cnt = academicDao.getCompleteCodeUseCnt(param);
		
		if(cnt > 0) {
			resultMap.put("status", "201");
			return resultMap;
		}else {
			academicDao.deleteCode(param);
		}
		
		return resultMap;
	}
	
	public ResultMap deleteSeCode(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		int cnt = academicDao.getSeCodeUseCount(param);
		
		if(cnt > 0) {
			if("L".equals(param.get("type").toString())){
				resultMap.put("status", "201"); //상위코드일 때 해당코드 및 하위코드로 사용하는것 있는지
				return resultMap; 
			}else {
				resultMap.put("status", "202");
				return resultMap; 
			}
		}else {
			academicDao.deleteSeCode(param);
		}
		
		
		
		return resultMap;
	}

	public ResultMap deleteUnitCode(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		int cnt = academicDao.getUnitCodeUseCount(param);
		
		if(cnt > 0) {
			if("L".equals(param.get("type").toString())){
				resultMap.put("status", "201"); //상위코드일 때 해당코드 및 하위코드로 사용하는것 있는지
				return resultMap; 
			}else {
				resultMap.put("status", "202");
				return resultMap; 
			}
		}else {
			academicDao.deleteUnitCode(param);
		}
		
		return resultMap;
	}
	

	public ResultMap getGradeCodeList() throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("gradeCodeList", academicDao.getGradeStandardList());
		return resultMap;
	}

	public ResultMap gradeCodeInsert(HashMap<String, Object> param, HttpServletRequest request) throws Exception {
		ResultMap resultMap = new ResultMap();
		String[] grade_code = request.getParameterValues("grade_code");
		String[] grade_percent = request.getParameterValues("grade_percent");
		String[] gs_seq = request.getParameterValues("gs_seq");
		
		for(int i=0; i<gs_seq.length;i++) {
			param.put("gs_seq", gs_seq[i]);			
			param.put("grade_code", grade_code[i]);
			param.put("grade_percent", grade_percent[i]);
			academicDao.insertGradeStandard(param);
		}	
		
		return resultMap;
	}

	public ResultMap deleteGradeCode(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		academicDao.deleteGradeStandard(param);
		return resultMap;
	}

	public HashMap<String, Object> getCode1List(HashMap<String, Object> param) throws Exception {
		
		int totalCount = academicDao.getCode1Count(param);
		
		PagingHelper pagingHelper = new PagingHelper();

		List<HashMap<String, Object>> list = academicDao.getCode1List(pagingHelper.getPagingParam(param, totalCount));
		
		return pagingHelper.getPageList(list, "getCode", totalCount);
	}

	public ResultMap deleteCode1(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		int count = academicDao.getCode1UseCount(param);
		
		if(count == 0) {
			academicDao.deleteCode1(param);
		}else {
			resultMap.put("status", "201"); //사용중인 코드			
		}
		
		return resultMap;
	}

	public ResultMap updateCode1(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		int count = academicDao.getCode1UseCount(param);
		
		if(count == 0) {
			academicDao.updateCode1(param);
		}else {
			resultMap.put("status", "201"); //사용중인 코드			
		}
		
		return resultMap;
	}

	public ResultMap getCode1SearchList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("list", academicDao.getCode1List(param));
		
		return resultMap;
	}
	
	@Transactional
	public ResultMap insertCode1(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		JSONParser jsonParser = new JSONParser();
		
		JSONObject jsonObject = (JSONObject)  jsonParser.parse(param.get("list").toString());
		
		JSONArray list = (JSONArray) jsonObject.get("list");
		
		for(int i=0; i<list.size(); i++) {
			JSONObject listInfo = (JSONObject) list.get(i);
			param.put("code_name", listInfo.get("code_name"));
			param.put("row_num", listInfo.get("row_num"));
			
			academicDao.insertCode1(param);
		}
		
		return resultMap;
	}
	
	public HashMap<String, Object> getCode2List(HashMap<String, Object> param) throws Exception {
		
		int totalCount = academicDao.getCode2Count(param);
		
		PagingHelper pagingHelper = new PagingHelper();

		List<HashMap<String, Object>> list = academicDao.getCode2List(pagingHelper.getPagingParam(param, totalCount));
		
		return pagingHelper.getPageList(list, "getCode", totalCount);
	}

	public ResultMap deleteCode2(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		int count = academicDao.getCode2UseCount(param);
		
		if(count == 0) {
			academicDao.deleteCode2(param);
		}else {
			resultMap.put("status", "201"); //사용중인 코드			
		}
		
		return resultMap;
	}

	public ResultMap updateCode2(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		int count = academicDao.getCode2UseCount(param);
		
		if(count == 0) {
			academicDao.updateCode2(param);
		}else {
			resultMap.put("status", "201"); //사용중인 코드			
		}
		
		return resultMap;
	}

	public ResultMap getCode2SearchList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("list", academicDao.getCode2List(param));
		
		return resultMap;
	}

	@Transactional
	public ResultMap insertCode2(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		JSONParser jsonParser = new JSONParser();
		
		JSONObject jsonObject = (JSONObject)  jsonParser.parse(param.get("list").toString());
		
		JSONArray list = (JSONArray) jsonObject.get("list");
		
		for(int i=0; i<list.size(); i++) {
			JSONObject listInfo = (JSONObject) list.get(i);
			param.put("code_name", listInfo.get("code_name"));
			param.put("row_num", listInfo.get("row_num"));
			
			academicDao.insertCode2(param);
		}
		
		return resultMap;
	}
	
	public HashMap<String, Object> getCode3List(HashMap<String, Object> param) throws Exception {
		
		int totalCount = academicDao.getCode3Count(param);
		
		PagingHelper pagingHelper = new PagingHelper();

		List<HashMap<String, Object>> list = academicDao.getCode3List(pagingHelper.getPagingParam(param, totalCount));
		
		return pagingHelper.getPageList(list, "getCode", totalCount);
	}

	public ResultMap deleteCode3(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		int count = academicDao.getCode3UseCount(param);
		
		if(count == 0) {
			academicDao.deleteCode3(param);
		}else {
			resultMap.put("status", "201"); //사용중인 코드			
		}
		
		return resultMap;
	}

	public ResultMap updateCode3(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		int count = academicDao.getCode3UseCount(param);
		
		if(count == 0) {
			academicDao.updateCode3(param);
		}else {
			resultMap.put("status", "201"); //사용중인 코드			
		}
		
		return resultMap;
	}

	public ResultMap getCode3SearchList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("list", academicDao.getCode3List(param));
		
		return resultMap;
	}

	@Transactional
	public ResultMap insertCode3(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		JSONParser jsonParser = new JSONParser();
		
		JSONObject jsonObject = (JSONObject)  jsonParser.parse(param.get("list").toString());
		
		JSONArray list = (JSONArray) jsonObject.get("list");
		
		for(int i=0; i<list.size(); i++) {
			JSONObject listInfo = (JSONObject) list.get(i);
			param.put("code_name_kr", listInfo.get("code_name_kr"));
			param.put("code_name_en", listInfo.get("code_name_en"));
			param.put("row_num", listInfo.get("row_num"));
			
			academicDao.insertCode3(param);
		}
		
		return resultMap;
	}

	public HashMap<String, Object> getCodeMenuName(HashMap<String, Object> param) throws Exception {
		HashMap<String, Object> list = academicDao.getCodeMenuName(param);
		
		
		return list;
	}

	public ResultMap updateCodeMenuName(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		academicDao.updateCodeMenuName(param);
		return resultMap;
	}

	public ResultMap getApplicationAcaList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("acaList", academicDao.getApplicationAcaList(param));
		
		return resultMap;
	}

	public ResultMap getApplicationCurrList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("currList", academicDao.getApplicationCurrList(param));
		
		return resultMap;
	}

	public ResultMap getApplicationCurrInfo(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		List<HashMap<String, Object>> list = academicDao.getCurriculumList(param);
		resultMap.put("currList", list.get(0));

		param.put("professor_level", 1);		
		resultMap.put("mpfList", academicDao.getCurriculumMpAssignPf(param));
		
		param.put("professor_level", 2);		
		resultMap.put("pfList", academicDao.getCurriculumMpAssignPf(param));
				
		return resultMap;
	}

	@Transactional
	public ResultMap applicationInsert(HashMap<String, Object> param, MultipartHttpServletRequest request) throws Exception {
		ResultMap resultMap = new ResultMap();

		String[] caa_seq = request.getParameterValues("caa_seq");
		
		List<MultipartFile> file = request.getFiles("file");
		
		if(param.get("ca_seq").toString().equals("")) {
			if(academicDao.insertApplication(param) == 0) {
				throw new RuntimeLogicException("QUERY_FAIL [ course_application insert query fail ]", "004");
			}
		}else {
			if(academicDao.updateApplication(param) == 0) {
				throw new RuntimeLogicException("QUERY_FAIL [ course_application update query fail ]", "004");				
			}			
		}
		
		//첨부파일 전부 N 으로 변경
		param.put("use_flag", "N");
		academicDao.updateApplicationAttachYN(param);
		
		if(caa_seq != null) {
			for(int i=0;i<caa_seq.length;i++) {
				param.put("use_flag", "Y");
				param.put("caa_seq", caa_seq[i]);
				academicDao.updateApplicationAttachYN(param);
			}
		}
		
		String defaultPath = ROOT_PATH + RES_PATH + "/upload/course_application/"+param.get("ca_seq").toString()+"/";
		String dbInsertPath = "/upload/course_application/"+param.get("ca_seq").toString()+"/";
		
		//파일 simpan한다.
		if(file != null) {
			for(int i=0; i<file.size();i++){
				if(file.get(i) != null){
					String filename = FileUploader.uploadFile(defaultPath, file.get(i));
					param.put("file_name", file.get(i).getOriginalFilename());
					param.put("file_path", dbInsertPath+filename);
					if(academicDao.insertApplicationAttach(param) == 0)
						throw new RuntimeLogicException("QUERY_FAIL [ admin - course_application_attach file insert query fail ]", "004");
				}
			}
		}
				
		if(academicDao.applicationCurrInfo(param) == 0) {
			throw new RuntimeLogicException("QUERY_FAIL [ admin - course_application curriculum update query fail ]", "004");
		}
		
		resultMap.put("ca_seq", param.get("ca_seq"));		
		
		return resultMap;
	}

	public HashMap<String, Object> getApplicationList(HashMap<String, Object> param) throws Exception {
		int totalCount = academicDao.getApplicationCount(param);
		
		PagingHelper pagingHelper = new PagingHelper();
		List<HashMap<String, Object>> list = academicDao.getApplicationList(pagingHelper.getPagingParam(param, totalCount));
		
		return pagingHelper.getPageList(list, "getApplicationList", totalCount);
	}

	public ResultMap getApplicationInfo(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		List<HashMap<String, Object>> caList = academicDao.getApplicationList(param);
		if(!caList.isEmpty()) {
			resultMap.put("caList", caList.get(0));
			param.put("curr_seq", caList.get(0).get("curr_seq"));
			List<HashMap<String, Object>> list = academicDao.getCurriculumList(param);
			resultMap.put("currList", list.get(0));

			param.put("professor_level", 1);		
			resultMap.put("mpfList", academicDao.getCurriculumMpAssignPf(param));
			
			param.put("professor_level", 2);		
			resultMap.put("pfList", academicDao.getCurriculumMpAssignPf(param));
			
			param.put("level", 2);
			param.put("l_seq",caList.get(0).get("l_seq"));
			resultMap.put("mseqList", academicDao.getAcademicSystemSategeOfList(param));

			param.put("level", 3);
			param.put("l_seq",caList.get(0).get("l_seq"));
			param.put("m_seq",caList.get(0).get("m_seq"));
			resultMap.put("sseqList", academicDao.getAcademicSystemSategeOfList(param));

			param.put("aca_system_seq", caList.get(0).get("aca_system_seq"));
			resultMap.put("acaSelectList", academicDao.getApplicationAcaList(param));
			
			param.put("aca_seq", caList.get(0).get("aca_seq"));
			resultMap.put("currSelectList", academicDao.getApplicationCurrList(param));
			
			param.put("ca_seq", caList.get(0).get("ca_seq"));
			resultMap.put("attachList", academicDao.getApplicationAttach(param));
		}
		
		return resultMap;
	}

	public ResultMap getApplicationViewInfo(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		List<HashMap<String, Object>> caList = academicDao.getApplicationList(param);
		if(!caList.isEmpty()) {
			resultMap.put("caList", caList.get(0));
			param.put("curr_seq", caList.get(0).get("curr_seq"));
			List<HashMap<String, Object>> list = academicDao.getCurriculumList(param);
			resultMap.put("currList", list.get(0));

			param.put("professor_level", 1);		
			resultMap.put("mpfList", academicDao.getCurriculumMpAssignPf(param));
			
			param.put("professor_level", 2);		
			resultMap.put("pfList", academicDao.getCurriculumMpAssignPf(param));
			
			param.put("level", 2);
			param.put("l_seq",caList.get(0).get("l_seq"));
			resultMap.put("mseqList", academicDao.getAcademicSystemSategeOfList(param));

			param.put("level", 3);
			param.put("l_seq",caList.get(0).get("l_seq"));
			param.put("m_seq",caList.get(0).get("m_seq"));
			resultMap.put("sseqList", academicDao.getAcademicSystemSategeOfList(param));

			param.put("aca_system_seq", caList.get(0).get("aca_system_seq"));
			resultMap.put("acaSelectList", academicDao.getApplicationAcaList(param));
			
			param.put("aca_seq", caList.get(0).get("aca_seq"));
			resultMap.put("currSelectList", academicDao.getApplicationCurrList(param));
			
			resultMap.put("attachList", academicDao.getApplicationAttach(param));
			
			resultMap.put("stCntInfo", academicDao.getApplicationStCntInfo(param));
		}
		return resultMap;
	}		
	
	public HashMap<String, Object> getApplicationStList (HashMap<String, Object> param) throws Exception {
		
		int totalCount = academicDao.getApplicationStCount(param);
		
		PagingHelper pagingHelper = new PagingHelper();
		List<HashMap<String, Object>> list = academicDao.getApplicationStList(pagingHelper.getPagingParam(param, totalCount));
		
		return pagingHelper.getPageList(list, "getApplicationSt", totalCount);
	}

	public List<HashMap<String, Object>> getCode1ExcelList() throws Exception{
		// TODO Auto-generated method stub
		return academicDao.getCode1ExcelList();
	}

	public List<HashMap<String, Object>> getCode2ExcelList() throws Exception{
		// TODO Auto-generated method stub
		return academicDao.getCode2ExcelList();
	}
	
	public List<HashMap<String, Object>> getCode3ExcelList() throws Exception{
		// TODO Auto-generated method stub
		return academicDao.getCode3ExcelList();
	}

	public int getRefundCount() throws Exception{
		// TODO Auto-generated method stub
		return academicDao.getRefundCnt();
	}

	public ResultMap getRefundInfo() throws Exception{
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("list", academicDao.getRefundInfo());
		return resultMap;
	}

	public ResultMap insertRefund(MultipartHttpServletRequest request, HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		MultipartFile file = request.getFile("file");
		
		String defaultPath = ROOT_PATH + RES_PATH + "/upload/course_application/refund/";
		String dbInsertPath = "/upload/course_application/refund/";
		
		if(file != null) {
			//파일 simpan한다.
			String filename = FileUploader.uploadFile(defaultPath, file);
			param.put("file_name", file.getOriginalFilename());
			param.put("file_path", dbInsertPath+filename);
		}
		
		if(academicDao.insertRefund(param) == 0) {
			throw new RuntimeLogicException("QUERY_FAIL [ admin - refund insert/update query fail ]", "004");
		}
		
		return resultMap;
	}
	
	public int getNotesCount() throws Exception{
		// TODO Auto-generated method stub
		return academicDao.getNotesCnt();
	}

	public ResultMap getNotesInfo() throws Exception{
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("list", academicDao.getNotesInfo());
		return resultMap;
	}

	public ResultMap insertNotes(MultipartHttpServletRequest request, HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		MultipartFile file = request.getFile("file");
		
		String defaultPath = ROOT_PATH + RES_PATH + "/upload/course_application/notes/";
		String dbInsertPath = "/upload/course_application/notes/";
		
		if(file != null) {
			//파일 simpan한다.
			String filename = FileUploader.uploadFile(defaultPath, file);
			param.put("file_name", file.getOriginalFilename());
			param.put("file_path", dbInsertPath+filename);
		}
		
		if(academicDao.insertNotes(param) == 0) {
			throw new RuntimeLogicException("QUERY_FAIL [ admin - notes insert/update query fail ]", "004");
		}
		
		return resultMap;
	}

	public ResultMap updateDeposit(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		HashMap<String, String> state = academicDao.getCasState(param);
		
		if(state.get("cas_state").equals("Y"))
			resultMap.put("status", "201");
		else {
			if(academicDao.updateCasState(param) == 0) {
				throw new RuntimeLogicException("QUERY_FAIL [ admin - course_application_student deposit_state update query fail ]", "004");
			}
		}
			
		
		return resultMap;
	}

	@Transactional
	public ResultMap updateCasState(HashMap<String, Object> param) throws Exception {		
		ResultMap resultMap = new ResultMap();
		
		HashMap<String, String> state = academicDao.getCasState(param);
		
		if(!state.get("deposit_state").equals("Y"))
			resultMap.put("status", "201");
		else {
			param.put("cas_state", "Y");
			if(academicDao.updateCasState(param) == 0) {
				throw new RuntimeLogicException("QUERY_FAIL [ admin - course_application_student cas_state update query fail ]", "004");
			}
			
			if(academicDao.insertMpCurriculumSt(param) == 0) {
				throw new RuntimeLogicException("QUERY_FAIL [ admin - mp_curriculum_student insert update query fail ]", "004");
			}			
		}
		
		return resultMap;
	}

	public ResultMap updateCasCancelState(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		HashMap<String, String> state = academicDao.getCasState(param);
		
		if(!state.get("deposit_state").equals("Y"))
			resultMap.put("status", "201");
		else {
			param.put("cas_state", "N");
			if(academicDao.updateCasState(param) == 0) {
				throw new RuntimeLogicException("QUERY_FAIL [ admin - course_application_student cas_state update query fail ]", "004");
			}
			
			if(academicDao.updateMpCurriculumSt(param) == 0) {
				throw new RuntimeLogicException("QUERY_FAIL [ admin - mp_curriculum_student insert update query fail ]", "004");
			}			
		}
		
		return resultMap;
	}

	public ResultMap getStudentList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		if(param.get("searchType").toString().equals("search")) {
			resultMap.put("list", academicDao.getAcaStudentList(param));
		}else{
			resultMap.put("list", academicDao.getAcaAssignStudentList(param));	
		}
		
		return resultMap;
	}

	public ResultMap getAcaAssignStCnt(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("cnt", academicDao.getAcaAssignStudentCount(param));	
		resultMap.put("info", academicDao.getAcaAssignStudentAttendInfo(param));	
		
		return resultMap;
	}

	@Transactional
	public ResultMap insertAcaAssignSt(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		String[] user_seq = param.get("user_seq").toString().split(",");
		
		for(int i=0; i<user_seq.length; i++) {
			param.put("user_seq", user_seq[i]);
			if(academicDao.insertAcaAssignSt(param) == 0) {
				throw new RuntimeLogicException("QUERY_FAIL [ admin - mp_acadecmic_student insert/update query fail ]", "004");
			}	
		}
		
		return resultMap;
	}

	@Transactional
	public ResultMap cancelAcaAssignSt(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		String[] user_seq = param.get("user_seq").toString().split(",");
		
		for(int i=0; i<user_seq.length; i++) {
			param.put("user_seq", user_seq[i]);
			if(academicDao.updateAcaAssignSt(param) == 0) {
				throw new RuntimeLogicException("QUERY_FAIL [ admin - mp_acadecmic_student calcel update query fail ]", "004");
			}	
		}
		
		return resultMap;
	}

	@Transactional
	public ResultMap assignStConfirm(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		List<HashMap<String, Object>> stList = academicDao.confirmCancelAcaStList(param);
		
		if(academicDao.confirmAcaAssignSt(param) == 0) {
			throw new RuntimeLogicException("QUERY_FAIL [ admin - mp_acadecmic_student confirm update query fail ]", "004");
		}	
		
		for(int i=0; i<stList.size(); i++) {
			param.put("user_seq", stList.get(i).get("user_seq"));
			int cnt = academicDao.confirmUsersMcsUpdate(param);	
		}
		
		return resultMap;
	}

	@Transactional
	public ResultMap assignStConfirmCancel(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		List<HashMap<String, Object>> stList = academicDao.confirmCancelAcaStList(param);
		
		if(academicDao.confirmCancelAcaAssignSt(param) == 0) {
			throw new RuntimeLogicException("QUERY_FAIL [ admin - mp_acadecmic_student confirm calcel update query fail ]", "004");
		}	
		
		for(int i=0; i<stList.size(); i++) {
			param.put("user_seq", stList.get(i).get("user_seq"));
			if(academicDao.confirmCancelUsersAcaUpdate(param) == 0) {
				throw new RuntimeLogicException("QUERY_FAIL [ admin - users academic confirm calcel update query fail ]", "004");
			}	
		}
		
		return resultMap;
	}

	public ResultMap getReportInfo(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		//진행정보
		resultMap.put("progressInfo", pfLessonDao.getReportCurrProgress(param));
		//informasi dasar
		resultMap.put("basicInfo", pfLessonDao.getCurriculumBasicInfo(param));
		//현재 교육과정, 동일 학사, 전년 동일 교육과정 그래프 정보 가져오기
		resultMap.put("currGraph", pfLessonDao.getReportCurrGraph(param));
		//형성penilaian 리스트 가져오기
		resultMap.put("feList", pfLessonDao.getReportFe(param));
		//성적분포 리스트 가져오기
		resultMap.put("gradeList", pfLessonDao.getReportGrade(param));
		//syarat kelulusan별 시수
		resultMap.put("fcList", pfLessonDao.getReportFc(param));
		//단원 가져오기
		resultMap.put("unitList", unitDao.getUnitList(param));
		//임상표현 가져오기
		resultMap.put("clinicList", pfLessonDao.getReportClinic(param));
		//진단 가져오기
		resultMap.put("diaList",  pfLessonDao.getReportDia(param));
		
		param.put("professor_level", "1");
		resultMap.put("mpfList", academicDao.getCurriculumMpAssignPf(param));
		
		//부Dosen Penanggung jawab
		param.put("professor_level", "2");
		resultMap.put("dpfList", academicDao.getCurriculumMpAssignPf(param));
		return resultMap;
	}

	public ResultMap getCurrGradeStScore(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("scoreList", academicDao.getCurrGradeStScore(param));
		return resultMap;
	}

	public ResultMap getSoosiGradeStScore(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("scoreList", academicDao.getSoosiGradeStScore(param));
		return resultMap;
	}
}
