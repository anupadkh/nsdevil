package com.nsdevil.medlms.web.mpf.dao;

import java.util.List;
import java.util.HashMap;


public interface MPFGradeDao {
	public List<HashMap<String, Object>> getStList(HashMap<String, Object> param) throws Exception;

	public int insertGrade(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getGradeSubjectList(HashMap<String, Object> param) throws Exception;

	public int insertSubjectGrade(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getGradeCount(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getGradeStList(HashMap<String, Object> param) throws Exception;

	public int insertGradeSubject(HashMap<String, Object> param) throws Exception;

	public void updateGrade(HashMap<String, Object> param) throws Exception;

	public void insertSubjectScore(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getCurrStList(HashMap<String, Object> param) throws Exception;

	public int confirmGrade(HashMap<String, Object> param) throws Exception;

	public int insertGradeHistory(HashMap<String, Object> param) throws Exception;

	public int cancelConfirmGradeHistory(HashMap<String, Object> param) throws Exception;
}
