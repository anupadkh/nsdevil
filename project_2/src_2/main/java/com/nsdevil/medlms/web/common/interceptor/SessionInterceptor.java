package com.nsdevil.medlms.web.common.interceptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.nsdevil.medlms.common.service.LoginService;
import com.nsdevil.medlms.web.admin.service.AcademicService;


public class SessionInterceptor extends HandlerInterceptorAdapter {
	
	@Autowired
	private LoginService loginService;

	@Autowired
	private AcademicService academicService;
	
	@Value("#{config['config.overlapLogin']}")
	private String overLapLoginFlag;
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		HttpSession session = request.getSession();
		HashMap<String, Object> param = new HashMap<String, Object>();
		String userId = (String)session.getAttribute("S_USER_ID");
		String requestUrl = request.getRequestURL().toString();
		String contextPath = request.getContextPath();
		
 		if (session.getAttribute("S_USER_LEVEL") == null) {
 			if (!requestUrl.endsWith(contextPath+"/")) {
 				response.sendRedirect(request.getContextPath()+"/login");
 				return false;
 			}
 		}
		
		if (userId != null && !userId.equals("")) {
			//중복 로그인 처리 - 프로퍼티에서 설정 값 Y 일때만
			if("Y".equals(overLapLoginFlag)) {
				param.put("login_token", session.getAttribute("S_LOGIN_TOKEN"));
				if (loginService.isOverlapUser(param)) {
					request.getSession().invalidate();
					response.sendRedirect(request.getContextPath()+"?overlapLogin=Y");
					return false;
				}
			}
		}
		
		//메뉴 불러오기
		List<HashMap<String, Object>> allowMenuList = (List<HashMap<String, Object>>) session.getAttribute("S_ALLOW_MENU_LIST");
		if (allowMenuList == null || allowMenuList.isEmpty() || session.getAttribute("S_USER_LEVEL") == null) {
			param.put("user_level", null);
			allowMenuList = loginService.getAllowMenuList(param);
			session.setAttribute("S_ALLOW_MENU_LIST", allowMenuList);
		} else { //로그인 상태
			int userLevel = (int)session.getAttribute("S_USER_LEVEL");
			param.put("user_level", userLevel);
			allowMenuList = loginService.getAllowMenuList(param);
			session.setAttribute("S_ALLOW_MENU_LIST", allowMenuList);
		}
		
		for (int i = 0; i < allowMenuList.size(); i++) {
			HashMap<String, Object> allowMenu = allowMenuList.get(i);
			String url = String.valueOf(allowMenu.get("url"));
			if (requestUrl.indexOf(url) > -1) {
				allowMenu.put("now_menu_yn", "Y");
			} else {
				allowMenu.put("now_menu_yn", "N");
			}
			if (param.get("user_level") != null) {
				String boardCode = String.valueOf(allowMenu.get("board_menu_code"));
				if (!boardCode.equals("")) {
					List<HashMap<String, Object>> boardMenuList = new ArrayList<HashMap<String, Object>>();
					boardMenuList = loginService.getBoardMenuList(param);
					
					for (int k = 0; k < boardMenuList.size(); k++) {
						HashMap<String, Object> boardMenu = boardMenuList.get(k);
						String boardUrl = String.valueOf(boardMenu.get("url"));
						
						if (boardUrl.endsWith("/list")) {
							boardUrl = boardUrl.substring(0, boardUrl.lastIndexOf("/list"));
						} else if (boardUrl.endsWith("/detail")){
							boardUrl = boardUrl.substring(0, boardUrl.lastIndexOf("/detail"));
						}
						
						if (requestUrl.indexOf(boardUrl) > -1) {
							boardMenu.put("now_menu_yn", "Y");
						} else {
							boardMenu.put("now_menu_yn", "N");
						}
					}
					
					JSONArray jsonArray = new JSONArray();
					jsonArray.addAll(boardMenuList);
					session.setAttribute("S_BOARD_MENU_LIST", jsonArray.toJSONString());
				}
			}
			
		}
		
		if(requestUrl.indexOf("admin/academic") > -1) {
			param.put("code_cate", "osce_code_name");
			session.setAttribute("code1", academicService.getCodeMenuName(param));
			param.put("code_cate", "cpx_code_name");
			session.setAttribute("code2", academicService.getCodeMenuName(param));
			param.put("code_cate", "dia_code_name");
			session.setAttribute("code3", academicService.getCodeMenuName(param));
		}
		
		
		
		return true;	
	}
}

