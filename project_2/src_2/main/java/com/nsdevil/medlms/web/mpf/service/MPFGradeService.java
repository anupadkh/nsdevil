package com.nsdevil.medlms.web.mpf.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.exception.RuntimeLogicException;
import com.nsdevil.medlms.web.admin.dao.AcademicDao;
import com.nsdevil.medlms.web.common.dao.CommonDao;
import com.nsdevil.medlms.web.mpf.dao.MPFGradeDao;
import com.nsdevil.medlms.web.pf.dao.PFLessonDao;

@Service
public class MPFGradeService {
	
	@Autowired
	private PFLessonDao pfLessonDao;
	
	@Autowired
	private MPFGradeDao mpfGradeDao;
	
	@Autowired
	private CommonDao commonDao;
	
	@Autowired
	private AcademicDao academicDao;
	
	public ResultMap getStList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		//dasar dasar kurikulum
		resultMap.put("basicInfo", pfLessonDao.getCurriculumBasicInfo(param));
		
		//과목명 가져오기
		resultMap.put("subjectList", mpfGradeDao.getGradeSubjectList(param));
				
		//학생 정보
		resultMap.put("stList", mpfGradeDao.getStList(param));
		
		resultMap.put("gradeCount", mpfGradeDao.getGradeCount(param));
		return resultMap;
	}

	@Transactional
	public ResultMap insertGrade(HashMap<String, Object> param) throws Exception{
		ResultMap resultMap = new ResultMap();

		JSONParser jsonParser = new JSONParser();
		
		JSONObject jsonObject = (JSONObject) jsonParser.parse(param.get("scorelist").toString());
		
		JSONArray scoreList = (JSONArray) jsonObject.get("list");
		
		for(int i=0; i<scoreList.size() ; i++) {
			JSONObject score = (JSONObject) scoreList.get(i);
			
			param.put("id", score.get("id").toString());
			param.put("final_grade", score.get("final_grade").toString());
			
			
			JSONArray subject_list = (JSONArray) score.get("subject_seq_list");		
			
			for(int sub_i=0;sub_i<subject_list.size();sub_i++) {
				JSONObject subject_info = (JSONObject) subject_list.get(sub_i);
				param.put("subject_seq", subject_info.get("subject_seq").toString());
				param.put("score", subject_info.get("score").toString());
				if(mpfGradeDao.insertSubjectGrade(param) == 0) {
					throw new RuntimeLogicException("QUERY_FAIL [ pf - report_cart_subject_score insert/update query fail ]", "004");
				}
			}			

			if (mpfGradeDao.insertGrade(param) == 0) {
				throw new RuntimeLogicException("QUERY_FAIL [ pf - report_cart insert/update query fail ]", "004");
			}
		}
		
		return resultMap;
	}

	public ResultMap gradeStList(HashMap<String, Object> param) throws Exception{
		ResultMap resultMap = new ResultMap();
		resultMap.put("stList", mpfGradeDao.getGradeStList(param));
		
		return resultMap;
	}
	
	@Transactional
	public ResultMap gradeExcelCreate(MultipartFile uploadFile, HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		String reg_user_seq = param.get("reg_user_seq").toString();
		if (uploadFile != null) {

			//이전 registrasi된거 use_flag='N' 변경
			mpfGradeDao.updateGrade(param);
			
			@SuppressWarnings("resource")
			Workbook workbook = new XSSFWorkbook(uploadFile.getInputStream());
			//int sheetNum = workbook.getNumberOfSheets(); // 촟 Sheet 갯수
	
			Sheet sheet = workbook.getSheetAt(0); // Sheet의 데이터 가져옴
						
			int cellIndex = 0;
			int subjectIndex = 0;
			List<String> subject = new ArrayList<String>();
			int rows = sheet.getPhysicalNumberOfRows(); // 총 Row 갯수
			for (int r = 0; r < rows; r++) {
				Row row = sheet.getRow(r);
				HashMap<String, Object> gradeMap = new HashMap<String, Object>();
								
				if (row != null) { // Row가 null이 아닐때
					
					//과목 simpan
					if(r == 0) {
						int cells = row.getLastCellNum();
						for(int c=4;c<cells;c++) {
							Cell cell = row.getCell(c);
							String value = "";
							if (cell != null) {
								switch (cell.getCellTypeEnum()) {
									case NUMERIC: // 숫자형		
											value = "" + (double) cell.getNumericCellValue();
										break;
									case STRING: // 문자형
										value = "" + cell.getStringCellValue();
										break;
									case BLANK: // 값이 없을때
										value = "";
										break;
									case ERROR:
										value = "" + cell.getErrorCellValue(); // byte
										break;
									default:
								}
							}
							
							if(value.isEmpty())
								break;
							
							switch (c) {
								case 0: //No 
									break;
								case 1: //학번
									break;
								case 2: //학생이름
									break;
								case 3: //확정등급						
									break;								
								default :	
									subjectIndex++;
									param.put("subject", value);
									param.put("order_num", subjectIndex);
									mpfGradeDao.insertGradeSubject(param);
									subject.add(param.get("subject_seq").toString());
									cellIndex=c+1;
									break;							
							}							
						}		
					}else {			
						int scoreIndex = 0;
						for (int c = 1; c < cellIndex; c++) {
							Cell cell = row.getCell(c);
							String value = "";
							if (cell != null) {
								switch (cell.getCellTypeEnum()) {
									case NUMERIC: // 숫자형								
										if (c == 1)
											value = "" + (int) cell.getNumericCellValue();
										else
											value = "" + (double) cell.getNumericCellValue();
										break;
									case STRING: // 문자형
										value = "" + cell.getStringCellValue();
										break;
									case BLANK: // 값이 없을때
										value = "";
										break;
									case ERROR:
										value = "" + cell.getErrorCellValue(); // byte
										break;
									default:
								}
							}

							switch (c) {
								case 0: //No 
									break;
								case 1: //학번
									gradeMap.put("id", value);
									break;
								case 2: //학생이름
									break;
								case 3: //확정등급
									gradeMap.put("final_grade", value);								
									break;								
								default :
									param.put("subject_seq", subject.get(scoreIndex));
									param.put("score", value);
									param.put("id", gradeMap.get("id").toString());
									mpfGradeDao.insertSubjectScore(param);
									scoreIndex++;
									break;							
							}
						}
						gradeMap.put("reg_user_seq", reg_user_seq);	
						gradeMap.put("curr_seq", param.get("curr_seq").toString());
						
						HashMap<String, Object> chkMap = academicDao.getStCheck(gradeMap);
						
						if(chkMap.get("st_cnt").toString().equals("0")){
							resultMap.put("status", "001");
							resultMap.put("msg", gradeMap.get("id").toString()+" 해당 아이디는 없는 아이디 입니다.");
							throw new RuntimeLogicException(gradeMap.get("id").toString()+" 해당 아이디는 없는 아이디 입니다.");
						}else if(chkMap.get("curr_st_cnt").toString().equals("0")){
							resultMap.put("status", "001");
							resultMap.put("msg", gradeMap.get("id").toString()+" 해당 교육과정에 registrasi 되지 않은 학생 입니다.");
							throw new RuntimeLogicException(gradeMap.get("id").toString()+" 해당 교육과정에 registrasi 되지 않은 학생 입니다.");
						}
						/*
						if(commonDao.getIdCheck(gradeMap) == 0) {
							resultMap.put("status", "001");
							resultMap.put("msg", gradeMap.get("id").toString()+" 해당 아이디는 없는 아이디 입니다.");
						}
*/						
						mpfGradeDao.insertGrade(gradeMap);
					}
				}				
			}			
		}

		/*
		 * for(int i=0; i<patientVoArray.size();i++){
		 * patientDao.addPatient(patientVoArray.get(i)); }
		 */
		return resultMap;
	}

	public List<HashMap<String, Object>> getCurrStList(HashMap<String, Object> param) throws Exception {
		
		return mpfGradeDao.getCurrStList(param);
	}

	public List<HashMap<String, Object>> getSoosiList(HashMap<String, Object> param) throws Exception {
		return academicDao.getCurrSooSiList(param);
	}
	
	@Transactional
	public ResultMap confirmGrade(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		if(mpfGradeDao.insertGradeHistory(param) == 0) {
			throw new RuntimeLogicException("QUERY_FAIL [ mpf - grade history query fail ]", "004");
		}

		if(mpfGradeDao.confirmGrade(param) == 0) {
			throw new RuntimeLogicException("QUERY_FAIL [ mpf - grade confirm query fail ]", "004");
		}
		
		return resultMap;
	}

	public ResultMap confirmCancelGrade(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		if(mpfGradeDao.cancelConfirmGradeHistory(param) == 0) {
			throw new RuntimeLogicException("QUERY_FAIL [ mpf - grade history cancel query fail ]", "004");
		}	
		
		return resultMap;
	}

	
}