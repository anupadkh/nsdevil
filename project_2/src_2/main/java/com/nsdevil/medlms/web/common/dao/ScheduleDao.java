package com.nsdevil.medlms.web.common.dao;

import java.util.HashMap;
import java.util.List;


public interface ScheduleDao {

	public List<HashMap<String, Object>> getMYSchedule(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getMemo(HashMap<String, Object> param) throws Exception;
	public void addMemo(HashMap<String, Object> param) throws Exception;
	public void deleteMemo(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getMYScheduleSt(HashMap<String, Object> param) throws Exception;

}
