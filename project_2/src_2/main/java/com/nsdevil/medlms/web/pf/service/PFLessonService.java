package com.nsdevil.medlms.web.pf.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.exception.LogicException;
import com.nsdevil.medlms.common.exception.ResourceNotFoundException;
import com.nsdevil.medlms.common.exception.RuntimeLogicException;
import com.nsdevil.medlms.common.util.FileUploader;
import com.nsdevil.medlms.common.util.PagingHelper;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.common.util.ZipUtil;
import com.nsdevil.medlms.web.admin.dao.AcademicDao;
import com.nsdevil.medlms.web.mpf.dao.MPFUnitDao;
import com.nsdevil.medlms.web.pf.dao.PFLessonDao;

@Service
public class PFLessonService {

	@Autowired
	private PFLessonDao pfLessonDao;
	
	@Autowired
	private AcademicDao academicDao;

	@Autowired
	private MPFUnitDao unitDao;
	
	@Value("#{config['config.rootPath']}")
	private String ROOT_PATH;
	
	@Value("#{config['config.resPath']}")
	private String RES_PATH;
	
	@Value("#{config['config.quizPath']}")
	private String QUIZ_PATH;
	
	@Value("#{config['config.quizTempPath']}")
	private String QUIZ_TEMP_PATH;

	@Value("#{config['config.formationEvaluation']}")
	private String FORMATION_EVALUAION_PATH;
	
	
	
	public ResultMap getLessonLeftMenuList(HashMap<String, Object> param, HttpServletRequest request) throws Exception {
		ResultMap resultMap = new ResultMap();
		String currentAcademicYear = (String) param.get("academicYear");//현재 학사tahun
		
		//학사tahun목록
		List<HashMap<String, Object>> academicYearList = pfLessonDao.getLeftMenuAcademicYearList(param);
		
		if (currentAcademicYear == null || currentAcademicYear.equals("")) {//pilih한 학사tahun가 없으면 최근 첫번째 학사tahun
			if(request.getSession().getAttribute("S_CURRENT_ACADEMIC_YEAR") == null) {
				if(!academicYearList.isEmpty())
					currentAcademicYear = academicYearList.get(0).get("year").toString();					
			}else
				currentAcademicYear = request.getSession().getAttribute("S_CURRENT_ACADEMIC_YEAR").toString();
		}
		//현재 학사tahun session set
		request.getSession().setAttribute("S_CURRENT_ACADEMIC_YEAR", currentAcademicYear);
		
		param.put("current_academic_year", currentAcademicYear);
		List<HashMap<String, Object>> curriculumList = pfLessonDao.getLeftMenuCurriculumList(param);
		
		//교육과정에 수업계획서 리스트 insert
		for (HashMap<String, Object> curriculumInfo : curriculumList) {
			param.put("curr_seq", curriculumInfo.get("curr_seq"));
			param.put("curr_represent_yn", curriculumInfo.get("curr_represent_yn"));

			PagingHelper pagingHelper = new PagingHelper(5, 1);

			//카운트 취득
			int totalCount = pfLessonDao.getLessonPlanListCount(param);
			
			if(totalCount > 0) {
				//현재 hari dan tanggal에 가장 가까운거 rownumber 가져온다.
				int row_num = pfLessonDao.getTodayLessonPeriodSeq(param);
				
				int page = (int) Math.ceil(row_num/5.0);
				param.put("page", String.valueOf(page));
			}
				List<HashMap<String, Object>> list = pfLessonDao.getLessonPlanList(pagingHelper.getPagingParam(param, totalCount));
				
				curriculumInfo.put("lesson_plan_list", list);
				curriculumInfo.put("countInfo", pagingHelper.getPageCnt(totalCount));
			
		}	
		
		if(request.getSession().getAttribute("S_CURRICULUM_SEQ") == null) {
			if(!curriculumList.isEmpty())
				request.getSession().setAttribute("S_CURRICULUM_SEQ", curriculumList.get(0).get("curr_seq").toString());
		}
			
		resultMap.put("academic_year_list", academicYearList);
		resultMap.put("curriculum_list", curriculumList);
		resultMap.put("current_academic_year", currentAcademicYear);
		return resultMap;
	}
	
	
	
	public ResultMap getCurriculumExistCheck(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		int curriculumCount = pfLessonDao.getCurriculumCount(param);
		if (curriculumCount < 1) {
			throw new ResourceNotFoundException("NOT_ASSIGN_PROFESSOR", "007");
		}
		return resultMap;
	}
	
	
	public ResultMap getCurriculumDetail(HashMap<String, Object> param, HttpServletRequest request) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		//dasar dasar kurikulum
		HashMap<String, Object> curriculumBasicInfo = new HashMap<String, Object>();
		curriculumBasicInfo = pfLessonDao.getCurriculumBasicInfo(param);
		
		if (curriculumBasicInfo == null || curriculumBasicInfo.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_CURRICULUM_BASIC_INFO", "007");
		}
		resultMap.put("basicInfo", curriculumBasicInfo);
		//Dosen Penanggung jawab
		param.put("professor_level", "1");
		resultMap.put("mpfList", academicDao.getCurriculumMpAssignPf(param));
		
		//부Dosen Penanggung jawab
		param.put("professor_level", "2");
		resultMap.put("dpfList", academicDao.getCurriculumMpAssignPf(param));
		
		//일반교수
		param.put("professor_level", "3");
		resultMap.put("gpfList", academicDao.getCurriculumMpAssignPf(param));
		
		//tidak ada perkuliahan 리스트 가져오기
		resultMap.put("lessonMethodList", pfLessonDao.getLessonMethodList(param));
		
		//형성penilaian 리스트 가져오기
		resultMap.put("formationEvaluationCs", pfLessonDao.getFormationEvaluationCs(param));
		
		//총괄penilaian기준 가져오기
		resultMap.put("summativeEvaluationCs", pfLessonDao.getSummativeEvaluation(param));
		
		//syarat kelulusan 가져오기
		resultMap.put("mpCurriculumGraduationCapabilityList", pfLessonDao.getMpCurriculumGraduationCapabilityList(param));
		
		//nilai기준 가져오기
		//resultMap.put("gradeStandard", pfLessonDao.getGradeStandardList(param));
		
		return resultMap;
	}

	public List<HashMap<String, Object>> graduationCapabilityList(HashMap<String, Object> param) throws Exception {
		// TODO Auto-generated method stub
		return pfLessonDao.getGraduationCapabilityListMaxVersion(param);
	}



	public List<HashMap<String, Object>> mpCurriculumGraduationCapabilityList(HashMap<String, Object> param) throws Exception {
		// TODO Auto-generated method stub
		return pfLessonDao.getMpCurriculumGraduationCapabilityList(param);
	}


	@Transactional
	public ResultMap curriculumModify(HashMap<String, Object> param, String[] pf_user_seq, String[] pf_level,
			String[] eval_method_code, String[] eval_domain_code, String[] importance, String[] eval_method_text,
			String[] eval_etc, String[] fc_seq, String[] process_result, String[] teaching_method,
			String[] evaluation_method, String[] se_seq, String[] grade_standard_code, String[] grade_importance) throws Exception {

		ResultMap resultMap = new ResultMap();
		if (pfLessonDao.updateCurriculum(param) == 0) {
			throw new RuntimeLogicException("QUERY_FAIL [ mpf - curriculum update query fail ]", "004");
		}

		param.put("deleteType","pf");	
		//교수 맵핑 테이블 일반교수 삭제
		academicDao.deleteMpAssignPf(param);
		if(pf_user_seq != null) {
			for(int i=0;i<pf_user_seq.length;i++){		
				//일반교수만 registrasi한다.
				if(pf_level[i].equals("3")){
					param.put("user_seq", pf_user_seq[i]);
					param.put("professor_level", pf_level[i]);	
					academicDao.insertMpAssignPf(param);
				}
			}
		}
		
		//총괄penilaian기준 삭제 후 registrasi
		pfLessonDao.updateSummativeEvaluation(param);	
		if(se_seq != null) {
			for(int i=0;i<se_seq.length;i++){
				param.put("se_seq",se_seq[i]);
				param.put("eval_method_code",eval_method_code[i]);
				param.put("eval_domain_code",eval_domain_code[i]);
				param.put("importance",importance[i]);
				param.put("eval_method_text",eval_method_text[i]);
				param.put("eval_etc",eval_etc[i]);
				param.put("order_num", (i+1));
				pfLessonDao.insertSummativeEvaluation(param);
			}
		}
		
		//syarat kelulusan 삭제 후 registrasi		
		pfLessonDao.updateMpCurriculumFinishCapability(param);
		if(fc_seq != null) {
			for(int i=0;i<fc_seq.length;i++){
				param.put("fc_seq",fc_seq[i]);
				param.put("process_result",process_result[i]);
				param.put("teaching_method",teaching_method[i]);
				param.put("evaluation_method",evaluation_method[i]);
				param.put("fc_order",(i+1));	
				pfLessonDao.insertMpCurriculumFinishCapability(param);
			}
		}
		
		JSONParser jsonParser = new JSONParser();
		
		JSONObject jsonObject = (JSONObject) jsonParser.parse(param.get("lm_json").toString());
		
		JSONArray lmList = (JSONArray) jsonObject.get("list");
		
		for(int i=0; i<lmList.size();i++) {
			JSONObject lm = (JSONObject) lmList.get(i);
			
			param.put("lm_code", lm.get("lm_code").toString());
			param.put("period", lm.get("period").toString());
			pfLessonDao.insertCurrLessonMethod(param);
		}
		
		jsonObject = (JSONObject) jsonParser.parse(param.get("fe_json").toString());
		
		JSONArray feList = (JSONArray) jsonObject.get("list");
		
		for(int i=0; i<feList.size();i++) {
			JSONObject fe = (JSONObject) feList.get(i);
			
			param.put("fe_code", fe.get("fe_code").toString());
			param.put("fe_num", fe.get("period").toString());
			pfLessonDao.insertCurrFE(param);
		}
		
		/*
		//nilai기준 삭제 후 registrasi
		pfLessonDao.updateGradeStandard(param);
		if(grade_standard_code != null) {
			for(int i=0;i<grade_standard_code.length;i++){
				param.put("grade_code", grade_standard_code[i]);
				param.put("grade_importance", grade_importance[i]);
				pfLessonDao.insertGradeStandard(param);
			}
		}		
		*/
		return resultMap;
	}

	public List<HashMap<String, Object>> lessonMethodSujectList(HashMap<String, Object> param) throws Exception {
		
		return pfLessonDao.getLessonMethodSuject(param);
	}


	public ResultMap lessonPlanBasicInfo(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("lessonPlanBasicInfo", pfLessonDao.getLessonPlanBasicInfo(param)); //수업계획서 정보
		return resultMap;
	}

	public ResultMap lessonPlanSummativeInfo(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("lessonCoreClinicList", pfLessonDao.getLessonCoreClinicList(param)); //핵심 임상표현
		resultMap.put("lessonClinicList", pfLessonDao.getLessonClinicList(param)); //관련 임상표현
		resultMap.put("lessonDiagnosisList", pfLessonDao.getLessonDiagnosisList(param)); //진단명
		resultMap.put("lessonOsceList", pfLessonDao.getLessonOsceList(param)); //임상술기
		resultMap.put("lessonFinishCapabilityList", pfLessonDao.getLessonFinishCapabilityList(param)); //syarat kelulusan
		resultMap.put("formationEvaluationList", pfLessonDao.formationEvaluationCodeList(param)); //형성penilaian
		resultMap.put("lessonMethodList", pfLessonDao.lessonMethodCodeList(param)); //tidak ada perkuliahan
		
		return resultMap;
	}

	public List<HashMap<String, Object>> CurrFinishCapabilityList(HashMap<String, Object> param) throws Exception {

		int count = pfLessonDao.getCurrFinishCapabilityCount(param);
		
		List<HashMap<String, Object>> map = pfLessonDao.getCurrFinishCapability(param);
		if(count == 0)
			map = pfLessonDao.getGraduationCapabilityListMaxVersion(param);
		
		return map;
	}


	@Transactional
	public ResultMap lessonPlanModify(HashMap<String, Object> param, String[] coreClinic_code, String[] clinic_code, String[] osce_code, String[] dia_code,
			String[] fe_code, String[] fc_seq, String[] fe_seq, String[] lesson_method_code) throws Exception {
		
		//모든 테이블 N으로 변경 해준다.
		pfLessonDao.updateLessonAllTable(param);
		if (pfLessonDao.updateLessonPlan(param) == 0) {
			throw new RuntimeLogicException("QUERY_FAIL [ pf - lesson_plan update query fail ]", "004");
		}
		
		HashMap<String, Object> lessonPlanInfo = pfLessonDao.getLessonPlanBasicInfo(param);
		//수업계획서 perbaiki
		
		String lesson_subject = lessonPlanInfo.get("lesson_subject").toString();
		String lesson_date = lessonPlanInfo.get("lesson_date").toString();
		String quiz = "퀴즈";
		
		String fe_name = "";
		
		if(lesson_date.indexOf("/") != -1)
			fe_name = lesson_subject + " " + lesson_date.split("/")[0] +"월 " + lesson_date.split("/")[1] + "일 " + quiz;
		else
			fe_name = lesson_subject + " " + quiz;
		
		//핵심 임상표현 simpan
		if(coreClinic_code != null) {
			for(int i=0;i<coreClinic_code.length;i++){
				param.put("clinic_code", coreClinic_code[i]);
				if (pfLessonDao.insertLessonCoreClinic(param) == 0) {
					throw new RuntimeLogicException("QUERY_FAIL [ pf - lesson_core_clinic update query fail ]", "004");
				}
			}
		}
		
		//관련 임상표현 simpan
		if(clinic_code != null) {
			for(int i=0;i<clinic_code.length;i++){
				param.put("clinic_code", clinic_code[i]);
				
				if (pfLessonDao.insertLessonClinic(param) == 0) {
					throw new RuntimeLogicException("QUERY_FAIL [ pf - lesson_clinic update query fail ]", "004");
				}
			}
		}		

		//관련 임상표현 simpan
		if(osce_code != null) {
			for(int i=0;i<osce_code.length;i++){
				param.put("skill_code", osce_code[i]);
				if (pfLessonDao.insertLessonSkill(param) == 0) {
					throw new RuntimeLogicException("QUERY_FAIL [ pf - lesson_skill update query fail ]", "004");
				}
			}
		}
		
		//진단명 simpan
		if(dia_code != null) {
			for(int i=0;i<dia_code.length;i++){
				param.put("dia_code", dia_code[i]);
				if (pfLessonDao.insertLessonDiagnosis(param) == 0) {
					throw new RuntimeLogicException("QUERY_FAIL [ pf - lesson_diagnosis update query fail ]", "004");
				}
			}
		}
		
		//형성penilaian simpan
		if(fe_code != null) {
			for(int i=0;i<fe_code.length;i++){
				param.put("fe_code", fe_code[i]);
				param.put("fe_seq", fe_seq[i]);
				param.put("fe_name", fe_name);
				if (pfLessonDao.insertFormationEvaluation(param) == 0) {
					throw new RuntimeLogicException("QUERY_FAIL [ pf - lesson_finish_capability update query fail ]", "004");
				}
			}
		}
		
		//syarat kelulusan simpan
		if(fc_seq != null) {
			for(int i=0;i<fc_seq.length;i++){
				param.put("fc_seq", fc_seq[i]);
				if (pfLessonDao.insertLessonFinishCapability(param) == 0) {
					throw new RuntimeLogicException("QUERY_FAIL [ pf - lesson_finish_capability update query fail ]", "004");
				}
			}
		}
				
		//tidak ada perkuliahan simpan
		if(lesson_method_code != null) {
			for(int i=0;i<lesson_method_code.length;i++) {
				param.put("lesson_method_code", lesson_method_code[i]);
				if (pfLessonDao.insertLessonMethod(param) == 0) {
					throw new RuntimeLogicException("QUERY_FAIL [ pf - lesson_method update query fail ]", "004");
				}
			}
		}
		ResultMap resultMap = new ResultMap();
		return resultMap;
	}

	public List<HashMap<String, Object>> preLessonPlanList(HashMap<String, Object> param) throws Exception {
		
		return pfLessonDao.getPreLessonPlan(param);
	}



	public List<HashMap<String, Object>> getAcademicYear(HashMap<String, Object> param) throws Exception {
		
		return pfLessonDao.getLeftMenuAcademicYearList(param);
	}



	public List<HashMap<String, Object>> summativeEvaluationList(HashMap<String, Object> param) throws Exception {
		
		return pfLessonDao.getSummativeEvaluationCode(param);
	}



	public String PfLevelChk(HashMap<String, Object> param) {
		// TODO Auto-generated method stub
		String level = pfLessonDao.getPfLevelCheck(param);
		if(level == null)
			level = "";
		return level;
	}
	
	@Transactional
	public ResultMap lessonDataInsert(MultipartHttpServletRequest request, HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		List<MultipartFile> multiImg = request.getFiles("imgMulti");
		List<MultipartFile> multiFile = request.getFiles("fileMulti");
		List<MultipartFile> video = request.getFiles("video");
		String[] youtubeUrl = request.getParameterValues("youtubeUrl");
		
		String[] youtubeExplan = request.getParameterValues("youtubeExplan");
		String[] multiImgExplan = request.getParameterValues("multiImgExplan");
		String[] videoExplan = request.getParameterValues("videoExplan");
		
		String[] imgGroupNum = request.getParameterValues("imgGroupNum");
		String[] lesson_attach_seq = request.getParameterValues("lesson_attach_seq");
				
		String curr_seq = request.getSession().getAttribute("S_CURRICULUM_SEQ").toString(); //교육과정 시퀀스
		String year = request.getSession().getAttribute("S_CURRENT_ACADEMIC_YEAR").toString(); //tahun
		String lp_seq = request.getSession().getAttribute("S_LP_SEQ").toString(); //수업계획서 시퀀스
				
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("lp_seq", lp_seq);
		
		if(param.get("lesson_data_seq").toString().equals("0")){
			if (pfLessonDao.insertLessonData(param) == 0) {
				throw new RuntimeLogicException("QUERY_FAIL [ pf - lesson_data insert query fail ]", "004");
			}
		}
		else{
			if (pfLessonDao.updateLessonData(param) == 0) {
				throw new RuntimeLogicException("QUERY_FAIL [ pf - lesson_data update query fail ]", "004");
			}
		}
		
		String defaultPath = ROOT_PATH + RES_PATH + "/upload/"+year+"/"+curr_seq+"/"+lp_seq+"/lesson_data/";
		String dbInsertPath = "/upload/"+year+"/"+curr_seq+"/"+lp_seq+"/lesson_data/";
		//수업 첨부자료 use_flag N으로 변경
		pfLessonDao.updateLessonAttachUseFlag(param);
			
		//첨부자료 simpan 한것들 다시 Y 로 바꿔준다.
		if(lesson_attach_seq != null) {
			for(int i=0;i<lesson_attach_seq.length;i++){
				param.put("lesson_attach_seq", lesson_attach_seq[i]);
				if (pfLessonDao.updateLessonAttachUseFlagY(param) == 0)
					throw new RuntimeLogicException("QUERY_FAIL [ pf - lesson_attach update query fail ]", "004");
			}
		}
		
		//사진 여러장 simpan한다.
		int group = pfLessonDao.getLessonAttachGroupNum(param);
		int group_index = 0;
		int group_sum = 0;
		
		if(multiImg != null) {
			for(int i=0; i<multiImg.size();i++){
				if(multiImg.get(i) != null){					
					param.put("group_num", group+1);
					param.put("explan", multiImgExplan[group_index]);
					String filename = FileUploader.uploadFile(defaultPath, multiImg.get(i));
					String filePath = filename.substring(0, filename.lastIndexOf("."));
					
					//zip 파일 일경우
					if(Util.getFileType(filename) == "Z") {
						//압축해제
						ZipUtil.unzip(defaultPath+filename, defaultPath+filePath);
						File path = new File(defaultPath+filePath);
						File[] fileList=path.listFiles();
						if(fileList.length == 0)
							throw new RuntimeLogicException("[ pf - lesson_attach unzip error ]", "004");
						
						for(int fi=0; fi<fileList.length; fi++) {
							//변경할 파일명 가져온다.
							String changeFilename = FileUploader.getUploadFileName(defaultPath+filename, fileList[fi].getName());
							
							File fileNew = new File( path.getPath()+"/"+changeFilename );
							String fileType = Util.getFileType(fileList[fi].getName());
							if(!fileType.equals("I")) {
								throw new RuntimeLogicException("zip파일에 image가 아닌 파일이 있습니다.", "004");
							}
							//파일명 변경함.
							fileList[fi].renameTo(fileNew);
							param.put("file_name", fileList[fi].getName());
							param.put("file_path", dbInsertPath+filePath+"/"+changeFilename);
							param.put("attach_type", fileType);
							if(pfLessonDao.insertLessonAttach(param) == 0)
								throw new RuntimeLogicException("QUERY_FAIL [ pf - lesson_attach multiImg insert query fail ]", "004");
						}
						group_sum+=Integer.parseInt(imgGroupNum[group_index]);
						group_index++;
						group++;
					}else {				
					
						param.put("file_name", multiImg.get(i).getOriginalFilename());
						param.put("file_path", dbInsertPath+filename);
						param.put("attach_type", Util.getFileType(filename));
						if(pfLessonDao.insertLessonAttach(param) == 0)
							throw new RuntimeLogicException("QUERY_FAIL [ pf - lesson_attach multiImg insert query fail ]", "004");
						if((Integer.parseInt(imgGroupNum[group_index])+group_sum) == (i+1)){
							group_sum+=Integer.parseInt(imgGroupNum[group_index]);
							group_index++;
							group++;
						}
					}
				}
			}
		}
		
		if(multiFile != null) {
			//파일 simpan한다.
			for(int i=0; i<multiFile.size();i++){
				if(multiFile.get(i) != null){
					param.put("group_num", group+1);
					String filename = FileUploader.uploadFile(defaultPath, multiFile.get(i));
					param.put("file_name", multiFile.get(i).getOriginalFilename());
					param.put("file_path", dbInsertPath+filename);
					param.put("attach_type", Util.getFileType(filename));
					if(pfLessonDao.insertLessonAttach(param) == 0)
						throw new RuntimeLogicException("QUERY_FAIL [ pf - lesson_attach file insert query fail ]", "004");
					group++;
				}
			}
		}
		
		if(video != null) {
			//파일 simpan한다.
			for(int i=0; i<video.size();i++){
				if(video.get(i) != null){
					param.put("group_num", group+1);
					String filename = FileUploader.uploadFile(defaultPath, video.get(i));
					param.put("explan", videoExplan[i]);
					param.put("file_name", video.get(i).getOriginalFilename());
					param.put("file_path", dbInsertPath+filename);
					param.put("attach_type", Util.getFileType(filename));
					if(pfLessonDao.insertLessonAttach(param) == 0)
						throw new RuntimeLogicException("QUERY_FAIL [ pf - lesson_attach video insert query fail ]", "004");
					group++;
				}
			}
		}
		
		if(youtubeUrl != null){
			for(int i=0;i<youtubeUrl.length;i++){
				param.put("group_num", group+1);
				param.put("youtube_url", youtubeUrl[i]);
				param.remove("file_name");
				param.remove("file_path");
				param.put("attach_type", "Y");
				param.put("explan", youtubeExplan[i]);
				if(pfLessonDao.insertLessonAttach(param) == 0)
					throw new RuntimeLogicException("QUERY_FAIL [ pf - lesson_attach youtube insert query fail ]", "004");

				group++;
			}
		}
		resultMap.put("lesson_data_seq", param.get("lesson_data_seq"));
		return resultMap;
	}

	public int getLessonDataSeq(HashMap<String, Object> param) {
		int count = pfLessonDao.getLessonDataCount(param);
		if(count != 0)
			count = pfLessonDao.getLessonDataSeq(param);
		return count;
	}

	public ResultMap getLessonDataList(HashMap<String, Object> param) {
		ResultMap resultMap = new ResultMap();
		resultMap.put("lessonDataList", pfLessonDao.getLessonData(param));
		resultMap.put("lessonAttachList", pfLessonDao.getLessonAttach(param));
		return resultMap;
	}
	
	public ResultMap getFormationEvaluationList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		HashMap<String, Object> feTitleInfo = new HashMap<String, Object>();
		feTitleInfo = pfLessonDao.getFormationEvaluationTitleFull(param);
		List<HashMap<String, Object>> feList = new ArrayList<HashMap<String, Object>>();
		feList = pfLessonDao.getFormationEvaluationList(param);
		resultMap.put("feTitleInfo", feTitleInfo);
		resultMap.put("feList", feList);
		return resultMap;
	}
	
	public ResultMap getQuizList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		HashMap<String, Object> formationEvaluationInfo = pfLessonDao.getFormationEvaluationInfo(param); 
		if (formationEvaluationInfo == null || formationEvaluationInfo.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_FORMATION_EVALUATION_INFO", "007");
		}
		
		List<HashMap<String, Object>> quizList = new ArrayList<HashMap<String, Object>>();
		quizList = pfLessonDao.getQuizList(param);
		resultMap.put("feInfo", formationEvaluationInfo);
		resultMap.put("quizList", quizList);
		return resultMap;
	}
	
	public ResultMap formationEvaluationTitleModify(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		if (pfLessonDao.updateFormationEvaluationTitle(param) < 1) {
			throw new RuntimeLogicException("QUERY_FAIL [ formation evalution title update fail ]", "004");
		}		
		return resultMap;		
	}
	
	@Transactional
	public ResultMap quizCreate(HashMap<String, Object> param, HttpServletRequest request) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		int quizSeq = 0;
		int answerViewCnt = Integer.parseInt(String.valueOf(param.get("answerViewCnt"))); //답가지 개수
		String[] answerNumberList = String.valueOf(param.get("answerText")).split(", "); //정답 배열
		
		//QUIZ INSERT
		HashMap<String, Object> quizParam = new HashMap<String, Object>();
		quizParam.put("fe_seq", param.get("feSeq"));
		quizParam.put("quiz_question", param.get("quizQuestion"));
		quizParam.put("quiz_order", param.get("quizOrder"));
		quizParam.put("s_user_seq", param.get("s_user_seq"));
		if (pfLessonDao.insertQuiz(quizParam) < 1) {
			throw new RuntimeLogicException("QUERY_FAIL [ quiz insert fail ]", "004");
		}
		quizSeq = (int)quizParam.get("quiz_seq");
		
		//QUIZ_ANSWER INSERT
		HashMap<String, Object> quizAnswerParam = new HashMap<String, Object>();
		quizAnswerParam.put("quiz_seq", quizSeq);
		quizAnswerParam.put("s_user_seq", param.get("s_user_seq"));
		
		for (int answerContentIdx = 1; answerContentIdx <= answerViewCnt; answerContentIdx++) { //답가지 텍스트 입력 확인
			String answerViewText = String.valueOf(param.get("answerContent" + answerContentIdx));
			String answerFlag = "N";
			String attachType = "T"; // I : 이미지, T : 텍스트, V : 동영상
			if (ArrayUtils.contains(answerNumberList, String.valueOf(answerContentIdx))) { //답가지 정답 여부
				answerFlag = "Y"; //정답
			}
			quizAnswerParam.put("content", answerViewText);
			quizAnswerParam.put("answer_flag", answerFlag);
			quizAnswerParam.put("answer_order", answerContentIdx);
			quizAnswerParam.put("attach_type", attachType);
			if (pfLessonDao.insertQuizAnswer(quizAnswerParam) < 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ quiz_answer insert fail ]", "004");
			}
		}
		
		//QUIZ_ATTACH 자료제시 INSERT
		HashMap<String, Object> quizAttachParam = new HashMap<String, Object>();
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		MultipartFile quizAttachFile = mRequest.getFile("quizAttachFile");
		if (quizAttachFile != null && !quizAttachFile.isEmpty()) {
			String uploadPath = ROOT_PATH + RES_PATH + QUIZ_PATH + "/" + quizSeq;
			String realName = FileUploader.uploadFile(uploadPath, quizAttachFile);
			String attachType = "I"; //I : 이미지 , V : 동영상, A : 오디오
			if (realName == null || realName.equals("not")) {
				Util.deleteFolder(new File(uploadPath));
				throw new LogicException("FILE_UPLOAD_FAIL", "003");
			}
			quizAttachParam.put("quiz_seq", quizSeq);
			quizAttachParam.put("file_path", QUIZ_PATH + "/" + quizSeq + "/" + realName);
			quizAttachParam.put("file_name", quizAttachFile.getOriginalFilename());
			quizAttachParam.put("attach_type", attachType);
			quizAttachParam.put("s_user_seq", param.get("s_user_seq"));
			if (pfLessonDao.insertQuizAttach(quizAttachParam) < 1) {
				Util.deleteFolder(new File(uploadPath));
				throw new RuntimeLogicException("QUERY_FAIL [ quiz_attach insert fail ]", "004");
			}
		}
		
		return resultMap;		
	}
	
	
	@Transactional
	public ResultMap quizOrderModify(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		String orderUpFlag = String.valueOf(param.get("order_up_flag")); // "true", "false"
		
		HashMap<String, Object> preOrNextQuizSeqParam = pfLessonDao.getPreOrNextQuizSeq(param);
		if (preOrNextQuizSeqParam == null || preOrNextQuizSeqParam.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_QUIZ_INFO", "007");
		}
		
		HashMap<String, Object> quizParam = new HashMap<String, Object>();
		quizParam.put("s_user_seq", param.get("s_user_seq"));
		if (orderUpFlag.equals("true")) { //순서 위로
			//위에 퀴즈 order ++
			quizParam.put("quiz_seq", preOrNextQuizSeqParam.get("quiz_seq"));
			quizParam.put("order_update_flag", "plus");
			if (pfLessonDao.updateQuizOrder(quizParam) < 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ quiz order up update - 1 fail ]", "004");
			}
				
			//현재 퀴즈 order --
			quizParam.put("quiz_seq", param.get("quiz_seq"));
			quizParam.put("order_update_flag", "minus");
			if (pfLessonDao.updateQuizOrder(quizParam) < 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ quiz order up update - 2 fail ]", "004");
			}
		} else { //순서 아래로
			//아래 퀴즈 order --
			quizParam.put("quiz_seq", preOrNextQuizSeqParam.get("quiz_seq"));
			quizParam.put("order_update_flag", "minus");
			if (pfLessonDao.updateQuizOrder(quizParam) < 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ quiz order down update - 1 fail ]", "004");
			}
			//현재 퀴즈 order ++
			quizParam.put("quiz_seq", param.get("quiz_seq"));
			quizParam.put("order_update_flag", "plus");
			if (pfLessonDao.updateQuizOrder(quizParam) < 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ quiz order down update - 2 fail ]", "004");
			}
		}
		
		List<HashMap<String, Object>> quizList = new ArrayList<HashMap<String, Object>>();
		quizList = pfLessonDao.getQuizList(param);
		int orderCnt = 1;
		for (HashMap<String, Object> tempQuiz : quizList) {
			tempQuiz.put("quiz_order", orderCnt++);
			if (pfLessonDao.updateQuizOrderSort(tempQuiz) < 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ quiz order sort update fail ]", "004");
			}
		}
		
		return resultMap;
	}
	
	@Transactional
	public ResultMap quizRemove(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		String[] quizSeqs = ((String)param.get("quiz_seq_str")).split(",");
		HashMap<String, Object> removeParam = new HashMap<String, Object>();
		removeParam.put("fe_seq", param.get("fe_seq"));
		removeParam.put("s_user_seq", param.get("s_user_seq"));
		for (String quizSeq : quizSeqs) {
			removeParam.put("quiz_seq", quizSeq);
			if (pfLessonDao.deleteQuiz(removeParam) < 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ quiz delete fail ]", "004");
			}
			
			pfLessonDao.deleteQuizAttach(removeParam);
			
			if (pfLessonDao.deleteQuizAnswer(removeParam) < 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ quiz_answer delete fail ]", "004");
			}
		}
		
		List<HashMap<String, Object>> quizList = new ArrayList<HashMap<String, Object>>();
		quizList = pfLessonDao.getQuizList(param);
		int orderCnt = 1;
		for (HashMap<String, Object> tempQuiz : quizList) {
			tempQuiz.put("quiz_order", orderCnt++);
			if (pfLessonDao.updateQuizOrderSort(tempQuiz) < 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ quiz order sort update fail ]", "004");
			}
		}
		
		return resultMap;
	}
	
	
	public ResultMap quizPreview(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		HashMap<String, Object> quizPreviewInfo = new HashMap<String, Object>();
		quizPreviewInfo = pfLessonDao.getQuizPreviewInfo(param);
		if (quizPreviewInfo == null || quizPreviewInfo.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_QUIZ_PREVIEW_INFO", "007");
		}
		resultMap.put("quizPreviewInfo", quizPreviewInfo);
		return resultMap;
	}
	
	public ResultMap getQuizDetail(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		HashMap<String, Object> quizInfo = new HashMap<String, Object>();
		quizInfo = pfLessonDao.getQuizInfo(param);
		if (quizInfo == null || quizInfo.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_QUIZ_INFO", "007");
		}
		HashMap<String, Object> titleInfo = new HashMap<String, Object>();
		titleInfo = pfLessonDao.getFormationEvaluationTitle(param);
		if (titleInfo == null || titleInfo.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_FORMATION_EVAUATION_TITLE_INFO", "007");
		}
		
		quizInfo.put("lesson_subject", titleInfo.get("lesson_subject"));
		quizInfo.put("lesson_date", titleInfo.get("lesson_date"));
		quizInfo.put("fe_name", titleInfo.get("fe_name"));
		
		param.put("quiz_seq", quizInfo.get("quiz_seq"));
		
		HashMap<String, Object> quizAttachInfo = new HashMap<String, Object>();
		quizAttachInfo = pfLessonDao.getQuizAttachInfo(param);
		
		List<HashMap<String, Object>> quizAnswerList = new ArrayList<HashMap<String, Object>>();
		quizAnswerList = pfLessonDao.getQuizAnswerList(param);
		
		resultMap.put("quizInfo", quizInfo);
		resultMap.put("quizAttachInfo", quizAttachInfo);
		resultMap.put("quizAnswerList", quizAnswerList);
		
		return resultMap;
	}
	
	public ResultMap thumbnailUpload(HashMap<String, Object> param, HttpServletRequest request) throws Exception{
		ResultMap resultMap = new ResultMap();
		
		String feSeq = String.valueOf(param.get("fe_seq"));
		//QUIZ_ATTACH 자료제시 INSERT
		HashMap<String, Object> formationEvaluationParam = new HashMap<String, Object>();
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		MultipartFile uploadFile = mRequest.getFile("uploadFile");
		if (uploadFile != null && !uploadFile.isEmpty()) {
			String uploadPath = ROOT_PATH + RES_PATH + FORMATION_EVALUAION_PATH + "/" + feSeq;
			String realName = FileUploader.uploadFile(uploadPath, uploadFile);
			if (realName == null || realName.equals("not")) {
				Util.deleteFolder(new File(uploadPath));
				throw new LogicException("FILE_UPLOAD_FAIL", "003");
			}
			formationEvaluationParam.put("fe_seq", feSeq);
			formationEvaluationParam.put("thumbnail_path", FORMATION_EVALUAION_PATH + "/" + feSeq + "/" + realName);
			formationEvaluationParam.put("thumbnail_name", uploadFile.getOriginalFilename());
			formationEvaluationParam.put("s_user_seq", param.get("s_user_seq"));
			if (pfLessonDao.updateFormationEvaluationThumnail(formationEvaluationParam) < 1) {
				Util.deleteFolder(new File(uploadPath));
				throw new RuntimeLogicException("QUERY_FAIL [ quiz_attach insert fail ]", "004");
			}
		}
		
		return resultMap;
	}

	@Transactional
	public ResultMap quizModify(HashMap<String, Object> param, HttpServletRequest request) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		int quizSeq = 0;
		int answerViewCnt = Integer.parseInt(String.valueOf(param.get("answerViewCnt"))); //답가지 개수
		String [] answerNumberList = String.valueOf(param.get("answerText")).split(", "); //정답 배열
		
		//QUIZ UPDATE
		HashMap<String, Object> quizParam = new HashMap<String, Object>();
		quizParam.put("quiz_seq", param.get("quizSeq"));
		quizParam.put("fe_seq", param.get("feSeq"));
		quizParam.put("quiz_question", param.get("quizQuestion"));
		quizParam.put("s_user_seq", param.get("s_user_seq"));
		pfLessonDao.updateQuiz(quizParam);
		
		quizSeq = Integer.parseInt(String.valueOf(param.get("quizSeq")));
		
		//QUIZ_ANSWER DELETE
		if (pfLessonDao.deleteQuizAnswer(quizParam) < 1) {
			throw new RuntimeLogicException("QUERY_FAIL [ quiz_answer delete fail ]", "004");
		}
		
		
		//QUIZ_ANSWER INSERT
		HashMap<String, Object> quizAnswerParam = new HashMap<String, Object>();
		quizAnswerParam.put("quiz_seq", quizSeq);
		quizAnswerParam.put("s_user_seq", param.get("s_user_seq"));
		
		for (int answerContentIdx = 1; answerContentIdx <= answerViewCnt; answerContentIdx++) { //답가지 텍스트 입력 확인
			String answerViewText = String.valueOf(param.get("answerContent" + answerContentIdx));
			String answerFlag = "N";
			String attachType = "T"; // I : 이미지, T : 텍스트, V : 동영상
			if (ArrayUtils.contains(answerNumberList, String.valueOf(answerContentIdx))) { //답가지 정답 여부
				answerFlag = "Y"; //정답
			}
			quizAnswerParam.put("content", answerViewText);
			quizAnswerParam.put("answer_flag", answerFlag);
			quizAnswerParam.put("answer_order", answerContentIdx);
			quizAnswerParam.put("attach_type", attachType);
			if (pfLessonDao.insertQuizAnswer(quizAnswerParam) < 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ quiz_answer insert fail ]", "004");
			}
		}
		
		
		HashMap<String, Object> quizAttachParam = new HashMap<String, Object>();
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		MultipartFile quizAttachFile = mRequest.getFile("quizAttachFile");
		
		String quizAttachFileSeq = String.valueOf(param.get("quizAttachFileSeq"));
		String quizAttachFileDelYn = String.valueOf(param.get("quizAttachFileDelYn"));
		
		boolean preDataRemoveFlag = false;
		boolean currDataInsertFlag = false;
		
		if (!quizAttachFileSeq.equals("") && quizAttachFileDelYn.equals("Y") && quizAttachFile != null && !quizAttachFile.isEmpty()) {
			//이전 퀴즈자료 있고, 새 퀴즈자료 있음 -> 이전 퀴즈자료 N UPDATE, 새 퀴즈 자료 INSERT
			preDataRemoveFlag = true;
			currDataInsertFlag = true;
		} else if (!quizAttachFileSeq.equals("") && quizAttachFileDelYn.equals("Y") && (quizAttachFile == null || quizAttachFile.isEmpty())) {
			//이전 퀴즈자료 있고, 새 퀴즈자료 없음 -> 이전 퀴즈자료 N UPDATE
			preDataRemoveFlag = true;
		} else if (quizAttachFileSeq.equals("") && quizAttachFile != null && !quizAttachFile.isEmpty()) {
			//이전 퀴즈자료 없고, 새 퀴즈자료 있음 -> 새 퀴즈자료 INSERT
			currDataInsertFlag = true;
		} 
		
		//QUIZ_ATTACH 자료제시 DELETE
		if (preDataRemoveFlag) {
			quizAttachParam.put("quiz_attach_seq", quizAttachFileSeq);
			quizAttachParam.put("s_user_seq", param.get("s_user_seq"));
			if (pfLessonDao.deleteQuizAttachSeq(quizAttachParam) < 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ quiz_attach delete fail ]", "004");
			}
			quizAttachParam.remove("quiz_attach_seq");
		}
		
		//QUIZ_ATTACH 자료제시 INSERT
		if (currDataInsertFlag) {
			if (quizAttachFile != null && !quizAttachFile.isEmpty()) {
				String uploadPath = ROOT_PATH + RES_PATH + QUIZ_PATH + "/" + quizSeq;
				String realName = FileUploader.uploadFile(uploadPath, quizAttachFile);
				String attachType = "I"; //I : 이미지 , V : 동영상, A : 오디오
				if (realName == null || realName.equals("not")) {
					throw new LogicException("FILE_UPLOAD_FAIL", "003");
				}
				quizAttachParam.put("quiz_seq", quizSeq);
				quizAttachParam.put("file_path", QUIZ_PATH + "/" + quizSeq + "/" + realName);
				quizAttachParam.put("file_name", quizAttachFile.getOriginalFilename());
				quizAttachParam.put("attach_type", attachType);
				quizAttachParam.put("s_user_seq", param.get("s_user_seq"));
				if (pfLessonDao.insertQuizAttach(quizAttachParam) < 1) {
					throw new RuntimeLogicException("QUERY_FAIL [ quiz_attach insert fail ]", "004");
				}
			}
		}
		
		return resultMap;		
	}
	
	
	@Transactional
	public ResultMap formationEvaluationRemove(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		if (pfLessonDao.deleteFormationEvaluation(param) > 0) {
			pfLessonDao.deleteQuizAnswerOfFeSeq(param);
			pfLessonDao.deleteQuizAttachOfFeSeq(param);
			pfLessonDao.deleteQuizOfFeSeq(param);
		} else {
			throw new RuntimeLogicException("QUERY_FAIL [ formation evaluation delete fail ]", "004");
		}
		
		return resultMap;
	}
	
	
	public ResultMap getPastFormationEvaluationList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		//tahun가져오기
		List<HashMap<String, Object>> yearAcademicList = new ArrayList<HashMap<String, Object>>();
		yearAcademicList = pfLessonDao.getYearAcademicList(param);
		//지난 수업 퀴즈 가져오기
		List<HashMap<String, Object>> pastFormationEvaluationList = new ArrayList<HashMap<String, Object>>();
		pastFormationEvaluationList = pfLessonDao.getPastFormationEvaluationList(param);
		resultMap.put("yearAcademicList", yearAcademicList);
		resultMap.put("pastList", pastFormationEvaluationList);
		return resultMap;
	}
	
	@Transactional
	public ResultMap pastQuizCopy(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		String currFeSeq = String.valueOf(param.get("curr_fe_seq"));
		String pastFeSeq = String.valueOf(param.get("fe_seq"));
		
		HashMap<String, Object> formationEvaluation = new HashMap<String, Object>();
		param.put("fe_seq", pastFeSeq);
		formationEvaluation = pfLessonDao.getFormationEvaluation(param);
		if (formationEvaluation != null && !formationEvaluation.isEmpty()) {
			//현재 FESEQ로 퀴즈, 퀴즈 답가지, QUIZ ATTACH 삭제
			param.put("fe_seq", currFeSeq);
			pfLessonDao.deleteQuizAnswerOfFeSeq(param);
			pfLessonDao.deleteQuizAttachOfFeSeq(param);
			pfLessonDao.deleteQuizOfFeSeq(param);
			
			String thumbnailPath = String.valueOf(formationEvaluation.get("thumbnail_path"));
			if (thumbnailPath != null && !thumbnailPath.equals("")) {
				//퀴즈 썸네일 파일복사
				String currFilePathDir = FORMATION_EVALUAION_PATH + "/" + currFeSeq;
				String realName = Util.fileCopyTypeDir(ROOT_PATH + thumbnailPath, ROOT_PATH + currFilePathDir);
				if (realName == null) {
					throw new RuntimeLogicException("FILE_UPLOAD_FAIL [ quiz attach file copy fail ]", "003");
				}
				thumbnailPath = currFilePathDir + "/" + realName;
				formationEvaluation.put("thumbnail_path", thumbnailPath);
			}
			formationEvaluation.put("fe_seq", currFeSeq);
			formationEvaluation.put("s_user_seq", param.get("s_user_seq"));
			//FORMATION EVALUATION UPDATE
			if (pfLessonDao.updateFormationEvaluation(formationEvaluation) < 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ formation evaluation update fail ]", "004");
			}
			
			//지난 퀴즈 리스트 불러오기
			List<HashMap<String, Object>> pastQuizList = new ArrayList<HashMap<String, Object>>();
			param.put("fe_seq", pastFeSeq);
			pastQuizList = pfLessonDao.getPastQuizList(param);
			if (pastQuizList != null && !pastQuizList.isEmpty()) {
				int quizOrder = 1;
				for (HashMap<String, Object> pastQuiz : pastQuizList) {
					String pastQuizSeq = String.valueOf(pastQuiz.get("quiz_seq"));
					String currQuizSeq = "";

					//QUIZ INSERT
					pastQuiz.put("quiz_order", quizOrder++);
					pastQuiz.put("s_user_seq", param.get("s_user_seq"));
					pastQuiz.put("fe_seq", currFeSeq);
					if (pfLessonDao.insertQuiz(pastQuiz) < 1) {
						throw new RuntimeLogicException("QUERY_FAIL [ quiz insert fail ]", "004");
					}
					
					currQuizSeq = String.valueOf(pastQuiz.get("quiz_seq"));
					pastQuiz.put("quiz_seq", pastQuizSeq);
					int answerOrder = 1;
					List<HashMap<String, Object>> pastQuizAnswerList = new ArrayList<HashMap<String, Object>>();
					pastQuizAnswerList = pfLessonDao.getPastQuizAnswerList(pastQuiz);
					for (HashMap<String, Object> pastQuizAnswer : pastQuizAnswerList) {
						pastQuizAnswer.put("quiz_seq", currQuizSeq);
						pastQuizAnswer.put("s_user_seq", param.get("s_user_seq"));
						pastQuizAnswer.put("answer_order", answerOrder++);
						//QUIZ ANSWER INSERT
						if (pfLessonDao.insertQuizAnswer(pastQuizAnswer) < 1) {
							throw new RuntimeLogicException("QUERY_FAIL [ quiz answer insert fail ]", "004");
						}
					}
					
					List<HashMap<String, Object>> pastQuizAttachList = new ArrayList<HashMap<String, Object>>();
					pastQuizAttachList = pfLessonDao.getPastQuizAttachList(pastQuiz);
					for (HashMap<String, Object> pastQuizAttach : pastQuizAttachList) {
						HashMap<String, Object> quizAttach = new HashMap<String, Object>();
						String pastFilePath = String.valueOf(pastQuizAttach.get("file_path"));
						String pastFileName = String.valueOf(pastQuizAttach.get("file_name"));
						String pastFileAttachType = String.valueOf(pastQuizAttach.get("attach_type"));
						String currFileDirPath = QUIZ_PATH + "/" + currQuizSeq;
						String realName = Util.fileCopyTypeDir(ROOT_PATH + pastFilePath, ROOT_PATH + currFileDirPath);
						if (realName == null) {
							throw new RuntimeLogicException("FILE_UPLOAD_FAIL [ quiz attach file copy fail ]", "003");
						}
						quizAttach.put("file_path", currFileDirPath+"/"+realName);
						quizAttach.put("file_name", pastFileName);
						quizAttach.put("attach_type", pastFileAttachType);
						quizAttach.put("quiz_seq", currQuizSeq);
						quizAttach.put("s_user_seq", param.get("s_user_seq"));
						//QUIZ ATTACH INSERT
						if (pfLessonDao.insertQuizAttach(quizAttach) < 1) {
							throw new RuntimeLogicException("QUERY_FAIL [ quiz attach insert fail ]", "004");
						}
					}
				}
			}
		} else {
			throw new ResourceNotFoundException("NOT_FOUND_FORMATION_EVALUATION", "007");
		}
		
		return resultMap;
	}
	
	@SuppressWarnings({ "unchecked", "resource" })
	@Transactional
	public ResultMap quizExcelCreate (HashMap<String, Object> param, HttpServletRequest request) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		HashMap<String, Object> xlsQuizMap = new HashMap<String, Object>();
		HashMap<String, Object> xlsQuizAnswerMap = new HashMap<String, Object>();
		List<HashMap<String, Object>> xlsQuizList = new ArrayList<HashMap<String, Object>>();
		List<HashMap<String, Object>> xlsQuizAnswerList = new ArrayList<HashMap<String, Object>>();
		List<HashMap<String, Object>> unZipFileList = new ArrayList<HashMap<String, Object>>();
		int quizOrderNumber = 1;
		int quizAnswerOrderNumber = 1;
		String feSeq = String.valueOf(param.get("fe_seq"));
		int quizSeq = 0;
		String unZipDirPath = ROOT_PATH + RES_PATH + QUIZ_TEMP_PATH + "/" + feSeq;
		
		
		//registrasi 되어 있는 퀴즈 모두 삭제
		HashMap<String, Object> removeParam = new HashMap<String, Object>();
		removeParam.put("fe_seq", feSeq);
		removeParam.put("s_user_seq", param.get("s_user_seq"));
		pfLessonDao.deleteQuizAnswerOfFeSeq(removeParam);
		pfLessonDao.deleteQuizAttachOfFeSeq(removeParam);
		pfLessonDao.deleteQuizOfFeSeq(removeParam);
		
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		
		//unzip
		MultipartFile dataZipFile = mRequest.getFile("dataZipFile");
		if (dataZipFile != null && !dataZipFile.isEmpty()) {
			FileInputStream fis = (FileInputStream) dataZipFile.getInputStream();
			ZipUtil.unzip(fis, new File(unZipDirPath));
			unZipFileList = Util.getFileNameList(unZipDirPath);
			for (HashMap<String, Object> fileMap : unZipFileList) {
				String fileName = String.valueOf(fileMap.get("file_name")).toUpperCase();
				if (!fileName.endsWith("JPG") && !fileName.endsWith("PNG")) {
					Util.deleteFolder(new File(unZipDirPath));
					throw new RuntimeLogicException("VALIDITY_CHECK_FAIL [ file name only png, jpg ]", "002");
				}
			}
		}
		//EXCEL READ
		MultipartFile excelFile = mRequest.getFile("excelFile");
		if (excelFile != null && !excelFile.isEmpty()) {
			
			Workbook workbook = null;
			
			String fileName = excelFile.getOriginalFilename();
			if (fileName.endsWith("xlsx")) {
		        workbook = new XSSFWorkbook(excelFile.getInputStream());
		    } else if (fileName.endsWith("xls")) {
		        workbook = new HSSFWorkbook(excelFile.getInputStream());
		    } else {
		        throw new RuntimeLogicException("VALIDITY_CHECK_FAIL [ The specified file is not Excel file ]", "002");
		    }
			
			Sheet sheet = workbook.getSheetAt(1); //0 : 1번시트, 1 : 2번시트
			
			int totalRow = sheet.getPhysicalNumberOfRows(); //총 row 수
			
			for (int rowNum = 0; rowNum < totalRow; rowNum++) {
				
				Row currRow = sheet.getRow(rowNum);
				
				String cell_value_0 = getCellValue(currRow, 0); // A
				String cell_value_1 = getCellValue(currRow, 1); // B
				String cell_value_5 = getCellValue(currRow, 5); // F

				if (cell_value_0.equals("No")) {// 문항 시작
					xlsQuizMap = new HashMap<String, Object>();
					xlsQuizMap.put("quiz_order", quizOrderNumber++); // 문항 번호
				}
				
				if (cell_value_0.equals("문항줄기")) {
					xlsQuizMap.put("quiz_question", cell_value_1); // 문항 줄기
				}
						
				if (cell_value_0.equals("형성penilaian자료")) {
					String attachFileName = "";
					if (!cell_value_1.equals("")) {
						attachFileName = cell_value_1.trim();
						String temp = attachFileName.toUpperCase(); 
						if (!temp.endsWith("JPG") && !temp.endsWith("PNG")) {
							Util.deleteFolder(new File(unZipDirPath));
							throw new RuntimeLogicException("VALIDITY_CHECK_FAIL [ file name only png, jpg ]", "002");
						}
					}
					xlsQuizMap.put("attach_file_name", attachFileName);
				}
				
				if (cell_value_0.equals("답가지")) {
					xlsQuizAnswerList = new ArrayList<HashMap<String, Object>>();
					quizAnswerOrderNumber = 1;
				}
				
				boolean isAnswerNumber = false;
				try {
					if (Integer.parseInt(cell_value_0) > 0) {
						isAnswerNumber = true;
					}
				} catch (NumberFormatException e) {
					isAnswerNumber = false;
				}
				
				if (isAnswerNumber) {
					
					xlsQuizAnswerMap.put("content", cell_value_1); // 답가지 isi
					xlsQuizAnswerMap.put("answer_order", quizAnswerOrderNumber++); // 답가지 순서
					
					cell_value_5 = cell_value_5.toUpperCase().trim();
					
					if (!cell_value_5.equals("Y")) {
						cell_value_5 = "N";
					}
					
					xlsQuizAnswerMap.put("answer_flag", cell_value_5); // 답가지 여부 YN
					xlsQuizAnswerList.add((HashMap<String, Object>)xlsQuizAnswerMap.clone());
				}
				
				if (cell_value_0.equals("-")) { // 문항 종료
					xlsQuizMap.put("quiz_answer_list", xlsQuizAnswerList);
					xlsQuizList.add(xlsQuizMap);
				}
			}
		}
		
		for (HashMap<String, Object> quizMap : xlsQuizList) {
			//QUIZ INSERT
			HashMap<String, Object> quizParam = new HashMap<String, Object>();
			quizParam.put("fe_seq", feSeq);
			quizParam.put("quiz_question", quizMap.get("quiz_question"));
			quizParam.put("quiz_order", quizMap.get("quiz_order"));
			quizParam.put("s_user_seq", param.get("s_user_seq"));
			if (pfLessonDao.insertQuiz(quizParam) < 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ quiz insert fail ]", "004");
			}
			quizSeq = (int)quizParam.get("quiz_seq");
			
			//QUIZ_ANSWER INSERT
			for (HashMap<String, Object> quizAnswerParam : (ArrayList<HashMap<String, Object>>) quizMap.get("quiz_answer_list")) {
				quizAnswerParam.put("quiz_seq", quizSeq);
				quizAnswerParam.put("s_user_seq", param.get("s_user_seq"));
				String attachType = "T"; // I : 이미지, T : 텍스트, V : 동영상
				quizAnswerParam.put("attach_type", attachType);
				if (pfLessonDao.insertQuizAnswer(quizAnswerParam) < 1) {
					throw new RuntimeLogicException("QUERY_FAIL [ quiz_answer insert fail ]", "004");
				}
			}
			
			//QUIZ_ATTACH 자료제시 INSERT
			String attachFileName = String.valueOf(quizMap.get("attach_file_name"));
			if (!attachFileName.equals("")) {
				for (HashMap<String, Object> quizAttachParam : unZipFileList) {
					if (quizAttachParam.containsValue(attachFileName)) {
						String outFilePathDir = QUIZ_PATH + "/" + quizSeq;
						String orioriginalFileName = String.valueOf(quizAttachParam.get("file_name"));
						String inFilePath = unZipDirPath + "/" + orioriginalFileName;
						String realName = Util.fileCopyTypeDir(inFilePath, ROOT_PATH + RES_PATH + outFilePathDir);
						String attachType = "I"; //I : 이미지 , V : 동영상, A : 오디오
						if (realName == null) {
							throw new RuntimeLogicException("FILE_UPLOAD_FAIL [ quiz attach file copy fail ]", "003");
						}
						String uploadFilePath = outFilePathDir + "/" + realName;
						quizAttachParam.put("quiz_seq", quizSeq);
						quizAttachParam.put("file_path", uploadFilePath);
						quizAttachParam.put("file_name", orioriginalFileName);
						quizAttachParam.put("attach_type", attachType);
						quizAttachParam.put("s_user_seq", param.get("s_user_seq"));
						if (pfLessonDao.insertQuizAttach(quizAttachParam) < 1) {
							Util.deleteFolder(new File(unZipDirPath));
							throw new RuntimeLogicException("QUERY_FAIL [ quiz_attach insert fail ]", "004");
						}
					}
				}
			}
		}
		//압축해제 디렉터리 삭제
		Util.deleteFolder(new File(unZipDirPath));
		
		return resultMap;
	}
	
	
	public ResultMap getFormationEvaluationResult(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		HashMap<String, Object> formationEvaluationInfo = new HashMap<String, Object>();
		formationEvaluationInfo = pfLessonDao.getFormationEvaluationInfo(param);
		if (formationEvaluationInfo == null || formationEvaluationInfo.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_FORMATION_EVALUATION_INFO", "007");
		}
		
		List<HashMap<String, Object>> resultList = new ArrayList<HashMap<String, Object>>();
		resultList = pfLessonDao.getFormationEvaluationResultExcelList(param);
		
		double scoreMax = 0;
		double scoreMin = 0;
		double scoreAvg = 0;
		double scoreTotalSum = 0;
		String scoreAvgStr = "";
		
		for (HashMap<String, Object> result : resultList) {
			double score = Double.valueOf(String.valueOf(result.get("fe_score")));
			String absenceFlag = String.valueOf(result.get("absence_flag"));
			if (absenceFlag.equals("N")) {
				if (scoreMin == 0) { //min pengaturan ulang
					scoreMin = score;
				}
				
				if (scoreMax < score) { //max
					scoreMax = score;
				}
				
				if (scoreMin > score) { //min
					scoreMin = score;
				}
				
				scoreTotalSum = scoreTotalSum + score; //avg
			}
		}
		
		if (resultList.size() > 0) {
			scoreAvg = (scoreTotalSum/resultList.size());
			DecimalFormat format = new DecimalFormat("#.#");
			scoreAvgStr = format.format(scoreAvg);
		}
		
		String fileName = String.valueOf(formationEvaluationInfo.get("fe_name"));
		fileName = Util.getCurrentDate() + "_" + fileName; 
		
		formationEvaluationInfo.put("score_max", scoreMax);
		formationEvaluationInfo.put("score_min", scoreMin);
		formationEvaluationInfo.put("score_avg", scoreAvgStr);
		formationEvaluationInfo.put("result_list", resultList);
		
		resultMap.put("formation_evaluation", formationEvaluationInfo);
		resultMap.put("result_list", resultList);
		resultMap.put("file_name", fileName);
		
		return resultMap;
	}
	
	public String getCellValue(Row currRow, int cellNum) {
		String cellValue = "";
		Cell currCell = currRow.getCell(cellNum);
		if (currCell != null) {
			switch (currCell.getCellTypeEnum()) {
				case NUMERIC:								
					cellValue = String.valueOf((int)currCell.getNumericCellValue());
					break;
				case STRING:
					cellValue = String.valueOf(currCell.getStringCellValue());
					break;
				case BLANK:
					break;
				case ERROR:
					cellValue = String.valueOf(currCell.getErrorCellValue()); // byte
					break;
				default:
			}
		}
		
		return cellValue;
	}
	
	public ResultMap getFormationEvaluationResultList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		//형성penilaian 정보 resultInfo
		HashMap<String, Object> formationEvaluationInfo = new HashMap<String, Object>();
		formationEvaluationInfo = pfLessonDao.getFormationEvaluationResultInfo(param);
		
		//교수 정보
		HashMap<String, Object> professorInfo = new HashMap<String, Object>();
		professorInfo = pfLessonDao.getProfessorInfo(param);
		
		//결과 목록
		List<HashMap<String, Object>> resultList = new ArrayList<HashMap<String, Object>>();
		resultList = pfLessonDao.getFormationEvaluationResultList(param);
		
		
		resultMap.put("fe_info", formationEvaluationInfo);
		resultMap.put("professor_info", professorInfo);
		resultMap.put("result_list", resultList);
		return resultMap;
	}
	
	
	@Transactional
	public ResultMap assignMentInsert(MultipartHttpServletRequest request, HashMap<String, Object> param, String[] asgmt_attach_seq) throws Exception{
		ResultMap resultMap = new ResultMap();
		List<MultipartFile> fileList = request.getFiles("file");

		Object session_lp_seq = request.getSession().getAttribute("S_LP_SEQ"); 
		String curr_seq = request.getSession().getAttribute("S_CURRICULUM_SEQ").toString(); //교육과정 시퀀스
		String lp_seq = "";
		String year = request.getSession().getAttribute("S_CURRENT_ACADEMIC_YEAR").toString(); //tahun

		if(session_lp_seq != null)
			lp_seq = request.getSession().getAttribute("S_LP_SEQ").toString();
		
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));		
		param.put("curr_seq", curr_seq);
		param.put("lp_seq", lp_seq);
		if(param.get("asgmt_seq").toString().equals("")){
			if (pfLessonDao.insertAssignMent(param) == 0) {
				throw new RuntimeLogicException("QUERY_FAIL [ pf - assignment insert query fail ]", "004");
			}
		}
		else{
			//simpan한 것들 N으로 변경
			pfLessonDao.updateAssignMentUseFlag(param);
			if (pfLessonDao.updateAssignMent(param) == 0) {
				throw new RuntimeLogicException("QUERY_FAIL [ pf - assignment update query fail ]", "004");
			}
		}
		if(asgmt_attach_seq != null){
			for(int i=0;i<asgmt_attach_seq.length;i++){
				param.put("asgmt_attach_seq", asgmt_attach_seq[i]);
				if (pfLessonDao.updateAssignMentAttachUseFlagY(param) == 0)
					throw new RuntimeLogicException("QUERY_FAIL [ pf - assignment_attach update query fail ]", "004");
			}
		}
		
		String defaultPath = ROOT_PATH + RES_PATH + "/upload/"+year+"/"+curr_seq+"/"+lp_seq+"/assignMent/"+param.get("asgmt_seq").toString()+"/";
		String dbInsertPath = "/upload/"+year+"/"+curr_seq+"/"+lp_seq+"/assignMent/"+param.get("asgmt_seq").toString()+"/";
		//파일 simpan한다.
		for(int i=0; i<fileList.size();i++){
			if(fileList.get(i) != null){
				String filename = FileUploader.uploadFile(defaultPath, fileList.get(i));
				param.put("file_name", fileList.get(i).getOriginalFilename());
				param.put("file_path", dbInsertPath+filename);
				if(pfLessonDao.insertAssignmentAttach(param) == 0)
					throw new RuntimeLogicException("QUERY_FAIL [ pf - assignment_attach file insert query fail ]", "004");
			}
		}
		
		resultMap.put("asgmt_seq", param.get("asgmt_seq").toString());
		return resultMap;
	}



	public ResultMap getAssignMentOne(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("assignMent", pfLessonDao.getAssignMent(param));
		resultMap.put("assignMentAttachList", pfLessonDao.getAssignMentAttach(param));
		
		return resultMap;
	}



	public ResultMap getAssignMentList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("currInfo", pfLessonDao.getCurriculumBasicInfo(param));
		param.put("pf_level", pfLessonDao.getPfLevelCheck(param));
		resultMap.put("assignMentList", pfLessonDao.getAssignMentList(param));
		return resultMap;
	}



	public int getAssignMentCount(HashMap<String, Object> param) throws Exception {
		// TODO Auto-generated method stub
		return pfLessonDao.getAssignMentCount(param);
	}



	public ResultMap getAssignMentSubmitList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("assignMent", pfLessonDao.getAssignMent(param));
		if(param.get("asgmt_type").toString().equals("2"))
			resultMap.put("assignMentSubmitList", pfLessonDao.getAssignMentSubmitList(param));
		else
			resultMap.put("assignMentSubmitList", pfLessonDao.getCurrAssignMentSubmitList(param));
		return resultMap;
	}


	@Transactional
	public ResultMap assignMentFeedBackInsert(HttpServletRequest request, HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();

		String[] asgmt_submit_seq = request.getParameterValues("asgmt_submit_seq");
		String[] feedback = request.getParameterValues("feedback");
		String[] score = request.getParameterValues("score");
		
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		
		if(asgmt_submit_seq != null){
			for(int i=0;i<asgmt_submit_seq.length;i++){
				if(asgmt_submit_seq[i].equals("") || asgmt_submit_seq[i].isEmpty())
					continue;
				param.put("asgmt_submit_seq", asgmt_submit_seq[i]);
				param.put("content", feedback[i]);
				param.put("score", score[i]);
				
				if(pfLessonDao.insertAssignMentFeedBack(param) == 0)
					throw new RuntimeLogicException("QUERY_FAIL [ pf - assignment_feedback insert query fail ]", "004");
			}
		}
		
		return resultMap;
	}

	public ResultMap getAssignMentDetail(HashMap<String, Object> param) throws Exception{
		ResultMap resultMap = new ResultMap();
		resultMap.put("asgmt", pfLessonDao.getAssignMentDetail(param));
		resultMap.put("asgmtAttach", pfLessonDao.getAssignMentAttach(param));
		return resultMap;
	}



	public ResultMap attendanceList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("attendanceList", pfLessonDao.getAttendanceList(param));
		resultMap.put("attendanceCount", pfLessonDao.gatAttendanceCount(param));
		return resultMap;
	}



	public ResultMap assignMentAllFileDown(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		long milliSecond = System.currentTimeMillis();
		String milName = String.valueOf(milliSecond).substring(10, 13);
		
    	String TimeName = (sdf.format(new java.util.Date()) + milName);
    	
		File fileFolder = new File(ROOT_PATH + RES_PATH + "/upload/assignMent/"+TimeName+"_"+param.get("asgmt_seq").toString());		
		
		String zipName = "AllFile.zip";

    			
		//폴더 없으면 생성함
		if(!fileFolder.exists()){
			fileFolder.mkdirs();			
		}

        List<HashMap<String, Object>> makeFileList = new ArrayList<HashMap<String, Object>>();
		//파일명 리스트 가져옴
		List<HashMap<String, Object>> filelist = pfLessonDao.getAssignMentFileList(param);
		
		int chk = 0;
		//파일이 있는지 체크
		for(int index=0; index < filelist.size(); index++){			
			File file = new File(ROOT_PATH + "/"+filelist.get(index).get("file_path").toString()+filelist.get(index).get("file_name").toString());			
			if(file.exists()){
				makeFileList.add(chk, filelist.get(index));
    			chk++;
			}
		}
		        
		FileInputStream input = null;
		FileOutputStream output = null;
        //파일 복사
        for (int i=0; i<makeFileList.size(); i++) {
        	
			HashMap<String, Object> file_Info = makeFileList.get(i);
        	if(!file_Info.get("file_name").toString().equals("")){
		        try {
		        	File outFolder = new File(fileFolder.getPath() + "/" + file_Info.get("id").toString());
		        	
		        	if(!outFolder.exists())
		        		outFolder.mkdirs();
		        			        	
		        	input = new FileInputStream(ROOT_PATH + filelist.get(i).get("file_path").toString());
		        	output = new FileOutputStream(outFolder + "/" + filelist.get(i).get("file_name").toString());
		        	
		        	FileChannel fcin = input.getChannel();
		        	FileChannel fcout = output.getChannel();
		        	
		        	long size = fcin.size();
		        	fcin.transferTo(0,  size,  fcout);
		        	
		    	    fcout.close();
		    	    fcin.close();
		    	    input.close();
		    	    output.close();

		        } catch (IOException e) {
		        	e.printStackTrace();
		        }
        	}
        }
		
		if(chk==0)
			resultMap.put("status", "다운로드할 파일이 없습니다.");
		
        //과제 Keseluruhan 다운로드
        try {
        	File zipFolder = new File(ROOT_PATH + RES_PATH + "/upload/assignMentZip/"+TimeName);
        	if(!zipFolder.exists())
        		zipFolder.mkdirs();
        	
			ZipUtil.zip(fileFolder.getPath(), ROOT_PATH + RES_PATH + "/upload/assignMentZip/"+TimeName+"/"+ zipName);
		} catch (Exception e) {
        	resultMap.put("status", "fail");
		}
                
    	//파일 삭제
        FileUploader.deleteFile(fileFolder);
        fileFolder.delete();
        
        resultMap.put("filePath", RES_PATH + "/upload/assignMentZip/"+TimeName+"/"+ zipName);
        
		return resultMap;
	}
	
	public int getAssignMentSeq(HashMap<String, Object> param) throws Exception {
		// TODO Auto-generated method stub
		return pfLessonDao.getAssignMentSeq(param);
	}



	public ResultMap setCurrAndLp(HashMap<String, Object> param, HttpServletRequest request) throws Exception {
		ResultMap resultMap = new ResultMap();
		String currentAcademicYear = "";
		Calendar c = Calendar.getInstance(); 
		currentAcademicYear = String.valueOf(c.get(Calendar.YEAR));
		
		//학사tahun목록
		List<HashMap<String, Object>> academicYearList = pfLessonDao.getLeftMenuAcademicYearList(param);
		
		if(request.getSession().getAttribute("S_CURRENT_ACADEMIC_YEAR") == null) {
			if(!academicYearList.isEmpty())
				currentAcademicYear = academicYearList.get(0).get("year").toString();	
			
			//현재 학사tahun session set
			request.getSession().setAttribute("S_CURRENT_ACADEMIC_YEAR", currentAcademicYear);
		}
		
		if(request.getSession().getAttribute("S_CURRICULUM_SEQ") == null) {
			param.put("current_academic_year", currentAcademicYear);
			List<HashMap<String, Object>> curriculumList = pfLessonDao.getLeftMenuCurriculumList(param);
			
			if(!curriculumList.isEmpty()) {
				request.getSession().setAttribute("S_CURRICULUM_SEQ", curriculumList.get(0).get("curr_seq").toString());	
				param.put("curr_seq", curriculumList.get(0).get("curr_seq"));
				param.put("curr_represent_yn", "N");
				param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
				List<HashMap<String, Object>> lessonPlanList =  pfLessonDao.getLessonPlanList(param);
				if(!lessonPlanList.isEmpty())
					request.getSession().setAttribute("S_LP_SEQ", lessonPlanList.get(0).get("lp_seq").toString());
					
			}
			
		}

		
		return resultMap;
	}



	public ResultMap formationEvaluationState(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("state", pfLessonDao.formationEvaluationState(param));
		
		return resultMap;
	}



	public ResultMap getLeftLessonPlanList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		PagingHelper pagingHelper = new PagingHelper(5, 1);
		//카운트 취득
		int totalCount = pfLessonDao.getLessonPlanListCount(param);
		
		List<HashMap<String, Object>> list = pfLessonDao.getLessonPlanList(pagingHelper.getPagingParam(param, totalCount));
		resultMap.put("lesson_plan_list", list);
		resultMap.put("countInfo", pagingHelper.getPageCnt(totalCount));
		return resultMap;
	}



	public ResultMap getReportInfo(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		//진행정보
		resultMap.put("progressInfo", pfLessonDao.getReportCurrProgress(param));
		//informasi dasar
		resultMap.put("basicInfo", pfLessonDao.getCurriculumBasicInfo(param));
		//현재 교육과정, 동일 학사, 전년 동일 교육과정 그래프 정보 가져오기
		resultMap.put("currGraph", pfLessonDao.getReportCurrGraph(param));
		//형성penilaian 리스트 가져오기
		resultMap.put("feList", pfLessonDao.getReportFe(param));
		//성적분포 리스트 가져오기
		resultMap.put("gradeList", pfLessonDao.getReportGrade(param));
		//syarat kelulusan별 시수
		resultMap.put("fcList", pfLessonDao.getReportFc(param));
		//단원 가져오기
		resultMap.put("unitList", unitDao.getUnitList(param));
		//임상표현 가져오기
		resultMap.put("clinicList", pfLessonDao.getReportClinic(param));
		//진단 가져오기
		resultMap.put("diaList",  pfLessonDao.getReportDia(param));
		return resultMap;
	}

	public ResultMap deleteAssignMent(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		int feedback_cnt = pfLessonDao.getAssignMentFeedBackChk(param);
		if(feedback_cnt > 0) {
			resultMap.put("status", "001");
			resultMap.put("msg", "과제 피드백 입력된 데이터가 있습니다.\n manajemen자에게 문의해주세요");
		}else {
			pfLessonDao.deleteAssignMent(param);
		}
		
			
		return resultMap;
	}
	
	public ResultMap getPopupLessonList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		String gubun = param.get("gubun").toString();
		if("fc".equals(gubun)) {
			resultMap.put("list", pfLessonDao.getReportFcLesson(param));
		}else if("clinic".equals(gubun)) {
			resultMap.put("list", pfLessonDao.getReportClinicLesson(param));
		}else if("dia".equals(gubun)) {
			resultMap.put("list", pfLessonDao.getReportDiaLesson(param));
		}
		
		return resultMap;
	}
	
	@Transactional
	public ResultMap lessonDataCommentInsert(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		String[] lesson_attach_seq = param.get("lesson_attach_seq").toString().split(",");
		
		for(int i=0; i<lesson_attach_seq.length; i++) {
			param.put("lesson_attach_seq", lesson_attach_seq[i]);
			pfLessonDao.insertComment(param);
		}
		
		return resultMap;
	}



	public ResultMap getLessonPlanPeriodInfo(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("periodInfo", pfLessonDao.getLessonPlanPeriod(param));
		resultMap.put("lessonMethodInfo", pfLessonDao.getLessonPlanLessonMethodCnt(param));	
		resultMap.put("feInfo", pfLessonDao.getLessonPlanFeCount(param));		
		return resultMap;
	}



	public ResultMap getCurriculumInfo(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("basicInfo", pfLessonDao.getCurriculumBasicInfo(param));
		return resultMap;
	}



	public ResultMap getDiaCodePageList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		int totalCnt = pfLessonDao.getDiaCodeListCount(param);
		PagingHelper pagingHelper = new PagingHelper();
		List<HashMap<String, Object>> diaCodeList = pfLessonDao.getDiaCodePagingList(pagingHelper.getPagingParam(param, totalCnt));
		resultMap.putAll(pagingHelper.getPageList(diaCodeList, "getDiaCode", totalCnt));
		return resultMap;
	}



	public ResultMap getAssignMentAllList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("list", pfLessonDao.getLpAllAssignMentList(param));
		return resultMap;
	}



	public ResultMap getPreCurr(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		int count = pfLessonDao.getPreCurrCount(param);
		
		if(count == 0) {
			resultMap.put("status", "201");
			return resultMap;
		}
		
		HashMap<String, Object> preCurrInfo = pfLessonDao.getPreCurrInfo(param);
		resultMap.put("preCurrInfo", preCurrInfo);
		
		param.put("curr_seq", preCurrInfo.get("curr_seq").toString());
		
		//dasar dasar kurikulum
		HashMap<String, Object> curriculumBasicInfo = new HashMap<String, Object>();
		curriculumBasicInfo = pfLessonDao.getCurriculumBasicInfo(param);
		
		if (curriculumBasicInfo == null || curriculumBasicInfo.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_CURRICULUM_BASIC_INFO", "007");
		}
		resultMap.put("basicInfo", curriculumBasicInfo);
		//Dosen Penanggung jawab
		param.put("professor_level", "1");
		resultMap.put("mpfList", academicDao.getCurriculumMpAssignPf(param));
		
		//부Dosen Penanggung jawab
		param.put("professor_level", "2");
		resultMap.put("dpfList", academicDao.getCurriculumMpAssignPf(param));
		
		//일반교수
		param.put("professor_level", "3");
		resultMap.put("gpfList", academicDao.getCurriculumMpAssignPf(param));
		
		//tidak ada perkuliahan 리스트 가져오기
		resultMap.put("lessonMethodList", pfLessonDao.getLessonMethodList(param));
		
		//형성penilaian 리스트 가져오기
		resultMap.put("formationEvaluationCs", pfLessonDao.getFormationEvaluationCs(param));
		
		//총괄penilaian기준 가져오기
		resultMap.put("summativeEvaluationCs", pfLessonDao.getSummativeEvaluation(param));
		
		//syarat kelulusan 가져오기
		resultMap.put("mpCurriculumGraduationCapabilityList", pfLessonDao.getMpCurriculumGraduationCapabilityList(param));
		
		//nilai기준 가져오기
		//resultMap.put("gradeStandard", pfLessonDao.getGradeStandardList(param));
		
		return resultMap;
	}
}