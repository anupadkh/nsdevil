package com.nsdevil.medlms.web.admin.dao;

import java.util.HashMap;
import java.util.List;


public interface SurveyDao {

	public List<HashMap<String, Object>> getSFResearchList(HashMap<String, Object> param) throws Exception;
	public int insertSFResearch(HashMap<String, Object> param) throws Exception;
	public int updateSFResearch(HashMap<String, Object> param) throws Exception;
	public int insertSFResearchHeader(HashMap<String, Object> param) throws Exception;
	public int updateSFResearchHeader(HashMap<String, Object> param) throws Exception;
	public int insertSFResearchItem(HashMap<String, Object> param) throws Exception;
	public int updateSFResearchItem(HashMap<String, Object> param) throws Exception;
	public int updateSFResearchAll(HashMap<String, Object> param) throws Exception;
	public int insertSFResearchVersion(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getSFResearchQuestionList(HashMap<String, Object> param);
	
}
