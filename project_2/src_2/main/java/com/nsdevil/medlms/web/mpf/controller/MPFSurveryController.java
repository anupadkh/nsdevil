package com.nsdevil.medlms.web.mpf.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.web.admin.service.AcademicService;
import com.nsdevil.medlms.web.common.service.CommonService;
import com.nsdevil.medlms.web.mpf.service.MPFSurveryService;

@Controller
public class MPFSurveryController extends ExceptionController {

	@Autowired
	private MPFSurveryService mpfSurveryService;

	@Autowired
	private CommonService commonService;

	@Autowired
	private AcademicService academicService;
	
	@RequestMapping(value = "/pf/lesson/currSurvery", method = RequestMethod.GET)
	public String currSurveryView(HttpServletRequest request, Model model) throws Exception {
			
		return "mpf/lesson/currSurvery";
	}
	
	//과정만족도 조사 현황 리스트 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/SFSurvey/list", method = RequestMethod.POST)
	public String currSFSurveyList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ"));
		model.addAllAttributes(mpfSurveryService.getCurrSFSurveyInfo(param));	
		return JSON_VIEW;
	}
	
	@RequestMapping(value = "/pf/lesson/lessonPlanSurvery", method = RequestMethod.GET)
	public String lessonPlanView(HttpServletRequest request, Model model) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		model.addAllAttributes(commonService.getAcaState(param));		
		return "mpf/lesson/lessonPlanSurvery";
	}
	
	//수업만족도 조사 현황 리스트 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/lessonPlanSurvery/list", method = RequestMethod.POST)
	public String lessonPlanSFSurveyList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ"));
		model.addAllAttributes(mpfSurveryService.getlessonPlanSFSurveyInfo(param));	
		return JSON_VIEW;
	}
}
