package com.nsdevil.medlms.web.common.dao;

import java.util.HashMap;
import java.util.List;


public interface CommonDao {
	public int getBoardContentsCount(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getBoardContentsList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getCodeList(String codeCate) throws Exception;
	public int updateBoardHits(String boardSeq) throws Exception;
	public HashMap<String, Object> getAcaState(HashMap<String, Object> param) throws Exception;
	public int getIdCheck(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getAssignCurrList(HashMap<String, Object> param) throws Exception;
	public int insertBoard(HashMap<String, Object> param) throws Exception;
	public int insertBoardAttach(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getBoardAttachFileList(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getBoardDetailInfo(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getNextBoardInfo(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getPrevBoardInfo(HashMap<String, Object> param) throws Exception;
	public int deleteBoardAttach(HashMap<String, Object> param) throws Exception;
	public int deleteBoard(HashMap<String, Object> param) throws Exception;
	public int updateBoard(HashMap<String, Object> param) throws Exception;
	public int deleteBoardAttachForBoardAttachSeq(String param) throws Exception;
	public List<HashMap<String, Object>> getDomainCodeList(String codeCate) throws Exception;
	public List<HashMap<String, Object>> getCpxCode() throws Exception;
	public List<HashMap<String, Object>> getDiaCode() throws Exception;
	public List<HashMap<String, Object>> getUnitCode() throws Exception;
	public HashMap<String, Object> getAcademicSystemInfo(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getAcademicSystemInfoForSeq(String param) throws Exception;
	public List<HashMap<String, Object>> getOsceCode() throws Exception;
	public List<HashMap<String, Object>> getSTAssignCurrList(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getCurrFlagInfo(HashMap<String, Object> param) throws Exception;
	public int getAdminQnaCount() throws Exception;
	public int getAdminAcaScheduleCount(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getAcademicStageInfo(HashMap<String, Object> param) throws Exception;
}
