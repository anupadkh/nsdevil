package com.nsdevil.medlms.web.admin.dao;

import java.util.HashMap;
import java.util.List;


public interface LessonDao {

	public List<HashMap<String, Object>> getAcaYearList() throws Exception;

	public List<HashMap<String, Object>> getAcaDemicList(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getCurrList(HashMap<String, Object> param) throws Exception;

	public int getLpListCount(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getLpList(HashMap<String, Object> pagingParam) throws Exception;

	public int confirmFlagCurr(HashMap<String, Object> param) throws Exception;

	public int curriculumMonitoringCount(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> curriculumMonitoring(HashMap<String, Object> pagingParam) throws Exception;
}
