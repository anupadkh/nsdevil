package com.nsdevil.medlms.web.common.dao;

import java.util.HashMap;
import java.util.List;


public interface RoomReserveDao {

	public List<HashMap<String, Object>> getReserveList(HashMap<String, Object> param) throws Exception;

	public int insertReserve(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getFacilityCode(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getReserveDetail(HashMap<String, Object> param) throws Exception;


}
