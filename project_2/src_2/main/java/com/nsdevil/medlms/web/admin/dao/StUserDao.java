package com.nsdevil.medlms.web.admin.dao;

import java.util.HashMap;
import java.util.List;


public interface StUserDao {

	public HashMap<String, Object> getOverLapChk(HashMap<String, Object> param) throws Exception;

	public int insertUsertSt(HashMap<String, Object> param) throws Exception;

	public int getSudentListCount(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getSudentList(HashMap<String, Object> pagingParam) throws Exception;

	public HashMap<String, Object> stUserDetail(HashMap<String, Object> param) throws Exception;

	public int updateUsertSt(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getSTAcaInfo(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getStNowAcaInfo(HashMap<String, Object> param) throws Exception;

	public int idOverlapChk(HashMap<String, Object> map) throws Exception;

	public int emailOverlapChk(HashMap<String, Object> map) throws Exception;
	
}
