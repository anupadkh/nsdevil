package com.nsdevil.medlms.web.student.dao;

import java.util.HashMap;
import java.util.List;


public interface STAcademicDao {

	public List<HashMap<String,Object>> getCourseApplicationList(HashMap<String, Object> param) throws Exception;

	public HashMap<String,Object> getCourseApplicationDetail(HashMap<String, Object> param) throws Exception;

	public HashMap<String,Object> getCourseApplicationAttach(HashMap<String, Object> param) throws Exception;

	public HashMap<String,Object> getRefund(HashMap<String, Object> param) throws Exception;

	public HashMap<String,Object> getNotes(HashMap<String, Object> param) throws Exception;

	public void insertCaSt(HashMap<String, Object> param) throws Exception;

	public int getCaStCount(HashMap<String, Object> param) throws Exception;	
}
