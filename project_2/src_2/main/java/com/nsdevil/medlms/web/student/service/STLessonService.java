package com.nsdevil.medlms.web.student.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.exception.RuntimeLogicException;
import com.nsdevil.medlms.common.util.FileUploader;
import com.nsdevil.medlms.common.util.PagingHelper;
import com.nsdevil.medlms.common.util.ZipUtil;
import com.nsdevil.medlms.web.pf.dao.PFLessonDao;
import com.nsdevil.medlms.web.student.dao.STLessonDao;

@Service
public class STLessonService {

	@Autowired
	private STLessonDao stLessonDao;
	
	@Autowired
	private PFLessonDao pfLessonDao;
	
	@Value("#{config['config.rootPath']}")
	private String ROOT_PATH;
	
	@Value("#{config['config.resPath']}")
	private String RES_PATH;
	
	@Value("#{config['config.quizPath']}")
	private String QUIZ_PATH;

	@Value("#{config['config.formationEvaluation']}")
	private String FORMATION_EVALUAION_PATH;
	
	
	
	public ResultMap getLessonLeftMenuList(HashMap<String, Object> param, HttpServletRequest request) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		String currentAcademicYear = (String) param.get("academicYear");//현재 학사tahun
		
		//학사tahun목록
		List<HashMap<String, Object>> academicYearList = stLessonDao.getLeftMenuAcademicYearList(param);
		
		if (currentAcademicYear == null || currentAcademicYear.equals("")) {//pilih한 학사tahun가 없으면 최근 첫번째 학사tahun
			currentAcademicYear = academicYearList.get(0).get("year").toString();
		}
		if(currentAcademicYear.isEmpty() || currentAcademicYear.equals("")) {
			Calendar c = Calendar.getInstance(); 
			currentAcademicYear = String.valueOf(c.get(Calendar.YEAR));
		}
		
		//현재 학사tahun session set
		request.getSession().setAttribute("S_CURRENT_ACADEMIC_YEAR", currentAcademicYear);

		param.put("year", currentAcademicYear);
		
		List<HashMap<String, Object>> curriculumList = stLessonDao.getLeftMenuCurriculumList(param);

		resultMap.put("academic_year_list", academicYearList);
		resultMap.put("curriculum_list", curriculumList);
		resultMap.put("current_academic_year", currentAcademicYear);
		return resultMap;
	}

	public ResultMap getLessonPlanLeftMenuList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		PagingHelper pagingHelper = new PagingHelper(7, 1);
		//카운트 취득
		int totalCount = stLessonDao.getLeftMenuLessonPlanListCount(param);	
		
		if(totalCount > 0 && param.get("newYN").toString().equals("Y")) {
			//현재 hari dan tanggal에 가장 가까운거 rownumber 가져온다.
			int row_num = stLessonDao.getTodayLessonPeriodSeq(param);
			
			int page = (int) Math.ceil(row_num/7.0);
			param.put("page", String.valueOf(page));
		}
		
		resultMap.put("lessonPlanList", stLessonDao.getLeftMenuLessonPlanList(pagingHelper.getPagingParam(param, totalCount)));
		resultMap.put("countInfo", pagingHelper.getPageCnt(totalCount));
		
		return resultMap;
	}

	public ResultMap getCurriculumLeftMenuList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("curriculumPlanList", stLessonDao.getLeftMenuCurriculumList(param));
		return resultMap;
	}

	public ResultMap getLessonPlanTitle(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		param.put("cnt", stLessonDao.getLessonPlanDefaultCount(param));
		HashMap<String, Object> map = stLessonDao.getLessonPlanOne(param); 
		resultMap.put("lessonPlan", map);
		if(map != null && !map.isEmpty())
			param.put("lp_seq", map.get("lp_seq").toString());
		
		int attendanceCount = stLessonDao.getAttendanceCount(param); 
		if(attendanceCount > 0)
			resultMap.put("attendance", stLessonDao.getAttendance(param));
		else
			resultMap.put("attendance", attendanceCount);
		return resultMap;
	}

	public ResultMap setCurrAndLpSeq(HttpServletRequest request) throws Exception {
		ResultMap resultMap = new ResultMap();
		String currentAcademicYear = "";
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		
		//학사tahun목록
		List<HashMap<String, Object>> academicYearList = stLessonDao.getLeftMenuAcademicYearList(param);
		
		if (currentAcademicYear == null || currentAcademicYear.equals("")) {//pilih한 학사tahun가 없으면 최근 첫번째 학사tahun
			if(academicYearList != null)
				currentAcademicYear = academicYearList.get(0).get("year").toString();
		}
		if(currentAcademicYear.isEmpty() || currentAcademicYear.equals("")) {
			Calendar c = Calendar.getInstance(); 
			currentAcademicYear = String.valueOf(c.get(Calendar.YEAR));
		}

		request.getSession().setAttribute("S_CURRENT_ACADEMIC_YEAR", currentAcademicYear);

		param.put("year", currentAcademicYear);
		
		if(request.getSession().getAttribute("S_CURRICULUM_SEQ") == null) {
			List<HashMap<String, Object>> curriculumList = stLessonDao.getLeftMenuCurriculumList(param);
			if(curriculumList != null && !curriculumList.isEmpty())
				request.getSession().setAttribute("S_CURRICULUM_SEQ", curriculumList.get(0).get("curr_seq").toString());
		}	
		
		if(request.getSession().getAttribute("S_LP_SEQ") == null) {
			if(request.getSession().getAttribute("S_CURRICULUM_SEQ") != null) {
				param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());	
				
				
				int cnt = stLessonDao.getLessonPlanDefaultCount(param);				
				param.put("cnt", cnt);
				HashMap<String, Object> map = null;
				if(cnt == 0)
					map = stLessonDao.getLessonPlanOne(param);
				else
					map = stLessonDao.getLessonPlanDefault(param);
				
				resultMap.put("lessonPlan", map);
				if(map != null && !map.isEmpty()) 
					request.getSession().setAttribute("S_LP_SEQ", map.get("lp_seq").toString());
				
			}
		}
		
		return resultMap;
	}

	public ResultMap STFormationEvaluationList(HashMap<String, Object> param) throws Exception  {
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("feList", stLessonDao.getSTFormationEvaluationList(param));
		return resultMap;
	}

	public ResultMap STAssignMentList(HashMap<String, Object> param) throws Exception  {
		ResultMap resultMap = new ResultMap();
		resultMap.put("assignMentList", stLessonDao.getAssignMentSTList(param));
		return resultMap;
	}

	public ResultMap STAssignMentOneList(HashMap<String, Object> param) throws Exception  {
		ResultMap resultMap = new ResultMap();
		HashMap<String, Object> assignMentInfo = pfLessonDao.getAssignMent(param);
		param.put("group_flag", assignMentInfo.get("group_flag").toString());
		resultMap.put("assignMent", assignMentInfo);
		resultMap.put("assignMentSubmit", stLessonDao.getAssignMentSubmitST(param));
		resultMap.put("assignMentAttachList", pfLessonDao.getAssignMentAttach(param));
		return resultMap;
	}

	@Transactional
	public ResultMap assignMentSubmitInsert(HashMap<String, Object> param, MultipartHttpServletRequest request) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		if(!param.get("asgmt_submit_seq").toString().isEmpty()) {
			if(stLessonDao.getAssignMentFeedbackYN(param) > 0) {
				resultMap.put("status", "피드백 받은 과제는 perbaiki이 불가능합니다.");
				return resultMap; 
			}
		}
		
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());	

		MultipartFile file = request.getFile("File");
		
		//수업시퀀스 가져온다.
		int lp_seq = stLessonDao.getAssignMentLpseq(param);
		
		String curr_seq = request.getSession().getAttribute("S_CURRICULUM_SEQ").toString(); //교육과정 시퀀스
		String year = request.getSession().getAttribute("S_CURRENT_ACADEMIC_YEAR").toString(); //tahun
		String id = request.getSession().getAttribute("S_USER_ID").toString();
		if(file != null) {
			String defaultPath = ROOT_PATH + RES_PATH + "/upload/"+year+"/"+curr_seq+"/"+lp_seq+"/assignment_submit/"+id+"/";
			String dbInsertPath = "/upload/"+year+"/"+curr_seq+"/"+lp_seq+"/assignment_submit/"+id+"/";
			File files = new File(defaultPath);
			if(!files.exists()) {
				files.mkdirs();
			}
			
			String filename = FileUploader.uploadFile(defaultPath, file);
			
			param.put("file_name", file.getOriginalFilename());
			param.put("file_path", dbInsertPath+filename);
			
		}
		
		//그룹과제일 경우 해당 그룹인 애들 다 인서트
		if(param.get("group_flag").toString().equals("Y")) {
			List<HashMap<String,Object>> stList = stLessonDao.getAssignMentGroupStList(param);
			
			for(int i=0; i<stList.size(); i++) {
				param.put("user_seq", stList.get(i).get("user_seq").toString());	
				param.put("asgmt_submit_seq", stList.get(i).get("asgmt_submit_seq").toString());
				if(param.get("asgmt_submit_seq").toString().equals("0")) {
					if(stLessonDao.insertAssignMentSubmit(param) == 0)
						throw new RuntimeLogicException("QUERY_FAIL [ st - assignment_submit insert query fail ]", "004");
				}else {
					if(stLessonDao.updateAssignMentSubmit(param) == 0)
						throw new RuntimeLogicException("QUERY_FAIL [ st - assignment_submit update query fail ]", "004");
				}
			}
		}else {
			if(param.get("asgmt_submit_seq").toString().equals("")) {
				if(stLessonDao.insertAssignMentSubmit(param) == 0)
					throw new RuntimeLogicException("QUERY_FAIL [ st - assignment_submit insert query fail ]", "004");
			}else {
				if(stLessonDao.updateAssignMentSubmit(param) == 0)
					throw new RuntimeLogicException("QUERY_FAIL [ st - assignment_submit update query fail ]", "004");
			}
		}

		
		resultMap.put("asgmt_seq", param.get("asgmt_seq").toString());
		
		return resultMap;
	}

	public ResultMap assignMentSubmitFileDel(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		if(stLessonDao.updateAssignMentSubmitFileDel(param) == 0)
			throw new RuntimeLogicException("QUERY_FAIL [ st - assignment_submit file del update query fail ]", "004");
		
		return resultMap;
	}

	public ResultMap lessonDataMultiFileDown(HashMap<String, Object> param) throws Exception  {
		ResultMap resultMap = new ResultMap();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		long milliSecond = System.currentTimeMillis();
		String milName = String.valueOf(milliSecond).substring(10, 13);
		
    	String TimeName = (sdf.format(new java.util.Date()) + milName);
    	
		File fileFolder = new File(ROOT_PATH + RES_PATH + "/upload/lessonData/"+TimeName);		
		
		String zipName = TimeName+"_File.zip";

    			
		//폴더 없으면 생성함
		if(!fileFolder.exists()){
			fileFolder.mkdirs();			
		}

        List<HashMap<String, String>> makeFileList = new ArrayList<HashMap<String, String>>();
		//파일명 리스트 가져옴
        String[] filePathList = param.get("file_path").toString().split("\\|\\|");
		String[] filenameList = param.get("file_name").toString().split("\\|\\|");
		int chk = 0;
		
		//파일이 있는지 체크
		for(int index=0; index < filePathList.length; index++){			
			File file = new File(ROOT_PATH + RES_PATH +filePathList[index]);			
			if(file.exists()){
				HashMap<String, String> fileMap = new HashMap<String, String>();
				fileMap.put("file_name", filenameList[index]);
				fileMap.put("file_path", filePathList[index]);
				makeFileList.add(chk, fileMap);
    			chk++;
			}
		}
		        
		FileInputStream input = null;
		FileOutputStream output = null;
        //파일 복사
        for (int i=0; i<makeFileList.size(); i++) {
        	
			HashMap<String, String> file_Info = makeFileList.get(i);
        	if(!file_Info.get("file_name").toString().equals("")){
		        try {		        			        	
		        	input = new FileInputStream(ROOT_PATH + RES_PATH + file_Info.get("file_path").toString());
		        	output = new FileOutputStream(fileFolder + "/" + file_Info.get("file_name").toString());
		        	
		        	FileChannel fcin = input.getChannel();
		        	FileChannel fcout = output.getChannel();
		        	
		        	long size = fcin.size();
		        	fcin.transferTo(0,  size,  fcout);
		        	
		    	    fcout.close();
		    	    fcin.close();
		    	    input.close();
		    	    output.close();

		        } catch (IOException e) {
		        	e.printStackTrace();
		        }
        	}
        }
		
		if(chk==0)
			resultMap.put("status", "다운로드할 파일이 없습니다.");
		
        //과제 Keseluruhan 다운로드
        try {
        	File zipFolder = new File(ROOT_PATH + RES_PATH + "/upload/lessonDataZip/"+TimeName);
        	if(!zipFolder.exists())
        		zipFolder.mkdirs();
        	
			ZipUtil.zip(fileFolder.getPath(), ROOT_PATH + RES_PATH + "/upload/lessonDataZip/"+TimeName+"/"+ zipName);
		} catch (Exception e) {
        	resultMap.put("status", "fail");
		}
                
    	//파일 삭제
        FileUploader.deleteFile(fileFolder);
        fileFolder.delete();
        
        resultMap.put("filePath", RES_PATH + "/upload/lessonDataZip/"+TimeName+"/"+ zipName);
        
		
		return resultMap;
	}

	public ResultMap getCurrGradeList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("currList", stLessonDao.getLessonCurrGradeList(param));
		return resultMap;
	}

	public ResultMap getAcaState(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("aca_state", stLessonDao.getCurrAcaState(param));		
		return resultMap;
	}

	public ResultMap getSoosiGradeList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("soosiList", stLessonDao.getLessonSoosiGradeList(param));
		return resultMap;
	}

	public ResultMap getFeScoreList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("feList", stLessonDao.getLessonFeScoreList(param));
		return resultMap;
	}

	public String getCurrEndChk(HashMap<String, Object> param) throws Exception {
				
		return stLessonDao.getCurrEndChk(param);
	}

	public String getLessonEndChk(HashMap<String, Object> param) throws Exception {
		 
		return stLessonDao.getLessonEndChk(param);
	}
}