package com.nsdevil.medlms.web.common.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.web.common.dao.ScheduleDao;
import com.nsdevil.medlms.web.mpf.dao.MPFScheduleDao;

@Service
public class ScheduleService {

	@Autowired
	private ScheduleDao scheduleDao;

	@Autowired
	private MPFScheduleDao mpfScheduleDao;
	
	public ResultMap getMYSchedule(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("schedule", scheduleDao.getMYSchedule(param));
		
		return resultMap;
	}

	public ResultMap getMYScheduleMemo(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();

		resultMap.put("memo", scheduleDao.getMemo(param));
		
		return resultMap;
	}
	
	/**
	 * 일정 registrasi
	 */
	public ResultMap addMemo(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		scheduleDao.addMemo(param);
		return resultMap;
	}
	
	/**
	 * 메모 삭제
	 */
	public ResultMap deleteMemo(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		scheduleDao.deleteMemo(param);
		return resultMap;
	}
	
	/**
	 * 일정 상세
	 */
	public HashMap<String, Object> getMemo(HashMap<String, Object> param) throws Exception {
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		List<HashMap<String, Object>> resultList = scheduleDao.getMemo(param);
		
		if (0 < resultList.size()) {
			result = resultList.get(0);
		}
		return result;
	}

	public ResultMap getMYScheduleST(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		resultMap.put("schedule", scheduleDao.getMYScheduleSt(param));
		
		return resultMap;
	}
	
}
