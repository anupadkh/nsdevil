package com.nsdevil.medlms.web.common.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.exception.LogicException;
import com.nsdevil.medlms.common.exception.ResourceNotFoundException;
import com.nsdevil.medlms.common.exception.RuntimeLogicException;
import com.nsdevil.medlms.common.util.FileUploader;
import com.nsdevil.medlms.common.util.PagingHelper;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.common.dao.CommonDao;
import com.nsdevil.medlms.web.mpf.dao.MPFUnitDao;

@Service
public class CommonService {

	@Value("#{config['config.rootPath']}")
	private String ROOT_PATH;
	
	@Value("#{config['config.resPath']}")
	private String RES_PATH;
	
	@Value("#{config['config.boardPath']}")
	private String BOARD_PATH;
	
	@Autowired
	private CommonDao dao;
	
	@Autowired
	private MPFUnitDao unitDao;

	
	public ResultMap getBoardList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		int totalCnt = dao.getBoardContentsCount(param);
		PagingHelper pagingHelper = new PagingHelper();
		List<HashMap<String, Object>> ContentsList = dao.getBoardContentsList(pagingHelper.getPagingParam(param, totalCnt));
		resultMap.putAll(pagingHelper.getPageList(ContentsList, (String)param.get("listFunctionName"), totalCnt));
		return resultMap;
	}

	public List<HashMap<String, Object>> getCodeList(String codeCate) throws Exception {
		List<HashMap<String, Object>> codeList = new ArrayList<HashMap<String, Object>>();
		codeList = dao.getCodeList(codeCate);
		return codeList;
	}
	
	public List<HashMap<String, Object>> getDomainCodeList(HashMap<String, Object> param) throws Exception {
		List<HashMap<String, Object>> codeList = new ArrayList<HashMap<String, Object>>();
		codeList = unitDao.getUnitCode(param);
		return codeList;
	}
	
	public void updateBoardHits(String boardSeq) throws Exception {
		if (boardSeq != null && !boardSeq.equals("")) {
			dao.updateBoardHits(boardSeq);
		}
	}
	
	public ResultMap getAcaState(HashMap<String, Object> param) throws Exception{
		ResultMap resultMap = new ResultMap();
		resultMap.put("acaState", dao.getAcaState(param));
		
		return resultMap;
	}

	public ResultMap getAssignCurrList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("currList", dao.getAssignCurrList(param));
		
		return resultMap;
	}
	
	public HashMap<String, Object> getBoardAttachFileInfo(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		List<HashMap<String, Object>> fileList = new ArrayList<HashMap<String, Object>>();
		fileList = dao.getBoardAttachFileList(param);
		String zipFileName = (String)param.get("zip_file_name");
		if (zipFileName != null && !zipFileName.equals("")) {
			resultMap.put("zipFileName", zipFileName);
		}
		resultMap.put("fileList", fileList);
		return resultMap;
	}
	
	
	public ResultMap getBoardDetail(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		
		HashMap<String, Object> boardDetailInfo = new HashMap<String, Object>();
		boardDetailInfo = dao.getBoardDetailInfo(param);
		if (boardDetailInfo == null || boardDetailInfo.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_NOTICE_INFO", "007");
		}
		
		List<HashMap<String, Object>> attachList = new ArrayList<HashMap<String, Object>>();
		attachList = dao.getBoardAttachFileList(param);
		
		HashMap<String, Object> nextBoardInfo = new HashMap<String, Object>();
		nextBoardInfo = dao.getNextBoardInfo(param);
		HashMap<String, Object> prevBoardInfo = new HashMap<String, Object>();
		prevBoardInfo = dao.getPrevBoardInfo(param);
		
		resultMap.put("board_info", boardDetailInfo);
		resultMap.put("attach_list", attachList);
		resultMap.put("next_board_info", nextBoardInfo);
		resultMap.put("prev_board_info", prevBoardInfo);
		return resultMap;
	}
	
	@Transactional
	public ResultMap boardCreate(HashMap<String, Object> param, HttpServletRequest request) throws Exception {
		ResultMap resultMap = new ResultMap();
		HashMap<String, Object> boardParam = new HashMap<String, Object>();
		String boardCateCode= (String)param.get("board_cate_code");
		if (boardCateCode == null || boardCateCode.equals("")) {
			boardCateCode = null;
		}
		boardParam.put("board_code", param.get("board_code"));
		boardParam.put("board_cate_code", boardCateCode);
		boardParam.put("show_target", "00");
		boardParam.put("title", param.get("title"));
		boardParam.put("content", param.get("content"));
		boardParam.put("s_user_seq", param.get("s_user_seq"));
		
		if (dao.insertBoard(boardParam) < 1) {
			throw new RuntimeLogicException("QUERY_FAIL [board insert query fail ]", "004");
		}
		String boardSeq = String.valueOf(boardParam.get("board_seq"));
		
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		List<MultipartFile> attachFileList = mRequest.getFiles("uploadFile");
		String uploadPath = ROOT_PATH + RES_PATH + BOARD_PATH + "/" + boardSeq;
		String filePath = BOARD_PATH + "/" + boardSeq;
		for (MultipartFile attachFile : attachFileList) {
			HashMap<String, Object> boardAttachParam = new HashMap<String, Object>();
			String realName = FileUploader.uploadFile(uploadPath, attachFile);
			if (realName == null || realName.equals("not")) {
				Util.deleteFolder(new File(uploadPath));
				throw new LogicException("FILE_UPLOAD_FAIL", "003");
			}
			boardAttachParam.put("board_seq", boardSeq);
			boardAttachParam.put("file_path", filePath+"/"+realName);
			boardAttachParam.put("file_name", attachFile.getOriginalFilename());
			boardAttachParam.put("attach_type", Util.getFileType(realName));
			boardAttachParam.put("s_user_seq", param.get("s_user_seq"));
			if (dao.insertBoardAttach(boardAttachParam) < 1) {
				Util.deleteFolder(new File(uploadPath));
				throw new RuntimeLogicException("QUERY_FAIL [ board_attach insert fail ]", "004");
			}
		}
		return resultMap;
	}
	
	@Transactional
	public ResultMap removeBoard(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		String boardSeqsStr = (String) param.get("remove_board_seqs");
		if (boardSeqsStr != null && !boardSeqsStr.equals("")) {
			String[] boardSeqsArr = boardSeqsStr.split(",");
			for (String boardSeq : boardSeqsArr) {
				HashMap<String, Object> paramBoard = new HashMap<String, Object>();
				paramBoard.put("board_seq", boardSeq);
				paramBoard.put("s_user_seq", param.get("s_user_seq"));
				dao.deleteBoardAttach(paramBoard);
				if (dao.deleteBoard(paramBoard) < 1) {
					throw new RuntimeLogicException("QUERY_FAIL [ board delete query fail ]", "004");
				}
			}
		}
		return resultMap;
	}
	
	@Transactional
	public ResultMap boardModify(HashMap<String, Object> param, HttpServletRequest request) throws Exception {
		ResultMap resultMap = new ResultMap();
		HashMap<String, Object> boardParam = new HashMap<String, Object>();
		String boardCateCode= (String)param.get("board_cate_code");
		if (boardCateCode == null || boardCateCode.equals("")) {
			boardCateCode = null;
		}
		boardParam.put("board_cate_code", boardCateCode);
		boardParam.put("board_seq", param.get("board_seq"));
		boardParam.put("title", param.get("title"));
		boardParam.put("content", param.get("content"));
		boardParam.put("s_user_seq", param.get("s_user_seq"));
		
		if (dao.updateBoard(boardParam) < 1) {
			throw new RuntimeLogicException("QUERY_FAIL [board update query fail ]", "004");
		}
		
		String boardSeq = String.valueOf(boardParam.get("board_seq"));
		
		String removeAttachSeqStr = (String)param.get("removeAttachSeqStr");
		if (removeAttachSeqStr != null && !removeAttachSeqStr.equals("")) {
			String [] removeAttachSeqArr = removeAttachSeqStr.split(",");
			for(String boardAttachSeq : removeAttachSeqArr) {
				if (dao.deleteBoardAttachForBoardAttachSeq(boardAttachSeq) < 1) {
					throw new RuntimeLogicException("QUERY_FAIL [board_attach delete query fail ]", "004");
				}
			}
		}
		
		MultipartHttpServletRequest mRequest = (MultipartHttpServletRequest) request;
		List<MultipartFile> attachFileList = mRequest.getFiles("uploadFile");
		String uploadPath = ROOT_PATH + RES_PATH + BOARD_PATH + "/" + boardSeq;
		String filePath = BOARD_PATH + "/" + boardSeq;
		for (MultipartFile attachFile : attachFileList) {
			HashMap<String, Object> boardAttachParam = new HashMap<String, Object>();
			String realName = FileUploader.uploadFile(uploadPath, attachFile);
			if (realName == null || realName.equals("not")) {
				throw new LogicException("FILE_UPLOAD_FAIL", "003");
			}
			boardAttachParam.put("board_seq", boardSeq);
			boardAttachParam.put("file_path", filePath+"/"+realName);
			boardAttachParam.put("file_name", attachFile.getOriginalFilename());
			boardAttachParam.put("attach_type", Util.getFileType(realName));
			boardAttachParam.put("s_user_seq", param.get("s_user_seq"));
			if (dao.insertBoardAttach(boardAttachParam) < 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ board_attach insert fail ]", "004");
			}
		}
		return resultMap;
	}

	public List<HashMap<String, Object>> getCpxCode() throws Exception {
		List<HashMap<String, Object>> codeList = new ArrayList<HashMap<String, Object>>();
		codeList = dao.getCpxCode();
		return codeList;
	}

	public List<HashMap<String, Object>> getDiaCode() throws Exception {
		List<HashMap<String, Object>> codeList = new ArrayList<HashMap<String, Object>>();
		codeList = dao.getDiaCode();
		return codeList;
	}

	public List<HashMap<String, Object>> getUnitCode() throws Exception {
		List<HashMap<String, Object>> codeList = new ArrayList<HashMap<String, Object>>();
		codeList = dao.getUnitCode();
		return codeList;
	}

	public List<HashMap<String, Object>> getOsceCode() throws Exception {
		List<HashMap<String, Object>> codeList = new ArrayList<HashMap<String, Object>>();
		codeList = dao.getOsceCode();
		return codeList;
	}

	public ResultMap getSTAssignCurrList(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("currList", dao.getSTAssignCurrList(param));
		
		return resultMap;
	}
	
	
	/* 임상표현(CPX) 임상술기(OSCE) 진단명(DIA) */
	public String getCodeName(String codeCate) throws Exception {
		String codeName = "";
		List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		list = getCodeList(codeCate);
		if (list != null && !list.isEmpty()) {
			HashMap<String, Object> map = new HashMap<String, Object>(); 
			map = list.get(0);
			if (map != null && !map.isEmpty()) {
				codeName = String.valueOf(map.get("code_name"));
			}
		}
		return codeName;
	}
	
	public ResultMap getCurrFlagInfo(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("currFlagInfo", dao.getCurrFlagInfo(param));
		
		return resultMap;
	}

	public ResultMap getQnaCnt(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("qnaCnt", dao.getAdminQnaCount());
		resultMap.put("acaScheduleCnt", dao.getAdminAcaScheduleCount(param));
		
		return resultMap;
	}

	public ResultMap getAcademicStageInfo(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		resultMap.put("acaList", dao.getAcademicStageInfo(param));
		
		return resultMap;
	}
}
