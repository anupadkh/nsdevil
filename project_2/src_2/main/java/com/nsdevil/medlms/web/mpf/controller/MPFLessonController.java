package com.nsdevil.medlms.web.mpf.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.admin.service.AcademicService;
import com.nsdevil.medlms.web.mpf.service.MPFLessonService;

@Controller
public class MPFLessonController extends ExceptionController {
	
	@Autowired
	private MPFLessonService mpfLessonService;

	@Autowired
	private AcademicService academicService;
	
	//Dosen Penanggung jawab 리스트 가져오기
	@RequestMapping(value = "/ajax/mpf/list", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String getUserInfo(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception{
		if(!param.get("pf_seq").toString().equals(""))
			param.put("pf_seq", param.get("pf_seq").toString());

		List<HashMap<String, Object>> map = mpfLessonService.getprofessor(param);		

		model.addAttribute("pf_list", map);
		return JSON_VIEW;
	}	
		
	
	//waktu표manajemen
	
	
	/*********************************************************************************************************************************
	 
	 unit menejemen
	 
	 *********************************************************************************************************************************/
	
	
	//성적manajemen
	
	//수업계획registrasi현황
	@RequestMapping(value = "/pf/lesson/lessonPlanRegCs", method = RequestMethod.GET)
	public String lessonPlanRegCsView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("code_cate", "osce_code_name");
		model.addAttribute("code1", academicService.getCodeMenuName(param));
		param.put("code_cate", "cpx_code_name");
		model.addAttribute("code2", academicService.getCodeMenuName(param));
		param.put("code_cate", "dia_code_name");
		model.addAttribute("code3", academicService.getCodeMenuName(param));
		return "mpf/lesson/lessonPlanRegCs";
	}
	
	@RequestMapping(value = "/ajax/pf/lesson/lessonPlanRegCs/list", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String lessonPlanRegCsList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception{				
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		model.addAllAttributes(mpfLessonService.getLessonPlanRegCs(param));
		return JSON_VIEW;
	}	
	
	//수업계획서 정보 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/lessonPlanRegCs/lessonPlanInfo", method = RequestMethod.POST)
	public String lessonPlanBasicInfo(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {	
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		
		Util.requiredCheck(param, new String[] {"lp_seq","curr_seq"});
		model.addAllAttributes(mpfLessonService.getLessonPlan(param));
		return JSON_VIEW;
	}
	
	//결석자 리스트 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/lessonPlanRegCs/absenceSTList", method = RequestMethod.POST)
	public String absenceSTList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {	
		
		
		Util.requiredCheck(param, new String[] {"lp_seq"});
		model.addAllAttributes(mpfLessonService.getAbsenceSTList(param));
		return JSON_VIEW;
	}
	
	//운영보고서
}
