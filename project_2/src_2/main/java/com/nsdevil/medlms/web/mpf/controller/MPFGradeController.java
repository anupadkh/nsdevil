package com.nsdevil.medlms.web.mpf.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.admin.service.AcademicService;
import com.nsdevil.medlms.web.common.service.CommonService;
import com.nsdevil.medlms.web.mpf.service.MPFGradeService;

@Controller
public class MPFGradeController extends ExceptionController {

	@Autowired
	private MPFGradeService mpGradeService;

	@Autowired
	private CommonService commonService;

	@Autowired
	private AcademicService academicService;

	
	@RequestMapping(value = "/pf/lesson/grade", method = RequestMethod.GET)
	public String gradeView(HttpServletRequest request, Model model) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		model.addAllAttributes(commonService.getAcaState(param));	
		model.addAllAttributes(commonService.getCurrFlagInfo(param));	
		return "mpf/lesson/grade";
	}
	
	@RequestMapping(value = "/ajax/pf/lesson/grade/getStList", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String getStList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		//param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		model.addAllAttributes(mpGradeService.getStList(param));
		model.addAllAttributes(academicService.getGradeCodeList());
		return JSON_VIEW;
	}
	
	@RequestMapping(value = "/ajax/pf/lesson/grade/getStList/insert", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String GradeInsert(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		Util.requiredCheck(param, new String[] {"reg_user_seq", "curr_seq"});
		model.addAllAttributes(mpGradeService.insertGrade(param));
		return JSON_VIEW;
	}	

	@RequestMapping(value = "/ajax/pf/lesson/grade/getGradeStList", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String getGradeStList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(mpGradeService.gradeStList(param));
		return JSON_VIEW;
	}

	@RequestMapping(value = "/ajax/pf/lesson/grade/excel/create", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String gradeExcelUpload(MultipartHttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		Util.requiredCheck(param, new String[] {"curr_seq"});

		MultipartFile uploadFile = request.getFile("xlsFile");
		
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(mpGradeService.gradeExcelCreate(uploadFile, param));
		
		return JSON_VIEW;
	}
	
	@RequestMapping(value = "/ajax/pf/lesson/grade/getStList/confirm", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String GradeConfirm(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		Util.requiredCheck(param, new String[] {"reg_user_seq", "curr_seq"});
		model.addAllAttributes(mpGradeService.confirmGrade(param));
		return JSON_VIEW;
	}	
	
	
	@RequestMapping(value = "/ajax/pf/lesson/grade/getStList/cancelConfirm", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public String GradeCancelConfirm(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ").toString());
		Util.requiredCheck(param, new String[] {"reg_user_seq", "curr_seq"});
		model.addAllAttributes(mpGradeService.confirmCancelGrade(param));
		return JSON_VIEW;
	}	
	/**
	 * menejemen kurikulum akadmik 엑셀 출력
	 */
	@RequestMapping(value = "/pf/lesson/grade/excelTmplDown", method = RequestMethod.GET)
	public String gradeExcelTmplDown(HttpServletRequest req, Model model,@RequestParam HashMap<String, Object> param, HttpServletResponse response)  throws Exception {
		
		//엑셀 제목 크기
		int[] sheetWidth = {1500, 4000, 3000, 3500, 3500, 3500, 3500};
		
		//제목
		String[] sheetTitle = {"No","학번","이름","(pilih) 확정등급","과목명입력 (우측으로 계속입력)","과목1","과목2","과목 미입력시 해당 열 점수 simpan 안됩니다."};
		
		String curDate= (new SimpleDateFormat("yyyy-MM-dd")).format( new Date() );
		
		model.addAttribute("excel_view_type", "gradeXlsxTmplDown");
		model.addAttribute("filename","gradeXlsxTmplDown_"+curDate);
		
		model.addAttribute("sheetName","sheet1");
		model.addAttribute("sheetTitle", sheetTitle);
		model.addAttribute("sheetWidth",sheetWidth);
		model.addAttribute("stList", mpGradeService.getCurrStList(param));

		response.setHeader("Content-disposition", "attachment; filename=" + "gradeXlsxTmplDown_"+curDate+".xlsx"); 
		
		return "excelDownloadView";
	}
	
	/**
	 * menejemen kurikulum akadmik 엑셀 출력
	 */
	@RequestMapping(value = "/pf/lesson/grade/excelDown", method = RequestMethod.GET)
	public String gradeExcelDown(HttpServletRequest req, Model model,@RequestParam HashMap<String, Object> param, HttpServletResponse response)  throws Exception {

		//엑셀 제목 크기
		int[] sheetWidth = {1500, 4000, 3000, 3500};
		
		ResultMap resultMap = mpGradeService.getStList(param);
		
		List<String> titleList = new ArrayList<String>();
		titleList.add("No");
		titleList.add("학번");
		titleList.add("학생");
		titleList.add("조");
		
		for(HashMap<String, Object> map : (List<HashMap<String,Object>>) resultMap.get("subjectList")) {
			titleList.add(map.get("subject").toString());
		}
		titleList.add("총점");
		titleList.add("석차");
		titleList.add("확정등급");
		titleList.add("변환점수");
		//제목
		String[] sheetTitle = titleList.toArray(new String[titleList.size()]);
		
		String curDate= (new SimpleDateFormat("yyyy-MM-dd")).format( new Date() );
		
		model.addAttribute("excel_view_type", "gradeXlsxDown");
		model.addAttribute("filename","gradeXlsxDown_"+curDate);
		
		model.addAttribute("sheetName","sheet1");
		model.addAttribute("sheetTitle", sheetTitle);
		model.addAttribute("sheetWidth",sheetWidth);
		model.addAttribute("stList", resultMap.get("stList"));

		response.setHeader("Content-disposition", "attachment; filename=" + "gradeXlsxDown_"+curDate+".xlsx"); 
		
		return "excelDownloadView";
	}
	
	@RequestMapping(value = "/pf/lesson/soosiGrade", method = RequestMethod.GET)
	public String soosiGradeView(HttpServletRequest request, Model model) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("curr_seq", request.getSession().getAttribute("S_CURRICULUM_SEQ").toString());
		model.addAllAttributes(commonService.getAcaState(param));
		model.addAttribute("soosiList", mpGradeService.getSoosiList(param));
		return "mpf/lesson/soosiGrade";
	}

	//nilai yang diinginkan 리스트가져오기
	@RequestMapping(value = "/ajax/pf/lesson/soosiGrade/create/gradelist", method = RequestMethod.POST)
	public String sooSiScoreGradeList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		Util.requiredCheck(param, new String[] {"curr_seq"});
		
		model.addAttribute("soosiList", mpGradeService.getSoosiList(param));

		return JSON_VIEW;
	}	
	
	//nilai yang diinginkan 가져오기
	@RequestMapping(value = "/ajax/pf/lesson/soosiGrade/create/list", method = RequestMethod.POST)
	public String sooSiScoreList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {		
		Util.requiredCheck(param, new String[] {"curr_seq"});
		
		model.addAllAttributes(academicService.getSooSiScore(param));

		return JSON_VIEW;
	}	
	
	//nilai yang diinginkan 양식 엑셀 다운로드
	@RequestMapping(value = "/pf/lesson/soosiGrade/TmplExcelDown", method = RequestMethod.GET)
	public String TmplExcelDown(HttpServletRequest request,  Model model
			, @RequestParam HashMap<String, Object> param
			, HttpServletResponse response) throws Exception {	
		//엑셀 제목 크기
		int[] sheetWidth = {3000, 5000, 4000, 4000, 4000};

		String curDate= (new SimpleDateFormat("yyyy-MM-dd")).format( new Date() );
		
		model.addAttribute("excel_view_type", "currScoreTmplDown");
		model.addAttribute("filename","currScoreTmplDown_"+curDate);
				
		model.addAttribute("sheetName","sheet1");
		model.addAttribute("sheetWidth",sheetWidth);
		model.addAttribute("stList", academicService.getGradeStList(param));

		response.setHeader("Content-disposition", "attachment; filename=" + "currScoreTmplDown_"+curDate+".xlsx"); 
		
		return "excelDownloadView";
	}	
}
