package com.nsdevil.medlms.web.common.controller;


import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.mobileweb.student.service.MobileStudentService;
import com.nsdevil.medlms.web.common.service.CommonService;
import com.nsdevil.medlms.web.common.service.MyService;
import com.nsdevil.medlms.web.pf.service.PFLessonService;

@Controller
public class MyController extends ExceptionController {
	
	@Autowired
	private MyService myService;
	
	@Autowired
	private CommonService commonService;

	@Autowired
	private PFLessonService pfLessonservice;
	
	@Autowired
	private MobileStudentService mobileStudentService;
	
	//프로필manajemen 상세 화면
	@RequestMapping(value = {"/common/my", "/common/my/profile/detail"}, method = RequestMethod.GET)
	public String profileDetailView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		model.addAllAttributes(myService.getProfileDetail(param));
		return "common/my/profile/profileDetail";
	}
	
	//프로필manajemen 정보 가져오기
	@RequestMapping(value = {"/ajax/common/my/profile/detail"}, method = RequestMethod.GET)
	public String getProfileDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(myService.getProfileDetail(param));
		return JSON_VIEW;
	}
	
	//프로필manajemen perbaiki 화면
	@RequestMapping(value = "/common/my/profile/modify", method = RequestMethod.GET)
	public String profileModifyView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(myService.getProfileDetail(param));
		model.addAttribute("emailCodeList", commonService.getCodeList("email"));
		model.addAttribute("telCodeList", commonService.getCodeList("tel"));
		return "common/my/profile/profileModify";
	}
	
	//프로필manajemen perbaiki
	@RequestMapping(value = "/ajax/common/my/profile/modify", method = RequestMethod.POST)
	public String profileModify(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[]{"name", "tel", "email"});
		param.put("upt_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(myService.profileModify(request, param));
		return JSON_VIEW;
	}
	
	//나의 lecture 자료
	@RequestMapping(value = "/common/my/lessonData", method = RequestMethod.GET)
	public String myLessonDataView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAttribute("yearList", pfLessonservice.getAcademicYear(param));
		return "common/my/profile/myLessonData";
	}
	
	//lecture자료 리스트
	@RequestMapping(value = "/ajax/common/my/lessonData/list", method = RequestMethod.POST)
	public String lessonDataList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(myService.getMyLessonData(param));
		return JSON_VIEW;
	}
	
	//형성penilaian
	@RequestMapping(value = "/common/my/feScore", method = RequestMethod.GET)
	public String myFeScoreView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		HashMap<String, Object> map = myService.getAcaInfo(param);
		if(map != null) {
			model.addAttribute("aca_name", map.get("aca_name").toString());
			model.addAttribute("aca_seq", map.get("aca_seq").toString());
			model.addAttribute("aca_state", map.get("aca_state").toString());
		}
		return "common/my/st/feScore";
	}
	
	//형성penilaian 점수 가져오기
	@RequestMapping(value = "/ajax/common/my/feScore/list", method = RequestMethod.POST)
	public String getFeScoreList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(myService.getFeScore(param));
		return JSON_VIEW;
	}
	
	//종합성적
	@RequestMapping(value = "/common/my/currGrade", method = RequestMethod.GET)
	public String myCurrGradeView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		HashMap<String, Object> map = myService.getAcaInfo(param);
		if(map != null) {
			model.addAttribute("aca_name", map.get("aca_name").toString());
			model.addAttribute("aca_seq", map.get("aca_seq").toString());
			model.addAttribute("aca_state", map.get("aca_state").toString());
		}
		return "common/my/st/currGrade";
	}
	
	//종합성적 점수 가져오기
	@RequestMapping(value = "/ajax/common/my/currGrade/list", method = RequestMethod.POST)
	public String getCurrGradeList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(myService.getCurrGrade(param));
		return JSON_VIEW;
	}	
	
	//nilai yang diinginkan
	@RequestMapping(value = "/common/my/soosiGrade", method = RequestMethod.GET)
	public String mySoosiGradeView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		HashMap<String, Object> map = myService.getAcaInfo(param);
		if(map != null) {
			model.addAttribute("aca_name", map.get("aca_name").toString());
			model.addAttribute("aca_seq", map.get("aca_seq").toString());
			model.addAttribute("aca_state", map.get("aca_state").toString());
		}
		return "common/my/st/soosiGrade";
	}
	
	//종합성적 점수 가져오기
	@RequestMapping(value = "/ajax/common/my/soosiGrade/list", method = RequestMethod.POST)
	public String getSoosiGradeList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(myService.getSoosiGrade(param));
		return JSON_VIEW;
	}	
	
	//과제 조회
	@RequestMapping(value = "/common/my/asgmt/list", method = RequestMethod.GET)
	public String myAsgmtView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		HashMap<String, Object> map = myService.getAcaInfo(param);
		if(map != null) {
			model.addAttribute("aca_name", map.get("aca_name").toString());
			model.addAttribute("aca_seq", map.get("aca_seq").toString());
			model.addAttribute("aca_state", map.get("aca_state").toString());
		}
		return "common/my/st/assignMentView";
	}
	
	//수업만족도 조사 리스트 화면
	@RequestMapping(value = "/common/my/sfResearch/list", method = RequestMethod.POST)
	public String sfResearchListView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAttribute("type", param.get("type").toString());
		return "common/my/st/satisfactionResearchList";
	}
	
	//수업만족도 설문 하기
	@RequestMapping(value = "/common/my/sfResearch/create", method = RequestMethod.POST)
	public String sfResearchListCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(mobileStudentService.getLessonPlanTitleInfo(param));	
		model.addAttribute("lp_seq", param.get("s_lp_seq").toString());	
		model.addAttribute("curr_seq", param.get("curr_seq").toString());
		model.addAttribute("sr_type", param.get("sr_type").toString());
		return "common/my/st/satisfactionResearchCreate";
	}
	
	
	//학생 메인화면 수업만족도조사, 과정만족도조사, 과제, 성적
	@RequestMapping(value = "/ajax/common/my/news/list", method = RequestMethod.GET)
	public String getMyNewsList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(myService.getMyNewsList(param));
		return JSON_VIEW;
	}
}
