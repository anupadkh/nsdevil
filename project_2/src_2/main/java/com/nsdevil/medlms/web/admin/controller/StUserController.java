package com.nsdevil.medlms.web.admin.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.admin.service.AcademicService;
import com.nsdevil.medlms.web.admin.service.StUserService;
import com.nsdevil.medlms.web.common.service.CommonService;

@Controller
public class StUserController extends ExceptionController {
	
	@Autowired
	private StUserService stUserService;
	
	@Autowired
	private AcademicService academicService;
	
	@Autowired
	private CommonService commonService;
		
	//학생 리스트 화면
	@RequestMapping(value = "/admin/user/st/list", method = RequestMethod.GET)
	public String stListView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAttribute("st_attend_code", commonService.getCodeList("st_attend_code")); //재학상태
		model.addAllAttributes(academicService.getAcademicYearList());
		param.put("level", 1);		
		model.addAttribute("lseqList", academicService.academicSystemSategeOfList(param));
		model.addAllAttributes(param);
		return "admin/user/st/stList";
	}	
	
	//학생 리스트 가져오기
	@RequestMapping(value = "/ajax/admin/user/st/list", method = RequestMethod.POST)
	public String getStList (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(stUserService.getStList(param));
		return JSON_VIEW;
	}
	
	//학생 registrasi 화면
	@RequestMapping(value = "/admin/user/st/create", method = RequestMethod.GET)
	public String stCreateView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(academicService.getAcademicYearList());
		param.put("level", 1);		
		model.addAttribute("lseqList", academicService.academicSystemSategeOfList(param));
		
		return "admin/user/st/stCreate";
	}	
	
	//학생 registrasi
	@RequestMapping(value = "/ajax/admin/user/st/create", method = RequestMethod.POST)
	public String stCreate (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"user_name", "email", "user_id"});
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(stUserService.stCreate(request, param));
		return JSON_VIEW;
	}

	@RequestMapping(value = "/ajax/admin/user/st/excel/create", method = RequestMethod.POST)
	public String stExcelCreate (MultipartHttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"xlsFile"});
		param.put("user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(stUserService.stExcelCreate(request, param));
		return JSON_VIEW;
	}
	
	//학생 한명 조회 화면
	@RequestMapping(value = "/admin/user/st/detail", method = RequestMethod.POST)
	public String stDetailView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(academicService.getAcademicYearList());
		param.put("level", 1);		
		model.addAttribute("lseqList", academicService.academicSystemSategeOfList(param));
		model.addAttribute("st_attend_code", commonService.getCodeList("st_attend_code")); //재학상태
		if(request.getParameter("user_seq") != null) {
			model.addAttribute("user_seq", request.getParameter("user_seq").toString());
		}
		return "admin/user/st/stDetail";
	}	

	//학생 상세
	@RequestMapping(value = "/ajax/admin/user/st/info", method = RequestMethod.POST)
	public String stInfo (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"user_seq"});
		model.addAllAttributes(stUserService.getStInfo(param));
		return JSON_VIEW;
	}
	
	//학생 perbaiki 화면
	@RequestMapping(value = "/admin/user/st/modify", method = RequestMethod.POST)
	public String stModifyView (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(academicService.getAcademicYearList());
		param.put("level", 1);		
		model.addAttribute("lseqList", academicService.academicSystemSategeOfList(param));
		model.addAttribute("st_attend_code", commonService.getCodeList("st_attend_code")); //재학상태
		model.addAttribute("user_seq", request.getParameter("user_seq").toString());
		return "admin/user/st/stModify";
	}
	
	//학생 registrasi
	@RequestMapping(value = "/ajax/admin/user/st/modify", method = RequestMethod.POST)
	public String stModify (HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[] {"st_name", "email"});
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(stUserService.stModify(request, param));
		return JSON_VIEW;
	}
}
