package com.nsdevil.medlms.web.pf.dao;

import java.util.HashMap;
import java.util.List;


public interface PFLessonDao {
	public List<HashMap<String, Object>> getLeftMenuAcademicYearList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getLeftMenuCurriculumList(HashMap<String, Object> param) throws Exception;
	public int getCurriculumCount(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getLessonPlanList(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getCurriculumBasicInfo(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getFormationEvaluationCs(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getSummativeEvaluation(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getGraduationCapabilityListMaxVersion(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getMpCurriculumGraduationCapabilityList(HashMap<String, Object> param) throws Exception;
	public int updateCurriculum(HashMap<String, Object> param) throws Exception;
	public int insertSummativeEvaluation(HashMap<String, Object> param) throws Exception;
	public int insertMpCurriculumFinishCapability(HashMap<String, Object> param) throws Exception;
	public void updateSummativeEvaluation(HashMap<String, Object> param) throws Exception;
	public void updateMpCurriculumFinishCapability(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getLessonMethodList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getLessonMethodSuject(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getLessonPlanBasicInfo(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getCurrFinishCapability(HashMap<String, Object> param) throws Exception;
	public void updateLessonAllTable(HashMap<String, Object> param) throws Exception;
	public int updateLessonPlan(HashMap<String, Object> param) throws Exception;
	public int insertLessonClinic(HashMap<String, Object> param) throws Exception;
	public int insertLessonCoreClinic(HashMap<String, Object> param) throws Exception;
	public int insertLessonDiagnosis(HashMap<String, Object> param) throws Exception;
	public int insertLessonFinishCapability(HashMap<String, Object> param) throws Exception;
	public int insertFormationEvaluation(HashMap<String, Object> param) throws Exception;
	public int insertLessonMethod(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getLessonCoreClinicList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getLessonClinicList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getLessonDiagnosisList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getLessonFinishCapabilityList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> formationEvaluationCodeList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getPreLessonPlan(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getSummativeEvaluationCode(HashMap<String, Object> param);
	public String getPfLevelCheck(HashMap<String, Object> param);
	public List<HashMap<String, Object>> getGradeStandardList(HashMap<String, Object> param);
	public void updateGradeStandard(HashMap<String, Object> param);
	public void insertGradeStandard(HashMap<String, Object> param);
	public int insertLessonData(HashMap<String, Object> param);
	public int updateLessonData(HashMap<String, Object> param);
	public void updateLessonAttachUseFlag(HashMap<String, Object> param);
	public int updateLessonAttachUseFlagY(HashMap<String, Object> param);
	public int insertLessonAttach(HashMap<String, Object> param);
	public int getLessonDataSeq(HashMap<String, Object> param);
	public HashMap<String, Object> getLessonData(HashMap<String, Object> param);
	public List<HashMap<String, Object>> getLessonAttach(HashMap<String, Object> param);
	public int getLessonDataCount(HashMap<String, Object> param);
	public int getLessonPlanListCount(HashMap<String, Object> param) throws Exception;
	public void insertCurrLessonMethod(HashMap<String, Object> param) throws Exception;
	public void insertCurrFE(HashMap<String, Object> param) throws Exception;
	public int getLessonAttachGroupNum(HashMap<String, Object> param) throws Exception;
	public void insertComment(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getLessonPlanPeriod(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getLessonPlanLessonMethodCnt(HashMap<String, Object> param) throws Exception;
	public int getCurrFinishCapabilityCount(HashMap<String, Object> param) throws Exception;
	public int getDiaCodeListCount(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getDiaCodePagingList(HashMap<String, Object> pagingParam) throws Exception;
	public int insertLessonSkill(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getLessonOsceList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getLpAllAssignMentList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getLessonPlanFeCount(HashMap<String, Object> param) throws Exception;
	public int getPreCurrCount(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getPreCurrInfo(HashMap<String, Object> param) throws Exception;
	public int getTodayLessonPeriodSeq(HashMap<String, Object> param) throws Exception;
	
	/* 형성penilaian */
	public HashMap<String, Object> getFormationEvaluationTitleFull(HashMap<String, Object> param) throws Exception;;
	public List<HashMap<String, Object>> getFormationEvaluationList(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getFormationEvaluationInfo(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getQuizList(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getQuizInfo(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getQuizAttachInfo(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getQuizAnswerList(HashMap<String, Object> param) throws Exception;
	public int updateFormationEvaluationTitle(HashMap<String, Object> param) throws Exception;
	public int insertQuiz(HashMap<String, Object> param) throws Exception;
	public int insertQuizAnswer(HashMap<String, Object> param) throws Exception;
	public int insertQuizAttach(HashMap<String, Object> param) throws Exception;
	public int updateQuizOrder(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getPreOrNextQuizSeq(HashMap<String, Object> param) throws Exception;
	public int deleteQuiz(HashMap<String, Object> param) throws Exception;
	public int deleteQuizAnswer(HashMap<String, Object> param) throws Exception;
	public int deleteQuizAttach(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getQuizPreviewInfo(HashMap<String, Object> param) throws Exception;
	public int updateQuizOrderSort(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getFormationEvaluationTitle(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getQuizTitle(HashMap<String, Object> param) throws Exception;
	public int updateFormationEvaluationThumnail(HashMap<String, Object> param) throws Exception;
	public int updateQuiz(HashMap<String, Object> param) throws Exception;
	public int deleteQuizAttachSeq(HashMap<String, Object> param) throws Exception;
	public int deleteFormationEvaluation(HashMap<String, Object> param) throws Exception;
	public int deleteQuizAnswerOfFeSeq(HashMap<String, Object> param) throws Exception;
	public int deleteQuizAttachOfFeSeq(HashMap<String, Object> param) throws Exception;
	public int deleteQuizOfFeSeq(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getYearAcademicList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getPastFormationEvaluationList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getPastQuizList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getPastQuizAnswerList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getPastQuizAttachList(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getFormationEvaluation(HashMap<String, Object> param) throws Exception;
	public int updateFormationEvaluation(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getFormationEvaluationResultExcelList(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getFormationEvaluationResultList(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getFormationEvaluationResultInfo(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getProfessorInfo(HashMap<String, Object> param) throws Exception;
	
	/* 형성penilaian */
	
	
	/* 과제 */
	public int insertAssignMent(HashMap<String, Object> param) throws Exception;
	public int updateAssignMent(HashMap<String, Object> param) throws Exception;
	public void updateAssignMentUseFlag(HashMap<String, Object> param) throws Exception;
	public int updateAssignMentAttachUseFlagY(HashMap<String, Object> param) throws Exception;
	public int insertAssignmentAttach(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getAssignMent(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getAssignMentAttach(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getAssignMentList(HashMap<String, Object> param) throws Exception;
	public int getAssignMentCount(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getAssignMentSubmitList(HashMap<String, Object> param) throws Exception;
	public int insertAssignMentFeedBack(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getAssignMentDetail(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getCurrAssignMentSubmitList(HashMap<String, Object> param) throws Exception;
	public int getAssignMentFeedBackChk(HashMap<String, Object> param) throws Exception;
	public void deleteAssignMent(HashMap<String, Object> param) throws Exception;
	
	/* absensi kehadiran */
	public List<HashMap<String, Object>> getAttendanceList(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> gatAttendanceCount(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getAssignMentFileList(HashMap<String, Object> param) throws Exception;
	public int getAssignMentSeq(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> formationEvaluationState(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> lessonMethodCodeList(HashMap<String, Object> param) throws Exception;
	
	/* 보고서 */
	public List<HashMap<String, Object>> getReportCurrGraph(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getReportFe(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getReportGrade(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getReportFc(HashMap<String, Object> param) throws Exception;
	public HashMap<String, Object> getReportCurrProgress(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getReportClinic(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getReportDia(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getReportFcLesson(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getReportClinicLesson(HashMap<String, Object> param) throws Exception;
	public List<HashMap<String, Object>> getReportDiaLesson(HashMap<String, Object> param) throws Exception;
}
