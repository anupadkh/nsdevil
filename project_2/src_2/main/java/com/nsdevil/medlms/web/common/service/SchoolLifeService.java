package com.nsdevil.medlms.web.common.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.nsdevil.medlms.common.bean.ResultMap;
import com.nsdevil.medlms.common.constants.Constants;
import com.nsdevil.medlms.common.exception.ResourceNotFoundException;
import com.nsdevil.medlms.common.exception.RuntimeLogicException;
import com.nsdevil.medlms.common.util.PagingHelper;
import com.nsdevil.medlms.web.common.dao.CommonDao;
import com.nsdevil.medlms.web.common.dao.SchoolLifeDao;

@Service
public class SchoolLifeService {
	
	@Value("#{config['config.resPath']}")
	private String RES_PATH;

	@Autowired
	private SchoolLifeDao dao;
	
	@Autowired
	private CommonDao commonDao;

	public HashMap<String, Object> getQNAList(HashMap<String, Object> param) throws Exception {
		int totalCnt = dao.getQNAContentsCount(param);
		PagingHelper pagingHelper = new PagingHelper();
		List<HashMap<String, Object>> ContentsList = dao.getQNAContentsList(pagingHelper.getPagingParam(param, totalCnt));
		return pagingHelper.getPageList(ContentsList, (String)param.get("listFunctionName"), totalCnt);
	}
	
	public ResultMap answerCreate(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		int existQuestionCount = dao.getExistQuestionCount(param);
		int existAnswerCount = dao.getExistAnswerCount(param);
		
		if (existQuestionCount < 1) { //질문을 찾을 수 없음
			throw new ResourceNotFoundException("NOT_FOUND_QNA_SEQ_INFO", "007");
		}
		
		if (existAnswerCount > 0) { //질문에  이미 답변이 달림
			throw new RuntimeLogicException("QUERY_FAIL [ qna_answer answer exist ]", "004");
		}
		
		if (existQuestionCount > 0) {
			if (dao.insertAnswer(param) != 1) {
				throw new RuntimeLogicException("QUERY_FAIL [ qna_answer insert query fail ]", "004");
			}
		} 
		
		return resultMap;
	}
	
	public ResultMap answerModify(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		if (dao.updateAnswer(param) != 1) {
			throw new RuntimeLogicException("QUERY_FAIL [ qna_answer update query fail ]", "004");
		}
		return resultMap;
	}
	
	public ResultMap answerRemove(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		if (dao.deleteAnswer(param) != 1) {
			throw new RuntimeLogicException("QUERY_FAIL [ qna_answer update query fail ]", "004");
		}
		return resultMap;
	}
	
	public ResultMap questionCreate(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		//manajemen자일경우
		param.put("answer_user_seq", 2);
		param.put("answer_user_level", null);
		
		//교수일경우
		String answerUserSeq = String.valueOf(param.get("user_seq"));
		if (answerUserSeq != null && !answerUserSeq.equals("")) {
			HashMap<String, Object> userInfo = new HashMap<String, Object>();
			userInfo = dao.getAnswerUserLevel(param);
			if (userInfo != null && !userInfo.isEmpty()) {
				param.put("answer_user_seq", param.get("user_seq"));
				param.put("answer_user_level", userInfo.get("user_level"));
			}
		}
		
		if (dao.insertQuestion(param) != 1) {
			throw new RuntimeLogicException("QUERY_FAIL [ qna insert query fail ]", "004");
		}
		
		return resultMap;
	}
	
	public ResultMap getNoticeList(HashMap<String, Object> param) throws Exception {
		
		ResultMap resultMap = new ResultMap();
		
		HashMap<String, Object> academicSystemInfo = new HashMap<String, Object>();
		if (param.get("s_user_level") != null ) {
			int sUserLevel = (int) param.get("s_user_level");
			if (sUserLevel == Constants.STUDENT_LEVEL) {
				academicSystemInfo = commonDao.getAcademicSystemInfo(param);
			}
			
			if (academicSystemInfo != null && !academicSystemInfo.isEmpty()) {
				param.put("l_seq", academicSystemInfo.get("l_seq"));
				param.put("m_seq", academicSystemInfo.get("m_seq"));
				param.put("s_seq", academicSystemInfo.get("s_seq"));
				
				if (academicSystemInfo.get("s_seq") == null) {
					param.put("s_seq", academicSystemInfo.get("ss_seq"));
				}
			}
		}
		
		int totalCnt = dao.getNoticeContentsCount(param);
		PagingHelper pagingHelper = new PagingHelper();
		List<HashMap<String, Object>> ContentsList = dao.getNoticeContentsList(pagingHelper.getPagingParam(param, totalCnt));
		resultMap.putAll(pagingHelper.getPageList(ContentsList, (String)param.get("listFunctionName"), totalCnt));
		return resultMap;
	}
	
	public ResultMap getNoticeDetail(HashMap<String, Object> param) throws Exception {
		ResultMap resultMap = new ResultMap();
		HashMap<String, Object> academicSystemInfo = new HashMap<String, Object>();
		if (param.get("s_user_level") != null ) {
			int sUserLevel = (int) param.get("s_user_level");
			if (sUserLevel == Constants.STUDENT_LEVEL) {
				academicSystemInfo = commonDao.getAcademicSystemInfo(param);
			}
			
			if (academicSystemInfo != null && !academicSystemInfo.isEmpty()) {
				param.put("l_seq", academicSystemInfo.get("l_seq"));
				param.put("m_seq", academicSystemInfo.get("m_seq"));
				param.put("s_seq", academicSystemInfo.get("s_seq"));
				
				if (academicSystemInfo.get("s_seq") == null) {
					param.put("s_seq", academicSystemInfo.get("ss_seq"));
				}
			}
		}
		HashMap<String, Object> noticeDetailInfo = new HashMap<String, Object>();
		noticeDetailInfo = dao.getNoticeDetailInfo(param);
		if (noticeDetailInfo == null || noticeDetailInfo.isEmpty()) {
			throw new ResourceNotFoundException("NOT_FOUND_NOTICE_INFO", "007");
		}
		
		
		List<HashMap<String, Object>> attachList = new ArrayList<HashMap<String, Object>>();
		attachList = commonDao.getBoardAttachFileList(param);
		for (HashMap<String, Object> map : attachList) {
			map.put("file_path", RES_PATH + map.get("file_path"));
		}
		
		HashMap<String, Object> nextNoticeInfo = new HashMap<String, Object>();
		nextNoticeInfo = dao.getNextNoticeInfo(param);
		HashMap<String, Object> prevNoticeInfo = new HashMap<String, Object>();
		prevNoticeInfo = dao.getPrevNoticeInfo(param);
		
		resultMap.put("notice_info", noticeDetailInfo);
		resultMap.put("attach_list", attachList);
		resultMap.put("next_notice_info", nextNoticeInfo);
		resultMap.put("prev_notice_info", prevNoticeInfo);
		return resultMap;
	}
	
}
