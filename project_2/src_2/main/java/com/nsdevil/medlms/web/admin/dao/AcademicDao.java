package com.nsdevil.medlms.web.admin.dao;

import java.util.HashMap;
import java.util.List;


public interface AcademicDao {
	public int insertAcademicSystem(HashMap<String, Object> param) throws Exception;
	
	public List<HashMap<String, Object>> getAcademicSystemListNoAdd(HashMap<String, Object> param) throws Exception;
	
	public List<HashMap<String, Object>> getAcamedicSystemTemplateList() throws Exception;
	
	public int updateAcademicSystemAcaName(HashMap<String, Object> param) throws Exception;

	public int updateAcademicSystemAcaOrder(HashMap<String, Object> param) throws Exception;

	public int deleteAcademicSystem(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getAcademicSystemSategeOfList(HashMap<String, Object> param) throws Exception;

	public int insertCurriculum(HashMap<String, Object> param) throws Exception;

	public int updateCurriculum(HashMap<String, Object> param) throws Exception;

	public int getCurriculumCount(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getCurriculumList(HashMap<String, Object> pagingParam) throws Exception;
	
	public int getCurriculumCodeCount(HashMap<String, Object> param) throws Exception;

	public int deleteCurriculum(String string) throws Exception;

	public int deleteMpAssignPf(HashMap<String, Object> param) throws Exception;

	public int insertMpAssignPf(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getCurriculumMpAssignPf(HashMap<String, Object> param) throws Exception;
	
	public HashMap<String, Object> getCurriculumExcelUpChk(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getAcademicList(HashMap<String, Object> param) throws Exception;

	public int getAcademicListCount(HashMap<String, Object> param) throws Exception;

	public int professorChk(HashMap<String, Object> maps) throws Exception;

	public List<HashMap<String, Object>> gradCapabilityGetVersion() throws Exception;

	public int getGraduationCapabilityCount(String version) throws Exception;

	public void insertGraduationCapability(HashMap<String, Object> maps) throws Exception;

	public List<HashMap<String, Object>> getGraduationCapabilityList(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getGraduationCapabilityLastVersion() throws Exception;

	public int getGCVersionCount() throws Exception;

	public String getGCMaxVersion() throws Exception;

	public List<HashMap<String, Object>> getAcademicYearList() throws Exception;

	public HashMap<String, Object> getAcademicStudentCount(HashMap<String, Object> param) throws Exception;

	public int checkAcademicOverlap(HashMap<String, Object> param) throws Exception;

	public int insertAcademic(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getAcademicOne(HashMap<String, Object> param) throws Exception;

	public int updateAcademic(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getAssignCurrList(HashMap<String, Object> param) throws Exception;

	public int academicConfirm(HashMap<String, Object> param) throws Exception;

	public String getAcademicState(HashMap<String, Object> param) throws Exception;

	public int curriculumAssignUpdate(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getAcademicConfirmFlag(HashMap<String, Object> param) throws Exception;

	public int academicConfirmChk(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getCurrSchdList(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getSFSurveyCS(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getCurrNameList(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getCurrLPList(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getLessonPFList(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> unsubmissionSTList(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getLessonPlanSubject(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getSRSurveyResult(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getSRSurveyInfo(HashMap<String, Object> param) throws Exception;

	public int getAcaLevel(HashMap<String, Object> param) throws Exception;

	public int academicCntChk(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getAcaStateInfo(HashMap<String, Object> param) throws Exception;

	public void deleteAcademic(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getAcaConfirmInfo(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getAssignCurrCount(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getCurriculumInfo(String string) throws Exception;

	public List<HashMap<String, Object>> getAcaCurrList(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getGradeStList(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getGradeScoreList(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getCurriculumInfo(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getAcaCurrCount(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getAcaStGradeInfo(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getAcaSooSiCurrCount(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getStCheck(HashMap<String, Object> maps) throws Exception;

	public void insertSooSiScore(HashMap<String, Object> srcMap) throws Exception;

	public void insertSooSiScoreItem(HashMap<String, Object> maps) throws Exception;

	public List<HashMap<String, Object>> getAcaSooSiScoreList(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getAcaSooSiScoreRank(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getAcaSooSiCurrList(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getCurrSooSiList(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getCurrSooSiScoreList(HashMap<String, Object> param) throws Exception;

	public int updateSooSiScore(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getAcaFeCurrList(HashMap<String, Object> param) throws Exception;

	public void insertCode(HashMap<String, Object> param) throws Exception;

	public void updateCode(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getSeCode() throws Exception;

	public void insertSeCode(HashMap<String, Object> param) throws Exception;

	public void insertLSeCode(HashMap<String, Object> param) throws Exception;

	public void updateSeCode() throws Exception;

	public List<HashMap<String, Object>> getUnitCode() throws Exception;

	public void updateUnitCode() throws Exception;

	public void insertLUnitCode(HashMap<String, Object> param) throws Exception;

	public void insertUnitCode(HashMap<String, Object> param) throws Exception;

	public int getCompleteCodeUseCnt(HashMap<String, Object> param) throws Exception;

	public void deleteCode(HashMap<String, Object> param) throws Exception;

	public int getSeCodeUseCount(HashMap<String, Object> param) throws Exception;

	public void deleteSeCode(HashMap<String, Object> param) throws Exception;

	public int getUnitCodeUseCount(HashMap<String, Object> param) throws Exception;

	public void deleteUnitCode(HashMap<String, Object> param) throws Exception;

	public void updateSooSi(HashMap<String, Object> srcMap) throws Exception;
	
	public int getCode1Count(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getCode1List(HashMap<String, Object> param) throws Exception;

	public int getCode1UseCount(HashMap<String, Object> param) throws Exception;

	public void deleteCode1(HashMap<String, Object> param) throws Exception;

	public void updateCode1(HashMap<String, Object> param) throws Exception;

	public void insertCode1(HashMap<String, Object> param) throws Exception;
	
	public int getCode2Count(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getCode2List(HashMap<String, Object> param) throws Exception;

	public int getCode2UseCount(HashMap<String, Object> param) throws Exception;

	public void deleteCode2(HashMap<String, Object> param) throws Exception;

	public void updateCode2(HashMap<String, Object> param) throws Exception;

	public void insertCode2(HashMap<String, Object> param) throws Exception;
	
	public int getCode3Count(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getCode3List(HashMap<String, Object> param) throws Exception;

	public int getCode3UseCount(HashMap<String, Object> param) throws Exception;

	public void deleteCode3(HashMap<String, Object> param) throws Exception;

	public void updateCode3(HashMap<String, Object> param) throws Exception;

	public void insertCode3(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getCodeMenuName(HashMap<String, Object> param) throws Exception;

	public void updateCodeMenuName(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getApplicationAcaList(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getApplicationCurrList(HashMap<String, Object> param) throws Exception;

	public int insertApplication(HashMap<String, Object> param) throws Exception;

	public int updateApplication(HashMap<String, Object> param) throws Exception;

	public int insertApplicationAttach(HashMap<String, Object> param) throws Exception;

	public void updateApplicationAttachYN(HashMap<String, Object> param) throws Exception;

	public int applicationCurrInfo(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getApplicationList(HashMap<String, Object> param) throws Exception;

	public int getApplicationCount(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getApplicationAttach(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getApplicationStList(HashMap<String, Object> pagingParam) throws Exception;

	public int getApplicationStCount(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getApplicationStCntInfo(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getCode1ExcelList() throws Exception;

	public List<HashMap<String, Object>> getCode2ExcelList() throws Exception;

	public List<HashMap<String, Object>> getCode3ExcelList() throws Exception;

	public List<HashMap<String, Object>> getGradeStandardList() throws Exception;

	public void insertGradeStandard(HashMap<String, Object> param) throws Exception;

	public void deleteGradeStandard(HashMap<String, Object> param) throws Exception;

	public int getRefundCnt() throws Exception;

	public HashMap<String, Object> getRefundInfo() throws Exception;

	public int insertRefund(HashMap<String, Object> param) throws Exception;

	public int getNotesCnt() throws Exception;
 
	public HashMap<String, Object> getNotesInfo() throws Exception;

	public int insertNotes(HashMap<String, Object> param) throws Exception;

	public HashMap<String, String> getCasState(HashMap<String, Object> param) throws Exception;

	public int updateCasState(HashMap<String, Object> param) throws Exception;

	public int insertMpCurriculumSt(HashMap<String, Object> param) throws Exception;

	public int updateMpCurriculumSt(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getAcaStudentList(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getAcaAssignStudentList(HashMap<String, Object> param) throws Exception;

	public int getAcaAssignStudentCount(HashMap<String, Object> param) throws Exception;

	public int insertAcaAssignSt(HashMap<String, Object> param) throws Exception;

	public int updateAcaAssignSt(HashMap<String, Object> param) throws Exception;

	public int confirmAcaAssignSt(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> confirmCancelAcaStList(HashMap<String, Object> param) throws Exception;

	public int confirmCancelUsersAcaUpdate(HashMap<String, Object> param) throws Exception;

	public int confirmCancelAcaAssignSt(HashMap<String, Object> param) throws Exception;

	public HashMap<String, Object> getAcaAssignStudentAttendInfo(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getSRSurveyCurrResult(HashMap<String, Object> param) throws Exception;
	
	public HashMap<String, Object> getCurrSRSurveyInfo(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> unsubmissionCurrSTList(HashMap<String, Object> param) throws Exception;

	public int confirmUsersMcsUpdate(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getCurrNotConfirmList(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getCurrGradeStScore(HashMap<String, Object> param) throws Exception;

	public List<HashMap<String, Object>> getSoosiGradeStScore(HashMap<String, Object> param) throws Exception;
}
