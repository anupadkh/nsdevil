package com.nsdevil.medlms.web.common.controller;


import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nsdevil.medlms.common.constants.Constants;
import com.nsdevil.medlms.common.controller.ExceptionController;
import com.nsdevil.medlms.common.util.Util;
import com.nsdevil.medlms.web.common.service.CommonService;
import com.nsdevil.medlms.web.common.service.SchoolLifeService;

@Controller
public class SchoolLifeController extends ExceptionController {
	
	@Value("#{config['config.rootPath']}")
	private String ROOT_PATH;
	
	@Value("#{config['config.resPath']}")
	private String RES_PATH;
	
	@Autowired
	private SchoolLifeService service;
	
	@Autowired
	private CommonService commonService;

	@RequestMapping(value = "/common/SLife", method = RequestMethod.GET)
	public String slifeView() throws Exception {
		return "redirect:/common/SLife/hotnews/list";
	}
	
	//공지사항 목록화면
	@RequestMapping(value = "/common/SLife/notice/list", method = RequestMethod.GET)
	public String noticeListView() throws Exception {
		return "common/SLife/notice/noticeList";
	}
	
	//공지사항 목록
	@RequestMapping(value = "/ajax/common/SLife/notice/list", method = RequestMethod.GET)
	public String noticeList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.NOTICE_BOARD_CODE);
		param.put("listFunctionName", "noticeListView");
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		model.addAllAttributes(service.getNoticeList(param));
		return JSON_VIEW;
	}
	
	//공지사항 상세화면
	@RequestMapping(value = "/common/SLife/notice/detail", method = RequestMethod.GET)
	public String noticeDetailView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "common/SLife/notice/noticeDetail";
	}
	
	//공지사항 상세
	@RequestMapping(value = "/ajax/common/SLife/notice/detail", method = RequestMethod.GET)
	public String noticeDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("s_board_seq", request.getSession().getAttribute("S_BOARD_SEQ"));
		param.put("board_code", Constants.NOTICE_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq"});
		model.addAllAttributes(service.getNoticeDetail(param));
		
		String sBoardSeq = (String)param.get("s_board_seq");
		String boardSeq = (String)param.get("board_seq");
		if (sBoardSeq == null || !sBoardSeq.equals(boardSeq)) {
			commonService.updateBoardHits(boardSeq);
			request.getSession().setAttribute("S_BOARD_SEQ", param.get("board_seq"));
		}
		return JSON_VIEW;
	}
	
	//FAQ
	@RequestMapping(value = "/common/SLife/faq/list", method = RequestMethod.GET)
	public String faqListView() throws Exception {
		return "common/SLife/faq/faqList";
	}
	
	//FAQ 목록
	@RequestMapping(value = "/ajax/common/SLife/faq/list", method = RequestMethod.GET)
	public String faqList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.FAQ_BOARD_CODE);
		param.put("listFunctionName", "faqListView");
		model.addAllAttributes(commonService.getBoardList(param));
		return JSON_VIEW;
	}
	
	//QNA
	@RequestMapping(value = "/common/SLife/qna/list", method = RequestMethod.GET)
	public String qnaListView() throws Exception {
		return "common/SLife/qna/qnaList";
	}
	
	//QNA 목록
	@RequestMapping(value = "/ajax/common/SLife/qna/list", method = RequestMethod.GET)
	public String qnsList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("listFunctionName", "qnaListView");
		model.addAllAttributes(service.getQNAList(param));
		return JSON_VIEW;
	}
	
	//QNA answer 답변 registrasi
	@RequestMapping(value = "/ajax/common/SLife/qna/answer/create", method = RequestMethod.POST)
	public String answerCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[]{"qna_seq", "answer_content"});
		param.put("reg_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.answerCreate(param));
		return JSON_VIEW;
	}
	
	//QNA answer perbaiki
	@RequestMapping(value = "/ajax/common/SLife/qna/answer/modify", method = RequestMethod.POST)
	public String answerModify(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[]{"qna_seq", "answer_content"});
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		param.put("upt_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.answerModify(param));
		return JSON_VIEW;
	}
	
	//QNA answer 삭제
	@RequestMapping(value = "/ajax/common/SLife/qna/answer/remove", method = RequestMethod.POST)
	public String answerRemove(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[]{"qna_seq"});
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		param.put("upt_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.answerRemove(param));
		return JSON_VIEW;
	}
	
	//QNA question registrasi 화면
	@RequestMapping(value = "/common/SLife/qna/question/create", method = RequestMethod.GET)
	public String qnaCreateView() throws Exception {
		//TODO 카테고리 불러오기
		return "common/SLife/qna/questionCreate";
	}
	
	//QNA question registrasi
	@RequestMapping(value = "/ajax/common/SLife/qna/question/create", method = RequestMethod.POST)
	public String questionCreate(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		Util.requiredCheck(param, new String[]{"title", "content"});
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(service.questionCreate(param));
		return JSON_VIEW;
	}
	
	
	//핫뉴스 목록화면
	@RequestMapping(value = "/common/SLife/hotnews/list", method = RequestMethod.GET)
	public String hotnewsListView() throws Exception {
		return "common/SLife/hotnews/hotnewsList";
	}
	
	//핫뉴스 목록
	@RequestMapping(value = "/ajax/common/SLife/hotnews/list", method = RequestMethod.GET)
	public String hotnewsList(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("board_code", Constants.HOTNEWS_BOARD_CODE);
		param.put("listFunctionName", "boardListView");
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		model.addAllAttributes(commonService.getBoardList(param));
		return JSON_VIEW;
	}
	
	//핫뉴스 상세화면
	@RequestMapping(value = "/common/SLife/hotnews/detail", method = RequestMethod.GET)
	public String hotnewsDetailView(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(param);
		return "common/SLife/hotnews/hotnewsDetail";
	}
	
	//핫뉴스 상세
	@RequestMapping(value = "/ajax/common/SLife/hotnews/detail", method = RequestMethod.GET)
	public String hotnewsDetail(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		param.put("s_board_seq", request.getSession().getAttribute("S_BOARD_SEQ"));
		param.put("board_code", Constants.HOTNEWS_BOARD_CODE);
		param.put("s_user_seq", request.getSession().getAttribute("S_USER_SEQ"));
		param.put("s_user_level", request.getSession().getAttribute("S_USER_LEVEL"));
		Util.requiredCheck(param, new String[] {"board_seq", "s_user_level", "s_user_seq"});
		model.addAllAttributes(commonService.getBoardDetail(param));
		String sBoardSeq = (String)param.get("s_board_seq");
		String boardSeq = (String)param.get("board_seq");
		if (sBoardSeq == null || !sBoardSeq.equals(boardSeq)) {
			commonService.updateBoardHits(boardSeq);
			request.getSession().setAttribute("S_BOARD_SEQ", param.get("board_seq"));
		}
		return JSON_VIEW;
	}
	
	
	//핫뉴스 상세
	@RequestMapping(value = "/ajax/common/SLife/admin/qnaCnt", method = RequestMethod.GET)
	public String qnaCnt(HttpServletRequest request, @RequestParam HashMap<String, Object> param, Model model) throws Exception {
		model.addAllAttributes(commonService.getQnaCnt(param));
		return JSON_VIEW;
	}
}
