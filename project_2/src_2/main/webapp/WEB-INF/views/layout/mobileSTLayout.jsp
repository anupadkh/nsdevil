<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta property="og:title" content="MLMS">
		<meta property="og:image" content="img/favicon.png">
		<meta property="og:description" content="의과대학 LMS">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no, maximum-scale=1, width=device-width" />
		<title>MLMS</title>
		
		<link rel="icon" type="img/png" href="${M_IMG}/favicon.png">
		<link rel="shortcut icon" href="${M_IMG}/favicon.png">
		<link rel="icon" href="${M_IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${M_IMG}/favicon.ico">
		<link rel="stylesheet" href="${CSS}/css_m3.css" type="text/css">
		<link rel="stylesheet" href="${CSS}/swiper.min.css">
		<link rel="stylesheet" href="${JS}/lib/mobile/fullcalendar-v3.6.2/css/calendar1.css">
		<link rel="stylesheet" href="${CSS}/dev_css_m3.css">
		<script src="${JS}/lib/jquery-1.11.1.js"></script>
		<script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.form.min.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/lib/DateTimePicker.js"></script>
		<script src="${JS}/lib/swiper.min.js"></script>
		<script src="${JS}/mobileCommon.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				initPageTopButton();
				initSwiperContainer();
				getSFResearchCount();
				getAsgmtScoreNew();
			});
		
			function getAsgmtScoreNew(){
				 $.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/st/asgmtScore/newCount",
		            data: {                  
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		if(data.newCnt.asgmt_cnt != 0){
		            			$("span[data-name=asgmtNew]").text("N");
		            		}else{
		            			$("span[data-name=asgmtNew]").text("");
		            		}
		            		if(data.newCnt.curr_new != 0 || data.newCnt.oc_new != 0){
		            			$("span[data-name=scoreNew]").text("N");		            			
		            		}else{
		            			$("span[data-name=scoreNew]").text("");
		            		}
		            	}
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        }); 			
			}	
			
			function getSFResearchCount(){
				 $.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/st/sfResearch/count",
		            data: {                  
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		if(data.sfCnt!=0){
		            			$("#sfResearch").show();
		            			$("#sfResearchCnt").text(data.sfCnt);
		            		}else{
		            			$("#sfResearch").hide();
		            		}
		            		if(data.currSfCnt!=0){
		            			$("#currSfResearch").show();
		            			$("#currSfResearchCnt").text(data.currSfCnt);
		            		}else{
		            			$("#currSfResearch").hide();
		            		}
		            	}
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        }); 			
			}			
			
			function notifyOpen() {
				var notifyDiv = $("#pop_m1");
				if (notifyDiv.css("display") == "none" ) {
					notifyDiv.show();
					notifyDiv.delay(3000).fadeOut("slow");
				} else {
					notifyDiv.hide();
				}
			}
		
			function nofifyClose() {
				notifyDiv.css("display", "none");
			}
		
			function rightMenuOpen() {
				//TODO 열때마다 ajax
				$("#gnb").css("width", "100%");
			}

			function rightMenuClose() {
				$("#gnb").css("width", "0%");
			}
			
			function monthScheduleListView() {
				location.href = "${M_HOME}/st/schedule/month/list";
			}
			
			function privateScheduleListView() {
				post_to_url("${M_HOME}/st/schedule/private/list", {"memo_date":getTodayYYYYMMDD()});
			}
		</script>
		<sitemesh:write property='head' />
	</head>
	
	<body class="color1">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			
			<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
		<!-- s_wrap -->
		<div id="wrap">
		
			<!-- s_header -->
			<header class="header">
				<div class="wrap">
	
					<!-- s_h1 -->
					<h1 class="logo">
						<a href="${M_HOME}/st/curriculum/list" title="waktu표 처음 페이지로 가기"><img src="${M_IMG}/m_ic_golist.png" alt="의과대학 로고"></a>

					</h1>
					
					<!-- e_h1 -->
					<!-- s_top_date -->
					<div class="top_date" name="titleDiv">
						<input type="text" data-field="date" data-format="yyyy-MM-dd" id="date2" class="dt001" readonly>
						<div id="dtBox"></div>
					</div>
					
					<!-- e_top_date -->
	
					<!-- s_menu_button -->
					<div class="menu_button">
						<div class="menu_list" title="메뉴 펼치기" onclick="javascript:rightMenuOpen();"></div>
					</div>
					<!-- e_menu_button -->
				</div>				
			</header>
			<!-- e_header -->
	
			<!-- s_container_table -->
			<div id="container" class="container_table">
				<sitemesh:write property='body' />			
			</div>
			<!-- e_container_table -->
	
			<a href="#" id="backtotop" title="To top" class="totop"></a>
	
		</div>
		<!-- e_wrap -->
	
		<!-- s_gnb -->
		<div id="gnb" class="sidenav">
			<div class="nav_wrap">
				<div class="con_wrap">
					<div class="top_wrap">
						<div class="a_mp" onclick="javascript:notifyOpen();" title="프로필 사진은 PC웹에서 변경하세요.">
							<div class="pt01">
								<img src="${M_IMG}/profile_default.png" alt="registrasi된 사진 이미지" class="pt_img">
							</div>
							<div class="ssp1">

								<span class="ssp_s1">${sessionScope.S_USER_NAME}</span><span class="ssp_s2">(${sessionScope.S_USER_DEPARTMENT_NAME})</span>

							</div>
						</div>
						<div id="pop_m1" onclick="javscript:notifyClose();">프로필 사진은 PC웹에서 변경하세요.</div>
						<a href="javascript:void(0)" class="closebtn" onclick="javascript:rightMenuClose();"></a>
					</div>
					<div class="cld_wrap">
						<div class="s_wrap">
							<div class="cld mm">
								<a href="javascript:void(0);" onclick="javascript:monthScheduleListView();"><span class="tt">월간</span></a>
							</div>
							<div class="cld dd">
								<a href="${M_HOME}/st/main"><span class="tt">일간</span></a>
							</div>
							<div class="cld pp">
								<a href="javascript:privateScheduleListView();" onclick=""><span class="tt">개인일정</span></a>
							</div>
						</div>
					</div>
					<div class="menu_wrap">
						<a href="javascript:post_to_url('${M_HOME }/st/sfResearch/list', {'type':'1'});" class="mn mn1" id="sfResearch"><img src="${M_IMG}/m_cntr_act.png" alt="sign" class="sign">
							<span class="sp_st1"><span class="t1">대기</span><span class="t2" id="sfResearchCnt"></span></span>
							<span class="sp_st2">수업만족도 조사</span>
							<span class="sp_st3"><span class="ts1">periode 내 완료하지 않을 경우</span><span class="ts2">감점</span></span>
						</a>
						<a href="javascript:post_to_url('${M_HOME }/st/sfResearch/list', {'type':'2'});" class="mn mn1" id="currSfResearch"><img src="${M_IMG}/m_cntr_act.png" alt="sign" class="sign">
							<span class="sp_st1_1"><span class="t1">대기</span><span class="t2" id="currSfResearchCnt"></span></span>
							<span class="sp_st2_1">과정만족도 조사</span>
							<span class="sp_st3_1"><span class="ts1">periode 내 완료하지 않을 경우</span><span class="ts2">감점</span></span>
						</a>
						<a href="${M_HOME}/st/asgmt" class="mn mn1">
							<img src="${M_IMG}/m_cntr_act.png" alt="sign" class="sign">
							<span class="sp01">과제</span>
							<span class="newsign" data-name="asgmtNew"></span>							
						</a>
						<a href="${M_HOME }/st/grade" class="mn mn1">
							<img src="${M_IMG}/m_cntr_act.png" alt="sign" class="sign">
							<span class="sp01">성적</span>
							<span class="newsign" data-name="scoreNew"></span>
						</a>
						<a href="${M_HOME}/st/notice" class="mn mn1">
							<img src="${M_IMG}/m_cntr_act.png" alt="sign" class="sign">
							<span class="sp01">알림</span>							
						</a>
						<a href="${M_HOME}/st/learning/board/list" class="mn mn1">
							<img src="${M_IMG}/m_cntr_act.png" alt="sign" class="sign">
							<span class="sp01">학습자료실</span>
						</a>
						<a href="${M_HOME}/st/SLife/board/list" class="mn mn1">
							<img src="${M_IMG}/m_cntr_act.png" alt="sign" class="sign">
							<span class="sp01">학교생활</span>
						</a>
						<a href="#" class="mn mn1">
							<img src="${M_IMG}/m_cntr_act.png" alt="sign" class="sign">
							<span class="sp01">시설예약</span>
						</a>
						<a href="${M_HOME}/logout" class="mn mn1">
							<img src="${M_IMG}/m_cntr_act.png" alt="sign" class="sign">
							<span class="sp01">로그아웃</span>
							<span class="sp02">${S_USER_ID }</span>
						</a>
						<a href="${M_HOME}/pcVersion" class="mn mn1">
							<img src="${M_IMG}/m_cntr_act.png" alt="sign" class="sign">
							<span class="sp01">PC버전</span>
						</a>
						<a href="${M_HOME}/st/main" title="로그아웃 | 홈으로 가기" class="mn mn2">${UNIVERSITY_NAME }</a>
					</div>
				</div>
			</div>
		</div>
		<!-- e_gnb -->
	</body>
</html>