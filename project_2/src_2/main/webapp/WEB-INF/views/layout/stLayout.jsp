<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="Description" content="의과대학 LMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="width=1100">
		
		<title>MLMS</title>
		
		<link rel="icon" type="img/png" href="${IMG}/favicon.png">
		<link rel="shortcut icon" href="${IMG}/favicon.png">
		<link rel="icon" href="${IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${IMG}/favicon.ico"> 
        <link rel="stylesheet" href="${CSS}/st_style.css" type="text/css">		
		<link rel="stylesheet" href="${CSS}/dev_st_style.css" type="text/css">
        <link rel="stylesheet" href="${CSS}/datetime_1.css" type="text/css" >
		<script src="${JS}/lib/jquery-1.11.1.js"></script>
        <script src="${JS}/lib/datetimep_1.js"></script>
		<script src="${JS}/lib/monthdatepicker_ui.js"></script>
        <script src="${JS}/lib/jquery.form.min.js"></script>
        <script src="${HOME}/resources/smarteditor_v2.3/js/HuskyEZCreator.js"></script>
		<script src="${JS}/lib/autosize.min.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/lib/jquery.ui.accordion.js"></script>
        <script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/common.js"></script>
		
		<style>
			.gnb li a{display: inline-block;margin: 0;padding: 0px; width:100%;height:40px;line-height: 40px;float: left;font-size:15px;color: rgba(255,255,255,1);border-bottom: solid 2px rgba(1, 96, 122, 1);text-align: center;text-indent: 0;box-sizing: border-box;}
		</style>
		<script type="text/javascript">
			$(document).ready(function(){
				topPopupInit();
				initPageTopButton();
				customSelectBoxInit();
				$(document).mouseup(function(e){
					layerPopupCloseInit(['div[name="myProfilePopupDiv"]']);
				});
				
				$('.btn-yearmonthpick').bind('click',function(){
					$(this).siblings('input').datetimepicker('show');
				});
				
				//사이드 메뉴
				if ("${rightMenuOpenYn}" == "N" || $.cookie("rightMenuOpenYn") == null || $.cookie("rightMenuOpenYn") == "N") {
					openLeftMenu();
				} else {
					openRightMenu();
				}
				
				bindPopupEvent("99");
				
				layerPopupCloseInit(['div.uoptions']);
				
				$(".btn_l").bind("click", function(){
			        $(".btn_l").toggleClass("move-trigger_l");
			        $(".aside_l").toggleClass("folding");
			        $(".main_con").toggleClass("");
			    });
			});
			
			
// 			function loadCurriculumList() {
// 				$.ajax({
// 			        type: "GET",
// 			        url: "${HOME}/ajax/pf/lesson/menu/list",
// 			        data: $("#frm").serialize(),
// 			        dataType: "json",
// 			        success: function(data, status) {
			        	
// 			        },
// 			        error: function(xhr, textStatus) {
// 			            alert(textStatus);
// 			            document.write(xhr.responseText);
// 			        }
// 			    });
// 			}
			
			function openLeftMenu() {
				
				$.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/st/lesson/menu/left/list",
			        data: $("#frm").serialize(),
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		leftMenuSetting(data);
			        	} else {
			        		
			        	}
			        },
			        error: function(xhr, textStatus) {
// 			            alert(textStatus);
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}

			function academicYearSelect(year) {
				$("#academicYear").val(year);
				openLeftMenu();
			}
			
			function leftMenuSetting(jsonData) {
				//$("div#leftMenuDiv").removeClass("left_m").addClass("left_mcon");
				//$("#leftMenuBtn").hide();
				$("#yearSelectDiv").show();
				$("#mn").show();
				
				var academicYearList = jsonData.academic_year_list;
				var curriculumList = jsonData.curriculum_list;
				var html = "";
				
				//학사tahun 셀렉트 박스
				$(academicYearList).each(function(){
					html += '<span class="uoption" value="'+this.year+'" onclick="javascript:academicYearSelect('+this.year+');">' + this.year + 'tahun</span>';
				});

				$("#yearListAdd").html(html);
				$("#academicYearSelectBox").val(jsonData.current_academic_year);
				$("#academicYearSelectBox span.uselected").text(jsonData.current_academic_year+"tahun");
				
				//교육과정 목록
				html = '';
				$.each(jsonData.curriculum_list, function(){
					html += '<div class="title" id="mn1" title="교육과정 상세보기" name="'+this.curr_seq+'" onClick="detailCurriculum('+this.curr_seq+',\''+this.curr_name+'\',1,\'Y\');"><span class="sp_tt">'+this.curr_name+'</span></div>';
				});				  
				$("#mn").html(html);

				if($.cookie("lpMenuYN") == "Y"){
					$("#mn div[name=${S_CURRICULUM_SEQ}]").trigger("click");
				}
				
				$("div#rightMenuDiv").html('<button type="button" id="openRightBtn" onclick="openRightMenu();" title="일정" class="ic_list"></button>');
				$("div#rightMenuDiv").removeClass("right_mcon").addClass("right_m");
				$(".xdsoft_datetimepicker").remove();//오른쪽 메뉴 캘린더 삭제
				$("#leftMenuList").accordion({collapsible: true, active: false}).accordion("refresh");
				$.cookie("rightMenuOpenYn", "N", { expires: 1 });
			}
			
			function openCurriculumList(){
				$.cookie("lpMenuYN", "N", { expires: 1 });
				openLeftMenu();				
			}
			
			function detailCurriculum(curr_seq, curr_name, page, newYN){
				$.cookie("lpMenuYN", "Y", { expires: 1 });
				$.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/st/lesson/menu/left/lessonPlanList",
			        data: {
			        	"curr_seq" : curr_seq
			        	,"page" : page
			        	,"newYN" : newYN //처음 가져올때 Y 아닐때 N 처음가져올때는 hari dan tanggal로 page 셋팅한다.
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {

			        		var htmls = "";			        		
			        		htmls += '<div class="title" title="교육과정 목록보기" onClick="openCurriculumList();"><span class="sp_tt">'+curr_name+'</span></div>';			        		
			        		htmls += '<ul class="panel" style="overflow-y:hidden;">';
			        		htmls += '    	 <li class="wrap">';
							htmls += '		     <button type="button" class="btn_go1" onclick="javascript:pageMoveCurriculumView(' + curr_seq + '); return false;">교육과정 계획서 바로가기<span class="sign">▶</span></button>';      
							htmls += '    	 </li>';
							
							var countInfo = data.countInfo;
							
							if(countInfo.preCnt > 0){
								htmls+='<li class="wrap_more" data-name="'+curr_seq+'">'
				              	+'<button class="more1" onClick="detailCurriculum('+curr_seq+',\''+curr_name+'\','+countInfo.preCnt+',\'N\');">'                        
				              	+'이전 수업 더보기</button></li>';
							}								
							
			        		$.each(data.lessonPlanList, function(){
			        			var nonregi = "", lesson_subject = "";
			        			lesson_subject = this.lesson_subject;
			        			if(isEmpty(this.lesson_subject)){
			        				nonregi = "nonregi";
			        				lesson_subject = "수업명 미registrasi";
			        			}
			        			htmls += '<li class="wrap" onClick="pageMoveLessonPlanView('+this.lp_seq+','+curr_seq+');" style="cursor:pointer;"><span class="list_date">['+this.lesson_date+'] '+this.period+' pengajar</span><span class="tt_1 '+nonregi+'">'+lesson_subject+'</span></li>';
			        		});
			        		
			        		if(countInfo.nextCnt <= countInfo.totalCnt){
			        			htmls += '<li class="wrap_more" data-name="'+curr_seq+'">'
				              		+'<button class="more2" onClick="detailCurriculum('+curr_seq+',\''+curr_name+'\','+countInfo.nextCnt+',\'N\');">'                         
		                        	+'수업 더보기</button></li>';					
							}
			        		
		        	        htmls += '</ul>';
		        	        $("#mn").html(htmls);
			        	} else {
			        		
			        	}
			        },
			        error: function(xhr, textStatus) {
// 			            alert(textStatus);
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function pageMoveCurriculumView(seq) {	
				$.cookie("lpMenuYN", "N", { expires: 1 });
				$("#currSeq").val(seq);
				$("#academicYear").val($("#academicYearSelectBox").attr("value"));
				$("#frm").attr("action", "${HOME}/st/lesson/curriculum");
				$("#frm").attr("method", "POST");
				$("#frm").submit();
			}
			
			function pageMoveLessonPlanView(lp_seq, curr_seq){
				$.cookie("lpMenuYN", "Y", { expires: 1 });	
				$("#lpSeq").val(lp_seq);
				$("#currSeq").val(curr_seq);
                $("#academicYear").val($("#academicYearSelectBox").attr("value"));
                $("#frm").attr("action", "${HOME}/st/lesson/lessonPlan");
                $("#frm").attr("method", "POST");
                $("#frm").submit();
			}
			
			function openRightMenu() {
				$("div#rightMenuDiv").removeClass("right_m").addClass("right_mcon");
				$("#openRightBtn").remove();
				var html = "";
				html += '<button type="button" onclick="javascript:alert("//TODO waktu표 이동")" class="btn bt02">My waktu표</button>';
				html += '<button type="button" onclick="location.href=\'${HOME}/st/lesson/stNotice/view\';" class="btn bt03">알림</button>';
				html += '<button type="button" class="btn bt04 open99">일정 쓰기</button>';
				html += '<input type="text" id="rightMenuCalendar" class="dateyearpicker-input ip_date">';
				html += '<div class="schd_list">';
				html += '    <table class="list">';
				html += '        <tbody>';
				html += '            <tr class="li_wrap">';
				html += '                <td class="t_wrap"><span class="sp01">오전</span><span class="sp02">09 : 00</span><span class="sp01">50분</span></td>';
				html += '                <td class="c_wrap"><span class="sp01"><span class="tt">1 pengajar : </span>해부학 lecture</span><span class="sp02">라파엘관 501호</span></td>';
				html += '            </tr>';
				html += '            <tr class="li_wrap">';
				html += '                <td class="t_wrap"><span class="sp01">오전</span><span class="sp02">10 : 00</span><span class="sp01">50분</span></td>';
				html += '                <td class="c_wrap"><span class="sp01"><span class="tt">2 pengajar : </span>긴 수업명 테스트 하나 둘 셋 넷 다섯</span><span class="sp02">라파엘관 501호</span></td>';
				html += '            </tr>';
				html += '            <tr class="li_wrap">';
				html += '                <td class="t_wrap"><span class="sp01">오후</span><span class="sp02">2 : 00</span><span class="sp01">100분</span></td>';
				html += '                <td class="c_wrap"><span class="sp01"><span class="tt">5, 6 pengajar : </span>해부학 pelatihan</span><span class="sp02">라파엘관 501호</span></td>';
				html += '            </tr>';
				html += '        </tbody>';
				html += '    </table>';
				html += '</div>';
				$("div#rightMenuDiv").html(html);
				$("#yearSelectDiv").hide();
				$("#mn").hide();
				$("#leftMenuBtn").show();
				$("div#leftMenuDiv").removeClass("left_mcon").addClass("left_m");
				
				//오른쪽 메뉴 캘린더
				$.datetimepicker.setLocale('kr');
				$("#rightMenuCalendar").datetimepicker('show');
				$("#rightMenuCalendar").datetimepicker({
			        timepicker:true,
			        datepicker:true,
			        format:'y.m.d',
			        formatDate:'y.m.d'
			    });
				$.cookie("rightMenuOpenYn", "Y", { expires: 1 });
			}
		</script>
		<style>		
		.boardwrap .panel .wrap .btn_go1{position:relative;display:inline-block;width: 202px;margin: 2px 0 3px 0;padding: 0;height:35px;line-height: 33px;font-size: 15px;text-align: center;text-indent: 5px;letter-spacing: -2px;word-spacing: 2px;color: rgba(255, 255, 255, 1);background: rgba(57, 166, 196, 1);float: left;cursor: pointer;border-bottom-right-radius: 17px;border-top-right-radius: 17px;box-shadow: 0px 1px 2px 1px rgba(167, 176, 176, 0.7);border-bottom: solid 1px rgb(36, 154, 186);}
		</style>
		<sitemesh:write property='head'/>
	</head>

	<body class="full_schd fold">
		<form id="frm" name="frm">
			<input type="hidden" id="academicYear" name="academicYear" value="${S_CURRENT_ACADEMIC_YEAR }">
			<input type="hidden" id="currSeq" name="currSeq" value="">
			<input type="hidden" id="lpSeq" name="lpSeq" value="">			
		</form>
		
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
		
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a>
			<a href="#container">컨텐츠 바로가기</a>
		</div>
		
		<div id="wrap">
			<div id="top_pop">
				<div class="wrap">
					<div class="slide-wrap">
						<ul>
							<li class="tt"><img src="${IMG}/pop001.png" alt="의과대학 의학교육 penilaian인증 인증 획득 4년"></li>
						</ul>
					</div>
				</div>
			</div>
			<header class="header">
				<div class="header_top">
					<div class="top_wrap">       
						<div class="top_m">
							<!-- //TODO 임시로 제외시킴 -->
							<!-- <a href="javascript:void(0);" id="topPopupBtn" class="a_m a1" onclick="javascript:topPopupToggle();">팝업닫기</a> -->
							<c:choose>
								<c:when test='${HOME ne ""}'>
									<a href="${HOME}" class="a_m a3">HOME</a>
								</c:when>
								<c:otherwise>
									<a href="/" class="a_m a3">HOME</a>
								</c:otherwise>
							</c:choose>
							<!-- //TODO 임시로 제외시킴 -->
							<!-- <a href="#" class="a_m a4">사이트맵</a> -->
							<c:choose>
								<c:when test="${sessionScope.S_USER_ID ne null}">
									<a href="${HOME }/logout" class="a_m a5">로그아웃</a>
								</c:when>
								<c:otherwise>
									<a href="${HOME }/login" class="a_m a5">로그인</a>
								</c:otherwise>
							</c:choose>
							<a href="${HOME_URL }" class="a_m a6">기관Home</a>
						</div>
						<c:if test="${sessionScope.S_USER_ID ne null}">
							<div class="a_mp a7" onclick="myProfilePopup()" >
								<c:choose>
									<c:when test='${sessionScope.S_USER_PICTURE_PATH ne null and sessionScope.S_USER_PICTURE_PATH ne ""}'>
										<span class="pt01"><img src="${sessionScope.S_USER_PICTURE_PATH}" alt="나의 대표 이미지" class="pt_img"></span>
									</c:when>
									<c:otherwise>
										<span class="pt01"><img src="${DEFAULT_PICTURE_IMG}" alt="나의 대표 이미지" class="pt_img"></span>
									</c:otherwise>
								</c:choose>
								
								<span class="ssp1">${sessionScope.S_USER_NAME} (${sessionScope.S_USER_DEPARTMENT_NAME})</span>
							</div>
							<div id="pop_pop9" name="myProfilePopupDiv">
								<div class="spop9 show" id="spop9">            
									<span class="tt">${sessionScope.S_USER_NAME} (${sessionScope.S_USER_DEPARTMENT_NAME})</span>
									<span class="tt">${sessionScope.S_USER_EMAIL}</span>
									<button onclick="location.href='${HOME}/common/SLife/qna/list'" class="btn btn05">1:1 문의</button>
									<span onclick="$('#pop_pop9').hide();" class="pop_close_s" title="닫기">X</span>
								</div>
							</div>
						</c:if>
					</div>
				</div>     
				
				<div class="gnbwrap">
					<div class="gnb_swrap">
						<h1 class="logo">
						<c:choose>
							<c:when test='${HOME ne ""}'>
								<a href="${HOME}" title="홈으로 가기"><img src="${TOP_LOGO}" alt="의과대학 로고"></a>
							</c:when>
							<c:otherwise>
								<a href="/" title="홈으로 가기"><img src="${TOP_LOGO}" alt="의과대학 로고">	</a>
							</c:otherwise>
						</c:choose>
						</h1>
					</div>
					<div class="gnb_fwrap">
						<div class="gnb_swrap2">
							<ul id="gnb" class="gnb">
								<c:forEach items="${sessionScope.S_ALLOW_MENU_LIST}" var="allowMenuList" step="1">
									<li><a href="${HOME}${allowMenuList.url}" onclick="javascript:pageLinkCheck('${HOME}','${HOME}${allowMenuList.url}'); return false;" <c:if test='${allowMenuList.now_menu_yn eq "Y" && allowMenuList.url ne ""}'>class="on"</c:if>>${allowMenuList.name}</a></li>
								</c:forEach>
							</ul>
						</div>
					</div>
				</div>
			</header>
	<!-- s_container_table -->
			<div id="container" class="container_table">
			<!-- s_contents -->
			    <div class="contents main">
			        
					<div class="left_mcon st aside_l" id="leftMenuDiv">
						<div class="wrap1_lms_uselectbox" id="yearSelectDiv">
							<div class="uselectbox" id="academicYearSelectBox" value="">
								<span class="uselected"></span><span class="uarrow">▼</span>
								<div class="uoptions" id="yearListAdd">
								</div>
							</div>
						</div>
						
						<button type="button" onclick="location.href='${HOME}/st/lesson/stNotice/view'" class="btn_tdlist">알림</button>
						
						<div class="boardwrap" id="mn" style="max-height:none;">
						</div>
					    <button type="button" title="수업리스트" class="ic_list" onclick="openLeftMenu(this);"></button>
					</div>
			
			        <!-- s_main_con -->
			        <div class="main_con st">
			        	
						<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
						<sitemesh:write property='body'/>
					</div>
			        <!-- e_main_con -->
			
			        <!-- s_right_m -->
			        <!-- <div class="right_m" id="rightMenuDiv">
			            <button type="button" id="leftMenuBtn" onclick="openRightMenu(this);" title="일정" class="ic_list"></button>
			        </div> -->
			    </div>
			</div>
			
			<a href="#" id="backtotop" title="To top" class="totop" ></a>
			
			<footer id="footer" class="">
		        <div class="footer_con">           
		            <div class="footer_scon">   
		                <ul class="f_menu">
						    <li><a href="#" title="새창" class="fmn1">개인정보처리방침</a></li>
		                    <li><a href="#" title="새창" class="fmn2"><span>|</span>이메일무단수집거부</a></li>                
					    </ul>   
		                <p class="addr">${ADDRESS }</p>
		                <a href="${HOME}" title="홈으로 가기" class="logo_wrap"><img src="${BOTTOM_LOGO}" alt="의과대학 로고" class="b_logo"></a>
		            </div>
		        </div>
			</footer>
		</div>
	</body>
</html>