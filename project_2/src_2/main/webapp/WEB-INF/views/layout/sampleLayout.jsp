<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<script src="${JS}/lib/jquery-1.11.1.js"></script>
	
		<sitemesh:write property='head'/>
		
	</head>
	<body>
		<div>상단 공통 Area</div>
		<sitemesh:write property='body'/>
		<div>하단 공통Area</div>
	</body>
</html>
