<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="Description" content="의과대학 LMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="width=1100">
		
		<title>MLMS</title>
		
		<link rel="icon" type="img/png" href="${IMG}/favicon.png">
		<link rel="shortcut icon" href="${IMG}/favicon.png">
		<link rel="icon" href="${IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${IMG}/favicon.ico">
		<link rel="stylesheet" href="${CSS}/pf_style.css" type="text/css">
		<link rel="stylesheet" href="${CSS}/datetime_1.css" type="text/css" >
		<link rel="stylesheet" href="${CSS}/css/jquery.ui.datepicker.min.css" type="text/css" >
		<link rel="stylesheet" href="${CSS}/dev_pf_style.css" type="text/css">
		<script src="${JS}/lib/jquery-1.11.1.min.js"></script>
        <script src="${JS}/lib/datetimep_1.js"></script>
		<script src="${JS}/lib/timepicki.js"></script>
		<script src="${JS}/lib/monthdatepicker_ui.js"></script>
        <script src="${JS}/lib/jquery.form.min.js"></script>
		<script src="${JS}/lib/autosize.min.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/lib/jquery.ui.accordion.js"></script>
        <script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.ui-1.10.4.datepicker.min.js"></script>
		<script src="${JS}/common.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				topPopupInit();
				initPageTopButton();
				customSelectBoxInit();
				$(document).mouseup(function(e){
					layerPopupCloseInit(['div[name="myProfilePopupDiv"]']);
				});
				
				$('.timepicker1').timepicki();
		    	$('.timepicker2').timepicki({custom_classes:"time2"});
				
				//사이드 메뉴
				if ("${rightMenuOpenYn}" == "N" || $.cookie("rightMenuOpenYn") == null || $.cookie("rightMenuOpenYn") == "N") {
					openLeftMenu(1);
				} else {
					openRightMenu();
				}
				
				layerPopupCloseInit(['div.uoptions']);
				
				$(document).on("change", "input[name='all_day']", function(index){
					if($(this).is(":checked")){
						$(":input[name='memo_start_time']").val("");
						$(":input[name='memo_end_time']").val("");
						$(":input[name='memo_start_time']").prop("disabled", true);
						$(":input[name='memo_end_time']").prop("disabled", true);
						$(":input[name='memo_edate']")
						if(!isEmpty($(":input[name='memo_date']").val())){
							$(":input[name='memo_edate']").val($(":input[name='memo_date']").val());
							$(":input[name='memo_edate']").prop("disabled", true);							
						}						
					}else{
						$(":input[name='memo_edate']").prop("disabled", false);
						$(":input[name='memo_start_time']").prop("disabled", false);
						$(":input[name='memo_end_time']").prop("disabled", false);						
					}
				});
				$(document).on("change", "input[name='memo_date']",function(){
					if($(":input[name='all_day']").is(":checked"))
						$(":input[name='memo_edate']").val($(this).val());
				});
				
				$(".btn_l").bind("click", function(){
			        $(".btn_l").toggleClass("move-trigger_l");
			        $(".aside_l").toggleClass("folding");
			        $(".main_con").toggleClass("");
			    });
			});
			
			$(document).click(function(){
			    $.datetimepicker.setLocale('kr');
			    $('.dateyearpicker-input_1').datetimepicker({
			        timepicker:false,
			        datepicker:true,
			        format:'y-m-d',
			        formatDate:'y-m-d'
			    });
			});
			
// 			function loadCurriculumList() {
// 				$.ajax({
// 			        type: "GET",
// 			        url: "${HOME}/ajax/pf/lesson/menu/list",
// 			        data: $("#frm").serialize(),
// 			        dataType: "json",
// 			        success: function(data, status) {
			        	
// 			        },
// 			        error: function(xhr, textStatus) {
// 			            alert(textStatus);
// 			            document.write(xhr.responseText);
// 			        }
// 			    });
// 			}
			
			function openLeftMenu(page) {
				$("#page").val(page);				
				
				$.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/pf/lesson/menu/left/list",
			        data: $("#frm").serialize(),
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		leftMenuSetting(data);
			        	} else {
			        		
			        	}
			        },
			        error: function(xhr, textStatus) {
// 			            alert(textStatus);
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function academicYearSelect(year) {
				$("#academicYear").val(year);
				openLeftMenu(1);
			}
			
			function leftMenuSetting(jsonData) {
				//$("div#leftMenuDiv").removeClass("left_m").addClass("left_mcon");
				$("#leftMenuBtn").remove();
				
				var academicYearList = jsonData.academic_year_list;
				var curriculumList = jsonData.curriculum_list;
				var html = "";
				
				//학사tahun 셀렉트 박스
				html += '<div class="wrap1_lms_uselectbox">';
				html += '    <div class="uselectbox" id="academicYearSelectBox" value="' + jsonData.current_academic_year + '">';
				html += '        <span class="uselected">' + jsonData.current_academic_year + 'tahun</span>';
				html += '        <span class="uarrow">▼</span>';
				html += '        <div class="uoptions">';
				$(academicYearList).each(function(){
					html += '            <span class="uoption" value="'+this.year+'" onclick="javascript:academicYearSelect('+this.year+');">' + this.year + 'tahun</span>';
				});
				html += '        </div>';
				html += '    </div>';
				html += '</div>';
				html += '<button type="button" onclick="location.href=\'${HOME}/pf/lesson/pfNotice/view\'" class="btn_tdlist">알림</button>';
				
				//교육과정 목록
				html += '<div class="boardwrap" id="leftMenuList" style="max-height:none;">';
				$(curriculumList).each(function(){
					var curr_start_date = this.curr_start_date;
					var curr_end_date = this.curr_end_date;
					var curr_represent_yn = this.curr_represent_yn;
					var curr_date = "";
					if(isEmpty(curr_start_date)){
						curr_date = "periode 미입력";
					}else{
						curr_date = "["+curr_start_date+" ~ "+curr_end_date+"]";
					}
					
					html += '    <div class="tt_wrap"><span class="sp_tt">' + this.curr_name + '</span><span class="sp_tm">'+curr_date+'</span></div>';
					html += '    <ul class="panel" data-name="'+this.curr_seq+'">';
					html += '    	 <li class="wrap">';
					html += '		     <button type="button" class="btn_go1" onclick="javascript:pageMoveCurriculumView(' + this.curr_seq + '); return false;">교육과정 계획서 바로가기<span class="sign">▶</span></button>';      
					html += '    	 </li>';
					
					//수업계획서 목록					
					var lessonPlanListHtml = "";
					var lessonPlanList = this.lesson_plan_list;
					var countInfo = this.countInfo;
					var progression = 0; //교육과정의 수업계획서 진행 상태 백분률 값
					var curr_seq = this.curr_seq;
					if(countInfo.preCnt > 0){
						lessonPlanListHtml = '<li class="wrap_more" data-name="'+curr_seq+'">'
			              	+'<button class="more1" onClick="getLessonPlanList('+curr_seq+','+countInfo.preCnt+',\''+curr_represent_yn+'\');">'                        
			              	+'이전 수업 더보기</button></li>';              							
					}
					
					$(lessonPlanList).each(function(){
						var lessonDate = '';
						if (typeof this.lesson_date != "undefined" && this.lesson_date != '') {
							lessonDate = new Date(this.lesson_date);
							
							lessonDate = (this.lesson_date).substring(5, lessonDate.length); //format convert(YYYY/MM/DD -> MM/DD)
						}
						var lessonSubject = this.lesson_subject;
						lessonPlanListHtml += '<li class="wrap" data-name="'+curr_seq+'">';
						lessonPlanListHtml += '<span class="list_date">' + lessonDate + '</span>';
						if (lessonSubject == '') {//수업명 미registrasi시
							lessonSubject = "수업명(미registrasi)";
							lessonPlanListHtml += '<span class="tt_1 nonregi" onClick="pageMoveLessonPlanView('+this.lp_seq+','+curr_seq+');">' + lessonSubject +'</span>';
						} else {
							lessonPlanListHtml += '<span class="tt_1" onClick="pageMoveLessonPlanView('+this.lp_seq+','+curr_seq+');">' + lessonSubject +'</span>';
						}
						
						lessonPlanListHtml += '</li>';
					});
					

					if(countInfo.nextCnt <= countInfo.totalCnt){
						lessonPlanListHtml += '<li class="wrap_more" data-name="'+curr_seq+'">'
                    		+'<button class="more2" onClick="getLessonPlanList('+curr_seq+','+countInfo.nextCnt+',\''+curr_represent_yn+'\');">'                        
                        	+'수업 더보기</button></li>';					
					}
					
					// 교육과정의 수업계획서 진행 상태 백분률 값 = 완료된 수업 계획서 수 / 수업계획서 Keseluruhan 개수 * 100
					if(this.total_period == 0)
						progression = 0;
					else
						progression = Math.floor(this.pre_lp_cnt/this.total_period*100);
										
					if (curr_represent_yn == "Y") { //Dosen Penanggung jawab일때 현재 진행 상태 노출
						html += '	<li class="wrap">';
						html += '		<div class="wrap_prg">';
						html += '   		<div class="prg"><span class="bar" style="width:'+ (progression*2) + 'px;"></span></div>';//프로그레스바의 width값 = 교육과정의 수업계획서 진행 상태 백분률 값 * 2
						html += '       	<span class="sp1">' + progression + '%</span>';
						html += '		</div>';
						html += '	</li>';
					}
					html += lessonPlanListHtml;
					html += '</ul>';
				});
				html += '</div>';
				
				$("div#leftMenuDiv").html(html);
				$("div#rightMenuDiv").html('<button type="button" id="openRightBtn" onclick="openRightMenu();" title="일정" class="ic_list"></button>');
				$("div#rightMenuDiv").removeClass("right_mcon").addClass("right_m");
				$(".xdsoft_datetimepicker").remove();//오른쪽 메뉴 캘린더 삭제
				$("#leftMenuList").accordion({collapsible: true, active: false}).accordion("refresh");
				$.cookie("rightMenuOpenYn", "N", { expires: 1 });
			}
			
			
			function pageMoveCurriculumView(seq) {
				$("#currSeq").val(seq);
				$("#academicYear").val($("#academicYearSelectBox").attr("value"));
				$("#frm").attr("action", "${HOME}/pf/lesson/curriculum");
				$("#frm").attr("method", "POST");
				$("#frm").submit();
			}
			
			function pageMoveLessonPlanView(lp_seq, curr_seq){
				$("#lpSeq").val(lp_seq);
				$("#currSeq").val(curr_seq);
                $("#academicYear").val($("#academicYearSelectBox").attr("value"));
                $("#frm").attr("action", "${HOME}/pf/lesson/lessonPlan");
                $("#frm").attr("method", "POST");
                $("#frm").submit();
			}
			
			function openRightMenu() {
				$("div#rightMenuDiv").removeClass("right_m").addClass("right_mcon");
				$("#openRightBtn").remove();
				var html = "";
			    //html += '<button type="button" onclick="location.href=\'${HOME}/pf/lesson/academicM\'" class="btn bt01">학사력</button>';
				html += '<button type="button" onclick="location.href=\'${HOME}/pf/lesson/scheduleM\'" class="btn bt02">My waktu표</button>';
				html += '<button type="button" onclick="location.href=\'${HOME}/pf/lesson/pfNotice/view\';" class="btn bt03">알림</button>';
				html += '<button type="button" class="btn bt04 open_memo" onclick="javascript:openWriteMemo();">일정 쓰기</button>';
				html += '<input type="text" id="rightMenuCalendar" class="dateyearpicker-input ip_date">';
				
				html += '<div class="schd_list">';
				html += '    <table class="list">';
				html += '    <tbody id="scheduleDayList"></tbody>';
				html += '    </table>';
				html += '</div>';
				$("div#rightMenuDiv").html(html);
				$("div#leftMenuDiv").html('<button type="button" id="leftMenuBtn" title="수업리스트" class="ic_list" onclick="openLeftMenu(1);"></button>');
				$("div#leftMenuDiv").removeClass("left_mcon").addClass("left_m");
				
				//오른쪽 메뉴 캘린더
				$.datetimepicker.setLocale('kr');
				$("#rightMenuCalendar").datetimepicker({
			        timepicker:false,
			        datepicker:true,
			        format:'y-m-d',
					inline:true,
			        onSelectDate: function(dateText, $inst) {
						getSelectedDaySchedule("${S_CURRICULUM_SEQ}");
					}
			    });
				
				$("#rightMenuDiv .xdsoft_today_button").click(function() {
					$("#rightMenuCalendar").val(dateToString(new Date()));
					getSelectedDaySchedule("${S_CURRICULUM_SEQ}");
				});
				
				$.cookie("rightMenuOpenYn", "Y", { expires: 1 });
				
				getSelectedDaySchedule("${S_CURRICULUM_SEQ}");
			}
			
			function getLessonPlanList(curr_seq, page, curr_represent_yn){
				$.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/pf/lesson/menu/left/lessonPlanList",
			        data: {
			        	"curr_seq": curr_seq
			        	,"page" : page
			        	,"curr_represent_yn" : curr_represent_yn
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if(data.status=="200"){
			        		$("li[data-name="+curr_seq+"]").remove();
			        		var htmls = "";
				        	if(data.countInfo.preCnt > 0){
				        		htmls = '<li class="wrap_more" data-name="'+curr_seq+'">'
				              	+'<button class="more1" onClick="getLessonPlanList('+curr_seq+','+data.countInfo.preCnt+',\''+curr_represent_yn+'\');">'                        
				              	+'이전 수업 더보기</button></li>';
							}
	
				        	$.each(data.lesson_plan_list, function(index){
				        		var lessonDate = '';
								if (typeof this.lesson_date != "undefined" && this.lesson_date != '') {
									lessonDate = new Date(this.lesson_date);
									
									lessonDate = (this.lesson_date).substring(5, lessonDate.length); //format convert(YYYY/MM/DD -> MM/DD)
								}
								var lessonSubject = this.lesson_subject;
								htmls += '<li class="wrap" data-name="'+curr_seq+'">';
								htmls += '<span class="list_date">' + lessonDate + '</span>';
								if (lessonSubject == '') {//수업명 미registrasi시
									lessonSubject = "수업명(미registrasi)";
									htmls += '<span class="tt_1 nonregi" onClick="pageMoveLessonPlanView('+this.lp_seq+','+curr_seq+');">' + lessonSubject +'</span>';
								} else {
									htmls += '<span class="tt_1" onClick="pageMoveLessonPlanView('+this.lp_seq+','+curr_seq+');">' + lessonSubject +'</span>';
								}								
								
								htmls += '</li>';
				        	});
				        	
							if(data.countInfo.nextCnt <= data.countInfo.totalCnt){
								htmls += '<li class="wrap_more" data-name="'+curr_seq+'">'
				              	+'<button class="more2" onClick="getLessonPlanList('+curr_seq+','+data.countInfo.nextCnt+',\''+curr_represent_yn+'\');">'                        
				              	+'수업 더보기</button></li>';
							}
							$("ul[data-name="+curr_seq+"]").append(htmls);
			        	}
			        },
			        error: function(xhr, textStatus) {
			            alert("오류가 발생했습니다.");
			        }
			    });				
			}
		</script>
		<sitemesh:write property='head'/>
	</head>

	<body class="fold">		
		<form id="frm" name="frm">
			<input type="hidden" id="academicYear" name="academicYear" value="">
			<input type="hidden" id="currSeq" name="currSeq" value="">
			<input type="hidden" id="lpSeq" name="lpSeq" value="">	
			<input type="hidden" id="page" name="page" value="">			
		</form>
		
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
		
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a>
			<a href="#container">컨텐츠 바로가기</a>
		</div>
		
		<div id="wrap">
			<div id="top_pop">
				<div class="wrap">
					<div class="slide-wrap">
						<ul>
							<li class="tt"><img src="${IMG}/pop001.png" alt="의과대학 의학교육 penilaian인증 인증 획득 4년"></li>
						</ul>
					</div>
				</div>
			</div>
			<header class="header">
				<div class="header_top">
					<div class="top_wrap">       
						<div class="top_m">
							<!-- //TODO 임시로 제외시킴 -->
							<!-- <a href="javascript:void(0);" id="topPopupBtn" class="a_m a1" onclick="javascript:topPopupToggle();">팝업닫기</a> -->
							<c:choose>
								<c:when test='${HOME ne ""}'>
									<a href="${HOME}" class="a_m a3">HOME</a>
								</c:when>
								<c:otherwise>
									<a href="/" class="a_m a3">HOME</a>
								</c:otherwise>
							</c:choose>
						
							
							<!-- //TODO 임시로 제외시킴 -->
							<!-- <a href="#" class="a_m a4">사이트맵</a> -->
							<c:choose>
								<c:when test="${sessionScope.S_USER_ID ne null}">
									<a href="${HOME }/logout" class="a_m a5">로그아웃</a>
								</c:when>
								<c:otherwise>
									<a href="${HOME }/login" class="a_m a5">로그인</a>
								</c:otherwise>
							</c:choose>
							<a href="${HOME_URL }" class="a_m a6">기관Home</a>
						</div>
						<c:if test="${sessionScope.S_USER_ID ne null}">
							<div class="a_mp a7" onclick="myProfilePopup()" >
								<c:choose>
									<c:when test='${sessionScope.S_USER_PICTURE_PATH ne null and sessionScope.S_USER_PICTURE_PATH ne ""}'>
										<span class="pt01"><img src="${sessionScope.S_USER_PICTURE_PATH}" alt="나의 대표 이미지" class="pt_img"></span>
									</c:when>
									<c:otherwise>
										<span class="pt01"><img src="${DEFAULT_PICTURE_IMG}" alt="나의 대표 이미지" class="pt_img"></span>
									</c:otherwise>
								</c:choose>
								
								<span class="ssp1">${sessionScope.S_USER_NAME} (${sessionScope.S_USER_DEPARTMENT_NAME})</span>
							</div>
							<div id="pop_pop9" name="myProfilePopupDiv">
								<div class="spop9 show" id="spop9">            
									<span class="tt">${sessionScope.S_USER_NAME} (${sessionScope.S_USER_DEPARTMENT_NAME})</span>
									<span class="tt">${sessionScope.S_USER_EMAIL}</span>
									<button onclick="location.href='${HOME}/common/SLife/qna/list'" class="btn btn05">1:1 문의</button>
									<span onclick="$('#pop_pop9').hide();" class="pop_close_s" title="닫기">X</span>
								</div>
							</div>
						</c:if>
					</div>
				</div>     
				
				<div class="gnbwrap">
					<div class="gnb_swrap">
						<h1 class="logo">
						<c:choose>
							<c:when test='${HOME ne ""}'>
								<a href="${HOME}" title="홈으로 가기"><img src="${TOP_LOGO}" alt="의과대학 로고"></a>
							</c:when>
							<c:otherwise>
								<a href="/" title="홈으로 가기"><img src="${TOP_LOGO}" alt="의과대학 로고">	</a>
							</c:otherwise>
						</c:choose>
						</h1>
					</div>
					<div class="gnb_fwrap">
						<div class="gnb_swrap2">
							<ul id="gnb" class="gnb">
								<c:forEach items="${sessionScope.S_ALLOW_MENU_LIST}" var="allowMenuList" step="1">
									<li><a href="${HOME}${allowMenuList.url}" onclick="javascript:pageLinkCheck('${HOME}','${HOME}${allowMenuList.url}'); return false;" <c:if test='${allowMenuList.now_menu_yn eq "Y" && allowMenuList.url ne ""}'>class="on"</c:if>>${allowMenuList.name}</a></li>
								</c:forEach>
							</ul>
						</div>
					</div>
				</div>
			</header>
	<!-- s_container_table -->
			<div id="container" class="container_table">
			<!-- s_contents -->
			    <div class="contents main">
			        
					<div class="left_mcon aside_l" id="leftMenuDiv">
						<!-- TODO 여기 메뉴 UI 부터하쟙 -->
					    <button type="button" title="수업리스트" class="ic_list" onclick="openLeftMenu(1);"></button>
					</div>
			
			        <!-- s_main_con -->
			        <div class="main_con">    	
						<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>		    
						<sitemesh:write property='body'/>
					</div>
			        <!-- e_main_con -->
			
			        <!-- s_right_m -->
			        <!-- <div class="right_m" id="rightMenuDiv">
			            <button type="button" onclick="openRightMenu(this);" title="일정" class="ic_list"></button>
			        </div> -->
			    </div>
			</div>
			
			<a href="#" id="backtotop" title="To top" class="totop" ></a>
			
			<footer id="footer" class="ind">
		        <div class="footer_con">           
		            <div class="footer_scon">   
		                <ul class="f_menu">
						    <li><a href="#" title="새창" class="fmn1">개인정보처리방침</a></li>
		                    <li><a href="#" title="새창" class="fmn2"><span>|</span>이메일무단수집거부</a></li>                
					    </ul>   
		                <p class="addr">${ADDRESS }</p>
		                <a href="${HOME}" title="홈으로 가기" class="logo_wrap"><img src="${BOTTOM_LOGO}" alt="의과대학 로고" class="b_logo"></a>
		            </div>
		        </div>
			</footer>
		</div>
		

		
	</body>
</html>