<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="Description" content="의과대학 LMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="width=1100">
		
		<title>MLMS</title>
		
		<link rel="icon" type="img/png" href="${IMG}/favicon.png">
		<link rel="shortcut icon" href="${IMG}/favicon.png">
		<link rel="icon" href="${IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${IMG}/favicon.ico"> 
		<link rel="stylesheet" href="${CSS}/admin_style.css" type="text/css">
		<link rel="stylesheet" href="${CSS}/datetime_1.css" type="text/css" >
		<link rel="stylesheet" href="${CSS}/css/jquery.ui.datepicker.min.css" type="text/css" >
		<link rel="stylesheet" href="${CSS}/dev_admin_style.css" type="text/css">
		
		<script src="${JS}/lib/jquery-1.11.1.min.js"></script>
        <script src="${JS}/lib/datetimep_1.js"></script>
		<script src="${JS}/lib/timepicki.js"></script>
		<script src="${JS}/lib/monthdatepicker_ui.js"></script>
        <script src="${JS}/lib/jquery.form.min.js"></script>
		<script src="${JS}/lib/autosize.min.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/lib/jquery.ui.accordion.js"></script>
        <script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.ui-1.10.4.datepicker.min.js"></script>
		<script src="${JS}/common.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				topPopupInit();
				initPageTopButton();
				customSelectBoxInit();
				$(document).mouseup(function(e){
					layerPopupCloseInit(['div[name="myProfilePopupDiv"]']);
				});
							    
			    $(".btn_l").bind("click", function(){
			        $(".btn_l").toggleClass("move-trigger_l");
			        $(".aside_l").toggleClass("folding");
			        $(".main_con").toggleClass("");
			    });
			    
			    $(function() {
					$( ".boardwrap" ).accordion({
						collapsible: true,
						active: false
					});
				});
			    
			    $(document).on("click", "#yearListAdd span", function(event){
			    	getAcademicList($(this).attr("data-value"));
			    });
			    
				if(!isEmpty(getCookie("acaYear"))){
					$("#year").attr("data-value",getCookie("acaYear"));
					$("#year").text(getCookie("acaYear"));
				}
				
				if(!isEmpty(getCookie("l_seq"))){	
					$("#l_seq").attr("data-value", getCookie("l_seq"));
					$("#m_seq").attr("data-value", getCookie("m_seq"));
					$("#s_seq").attr("data-value", getCookie("s_seq"));
					$("#aca_seq").attr("data-value", getCookie("aca_seq"));		
					curriculumSearch();
								
					acasystemStageOfList(1,getCookie("l_seq"));				
					acasystemStageOfList(1,getCookie("l_seq"), getCookie("m_seq"));					
					acasystemStageOfList(1,getCookie("l_seq"), getCookie("m_seq"), getCookie("s_seq"));					
					acasystemStageOfList(1,getCookie("l_seq"), getCookie("m_seq"), getCookie("s_seq"), getCookie("aca_seq"));
				}

				
			    getYearList();
			    acasystemStageOfList(1);
			});
			
			$(document).click(function(){
			    $.datetimepicker.setLocale('kr');
			    $('.dateyearpicker-input_1').datetimepicker({
			        timepicker:false,
			        datepicker:true,
			        format:'y-m-d',
			        formatDate:'y-m-d'
			    });
			});
			
			function getYearList(){
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/lesson/yearList",
		            data: {    

		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
							var htmls = '';
							$.each(data.yearList, function(index){
								htmls+='<span class="uoption" data-value="'+this.year+'">'+this.year+'</span>';
							});
							
							$("#yearListAdd").html(htmls);							
							
		            	}else
		            		alert("학사 tahun 가져오기 실패");
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        });						
			}
			
			function acasystemStageOfList(level, l_seq, m_seq, s_seq){
                if(level == ""){
                    alert("분류 없음");
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "${HOME}/ajax/admin/academic/academicSystem/acaStageOfList",
                    data: {
                        "level" : level,
                        "l_seq" : l_seq,                       
                        "m_seq" : m_seq,
                        "s_seq" : s_seq
                        },
                    dataType: "json",
                    success: function(data, status) {
                        var htmls = '';
                        
                        if(level == 1){
                        	htmls = '<span class="uoption firstseleted" data-value="" onClick="lSeqSet(0);">pilih</span>';
                        	$.each(data.list, function(index){
                                htmls +=  '<span class="uoption" data-value="'+this.aca_system_seq+'" onClick="lSeqSet('+this.aca_system_seq+');">'+this.aca_system_name+'</span>';
                            });
                            $("#lacaListAdd").html(htmls); 
                            
                            if(!isEmpty(getCookie("l_seq"))){
                            	if($("#lacaListAdd span[data-value="+getCookie("l_seq")+"]").length != 0)
                            		$("#lacaListAdd span[data-value="+getCookie("l_seq")+"]").click();
                            	else
                            		$("#lacaListAdd span:eq(0)").click();
                            }else{
                            	$("#lacaListAdd span:eq(0)").click();
                            }
                        }                      
                        else if(level == 2){
                        	htmls = '<span class="uoption firstseleted" data-value="" onClick="mSeqSet(0);">jurusan</span>';
                        	$.each(data.list, function(index){
                                htmls +=  '<span class="uoption" data-value="'+this.aca_system_seq+'" onClick="mSeqSet('+this.aca_system_seq+');">'+this.aca_system_name+'</span>';
                            });
                            $("#macaListAdd").html(htmls);
                            
                            if(!isEmpty(getCookie("m_seq"))){
                            	if($("#macaListAdd span[data-value="+getCookie("m_seq")+"]").length != 0)
                            		$("#macaListAdd span[data-value="+getCookie("m_seq")+"]").click();
                            	else
                            		$("#macaListAdd span:eq(0)").click();
                            }else{
                            	$("#macaListAdd span:eq(0)").click();
                            }
                        }else if(level == 3){
                        	htmls = '<span class="uoption firstseleted" data-value="" onClick="sSeqSet(0);">tahun ajaran</span>';
                        	$.each(data.list, function(index){
                                htmls +=  '<span class="uoption" data-value="'+this.aca_system_seq+'" onClick="sSeqSet('+this.aca_system_seq+');">'+this.aca_system_name+'</span>';
                            });
                            $("#sacaListAdd").html(htmls);
                            
                            if(!isEmpty(getCookie("s_seq"))){
                            	if($("#sacaListAdd span[data-value="+getCookie("s_seq")+"]").length != 0)
                            		$("#sacaListAdd span[data-value="+getCookie("s_seq")+"]").click();
                            	else
                            		$("#sacaListAdd span:eq(0)").click();
                            }else{
                            	$("#sacaListAdd span:eq(0)").click();
                            }                            
                        }else{
                        	htmls = '<span class="uoption firstseleted" data-value="">periode</span>';
                        	$.each(data.list, function(index){
                                htmls +=  '<span class="uoption" data-value="'+this.aca_system_seq+'">'+this.aca_system_name+'</span>';
                            });
                            $("#acaListAdd").html(htmls);

                            if(!isEmpty(getCookie("aca_seq"))){
                            	if($("#acaListAdd span[data-value="+getCookie("aca_seq")+"]").length != 0)
                            		$("#acaListAdd span[data-value="+getCookie("aca_seq")+"]").click();
                            	else
                            		$("#acaListAdd span:eq(0)").click();
                            }else{
                            	$("#acaListAdd span:eq(0)").click();
                            }
                        }
                    },
                    error: function(xhr, textStatus) {
                        //alert("오류가 발생했습니다.");
                        document.write(xhr.responseText);
                    },beforeSend:function() {
                    },
                    complete:function() {
                    }
                }); 
           	}
			
			function getAcademicList(year){
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/lesson/acaList",
		            data: {
		            	"year" : year
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
							var htmls = '<span class="uoption firstseleted" data-value="">pilih</span>';
							$.each(data.academicList, function(index){
								htmls+='<span class="uoption" data-value="'+this.aca_seq+'">'+this.academic_name+'</span>';
							});						
							$("#academicListAdd").html(htmls);
							
							if(!isEmpty(getCookie("academic"))){
								if($("#academicListAdd span[data-value="+getCookie("academic")+"]").length != 0)
                            		$("#academicListAdd span[data-value="+getCookie("academic")+"]").click();
								else
									$("#academicListAdd span:eq(0)").click();									
                            }else{
                            	$("#academicListAdd span:eq(0)").click();
                            }
							 
		            	}else
		            		alert("학사리스트 가져오기 실패");
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        });						
			}
			
			function lSeqSet(seq){
				acasystemStageOfList(2, seq);
                $("#m_seq").text("jurusan");
                $("#m_seq").attr("data-value","");
                $("#s_seq").text("tahun ajaran");
                $("#s_seq").attr("data-value","");
                $("#aca_seq").text("periode");
                $("#aca_seq").attr("data-value","");
			}
			
			function mSeqSet(seq){
				acasystemStageOfList(3, "", seq);
                $("#s_seq").text("tahun ajaran");
                $("#s_seq").attr("value","");
                $("#aca_seq").text("periode");
                $("#aca_seq").attr("data-value","");
			}
			
			function sSeqSet(seq){
				acasystemStageOfList(4, "", "", seq);
                $("#aca_seq").text("periode");
                $("#aca_seq").attr("data-value","");
			}
			
			function curriculumSearch(){
				if(!isEmpty($("#year").attr("data-value")))
					setCookie("acaYear", $("#year").attr("data-value"), 1);
				else
					setCookie("acaYear", "", -1);

				if(!isEmpty($("#l_seq").attr("data-value")))
					setCookie("l_seq", $("#l_seq").attr("data-value"), 1);
				else
					setCookie("l_seq", "", -1);
				
				if(!isEmpty($("#m_seq").attr("data-value")))
					setCookie("m_seq", $("#m_seq").attr("data-value"), 1);
				else
					setCookie("m_seq", "", -1);				

				if(!isEmpty($("#s_seq").attr("data-value")))
					setCookie("s_seq", $("#s_seq").attr("data-value"), 1);
				else
					setCookie("s_seq", "", -1);	
				
				if(!isEmpty($("#aca_seq").attr("data-value")))
					setCookie("aca_seq", $("#aca_seq").attr("data-value"), 1);
				else
					setCookie("aca_seq", "", -1);				

				if(!isEmpty($("#academic").attr("data-value")))
					setCookie("academic", $("#academic").attr("data-value"), 1);
				else
					setCookie("academic", "", -1);			
				
				if(isEmpty($("#year").attr("data-value"))){
					alert("tahun를 pilih해주세요.");
					return;
				}
				
				if($("input[name=pageName]").val()=="monitoring")
					getCurrList(1);
				
				$.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/admin/lesson/leftCurrList",
			        data:{
			        	"year" : $("#year").attr("data-value")
			        	,"aca_seq" : $("#academic").attr("data-value")
			        	,"laca" : $("#l_seq").attr("data-value")
			        	,"maca" : $("#m_seq").attr("data-value")
			        	,"saca" : $("#s_seq").attr("data-value")			        	
			        	,"aca_system_seq" : $("#aca_seq").attr("data-value")
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
							var htmls = "";
							$("#currListAdd").empty();
							htmls = '<div class="title" onClick="location.href=\'${HOME}/admin/lesson\'">'
	        				+'<div class="tt_wrap"><span class="sp_tt">Keseluruhan교육과정</span></div></div>'
	        				+'<ul class="panel" data-name="ALL" style="display:none">'
	        				+'<li class="wrap">'      
	        				+'</li></ul>';
							$("#currListAdd").append(htmls);
			        		$.each(data.currList, function (index){
			        			var percent = 0;
			        			var currDate = "periode 미입력";
			        			var curr_seq = this.curr_seq;
			        			htmls = "";
			        			if(!isEmpty(this.curr_start_date))
			        				currDate = "["+this.curr_start_date+" ~ "+this.curr_end_date+"]";
			        			
			        			if(this.total_period == 0)
			        				percent = 0;
								else
									percent = Math.floor(this.pre_lp_cnt/this.total_period*100);
			        			
			        			htmls = '<div class="title">'
			        				+'<div class="tt_wrap"><span class="sp_tt">'+this.curr_name+'</span>'
			        				+'<span class="sp_tm">'+currDate+'</span></div>'			        				
			        				+'</div>'
			        				+'<ul class="panel" data-name="'+curr_seq+'">'
			        				+'<li class="wrap">'
			        				+'<button type="button" class="btn_go1" onclick="pageMoveCurriculumView('+this.curr_seq+');">교육과정 계획서 바로가기<span class="sign">▶</span></button>'      
			        				+'</li>'
			        				+'<li class="wrap">'
			        				+'<div class="wrap_prg">'         
			        				+'<div class="prg"><span class="bar" style="width:'+(percent*2)+'px;"></span></div>'
			        				+'<span class="sp1">'+percent+'%</span>'
			        				+'</div>'
			        				+'</li>';
		        				if(this.countInfo.preCnt > 0){
		    						htmls+= '<li class="wrap_more" data-name="'+curr_seq+'">'
	    			              	+'<button class="more1" onClick="getLessonPlanList('+curr_seq+','+this.countInfo.preCnt+');">'                        
	    			              	+'이전 수업 더보기</button></li>';              							
		    					}
			        			$.each(this.lpList, function(index){
			        				htmls += '<li class="wrap" data-name="'+curr_seq+'">';
			        				htmls += '<span class="list_date">' + this.lesson_date + '</span>';
									if (isEmpty(this.lesson_subject)) {//수업명 미registrasi시
										htmls += '<span class="tt_1 nonregi" onClick="pageMoveLessonPlanView('+this.lp_seq+','+curr_seq+');">수업명(미registrasi)</span>';
									} else {
										htmls += '<span class="tt_1" onClick="pageMoveLessonPlanView('+this.lp_seq+','+curr_seq+');">' + this.lesson_subject +'</span>';
									}
									
									htmls += '</li>';
			        			});
			        			
			        			if(this.countInfo.nextCnt <= this.countInfo.totalCnt){
			        				htmls += '<li class="wrap_more" data-name="'+curr_seq+'">'
			                    		+'<button class="more2" onClick="getLessonPlanList('+curr_seq+','+this.countInfo.nextCnt+');">'                        
			                        	+'수업 더보기</button></li>';					
								}    
			        			htmls += '</ul>';

				        		$("#currListAdd").append(htmls);
			        		});
							$("#currListAdd").accordion({collapsible: true, active: false}).accordion("refresh");
			        	} else {
			        		
			        	}
			        },
			        error: function(xhr, textStatus) {
// 			            alert(textStatus);
			            document.write(xhr.responseText);
			        }
			    });
			}			
			
			function getLessonPlanList(curr_seq, page){
				$.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/admin/lesson/leftCurrList/lpList",
			        data: {
			        	"curr_seq": curr_seq
			        	,"page" : page
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if(data.status=="200"){
			        		$("li[data-name="+curr_seq+"]").remove();
			        		var htmls = "";
	
				        	if(data.countInfo.preCnt > 0){
	    						htmls+= '<li class="wrap_more" data-name="'+curr_seq+'">'
    			              	+'<button class="more1" onClick="getLessonPlanList('+curr_seq+','+data.countInfo.preCnt+');">'                        
    			              	+'이전 수업 더보기</button></li>';              							
	    					}
		        			$.each(data.lpList, function(index){
		        				htmls += '<li class="wrap" data-name="'+curr_seq+'">';
		        				htmls += '<span class="list_date">' + this.lesson_date + '</span>';
								if (isEmpty(this.lesson_subject)) {//수업명 미registrasi시
									htmls += '<span class="tt_1 nonregi" onClick="pageMoveLessonPlanView('+this.lp_seq+','+curr_seq+');">수업명(미registrasi)</span>';
								} else {
									htmls += '<span class="tt_1" onClick="pageMoveLessonPlanView('+this.lp_seq+','+curr_seq+');">' + this.lesson_subject +'</span>';
								}
								
								htmls += '</li>';
		        			});
		        			
		        			if(data.countInfo.nextCnt <= data.countInfo.totalCnt){
		        				htmls += '<li class="wrap_more" data-name="'+curr_seq+'">'
		                    		+'<button class="more2" onClick="getLessonPlanList('+curr_seq+','+data.countInfo.nextCnt+');">'                        
		                        	+'수업 더보기</button></li>';					
							}    
							$("ul[data-name="+curr_seq+"]").append(htmls);
			        	}
			        },
			        error: function(xhr, textStatus) {
			            alert("오류가 발생했습니다.");
			        }
			    });				
			}
			
			function pageMoveCurriculumView(curr_seq){
		 		post_to_url("${HOME}/admin/lesson/curriculum", {"curr_seq":curr_seq, "year" : $("#year").attr("data-value")});		
			}
			
			function pageMoveLessonPlanView(lp_seq, curr_seq){
		 		post_to_url("${HOME}/admin/lesson/lessonPlan", {"lp_seq":lp_seq, "curr_seq":curr_seq, "year" : $("#year").attr("data-value")});		
			}
		</script>
		<sitemesh:write property='head'/>
	</head>

	<body>
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
		
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a>
			<a href="#container">컨텐츠 바로가기</a>
		</div>
		
		<div id="wrap">
			<div id="top_pop">
				<div class="wrap">
					<div class="slide-wrap">
						<ul>
							<li class="tt"><img src="${IMG}/pop001.png" alt="의과대학 의학교육 penilaian인증 인증 획득 4년"></li>
						</ul>
					</div>
				</div>
			</div>
			
			<header class="header">
				<div class="header_top">
					<div class="top_wrap">       
						<div class="top_m">
							<a href="javascript:void(0);" id="topPopupBtn" class="a_m a1" onclick="javascript:topPopupToggle();">팝업닫기</a>
							<c:choose>
								<c:when test='${HOME ne ""}'>
									<a href="${HOME}" class="a_m a3">HOME</a>
								</c:when>
								<c:otherwise>
									<a href="/" class="a_m a3">HOME</a>
								</c:otherwise>
							</c:choose>
							<a href="#" class="a_m a4">사이트맵</a>
							<c:choose>
								<c:when test="${sessionScope.S_USER_ID ne null}">
									<a href="${HOME}/logout" class="a_m a5">로그아웃</a>
								</c:when>
								<c:otherwise>
									<a href="${HOME}/login" class="a_m a5">로그인</a>
								</c:otherwise>
							</c:choose>
							<a href="${HOME_URL }" class="a_m a6">기관Home</a>
						</div>
						<c:if test="${sessionScope.S_USER_ID ne null}">
							<div class="a_mp a7" onclick="myProfilePopup()">
								<c:choose>
									<c:when test='${sessionScope.S_USER_PICTURE_PATH ne null and sessionScope.S_USER_PICTURE_PATH ne ""}'>
										<span class="pt01"><img src="${sessionScope.S_USER_PICTURE_PATH}" alt="나의 대표 이미지" class="pt_img"></span>
									</c:when>
									<c:otherwise>
										<span class="pt01"><img src="${DEFAULT_PICTURE_IMG}" alt="나의 대표 이미지" class="pt_img"></span>
									</c:otherwise>
								</c:choose>
								<span class="ssp1">${sessionScope.S_USER_NAME} (${sessionScope.S_USER_DEPARTMENT_NAME})</span>
							</div>
							<div id="pop_pop9" name="myProfilePopupDiv">
								<div class="spop9 show" id="spop9">
									<span class="tt">${sessionScope.S_USER_NAME} (${sessionScope.S_USER_DEPARTMENT_NAME})</span>
									<span class="tt">${sessionScope.S_USER_EMAIL}</span>
									<button onclick="location.href='${HOME}/common/SLife/qna/list'" class="btn btn05">1:1 문의</button>
									<span onclick="$('#pop_pop9').hide();" class="pop_close_s" title="닫기">X</span>
								</div>
							</div>
						</c:if>
					</div>
				</div>     
				
				<div class="gnbwrap">
					<div class="gnb_swrap">
						<h1 class="logo">
						<c:choose>
							<c:when test='${HOME ne ""}'>
								<a href="${HOME}" title="홈으로 가기"><img src="${TOP_LOGO}" alt="의과대학 로고"></a>
							</c:when>
							<c:otherwise>
								<a href="/" title="홈으로 가기"><img src="${TOP_LOGO}" alt="의과대학 로고">	</a>
							</c:otherwise>
						</c:choose>
						</h1>
					</div>
					
					<div class="gnb_fwrap">
						<div class="gnb_swrap2">
							<ul id="gnb" class="gnb">
								<c:forEach items="${sessionScope.S_ALLOW_MENU_LIST}" var="allowMenuList" step="1">
									<li><a href="${HOME}${allowMenuList.url}" <c:if test='${allowMenuList.now_menu_yn eq "Y" && allowMenuList.url ne ""}'>class="on"</c:if>>${allowMenuList.name}</a></li>
								</c:forEach>
							</ul>
						</div>
					</div>
				</div>
			</header>
			
			<!-- s_container_table -->
		<div id="container" class="container_table">
			<!-- s_contents -->
			<div class="contents main adm_ccschd">
				<!-- s_adm_topsch -->
				<div class="adm_topsch">
					<!-- s_wrap1_lms_uselectbox -->
					<div class="wrap1_lms_uselectbox">
						<div class="uselectbox">
							<span class="uselected" id="year" data-value=""></span> <span class="uarrow">▼</span>

							<div class="uoptions" id="yearListAdd">
							</div>
						</div>
					</div>
					<!-- e_wrap1_lms_uselectbox -->

					<span class="tt_1">sistem akademik</span>

					<!-- s_wrap1_lms_uselectbox -->
					<div class="wrap1_lms_uselectbox">
						<div class="uselectbox">
							<span class="uselected" id="l_seq" data-value=""></span> <span class="uarrow">▼</span>

							<div class="uoptions" id="lacaListAdd">
							</div>
						</div>
					</div>
					<!-- e_wrap1_lms_uselectbox -->

					<!-- s_wrap1_lms_uselectbox -->
					<div class="wrap1_lms_uselectbox">
						<div class="uselectbox">
							<span class="uselected" id="m_seq" data-value=""></span> <span class="uarrow">▼</span>

							<div class="uoptions" id="macaListAdd">
							</div>
						</div>
					</div>
					<!-- e_wrap1_lms_uselectbox -->

					<!-- s_wrap1_lms_uselectbox -->
					<div class="wrap1_lms_uselectbox">
						<div class="uselectbox">
							<span class="uselected" id="s_seq" data-value=""></span> <span class="uarrow">▼</span>

							<div class="uoptions" id="sacaListAdd">
							</div>
						</div>
					</div>
					<!-- e_wrap1_lms_uselectbox -->

					<!-- s_wrap1_lms_uselectbox -->
					<div class="wrap1_lms_uselectbox">
						<div class="uselectbox">
							<span class="uselected" id="aca_seq" data-value=""></span> <span class="uarrow">▼</span>

							<div class="uoptions" id="acaListAdd">
							</div>
						</div>
					</div>
					<!-- e_wrap1_lms_uselectbox -->


					<span class="tt_1">gelar akademik</span>

					<!-- s_wrap2_lms_uselectbox -->
					<div class="wrap2_lms_uselectbox">
						<div class="uselectbox">
							<span class="uselected" id="academic" data-value=""></span> <span class="uarrow">▼</span>

							<div class="uoptions" id="academicListAdd" style="width:300px;">
								
							</div>
						</div>
					</div>
					<!-- e_wrap2_lms_uselectbox -->
					<button class="btn_tt1" onClick="curriculumSearch();">Pencarian</button>
				</div>
				<!-- s_adm_topsch -->

				<!-- s_left_mcon -->
				<div class="left_mcon aside_l">
					<div class="boardwrap" id="currListAdd">
						
						
					</div>
				</div>

				<sitemesh:write property='body'/>
			</div>
			<!-- e_contents -->
		</div>
		<!-- e_container_table -->
			
			<a href="../#" id="backtotop" title="To top" class="totop" ></a>
			
			<footer id="footer">
		        <div class="footer_con">           
		            <div class="footer_scon">   
		                <ul class="f_menu">
						    <li><a href="#" title="새창" class="fmn1">개인정보처리방침</a></li>
		                    <li><a href="#" title="새창" class="fmn2"><span>|</span>이메일무단수집거부</a></li>                
					    </ul>   
		                <p class="addr">${ADDRESS }</p>
		                <a href="${HOME}" title="홈으로 가기" class="logo_wrap"><img src="${BOTTOM_LOGO}" alt="의과대학 로고" class="b_logo"></a>
		            </div>
		        </div>
			</footer>
			
		</div>
	</body>
</html>