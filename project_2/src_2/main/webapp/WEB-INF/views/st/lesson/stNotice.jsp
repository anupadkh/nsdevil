<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
	var page_num = 1;
	$(document).ready(function() {
		getMobileCurrLessonPlan(page_num);
		$(".more").click(function(){
			page_num++;
			getMobileCurrLessonPlan(page_num);
		});
	});

	function getMobileCurrLessonPlan(page_num){
		var url = "${HOME}/mobile/ajax/st/notice/list";
	
		$.ajax({
	        type: "POST",
	        url: url,
	        data: {
	        	"page" : page_num
	        },
	        dataType: "json",
	        success: function(data, status) {
	        	var htmls = "";
	        	
	            if (data.status == "200") {
	            	if(data.noticeList.length == 0){
	            		alert("데이터가 없습니다.");page_num--;
	            	}else{
		            	$.each(data.noticeList, function(index){
		            		var type = this.notice_type;
		            		var type_name = "";
		            		var type_title = "";
		            		var type_html = "";
		            		//tr 클래스 숫자 바꿔준다.
		            		var type_class = "";
		            		if(type=="board"){
		            			type_name = "[공지]";
		            			type_class = "1";
		            			type_html = this.full_title;
		            		}else if(type=="asgmtDeadline"){
		            			type_name = "[과제마감]";
		            			if(this.state == 0)
		            				type_title = "「"+this.sub_title+"」 과제 제출 마감일 입니다.";
		            			else if(this.state > 0)
		            				type_title = "「"+this.sub_title+"」 과제가 제출 마감 되었습니다.";
		            			else
		            				type_title = "「"+this.sub_title+"」 과제가 제출 마감 "+Math.abs(this.state)+"일 전입니다.";
		            			
		            			type_html = type_title;
		            		}else if(type=="asgmtAllSubmit"){
		            			type_name = "[과제전원제출]";
		            			type_title = "「"+this.sub_title+"」 과제가 전원제출 완료 되었습니다.";
		            			type_html = type_title;
		            		}else if(type=="memo"){
		            			type_name = "[개인일정]";
		            			type_html = this.full_title;
		            			
		            			//개인일정 장소 있으면
		            			if(!isEmpty(this.sub_title))
		            				type_html += '<br>장소 : ' + this.sub_title;
		            				
		            		}else if(type=="UnlessonPlan"){
		            			type_name = "[수업계획서 미registrasi]";
		            			type_title = "「"+this.full_title+"」수업계획서 미registrasi 상태입니다.";
		            			type_html = type_title;
		            		}else if(type=="asgmtNotSubmit"){
		            			type_name = "[과제 미제출]";
		            			type_title = "「"+this.sub_title+"」 과제 미제출 상태입니다.";
		            			type_html = type_title;
		            		}else if(type=="qna"){
		            			type_name = "[1:1질의응답]";
		            			type_title = "에게 1:1질의를 받으셨습니다.";
		            			
		            			//end_time = 유저 레벨, sub_title = 유저 이름
		            			if(this.end_time == "4")
		            				type_title = " 학생 " + type_title;
		            			else if(this.end_time == "2" || this.end_time == "3")
		            				type_title = " 교수 " + type_title;
		            			
		            			type_html = this.sub_title+type_title;
		            		}else if(type=="qnaAnswer"){
		            			type_name = "[1:1질의응답 답변]";
		            			type_title = "에게 1:1질의 답변을 받으셨습니다.";
		            			
		            			//end_time = 유저 레벨, sub_title = 유저 이름
		            			if(this.end_time == "4")
		            				type_title = " 학생 " + type_title;
		            			else if(this.end_time == "2" || this.end_time == "3")
		            				type_title = " 교수 " + type_title;
		            			
		            			type_html = this.sub_title+type_title;
		            		}
		            		switch(this.day){
			            		case 0 : dayOfWeek="일";break;
			            		case 1 : dayOfWeek="월";break;
			            		case 2 : dayOfWeek="화";break;
			            		case 3 : dayOfWeek="수";break;
			            		case 4 : dayOfWeek="목";break;
			            		case 5 : dayOfWeek="금";break;
			            		case 6 : dayOfWeek="토";break;
		            		}
		            			        							
							htmls='<li class="notice" onClick="">';
					        htmls+='<div class="dv w_ic">';
					        htmls+='<span class="am_option opt1"></span>';
					        htmls+='</div>';
					        htmls+='<div class="dv w_con">';
					        htmls+='<span class="am_opt_tt"><span class="tt">'+type_name+'</span>'+type_html+'</span>';
					        htmls+='<span class="am_opt_tts">'+this.real_date+'</span>';
					        htmls+='</div>';
					        htmls+='</li>';
			            	$("#noticeListAdd").append(htmls);
		            	});
	            	}
	            } else {
	                alert("알림 리스트 가져오기 실패.");
	            }
	        },
	        error: function(xhr, textStatus) {
	            //alert("오류가 발생했습니다.");
	            document.write(xhr.responseText);
	        },
	        beforeSend:function() {
	            $.blockUI();
	        },
	        complete:function() {
	            $.unblockUI();
	        }
	    });
	}
</script>
</head>
<body>
	<!-- s_tt_wrap -->
	<div class="tt_wrap">
		<h3 class="am_tt tt1">알림</h3>
	</div>
	<!-- e_tt_wrap -->
	<div class="pf_tdlwrap">
		<!-- s_lms_table_tdl -->
		<ul class="lms_table_tdl" id="noticeListAdd">
		</ul>
		<!-- e_lms_table_tdl -->
	</div>

	<div class="more_wrap">
		<button class="more">
			더 보기<span class="uarrow">▼</span>
		</button>
	</div>

	</div>
</body>
</html>