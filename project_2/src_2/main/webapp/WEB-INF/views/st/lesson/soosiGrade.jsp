<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
	$(document).ready(function() {
		getCurriculum();
		getSoosiScore();
	});		
	
	//교육과정 가져오기
	function getCurriculum() {
		$.ajax({
			type : "GET",
			url : "${HOME}/ajax/pf/lesson/curriculumInfo",
			data : {},
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {

					$("#curr_name").text(data.basicInfo.curr_name);
					$("#aca_system_name").text("(" + data.basicInfo.aca_system_name + ")");
                    
				} else {

				}
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}
	
	function getSoosiScore(){
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/st/lesson/soosiGrade/list",
            data: {    
            	"aca_seq" : "${aca_seq}"
            },
            dataType: "json",
            success: function(data, status) {
            	if(data.status=="200"){
            		var htmls="";
            		
            		$("#soosiListAdd").empty();
            		
            		if(data.soosiList.length == 0){
            			$("#scoreDiv").removeClass("class_0").addClass("class_x");
            		}
            		
            		$.each(data.soosiList , function(index){  				
            			
            			htmls='<tr class="">'
            				+'<td>'+this.src_date+'</td>'
                            +'<td class="t_l">'+this.curr_name+'</td>'
                            +'<td class="t_l">'+this.src_name+'</td>'
                            +'<td class="">'+this.name+'</td>'
                            +'<td>'+this.question_cnt+'</td>'
                            +'<td>'+this.answer_cnt+'</td>'
                            +'<td>'+this.score+'</td>'
                            +'<td>'+this.avg_score+'</td>'
                            +'<td>'+this.deviation+'</td>'
                            +'</tr>';
						
						$("#soosiListAdd").append(htmls);
            		});
            	}else
            		alert("실패");
            },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            }
        });					
	}
</script>
</head>
<body>
	<!-- s_tt_wrap -->
	<div class="tt_wrap">
		<h3 class="am_tt" style="margin-left:15px;">
			<span class="tt" id="curr_name"></span>
			<span class="tt_s" id="aca_system_name"></span>
		</h3>
	</div>
	<!-- e_tt_wrap -->

	<!-- s_tab_wrap_cc -->
	<div class="tab_wrap_cc">
		<button class="tab01 tablinks" onClick="pageMoveCurriculumView(${S_CURRICULUM_SEQ});">과정 계획</button>
		<button class="tab01 tablinks" onClick="">수업waktu표</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/unit'">단원manajemen</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/sfCurrResearch'">만족도조사</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/currAsgmt'">종합과제</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/feScore'">형성penilaian</button>
		<button class="tab01 tablinks active" onClick="location.href='${HOME}/st/lesson/soosiGrade'">nilai yang diinginkan</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/currGrade'">종합성적</button>	
	</div>
	<!-- e_tab_wrap_cc -->

	<div class="sub">
	<!-- s_sub_con -->
	<div class="sub_con rcard">

		<!-- s_tt_wrap -->
		<div class="tt_wrap grdcard">
			<h3 class="am_tt">
				<span class="tt_s">nilai yang diinginkan</span>
			</h3>
			<div class="w_r">
				<!-- <button class="btn_pdf" title="PDF다운로드"></button> -->
				<button class="btn_prt" title="인쇄하기"></button>
			</div>
		</div>
		<!-- e_tt_wrap -->

		<!-- s_rfr_con -->
		<!-- s_registrasi된 성적이 없을 때 class : class_x, 조회periode이 아닐 때 class : class_0 tambahkan -->
		<div class="rfr_con grd2" id="scoreDiv">
			<table class="mlms_tb card">
				<thead>
					<tr>
                           <th class="th01 bd01 wn3">hari dan tanggal</th>
                           <th class="th01 bd01 wn4">Nama Mata Kuliah</th>
                           <th class="th01 bd01 wn5">시험명</th>
                           <th class="th01 bd01 wn2">Dosen Penanggung jawab명</th>
                           <th class="th01 bd01 wn1">총문항</th>
                           <th class="th01 bd01 wn1">정답수</th>
                           <th class="th01 bd01 wn1">내점수</th>
                           <th class="th01 bd01 wn1">평균</th>
                           <th class="th01 bd01 wn1">편차</th>
                       </tr>
				</thead>
				<tbody id="soosiListAdd">
					
				</tbody>
			</table>

		</div>
		<!-- e_rfr_con -->

	</div>
	<!-- e_sub_con -->
	</div>
</body>
</html>