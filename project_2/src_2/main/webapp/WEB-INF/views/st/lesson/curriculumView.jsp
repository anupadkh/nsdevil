<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
	$(document).ready(function() {
		getCurriculum();
		
		var mo4 = document.getElementById('m_pop4');
		var button = document.getElementsByClassName('close4')[0];
		var b = document.getElementsByClassName('open4');
		for (var i = 0; i < b.length; i++) {
			b[i].onclick = function() {
				mo4.style.display = "block";
			}

			button.onclick = function() {
				mo4.style.display = "none";
			}

			window.onclick = function(event) {
				if (event.target == mo4) {
					mo4.style.display = "none";
				}
			}
		}
	});		
	
	//교육과정 가져오기
	function getCurriculum() {
		$.ajax({
			type : "GET",
			url : "${HOME}/ajax/pf/lesson/curriculum",
			data : {},
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {
					$("#addPfList tr").remove();

					var htmls = "";

					$("#curr_name").text(data.basicInfo.curr_name);
					$("#aca_system_name").text("(" + data.basicInfo.aca_system_name + ")");
					
					$("span[name=curr_name]").text(data.basicInfo.curr_name);
					$("span[name=aca_system_name]").text("(" + data.basicInfo.aca_system_name + ")");
					$("td[name=curr_code]").text(data.basicInfo.curr_code);
					$("td[name=curr_name]").text(data.basicInfo.curr_name);
					$("td[name=aca_name]").text(data.basicInfo.aca_name);
					$("td[name=aca_system_name]").text(data.basicInfo.aca_system_name);
					$("td[name=complete_name]").text(data.basicInfo.complete_name);
					$("td[name=administer_name]").text(data.basicInfo.administer_name);
					$("td[name=grade]").text(data.basicInfo.grade);
					$("td[name=target_name]").text(data.basicInfo.target_name);
					$("td[name=lesson_date]").text(data.basicInfo.curr_start_date + " ~ " + data.basicInfo.curr_end_date);
					
					if(isEmpty(data.basicInfo.curr_summary))
						$(".curr_summary").hide();
					if(isEmpty(data.basicInfo.reference_data))
						$(".reference_data").hide();
					if(isEmpty(data.basicInfo.management_plan))
						$(".management_plan").hide();
					if(isEmpty(data.basicInfo.curr_outcome))
						$(".curr_outcome").hide();
					
					$("td[name=curr_summary]").html(data.basicInfo.curr_summary);
					$("td[name=reference_data]").html(data.basicInfo.reference_data);
					$("td[name=management_plan]").html(data.basicInfo.management_plan);
					$("td[name=curr_outcome]").html(data.basicInfo.curr_outcome);
					
					var grade_method_name = "";
					if(data.basicInfo.grade_method_code == "00")
						grade_method_name = "이용안함";
					else if(data.basicInfo.grade_method_code == "01")
                              grade_method_name = "상대penilaian";
					else if(data.basicInfo.grade_method_code == "02")
                              grade_method_name = "절대penilaian";
					$("span[name=grade_method_name]").text(grade_method_name);					

					//주당평균waktu 구하기
					var period_avg = (data.basicInfo.total_period / data.basicInfo.curr_week) + "";
					if (period_avg.indexOf(".") > 0)
						period_avg = period_avg.substring(0, period_avg.indexOf(".") + 2);

					$("td[name=period_avg]").text(period_avg + " / 1주");
					$("td[name=lecture_cnt]").text(data.basicInfo.lecture_period+"waktu");
					$("td[name=unlecture_cnt]").text(data.basicInfo.unlecture_period+"waktu");
					$("td[name=lp_cnt]").text(data.basicInfo.period_cnt + "waktu");
					$("td[name=etc_period]").text(data.basicInfo.etc_period + "waktu");
					$("td[name=total_period_cnt]").text(data.basicInfo.total_period+"waktu");
					$("td[name=lesson_week]").text(data.basicInfo.curr_week);
					//Dosen Penanggung jawab
					$("#addPfList").empty();

					$.each(data.mpfList,function() {
						htmls += '<tr class="tr01">';
						htmls += '<td class="ta_c">책임<br>교수</td>';
						htmls += '<td class="ta_c">'+ this.name+ '</td>';
						htmls += '<td class="ta_c">'+ this.department_name+ '</td>';
						htmls += '<td class="ta_c">'+ this.specialty+ '</td>';
						htmls += '<td class="ta_c ls_1">'+ this.tel + '</td>';
						htmls += '<td class="ta_c ls_1"><a class="mailto_1" href="mailto:testid0000@gmail.com" target="_top">'+ this.email+ '</a></td></tr>';
					});
					$("#addPfList").append(htmls);
					htmls = "";

					//부Dosen Penanggung jawab
					$.each(data.dpfList,function() {
						htmls += '<tr class="tr01">';
						htmls += '<td class="ta_c">부책임<br>교수</td>';
						htmls += '<td class="ta_c">' + this.name + '</td>';
						htmls += '<td class="ta_c">' + this.department_name + '</td>';
						htmls += '<td class="ta_c">' + this.specialty + '</td>';
						htmls += '<td class="ta_c ls_1">' + this.tel + '</td>';
						htmls += '<td class="ta_c ls_1"><a class="mailto_1" href="mailto:testid0000@gmail.com" target="_top">' + this.email + '</a></td></tr>';
					});
					$("#addPfList").append(htmls);
					htmls = "";

					//일반교수
					$.each(data.gpfList,function() {
						htmls += '<tr class="tr01">';
						htmls += '<td class="ta_c">교수</td>';
						htmls += '<td class="ta_c">' + this.name + '</td>';
						htmls += '<td class="ta_c">' + this.department_name + '</td>';
						htmls += '<td class="ta_c">' + this.specialty + '</td>';
						htmls += '<td class="ta_c ls_1">' + this.tel + '</td>';
						htmls += '<td class="ta_c ls_1"><a class="mailto_1" href="mailto:testid0000@gmail.com" target="_top">' + this.email + '</a></td></tr>';
					});
					$("#addPfList").append(htmls);
					htmls = '';

					//tidak ada perkuliahan 가져오기
					var lessonMethod_code_arry = new Array();
					var lessonMethod_name_arry = new Array();
					var lessonMethod_cnt_arry = new Array();

					$.each(data.lessonMethodList, function(index) {
						lessonMethod_code_arry[index] = this.code;
						lessonMethod_name_arry[index] = this.code_name;
						lessonMethod_cnt_arry[index] = this.lp_cnt;
					});

					var for_int = 0;
					if(lessonMethod_code_arry.length <= 4)
						for_int = 1;
					else if ((lessonMethod_code_arry.length) % 4 == 0)
						for_int = lessonMethod_code_arry.length / 4;
					else
						for_int = parseInt((lessonMethod_code_arry.length + 1) / 4) + 1;

					var cnt_chk = "";
					var popup_class = "";

					for (var i = 0; i < for_int * 4; i += 4) {
						htmls += '<tr class="tr01">';
						if (i == 0)
							htmls += '<td rowspan="' + (for_int * 2) + '" class="th01 bd01 bd02 w6">tidak ada perkuliahan<br>상세</td>';
						if (lessonMethod_name_arry.length > i)
							htmls += '<td class="th01 w8">' + lessonMethod_name_arry[i] + '</td>';
						else
							htmls += '<td class="th01 w8"></td>';
						if (lessonMethod_name_arry.length > (i + 1))
							htmls += '<td class="th01 w8">' + lessonMethod_name_arry[i + 1] + '</td>';
						else
							htmls += '<td class="th01 w8"></td>';
						if (lessonMethod_name_arry.length > (i + 2))
							htmls += '<td class="th01 w8">' + lessonMethod_name_arry[i + 2] + '</td>';
						else
							htmls += '<td class="th01 w8"></td>';
						if (lessonMethod_name_arry.length > (i + 3))
							htmls += '<td class="th01 w8">' + lessonMethod_name_arry[i + 3] + '</td>';
						else
							htmls += '<td class="th01 w8"></td>';

						htmls += '</tr><tr class="tr01">';
						if (lessonMethod_code_arry.length > i) {
							if (lessonMethod_cnt_arry[i] == 0) {
								cnt_chk = "-";
								popup_class = "";
							} else {
								cnt_chk = lessonMethod_cnt_arry[i];
								popup_class = " pop";
							}
							htmls += '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="' + cnt_chk + '"></td>';
						} else
							htmls += '<td class="w6 ta_c"><input type="text" readonly class="ip04" value=""></td>';
						if (lessonMethod_code_arry.length > (i + 1)) {
							if (lessonMethod_cnt_arry[i + 1] == 0) {
								cnt_chk = "-";
								popup_class = "";
							} else {
								cnt_chk = lessonMethod_cnt_arry[i + 1];
								popup_class = " pop";
							}
							htmls += '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="' + cnt_chk + '"></td>';
						} else
							htmls += '<td class="w6 ta_c"><input type="text" readonly class="ip04" value=""></td>';
						if (lessonMethod_code_arry.length > (i + 2)) {
							if (lessonMethod_cnt_arry[i + 2] == 0) {
								cnt_chk = "-";
								popup_class = "";
							} else {
								cnt_chk = lessonMethod_cnt_arry[i + 2];
								popup_class = " pop";
							}
							htmls += '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="' + cnt_chk + '"></td>';
						} else
							htmls += '<td class="w6 ta_c"><input type="text" readonly class="ip04" value=""></td>';
						if (lessonMethod_code_arry.length > (i + 3)) {
							if (lessonMethod_cnt_arry[i + 3] == 0) {
								cnt_chk = "-";
								popup_class = "";
							} else {
								cnt_chk = lessonMethod_cnt_arry[i + 3];
								popup_class = " pop";
							}
							htmls += '<td class="w6 ta_c"><input type="text" readonly class="ip04' + popup_class + '" value="' + cnt_chk + '"></td>';
						} else
							htmls += '<td class="w6 ta_c"><input type="text" readonly class="ip04" value=""></td>';
						htmls += '</tr>';
					}
					$("#lessonMethodList").empty();
					$("#lessonMethodList").append(htmls);

					//형성penilaian 리스트        		
					htmls = '';
					var fe_sum = 0;
					var fe_name_arry = new Array();
					var fe_cnt_arry = new Array();

					$.each(data.formationEvaluationCs, function(index) {
						fe_name_arry[index] = this.code_name
						fe_cnt_arry[index] = this.cnt;
						fe_sum += fe_cnt_arry[index] * 1;
					});

					for_int = 0;
					if(fe_name_arry.length <= 5)
						for_int = 1;
					else if ((fe_name_arry.length) % 5 == 0)
						for_int = fe_name_arry.length / 5;
					else
						for_int = parseInt((fe_name_arry.length + 1) / 5) + 1;

					for (var i = 0; i < for_int * 5; i += 5) {
						htmls += '<tr class="tr01">';
						if (fe_name_arry.length > i)
							htmls += '<td class="th01 w6">' + fe_name_arry[i] + '</td>';
						else
							htmls += '<td class="th01 w6"></td>';

						if (fe_name_arry.length > (i + 1))
							htmls += '<td class="th01 w8">' + fe_name_arry[i + 1] + '</td>';
						else
							htmls += '<td class="th01 w8"></td>';

						if (fe_name_arry.length > (i + 2))
							htmls += '<td class="th01 w8">' + fe_name_arry[i + 2] + '</td>';
						else
							htmls += '<td class="th01 w8"></td>';

						if (fe_name_arry.length > (i + 3))
							htmls += '<td class="th01 w8">' + fe_name_arry[i + 3] + '</td>';
						else
							htmls += '<td class="th01 w8"></td>';

						if (fe_name_arry.length > (i + 4))
							htmls += '<td class="th01 w8">' + fe_name_arry[i + 4] + '</td>';
						else
							htmls += '<td class="th01 w8">subtotal</td>';
						htmls += '</tr>';

						htmls += '<tr class="tr01">';
						if (fe_name_arry.length > i)
							htmls += '<td class="bd01 w6 ta_c">'+fe_cnt_arry[i]+'</td>';
						else
							htmls += '<td class="bd01 w6 ta_c"></td>';

						if (fe_name_arry.length > (i + 1))
							htmls += '<td class="bd01 w6 ta_c">'+fe_cnt_arry[i+1]+'</td>';
						else
							htmls += '<td class="bd01 w6 ta_c"></td>';

						if (fe_name_arry.length > (i + 2))
							htmls += '<td class="bd01 w6 ta_c">'+fe_cnt_arry[i+2]+'</td>';
						else
							htmls += '<td class="bd01 w6 ta_c"></td>';

						if (fe_name_arry.length > (i + 3))
							htmls += '<td class="bd01 w6 ta_c">'+fe_cnt_arry[i+3]+'</td>';
						else
							htmls += '<td class="bd01 w6 ta_c"></td>';

						if (fe_name_arry.length > (i + 4))
							htmls += '<td class="bd01 w6 ta_c">'+fe_cnt_arry[i+4]+'</td>';
						else
							htmls += '<td class="bd01 w6 ta_c">'+fe_sum+'</td>';

						htmls += '</tr>';
					}
					$("#feAdd").empty();
					$("#feAdd").append(htmls);
					htmls = '';

					//총괄penilaian기준
					$.each(data.summativeEvaluationCs,function(index) {
						htmls += '<tr class="tr01">';
						htmls += '<td class="w6 ta_c">' + this.eval_method_name + '</td>';
						htmls += '<td class="w6 ta_c">' + this.eval_domain_name + '</td>';
						htmls += '<td class="w6_1 ta_c">' + this.importance + '%</td>';
						htmls += '<td class="w8_1 ta_c">'+ this.eval_method_text + '</td>';
						htmls += '<td class="w8_1 ta_c">' + this.eval_etc	+ '</td></tr>';
					});
					$("#AddEvalList").empty();
					$("#AddEvalList").append(htmls);
					htmls = '';

					//syarat kelulusan
					$.each(data.mpCurriculumGraduationCapabilityList,function(index) {
						htmls += '<tr class="tr01">';
						htmls += '<td class="">'+ this.fc_name+ '</td>';
						htmls += '<td class="">' + this.process_result + '</textarea></td>';
						htmls += '<td class="ta_c">' + this.teaching_method + '</textarea></td>';
						htmls += '<td class="ta_c">' + this.evaluation_method + '</td></tr>';
					});

					$("#mpFinishCapabilityList").empty();
					$("#mpFinishCapabilityList").append(htmls);
					
					if(data.mpCurriculumGraduationCapabilityList.length == 0){
						$(".finishCapaView").hide();
					}
					
/* 
					//nilai 기준
                    var htmls1='<tr class="tr01">';
                    var htmls2='<tr class="tr01">';
                    var sumGrade = 0;

                    $.each(data.gradeStandard, function(index){
                    	htmls1+='<td class="th01 w9_1">'+this.code_name+'</td>';
                        htmls2+='<td class="bd01 ta_c">'+this.grade_importance+'%</td>';
                        sumGrade += this.grade_importance;
                    });
                    htmls1+='<td class="th01 w9_2">계</td></tr>';
                    htmls2+='<td class="bd01 ta_c" name="totalGradeImportance">'+sumGrade+'%</td></tr>';
                    $("#gradeStandardList").html(htmls1+htmls2);
                     */
				} else {

				}
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}

	function detailLesson(code){
        
        //tidak ada perkuliahan 상세 없을경우 0으로 들어온다.
        if(code=="0")
            return false;
        
        $.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/lessonMethodSujectList",
            data: {
                "code" : code,                   
            },
            dataType: "json",
            success: function(data, status) {
                var html = '';            
                $.each(data.lessonMethodSujectList,function(index){
                    html += '<div class="con_s2">';
                    html += '<span class="tt1" title="'+this.lesson_subject+'">'+this.lesson_subject+'</span>';
                    html += '<div class="a_mp">';      
                    html += '<span class="pt01"><img src="" alt="사진" class="pt_img"></span><span class="ssp1">'+this.name+'('+this.specialty+')</span>';
                    html += '</div></div>';                
                });

                $("#lessonDetailAdd").empty();
                $("#lessonDetailAdd").append(html);
                $("#m_pop4").show();
            },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                document.write(xhr.responseText);
            }
        }); 
    }
	
	function currPrint(){
		var initBody = document.body.innerHTML;
		window.onbeforeprint = function () {

			document.body.innerHTML = '<div id="currDiv" class="tabcontent tabcontent1">'+document.getElementById("currDiv").innerHTML+'</div>';
		}
		window.onafterprint = function () {

			document.body.innerHTML = initBody;

		}
		window.print();
	}
</script>
</head>
<body>
	<!-- s_tt_wrap -->
	<div class="tt_wrap">
		<h3 class="am_tt" style="margin-left:15px;">
			<span class="tt" id="curr_name"></span>
			<span class="tt_s" id="aca_system_name"></span>
		</h3>
	</div>
	<!-- e_tt_wrap -->

	<!-- s_tab_wrap_cc -->
	<div class="tab_wrap_cc">
		<button class="tab01 tablinks active" onClick="pageMoveCurriculumView(${S_CURRICULUM_SEQ});">과정 계획</button>
		<button class="tab01 tablinks" onClick="">수업waktu표</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/unit'">단원manajemen</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/sfCurrResearch'">만족도조사</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/currAsgmt'">종합과제</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/feScore'">형성penilaian</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/soosiGrade'">nilai yang diinginkan</button>
		<button class="tab01 tablinks" onClick="location.href='${HOME}/st/lesson/currGrade'">종합성적</button>		
	</div>
	<!-- e_tab_wrap_cc -->

	<!-- s_tabcontent -->
	<div id="currDiv" class="tabcontent tabcontent1">
		<div class="st_tt">
			<div class="w_l">
				<span class="tt1" name="curr_name"></span>
			</div>
			<div class="w_r">
				<!-- <button class="btn_pdf" title="PDF다운로드"></button> -->
				<button class="btn_prt" onClick="currPrint();" title="인쇄하기"></button>
			</div>
		</div>

		<!-- s_scroll_wrap -->
		<div class="scroll_wrap">
			<div class="t_title">dasar dasar kurikulum</div>
			<table class="tab_table ttb1">
				<tbody>
					<tr class="tr01">
                           <td class="th01 w1">교육과정<br>코드</td>
                           <td class="w2" name="curr_code"></td>
                           <td class="th01 w1">교육<br>과정명</td>
                           <td colspan="3" class="w3" name="curr_name"></td>
                       </tr>
                       <tr class="tr01">
                           <td class="th01 w1">개설<br>semester</td>
                           <td class="w2" name="aca_name"></td>
                           <td class="th01 w1">학사<br>체계</td>
                           <td class="w2" colspan="3" name="aca_system_name"></td>
                       </tr>
                       <tr class="tr01">
                           <td class="th01 w1">이수<br>구분</td>
                           <td class="w2" name="complete_name"></td>
                           <td class="th01 w1">manajemen<br>구분</td>
                           <td class="w2" name="administer_name"></td>
                           <td class="th01 w1">nilai</td>
                           <td class="w2" name="grade"></td>
                       </tr>
                       <tr class="tr01">
                           <td class="th01 bd01 w1">Target</td>
                           <td class="bd01 w2" name="target_name"></td>
                           <td class="th01 bd01 w1">pelatihan</td>
                           <td class="bd01 w2"></td>
                           <td class="th01 bd01 w1"></td>
                           <td class="bd01 w2">-</td>
                       </tr>
				</tbody>
			</table>

			<div class="t_title">Informasi Instruktur</div>
			<table class="tab_table ttb1">
				<thead>
                    <tr class="tr01">
                        <td class="th01 w4">posisi </td>
                        <td class="th01 w4">nama </td>
                        <td class="th01 w4">departement</td>
                        <td class="th01 w4">jurusan khusus</td>
                        <td class="th01 w4">kontak</td>
                        <td class="th01 w5">E-mail</td>
                    </tr>                    
                </thead>
                <tbody id="addPfList">
                        
                </tbody>
			</table>

			<div class="t_title">Durasi belajar dan manajemen waktu</div>
			<table class="tab_table ttb1 mg_1">
				<tbody>
					<tr class="tr01">
						<td class="th01 w6">periode</td>
						<td colspan="3" class="w7" name="lesson_date"></td>
						<td class="th01 w6">total peiode (minggu)</td>
						<td class="w6" name="lesson_week"></td>
					</tr>
					<tr class="tr01">
						<td class="th01 w6">Jam rata-rata per minggu</td>
						<td class="w6" name="period_avg"></td>
						<td class="th01 w6">waktu</td>
						<td class="w6" name="lp_cnt"></td>
						<td class="th01 w6">durasi pembelajaran</td>
						<td class="w6" name="etc_period">-</td>
					</tr>
					<tr class="tr01">
						<td class="th01 w6">lecture</td>
						<td class="w6" name="lecture_cnt"></td>
						<td class="th01 w6">tidak ada perkuliahan</td>
						<td class="w6" name="unlecture_cnt"></td>
						<td class="th01 w6">Total jumlah jam</td>
						<td class="w6" name="total_period_cnt"></td>
					</tr>
				</tbody>
			</table>
			<table class="tab_table ttb1 b_2">
				<tbody id="lessonMethodList">
                        
                    </tbody>
			</table>

			<div class="t_title">Apakah evaluasi formasi dilakukan dan berapakali</div>
			<table class="tab_table ttb1">
				<tbody id="feAdd">
				</tbody>
			</table>

			<div class="t_title">총괄 penilaian 기준</div>
			<table class="tab_table ttb1">
				<thead>
                        <tr class="tr01">
                            <td class="th01 w6">Metode evaluasi</td>
                            <td class="th01 w8">penilaianArea</td>
                            <td class="th01 w6_1">penting</td>
                            <td class="th01 w8_1">Metode dan Kriteria Evaluasi</td>
                            <td class="th01 w8_1">Keterangan</td>
                        </tr>
                    </thead>
                    <tbody id="AddEvalList">                    
                        
                    </tbody>
                    <tr class="tr01">
                        <td colspan="2" class="th01 bd01 w6 ta_c">Total</td>
                        <td class="bd01 w6 ta_c">100 %</td>
                        <td class="bd01 w6 ta_c">-</td>
                        <td class="bd01 w6 ta_c">-</td>
                    </tr>
			</table>
<!-- 
			<div class="t_title">
				nilai 기준 : <span class="tt_s2" name="grade_method_name"></span>
			</div>
			<table class="tab_table ttb1">
				<tbody id="gradeStandardList">
                	
                </tbody>
			</table>
 -->
			<div class="t_title curr_summary">교육과정 개요 및 목적</div>
			<table class="tab_table ttb1 curr_summary">
				<tbody>
					<tr class="tr01">
						<td class="pd_1 bd01" name="curr_summary"></td>
					</tr>
				</tbody>
			</table>

			<div class="t_title reference_data">Buku teks, bahan tambahan, bahan referensi</div>
			<table class="tab_table ttb1 reference_data">
				<tbody>
					<tr class="tr01">
						<td class="pd_1 bd01" name="reference_data"></td>
					</tr>
				</tbody>
			</table>

			<div class="t_title management_plan">Rencana dan peraturan operasi mandiri (referensi khusus untuk kursus)</div>
			<table class="tab_table ttb1 management_plan">
				<tbody>
					<tr class="tr01">
						<td class="pd_1 bd01" name="management_plan"></td>
					</tr>
				</tbody>
			</table>
			
			<div class="t_title curr_outcome">proses kinerja</div>
			<table class="tab_table ttb1 curr_outcome">
				<tbody>
					<tr class="tr01">
						<td class="pd_1 bd01" name="curr_outcome"></td>
					</tr>
				</tbody>
			</table>
			

			<div class="t_title finishCapaView">Kompetensi Kelulusan dan proses kinerja</div>
			<table class="tab_table ttb1 finishCapaView">
				<thead>
                    <tr class="tr01">
                        <td class="th01 w5">syarat kelulusan</td>
                        <td class="th01 w10">proses kinerja</td>
                        <td class="th01 w4">cara pengajaran</td>
                        <td class="th01 w4">Metode evaluasi</td>
                    </tr>                       
                </thead>
                <tbody id="mpFinishCapabilityList">
                    
                </tbody>
			</table>

		</div>
		<!-- e_scroll_wrap -->
	</div>
	<!-- e_tabcontent -->
</body>
</html>