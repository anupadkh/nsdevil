<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="Description" content="의과대학 LMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="width=1100">
		
		<title>MLMS</title>
		
		<link rel="icon" type="img/png" href="${IMG}/favicon.png">
		<link rel="shortcut icon" href="${IMG}/favicon.png">
		<link rel="icon" href="${IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${IMG}/favicon.ico"> 
		<link rel="stylesheet" href="${CSS}/mpf_style.css" type="text/css" >
		<link rel="stylesheet" href="${CSS}/datetime.css" type="text/css" >
		<link rel="stylesheet" href="${CSS}/dev_pf_style.css" type="text/css" >
		
		<script src="${JS}/lib/jquery-1.11.1.js"></script>
		<script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.form.min.js"></script>
		<script src="${JS}/lib/flexslider.js"></script>
		<script src="${JS}/lib/slied_js.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/common.js"></script>
		
		<script type="text/javascript">
			var isExistPRBanner = "N"; //기관홍보 배너 여부
			var loopMax = 4;
			$(document).ready(function(){
				topPopupInit();
				initPageTopButton();
				customSelectBoxInit();
				$(document).mouseup(function(e){
					layerPopupCloseInit(['div[name="myProfilePopupDiv"]']);
				});
				
				prBannerSetting();//기관홍보배너삽입
				getNoticeList();
				getHotnewsList();
			});
			
			function prBannerSetting() {
				//TODO 홍보배너 유무 가져오기
				//isExistPRBanner = "Y";
				//loopMax = 3; //배너가 있으면 3
				if (isExistPRBanner == "N") {
					$("#prBannerDiv").hide();
				}
						
			}
			
			function getNoticeList() {
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/common/SLife/notice/list",
			        data: {},
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var list = data.list;
				        	var listHtml = '';
				        	var cnt = 0;
				        	if (isExistPRBanner == "N") {
				        		loopMax = 4;
				        	}
				        	$(list).each(function(){
				        		var boardSeq = this.board_seq;
				        		var title = this.title;
								if (cnt < loopMax) {
									$("#noticeListArea").append('<li class="li_1"><a class="tts" href="javascript:void(0);" onclick="javascript:noticeDetail('+boardSeq+');" ><span class="sign">■</span><span class="tt">'+title+'</span></a></li>')
								}
								cnt++;
				        	});
				        	for (var i=cnt; i<loopMax; i++) {
				        		if (i==0) {
				        			$("#noticeListArea").append('<li class="li_1"><a class="tts" href="javascript:void(0);"><span class="sign">■</span><span class="tt">registrasi된 게시글이 없습니다.</span></a></li>');
				        		} else {
				        			$("#noticeListArea").append('<li class="li_1"><a class="tts" href="javascript:void(0);"><span class="sign"></span><span class="tt"></span></a></li>');
				        		}
				        	}
			        	} else {
			        		//TODO
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function getHotnewsList() {
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/common/SLife/hotnews/list",
			        data: {},
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var list = data.list;
				        	var listHtml = '';
				        	var cnt = 0;
				        	$(list).each(function(){
				        		var boardSeq = this.board_seq;
				        		var title = this.title;
								if (cnt < loopMax) {
									$("#hotnewsListArea").append('<li class="li_1"><a class="tts" href="javascript:void(0);" onclick="javascript:hotnewsDetail('+boardSeq+');" ><span class="sign">■</span><span class="tt">'+title+'</span></a></li>')
								}
								cnt++;
				        	});
				        	for (var i=cnt; i<loopMax; i++) {
				        		if (i==0) {
				        			$("#hotnewsListArea").append('<li class="li_1"><a class="tts" href="javascript:void(0);"><span class="sign">■</span><span class="tt">registrasi된 게시글이 없습니다.</span></a></li>');
				        		} else {
				        			$("#hotnewsListArea").append('<li class="li_1"><a class="tts" href="javascript:void(0);"><span class="sign"></span><span class="tt"></span></a></li>');
				        		}
				        	}
			        	} else {
			        		//TODO
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function noticeDetail(boardSeq) {
				if (typeof boardSeq !=  "undefined" && boardSeq != "") {
					location.href = '${HOME}/common/SLife/notice/detail?seq='+boardSeq
				}
			}
			
			function hotnewsDetail(boardSeq) {
				if (typeof boardSeq !=  "undefined" && boardSeq != "") {
					location.href = '${HOME}/common/SLife/hotnews/detail?seq='+boardSeq
				}
			}
			
			if ('${overlapLogin}' == 'Y') {
				alert("다른 사용자가 로그인을 하였습니다.");
				location.href = '${HOME}/login';
			}
			
			
			function loginSubmit() {
			    
				var id = $("#id").val();
				if (isEmpty(id) || isBlank(id)) {
					alert("아이디를 입력해주세요");
					$("#id").focus();
					return;
				}
				
				var pwd = $("#pwd").val();
				if (isEmpty(pwd) || isBlank(pwd)) {
					alert("비밀번호를 입력해주세요");
					$("#pwd").focus();
					return;
				}
			    
			    $.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/login",
			        data: {
			            "id" : id,
			            "pwd" : pwd
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	location.href = "${HOME}";
			            } else if (data.status == "102") {
			            	alert("사용할 수 없는 계정입니다.");
			        	}else {
			                alert("잘못된 ID 또는 비밀번호 입니다.");
			            }
			        },
			        error: function(xhr, textStatus) {
			            //alert("오류가 발생했습니다.");
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
		</script>
	</head>

	<body class="new1">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
		    <p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a>
			<a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
		<!-- s_wrap -->
		<div id="wrap">
			<!-- s_top_pop -->
			<div id="top_pop">
				<div class="wrap">
					<div class="slide-wrap">
						<ul>
						    <li class="tt"><img src="${IMG}/pop001.png" alt="의과대학 의학교육 penilaian인증 인증 획득 4년"></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- e_top_pop -->
			
			<!-- s_header -->
			<header class="header">
				<div class="header_top">
					<div class="top_wrap">       
						<div class="top_m">
						<!-- //TODO 임시로 제외시킴 -->
						<!-- <a href="javascript:void(0);" id="topPopupBtn" class="a_m a1" onclick="javascript:topPopupToggle();">팝업닫기</a> -->
							<c:choose>
								<c:when test='${HOME ne ""}'>
									<a href="${HOME}" class="a_m a3">HOME</a>
								</c:when>
								<c:otherwise>
									<a href="/" class="a_m a3">HOME</a>
								</c:otherwise>
							</c:choose>
							<!-- //TODO 임시로 제외시킴 -->
							<!-- <a href="#" class="a_m a4">사이트맵</a> -->
							<c:if test="${sessionScope.S_USER_ID ne null}">
								<a href="${HOME}/logout" class="a_m a5">로그아웃</a>
							</c:if>
							<a href="${HOME_URL }" class="a_m a6">기관Home</a>
						</div>
						<c:if test="${sessionScope.S_USER_ID ne null}">
							<div class="a_mp a7" onclick="myProfilePopup()">
								<c:choose>
									<c:when test='${sessionScope.S_USER_PICTURE_PATH ne null and sessionScope.S_USER_PICTURE_PATH ne ""}'>
										<span class="pt01"><img src="${sessionScope.S_USER_PICTURE_PATH}" alt="나의 대표 이미지" class="pt_img"></span>
									</c:when>
									<c:otherwise>
										<span class="pt01"><img src="${DEFAULT_PICTURE_IMG}" alt="나의 대표 이미지" class="pt_img"></span>
									</c:otherwise>
								</c:choose>
								
								<span class="ssp1">${sessionScope.S_USER_NAME} (${sessionScope.S_USER_DEPARTMENT_NAME})</span>
							</div>
							<!-- s_pop_pop9 -->
							<div id="pop_pop9" name="myProfilePopupDiv">
								<div class="spop9 show" id="spop9">
									<span class="tt">${sessionScope.S_USER_NAME} (${sessionScope.S_USER_DEPARTMENT_NAME})</span>
									<span class="tt">${sessionScope.S_USER_EMAIL}</span>
									<button onclick="location.href='${HOME}/common/my'" class="btn btn05">내 정보 perbaiki</button>
									<span onclick="$('#pop_pop9').hide();" class="pop_close_s" title="닫기">X</span>
								</div>
							</div>
							<!-- e_pop_pop9 --> 
						</c:if>
					</div>
				</div>
				<!-- s_gnbwrap -->			
				<div class="gnbwrap">
					<!-- s_gnb_swrap -->  
					<div class="gnb_swrap">
						<h1 class="logo">
						<c:choose>
							<c:when test='${HOME ne ""}'>
								<a href="${HOME}" title="홈으로 가기"><img src="${TOP_LOGO}" alt="의과대학 로고"></a>
							</c:when>
							<c:otherwise>
								<a href="/" title="홈으로 가기"><img src="${TOP_LOGO}" alt="의과대학 로고">	</a>
							</c:otherwise>
						</c:choose>
						</h1>
					</div>
					<!-- e_gnb_swrap --> 
					
					<!-- s_gnb_fwrap -->  
					<div class="gnb_fwrap">
						<div class="gnb_swrap2">
							<ul id="gnb" class="gnb">
								<c:forEach items="${sessionScope.S_ALLOW_MENU_LIST}" var="allowMenuList" step="1">
									<li><a href="${HOME}${allowMenuList.url}" onclick="javascript:pageLinkCheck('${HOME}','${HOME}${allowMenuList.url}'); return false;" <c:if test='${allowMenuList.now_menu_yn eq "Y" && allowMenuList.url ne ""}'>class="on"</c:if>>${allowMenuList.name}</a></li>
								</c:forEach>
							</ul>
						</div>
					</div>
					<!-- e_gnb_fwrap --> 
				</div>
				<!-- e_gnbwrap -->      
			</header>   
			
			<!-- s_container_table  기관 홍보배너가 없을 때 class nonbn 삽입 -->
			<div id="container" class="container_table">
				<!-- s_contents -->
				<div class="contents index">
					<!-- s_slidewrap -->   
					<div class="slidewrap">
						<div id="slider001" class="flexslider" style="display: block;">
							<!-- s_ul slides -->
							<ul class="slides sd1">
								<li class="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
									<a href="#" title="" target="_blank">
										<div class="sd_swrap bg1">
											<h3>2017-1semester 졸업예정자<br>취업심화교육 이수자<br>수강료지원 접수</h3>
											<p class="tt">Target</p>
											<p class="con">2017tahun ajaran도 1semester 졸업연기자 중<br>교육기관(학원포함)에서<br>취업관련 교육을 받은 학생</p>
											<p class="tt">periode</p>
											<p class="con">2017.05.08.(월)~ 2017.05.12.(금)</p>
											<p class="tt">문의</p>
											<p class="con">해당학과 사무실</p>
										</div>
										<div class="sd_bwrap"></div>
										<div class="sd_img1"><img src="${SLIDE_IMG_1}" alt="메인 비주얼 이미지" draggable="false" ></div>
									</a>
								</li>
								<li class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;">
									<a href="#" title="" target="_blank">
										<div class="sd_swrap bg2">
											<h3>메인 비주얼 알림<br>대제목 두울<br>대제목 두울</h3>
											<p class="tt">소제목1</p>
											<p class="con">메인 비주얼 알림 isi들</p>
											<p class="tt">소제목2</p>
											<p class="con">메인 비주얼 알림 isi들</p>
											<p class="tt">소제목3</p>
											<p class="con">메인 비주얼 알림 isi들</p>
										</div>
										<div class="sd_bwrap"></div>
										<div class="sd_img1"><img src="${SLIDE_IMG_2}" alt="메인 비주얼 이미지" draggable="false" ></div>
									</a>
								</li>
								<li class="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
									<a href="#" title="" target="_blank">
										<div class="sd_swrap bg3">
											<h3>메인 비주얼 알림<br>대제목 셋<br>대제목 셋</h3>
											<p class="tt">소제목1</p>
											<p class="con">메인 비주얼 알림 isi들</p>
											<p class="tt">소제목2</p>
											<p class="con">메인 비주얼 알림 isi들</p>
											<p class="tt">소제목3</p>
											<p class="con">메인 비주얼 알림 isi들</p>
										</div>
										<div class="sd_bwrap"></div>
										<div class="sd_img1"><img src="${SLIDE_IMG_3}" alt="메인 비주얼 이미지" draggable="false" ></div>
									</a>
								</li>
							</ul>
							<!-- e_ul slides -->
						</div>
					</div>
					<!-- e_slidewrap --> 
			
					<!-- s_div1 -->
					<div class="div1">
						<div class="ind_tt_wrap">
							<h3 class="tt1">
								<span class="tts1">${UNIVERSITY_NAME}</span>
								<span class="tts2">U-Learning Management System</span>
							</h3>
						</div>
						<div class="login_wrap">
							<div class="sbox1">
								<input type="text" class="tts1" placeholder="아이디" id="id" onkeypress="javascript:if(event.keyCode==13){loginSubmit(); return false;}">
								<input type="password" class="tts1" placeholder="비밀번호" id="pwd"onkeypress="javascript:if(event.keyCode==13){loginSubmit(); return false;}">
							</div>
							<div class="sbox2">
								<button onclick="loginSubmit();" class="login">로그인</button>
							</div>
							<div class="sbox3">
								<div class="askidpw"><span class="sign">!</span><span>manajemen자에게 비밀번호 확인요청하기</span></div>
							</div>
						</div>
					</div>
					<!-- e_div1 -->
			
					<!-- s_div2 -->
					<div class="div2">
						<div class="ind_tt_wrap">
							<h3 class="tt1">공지사항</h3>
							<c:choose>
								<c:when test="${sessionScope.S_USER_LEVEL < 2}">
									<a href="${HOME}/admin/board/notice/list" class="more">더보기 <span class="sign">▶</span></a>
								</c:when>
								<c:otherwise>
									<a href="${HOME}/common/SLife/notice/list" class="more">더보기 <span class="sign">▶</span></a>
								</c:otherwise>
							</c:choose>
						</div>
						<ul class="tts_wrap" id="noticeListArea"></ul>
					</div>
					<!-- e_div2 -->
			
					<!-- s_div3 -->
					<div class="div3">
						<div class="ind_tt_wrap">
							<h3 class="tt1">Hot News</h3>
							<c:choose>
								<c:when test="${sessionScope.S_USER_LEVEL < 2}">
									<a href="${HOME}/admin/board/hotnews/list" class="more">더보기 <span class="sign">▶</span></a>
								</c:when>
								<c:otherwise>
									<a href="${HOME}/common/SLife/hotnews/list" class="more">더보기 <span class="sign">▶</span></a>
								</c:otherwise>
							</c:choose>
						</div>
						<ul class="tts_wrap" id="hotnewsListArea"></ul>
					</div>
					<!-- e_div3 -->
			
					<!-- s_div4 -->
					<div class="div4">
						<div class="ind_tt_wrap">
							<h3 class="tt1">모바일 QR코드</h3>
						</div>
			
						<div class="qr">
							<span class="tts2"> QR코드를 찍으면<br>모바일페이지로 이동합니다.</span>
							<img src="${QR_CODE}" alt="mlms 모바일 페이지 QR코드" onClick="location.href='${MOBILE_URL}'">
						</div>
			
					</div>
					<!-- e_div4 -->  
			
					<!-- s_div5 -->
					<div class="div5" id="prBannerDiv">
						<div class="bn">기관 홍보배너가 삽입되는 자리입니다.</div>
					</div>
					<!-- e_div5 -->  
			
					<!-- s_div6 -->
					<div class="div6">
						<div class="ind_tt_wrap"><h3 class="tt1">시스템 사용방법</h3></div>
						<a class="tt_s1 s1" href="#">학생 가이드<span class="sign">GO</span></a>
						<a class="tt_s1 s2" href="#">교수 가이드<span class="sign">GO</span></a>
					</div>
					<!-- e_div6 -->
			
				</div>
				<!-- e_contents -->
			</div>
			<!-- e_container_table -->
			
			<a href="#" id="backtotop" title="To top" class="totop"></a>
			
			<!-- s_footer -->
			<footer id="footer" class="ind">
				<div class="footer_con">           
					<div class="footer_scon">
						<ul class="f_menu">
							<li><a href="#" onclick="javascript:alert('개인정보처리방침'); return false;" title="새창" class="fmn1">개인정보처리방침</a></li>
							<li><a href="#" onclick="javascript:alert('이메일무단수집거부'); return false;" title="새창" class="fmn2"><span>|</span>이메일무단수집거부</a></li>                
						</ul>
						<p class="addr">${ADDRESS }</p>
						<a href="${HOME}" title="홈으로 가기" class="logo_wrap"><img src="${BOTTOM_LOGO}" alt="의과대학 로고" class="b_logo"></a>
					</div>
				</div>
			</footer>
			<!-- e_footer -->
		</div>
		<!-- e_wrap -->
	</body>
</html>