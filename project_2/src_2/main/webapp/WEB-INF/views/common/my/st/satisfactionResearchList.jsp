<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
	$(document).ready(function() {
		getSFResearchList("N", "N");
		
		if("${type}" == "1"){
			$("span[data-name=title]").text('수업만족도 조사');
			$("a[data-name=lp]").addClass("on");
		}
		if("${type}" == "2"){
			$("span[data-name=title]").text('과정만족도 조사');
			$("a[data-name=curr]").addClass("on");
		}

		$(".tab").click(function() {
			$(".tab").removeClass("on");
			$(this).addClass("on");
		});
		$( ".boardwrap" ).accordion({
			collapsible: true,
			active: false
		});
	});

	function getSFResearchList(yn_flag, minusPoint_flag) {

		var type = "";
		
		if("${type}" == "1")
			type = "1";
		if("${type}" == "2")
			type = "2";
		
		$.ajax({
			type : "POST",
			url : "${M_HOME}/ajax/st/sfResearch/list",
			data : {
				"sr_type" : type,
				"researchYN" : yn_flag,
				"minusPoint_flag" : minusPoint_flag
			},
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {
					$("#sfResearchListAdd").empty();
					var htmls = "";

					$.each(data.sfResearchList, function(index) {
						//수업종료 부터 오늘까지 일수 / 7
						var week_chk = 0;
						if(this.enddate_to_now > 0 && this.enddate_to_now <= 7)
							week_chk = 1;
						else
							week_chk = parseInt(this.enddate_to_now / 7);
												
						var research_submit_week = 0;
						//제출기한 부터 제출일까지 일수
						if(this.research_to_now > 0)
							research_submit_week = parseInt(this.research_to_now / 7);
						
						htmls = '<tr class="box_wrap">';
						htmls += '<td class="box_s w1">';
						//제출 완료 인지 체크
						if(this.research_chk == "N"){
							//수업종료 후 1주 이내 일 때
							if(week_chk <= 1){
								htmls += '<div class="wrap_s s1">';
								htmls += '<span class="num">'+this.end_date_monday+'</span>';
								htmls += '<span class="sp_s1">까지</span>';
								htmls += '</div>';
							}else{
								htmls += '<div class="wrap_s s2">';
								htmls += '<span class="sp_s1">감점</span>';
								htmls += '<span class="num">'+(week_chk*0.5)+'점</span>';
								htmls += '</div>';
							}
						}else{
							htmls += '<div class="wrap_s s3">';
							htmls += '<span class="sp_s1">제출<br>완료</span>';
							htmls += '</div>';
						}
						htmls += '</td>';
						htmls += '<td class="box_s w2">';
						if(this.enddate_to_now <= 0)
							htmls += '<div class="wrap_s ssw">';
						else
							htmls += '<div class="wrap_s ssw" onclick="getSFResearchView('+this.curr_seq+','+this.lp_seq+',\''+type+'\')">';	
						htmls += '<div class="wrap_ss">';
						htmls += '<span class="tm tm1">응답periode</span>';
						htmls += '<span class="tm num_s1">'+this.start_date+'</span>';
						htmls += '<span class="tm sign">~</span>';
						htmls += '<span class="tm num_s1">'+this.research_end_date+'</span>';
						if(week_chk > 0 && this.research_chk == "N"){
							htmls += '<span class="tm sign">(</span>';
							htmls += '<span class="tm num_2">'+week_chk+'</span>';
							htmls += '<span class="tm tm1">주 지남</span>';
							htmls += '<span class="tm sign">)</span>';
						}else if(research_submit_week > 0 && this.research_chk == "Y"){
							htmls += '<span class="tm sign">(</span>';
							htmls += '<span class="tm num_2">'+research_submit_week+'</span>';
							htmls += '<span class="tm tm1">주 지남</span>';
							htmls += '<span class="tm sign">)</span>';
						}
					
						htmls += '</div>';
						htmls += '<div class="wrap_ss">';
						
						if(!isEmpty(this.lesson_subject))
							htmls += '<span class="sp_1">'+this.curr_name+' - '+this.lesson_subject+'</span>';
						else
							htmls += '<span class="sp_1">'+this.curr_name+'</span>';
							
						if(research_submit_week > 0){
							htmls += '<span class="sp_3">'+(research_submit_week*0.5)+'점 감점 됨</span>';
						}else{
							if("${type}" == "1")
								htmls += '<span class="sp_2">( 주 - 0.5 감점 )  수업만족도 조사를 제출하세요.</span>';
							else if("${type}" == "2")
								htmls += '<span class="sp_2">( 주 - 0.5 감점 )  과정만족도 조사를 제출하세요.</span>';
						}
						htmls += '</div>';
						htmls += '</div>';
						htmls += '</td>';
						htmls += '</tr>';
						$("#sfResearchListAdd").append(htmls);
					});
				}
			},
			error : function(xhr, textStatus) {
				//alert("오류가 발생했습니다.");
				document.write(xhr.responseText);
			}
		});
	}

	function getSFResearchView(curr_seq, lp_seq, sr_type) {
		post_to_url("${HOME}/common/my/sfResearch/create", { "curr_seq" : curr_seq, "s_lp_seq" : lp_seq , "sr_type" : sr_type});
	}
</script>
</head>
<body>
	<div id="container" class="container_table">
			<div class="contents sub">
				<div class="left_mcon my">   
					<div class="sub_menu st_my">
						<div class="title">My</div>

						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/common/my/profile/detail'">프로필 perbaiki</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>							
							<c:choose>
								<c:when test='${sessionScope.S_USER_LEVEL eq 1 or sessionScope.S_USER_LEVEL eq 2}'>
								</c:when>
								<c:when test='${sessionScope.S_USER_LEVEL eq 3}'>
									<div class="title1 wrapx">
										<span class="sp_tt" onClick="location.href='${HOME}/common/my/lessonData'">나의lecture자료</span>
									</div>
									<ul class="panel">
										<li class="wrap xx"></li>
									</ul>	
								</c:when>
								<c:when test='${sessionScope.S_USER_LEVEL eq 4}'>
									
									<div class="title1 wrapx"><span class="sp_tt on">만족도 조사</span></div>
								    <ul class="panel on">
								        <li class="wrap"><a data-name="lp" href="javascript:post_to_url('${HOME }/common/my/sfResearch/list', {'type':'1'});" class="tt_2">수업만족도 조사</a></li>
								        <li class="wrap"><a data-name="curr" href="javascript:post_to_url('${HOME }/common/my/sfResearch/list', {'type':'2'});" class="tt_2">과정만족도 조사</a></li>
								    </ul>
								
									<div class="title1 wrapx"><span class="sp_tt" onClick="location.href='${HOME}/common/my/asgmt/list'">과제 조회</span></div>
								    <ul class="panel">
								        <li class="wrap xx"></li>
								    </ul>
								    
									<div class="title1">
										<span class="sp_tt">성적조회</span>
									</div>
									<ul class="panel">
										<li class="wrap"><a href="${HOME }/common/my/feScore" class="tt_2">형성penilaian</a></li>
										<li class="wrap"><a href="${HOME }/common/my/soosiGrade" class="tt_2">nilai yang diinginkan</a></li>
										<li class="wrap"><a href="${HOME }/common/my/currGrade" class="tt_2">종합성적</a></li>
									</ul>
								</c:when>
							</c:choose>
							
						</div>
					</div>
				</div>

			<!-- Tab : cc_hwork_list.html, 각각을 Page로 나눈 것 cc_hwork_list1.html, cc_hwork_list2.html : 필요에 따라 pilih 사용 -->
		<!-- s_hworktb_wrap -->
		<!-- s_sub_con -->
		<div class="sub_con my">
		 
		<!-- s_tt_wrap -->                
		<div class="tt_wrap">
			<h3 class="am_tt"><span class="tt" data-name="title"></span></h3>                 
		</div>
		<!-- e_tt_wrap -->
			
		<!-- s_rfr_con -->
		<div class="rfr_con survey"> 

			<div class="svy_ttwrap">
				<span class="svy1 tab on" onClick="getSFResearchList('N','N');">대기</span>
				<span class="svy2 tab" onClick="getSFResearchList('','Y');">감점</span>
				<span class="svy3 tab" onClick="getSFResearchList('','');">Keseluruhan</span>
			</div>

			<div class="mtb_wrap">
				<!-- s_tb_svy2 -->
				<table class="tb_svy tb_svy2" id="sfResearchListAdd">

				</table>
				<!-- e_tb_svy2 -->

			</div>
			<!-- e_mtb_wrap -->

		</div>
		<!-- e_mtb_wrap -->
	</div>
	<!-- e_contents -->
	</div>
	</div>
</body>
</html>
