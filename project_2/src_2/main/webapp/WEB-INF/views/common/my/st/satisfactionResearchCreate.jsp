<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
	$(document).ready(function() {
		$("body").addClass("bd_svy");
		$("body").addClass("bd_svyf");

		if("${sr_type}" == "1"){
			$("span[data-name=title]").text('수업만족도 조사');
			$("li[data-name='lp'] a").addClass("on");
		}
		if("${sr_type}" == "2"){
			$("span[data-name=title]").text('과정만족도 조사');
			$("li[data-name='curr'] a").addClass("on");
		}
		
		getResearchList();
		
		$(document).on("change","#researchListAdd input:radio", function(){
			$(this).closest("div.svyf_box_s1").find("input[name=sri_seq]").val($(this).val());
		});
		
		$(document).on("keyup","#researchListAdd textarea", function(){
			$(this).closest("div.svyf_box_s1").find("input[name=sri_answer]").val($(this).attr("name")+"||"+$(this).val());
			
		});
		$( ".boardwrap" ).accordion({
			collapsible: true,
			active: false
		});
	});
	
	function getResearchList(){
		$.ajax({
			type : "POST",
			url : "${M_HOME}/ajax/st/sfResearch/questionList",
			data : {
				"lp_seq" : $("#lp_seq").val(),
				"curr_seq" : $("#curr_seq").val(),
				"sr_type" : $("#sr_type").val()
			},
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {
					$("#researchListAdd").empty();
					var htmls = "";
					var pre_srh_seq = "";
					var num = 0;
					var disable_flag = "";
					
					if(data.SFResearchSubmitChk != 0){
						disable_flag = 'disabled="disabled"';
						$("#footer").remove();
					}
					
					$.each(data.sfResearchQuestionList, function(index) {
						
						if(index != 0 && pre_srh_seq != this.srh_seq){
							htmls+= '</table>';  
					        htmls+= '</div>';
					        htmls+= '</div>';
					        $("#researchListAdd").append(htmls);
						}
	            			
						if(pre_srh_seq != this.srh_seq){
					        num++;
							htmls = '<div class="svyf_box_s1">';
																					
							//객관식일때 sri_seq 에 넣고 주관식일 때 sri_answer에 넣는다.
							//1=객관식, 2=주관식, 3=표형
							if(this.srh_type=="1")
								htmls+= '<input type="hidden" name="sri_seq" value=""/>';
							else if(this.srh_type=="2")
								htmls+= '<input type="hidden" name="sri_answer" value=""/>';
								
							htmls+= '<input type="hidden" name="essential_flag" value="'+this.essential_flag+'"/>';
							htmls+= '<input type="hidden" name="srh_type" value="'+this.srh_type+'"/>';
					        htmls+= '<table class="svyf tb2">';		 
					        htmls+= '<tr>';
					        htmls+= '<td class="t1"><span>'+this.srh_num+'</span></td>';
					        htmls+= '<td class="t2">';
					        htmls+= '<div>'+this.srh_subject+'</div>';
					        htmls+= '</td>';	
					        htmls+= '</tr>';
					        htmls+= '</table>';		
					        htmls+= '<div class="svyf2_wrap">';                             
					        htmls+= '<table class="svyf2 selectable ui-selectable">';	
					        if(this.srh_type=="1"){
						        htmls+= '<tr class="n1">';
						        htmls+= '<td class="w3" colspan="2">';
						        if(!isEmpty(this.srh_explan))						        	
						        	htmls+= '<div>'+this.srh_explan+'</div>';		
						        htmls+= '</td>';
						        htmls+= '</tr>';
					        }
						}
						
						//설문 유형 1:객관식 2:주관식 3:표형
						if(this.srh_type=="1"){
							
					        htmls+= '<tr class="n1">';
					        htmls+= '<td class="w1">';
					        htmls+= '<label class="ckwrap">';
					        if(this.answer_seq == this.sri_seq)
					        	htmls+= '<input type="radio" '+disable_flag+' checked name="rd'+num+'" value="'+this.srh_seq+'||'+this.sri_seq+'">';
				        	else
				        		htmls+= '<input type="radio" '+disable_flag+' name="rd'+num+'" value="'+this.srh_seq+'||'+this.sri_seq+'">';
					        htmls+= '<span class="ckmark"></span>';
					        htmls+= '</label>';		
					        htmls+= '</td>';
					        htmls+= '<td class="w2"><div>'+this.sri_item_explan+'</div></td>';
					        htmls+= '</tr>';
						}else if(this.srh_type=="2"){
							htmls+= '<tr class="n1">';
					        htmls+= '<td class="w3">';		
					        htmls+= '<div class="ip_wrap free_textarea"> <textarea '+disable_flag+' name="'+this.srh_seq+'" class="tarea01" style="height: 60px;">'+this.answer+'</textarea> </div>'
					        htmls+= '</td>';
					        htmls+= '</tr>';
						}
						
				        if(data.sfResearchQuestionList.length == (index+1)){
					        htmls+= '</table>';  
					        htmls+= '</div>';
					        htmls+= '</div>';
					        $("#researchListAdd").append(htmls);
				        }
				        pre_srh_seq = this.srh_seq;
					});
				}
			},
			error : function(xhr, textStatus) {
				//alert("오류가 발생했습니다.");
				document.write(xhr.responseText);
			}
		});
	}
	
	function sfResearchSubmit(){
		var chk = true;
		var radioArray = new Array();
		var pre_radio_name = "";
		var radioIndex = 0;
		
		//필수문제인거 체크
		$.each($("#researchListAdd div.svyf_box_s1"), function(){
			//필수 일 경우
			if($(this).find("input[name=essential_flag]").val() == "Y"){			
				if($(this).find("input[name=srh_type]").val() == "1"){
					if(!$(this).find("input[type=radio]").is(":checked")){
						$(this).attr("tabindex", -1).focus();		
						alert("pilih되지 않은 설문이 있습니다.");	
						chk = false;
						return false;
					}
				}else if($(this).find("input[name=srh_type]").val() == "2"){
					if(isEmpty($(this).find("textarea").val())){
						$(this).attr("tabindex", -1).focus();		
						alert("입력하지 않은 설문이 있습니다.");
						chk = false;
						return false;
					}
				}else{
					
				}			
			}
		});
		
		if(chk == false)
			return;
		
		/* 
		//라디오 동적으로 tambahkan라 라디온 name 명 가져옴.
		$.each($("#researchListAdd input:radio"),function(){
			if(pre_radio_name != $(this).attr("name")){
				radioArray[radioIndex] = $(this).attr("name");
				radioIndex++;				
			}				
			pre_radio_name = $(this).attr("name");				
		});
		
		//라디오 박스 체크 했는지 확인
		for(var i=0;i<radioArray.length;i++){			
			if(!$("#researchListAdd input:radio[name="+radioArray[i]+"]").is(":checked")){
				$("#researchListAdd input:radio[name="+radioArray[i]+"]").closest("div.svyf_box_s1").attr("tabindex", -1).focus();
				chk = false;		
				alert("pilih되지 않은 값이 있습니다.");		
				return;
			}
		}
		 */
		if(!confirm("제출하시겠습니까?\n제출 후 perbaiki할 수 없습니다."))
			return;
		
		$("#sfResearchForm").ajaxForm({
			type: "POST",
			url: "${M_HOME}/ajax/st/sfResearch/insert",
			dataType: "json",
			success: function(data, status){
				if (data.status == "200") {
					alert("제출 완료 되었습니다.");
					post_to_url('${HOME }/common/my/sfResearch/list', {'type':'${sr_type}'});
				} else {
					alert("제출 실패 하였습니다.");
					$.unblockUI();							
				}				
			},
			error: function(xhr, textStatus) {
				document.write(xhr.responseText); 
				$.unblockUI();						
			},beforeSend:function() {
				$.blockUI();						
			},complete:function() {
				$.unblockUI();						
			}    					
		});		
		$("#sfResearchForm").submit();
	}
</script>
</head>
<body class="color1">
	<div id="container" class="container_table">
			<div class="contents sub">
				<div class="left_mcon my">   
					<div class="sub_menu st_my">
						<div class="title">My</div>

						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/common/my/profile/detail'">프로필 perbaiki</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>							
							<c:choose>
								<c:when test='${sessionScope.S_USER_LEVEL eq 1 or sessionScope.S_USER_LEVEL eq 2}'>
								</c:when>
								<c:when test='${sessionScope.S_USER_LEVEL eq 3}'>
									<div class="title1 wrapx">
										<span class="sp_tt" onClick="location.href='${HOME}/common/my/lessonData'">나의lecture자료</span>
									</div>
									<ul class="panel">
										<li class="wrap xx"></li>
									</ul>	
								</c:when>
								<c:when test='${sessionScope.S_USER_LEVEL eq 4}'>
									<div class="title1 wrapx"><span class="sp_tt on">만족도 조사</span></div>
								    <ul class="panel on">
								        <li class="wrap" data-name="lp"><a href="javascript:post_to_url('${HOME }/common/my/sfResearch/list', {'type':'1'});" class="tt_2">수업만족도 조사</a></li>
								        <li class="wrap" data-name="curr"><a href="javascript:post_to_url('${HOME }/common/my/sfResearch/list', {'type':'2'});" class="tt_2">과정만족도 조사</a></li>
								    </ul>
								
									<div class="title1 wrapx"><span class="sp_tt" onClick="location.href='${HOME}/common/my/asgmt/list'">과제 조회</span></div>
								    <ul class="panel">
								        <li class="wrap xx"></li>
								    </ul>
								    
									<div class="title1">
										<span class="sp_tt">성적조회</span>
									</div>
									<ul class="panel">
										<li class="wrap"><a href="${HOME }/common/my/feScore" class="tt_2">형성penilaian</a></li>
										<li class="wrap"><a href="${HOME }/common/my/soosiGrade" class="tt_2">nilai yang diinginkan</a></li>
										<li class="wrap"><a href="${HOME }/common/my/currGrade" class="tt_2">종합성적</a></li>
									</ul>
								</c:when>
							</c:choose>
							
						</div>
					</div>
				</div>

			<!-- Tab : cc_hwork_list.html, 각각을 Page로 나눈 것 cc_hwork_list1.html, cc_hwork_list2.html : 필요에 따라 pilih 사용 -->
		<!-- s_hworktb_wrap -->
		<!-- s_sub_con -->
		<div class="sub_con my">
		 
		<!-- s_tt_wrap -->                
		<div class="tt_wrap">
			<h3 class="am_tt"><span class="tt" data-name="title"></span></h3>                 
		</div>
		<!-- e_tt_wrap -->
			
		<!-- s_rfr_con -->
		<div class="rfr_con survey"> 
			<!-- s_tt_wrap -->                
			<div class="tt_wrap cc">
				<c:if test='${sr_type == "1" }'>
			    <h3 class="am_tt"><span class="h_date">${ info.lesson_date}</span><span class="tt">[${info.period } pengajar] : ${info.start_time }~${info.end_time }</span>
			        <span class="tt_s">${info.lesson_subject }</span>
			    </h3>   
			    
			    <div class="r_wrap">        
			        <div class="wrap2">
			            <span class="a_mp">
				            <span class="pt01">
				            	<c:if test='${info.picture_path eq "" }'>
				            	<img src="${IMG}/ph_2.png" alt="registrasi된 사진 이미지" class="ph_img">
				            	</c:if>
				            	<c:if test='${info.picture_path ne "" }'>
				            	<img src="${RES_PATH}${info.picture_path}" alt="registrasi된 사진 이미지" class="ph_img">
				            	</c:if>
				            </span>
				            <span class="ssp1">${info.name }&nbsp; &#40;${info.code_name}&#41;</span>
			            </span>
			        </div>
			    </div>    
			    </c:if>
			    <c:if test='${sr_type == "2" }'>
			    <h3 class="am_tt">
					<span class="tt_cc">${info.curr_name }</span>
			        <span class="tt1">교육과정periode</span><span class="h_date_cc">${ info.curr_start_date} ~ ${info.curr_end_date_mmdd }</span>
			    </h3>   
			    
			    <div class="r_wrap">        
			        <div class="wrap2">
			            <span class="a_mp">
				            <span class="pt01"><img src="../img/ph_2.png" alt="registrasi된 사진 이미지" class="ph_img"></span>
				            <span class="ssp1">${mpfInfo.mpf }</span>
			            </span>
			        </div>
			    </div>                          
			    </c:if>      
			</div>
			<!-- e_tt_wrap -->
				
			<div class="mtb_wrap">
				
			<!-- s_survey -->
			<div class="survey">           
			
			<!-- s_cont -->  	
			<div class="cont">	
			<!-- s_svyf_wrap -->
				<div class="svyf_wrap">
				    <div class="svyf_wrap_s">
			            <div class="svyf_tt_box">본 설문지는 수업개선의 기초 및 교육의 질적인 향상을 위해 실시합니다.<br>						
			            <span class="c1">periode 내 과정만족도 조사를 완료하지 않을 경우 매주 -0.5 점씩 감점 처리 됩니다. <br>
						바로 설문을 완료하여 제출해주세요.</span>
			            </div>	
			        </div>
			
			 </div>

			<form id="sfResearchForm" onsubmit="return false;">
				<input type="hidden" id="lp_seq" name="lp_seq" value="${lp_seq }">
				<input type="hidden" id="curr_seq" name="curr_seq" value="${curr_seq }">
				<input type="hidden" id="sr_type" name="sr_type" value="${sr_type }">
				<!-- s_svyf_box1 -->
				<div class="svyf_box1" id="researchListAdd">
	
					
				</div>			
				<!-- e_svyf_box1 -->
			</form>
		</div>
		<!-- e_ptb_wrap -->
		<div class="wrap_btn_svy" id="footer">
			<button class="btn_svy" onClick="sfResearchSubmit();">제출하기</button>
		</div>
	</div>
	<!-- e_contents -->
	</div>
		
	</div>
	</div>
	</div>
	</div>
</body>
</html>
