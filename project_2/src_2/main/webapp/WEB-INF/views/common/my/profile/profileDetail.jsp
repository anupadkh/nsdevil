<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script>
			$(document).ready(function(){
				$( ".boardwrap" ).accordion({
					collapsible: true,
					active: false
				});
			});
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
			<div class="contents sub">
				<div class="left_mcon my">   
					<div class="sub_menu st_my">
						<div class="title">My</div>

						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt on" onClick="location.href='${HOME}/common/my/profile/detail'">프로필 perbaiki</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>							
							<c:choose>
								<c:when test='${sessionScope.S_USER_LEVEL eq 1 or sessionScope.S_USER_LEVEL eq 2}'>
								</c:when>
								<c:when test='${sessionScope.S_USER_LEVEL eq 3}'>
									<div class="title1 wrapx">
										<span class="sp_tt" onClick="location.href='${HOME}/common/my/lessonData'">나의lecture자료</span>
									</div>
									<ul class="panel">
										<li class="wrap xx"></li>
									</ul>	
								</c:when>
								<c:when test='${sessionScope.S_USER_LEVEL eq 4}'>
									<div class="title1"><span class="sp_tt">만족도 조사</span></div>
								    <ul class="panel">
								        <li class="wrap" data-name="lp"><a href="javascript:post_to_url('${HOME }/common/my/sfResearch/list', {'type':'1'});" class="tt_2">수업만족도 조사</a></li>
								        <li class="wrap" data-name="curr"><a href="javascript:post_to_url('${HOME }/common/my/sfResearch/list', {'type':'2'});" class="tt_2">과정만족도 조사</a></li>
								    </ul>
								
									<div class="title1 wrapx"><span class="sp_tt" onClick="location.href='${HOME}/common/my/asgmt/list'">과제 조회</span></div>
								    <ul class="panel">
								        <li class="wrap xx"></li>
								    </ul>
								    
									<div class="title1">
										<span class="sp_tt">성적조회</span>
									</div>
									<ul class="panel">
										<li class="wrap"><a href="${HOME }/common/my/feScore" class="tt_2">형성penilaian</a></li>
										<li class="wrap"><a href="${HOME }/common/my/soosiGrade" class="tt_2">nilai yang diinginkan</a></li>
										<li class="wrap"><a href="${HOME }/common/my/currGrade" class="tt_2">종합성적</a></li>
									</ul>
								</c:when>
							</c:choose>
							
						</div>
					</div>
				</div>
				<div class="sub_con my">
					<div class="tt_wrap">
						<h3 class="am_tt"><span class="tt">프로필 정보</span></h3>                       
					</div>
					<div class="rfr_con my"> 
						<table class="table_my">
						    <tbody>
						        <tr>
						            <td rowspan="3" class="td01 w01 bd04">
										<div class="ph_wrap">
						                	<c:choose>
												<c:when test='${picture_path ne null and picture_path ne ""}'>
													<div class="photo1"><img src="${RES_PATH}${picture_path}" alt="프로필 사진" ></div>
												</c:when>
												<c:otherwise>
													<div class="photo1"><img src="${IMG}/profile_default.png" alt="프로필 사진" ></div>
												</c:otherwise>
											</c:choose>
						                </div> 
						            </td>
						            <td class="td01 w02">성함</td>
						            <td class="td01 w03">
						            	<c:choose>
											<c:when test='${sessionScope.S_USER_LEVEL eq 3}'>
												<c:set var="name" value="${name} (${department_name}) - ${professor_id }" />
											</c:when>
											<c:when test='${sessionScope.S_USER_LEVEL eq 4}'>
												<c:set var="name" value="${name} (${department_name} ${aca_name})" />
												<c:set var="position_name" value="${group_number} 조" />
											</c:when>
											<c:otherwise>
												<c:set var="name" value="${name}"/>
											</c:otherwise>
										</c:choose>
										
						                <input type="text" class="ip_001 dis" value="${name}" disabled>
						                
						                <c:choose>
							                <c:when test="${group_leader_flag eq 'Y' }">
							                	<span class="tt1 leader" title="posisi명">${position_name} 조장</span>
							                </c:when>
							                <c:otherwise>
												<span class="tt1" title="posisi명">${position_name}</span>							                
							                </c:otherwise>
						                </c:choose>
						                
						            </td>
						        </tr>
						        <tr>
						            <td class="td01 w02">전화번호</td>
						            <td class="td01 td_wrap1 w03"> 
										<input type="text" class="ip_tel1 dis" maxlength="4" value="${fn:split(tel,'-')[0]}" disabled>            
										<span class="sign1">-</span>
										<input type="text" class="ip_tel1 dis" maxlength="4" value="${fn:split(tel,'-')[1]}" disabled>
										<span class="sign1">-</span>
										<input type="text" class="ip_tel1 dis" maxlength="4" value="${fn:split(tel,'-')[2]}" disabled>
						            </td>
						        </tr>
						        <tr>
						            <td class="td01 w02">이메일</td>
						            <td class="td01 w03 td_wrap1">
										<input type="text" class="ip_mail1 dis" value="${email}" disabled>
						            </td>
						        </tr>
						    </tbody>
						</table>
						<div class="t_dd">
							<div class="pop_btn_wrap">
								<button type="button" class="btn00 dis" onclick="location.href='${HOME}/common/my/profile/modify'">perbaiki</button>        
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>