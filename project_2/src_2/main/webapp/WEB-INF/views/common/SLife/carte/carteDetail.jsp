<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function(){
				getCarteDetail();
			});
			
			function carteSetting(list, selector) {
				for (var i=0; i< list.length; i++) {
					$("#"+selector+"_"+i).html(list[i]);
				}
			}
			
			function getCarteDetail() {
				$.ajax({
					type: "GET",
					dataType:"json",
					url: "${HOME}/ajax/common/SLife/carte/detail",
					success: function(data, status) {
						if(data.status == 200) {
							$("#cartePeriod").html(data.carte_period);
							$("#eatTime").html(data.eat_time);
							$("#allDaySale").html(data.all_day_sale);
							weekdaysSetting(data.start_date);
							carteSetting(data.lunch_a_list, "lunchA"); //점심 A
							carteSetting(data.lunch_b_list, "lunchB"); //점심 B
							carteSetting(data.diner_a_list, "dinerA"); //저녁 A
							carteSetting(data.diner_b_list, "dinerB"); //저녁 B
							carteSetting(data.add_food_list, "addFood"); //tambahkan찬
						}
					}
				});
			}
		
			function weekdaysSetting(paramDate) {
				var pDate = paramDate.replace(/\./gi,"/");
				var date = new Date(pDate);
				for (var i=0; i<5; i++) {
					var month = date.getMonth() + 1;
					var day = date.getDate();
					if (day < 10) {
						day = "0"+day;
					}
					$("#day_"+i).html(day);
					date.setDate(date.getDate() + 1);
				}
			}
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
			<div class="contents sub">
				<div class="left_mcon">
					<div class="sub_menu slife">
						<div class="title">학교생활</div>
						<ul class="sub_panel" id="sLifeMenuArea"></ul>
					</div>
				</div>
				<div class="sub_con menu">
					<!-- s_tt_wrap -->
					<div class="tt_wrap">
						<h3 class="am_tt"><span class="tt" id="boardTitleValue"></span></h3>
						<div class="wrap_r"> 
							<div class="cwrap">
								<span class="tt" id="cartePeriod"></span>
							</div>
							<span class="tt2" id="eatTime"></span>
						</div>
					</div>
					<!-- e_tt_wrap --> 

					<!-- s_rfr_con -->
					<div class="rfr_con">
					
						<table class="bk_table_menu">
							<tbody>
								<tr>
									<td class="th01 bd01 w0">구분</td>
									<td class="th01 bd01 w1"><span class="sp_tt">월</span><span class="sp_date" id="day_0"></span></td><!-- 공휴일 day1 -->
									<td class="th01 bd01 w1"><span class="sp_tt">화</span><span class="sp_date" id="day_1"></span></td>
									<td class="th01 bd01 w1"><span class="sp_tt">수</span><span class="sp_date" id="day_2"></span></td>
									<td class="th01 bd01 w1"><span class="sp_tt">목</span><span class="sp_date" id="day_3"></span></td>
									<td class="th01 bd01 w1"><span class="sp_tt">금</span><span class="sp_date" id="day_4"></span></td>
								</tr>
								<tr class="m1">
									<td class="td01 bd01 w0">점심A</td>
									<td class="td01 bd01"><span class="tt" id="lunchA_0"></span></td>
									<td class="td01 bd01"><span class="tt" id="lunchA_1"></span></td>
									<td class="td01 bd01"><span class="tt" id="lunchA_2"></span></td>
									<td class="td01 bd01"><span class="tt" id="lunchA_3"></span></td>
									<td class="td01 bd01"><span class="tt" id="lunchA_4"></span></td>
								</tr>
								<tr class="m2">
									<td class="td01 bd01 w0">점심B</td>
									<td class="td01 bd01"><span class="tt" id="lunchB_0"></span></td>
									<td class="td01 bd01"><span class="tt" id="lunchB_1"></span></td>
									<td class="td01 bd01"><span class="tt" id="lunchB_2"></span></td>
									<td class="td01 bd01"><span class="tt" id="lunchB_3"></span></td>
									<td class="td01 bd01"><span class="tt" id="lunchB_4"></span></td>
								</tr> 
								<tr class="m3">
									<td class="td01 bd01 w0">저녁A</td>
									<td class="td01 bd01"><span class="tt" id="dinerA_0"></span></td>
									<td class="td01 bd01"><span class="tt" id="dinerA_1"></span></td>
									<td class="td01 bd01"><span class="tt" id="dinerA_2"></span></td>
									<td class="td01 bd01"><span class="tt" id="dinerA_3"></span></td>
									<td class="td01 bd01"><span class="tt" id="dinerA_4"></span></td>
								</tr>
								<tr class="m4">
									<td class="td01 bd01 w0">저녁B</td>
									<td class="td01 bd01"><span class="tt" id="dinerB_0"></span></td>
									<td class="td01 bd01"><span class="tt" id="dinerB_1"></span></td>
									<td class="td01 bd01"><span class="tt" id="dinerB_2"></span></td>
									<td class="td01 bd01"><span class="tt" id="dinerB_3"></span></td>
									<td class="td01 bd01"><span class="tt" id="dinerB_4"></span></td>
								</tr> 
								<tr class="m5">
									<td class="td01 bd01 w0">tambahkan찬</td>
									<td class="td01 bd01"><span class="tt" id="addFood_0"></span></td>
									<td class="td01 bd01"><span class="tt" id="addFood_1"></span></td>
									<td class="td01 bd01"><span class="tt" id="addFood_2"></span></td>
									<td class="td01 bd01"><span class="tt" id="addFood_3"></span></td>
									<td class="td01 bd01"><span class="tt" id="addFood_4"></span></td>
								</tr>
							</tbody>
						</table>
					
						<!-- s_tt_wrap -->
						<div class="tt_wrap sub">
							<span class="tt">종일 판매( 10 : 30 ~ 17:00 )</span>
							<div class="wrap_s"><span class="tt" id="allDaySale"></span></div>
							<span class="tts">* 본 식당은 면세사업장으로 카드 및 영수증 발급이 불가하오니, 양해 바랍니다.</span>
							<span class="tts">* 위 식단은 시장 사저에 의해 변경될 수 있습니다.</span>
						</div>
						<!-- e_tt_wrap -->
					</div>
					<!-- e_rfr_con -->
				</div>
				<!-- e_sub_con -->

				<!-- s_right_m -->
				<div class="right_m hidden">
					<button type="button" onclick="location.href='pf_schd_m.html'" title="일정" class="ic_list"></button>
				</div>
				<!-- e_right_m -->
			</div>
			<!-- e_contents -->
		</div>
		<!-- e_container_table -->
	</body>
</html>