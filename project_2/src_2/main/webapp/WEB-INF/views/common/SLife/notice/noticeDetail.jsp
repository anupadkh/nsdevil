<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function(){
 				getNoticeDetail();
			});
			
			function getNoticeDetail() {
				$.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/common/SLife/notice/detail",
			        data: {
			        	"board_seq":"${seq}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var noticeInfo = data.notice_info;
			        		var attachList = data.attach_list;
			        		var nextNoticeInfo = data.next_notice_info;
			        		var prevNoticeInfo = data.prev_notice_info;
			        		
			        		$("#title").html(noticeInfo.title);
			        		$("#regDate").html(noticeInfo.reg_date);
			        		$("#content").html(noticeInfo.content);
			        		
			        		if (attachList.length > 0) {
			        			var attachHtml = '<ul class="h_list m_tb">';
				        		$(attachList).each(function() {
				        			attachHtml += '<li class="li_1" onclick="fileDownload(\'${HOME}\', \''+this.file_path+'\', \'\', \''+this.file_name+'\')">' + this.file_name + '</li>';
				        		});
				        		attachHtml += '</ul>';
				        		$("#attachListArea").html(attachHtml);
			        		} else {
			        			$("#attachListArea").html("");
			        		}
			        		
			        		if (nextNoticeInfo != null) {
			        			$("#nextBtn").attr("onclick", "javascript:location.href='${HOME}/common/SLife/notice/detail?seq=" + nextNoticeInfo.next_board_seq + "'")
			        		}
			        		
			        		if (prevNoticeInfo != null) {
			        			$("#prevBtn").attr("onclick", "javascript:location.href='${HOME}/common/SLife/notice/detail?seq=" + prevNoticeInfo.prev_board_seq + "'")
			        		}
			        		
			        	} else {
			        		alert("공지사항 불러오기에 실패하였습니다. [" + data.status + "]");
			        		location.href="${HOME}/common/SLife/notice/list";
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
			<div class="contents sub">
			<div class="left_mcon">   
				<div class="sub_menu slife">
				    <div class="title">학교생활</div>   
			        <ul class="sub_panel" id="sLifeMenuArea"></ul>
				</div>
			</div>
			<div class="sub_con slife">
				<div class="tt_wrap">
				    <h3 class="am_tt"><span class="tt" id="boardTitleValue"></span></h3>
				    <div class="wrap">
				        <button class="ic_v3" onClick="location.href='${HOME}/common/SLife/notice/list'" title="목록 보기">목록</button>
				    </div>               
				</div>
				<div class="tt_wrap2">                        
				    <span class="tt_con" id="title"> </span>  
				    <span class="tt_date" id="regDate"></span>    
				</div>	
				<div class="rfr_con">
					<div id="attachListArea"></div>
					<div class="h_list mp_2"></div>
					<div class="con_wrap">
						<div class="con_s con_s_h" id="content"></div>
					</div>
				</div> 
				<div class="btn_wrap">
				    <button class="btn1" id="nextBtn" onclick="javascript:alert('다음 글이 없습니다.'); return false;"><span><span class="ic">▲</span>다음 글</span></button>
				    <button class="btn2" id="prevBtn" onclick="javascript:alert('이전 글이 없습니다.'); return false;"><span><span class="ic">▼</span>이전 글</span></button>
				</div>
			</div>
			
			</div>
		</div>
	</body>
</html>