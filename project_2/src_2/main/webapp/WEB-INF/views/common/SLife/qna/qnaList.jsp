<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script src="${JS}/lib/jquery.ui.accordion.js"></script>
		<script type="text/javascript">
			function accordionInit() {
				$("#qnaList").accordion({collapsible: true, active: false}).accordion("refresh");
			}
			
			
			function qnaListView(page) {
				
				if (typeof page == "undefined") {
					page = 1;
				}
				
				$("input[name='page']").val(page);
				
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/common/SLife/qna/list",
			        data: $("#searchFrm").serialize(),
			        dataType: "json",
			        success: function(data, status) {
			        	var html = "";
			        	$.each(data.list, function() {
							html += qnaListCreate(this);
			        	});
			        	if(data.list.length > 0) {
				        	$("#qnaList").html(html);
				            $(".pagination").html(data.pageNav);
		        		} else {
		        			html = "<ul><li class=\"faq-list-content-li\"><div class=\"question\">registrasi된 게시글이 없습니다.</div></li></ul>";
		        			$("#qnaList").html(html);
		        		}
			        	accordionInit();
			        	autosize($("textarea"));
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function qnaListCreate(t) {
				var qPicture = "${DEFAULT_PICTURE_IMG}";
				var answerPicture = "${DEFAULT_PICTURE_IMG}";
				var qnaContent = "";
				if (t.qPicture != "" && typeof t.qPicture != "undefined") {
					qPicture = t.qPicture;
				}
				if (t.answerPicture != "" && typeof t.answerPicture != "undefined") {
					answerPicture = t.answerPicture;
				}
				if (t.answer_state == 'Y') { //답변완료
					qnaContent += '<div class="title">';
					qnaContent += '    <span class="sp_tt">';
					qnaContent += '        <span class="tt">' + t.q_title + '</span>';
					qnaContent += '        <span class="sp1">tanggal registrasi시 : ' + t.q_reg_date + '</span>';
					qnaContent += '        <span class="sp2_1">답변완료</span>';
					qnaContent += '    </span>';
					qnaContent += '</div>';
					qnaContent += '<ul class="panel">';
					qnaContent += '    <input type="hidden" name="qnaSeq" value="' + t.qna_seq + '">';
					qnaContent += '    <li class="qwrap1">';
					qnaContent += '        <span class="a_mp">';
					qnaContent += '            <span class="pt01"><img src="' + qPicture + '" alt="사진" class="pt_img"></span>';
					qnaContent += '            <span class="ssp1">';
					qnaContent += '                <span>' + t.q_name + '</span>';
					qnaContent += '                <span class="ssp2">(' + t.q_department_name + ')</span>';
					qnaContent += '            </span>';
					qnaContent += '        </span>';
					qnaContent += '    </li>';
					qnaContent += '    <li class="qwrap2">' + t.q_content + '</li>';
					qnaContent += '    <li class="awrap1 tarea free_textarea">';
					if ("${S_USER_LEVEL}" < 4) { //학생이상(교수,Dosen Penanggung jawab,행정,manajemen자)
						qnaContent += '    <textarea class="tarea01" style="height: 30px;" maxlength="500" name="answerContent">' + t.answer_content + '</textarea> ';	
					} else {
						qnaContent += '    <textarea class="tarea01" style="height: 30px;" disabled maxlength="500" name="answerContent">' + t.answer_content + '</textarea> ';
					}
					qnaContent += '    </li>';
					qnaContent += '    <li class="awrap2">';
					if ("${S_USER_LEVEL}" < 4) { //학생이상(교수,Dosen Penanggung jawab,행정,manajemen자)
						qnaContent += '    <button class="btn_qnar2" title="답변 perbaiki하기" onclick="answerModify(this);">perbaiki</button><button class="btn_qnar3" title="답변 삭제하기" onclick="answerRemove(this);">삭제</button>';
					}					
					qnaContent += '        <span class="a_mp">';
					qnaContent += '            <span class="pt01"><img src="' + answerPicture + '" alt="사진" class="pt_img"></span>';
					qnaContent += '            <span class="ssp1">';
					qnaContent += '                <span>' + t.answer_name + '</span>';
					qnaContent += '                <span class="ssp2">(' + t.answer_department_name + ')</span>';
					qnaContent += '            </span>';
					qnaContent += '        </span>';
					qnaContent += '    </li>';
					qnaContent += '</ul>';
				} else { //대기 중
					if ("${sessionScope.S_USER_PICTURE_PATH}" != "") {
						answerPicture = "${sessionScope.S_USER_PICTURE_PATH}";
					}
					qnaContent += '<div class="title">';
					qnaContent += '    <span class="sp_tt">';
					qnaContent += '        <span class="tt">' + t.q_title + '</span>';
					qnaContent += '        <span class="sp1">tanggal registrasi시 : ' + t.q_reg_date + '</span>';
					qnaContent += '        <span class="sp2_2">대기 중</span>    ';
					qnaContent += '    </span>';
					qnaContent += '</div>';
					qnaContent += '<ul class="panel">';
					qnaContent += '    <input type="hidden" name="qnaSeq" value="' + t.qna_seq + '">';
					qnaContent += '    <li class="qwrap1">';
					qnaContent += '        <span class="a_mp">';
					qnaContent += '        	<span class="pt01"><img src="' + qPicture + '" alt="사진" class="pt_img"></span>';
					qnaContent += '        	<span class="ssp1"><span>' + t.q_name + '</span>';
					qnaContent += '        	<span class="ssp2">(' + t.q_department_name + ')</span></span>';
					qnaContent += '        </span>';
					qnaContent += '    </li>';
					qnaContent += '    <li class="qwrap2">' + t.q_content + '</li>';
					if (t.q_answer_user_seq == "${sessionScope.S_USER_SEQ}" || "${sessionScope.S_USER_LEVEL}" < 3) {//답변자거나 행정직원
						qnaContent += '<li class="awrap1 tarea free_textarea">';
						qnaContent += '    <textarea class="tarea01" style="height: 30px;" name="answerContent" maxlength="500" placeholder="답변을 registrasi하세요. (최대 500자)">' + t.answer_content + '</textarea>';
						qnaContent += '</li>';
						qnaContent += '<li class="awrap2">';
						qnaContent += '    <button class="btn_qnar1" title="답변 registrasi하기" onclick="answerSubmit(this);">registrasi</button>';
						qnaContent += '    <span class="a_mp">';
						qnaContent += '       <span class="pt01"><img src="' + answerPicture + '" alt="사진" class="pt_img"></span>';
						qnaContent += '          <span class="ssp1">';
						qnaContent += '           <span>${S_USER_NAME}</span>';
						qnaContent += '           <span class="ssp2">(${sessionScope.S_USER_DEPARTMENT_NAME})</span>';
						qnaContent += '       </span>';
						qnaContent += '   </span>';
						qnaContent += '</li>';
					}
					qnaContent += '</ul>';
				}
				return qnaContent;
			}
			
			function answerSubmit(t) {
				if(confirm("답변을 registrasi하시겠습니까?")) {
					var ulPanel = $(t).parent().parent();
					var answerContent = $(ulPanel).find("textarea[name='answerContent']").val();
					if (isEmpty(answerContent) || isBlank(answerContent)) {
						alert("답변 isi을 입력해주세요.");
						$(answerContent).focus();
						return false;
					}
					
					$.ajax({
				        type: "POST",
				        url: "${HOME}/ajax/common/SLife/qna/answer/create",
				        data: {
				        	"qna_seq" : $(ulPanel).find("input[name='qnaSeq']").val(),
				        	"answer_content" : answerContent
				        },
				        dataType: "json",
				        success: function(data, status) {
				            if (data.status == "200") {
				            	alert("답변이 registrasi되었습니다.");
				            } else {
				            	alert("답변이 registrasi이 실패하였습니다.");
				            }
				            location.reload();
				        },
				        error: function(xhr, textStatus) {
				            document.write(xhr.responseText);
				        },
				        beforeSend:function() {
				            $.blockUI();
				        },
				        complete:function() {
				            $.unblockUI();
				        }
				    });
				}
			}
			
			
			function answerModify(t) {
				if(confirm("답변의 isi을 perbaiki하시겠습니까?")) {
					var ulPanel = $(t).parent().parent();
					var answerContent = $(ulPanel).find("textarea[name='answerContent']").val();
					if (isEmpty(answerContent) || isBlank(answerContent)) {
						alert("답변 isi을 입력해주세요.");
						$(answerContent).focus();
						return false;
					}
					
					$.ajax({
				        type: "POST",
				        url: "${HOME}/ajax/common/SLife/qna/answer/modify",
				        data: {
				        	"qna_seq" : $(ulPanel).find("input[name='qnaSeq']").val(),
				        	"answer_content" : answerContent
				        },
				        dataType: "json",
				        success: function(data, status) {
				        	if (data.status == "200") {
				            	alert("답변의 isi이 perbaiki되었습니다.");
				            } else {
				            	alert("답변의 isi perbaiki이 실패하였습니다.");
				            }
				            location.reload();
				        },
				        error: function(xhr, textStatus) {
				            document.write(xhr.responseText);
				        },
				        beforeSend:function() {
				            $.blockUI();
				        },
				        complete:function() {
				            $.unblockUI();
				        }
				    });
				}
			}
			
			function answerRemove(t) {
				if(confirm("답변을 삭제하시겠습니까?")) {
					var ulPanel = $(t).parent().parent();
					$.ajax({
				        type: "POST",
				        url: "${HOME}/ajax/common/SLife/qna/answer/remove",
				        data: {
				        	"qna_seq" : $(ulPanel).find("input[name='qnaSeq']").val()
				        },
				        dataType: "json",
				        success: function(data, status) {
				        	if (data.status == "200") {
				            	alert("답변이 삭제되었습니다.");
				            } else {
				            	alert("답변 삭제에 실패하였습니다.");
				            }
				            location.reload();
				        },
				        error: function(xhr, textStatus) {
				            document.write(xhr.responseText);
				        },
				        beforeSend:function() {
				            $.blockUI();
				        },
				        complete:function() {
				            $.unblockUI();
				        }
				    });
				}
			}
			
			$(document).ready(function(){
				qnaListView(1);
			});
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
			<div class="contents sub">
				<div class="left_mcon">   
					<div class="sub_menu slife">
					    <div class="title">학교생활</div>   
					    <ul class="sub_panel" id="sLifeMenuArea"></ul>
					</div>
				</div>
				<div class="sub_con slife">
					<div class="tt_wrap">
					    <h3 class="am_tt"><span class="tt" id="boardTitleValue">1 : 1 질의 응답</span></h3>
					    <div class="wrap">
					    	<button type="button" class="btn_qnar" onclick="location.href='${HOME}/common/SLife/qna/question/create';" >1 : 1 질의registrasi</button>     
					    </div>               
					</div>
					<div class="rfr_con">
						<div class="qna_wrap">
							<div class="boardwrap" id="qnaList">
							</div>
						</div>
					</div> 
					<div class="pagination"></div>
				</div>
			</div>
		</div>
	</body>
</html>