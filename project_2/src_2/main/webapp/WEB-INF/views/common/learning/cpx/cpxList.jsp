<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script src="${JS}/lib/datetimep_1.js"></script>
		<script src="${JS}/lib/monthdatepicker_ui.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				getCPXCodeList();
				boardListView(1);
				
				$.datetimepicker.setLocale('kr');
			    $('#startDate').datetimepicker({
			        timepicker:false,
			        datepicker:true,
			        format:'y-m-d',
			        formatDate:'y-m-d'
			    });
			    
			    $('#endDate').datetimepicker({
			        timepicker:false,
			        datepicker:true,
			        format:'y-m-d',
			        formatDate:'y-m-d'
			    });
			    
			    $("#startDate").on("change", function(e){
			    	var startDate = $("#startDate").val();
			    	if (startDate != "") {
				    	$('#endDate').datetimepicker({minDate: startDate});
			    	}
			    });
			    
			    $("#endDate").on("change", function(e){
			    	var endDate = $("#endDate").val();
			    	if (endDate != "") {
				    	$('#startDate').datetimepicker({maxDate: endDate});
			    	}
			    });
			});
	
			function boardListView(page) {
				if (typeof page == "undefined") {
					page = 1;
				}
				$("input[name='page']").val(page);
				
				var startDate = $("#startDate").val();
				var endDate = $("#endDate").val();
				
				if (!isEmpty(startDate)) {
					if (!isValidDate(startDate)) {
						alert("시작일의 hari dan tanggal형식이 올바르지않습니다.(YYYY-MM-DD)")
						return false;
					}
				}
				
				if (!isEmpty(endDate)) {
					if (!isValidDate(endDate)) {
						alert("종료일의 hari dan tanggal형식이 올바르지않습니다.(YYYY-MM-DD)")
						return false;
					}
				}
				
				var cateCodeValue = $("#cpxCodeSelectBox").attr("value");
				$("#searchCPXCode").val(cateCodeValue);
				
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/common/learning/cpx/list",
			        data: $("#searchFrm").serialize(),
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var list = data.list;
				        	var listHtml = '';
				        	$(list).each(function(){
				        		var boardSeq = this.board_seq;
				        		var rowNum = this.row_num;
				        		var hits = this.hits;
				        		var title = this.title;
				        		var cpxName = this.cpx_name;
				        		var content = this.content;
				        		var filePath = this.file_path;
				        		var userName = this.name;
				        		var departName = this.user_department_name;
				        		var userLevel = this.user_level;

				        		if (Number(userLevel) > 2) {
				        			userName = userName + " ("+departName+")";
				        		} else {
				        			userName = "manajemen자";
				        		}
				        		content = content.replace(/(<([^>]+)>)/ig,"");
								content = content.replace(/\n/g, "");//행 바꿈 제거
				            	content = content.replace(/\s+/, "");//왼쪽 공백 제거
				            	content = content.replace(/\r/g, "");//엔터 제거
				        		var fileCnt = this.file_cnt;
				        		var regDate = this.reg_date;
				        		if (cpxName == "") {
				        			cpxName = 'Keseluruhan';
				        		}
				        		listHtml += '<div class="cln_wrap">';
				        		listHtml += '    <div class="pt_wrap">';
				        		listHtml += '        <video width="220" height="140" controls>';
				        		listHtml += '            <source src="${RES_PATH}'+filePath+'" type="video/mp4">';
				        		listHtml += '        </video>';
				        		listHtml += '    </div>';
				        		listHtml += '    <div class="tt_wrap">';
				        		listHtml += '        <span class="sp01" onclick="boardDetail('+boardSeq+');" title="상세보기"><span class="tt1 custom">[ ' + cpxName + ' ]</span><span class="tt2">' + title + '</span></span>';
				        		listHtml += '        <span class="sp02">' + regDate + '</span>';
				        		listHtml += '        <span class="sp03"><span class="tt4">작성자 : ' + userName + '</span></span>';
				        		listHtml += '    </div>';
				        		listHtml += '	 <button class="dw" title="다운로드" onclick="javascript:redirectZipfileDownload(\'${HOME}\','+boardSeq+', '+fileCnt+', \'' + title + '\', \'/common/learning/osce/list\');">다운로드</button>';
				        		listHtml += '</div>';
				        	});
				        	if (list.length > 0) {
				        		$("#boardList").html(listHtml);
					        	$("#pageNationArea").html(data.pageNav);
				        	} else {
				        		$("#boardList").html('<tr><td colspan="3">registrasi된 게시글이 없습니다.</td></tr>');
				        	}
			        	} else {
			        		//TODO
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function getCPXCodeList() {
				$.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/common/learning/cpx/code/list",
			        data: {},
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		cpxCodeSelectBoxSetting(data.code_list);
			        	} else {
			        		alert("임상표현 항목 불러오기에 실패하였습니다. [" + data.status + "]");
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			
			function cpxCodeSelectBoxSetting(list) {
				var target = $("#cpxCodeSelectBox").find("div.uoptions");
				var listHtml = '<span class="uoption" value="">Keseluruhan</span>';
				$(list).each(function() {
					listHtml += '<span class="uoption" value="' + this.cpx_code + '">' + this.cpx_name + '</span>';
				});
				$(target).html(listHtml);
			}
			
			function boardDetail(boardSeq) {
				if (typeof boardSeq !=  "undefined" && boardSeq != "") {
					location.href = '${HOME}/common/learning/cpx/detail?seq='+boardSeq
				}
			}
			
			function cpxCreateView() {
				location.href="${HOME}/common/learning/cpx/create";
			}
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
			<div class="contents sub">
				<div class="left_mcon">   
					<div class="sub_menu">
					    <div class="title">학습자료실</div>   
					    <ul class="sub_panel" id="learningMenuArea"></ul>
					</div>
				</div>
				<div class="sub_con">
					<div class="tt_wrap">
					    <h3 class="am_tt"><span class="tt" id="boardTitleValue"></span></h3>
					    <c:if test='${sessionScope.S_USER_LEVEL ne NULL and sessionScope.S_USER_LEVEL < 4}'>
						    <div class="wrap">
						        <button class="ic_v4" onClick="cpxCreateView();" title="임상표현 동영상 registrasi">registrasi</button>
						    </div>               
					    </c:if>
					</div>
						
					<form id="searchFrm" name="searchFrm" method="get" onsubmit="return false;">
						<input type="hidden" id="searchCPXCode" name="searchCPXCode" value="">
						<div class="sch_wrap">
							<span class="tt">구분</span>
							<div class="wrap2_2_uselectbox custom">
								<div class="uselectbox" id="cpxCodeSelectBox" value="">
									<span class="uselected">Keseluruhan</span>
									<span class="uarrow">▼</span>			
									<div class="uoptions" style="display: none;">
										<span class="uoption firstseleted">Keseluruhan</span>
									</div>
								</div>
							</div>
							<input type="hidden" id="page" name="page">
						    <span class="tt">tanggal registrasi구간</span>
						    <input type="text" class="ip_date" id="startDate" name="search_start_date" placeholder="시작일">
						    <span class="tt">~</span>
						    <input type="text" class="ip_date" id="endDate" name="search_end_date" placeholder="종료일">
						    <input type="text" class="ip_search" name="search_text" placeholder="제목">
						    <button class="btn_search1" onclick="javascript:boardListView(1);"></button>
						</div>
					</form>
					
					<div class="rfr_con">
						<div class="cl_wrap" id="boardList"></div>
					</div>

					<div class="pagination" id="pageNationArea"></div>
				</div>
			</div>
		</div>
	</body>
</html>