<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<link rel="stylesheet"
	href="${RES}/fullcalendar-v3.6.2/css/calendar1.css">
<link rel="stylesheet"
	href="${RES}/fullcalendar-v3.6.2/css/fullcalendar.print.css"
	media="print">
<script src="${RES}/fullcalendar-v3.6.2/js/moment.min.js"></script>
<script src="${RES}/fullcalendar-v3.6.2/js/fullcalendar.js"></script>
<script src="${RES}/fullcalendar-v3.6.2/js/locale-all.js"></script>
<script src="${JS}/schedule.js"></script>
<script type="text/javascript">
	var color = [
		"#F6B190"
		, "#D0BB63"
		, "#8DD4C3"
		, "#86CCED"
		, "#7EA6E9"
		, "#BC95E8"
		, "#F78880"
		, "#C600FF"
		, "#FF8000" 
		, "#E5CC80"
		, "#F48CBA"
		, "#AAD372"
		, "#FFD100"
		, "#FF0000"
		, "#00FF00"
		, "#71D5FF"
		, "#FFFF98"
		, "#6D6E70"
		, "#0081FF"
		, "#FFF468"
		, "#9382C9"
		, "#00FFBA"
		, "#88AAFF"
		, "#40BF40"
		, "#FFFF00"
		, "#634E37"
		, "#7A465D"
		, "#556A39"
		, "#807A34"
		, "#620F1E"
		, "#122D80"
		, "#346678"
		, "#4A4165"
		, "#803E05"
		, "#"
		, "#"
		, "#"
		, "#"
		]
	$(document).ready(function() {
		$("body").addClass("full_schd");
		$('.free_textarea').on( 'keyup', 'textarea', function (e){
			$(this).css('height', 'auto' );
			$(this).height( this.scrollHeight );
		});
		$('.free_textarea').find( 'textarea' ).keyup();
		
		$("#open_01").hide();
        $("#fold_01").click(function(){
	        $("#top_pop").hide();
			$("#open_01").show();
	        $("#fold_01").hide();
	    });
        $("#open_01").click(function(){
	        $("#top_pop").show();
			$("#open_01").hide();
	        $("#fold_01").show();
	    });
		
        $('#calendar1').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaTwoWeek,agendaWeek,agendaDay,listWeek'
			},
			navLinks: true,
			editable: true,
			locale: "ko",
			eventLimit: true,
			fixedWeekCount: true,	//true: 6주로 고정
			viewRender : function (view, element) {
				//tahun가 변경되었을때만 로드 
  				var calendarYear = $('#calendar1').fullCalendar('getDate').format("YYYY");
				if ($(":input[name='calendarYear']").val() != calendarYear) {
					getScheduleMonth("");
				}
			},
			eventDrop: function(event, delta, revertFunc) {
				revertFunc();
				return;
			
				if (event.event_type == "lesson" && "${S_USER_LEVEL}" == "3" && "${S_USER_SEQ}" != event.reg_user_seq) {
					alert("변경할 권한이 없습니다.");
					revertFunc();
					return;
				}
				if (confirm("\"" + event.title + "\" 일정을 " + event.start.format("YYYY-MM-DD") + " " + event.start.format("a hh:mm") + "로 변경하시겠습니까?")) {
		        	changeEventDate(event.event_seq, event.event_type, event.start.format("YYYY-MM-DD"), event.start.format("HH:mm"), event.event_minute);
		        } else {
		        	revertFunc();
		        }
		    },
		    eventResize: function(event, delta, revertFunc) {
		    	revertFunc();
		    },
			views: {
				agendaTwoWeek: {
					type: 'basic',
					duration: { weeks: 2 },
					buttonText: '2주'
				}
			},
			minTime: '08:00:00',
	        maxTime: '22:00:00'
		});
        
        $(".btn_l").bind("click", function(){
            $(".btn_l").toggleClass("move-trigger_l");
            $(".aside_l").toggleClass("folding");
            $(".main_con").toggleClass("");
        });
        
        $("#btnDiv").remove();
	});
	
	//waktu표 목록
	function getScheduleMonth(curr_seq) {
		/* 
		if (curr_seq == "") {
			alert("교육과정을 pilih해 주세요.");
			history.back(-1);
			return;
		} */
		
		var dateStart = $('#calendar1').fullCalendar('getView').start.format("YYYY-MM-DD");
		var dateEnd = $('#calendar1').fullCalendar('getView').end.format("YYYY-MM-DD");
		var calendarYear = $('#calendar1').fullCalendar('getDate').format("YYYY");
		
        $.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/getMYSchedule",
            data: {
            	"curr_seq": curr_seq,
            	"dateStart" : dateStart,
            	"dateEnd" : dateEnd,
            	"calendarYear": calendarYear
            },
            dataType: "json",
            success: function(data, status) {

            	$(":input[name='calendarYear']").val(calendarYear);
            	
            	var html = "";
            	var pre_curr_seq = "";

            	var eventList = [];
            	var curr_index = -1;
            	$.each(data.schedule, function(index){
            		
            		if(pre_curr_seq != this.curr_seq){
            			curr_index++;
            			var startDate = this.curr_start_date;
	                	var endDate = this.curr_end_date;
	                	startDate += "T00:00:00";
                		endDate += "T24:00:00";
	            		var currEventList =	{
		                		event_type: "lesson",
		                		title: this.curr_name+"["+this.curr_start_date_mmdd+"~"+this.curr_end_date_mmdd+"]",
		    					start: this.curr_start_date,
		    					end: this.curr_end_date,
		    					editable: false,
		    					color : color[curr_index],
		    					textColor : "white",
								start: startDate,
								end: endDate,
			    				allDay : true
		                	};
	            		eventList.push(currEventList);
            		}
            		
            		if(this.lp_seq != 0){
	            		var startDate = this.lesson_date;
	                	var endDate = this.lesson_date;
	                	
	                	if (this.start_time != null && this.end_time != null) {
	                		startDate += "T" + this.start_time;
	                		endDate += "T" + this.end_time;
	                	}
	                	
	                	var eventMinute = ampmTimeToMinute(this.end_time) - ampmTimeToMinute(this.start_time);
	                	
	                	var period = this.period.split(",");
	                	
	                	if(period.length > 1)
	                		period = "["+period[0]+"~"+period[period.length-1]+"]";
	                	else
	                		period = "["+this.period+"]";
	                	
	                	var newEvent = {
	                		event_type: "lesson",
	                		event_seq : this.lp_seq,
	                		title: period+this.lesson_subject,
	                		reg_user_seq: this.reg_user_seq,
							url: 'javascript:openEditSchedulePopup(' + this.lp_seq + ','+this.curr_seq+');',
							start: startDate,
							end: endDate,
							event_minute: eventMinute,
	    					color : color[curr_index],
	    					textColor : "white"
						}
	                	$('.fc-event').css('font-size', '0.9em');
	                	
						eventList.push(newEvent);  
            		}
                	pre_curr_seq = this.curr_seq;
                    
            	});

            	$('#calendar1').fullCalendar('removeEventSources');
                $('#calendar1').fullCalendar('addEventSource', eventList);
                
                bindPopupEvent("#m_pop1", ".open1");
            },
            error: function(xhr, textStatus) {
                alert("오류가 발생했습니다.");
                //document.write(xhr.responseText);
            }
        });
    }
	
	function changeEventDate(event_seq, event_type, event_date, start_time, event_minute) {
		//종료waktu 계산
		var endMinute = (Number(start_time.substring(0, 2)) * 60) + Number(start_time.substring(3, 5)) + event_minute;
		var end_time = Math.floor(endMinute / 60) + ":" + endMinute % 60;
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/changeEventDate",
            data: {
            	"event_seq" : event_seq,
            	"event_type" : event_type,
            	"event_date" : event_date,
            	"start_time" : start_time,
            	"end_time" : end_time
            },
            dataType: "json",
            success: function(data, status) {
            	if (data.status == "200") {
            		alert("변경 되었습니다.");
            		getScheduleMonth("${S_CURRICULUM_SEQ}");
            	} else {
					alert(data.msg);
				}
            },
            error: function(xhr, textStatus) {
                alert("오류가 발생했습니다.");
                //document.write(xhr.responseText);
            }
        });
	}
	
	function ExcelVer(){
 		post_to_url("${HOME}/aca/MYschedule", {"curr_seq":$("#curr_seq span:eq(0)").attr("data-value")});		
	}
	
	function SearchVer(){
		post_to_url("${HOME}/aca/MYscheduleSearch", {"curr_seq":$("#curr_seq span:eq(0)").attr("data-value")});
	}
</script>
</head>

<body class="full_schd">

	<!-- s_container_table -->
	<div id="container" class="container_table">
		<!-- s_contents -->
		<div class="contents main">

			<!-- s_left_mcon -->
			<div class="left_mcon aside_l">

				<div class="sub_menu st_am">
					<div class="title">학사일정</div>
					<ul class="sub_panel">
						<li class="mn"><a href="${HOME }/aca/academicM" class="">학 사 력</a></li>
						<li class="mn"><a href="${HOME }/aca/MYscheduleM" class="on">MY waktu표</a></li>
						<li class="mn"><a href="${HOME }/aca/MYscheduleMemo" class="">개인일정</a></li>
						<c:choose>
							<c:when test="${S_USER_LEVEL == 4}">
								<li class="mn"><a href="" class="">수강신청</a></li>
							</c:when>
						</c:choose>
					</ul>
				</div>

			</div>
			<!-- e_left_mcon -->

			<!-- s_main_con -->
			<div class="main_con cld">
				<!-- s_메뉴 접기 버튼 -->
				<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
				<!-- e_메뉴 접기 버튼 -->

				<!-- s_mpf_tabcontent2 -->
				<div class="mpf_tabcontent2">
					<!-- s_tt_wrap -->
					<div class="tt_wrap">

						<!-- s_sch_wrap -->
						<div class="sch_wrap">
							<!-- s_wrap_p1_uselectbox -->

							<div class="wrap_p1_uselectbox" style="width:300px;">
								<div class="uselectbox" id="curr_seq">
									<span class="uselected" data-value="">Keseluruhan 교육과정</span> <span class="uarrow">▼</span>
									<div class="uoptions">
										<span class="uoption firstseleted" data-value="" onClick="getScheduleMonth('');">Keseluruhan 교육과정</span> 
										<c:forEach var="currList" items="${currList}">
		                                    <span class="uoption" data-value="${currList.curr_seq}" onClick="getScheduleMonth('${currList.curr_seq}');">${currList.curr_name}</span>
		                                </c:forEach>
									</div>
								</div>
							</div>

							<!-- e_wrap_p1_uselectbox -->
							<!-- <button onclick="javascript:ExcelVer();" class="btn_view full">엑셀 보기</button> -->
							<!-- <button onclick="javascript:SearchVer();" class="btn_search1" title="Pencarian"></button> -->

						</div>
						<!-- e_sch_wrap -->
					</div>
					<!-- e_tt_wrap -->

					<!-- s_calendar1 -->
					<div id="calendar1">
						<input type="hidden" name="calendarYear">
					</div>
					<!-- e_calendar1 -->
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="../../mpf/lesson/schedulePopup.jsp">
	<jsp:param name="pageName" value="scheduleM" />
</jsp:include>
</body>
</html>