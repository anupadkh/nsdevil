<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function(){
				getCodeCountList();
			});
			
			function getCodeCountList() {
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/admin/user/code/specialty/list",
			        data: {},
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var list = data.code_pf_cnt_list;
			        		var listHtml = "";
			        		var totalCnt = 0;
			        		$(list).each(function(idx){
			        				listHtml += '<tr class="codeTr" value="' + (idx+1) + '">';
			        				listHtml += '	<input type="hidden" name="codeCode" value="' + this.code + '">';
			        				listHtml += '	<input type="hidden" name="pfCnt" value="' + this.pf_cnt + '">';
			        				listHtml += '	<td class="td_1 row_num">' + (idx + 1) + '</td>';
			        				listHtml += '	<td class="td_1">';
			        				listHtml += '		<div class="mdf_wrap_a">';
		        					listHtml += '			<input type="text" class="ip01a" name="codeName" value="' + this.code_name + '">';
			        				listHtml += '		</div>';
			        				listHtml += '	</td>';
			        				listHtml += '	<td class="td_1">';
			        				listHtml += '		<div id="managementArea">';
		        					listHtml += '		<button class="mdf_wrap_b open_s1" onclick="removeCodePopupOpen(' + (idx+1) + ');"><span class="btn_x">X</span><span class="tts">삭제</span></button>';
			        				listHtml += '		</div>';
			        				listHtml += '		<div class="mdf_wrap_c">';
			        				listHtml += '			<div class="up" title="항목 위로" onclick="orderUpDown(this, true);">▲</div>';
			        				listHtml += '			<div class="down" title="항목 아래로" onclick="orderUpDown(this, false);">▼</div>';
			        				listHtml += '		</div>';
			        				listHtml += '	</td>';
			        				listHtml += '</tr>';	
			        		});
			        		listHtml += '<tr class="add">';
		        			listHtml += '    <td class="td_1 plus" colspan="3">';
	        				listHtml += '		<div class="mdf_wrap_d">';
        					listHtml += '			<button class="btn_add" title="tambahkan" onclick="codeAdd();"></button>';
       						listHtml += '			<span class="tts">jurusan khusus tambahkan하기</span>';
     						listHtml += '		</div>';
     						listHtml += '	</td>';
    						listHtml += '</tr>';
			        		$("#codeList").html(listHtml);
			        	} else {
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function orderUpDown(t, flag) {
				
				var isUpFlag = flag;
				
				//현재
				var	currTarget = $(t).parent().parent().parent();
				var currIdx = $(currTarget).attr("value");
				var currCodeCode = $(currTarget).find("input[name='codeCode']");
				var currCodeName = $(currTarget).find("input[name='codeName']");
				var currPfCnt = $(currTarget).find("input[name='pfCnt']");
				if (isUpFlag) {
					if (currIdx == 1) {
						return false;
					}
					currIdx = parseInt(currIdx) - 1;
				} else {
					if (currIdx == $("#codeList").find("tr").length-1) {
						return false;
					}
					currIdx = parseInt(currIdx) + 1;
				}

				//이동Target
				var moveTarget = $(".codeTr[value="+currIdx+"]");
				var moveCodeCode = $(moveTarget).find("input[name='codeCode']");
				var moveCodeName = $(moveTarget).find("input[name='codeName']");
				var movePfCnt = $(moveTarget).find("input[name='pfCnt']");
				
				//1. 현재 -> Temp
				var tempCode = $(currCodeCode).val();
				var tempName = $(currCodeName).val();
				var tempPfCnt = $(currPfCnt).val();

				//2. 이동Target -> 현재
				$(currCodeCode).val($(moveCodeCode).val());
				$(currCodeName).val($(moveCodeName).val());
				$(currPfCnt).val($(movePfCnt).val());
				
				//3. Temp -> 이동Target
				$(moveCodeCode).val(tempCode);
				$(moveCodeName).val(tempName);
				$(movePfCnt).val(tempPfCnt);
				managementSetting();
			}
			
			function managementSetting() {
				$("#codeList>tr").each(function (idx){
					if (!$(this).hasClass(".add")) {
						var code = $(this).find("input[name='codeCode']").val();
						var managementHtml = '<button class="mdf_wrap_b open_s1" onclick="removeCodePopupOpen(' + (idx+1) + ');"><span class="btn_x">X</span><span class="tts">삭제</span></button>' 
						$(this).find("#managementArea").html(managementHtml);
						$(this).find("td.row_num").html((idx+1));
						$(this).attr("value", (idx+1));
					}
				});
			}
			
			function codeAdd() {
				var addHtml = "";
				addHtml += '<tr class="codeTr" value="">';
				addHtml += '	<input type="hidden" name="codeCode" value="">';
				addHtml += '	<input type="hidden" name="pfCnt" value="">';
				addHtml += '	<td class="td_1 row_num"></td>';
				addHtml += '	<td class="td_1">';
				addHtml += '		<div class="mdf_wrap_a">';
				addHtml += '			<input type="text" class="ip01a" name="codeName" value="">';
				addHtml += '		</div>';
				addHtml += '	</td>';
				addHtml += '	<td class="td_1">';
				addHtml += '		<div id="managementArea">';
				addHtml += '			<button class="mdf_wrap_b open_s1" ><span class="btn_x">X</span><span class="tts">삭제</span></button>';
				addHtml += '		</div>';
				addHtml += '		<div class="mdf_wrap_c">';
				addHtml += '			<div class="up" title="항목 위로" onclick="orderUpDown(this, true);">▲</div>';
				addHtml += '			<div class="down" title="항목 아래로" onclick="orderUpDown(this, false);">▼</div>';
				addHtml += '		</div>';
				addHtml += '	</td>';
				addHtml += '</tr>';
				$("#codeList>tr.add").before(addHtml);
				managementSetting();
			}
			
			function codeSubmit() {
				//현재
				var codeArray = new Array();
				var isInvalidate = false;
				$("#codeList>tr").each(function (idx){
					if ($("#codeList>tr").length-1 > idx) {
						var obj = new Object();
						var code = $(this).find("input[name='codeCode']").val();
						var codeName = $(this).find("input[name='codeName']").val();
						obj.code = code;
						obj.code_name = codeName;
						obj.code_order = (idx+1);
						if (isEmpty(codeName) || isBlank(codeName)) {
							isInvalidate = true;
						}
						codeArray.push(obj);
					}
				});
				
				if (isInvalidate) {
					alert("jurusan khusus을 입력해주세요.");
					return false;
				}

				var removeCodeArrStr = $("input[name='removeCodeArr']").val();
				if (removeCodeArrStr != "") {
					removeCodeArrStr = removeCodeArrStr.substring(0, removeCodeArrStr.length-1);
				}
				var removeCodeArray = new Array();
				var removeCodeArr = removeCodeArrStr.split(",");
				for (var i = 0; i<removeCodeArr.length; i++) {
					if (removeCodeArr[i] != "") {
						var obj = new Object();
						obj.code = removeCodeArr[i];
						removeCodeArray.push(obj);
					}
				}
				
				var codeArrayStr = JSON.stringify(codeArray);
				var removeCodeArrayStr = JSON.stringify(removeCodeArray);
				$.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/admin/user/code/specialty/modify",
			        data: {
			        	"codeListStr" : codeArrayStr
			        	, "removeCodeListStr" : removeCodeArrayStr
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		alert("simpan이 완료되었습니다.");
			        		history.back(-1);
			        	} else {
			        		alert("simpan에 실패하였습니다.");
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });				
				
			}
			
			function removeCodePopupOpen(idx) {
				var removeTarget = $(".codeTr[value="+idx+"]");
				var code = $(removeTarget).find("input[name='codeCode']").val();
				var pfCnt = $(removeTarget).find("input[name='pfCnt']").val();
				var codeName = $(removeTarget).find("input[name='codeName']").val();
				if (pfCnt > 0) {
					$("#removeCodePopup").show();
					$(".code_name").html('[ '+codeName+' ]');
					$("#removeCodeBtn").attr("onclick","removeCode("+idx+");");
				} else {
					removeCode(idx);
				}
			}
			
			function removeCode(idx) {
				var removeTarget = $(".codeTr[value="+idx+"]");
				var code = $(removeTarget).find("input[name='codeCode']").val();
				var pfCnt = $(removeTarget).find("input[name='pfCnt']").val();
				
				if (typeof code != "undefined" && code != "") {
					var removeCodeArr = $("input[id='removeCodeArr']").val();
					removeCodeArr = removeCodeArr + code + ",";
					$("input[id='removeCodeArr']").val(removeCodeArr);
				}
				$(removeTarget).remove();
				managementSetting();
				removeCodePopupClose();
			}
			
			
			function removeCodePopupClose() {
				$("#removeCodePopup").hide();
				$("#removeCodeBtn").attr("onclick","");
			}
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
			<div class="contents main">
				<div class="left_mcon aside_l user">   
					<div class="sub_menu user">
						<div class="title">이용자manajemen</div>
						<div class="m_schwrap">
							<div class="wrap_s1_uselectbox">
							 	<div class="uselectbox" id="userTypeSelectBox" value="PROFESSOR">
									<span class="uselected">교수</span>
									<span class="uarrow">▼</span>
									<div class="uoptions">
										<span class="uoption" value="PROFESSOR">교수</span>
										<span class="uoption" value="STUDENT">학생</span>
										<span class="uoption" value="STAFF">직원</span>
									</div>
								</div>
							</div>
							<input type="text" class="ip_nm" id="searchUserName" value="" placeholder="이름">
							<button class="btn_sch" onclick="searchUser();"></button>
						</div>
			
						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt two on">이용자 registrasi정보 manajemen<br><span class="stt">( 교수 / 직원 )</span></span>
							</div>
							<ul class="m1 panel on">
								<li class="wrap on"><a href="${HOME}/admin/user/code/attend/list" class="tt_2">재직상태 목록manajemen</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/department/list" class="tt_2">departement 목록manajemen</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/position/list" class="tt_2">교수 posisi 목록manajemen</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/specialty/list" class="tt_2 on">교수 jurusan khusus 목록manajemen</a></li>
							</ul>
							<div class="title1"><span class="sp_tt">교수 manajemen</span></div>
							<ul class="panel" style="display: table !important;">
		                        <li class="wrap"><a href="${HOME}/admin/user/pf/list" class="tt_2">registrasi / Pencarian</a></li>
		                    </ul>
							<div class="title1"><span class="sp_tt">학생 manajemen</span></div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/user/st/list" class="tt_2">registrasi / Pencarian</a></li>
<!-- 								<li class="wrap"><a href="../adm/adm_user_system.html" class="tt_2">학생 sistem akademik별 조회</a></li> -->
<!-- 								<li class="wrap"><a href="#" class="tt_2">성취도 manajemen</a></li> -->
							</ul>
							<div class="title1 wrapx"><span class="sp_tt" onclick="location.href='${HOME}/admin/user/staff/list'">직원 manajemen</span></div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="main_con user">
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<div class="tt_wrap">
						<h3 class="am_tt">
							<span class="tt">이용자manajemen<span class="sign1">&gt;</span>이용자 registrasi정보 manajemen<span class="sign1">&gt;</span>교수 jurusan khusus 목록manajemen perbaiki</span>
						</h3>
					</div>
					<form id="codeFrm" name="codeFrm" onsubmit="return false;" method="post">
						<input type="hidden" id="removeCodeArr" name="removeCodeArr">
					</form>
					<div class="adm_content3 user catalog">
						<div class="wrap_wrap">
							<div class="tt_swrap">
								<span>* 순서조정 : 조정된 순서대로 리스트 출력됩니다. </span> 
							</div>
							<table class="mlms_tb s1">
								<thead>
									<tr>
										<th class="th01 b_2 w1">No.</th>
										<th class="th01 b_2 w2">jurusan khusus</th>
										<th class="th01 b_2 w3">manajemen<span class="sign">/</span>순서 조정</th>
									</tr>
								</thead>
								<tbody id="codeList"></tbody>
<!-- 									<tr> -->
<!-- 										<td class="td_1">1</td> -->
<!-- 										<td class="td_1"> -->
<!-- 											<div class="mdf_wrap_a"> -->
<!-- 												<input type="text" class="ip01a" value="jurusan khusus1"> -->
<!-- 											</div> -->
<!-- 										</td> -->
<!-- 										<td class="td_1"> -->
<!-- 											<button class="mdf_wrap_b open_s1"> <span class="btn_x">X</span> <span class="tts">삭제</span> </button> -->
<!-- 											<div class="mdf_wrap_c"> -->
<!-- 												<div class="up" title="항목 위로">▲</div> -->
<!-- 												<div class="down" title="항목 아래로">▼</div> -->
<!-- 											</div> -->
<!-- 										</td> -->
<!-- 									</tr> -->
<!-- 									<tr> -->
<!-- 										<td class="td_1 plus" colspan="3"> -->
<!-- 										<div class="mdf_wrap_d"> -->
<!-- 										<button class="btn_add" title="tambahkan"></button> -->
<!-- 										<span class="tts">posisi tambahkan하기</span> -->
<!-- 										</div> -->
<!-- 										</td> -->
<!-- 									</tr> -->
								</tbody>
							</table>
						</div> 
						<div class="bt_wrap">
							<button class="bt_2" onclick="codeSubmit();">simpan</button>
							<button class="bt_3" onclick="history.back(-1);">batal</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="pop_s1 mo_s1" id="removeCodePopup">
			<div class="wrap">
			<span class="tt">jurusan khusus 삭제</span>
				<span class="close close_s1" title="닫기" onclick="removeCodePopupClose();">X</span>
			    <div class="spop_wrap">
			    	<div class="sswrap1"></div>
					<div class="sswrap2">해당 jurusan khusus으로 registrasi된<br><span class="tx">교수</span>가 있습니다.<br><br><span class="tx code_name">[ 재직상태x ]</span>삭제 시,<br>없음으로 automatic설정됩니다.</div>
				</div>
				<div class="btn_wrap">
			    	<button class="btn1" id="removeCodeBtn"><span class="tx code_name">[ 재직상태x ]</span>삭제</button>
				    <button class="btn2" onclick="removeCodePopupClose();">batal</button>
				</div>
			</div>
		</div>
	</body>
</html>