<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function(){
				if ("${user_name}" != "") {
					$("#searchTypeSelectBox").find(".uoptions>span[value='N']").click();
				}
				staffListView(1);
				$(".btn_f2").hide();
		        $(".btn_f1").click(function(){
		            $(".btn_f1").hide();
		    		$(".btn_f2").show();
		            $(".rwrap").hide();
		            $(".btn_tt1").hide();
		        });
		        $(".btn_f2").click(function(){
		            $(".btn_f2").hide();
		    		$(".btn_f1").show();
		            $(".rwrap").show();
		            $(".btn_tt1").show();
		        });
			});
			
			
			function staffListView(page) {
				if (typeof page == "undefined") {
					page = 1;
				}
				$("input[name='page']").val(page);
				
				var searchType = $("#searchTypeSelectBox").attr("value");
				$("#searchType").val(searchType);
				
				var accountUseStateRadio = $("#accountUseStateRadio:checked").val();
				$("#searchAccountUseState").val(accountUseStateRadio);
				
				
				var useFlag				
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/admin/user/staff/list",
			        data: $("#searchFrm").serialize(),
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var list = data.list;
				        	var listHtml = '';
				        	var totalCnt = data.totalCnt;
				        	$(list).each(function(){
				        		var rowNum = this.row_num;
				        		var userSeq = this.user_seq;
				        		var departmentName = this.department_name;
				        		var professorId = this.professor_id;
				        		var name = this.name;
				        		var accountUseState = this.account_use_state;
				        		
				        		// ts1 사용중, ts2 사용안함
				        		var useClass = 'ts1';
				        		var useText = '사용중';
				        		if (accountUseState == 'N') {
				        			useClass = 'ts2';
				        			useText = '사용안함';
				        		}
				        		listHtml += '<tr class="user-hover" onclick="getDetailView(' + userSeq + ');">';
				        		listHtml += '	<td class="td_1">' + rowNum + '</td>';
				        		listHtml += '	<td class="td_1">' + departmentName + '</td>';
				        		listHtml += '	<td class="td_1">' + professorId + '</td>';
				        		listHtml += '	<td class="td_1">' + name + '</td>';
				        		listHtml += '	<td class="td_1"><span class="' + useClass + '">' + useText + '</span></td>';
				        		listHtml += '</tr>';
				        	});
				        	
				        	if (list.length > 0) {
				        		$("#staffList").html(listHtml);
					        	$("#pageNationArea").html(data.pageNav);
				        	} else {
				        		$("#staffList").html('<tr><td colspan="9">데이터가 없습니다.</td></tr>');
				        		$("#pageNationArea").html("");
				        	}
			        		$("#searchTotalCnt").html(totalCnt);
			        	} else {
			        		//TODO
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function getDetailView(userSeq) {
				post_to_url('${HOME}/admin/user/staff/detail', {"userSeq":userSeq});
			}
			
			function orderChange(type, t) {
				var currOrderText = $(t).text();
				if (currOrderText == "▼") {
					$(".order_btn").html("▼");
					$(t).html("▲");
					$("#searchOrderType").val(type);
					$("#searchOrder").val("ASC");
				} else {
					$(t).html("▼");
					$("#searchOrderType").val(type);
					$("#searchOrder").val("DESC");
				}
				staffListView(1);
			}
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
			<div class="contents main">
				<div class="left_mcon aside_l user">
		            <div class="sub_menu user">
		                <div class="title">이용자manajemen</div>
						<div class="m_schwrap">
							<div class="wrap_s1_uselectbox">
							 	<div class="uselectbox" id="userTypeSelectBox" value="PROFESSOR">
									<span class="uselected">교수</span>
									<span class="uarrow">▼</span>
									<div class="uoptions">
										<span class="uoption" value="PROFESSOR">교수</span>
										<span class="uoption" value="STUDENT">학생</span>
										<span class="uoption" value="STAFF">직원</span>
									</div>
								</div>
							</div>
							<input type="text" class="ip_nm" id="searchUserName" value="" placeholder="이름">
							<button class="btn_sch" onclick="searchUser();"></button>
						</div>
						
		                <div class="boardwrap">
							<div class="title1">
								<span class="sp_tt two">이용자 registrasi정보 manajemen<br><span class="stt">( 교수 / 직원 )</span></span>
							</div>
							<ul class="m1 panel">
								<li class="wrap"><a href="${HOME}/admin/user/code/attend/list" class="tt_2">재직상태 목록manajemen</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/department/list" class="tt_2">departement 목록manajemen</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/position/list" class="tt_2">교수 posisi 목록manajemen</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/specialty/list" class="tt_2">교수 jurusan khusus 목록manajemen</a></li>
							</ul>
							<div class="title1"><span class="sp_tt">교수 manajemen</span></div>
							<ul class="panel" style="display: table !important;">
		                        <li class="wrap"><a href="${HOME}/admin/user/pf/list" class="tt_2">registrasi / Pencarian</a></li>
		                    </ul>
							<div class="title1"><span class="sp_tt">학생 manajemen</span></div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/user/st/list" class="tt_2">registrasi / Pencarian</a></li>
<!-- 								<li class="wrap"><a href="../adm/adm_user_system.html" class="tt_2">학생 sistem akademik별 조회</a></li> -->
<!-- 								<li class="wrap"><a href="#" class="tt_2">성취도 manajemen</a></li> -->
							</ul>
							<div class="title1 wrapx"><span class="sp_tt on" onclick="location.href='${HOME}/admin/user/staff/list'">직원 manajemen</span></div>
							<ul class="panel on">
								<li class="wrap xx"></li>
							</ul>
						</div>
		            </div>
		        </div>
		
				<div class="main_con user">
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
		
					<div class="tt_wrap"><h3 class="am_tt"><span class="tt">이용자manajemen<span class="sign1">&gt;</span>직원 manajemen</span></h3></div>
		
					<div class="adm_content3 user">
						<div class="wrap_wrap">
							<div class="sch_wrap staff">
								<div class="rwrap">
									<div class="wrap_s a1st">
										<div class="t1">
											<span class="tts">▶ 직원Pencarian</span>
										</div>
										<form id="searchFrm" name="searchFrm" method="get" onsubmit="return false;">
											<input type="hidden" id="searchAccountUseState" name="searchAccountUseState">
											<input type="hidden" id="searchType" name="searchType">
											<input type="hidden" id="searchOrderType" name="searchOrderType">
											<input type="hidden" id="searchOrder" name="searchOrder">
											<input type="hidden" id="page" name="page">
											<div class="wrap_ss"> 
												<div class="ss1">
													<div class="l_wrap">
														<div class="lbwrap">
															<input type="radio" class="rd01" id="accountUseStateRadio" name="accountUseStateRadio" value="" checked="checked">
															<label>Keseluruhan</label>
														</div>  
														<div class="lbwrap">
															<input type="radio" class="rd01" id="accountUseStateRadio" name="accountUseStateRadio" value="Y">
															<label>사용중</label>
														</div>
														<div class="lbwrap">
															<input type="radio" class="rd01" id="accountUseStateRadio" name="accountUseStateRadio" value="N">
															<label>사용안함</label>
														</div>
													</div>	
													<div class="r_wrap">
														<div class="wrap_s1_uselectbox">
															<div class="uselectbox" id="searchTypeSelectBox" value="">
																<span class="uselected">Keseluruhan</span>
																<span class="uarrow">▼</span>
																<div class="uoptions">
																	<span class="uoption" value="">Keseluruhan</span>
																	<span class="uoption" value="D">departement</span>
																	<span class="uoption" value="P">사번</span>
																	<span class="uoption" value="N">이름</span>
																</div>
															</div>
														</div>
														<input type="text" class="ip_tt_1" id="searchText" name="searchText" placeholder="여기에 입력하세요." value="${user_name}">  
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
							<div class="btnwrap_s1">
								<div class="tt_g1"><span class="g_t2">Pencarian결과</span><span class="g_num" id="searchTotalCnt">0</span><span class="g_t3">명</span></div>
								<button class="btn_tt1" onclick="staffListView(1);">Pencarian</button>
							</div>
						</div> 
						<div class="wrap_wrap">
							<div class="btnwrap_s2 staff">
								<a class="go_r1" href="${HOME}/admin/user/staff/create">직원 registrasi</a>
							</div>
		
							<table class="mlms_tb staff">
								<thead>
									<tr class="staff_table_thead">
										<th class="th01 bd01 w1">No.</th>
										<th class="th01 bd01 w3">
											<div class="th_wrap pd1">departement</div>
											<button class="down order_btn" onclick="orderChange('D', this);">▼</button>
										</th>
										<th class="th01 bd01 w3">
											<div class="th_wrap pd1">사번</div>
											<button class="down order_btn" onclick="orderChange('P', this);">▼</button>
										</th>
										<th class="th01 bd01 w5">
											<div class="th_wrap pd1">이름</div>
											<button class="down order_btn" onclick="orderChange('N', this);">▼</button>
										</th>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd1">상태</div>
											<button class="down order_btn" onclick="orderChange('A', this);">▼</button>
										</th>
									</tr>
								</thead>
								<tbody id="staffList"></tbody>
							</table>	
						</div> 
						<div class="pagination" id='pageNationArea'></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>