<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function(){
				getAcademicSystem('', '', '', 1);
				pfListView(1);
				$(".btn_f2").hide();
		        $(".btn_f1").click(function(){
		            $(".btn_f1").hide();
		    		$(".btn_f2").show();
		            $(".rwrap").hide();
		            $(".btn_tt1").hide();
		        });
		        $(".btn_f2").click(function(){
		            $(".btn_f2").hide();
		    		$(".btn_f1").show();
		            $(".rwrap").show();
		            $(".btn_tt1").show();
		        });
			});
			
		
			
			function getAcademicSystem(lSeq, mSeq, sSeq, level) {
				var searchFlag = true;
				if (level == 1) {
        			acaSelectInit('#mAcaSelectBox', 2);
        			acaSelectInit('#sAcaSelectBox', 3);
        			acaSelectInit('#pAcaSelectBox', 4);
        		} else if (level == 2) {
        			acaSelectInit('#mAcaSelectBox', 2);
        			acaSelectInit('#sAcaSelectBox', 3);
        			acaSelectInit('#pAcaSelectBox', 4);
        		} else if (level == 3) {
       				acaSelectInit('#sAcaSelectBox', 3);
       				acaSelectInit('#pAcaSelectBox', 4);
        		} else if (level == 4) {
        			acaSelectInit('#pAcaSelectBox', 4);
        		}
				
				
				if (searchFlag) {
					$.ajax({
				        type: "GET",
				        url: "${HOME}/ajax/admin/user/academic/list",
				        data: {
				        	"l_seq": lSeq
				        	, "m_seq": mSeq
				        	, "s_seq": sSeq
				        },
				        dataType: "json",
				        success: function(data, status) {
				        	if (data.status == "200") {
				        		yearSelectBoxSetting(data.year_list);
				        		if (level == 1) {
				        			acaSelectBoxSetting('#lAcaSelectBox', data.l_aca_list, 1);
				        		} else if (level == 2) {
				        			acaSelectBoxSetting('#mAcaSelectBox', data.m_aca_list, 2);
				        		} else if (level == 3) {
				        			acaSelectBoxSetting('#sAcaSelectBox', data.s_aca_list, 3);
				        		} else if (level == 4) {
				        			acaSelectBoxSetting('#pAcaSelectBox', data.p_aca_list, 4);
				        		}
				        		
				        	} else {
				        		alert("sistem akademik 불러오기에 실패하였습니다. [" + data.status + "]");
				        	}
				        },
				        error: function(xhr, textStatus) {
				            document.write(xhr.responseText);
				        },
				        beforeSend:function() {
				            $.blockUI();
				        },
				        complete:function() {
				            $.unblockUI();
				        }
				    });
				}
			}
			
			
			
			function yearSelectBoxSetting(list) {
				var selectYear = $("#yearAcaSelectBox").attr("value");
				if (selectYear == "") {
					var listHtml = '<span class="uoption" value="">tahun</span>';
					$(list).each(function() {
						listHtml += '<span class="uoption" value="' + this.year + '">' + this.year + '</span>';
					});
					$("#yearAcaSelectBox").find("div.uoptions").html(listHtml);
				}
			}
			
			function acaSelectInit(t, level) {
				var firstSpanName = getLevelName(level);
				$(t).find("span.uselected").html(firstSpanName);
				$(t).attr("value", "");
				$(t).find("div.uoptions").html('<span class="uoption" value="" onclick="getAcademicSystem(\'\', \'\', '+(level+1)+');">' + firstSpanName + '</span>');
			}
			
			function getLevelName(level) {
				var result = "";
				if (level == 1) {
					result = "fakultas";
				} else if (level == 2) {
					result = "jurusan";
				} else if (level == 3) {
					result = "tahun ajaran";
				} else if (level == 4) {
					result = "periode";
				}
				return result;
			}
			
			function acaSelectBoxSetting(t, list, level) {
				var target = $(t).find("div.uoptions");
				var firstSpanName = getLevelName(level);
				var listHtml = '<span class="uoption" value="" onclick="getAcademicSystem(\'\', \'\', \'\', '+(level+1)+');">' + firstSpanName + '</span>';
				$(list).each(function() {
					if (level == 1) {
						listHtml += '<span class="uoption" value="' + this.aca_system_seq + '" onclick="getAcademicSystem(' + this.aca_system_seq + ', \'\', \'\', ' + (level+1) + ');">' + this.aca_system_name + '</span>';
					} else if (level == 2) {
						listHtml += '<span class="uoption" value="' + this.aca_system_seq + '" onclick="getAcademicSystem(' + this.l_seq + ', '+this.aca_system_seq+', \'\', ' + (level+1) + ');">' + this.aca_system_name + '</span>';
					} else if (level == 3) {
						listHtml += '<span class="uoption" value="' + this.aca_system_seq + '" onclick="getAcademicSystem(' + this.l_seq + ', '+this.m_seq+', '+this.aca_system_seq+', ' + (level+1) + ');">' + this.aca_system_name + '</span>';
					} else {
						listHtml += '<span class="uoption" value="' + this.aca_system_seq + '">' + this.aca_system_name + '</span>';
					}
				});
				$(target).html(listHtml);
			}
			
			
			
			function pfListView(page) {
				if (typeof page == "undefined") {
					page = 1;
				}
				$("input[name='page']").val(page);
				
				var searchYear = $("#yearAcaSelectBox").attr("value");
				$("#searchYear").val(searchYear);
				
				var searchLSeq = $("#lAcaSelectBox").attr("value");
				$("#searchLSeq").val(searchLSeq);
				
				var searchMSeq = $("#mAcaSelectBox").attr("value");
				$("#searchMSeq").val(searchMSeq);
				
				var searchSSeq = $("#sAcaSelectBox").attr("value");
				$("#searchSSeq").val(searchSSeq);
				
				var searchPSeq = $("#pAcaSelectBox").attr("value");
				$("#searchPSeq").val(searchPSeq);
				
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/admin/user/pf/list",
			        data: $("#searchFrm").serialize(),
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var list = data.list;
				        	var listHtml = '';
				        	var totalCnt = data.totalCnt;
				        	$(list).each(function(){
				        		
				        		var userSeq = this.user_seq;
				        		var rowNum = this.row_num;
				        		var userName = this.name;
				        		var professorId = this.professor_id;
				        		var departmentName = this.department_name;
				        		var positionName = this.position_name;
				        		var specialtyName = this.specialty_name;
				        		var tel = this.tel;
				        		var currName = this.curr_name;
				        		var currYear = this.curr_year;
				        		var currSeq = this.curr_seq;
				        		var accountUseState = this.account_use_state;
				        		var accountUseStateText = "사용함";
				        		var accountUseStateClass = "tt_o";
				        		var currPageMoveScript = "";
				        		if (currName != "") {
				        			currName = '(' + currYear + ') ' + currName;
				        		}
				        		
				        		listHtml += '<tr class="user-hover" onClick="getDetailView(' + userSeq + ');">';
				        		listHtml += '    <td class="td_1">' + rowNum + '</td>';
				        		listHtml += '    <td class="td_1">' + userName + '</td>';
				        		listHtml += '    <td class="td_1">' + professorId + '</td>';
				        		listHtml += '    <td class="td_1">' + positionName + '</td>';
				        		listHtml += '    <td class="td_1">' + departmentName + '</td>';
				        		listHtml += '    <td class="td_1">' + specialtyName + '</td>';
				        		listHtml += '    <td class="td_1">' + tel + '</td>';
				        		listHtml += '    <td class="td_1">';
				        		if (accountUseState == "N") { //퇴임
				        			accountUseStateClass = "tt_x";
				        			accountUseStateText = "사용<br>안함";
				        		}
			        			listHtml += '    	<span class="' + accountUseStateClass + '">' + accountUseStateText + '</span>';
				        		listHtml += '    </td>';
				        		listHtml += '    <td class="td_1 lt">';
				        		if (currName == "") {
					        		currName = "없음";
				        		}
				        		listHtml += '    	<a class="tta2" href="javascript:void(0);">' + currName + '</a>';
				        		listHtml += '    </td>';
				        		listHtml += '</tr>';
				        	});
				        	
				        	if (list.length > 0) {
				        		$("#pfList").html(listHtml);
					        	$("#pageNationArea").html(data.pageNav);
				        	} else {
				        		$("#pfList").html('<tr><td colspan="9">데이터가 없습니다.</td></tr>');
				        		$("#pageNationArea").html("");
				        	}
			        		$("#searchTotalCnt").html(totalCnt);
			        	} else {
			        		//TODO
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function pageMoveCurriculumView(currSeq, currYear){
		 		post_to_url("${HOME}/admin/lesson/curriculum", {"curr_seq":currSeq, "year" : currYear});		
			}
			
			function getDetailView(userSeq) {
				post_to_url('${HOME}/admin/user/pf/detail', {"userSeq":userSeq});
			}
			
			function excelPopupOpen() {
				$("#excelPopup").show();
			}
			
			function excelPopupClose() {
				$("#excelPopup").hide();
			}
			
			function fileSelect() {
				var fileInput = $("#uploadFile");
				$(fileInput).change(function(){
					var file = $(this).val();
					if (file != "") {
						var attachFileName = file.substring(file.lastIndexOf('\\') + 1); //파일명
						var ext = attachFileName.substring(attachFileName.lastIndexOf('.') + 1).toLowerCase(); //파일 확장자
						var fileMaxSize = 10; //10MB
						var fileSize = this.files[0].size/1024/1024;
						if (fileSize > fileMaxSize) {
							alert("10MB이하의 파일만 업로드 가능합니다.");
							fileValueInit(fileInput);
							return false;
						}
						
						//파일 확장자 유효성검사
						if (/(xlsx)$/i.test(ext) == false) {
							alert("파일의 확장자를 확인해주세요.(xlxs)");
							fileValueInit(fileInput);
							return false;
						}
						$("#excelFileName").val(attachFileName);
					} else {
						fileValueInit(fileInput);
						$("#excelFileName").val("");
					}
				});
				$(fileInput).click();
			}
			
			function excelUpload() {
				$("#excelFrm").attr("action", "${HOME}/ajax/admin/user/pf/excel/create").ajaxForm({
					beforeSend: function () {
						$.blockUI();
					},
					type: "POST",
					dataType:"json",
					success:function(data){
						if (data.status == "200") {
							alert(data.success_cnt+"개의 사용자 registrasi이 완료되었습니다.");
							location.reload();
						} else if (data.status == "301") {//사번 중복검사
							alert(data.msg+"번째 행의 사번이 중복됩니다.");
						} else if (data.status == "302") {//이메일 중복검사
							alert(data.msg+"번째 행의 이메일이 중복됩니다.");
						} else if (data.status == "303") {//아이디 중복검사
							alert(data.msg+"번째 행의 아이디가 중복됩니다.");
						} else {
							alert("registrasi에 실패 하였습니다.");
							location.reload();
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						document.write(xhr.responseText);
				    },
			        complete:function() {
			            $.unblockUI();
			        }
				}).submit();
			}
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
		    <div class="contents main">
		        <div class="left_mcon aside_l user">
		            <div class="sub_menu user">
		                <div class="title">이용자manajemen</div>
						<div class="m_schwrap">
							<div class="wrap_s1_uselectbox">
							 	<div class="uselectbox" id="userTypeSelectBox" value="PROFESSOR">
									<span class="uselected">교수</span>
									<span class="uarrow">▼</span>
									<div class="uoptions">
										<span class="uoption" value="PROFESSOR">교수</span>
										<span class="uoption" value="STUDENT">학생</span>
										<span class="uoption" value="STAFF">직원</span>
									</div>
								</div>
							</div>
							<input type="text" class="ip_nm" id="searchUserName" value="" placeholder="이름">
							<button class="btn_sch" onclick="searchUser();"></button>
						</div>
						
		                <div class="boardwrap">
							<div class="title1">
								<span class="sp_tt two">이용자 registrasi정보 manajemen<br><span class="stt">( 교수 / 직원 )</span></span>
							</div>
							<ul class="m1 panel">
								<li class="wrap"><a href="${HOME}/admin/user/code/attend/list" class="tt_2">재직상태 목록manajemen</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/department/list" class="tt_2">departement 목록manajemen</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/position/list" class="tt_2">교수 posisi 목록manajemen</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/specialty/list" class="tt_2">교수 jurusan khusus 목록manajemen</a></li>
							</ul>
							<div class="title1 wrapx"><span class="sp_tt">교수 manajemen</span></div>
							<ul class="panel on" style="display: table !important;">
		                        <li class="wrap"><a href="${HOME}/admin/user/pf/list" class="tt_2 on">registrasi / Pencarian</a></li>
		                    </ul>
							<div class="title1"><span class="sp_tt">학생 manajemen</span></div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/user/st/list" class="tt_2">registrasi / Pencarian</a></li>
<!-- 								<li class="wrap"><a href="../adm/adm_user_system.html" class="tt_2">학생 sistem akademik별 조회</a></li> -->
<!-- 								<li class="wrap"><a href="#" class="tt_2">성취도 manajemen</a></li> -->
							</ul>
							<div class="title1 wrapx"><span class="sp_tt" onclick="location.href='${HOME}/admin/user/staff/list'">직원 manajemen</span></div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
		            </div>
		        </div>
		
		        <div class="main_con user">
		           
		            <a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
		            <a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
		            
		            <div class="tt_wrap">
		                <h3 class="am_tt"><span class="tt">이용자manajemen<span class="sign1">&gt;</span>교수 manajemen</span></h3>                    
		            </div>
		            
		            <div class="adm_content3 user">
		                <div class="wrap_wrap">
		                    <div class="sch_wrap">
			                    <div class="rwrap">
				                    
				                    <form id="searchFrm" name="searchFrm" method="get" onsubmit="return false;">
				                    	<input type="hidden" id="searchYear" name="searchYear">
				                    	<input type="hidden" id="searchLSeq" name="searchLSeq">
				                    	<input type="hidden" id="searchMSeq" name="searchMSeq">
				                    	<input type="hidden" id="searchSSeq" name="searchSSeq">
				                    	<input type="hidden" id="searchPSeq" name="searchPSeq">
				                    	<input type="hidden" id="page" name="page">
					                    <div class="wrap_s a1">
						                    <div class="t1">▶ sistem akademik </div>
						                    <div class="wrap_ss">
							                    <div class="ss1">
								                    <div class="wrap_s1_uselectbox">
									                    <div class="uselectbox" id="yearAcaSelectBox" value="">
										                    <span class="uselected">tahun</span>
										                    <span class="uarrow">▼</span>           
										                    <div class="uoptions">
										                    	<span class="uoption" value="">tahun</span>
										                    </div>
									                    </div>                                  
								                    </div>
								
								                    <div class="wrap_s2_uselectbox">
									                    <div class="uselectbox" id="lAcaSelectBox" value="">
										                    <span class="uselected">fakultas</span>
										                    <span class="uarrow">▼</span>           
										                    <div class="uoptions">
										                    	<span class="uoption" value="">fakultas</span>
										                    </div>
									                    </div>                                  
								                    </div>
								
								                    <div class="wrap_s2_uselectbox">
									                    <div class="uselectbox" id="mAcaSelectBox" value="">
										                    <span class="uselected">jurusan</span>
										                    <span class="uarrow">▼</span>           
										                    <div class="uoptions">
										                    	<span class="uoption" value="">jurusan</span>
										                    </div>
									                    </div>                                  
								                    </div>
								
								                    <div class="wrap_s2_uselectbox">
									                    <div class="uselectbox" id="sAcaSelectBox" value="">
										                    <span class="uselected">tahun ajaran</span>
										                    <span class="uarrow">▼</span>           
										                    <div class="uoptions">
										                    	<span class="uoption" value="">tahun ajaran</span>
										                    </div>
									                    </div>                                  
								                    </div>
								
								                    <div class="wrap_s1_uselectbox">
									                    <div class="uselectbox" id="pAcaSelectBox" value="">
										                    <span class="uselected">periode</span>
										                    <span class="uarrow">▼</span>           
										                    <div class="uoptions">
										                    	<span class="uoption" value="">periode</span>
										                    </div>
									                    </div>
								                    </div>
							                    </div>
							                    <div class="ss1">
								                    <span class="tt2">Nama Mata Kuliah</span>
								                    <input class="ip_tt" type="text" id="searchCurrName" name="searchCurrName">
							                    </div>
						                    </div>
					                    </div>
									
					                    <div class="wrap_s a2">                
						                    <div class="t1">▶ 교perbaiki보 </div>
						                    <div class="wrap_ss"> 
							                    <div class="ss1">
								                    <span class="tt2">departement</span>     
								                    <input class="ip_tt_1" type="text" id="searchDepartmentName" name="searchDepartmentName">
								
								                    <span class="tt2">사번</span>     
								                    <input class="ip_tt_1" type="text" id="searcPropessorId" name="searcPropessorId" value="${propessor_id}">
								                    
								                    <span class="tt2">이름</span>     
								                    <input class="ip_tt_1" type="text" id="searchUserName" name="searchUserName" value="${user_name}">
								                    
							                    </div>
						                    </div>
					                    </div>
				                    
				                    </form>
				                    
				                    <button class="btn_f1">접기</button>              
			                    </div>
			                    <button class="btn_f2">Pencarian 펼치기</button>
		                    </div>
		                    <div class="btnwrap_s1">
			                    <div class="tt_g1">
			                    	<span class="g_t2">Pencarian결과</span><span class="g_num" id="searchTotalCnt">0</span><span class="g_t3">명</span>
			                    </div>
			                    <button class="btn_tt1" onclick="pfListView(1);">Pencarian</button>
		                    </div>
		
		                </div> 
		
		                <div class="wrap_wrap">     
		                    <div class="btnwrap_s2"> 
		                        <div class="btnwrap_s2_s">  
		                            <a class="go_r1" href="${HOME}/admin/user/pf/create">교수 registrasi</a>
		                            <button class="btn_up1 open1" onclick="excelPopupOpen();">엑셀 일괄 registrasi</button>
		                        </div>
		                    </div>   
		
		                    <table class="mlms_tb user">
		                        <thead>
		                            <tr>
		                            <th class="th01 bd01 w1">No.</th>
		                            <th class="th01 bd01 w2">nama dosen pengajar</th>
		                            <th class="th01 bd01 w3">사번</th>  
		                            <th class="th01 bd01 w4">posisi</th>
		                            <th class="th01 bd01 w5">departement</th> 
		                            <th class="th01 bd01 w6">jurusan khusus</th>
		                            <th class="th01 bd01 w7">kontak</th>
		                            <th class="th01 bd01 w8">계정<br>사용<br>여부</th>
		                            <th class="th01 bd01 w9">교육과정 이력</th>
		                            </tr>
		                        </thead>
		                        <tbody id="pfList">  
		                        </tbody>   
		                    </table>    
		                </div> 
		
		                <div class="pagination" id="pageNationArea"></div>
		            </div>
		        </div>
		    </div>
		</div>
		<!-- s_ 엑셀 일괄 registrasi 팝업 -->
		<div id="excelPopup" class="pop_up_ex pop_up_ex_1 mo1">
			<form id="excelFrm" name="excelFrm" onsubmit="return false;" enctype="multipart/form-data">
				<input type="file" id="uploadFile" name="uploadFile" class="blind-position">
				<div class="pop_wrap">
			    	<p class="popup_title">교수 registrasi 엑셀업로드</p>
					<button class="pop_close close1" type="button" onclick="excelPopupClose();">X</button>
					<button class="btn_exls_down_2" onclick="location.href='${HOME}/admin/user/pf/excel/template/down'">엑셀 양식 다운로드</button>
		            <div class="t_title_1">
		            	<span class="sp_wrap_1">교수 registrasi 엑셀양식을 먼저 다운로드 받으신 뒤,<br>다운로드 받은 엑셀파일양식에 교수 정보를 입력하여 파일 registrasi 하시면 일괄 registrasi됩니다.</span>
					</div>
					<div class="pop_ex_wrap">
		            	<div class="sub_fwrap_ex_1">
		                	<span class="ip_tt">file excel</span>
		                    <input type="text" class="ip_sort1_1" id="excelFileName" value="" readonly="readonly">
		                    <button class="btn_r1_1" onclick="fileSelect();">파일pilih</button>
						</div>
		                <div class="t_dd">
							<div class="pop_btn_wrap">
								<button type="button" class="btn01" onclick="excelUpload();">registrasi</button>
		                     	<button type="button" class="btn02" onclick="excelPopupClose();">batal</button>
							</div>
		                </div>
		           </div>
				</div>
			</form>
		</div>
		<!-- e_ 엑셀 일괄 registrasi 팝업 -->
	</body>
</html>