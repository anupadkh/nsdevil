<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function(){
				getCodeList();
			});
			
			function getCodeList() {
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/admin/user/code/list",
			        data: {},
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var d_list = data.staff_department_code_list;
			        		var a_list = data.attend_code_list;
			        		codeSelectBoxSetting("#departmentSelectBox", d_list);
			        		codeSelectBoxSetting("#attendSelectBox", a_list);
			        		getStaffDetail();
			        	} else {
			        		
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			
			function codeSelectBoxSetting(t, list) {
				var target = $(t).find("div.uoptions");
				var listHtml = '<span class="uoption" value="">pilih</span>';
				$(list).each(function() {
					listHtml += '<span class="uoption" value="' + this.code + '">' + this.code_name + '</span>';
				});
				$(target).html(listHtml);
			}
			
			
			function previewPictureUpload() {
				var target = $("#uploadFile");
				target.change(function(){
					var file = $(this).val();
					var previewPicture = $("#previewPicture");
					if (file != "") {
						var attachFileName = file.substring(file.lastIndexOf('\\') + 1);
						var ext = attachFileName.substring(attachFileName.lastIndexOf('.') + 1).toLowerCase();
						var fileMaxSize = 10; //10MB
						var fileSize = this.files[0].size/1024/1024;
						if (/(jpg|png|jpeg)$/i.test(ext) && (fileSize < fileMaxSize)) {
							previewPicture.attr("src", loadPreview(this, "previewPicture"));
						} else {
							alert("10MB이하 이미지 파일(jpg, png, jpeg)만 첨부 가능합니다.");
							previewPicture.attr("src", "${IMG}/profile_default.png");
							if (/msie/.test(navigator.userAgent.toLowerCase())) { // ie
								$("#uploadFile").replaceWith($("#uploadFile").clone(true)); 
							} else { // other browser 
								$("#uploadFile").val(""); 
							}
						}
					} else {
						previewPicture.attr("src", "${IMG}/profile_default.png");
					}
				});
				target.click();
			}
			
			
			function getStaffDetail() {
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/admin/user/staff/detail",
			        data: {
			        	"user_seq":"${user_seq}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		userInfoSetting(data.user_info);
			        	} else {
			        		
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function userInfoSetting(info) {
				var userInfo = info;
				var userName = info.name;
				var attendCode = info.attend_code;
				var accountUseState = info.account_use_state;
				var professorId = info.professor_id;
				var departmentCode = info.staff_department_code;
				var tel = info.tel;
				var userEmail = info.email;
				var userId = info.id;
				var picturePath = info.picture_path;
				var regDate = info.reg_date;
				var dayOfWeek = info.day_of_week;
				if (isEmpty(picturePath)) {
					picturePath = "${DEFAULT_PICTURE_IMG}";
				} else {
					picturePath = "${RES_PATH}"+picturePath;
        		}
				$("#previewPicture").attr("src", picturePath);
				
				var week = new Array('일', '월', '화', '수', '목', '금', '토');
				dayOfWeek = week[dayOfWeek];
				regDate = regDate + " ("+dayOfWeek+")";
				$("#regDate").html(regDate);
				$("#userName").val(userName);
				firstSelectBoxSelected("#attendSelectBox", attendCode);
				$("#professorId").val(professorId);
				$("#accountUseStateRadio[value='"+accountUseState+"']").prop("checked", "checked")
				firstSelectBoxSelected("#departmentSelectBox", departmentCode);
				if (tel != "") {
					$("#tel1").val(tel.split("-")[0]);
					$("#tel2").val(tel.split("-")[1]);
					$("#tel3").val(tel.split("-")[2]);
				}
				$("#userEmail").val(userEmail);
				$("#userId").val(userId);
			}
			
			function firstSelectBoxSelected(t, value) {
				if (typeof value == "undefined") {
					$(t).find("div.uoptions").find("span:eq(0)").click();
				} else {
					$(t).find("div.uoptions").find("span[value='" + value + "']").click();
				}
			}
			
			function userSubmit() {
				var name = $("#userName").val();
				if (isBlank(name) || isEmpty(name)) {
					alert("이름을 입력해주세요.");
					$("#userName").focus();
					return false;
				}
				
				var tel1 = $("#tel1").val().trim();
				var tel2 = $("#tel2").val().trim();
				var tel3 = $("#tel3").val().trim();
				if (tel1 != "" || tel2 != "" || tel3 != "") {
					var tel = tel1 + "-" + tel2 + "-" + tel3;
					if (!isTelType(tel)) {
						alert("kontak의 형식이 올바르지 않습니다.");
						$("#tel1").focus();
						return false;
					}
					$("#tel").val(tel);
				}
				
				
				var professorId = $("#professorId").val().trim();
				$("#professorId").val(professorId);
				
				var email = $("#userEmail").val().trim();
				$("#userEmail").val(email);
				if (isBlank(email) || isEmpty(email)) {
					alert("E-mail을 입력해주세요.");
					$("#userEmail").focus();
					return false;
				}
				
				if (!isEmailType(email)) {
					alert("E-mail의 형식이 올바르지 않습니다.");
					$("#userEmail").focus();
					return false;
				}
				
				var userId = $("#userId").val().trim();
				$("#userId").val(userId);
				
				var departmentValue = $("#departmentSelectBox").attr("value");
				$("#departmentCode").val(departmentValue);
				
				var attendValue = $("#attendSelectBox").attr("value");
				$("#attendCode").val(attendValue);
				
				var accountUseState = $("#accountUseStateRadio:checked").val();
				$("#accountUseState").val(accountUseState);
				
				if (attendValue == "2") {//재직상태가 퇴임이면 계정 사용안함
					$("#accountUseState").val("N");
				}
				
				$("#userSeq").val("${user_seq}");
				
				$("#userFrm").attr("action","${HOME}/ajax/admin/user/staff/modify").ajaxForm({
					beforeSend: function () {
						$.blockUI();
					},
					type: "POST",
					dataType:"json",
					success:function(data){
						if (data.status == "200") {
							alert("simpan 되었습니다.");
							location.href="${HOME}/admin/user/staff/list";
						} else if (data.status == "301") {//사번 중복검사
							alert("사번이 중복됩니다.");
							$("#professorId").focus();
						} else if (data.status == "302") {//이메일 중복검사
							alert("이메일이 중복됩니다.");
							$("#userEmail").focus();
						} else if (data.status == "303") {//아이디 중복검사
							alert("아이디가 중복됩니다.");
							$("#userId").focus();
						} else {
							alert("simpan에 실패 하였습니다.");
							location.reload();
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						document.write(xhr.responseText);
				    },
			        complete:function() {
			            $.unblockUI();
			        }
				}).submit();
				
			}
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
		    <div class="contents main">
		        <div class="left_mcon aside_l user">
		            <div class="sub_menu user">
		                <div class="title">이용자manajemen</div>
						<div class="m_schwrap">
							<div class="wrap_s1_uselectbox">
							 	<div class="uselectbox" id="userTypeSelectBox" value="PROFESSOR">
									<span class="uselected">교수</span>
									<span class="uarrow">▼</span>
									<div class="uoptions">
										<span class="uoption" value="PROFESSOR">교수</span>
										<span class="uoption" value="STUDENT">학생</span>
										<span class="uoption" value="STAFF">직원</span>
									</div>
								</div>
							</div>
							<input type="text" class="ip_nm" id="searchUserName" value="" placeholder="이름">
							<button class="btn_sch" onclick="searchUser();"></button>
						</div>
						
		                <div class="boardwrap">
							<div class="title1">
								<span class="sp_tt two">이용자 registrasi정보 manajemen<br><span class="stt">( 교수 / 직원 )</span></span>
							</div>
							<ul class="m1 panel">
								<li class="wrap"><a href="${HOME}/admin/user/code/attend/list" class="tt_2">재직상태 목록manajemen</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/department/list" class="tt_2">departement 목록manajemen</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/position/list" class="tt_2">교수 posisi 목록manajemen</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/specialty/list" class="tt_2">교수 jurusan khusus 목록manajemen</a></li>
							</ul>
							<div class="title1"><span class="sp_tt">교수 manajemen</span></div>
							<ul class="panel" style="display: table !important;">
		                        <li class="wrap"><a href="${HOME}/admin/user/pf/list" class="tt_2">registrasi / Pencarian</a></li>
		                    </ul>
							<div class="title1"><span class="sp_tt">학생 manajemen</span></div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/user/st/list" class="tt_2">registrasi / Pencarian</a></li>
<!-- 								<li class="wrap"><a href="../adm/adm_user_system.html" class="tt_2">학생 sistem akademik별 조회</a></li> -->
<!-- 								<li class="wrap"><a href="#" class="tt_2">성취도 manajemen</a></li> -->
							</ul>
							<div class="title1 wrapx"><span class="sp_tt on" onclick="location.href='${HOME}/admin/user/staff/list'">직원 manajemen</span></div>
							<ul class="panel on">
								<li class="wrap xx"></li>
							</ul>
						</div>
		            </div>
		        </div>
		
		        <div class="main_con user">
		        	<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
		            <a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
		
		            <div class="tt_wrap">
		                <h3 class="am_tt"><span class="tt">이용자manajemen<span class="sign1">&gt;</span>직원 manajemen<span class="sign1">&gt;</span>직원 조회( perbaiki )</span></h3>                    
		            </div>
		
		            <div class="adm_content3 user">
		            	<form id="userFrm" name="userFrm" onsubmit="return false;" enctype="multipart/form-data">
		            		<input type="file" id="uploadFile" name="uploadFile" value="" class="blind-position">
		            		<input type="hidden" id="attendCode" name="attendCode" value="">
		            		<input type="hidden" id="departmentCode" name="departmentCode" value="">
		            		<input type="hidden" id="tel" name="tel" value="">
		            		<input type="hidden" id="userSeq" name="userSeq" value="${user_seq}">
		            		<input type="hidden" id="accountUseState" name="accountUseState">
		            		
			                <table class="table_my v1">
			                    <tbody>
			                    	<tr>
							            <td rowspan="8" class="td01 w01 bd04">
							                <div class="ph_wrap">
					                        	<div class="photo1"><img id="previewPicture" src="${IMG}/profile_default.png" alt="프로필 사진"></div>
					                        </div> 
							                <button type="button" class="btn_reset" onclick="previewPictureUpload();">사진perbaiki</button>
							            </td>
										<td class="td00" colspan="2">
											<span class="sp_tt">[ 직원 ]</span>시스템 tanggal registrasi : <span class="sp_tm" id="regDate"></span>
							            </td>			
							        </tr>
			                        <tr>
				                        <td class="td01 w02">이름<span class="tts1">*(필수)</span></td>
				                        <td class="td01 w03">
				                        	<input type="text" class="ip_001v" id="userName" name="userName" placeholder="이름입력(필수)">
				                        	<span class="ip_tt1">재직상태</span>
											<div class="wrap_s1s_uselectbox maxH200px">
						                        <div class="uselectbox" id="attendSelectBox" value="">
					                                <span class="uselected">pilih</span>
					                                <span class="uarrow">▼</span>			
						                            <div class="uoptions">
					                                    <span class="uoption firstseleted">pilih</span>
						                            </div>
						                        </div>
											</div>
				                        </td>
			                        </tr>
			                        <tr>
							            <td class="td01 w02">계정 사용여부</td>
							            <td class="td01 w03">
											<label class="lb_id"><input type="radio" class="ip_id" id="accountUseStateRadio" name="accountUseStateRadio" checked="checked" value="Y">사용중</label><label class="lb_id"><input type="radio" class="ip_id" id="accountUseStateRadio" name="accountUseStateRadio" value="N">사용안함</label>
							            </td>
							        </tr>
			                        <tr>
				                        <td class="td01 w02">사번<span class="tts2">(pilih)</span></td>
				                        <td class="td01 w03">
				                        	<input type="text" class="ip_001" id="professorId" name="professorId" placeholder="사번입력(pilih)">
				                        </td>
			                        </tr>
			                        <tr>
				                        <td class="td01 w02">departement<span class="tts2">(pilih)</span></td>
				                        <td class="td01 w03">
					                        <div class="wrap_s1_uselectbox">
						                        <div class="uselectbox" id="departmentSelectBox" value="">
							                        <span class="uselected">pilih</span>
							                        <span class="uarrow">▼</span>			
							                        <div class="uoptions maxH200px">
							                        	<span class="uoption">pilih</span>
							                        </div>
						                        </div>
					                        </div>
				                        </td>
			                        </tr>
			                        <tr>
				                        <td class="td01 w02">kontak<span class="tts2">(pilih)</span></td>
				                        <td class="td01 td_wrap1 w03">   
					                        <input type="text" class="ip_tel1" id="tel1" maxlength="4" value="">            
					                        <span class="sign1">-</span>
					                        <input type="text" class="ip_tel1" id="tel2" maxlength="4" value="">
					                        <span class="sign1">-</span>
					                        <input type="text" class="ip_tel1" id="tel3" maxlength="4" value="">
				                        </td>
			                        </tr>
			                        <tr>
				                        <td class="td01 w02">E-mail<span class="tts1">*(필수)</span></td>
				                        <td class="td01 w03">
				                        	<input type="text" class="ip_001" id="userEmail" name="userEmail" placeholder="E-mail입력(필수)">
				                        </td>
			                        </tr>
			                        <tr>
				                        <td class="td01 w02">아이디<span class="tts2">(pilih)</span></td>
				                        <td class="td01 w03">
				                        	<input type="text" class="ip_001" id="userId" name="userId" placeholder="registrasi하지 않을 시 E-mail 정보로 automatic registrasi됩니다.">
				                        </td>
			                        </tr>
			                    </tbody>
			                </table>
						</form>
		                <div class="bt_wrap">
			                <button class="bt_2" onclick="userSubmit();">simpan</button>
			                <button class="bt_3" onclick="location.href='${HOME}/admin/user/pf/list'">batal</button>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</body>
</html>