<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function(){
				$(".btn_f2").hide();
			        $(".btn_f1").click(function(){
			        $(".btn_f1").hide();
					$(".btn_f2").show();
			        $(".rwrap").hide();
			    });
		        $(".btn_f2").click(function(){
			        $(".btn_f2").hide();
					$(".btn_f1").show();
			        $(".rwrap").show();
			    });
		        
		        getStList(1,"","");
				bindPopupEvent("#m_pop1", ".open1");
				
				$(document).on("click",".order", function(){
	            	if($(this).hasClass("up")){
	            		getStList(1,$(this).attr("name"),"DESC"); 
	            	    $(".order").not(this).html("▼");
	            	    $(".order").removeClass("up");
	                    $(this).html("▲");
	            	}else{
	            		getStList(1,$(this).attr("name"),"ASC");
	            		$(this).addClass("up");
	                    $(".order").not(this).removeClass("up");
	                    $(".order").html("▼");
	            	}
	            });
			});
			
			function getStList(page,orderType, order) {
			    $.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/admin/user/st/list",
			        data: {
			        	"page" : page			        	
			        	,"user_id" :  $("#user_id").val()
			        	,"user_name" : $("#user_name").val()
			        	,"st_attend_code" : $("#st_attend").attr("data-value")
			        	,"tel" : $("#tel").val()
			        	,"year" : $("#year").attr("data-value")
			        	,"l_seq" : $("#l_seq").attr("data-value")
			        	,"m_seq" : $("#m_seq").attr("data-value")
			        	,"s_seq" : $("#s_seq").attr("data-value")
			        	,"aca_seq" : $("#aca_seq").attr("data-value")
			        	,"orderType" : orderType
			        	,"order" : order			        	
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var htmls = "";
			        		
			        		$("span[data-name=totalCnt]").text(data.totalCnt);
			        		
			        		$("#stListAdd").empty();		        		
			        		//ts1 재학, ts2 휴학, ts3 졸업, ts4 퇴학
			        		$.each(data.list, function(index){
			        			htmls = '<tr class="user-hover" onClick="stDetail('+this.user_seq+');">'
			        			+'<td class="td_1">'+this.row_num+'</td>'
			        			+'<td class="td_1">'+this.id+'</td>'
			        			+'<td class="td_1">'+this.name+'</td>'
			        			+'<td class="td_1">'+this.year+'</td>'
			        			+'<td class="td_1">'+this.laca_name+'</td>'
			        			+'<td class="td_1">'+this.maca_name+'</td>'
			        			+'<td class="td_1">'+this.saca_name+'</td>'
			        			+'<td class="td_1">'+this.aca_name+'</td>'
			        			+'<td class="td_1">'
			        			+'</td>'
			        			+'<td class="td_1">'
			        			+'<span class="ts1">'+this.code_name+'</span>'							
			        			+'</td>'
			        			+'<td class="td_1">'+this.tel+'</td>'
			        			+'<td class="td_1">'
			        			+'<button class="btn_tt4" onclick="">조회</button>'
			        			+'</td>'
			        			+'</tr>'
			        			
			        			$("#stListAdd").append(htmls);
			        		});
			        		
			        		$("#pagingBtnAdd").html(data.pageNav);
			        	} else {
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}			
			
			function codeSelectBoxSetting(t, list) {
				var target = $(t).find("div.uoptions");
				var listHtml = '<span class="uoption" value="">pilih</span>';
				$(list).each(function() {
					listHtml += '<span class="uoption" value="' + this.code + '">' + this.code_name + '</span>';
				});
				$(target).html(listHtml);
			}
			
			function acaSystemSet(level, seq){
	       		if(level==1){
       				$("#m_seq_add span").remove();
                    $("#m_seq").attr("data-value","");
                    $("#m_seq").text("jurusan");
       				$("#s_seq_add span").remove();
                    $("#s_seq").attr("data-value","");
                    $("#s_seq").text("tahun ajaran");	
       				$("#aca_seq_add span").remove();
                    $("#aca_seq").attr("data-value","");
                    $("#aca_seq").text("periode");
	       			acasystemStageOfList(2, seq);	
	       		
	       		}else if(level==2){
	       			$("#s_seq_add span").remove();
	       			$("#s_seq").attr("data-value","");
	       			$("#s_seq").text("tahun ajaran");
       				$("#aca_seq_add span").remove();
                    $("#aca_seq").attr("data-value","");
                    $("#aca_seq").text("periode");	       			
	       			acasystemStageOfList(3, $("#l_seq").attr("data-value"), seq);
	       				
	       		}else if(level==3){
       				$("#aca_seq_add span").remove();
                    $("#aca_seq").attr("data-value","");
                    $("#aca_seq").text("periode");	       			
       				acasystemStageOfList(4, $("#l_seq").attr("data-value"), $("#m_seq").attr("data-value"), seq);	       			
	       		}
	       	}
	        
            function acasystemStageOfList(level, l_seq, m_seq, s_seq){
            	
            	if(level == ""){
            		alert("분류 없음");
            		return;
            	}
            	$.ajax({
            		type: "POST",
            		url: "${HOME}/ajax/admin/academic/academicSystem/acaStageOfList",
            		data: {
            			"level" : level,
            			"l_seq" : l_seq,                	   
            			"m_seq" : m_seq,
            			"s_seq" : s_seq
            			},
           			dataType: "json",
           			success: function(data, status) {
           				var cate = "";
           				if(level == 1)
           					cate = "fakultas";
           				else if(level == 2)
           					cate = "jurusan";
           				else if(level == 3)
           					cate = "tahun ajaran";
           				else if(level == 4)
           					cate = "periode";
           				
           				var htmls = '<span class="uoption" onClick="acaSystemSet('+level+',\'\');" data-value="">'+cate+'</span>';
           				$.each(data.list, function(index){
           					htmls +=  '<span class="uoption" onClick="acaSystemSet('+level+','+this.aca_system_seq+');" data-value="'+this.aca_system_seq+'">'+this.aca_system_name+'</span>';
           				});

           				if(level == 1){
           					$("#l_seq_add span").remove();
           					$("#l_seq_add").html(htmls);        
           				}	                   
           				else if(level == 2){
           					$("#m_seq_add span").remove();
           					$("#m_seq_add").html(htmls);         
           				}
           				else if(level == 3){
           					$("#s_seq_add span").remove();
           					$("#s_seq_add").html(htmls);
        				}else{
        					$("#aca_seq_add span").remove();
           					$("#aca_seq_add").html(htmls);
        				}
        			},
        			error: function(xhr, textStatus) {
        				//alert("오류가 발생했습니다.");
        				document.write(xhr.responseText);
       				},beforeSend:function() {
  					},
  					complete:function() {
    				}
     			});
            	
            }
            
            function stDetail(user_seq){
            	post_to_url("${HOME}/admin/user/st/detail",{"user_seq":user_seq});
            	
            }
                		
        	function file_nameChange(){    
        	    if($("#xlsFile").val() != ""){
        	        var fileValue = $("#xlsFile").val().split("\\");
        	        var fileName = fileValue[fileValue.length-1]; // 파일명
        	        
        	        var fileLen = fileName.length; 
        	        var lastDot = fileName.lastIndexOf('.'); //마지막 . 위치 가져온다.
        	        var fileExt = fileName.substring(lastDot+1, fileLen).toLowerCase(); //마지막 . 위치로 확장자 자름

        	        if(fileExt != "xlsx") {
        	        	alert("xlsx 파일을 registrasi해주세요");
        	        	$("#xlsFile").val("");
        	        	return;			        	
        	        }
        	        
        	        $("#xls_filename").val(fileName);
        	    }
        	}
        	
            function excelSubmit() {

        		if (isEmpty($("#xlsFile").val())) {
        			alert("파일을 pilih해주세요");
        			return;
        		}
        		
        		$("#xlsForm").ajaxForm({
        			type : "POST",
        			url : "${HOME}/ajax/admin/user/st/excel/create",
        			dataType : "json",
        			success : function(data, status) {
        				if (data.status == "200") {
        					alert("엑셀업로드가 완료 되었습니다.");
        					$("#xlsFile").val("");
        					$("#xls_filename").val("");
        					getStList(1);
        				}else if(data.status=="201"){
							alert(data.msg+"번째 행의 ID가 중복됩니다.");
        				}else if(data.status=="202"){
							alert(data.msg+"번째 행의 E-mail이 중복됩니다.");
        				}else if(data.status=="001"){
        					alert("파일을 확인해주세요");	
        				}else {
        					alert(data.status);
        				}
        			},
        			error : function(xhr, textStatus) {
        				alert(textStatus);
        				//document.write(xhr.responseText);
        				$.unblockUI();
        			},
        			beforeSend : function() {
        				$.blockUI();
        			},
        			complete : function() {
        				$.unblockUI();
        			}
        		});
        		$("#xlsForm").submit();
        	}
            
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
		    <div class="contents main">
		        <div class="left_mcon aside_l user">
		            <div class="sub_menu user">
		                <div class="title">이용자manajemen</div>
						<div class="m_schwrap">
							<div class="wrap_s1_uselectbox">
							 	<div class="uselectbox" id="userTypeSelectBox" value="PROFESSOR">
									<span class="uselected">교수</span>
									<span class="uarrow">▼</span>
									<div class="uoptions">
										<span class="uoption" value="PROFESSOR">교수</span>
										<span class="uoption" value="STUDENT">학생</span>
										<span class="uoption" value="STAFF">직원</span>
									</div>
								</div>
							</div>
							<input type="text" class="ip_nm" id="searchUserName" value="" placeholder="이름">
							<button class="btn_sch" onclick="searchUser();"></button>
						</div>
						
						
						<div class="boardwrap">
							<div class="title1">
								<span class="sp_tt two">이용자 registrasi정보 manajemen<br><span class="stt">( 교수 / 직원 )</span></span>
							</div>
							<ul class="m1 panel">
								<li class="wrap"><a href="${HOME}/admin/user/code/attend/list" class="tt_2">재직상태 목록manajemen</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/department/list" class="tt_2">departement 목록manajemen</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/position/list" class="tt_2">교수 posisi 목록manajemen</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/specialty/list" class="tt_2">교수 jurusan khusus 목록manajemen</a></li>
							</ul>
							<div class="title1"><span class="sp_tt">교수 manajemen</span></div>
							<ul class="panel" style="display: table !important;">
		                        <li class="wrap"><a href="${HOME}/admin/user/pf/list" class="tt_2">registrasi / Pencarian</a></li>
		                    </ul>
							<div class="title1 wrapx"><span class="sp_tt">학생 manajemen</span></div>
							<ul class="panel on">
								<li class="wrap"><a href="${HOME }/admin/user/st/list" class="tt_2 on">registrasi / Pencarian</a></li>
<!-- 								<li class="wrap"><a href="../adm/adm_user_system.html" class="tt_2">학생 sistem akademik별 조회</a></li> -->
<!-- 								<li class="wrap"><a href="#" class="tt_2">성취도 manajemen</a></li> -->
							</ul>
							<div class="title1 wrapx"><span class="sp_tt" onclick="location.href='${HOME}/admin/user/staff/list'">직원 manajemen</span></div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
		            </div>
		        </div>
		
				<!-- s_main_con -->
				<div class="main_con user">
					<!-- s_메뉴 접기 버튼 -->
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<!-- e_메뉴 접기 버튼 -->
	
					<!-- s_tt_wrap -->
					<div class="tt_wrap">
						<h3 class="am_tt">
							<span class="tt">이용자manajemen<span class="sign1">&gt;</span>학생 manajemen</span>
						</h3>
					</div>
					<!-- e_tt_wrap -->
	
					<!-- s_adm_content3 -->
					<div class="adm_content3 user">
						<!-- s_tt_wrap -->
	
						<!-- s_wrap_wrap -->
						<div class="wrap_wrap">
							<!-- s_sch_wrap -->
							<div class="sch_wrap">
								<!-- s_rwrap -->
								<div class="rwrap">
									<!-- s_wrap_s -->
									<div class="wrap_s a1st">
										<div class="t1">▶ sistem akademik</div>
	
										<div class="wrap_ss">
											<div class="ss1">
												<div class="wrap_s1_uselectbox">
													<div class="uselectbox">
														<span class="uselected" id="year" data-value="">pilih</span> <span class="uarrow">▼</span>
														<div class="uoptions" style="display: none;">
															<span class="uoption" data-value="">pilih</span>
															<c:forEach var="academicYearList" items="${academicYearList}">
			                                                    <span class="uoption" data-value="${academicYearList.year}">${academicYearList.year}</span>
			                                                </c:forEach>												
														</div>
													</div>
												</div>
	
												<div class="wrap_s2_uselectbox">
													<div class="uselectbox">
														<span class="uselected" id="l_seq" data-value="">fakultas</span> <span class="uarrow">▼</span>
														<div class="uoptions" style="display: none;" id="l_seq_add">
															<c:forEach var="lseqList" items="${lseqList}">
		                                                        <span class="uoption" data-value="${lseqList.aca_system_seq}" onClick="acaSystemSet(1,'${lseqList.aca_system_seq}');">${lseqList.aca_system_name}</span>
		                                                    </c:forEach>
														</div>														
													</div>
												</div>
	
												<div class="wrap_s2_uselectbox">
													<div class="uselectbox">
														<span class="uselected" id="m_seq" data-value="">jurusan</span><span class="uarrow">▼</span>
														<div class="uoptions" style="display: none;" id="m_seq_add">
															
														</div>														
													</div>
												</div>
	
												<div class="wrap_s2_uselectbox">
													<div class="uselectbox">
														<div class="uselectbox">
															<span class="uselected" id="s_seq" data-value="">tahun ajaran</span><span class="uarrow">▼</span>
															<div class="uoptions" style="display: none;" id="s_seq_add">
																
															</div>
														</div>
													</div>
												</div>
	
												<div class="wrap_s1_uselectbox">
													<div class="uselectbox">
														<div class="uselectbox">
															<span class="uselected" id="aca_seq" data-value="">periode</span><span class="uarrow">▼</span>
															<div class="uoptions" style="display: none;" id="aca_seq_add">
																
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- e_wrap_s -->
	
									<!-- s_wrap_s -->
									<div class="wrap_s a2st">
										<div class="t1">▶ 학생정보</div>
	
										<div class="wrap_ss">
											<div class="ss1">
	
												<span class="tt3">학번</span> 
												<input class="ip_tt_1" id="user_id" type="text" value="${user_id }"> 
												<span class="tt3">이름</span> 
												<input class="ip_tt_1" id="user_name" type="text" value="${user_name}"> 
												<span class="tt3">조장여부</span> 
												<input class="ip_chk_1" type="checkbox" id="group_leader"> 
												<span class="tt4">조장만 Pencarian</span>
											</div>
	
											<div class="ss1">
	
												<span class="tt3">status akademik</span>
												<div class="wrap_s2_uselectbox">
													<div class="uselectbox">
														<span class="uselected" id="st_attend" data-value="">pilih</span> <span class="uarrow">▼</span>
														<div class="uoptions" style="display: none;">
															<span class="uoption" data-value="">Keseluruhan</span>
															<c:forEach var="st_attend_code" items="${st_attend_code}">
			                                                    <span class="uoption" data-value="${st_attend_code.code}">${st_attend_code.code_name}</span>
			                                                </c:forEach>
														</div>
													</div>
												</div>
	
												<span class="tt3">kontak</span> <input class="ip_tt_2" value="" type="text" id="tel">
	
											</div>
										</div>
										<!-- e_wrap_s -->
	
	
									</div>
									<!-- s_wrap_s -->
	
									<button class="btn_f1" onClick="test();">접기</button>
								</div>
								<!-- e_rwrap -->
								<button class="btn_f2">Pencarian 펼치기</button>
							</div>
							<!-- s_sch_wrap -->
							<!-- s_btnwrap_s1 -->
							<div class="btnwrap_s1">
								<div class="tt_g1">
									<span class="g_t2">Pencarian결과</span>
									<span class="g_num" data-name="totalCnt"></span>
									<span class="g_t3">명</span>
								</div>
								<button class="btn_tt1" onclick="getStList(1);">Pencarian</button>
							</div>
							<!-- e_btnwrap_s1 -->
	
						</div>
						<!-- e_wrap_wrap -->
	
						<!-- s_wrap_wrap -->
						<div class="wrap_wrap">
							<!-- s_btnwrap_s2 -->
							<div class="btnwrap_s2">
								<!-- s_btnwrap_s2_s -->
								<div class="btnwrap_s2_s">
									<a class="go_r1" href="${HOME }/admin/user/st/create">학생 registrasi</a>
									<button class="btn_up1 open1">엑셀 일괄 registrasi</button>
								</div>
								<!-- e_btnwrap_s2_s -->
	
							</div>
							<!-- e_btnwrap_s2 -->
	
							<table class="mlms_tb userst">
								<thead>
									<tr>
										<th rowspan="2" class="th01 bd01 w1">No.</th>
										<th rowspan="2" class="th01 bd01 w3">
											<div class="th_wrap pd1">학번</div>
											<button class="order down" name="I">▼</button>
										</th>
										<th rowspan="2" class="th01 bd01 w5">
											<div class="th_wrap pd1">이름</div>
											<button class="order down" name="N">▼</button>
										</th>
										<th colspan="5" class="th01 bd01 w2">sistem akademik</th>
										<th rowspan="2" class="th01 bd01 w2">
											<div class="th_wrap pd1">조장<br>여부</div>
											<button class="order down">▼</button>
										</th>
										<th rowspan="2" class="th01 bd01 w2">
											<div class="th_wrap pd1">학사<br>상태</div>
											<button class="order down" name="SA">▼</button>
										</th>
										<th rowspan="2" class="th01 bd01 w4">
											<div class="th_wrap pd1">kontak</div>
										</th>
										<th rowspan="2" class="th01 bd01 w2">
											<div class="th_wrap pd1">학사<br>이력</div>
										</th>
									</tr>
									<tr>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">tahun</div>
											<button class="order down" name="Y">▼</button>
										</th>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">fakultas</div>
											<button class="order down" name="L">▼</button>
										</th>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">jurusan</div>
											<button class="order down" name="M">▼</button>
										</th>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">tahun ajaran</div>
											<button class="order down" name="S">▼</button>
										</th>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">periode</div>
											<button class="order down" name="A">▼</button>
										</th>
									</tr>
								</thead>
								<tbody id="stListAdd">									
									
								</tbody>
							</table>
	
						</div>
						<!-- e_wrap_wrap -->
	
						<!-- s_pagination -->
						<div class="pagination" id="pagingBtnAdd">
							<ul>
								<li><a href="#" title="처음" class="arrow bba"></a></li>
								<li><a href="#" title="이전" class="arrow ba"></a></li>
								<li><a href="#" class="active">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a href="#">6</a></li>
								<li><a href="#">7</a></li>
								<li><a href="#">8</a></li>
								<li><a href="#">9</a></li>
								<li><a href="#">10</a></li>
								<li><a href="#" title="다음" class="arrow na"></a></li>
								<li><a href="#" title="맨끝" class="arrow nna"></a></li>
							</ul>
						</div>
						<!-- e_pagination -->
	
					</div>
					<!-- e_adm_content3 -->
	
				</div>
				<!-- e_main_con -->
	
			</div>
		</div>
		
		<!-- s_ 엑셀 일괄 registrasi 팝업 -->
		<div id="m_pop1" class="pop_up_ex pop_up_ex_1 mo1">
	
			<div class="pop_wrap">
				<p class="popup_title">학생 registrasi 엑셀업로드</p>
	
				<button class="pop_close close1" type="button">X</button>
	
				<button class="btn_exls_down_2" onclick="downloadTmpl('${HOME}','student_template.xlsx');">엑셀 양식 다운로드</button>
	
				<div class="t_title_1">
					<span class="sp_wrap_1">학생 registrasi 엑셀양식을 먼저 다운로드 받으신 뒤,<br>다운로드 받은 엑셀파일양식에 학생 정보를 입력하여 파일 registrasi 하시면 일괄 registrasi됩니다. </span>
				</div>
	
				<div class="pop_ex_wrap">
					<form id="xlsForm" onSubmit="return false;" enctype="multipart/form-data">
						<div class="sub_fwrap_ex_1">
							<span class="ip_tt">file excel</span> 
							<input type="text" readonly class="ip_sort1_1" id="xls_filename" value="" onClick="$('#xlsFile').click();">
							<button class="btn_r1_1" onClick="$('#xlsFile').click();">파일pilih</button>
							<input type="file" style="display: none;" id="xlsFile" name="xlsFile" onChange="file_nameChange();">
						</div>
					</form>
					<!-- s_t_dd -->
					<div class="t_dd">	
						<div class="pop_btn_wrap">
							<button type="button" class="btn01" onclick="excelSubmit();">registrasi</button>
							<button type="button" class="btn02" onclick="$('#m_pop1').hide();">batal</button>
						</div>	
					</div>
					<!-- e_t_dd -->
	
				</div>
			</div>
		</div>
		<!-- e_ 엑셀 일괄 registrasi 팝업 -->
	</body>
</html>