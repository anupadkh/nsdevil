<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function(){
				getCodeCountList();
			});
			
			function getCodeCountList() {
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/admin/user/code/attend/list",
			        data: {},
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var list = data.code_cnt_list;
			        		var noCodePfCnt = data.no_code_pf_cnt;
			        		var noCodeStaffCnt = data.no_code_staff_cnt;
			        		var pfTotalCnt = 0;
			        		var staffTotalCnt = 0;
			        		var listHtml = "";
			        		$(list).each(function(idx){
			        		    listHtml += '<tr>';
			        		    listHtml += '	<td class="td_1">' + (idx + 1) + '</td>';
			        		    listHtml += '	<td class="td_1">';
			        		    listHtml += '		<div class="mdf_wrap_a">';
			        		    listHtml += '			<input type="text" class="ip01a" value="' + this.code_name + '" disabled>';
			        		    listHtml += '		</div>';
			        		    listHtml += '	</td>';
			        		    listHtml += '	<td class="td_1">';
			        		    listHtml += '		<span class="num1">' + this.pf_cnt + '</span>';
			        		    listHtml += '		<span class="sign">|</span>';
			        		    listHtml += '		<span class="num2">' + this.staff_cnt + '</span>';
			        		    listHtml += '	</td>';
			        		    listHtml += '</tr>';
			        		    pfTotalCnt = pfTotalCnt + this.pf_cnt;
			        		    staffTotalCnt = staffTotalCnt + this.staff_cnt;
			        		});
			        		 listHtml += '<tr>';
		        		    listHtml += '	<td class="td_1">-</td>';
		        		    listHtml += '	<td class="td_1">';
		        		    listHtml += '		<div class="mdf_wrap_a">';
		        		    listHtml += '			<input type="text" class="ip01a" value="없음" disabled>';
		        		    listHtml += '		</div>';
		        		    listHtml += '	</td>';
		        		    listHtml += '	<td class="td_1">';
		        		    listHtml += '		<span class="num1">' + noCodePfCnt + '</span>';
		        		    listHtml += '		<span class="sign">|</span>';
		        		    listHtml += '		<span class="num2">' + noCodeStaffCnt + '</span>';
		        		    listHtml += '	</td>';
		        		    listHtml += '</tr>';
		        		    
			        		listHtml += '<tr>';
			        		listHtml += '	<td colspan="2" class="td_1 plus">총 인원</td>';
			        		listHtml += '	<td class="td_1">';
			        		listHtml += '	<span class="num1">' + (pfTotalCnt + noCodePfCnt) + '</span>';
			        		listHtml += '	<span class="sign">|</span>';
			        		listHtml += '	<span class="num2">' + (staffTotalCnt + noCodeStaffCnt) + '</span>';
			        		listHtml += '	</td>';
			        		listHtml += '</tr>';
			        		$("#codeList").html(listHtml);
			        	} else {
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			
		</script>
	</head>
	<body>
		<div id="container" class="container_table">
			<div class="contents main">
				<div class="left_mcon aside_l user">   
					<div class="sub_menu user">
						<div class="title">이용자manajemen</div>
						<div class="m_schwrap">
							<div class="wrap_s1_uselectbox">
							 	<div class="uselectbox" id="userTypeSelectBox" value="PROFESSOR">
									<span class="uselected">교수</span>
									<span class="uarrow">▼</span>
									<div class="uoptions">
										<span class="uoption" value="PROFESSOR">교수</span>
										<span class="uoption" value="STUDENT">학생</span>
										<span class="uoption" value="STAFF">직원</span>
									</div>
								</div>
							</div>
							<input type="text" class="ip_nm" id="searchUserName" value="" placeholder="이름">
							<button class="btn_sch" onclick="searchUser();"></button>
						</div>
			
						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt two on">이용자 registrasi정보 manajemen<br><span class="stt">( 교수 / 직원 )</span></span>
							</div>
							<ul class="m1 panel on">
								<li class="wrap on"><a href="${HOME}/admin/user/code/attend/list" class="tt_2 on">재직상태 목록manajemen</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/department/list" class="tt_2">departement 목록manajemen</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/position/list" class="tt_2">교수 posisi 목록manajemen</a></li>
								<li class="wrap"><a href="${HOME}/admin/user/code/specialty/list" class="tt_2">교수 jurusan khusus 목록manajemen</a></li>
							</ul>
							<div class="title1"><span class="sp_tt">교수 manajemen</span></div>
							<ul class="panel" style="display: table !important;">
		                        <li class="wrap"><a href="${HOME}/admin/user/pf/list" class="tt_2">registrasi / Pencarian</a></li>
		                    </ul>
							<div class="title1"><span class="sp_tt">학생 manajemen</span></div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/user/st/list" class="tt_2">registrasi / Pencarian</a></li>
<!-- 								<li class="wrap"><a href="../adm/adm_user_system.html" class="tt_2">학생 sistem akademik별 조회</a></li> -->
<!-- 								<li class="wrap"><a href="#" class="tt_2">성취도 manajemen</a></li> -->
							</ul>
							<div class="title1 wrapx"><span class="sp_tt" onclick="location.href='${HOME}/admin/user/staff/list'">직원 manajemen</span></div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
					</div>
				</div>
			
				<div class="main_con user">
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<div class="tt_wrap">
						<h3 class="am_tt">
							<span class="tt">이용자manajemen<span class="sign1">&gt;</span>이용자 registrasi정보 manajemen<span class="sign1">&gt;</span>재직상태 목록manajemen 조회</span>
						</h3>                    
					</div>
					<div class="adm_content3 user catalog">
						<div class="wrap_wrap">     
							<div class="tt_swrap">
								<span class="tt">공통 ( 교수 / 직원 공통 사용 )</span>
								<span>* 퇴직의 경우, '계정 사용안함'으로 automatic 설정됩니다.</span>
							</div>
							<table class="mlms_tb s1 v1">
								<thead>
									<tr>
										<th class="th01 b_2 w1">No.</th>
										<th class="th01 b_2 w2">재직상태 목록</th>
										<th class="th01 b_2 w3">교수<span class="sign">|</span>직원</th>
									</tr>
								</thead>
								<tbody id="codeList"></tbody>   
							</table>
						</div> 
						<div class="bt_wrap">
							<button class="bt_2" onclick="location.href='${HOME}/admin/user/code/attend/modify'">perbaiki</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>