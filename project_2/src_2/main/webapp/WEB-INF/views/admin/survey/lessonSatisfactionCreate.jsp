<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
	<style>
	.form_survey_page .content .tb_wrap .wrap_wrap_s .ar_wrap .del{position:relative;top:0px;display: block;width: 16px;height: 25px;line-height: 23px;font-size: 12px;color: rgb(104, 104, 104);margin: 0;padding: 0;border: solid 1px rgb(255, 174, 174);text-align: center;cursor: pointer;box-sizing: border-box;}
	</style>
	<script>
		$(document).ready(function() {
			getSFResearchQuestionList();
			$(document).on("click",".up",function(){
				
				//현재 div 몇번째 인지
				var Index = $("div.wrap_s3").index($(this).closest("div.wrap_s3"));	
				if(Index == 0)
				{
					alert("맨 처음 입니다.");
				}
				else{
					Index -= 1;					
					$("div.wrap_s3:eq("+Index+")").insertAfter($(this).closest("div.wrap_s3"));					 
				}
			});
			
			$(document).on("click",".down", function(){
				//현재 div 몇번째 인지
				var Index = $("div.wrap_s3").index($(this).closest("div.wrap_s3"));	
				if(Index == ($("div.wrap_s3").length-1))
				{
					alert("맨 마지막 입니다.");
				}
				else{
					Index += 1;
					$("div.wrap_s3:eq("+Index+")").insertBefore($(this).closest("div.wrap_s3"));					 
				}
			});
			
			$(document).on("click", ".btn_del", function(){
				$(this).closest("div.free_textarea").remove();
			});
		});
	
		function getSFResearchQuestionList() {
			if(isEmpty($("#sr_seq").val()))
				return;
			
			$.ajax({
				type : "POST",
				url : "${HOME}/ajax/admin/survey/lessonSatisfaction/create/list",
				data : {
					"sr_seq" : $("#sr_seq").val()
				},
				dataType : "json",
				success : function(data, status) {
					if (data.status == "200") {
						var htmls = "";
						var pre_srh_seq = "";
						var pre_srh_detail_type = "";
						$.each(data.questionList, function(index) {
							var readonlyYN = "";
							var inputClass = "";
							var srh_type = "";
							var srh_detail_type = "";
							
							if(index != 0 && pre_srh_seq != this.srh_seq){
								if(pre_srh_detail_type == 4){
									htmls += '<button class="btn_add" onClick="addItem(this);">tambahkan</button></div></div>';
								}
								htmls += '</td></tr></table></div>';
								$("#researchList").append(htmls);
							}
							
							//설문 유형
							if(this.srh_type == 1)
								srh_type = "객관식";									
							else if(this.srh_type == 2)
								srh_type = "주관식";
							else if(this.srh_type == 3)
								srh_type = "표형";
							
							//설문 세부 유형
							if(this.srh_detail_type == 1){
								readonlyYN = "readonly";
								inputClass = "ip_tt";
								srh_detail_type = "1. 그렇다 유형";
							}else if(this.srh_detail_type == 2){
								readonlyYN = "readonly";
								inputClass = "ip_tt";
								srh_detail_type = "2. 만족 유형";
							}else if(this.srh_detail_type == 3){
								readonlyYN = "readonly";
								inputClass = "ip_tt";
								srh_detail_type = "3. (5)~(1) 점수 유형";
							}else if(this.srh_detail_type == 4){
								inputClass = "tarea01";
								srh_detail_type = "4. manual입력";
							}

							if(pre_srh_seq != this.srh_seq){
								htmls = '<div class="wrap_s3" style="border:0px;">'	
									+ '<input type="hidden" name="srh_seq" value="'+this.srh_seq+'">'
									+ '<div class="ar_wrap">'
									+ '<div class="up" title="항목 위로">▲</div>'
									+ '<div class="down" title="항목 아래로">▼</div>'
									+ '<div class="del" title="삭제" onClick="deleteDiv(this);">X</div>'
									+ '</div>'
									+ '<table class="tab_table_u ip_tb02">'
									+ '<tr class="">'
									+ '<td class="th01 list w1"><div class="tb_tt">설문 번호</div></td>'
									+ '<td class="th01 w2 td_l">'
									+ '<input type="text" class="ip_n" name="srh_num" value="'+this.srh_num+'">'
									+ '<span class="tt_s1">필수여부</span>'
									+ '<div class="wrap_rds1">';
								if(this.essential_flag=="Y"){
									htmls += '<span class="tt_ss1">필수</span><input type="radio" name="'+index+'" value="Y" class="rd01" checked="checked">';
									htmls += '<span class="tt_ss1">pilih</span><input type="radio" name="'+index+'" value="N" class="rd01">';
								}else{
									htmls += '<span class="tt_ss1">필수</span><input type="radio" name="'+index+'" value="Y" class="rd01">';
									htmls += '<span class="tt_ss1">pilih</span><input type="radio" name="'+index+'" value="N" class="rd01" checked="checked">';
								}
								
								htmls += '</div>'
									+ '</td>'
									+ '</tr>'
									+ '<tr class="">'
									+ '<td class="th01 list w1"><div class="tb_tt">항목 주제</div></td>'
									+ '<td class="th01 w2 td_l free_textarea">'
									+ '<input type="text" class="tarea01" style="height:20px" name="srh_subject" value="'+this.srh_subject+'"></td>'								
									+ '</tr>'
									+ '<tr class="">'
									+ '<td class="th01 list pd_x01 w1"><div class="tb_tt">설명</div></td>'
									+ '<td class="th01 pd_x01 td_l w2 free_textarea">'
									+ '<textarea class="tarea01" style="height: 20px" name="srh_explan">'+this.srh_explan+'</textarea></td>'
									+ '</tr>'
									+ '<tr class="">'
									+ '<td class="th01 list pd_x01 w1"><div class="tb_tt">유형</div></td>'
									+ '<td class="th01 pd_x01 td_l w2">'
									+ '<div class="f_wrap">'
									+ '<div class="wrap1_lms_uselectbox">'
									+ '<div class="uselectbox">'
									+ '<span class="uselected"><input type="hidden" name="" value="'+this.srh_type+'">'+srh_type+'</span> <span class="uarrow">▼</span>'
									+ '<div class="uoptions">'
									+ '<span class="uoption firstseleted a_t" onClick="questionType(1, this)"><input type="hidden" name="" value="1">객관식</span>'
									+ '<span class="uoption b_t" onClick="questionType(2, this)"><input type="hidden" name="" value="2">주관식</span>'
									//+ '<span class="uoption b_t" onClick="questionType(3, this)"><input type="hidden" name="" value="3">표형</span>'
									+ '</div>'
									+ '</div>'
									+ '</div>';
								if(this.srh_type == 2)
									htmls += '<div class="wrap2_lms_uselectbox" style="display:none;">';
								else
									htmls += '<div class="wrap2_lms_uselectbox">';	
									
								htmls += '<div class="uselectbox">'
									+ '<span class="uselected"><input type="hidden" name="" value="'+this.srh_detail_type+'">'+srh_detail_type+'</span> <span class="uarrow">▼</span>'
									+ '<div class="uoptions">'
									+ '<span class="uoption t1 firstseleted" onClick="questionDetailType(1, this);"><input type="hidden" name="" value="1">1. 그렇다 유형</span>'
									+ '<span class="t2 uoption" onClick="questionDetailType(2, this);"><input type="hidden" name="" value="2">2. 만족 유형</span>'
									+ '<span class="t3 uoption" onClick="questionDetailType(3, this);"><input type="hidden" name="" value="3">3. (5)~(1) 점수 유형</span>'
									+ '<span class="t4 uoption" onClick="questionDetailType(4, this);"><input type="hidden" name="" value="4">4. manual입력</span>'
									+ '</div>'
									+ '</div>'
									+ '</div>'
									+ '</div>'
									+ '</td>'
									+ '</tr>'
									+ '<tr class="blk">'
									+ '<td class="th01 blk">&nbsp</td>'
									+ '<td class="td1 td_l" name="itemList"><div class="type4">';
							}
							
							//객관식 일때
							if(this.srh_type==1){	
								htmls += '<div class="ip_wrap free_textarea">'
									+ '<input type="hidden" name="sri_seq" value="'+this.sri_seq+'">'
									+ '<input type="text" class="'+inputClass+'" '+readonlyYN+' style="height: 30px" name="sri_item_explan" value="'+this.sri_item_explan+'">'
									+ '<button class="btn_c btn_del" title="삭제하기">X</button>'
									+ '</div>';					
							}
							
							if(data.questionList.length == (index+1)){
								if(this.srh_detail_type == 4){
									htmls += '<button class="btn_add" onClick="addItem(this);">tambahkan</button></div></div>';
								}
								htmls += '</td></tr></table></div>';
								$("#researchList").append(htmls);
							}

							pre_srh_seq = this.srh_seq;
							pre_srh_detail_type = this.srh_detail_type;
						});
	
					} else {
						alert("수업만족도 설문 리스트 가져오기 실패");
					}
				},
				error : function(xhr, textStatus) {
					alert("실패");
					//alert("오류가 발생했습니다.");
					document.write(xhr.responseText);
				},
				beforeSend : function() {
				},
				complete : function() {
				}
			});
		}
		
		function addResearch(){
			var inputValue = new Array();
			var srh_type = "";
			var srh_type_name = "pilih";
			var srh_detail_type = "";
			var srh_detail_type_name = "pilih";
			var example_html = "<br><br><br><br>";
			var rdIndex = 0;
			var display = "";
			if($("#researchList div.wrap_s3").length > 0){
				srh_type = $("#researchList div.wrap_s3").last().find("div.wrap1_lms_uselectbox span.uselected input").val();
				srh_type_name = $("#researchList div.wrap_s3").last().find("div.wrap1_lms_uselectbox span.uselected").text();
				srh_detail_type = $("#researchList div.wrap_s3").last().find("div.wrap2_lms_uselectbox span.uselected input").val();
				srh_detail_type_name = $("#researchList div.wrap_s3").last().find("div.wrap2_lms_uselectbox span.uselected").text();
				example_html = $("#researchList div.wrap_s3").last().find("td[name=itemList]").html();
				rdIndex = parseInt($("#researchList div.wrap_s3").last().find("input[type=radio]").attr("name"))+1;
				if(srh_type=="2"){
					display = "style='display:none;'";
				}
			}
			$.each($("#researchList div.wrap_s3").last().find("td[name=itemList] input"), function(index){
				inputValue[index] = $(this).val();
			});
			
			var htmls = '<div class="wrap_s3" style="border:0px;">'	
			+ '<input type="hidden" name="srh_seq" value="">'
			+ '<div class="ar_wrap">'
			+ '<div class="up" title="항목 위로">▲</div>'
			+ '<div class="down" title="항목 아래로">▼</div>'
			+ '<div class="del" title="삭제" onClick="deleteDiv(this);">X</div>'
			+ '</div>'
			+ '<table class="tab_table_u ip_tb02">'
			+ '<tr class="">'
			+ '<td class="th01 list w1"><div class="tb_tt">설문 번호</div></td>'
			+ '<td class="th01 w2 td_l">'
			+ '<input type="text" class="ip_n" name="srh_num" value="">'
			+ '<span class="tt_s1">필수여부</span>'
			+ '<div class="wrap_rds1">'
			+ '<span class="tt_ss1">필수</span><input type="radio" name="'+rdIndex+'" class="rd01" checked="checked" value="Y">'
			+ '<span class="tt_ss1">pilih</span><input type="radio" name="'+rdIndex+'" class="rd01" value="Y">'
			+ '</div>'
			+ '</td>'
			+ '</tr>'
			+ '<tr class="">'
			+ '<td class="th01 list w1"><div class="tb_tt">항목 주제</div></td>'
			+ '<td class="th01 w2 td_l free_textarea">'
			+ '<input type="text" class="tarea01" style="height:20px" name="srh_subject"></td>'
			+ '</tr>'
			+ '<tr class="">'
			+ '<td class="th01 list pd_x01 w1"><div class="tb_tt">설명</div></td>'
			+ '<td class="th01 pd_x01 td_l w2 free_textarea">'
			+ '<textarea class="tarea01" style="height: 20px" name="srh_explan"></textarea></td>'
			+ '</tr>'
			+ '<tr class="">'
			+ '<td class="th01 list pd_x01 w1"><div class="tb_tt">유형</div></td>'
			+ '<td class="th01 pd_x01 td_l w2">'
			+ '<div class="f_wrap">'
			+ '<div class="wrap1_lms_uselectbox">'
			+ '<div class="uselectbox">'
			+ '<span class="uselected"><input type="hidden" name="" value="'+srh_type+'">'+srh_type_name+'</span> <span class="uarrow">▼</span>'
			+ '<div class="uoptions">'
			+ '<span class="uoption firstseleted a_t" onClick="questionType(1, this)"><input type="hidden" name="" value="1">객관식</span>'
			+ '<span class="uoption b_t" onClick="questionType(2, this)"><input type="hidden" name="" value="2">주관식</span>'
			//+ '<span class="uoption b_t" onClick="questionType(3, this)"><input type="hidden" name="" value="3">표형</span>'
			+ '</div>'
			+ '</div>'
			+ '</div>'
			+ '<div class="wrap2_lms_uselectbox" '+display+' >'
			+ '<div class="uselectbox">'
			+ '<span class="uselected"><input type="hidden" name="" value="'+srh_detail_type+'">'+srh_detail_type_name+'</span> <span class="uarrow">▼</span>'
			+ '<div class="uoptions">'
			+ '<span class="uoption t1 firstseleted" onClick="questionDetailType(1, this);"><input type="hidden" name="" value="1">1. 그렇다 유형</span>'
			+ '<span class="t2 uoption" onClick="questionDetailType(2, this);"><input type="hidden" name="" value="2">2. 만족 유형</span>'
			+ '<span class="t3 uoption" onClick="questionDetailType(3, this);"><input type="hidden" name="" value="3">3. (5)~(1) 점수 유형</span>'
			+ '<span class="t4 uoption" onClick="questionDetailType(4, this);"><input type="hidden" name="" value="4">4. manual입력</span>'
			+ '</div>'
			+ '</div>'
			+ '</div>'
			+ '</div>'
			+ '</td>'
			+ '</tr>'
			+ '<tr class="blk">'
			+ '<td class="th01 blk">&nbsp</td>'
			+ '<td class="td1 td_l" name="itemList">' + example_html						
			+ '</td>'
			+ '</tr>'
			+ '</table>'											
			+ '</div>';
			$("#researchList").append(htmls);
			
			$.each($("#researchList div.wrap_s3").last().find("td[name=itemList] input"),function(index){
				$(this).val(inputValue[index]);
			});
			
		}
		
		function deleteDiv(obj){
			if(!confirm("설문을 삭제하시겠습니까?\n삭제 후 simpan하셔야 반영됩니다."))
				return;
			
			$(obj).closest('div.wrap_s3').remove();
			
		}	
		
		function questionType(type, obj){
			if(type == 1){
				/* var htmls = '<div class="type4">'
				+ '<div class="ip_wrap free_textarea">'
				+ '<input type="text" readonly class="ip_tt" style="height: 30px" name="sri_item_explan" value="">'
				+ '<button class="btn_c btn_del" title="삭제하기">X</button>'
				+ '</div>'
				+ '<div class="ip_wrap free_textarea">'
				+ '<input type="text" readonly class="ip_tt" style="height: 30px" name="sri_item_explan" value="">'
				+ '<button class="btn_c btn_del" title="삭제하기">X</button>'
				+ '</div>'
				+ '<div class="ip_wrap free_textarea">'
				+ '<input type="text" readonly class="ip_tt" style="height: 30px" name="sri_item_explan" value="">'
				+ '<button class="btn_c btn_del" title="삭제하기">X</button>'
				+ '</div>'
				+ '<div class="ip_wrap free_textarea">'
				+ '<input type="text" readonly class="ip_tt" style="height: 30px" name="sri_item_explan" value="">'
				+ '<button class="btn_c btn_del" title="삭제하기">X</button>'
				+ '</div>'
				+ '<div class="ip_wrap free_textarea">'
				+ '<input type="text" readonly class="ip_tt" style="height: 30px" name="sri_item_explan" value="">'
				+ '<button class="btn_c btn_del" title="삭제하기">X</button>'
				+ '</div>';
				$(obj).closest("div.wrap_s3").find("td[name=itemList]").html(htmls); */
				$(obj).closest("div.wrap_s3").find("div.wrap2_lms_uselectbox").show();
				$(obj).closest("div.wrap_s3").find("div.wrap2_lms_uselectbox span.uselected").html("<input type='hidden' name='' value=''>pilih");
			}else if(type == 2){
				/* var htmls = '<div class="type5">'
					+ '<div class="ip_wrap free_textarea">'
					+ '<input type="hidden" name="sri_seq" value="">'
					+ '<textarea class="tarea02" style="height: 100px;" placeholder="isi입력" name=""></textarea>'
					+ '</div>'
					+ '</div>'; */
				$(obj).closest("div.wrap_s3").find("td[name=itemList]").html("<br><br><br><br>");
				$(obj).closest("div.wrap_s3").find("div.wrap2_lms_uselectbox").hide();
				$(obj).closest("div.wrap_s3").find("div.wrap2_lms_uselectbox span.uselected").html("<input type='hidden' name='' value=''>pilih");
			}else if(type == 3){
				var htmls = '<div class="type6">'				
					+ '<div class="ip_wrap">'
					+ '<table class="ip_tb">'
					+ '<tbody><tr>'
					+ '<td class="th_1"></td>'
					+ '<td>5</td>'
					+ '<td>4</td>'
					+ '<td>3</td>'
					+ '<td>2</td>'
					+ '<td>1</td>'
					+ '</tr>'
					+ '<tr>'
					+ '<td class="th_1 free_textarea"><textarea class="tarea03" style="height: 12px;" title="여기에 isi을입하세요.">보기주제</textarea></td>'
					+ '<td><input type="radio" name="Rd1" id="Rd6a_0" class="rd01"></td>'
					+ '<td><input type="radio" name="Rd1" id="Rd6a_1" class="rd01"></td>'
					+ '<td><input type="radio" name="Rd1" id="Rd6a_2" class="rd01"></td>'
					+ '<td><input type="radio" name="Rd1" id="Rd6a_3" class="rd01"></td>'
					+ '<td><input type="radio" name="Rd1" id="Rd6a_4" class="rd01"></td>'
					+ '</tr>'
					+ '</tbody></table>'					
					+ '</div>'
					+ '</div>';
				$(obj).closest("div.wrap_s3").find("td[name=itemList]").html(htmls);
				$(obj).closest("div.wrap_s3").find("div.wrap2_lms_uselectbox").show();
				$(obj).closest("div.wrap_s3").find("div.wrap2_lms_uselectbox span.uselected").text("pilih");
				$(obj).closest("div.wrap_s3").find("div.wrap2_lms_uselectbox span.uselected").html("<input type='hidden' name='' value=''>pilih");
			}
		}
		 
		function questionDetailType(type, obj){
			var htmls = "";
			var readonlyYN = "";
			var inputClass = "";
			var array = new Array();
			if(type == 1){
				array[0] = "매우 그렇다";
				array[1] = "그렇다";
				array[2] = "그저 그렇다";
				array[3] = "그렇지 않다";
				array[4] = "매우 그렇지 않다";
				readonlyYN = "readonly";
				inputClass = "ip_tt";
			}else if(type == 2){
				array[0] = "매우 만족";
				array[1] = "만족";
				array[2] = "그저 그렇다";
				array[3] = "불만족";
				array[4] = "매우 불만족";
				readonlyYN = "readonly";
				inputClass = "ip_tt";
			}else if(type == 3){
				array[0] = "5";
				array[1] = "4";
				array[2] = "3";
				array[3] = "2";
				array[4] = "1";
				readonlyYN = "readonly";
				inputClass = "ip_tt";
			}else if(type == 4){
				array[0] = "";
				array[1] = "";
				array[2] = "";
				array[3] = "";
				array[4] = "";
				inputClass = "tarea01";
			}
			htmls = '<div class="type4">'
				+ '<div class="ip_wrap free_textarea">'
				+ '<input type="hidden" name="sri_seq" value="">'
				+ '<input type="text" class="'+inputClass+'" '+readonlyYN+' style="height: 30px" name="sri_item_explan" value="'+array[0]+'">'
				+ '<button class="btn_c btn_del" title="삭제하기">X</button>'
				+ '</div>'
				+ '<div class="ip_wrap free_textarea">'
				+ '<input type="hidden" name="sri_seq" value="">'
				+ '<input type="text" class="'+inputClass+'" '+readonlyYN+' style="height: 30px" name="sri_item_explan" value="'+array[1]+'">'
				+ '<button class="btn_c btn_del" title="삭제하기">X</button>'
				+ '</div>'
				+ '<div class="ip_wrap free_textarea">'
				+ '<input type="hidden" name="sri_seq" value="">'
				+ '<input type="text" class="'+inputClass+'" '+readonlyYN+' style="height: 30px" name="sri_item_explan" value="'+array[2]+'">'
				+ '<button class="btn_c btn_del" title="삭제하기">X</button>'
				+ '</div>'
				+ '<div class="ip_wrap free_textarea">'
				+ '<input type="hidden" name="sri_seq" value="">'
				+ '<input type="text" class="'+inputClass+'" '+readonlyYN+' style="height: 30px" name="sri_item_explan" value="'+array[3]+'">'
				+ '<button class="btn_c btn_del" title="삭제하기">X</button>'
				+ '</div>'
				+ '<div class="ip_wrap free_textarea">'
				+ '<input type="hidden" name="sri_seq" value="">'
				+ '<input type="text" class="'+inputClass+'" '+readonlyYN+' style="height: 30px" name="sri_item_explan" value="'+array[4]+'">'
				+ '<button class="btn_c btn_del" title="삭제하기">X</button>'
				+ '</div>';
			if(type==4){
				htmls += '<button class="btn_add" onClick="addItem(this);">tambahkan</button></div></div>';
			}
			$(obj).closest("div.wrap_s3").find("td[name=itemList]").html(htmls);
		}
		
		function addItem(obj){
			var htmls = '<div class="ip_wrap free_textarea">'
				+ '<input type="hidden" name="sri_seq" value="">'
				+ '<input type="text" class="tarea01" style="height: 30px" name="sri_item_explan" value="">'
				+ '<button class="btn_c btn_del" title="삭제하기">X</button>'
				+ '</div>';
			$(obj).closest("div.wrap_s3").find("div.type4 div:last").after(htmls);
		}
		
		function sfResearchSubmit(){
			if(!confirm("simpan하시겠습니까?")){
				return;
			}
			var val_chk = true;			
			var researchArray = new Array();	
			var sr_seq = $("#sr_seq").val();
			
			if($("#researchList div.wrap_s3").length == 0){
				alert("simpan할 설문이 없습니다.");
				return;
			}
			
			
			$.each($("#researchList div.wrap_s3"), function(index){		
				var listInfo = new Object();
				var srh_seq = $(this).find("input[name=srh_seq]").val(); //헤더 시퀀스
				var srh_order = index+1; //순서
				var srh_subject = $(this).find("input[name=srh_subject]").val(); //주제
				var srh_explan = $(this).find("textarea[name=srh_explan]").val(); //설명
				var srh_type = $(this).find("div.wrap1_lms_uselectbox span.uselected input").val(); //설문 유형
				var srh_detail_type = $(this).find("div.wrap2_lms_uselectbox span.uselected input").val(); //설문 세부유형
				var srh_num = $(this).find("input[name=srh_num]").val(); //문제 번호
				var essential_flag = $(this).find("input[type=radio]:checked").val(); //필수여부
				
				if(isEmpty(srh_type)){
					$(this).attr("tabindex", -1).focus();
					val_chk = false;
					alert("입력안된 설문이 있습니다.");					
					return false;
				}else{
					//유형이 객관식, 표형일때 세부유형 pilih 안할경우
					if(srh_type == "1" || srh_type =="3"){
						if(isEmpty(srh_detail_type)){							
							$(this).attr("tabindex", -1).focus();
							val_chk = false;
							alert("입력안된 설문이 있습니다.");					
							return false;
						}
					}
				}			
				
				if(srh_type == 1){
					var itemListExplan = new Array();
					var itemListSeq = new Array();
					$.each($(this).find("div.type4 div.free_textarea"), function(itemIndex){
						itemListExplan[itemIndex] = $(this).find("input[name=sri_item_explan]").val();
						itemListSeq[itemIndex] = $(this).find("input[name=sri_seq]").val();
					});
					listInfo.sri_item_explan = itemListExplan;
					listInfo.sri_seq = itemListSeq;
				}
				
				listInfo.srh_seq = srh_seq;
				listInfo.srh_order = srh_order;
				listInfo.srh_subject = srh_subject;
				listInfo.srh_explan = srh_explan;
				listInfo.srh_type = srh_type;
				listInfo.srh_detail_type = srh_detail_type;
				listInfo.srh_num = srh_num;
				listInfo.essential_flag = essential_flag;
				researchArray.push(listInfo);
			});
			var researchList = new Object();
			researchList.list = researchArray;
			
			var jsonInfo = JSON.stringify(researchList);
						
			if(val_chk == false){
				return;
			}
			
			$.ajax({
	            type: "POST",
	            url: "${HOME}/ajax/admin/survey/lessonSatisfaction/insert",
	            data: {   
	            	"sr_seq" : sr_seq   
	            	,"sfResearchList" : jsonInfo
	            	,"sr_type" : 1
	            },
	            dataType: "json",
	            success: function(data, status) {
	            	if(data.status == "200"){
		            	alert("simpan 완료 되었습니다.");
		            	location.href='${HOME}/admin/survey';
	            	}else{
	            		alert("simpan 실패 하였습니다.");
	            	}
	            },
	            error: function(xhr, textStatus) {
	                //alert("오류가 발생했습니다.");
	                //document.write(xhr.responseText);
	            }
	        }); 
		}
	</script>
	</head>
	
	<body>
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
		<!-- s_container_table -->
		<div id="container" class="container_table">
			<input type="hidden" name="sr_seq" id="sr_seq" value="${sr_seq }">
			<!-- s_contents -->
			<div class="contents main">
	
				<!-- s_left_mcon -->
				<div class="left_mcon">
					<div class="sub_menu adm_survey">
						<div class="title">설문</div>
						<ul class="sub_panel">
						<li class="mn"><a href="${HOME }/admin/survey/lessonSatisfaction" class="on">수업만족도 조사</a></li>
						<li class="mn"><a href="${HOME }/admin/survey/currSatisfaction">과정만족도 조사</a></li>
						<li class="mn"><a href="#" class="">설문 registrasi / manajemen</a></li>
						</ul>
					</div>
				</div>
				<!-- e_left_mcon -->
	
				<!-- s_main_con -->
				<div class="main_con adm_survey">
	
					<!-- s_tt_wrap -->
					<div class="tt_wrap">
						<h3 class="am_tt">
							<span class="tt">수업만족도 조사 &gt; registrasi</span>
						</h3>
					</div>
					<!-- e_tt_wrap -->
	
					<!-- s_adm_content1 -->
					<div class="adm_content1 survey">
						<!-- s_tt_wrap -->
	
						<!-- s_wrap_wrap -->
						<div class="wrap_wrap">
							<!-- s_수업 만족도 조사 양식 -->
							<div class="form_survey_page">
								<!-- s_pop_wrap -->
								<div class="pop_wrap">
									<p class="t_title">수업만족도 조사</p>
									<!-- s_content -->
									<div class="content">
	
										<!-- s_tb_wrap -->
										<div class="tb_wrap">
	
											<div class="wrap_wrap_s">
												<div class="tt">
													본 설문지는 수업개선의 기초 및 교육의 질적인 향상을 위해 실시합니다.													
													<br>중 pilih하여 주시기 바랍니다. 
													<span class="sp_wrap2">periode 내 수업만족도 조사를 완료하지 않을 경우 매주 -0.5 점씩 감점 처리 됩니다. 
														<br>바로 설문을 완료하여 제출해주세요.
													</span>
												</div>
	
												<div id="researchList">
													
												</div>
												<!-- s_bt_wrap -->
												<div class="bt_wrap_a">
													<button class="bt_2" onClick="sfResearchSubmit();">simpan</button>
													<button class="bt_0" onClick="addResearch();">항목tambahkan<span class="sign">+</span></button>
												</div>
												<!-- e_bt_wrap -->
											</div>
	
										</div>
										<!-- e_tb_wrap -->
	
									</div>
									<!-- e_content -->
								</div>
								<!-- e_pop_wrap -->
							</div>
							<!-- e_수업 만족도 조사 양식 -->
						</div>
						<!-- e_wrap_wrap -->
	
					</div>
					<!-- e_adm_content1 -->
				</div>
				<!-- e_main_con -->
	
			</div>
			<!-- e_contents -->
	
		</div>
		<!-- e_container_table -->
	
	</body>
</html>