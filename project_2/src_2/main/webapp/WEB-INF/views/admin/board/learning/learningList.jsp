<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script src="${JS}/lib/datetimep_1.js"></script>
		<script src="${JS}/lib/monthdatepicker_ui.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				boardListView(1);
				
				$.datetimepicker.setLocale('kr');
			    $('#startDate').datetimepicker({
			        timepicker:false,
			        datepicker:true,
			        format:'y-m-d',
			        formatDate:'y-m-d'
			    });
			    
			    $('#endDate').datetimepicker({
			        timepicker:false,
			        datepicker:true,
			        format:'y-m-d',
			        formatDate:'y-m-d'
			    });
			    
			    $("#startDate").on("change", function(e){
			    	var startDate = $("#startDate").val();
			    	if (startDate != "") {
				    	$('#endDate').datetimepicker({minDate: startDate});
			    	}
			    });
			    
			    $("#endDate").on("change", function(e){
			    	var endDate = $("#endDate").val();
			    	if (endDate != "") {
				    	$('#startDate').datetimepicker({maxDate: endDate});
			    	}
			    });
			});
			
			function boardListView(page) {
				if (typeof page == "undefined") {
					page = 1;
				}
				$("input[name='page']").val(page);
				
				var startDate = $("#startDate").val();
				var endDate = $("#endDate").val();
				
				if (!isEmpty(startDate)) {
					if (!isValidDate(startDate)) {
						alert("시작일의 hari dan tanggal형식이 올바르지않습니다.(YYYY-MM-DD)");
						return false;
					}
				}
				
				if (!isEmpty(endDate)) {
					if (!isValidDate(endDate)) {
						alert("종료일의 hari dan tanggal형식이 올바르지않습니다.(YYYY-MM-DD)");
						return false;
					}
				}
				
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/admin/board/learning/list",
			        data: $("#searchFrm").serialize(),
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var list = data.list;
			        		var totalCnt = data.totalCnt;
				        	var listHtml = '';
				        	$(list).each(function(){
				        		var boardSeq = this.board_seq;
				        		var rowNum = this.row_num;
				        		var hits = this.hits;
				        		var title = this.title;
				        		var content = this.content;
				        		var thumbnailPath = this.thumbnail_path;
				        		content = content.replace(/(<([^>]+)>)/ig,"");
								content = content.replace(/\n/g, "");//행 바꿈 제거
				            	content = content.replace(/\s+/, "");//왼쪽 공백 제거
				            	content = content.replace(/\r/g, "");//엔터 제거
				            	var cateCode = this.board_cate_code;
				        		var fileCnt = this.file_cnt;
				        		var regDate = this.reg_date;
				        		var noText = (totalCnt+1) - rowNum;
				        		var noClass = "";
				        		if (cateCode == "96") {
				        			noText = "공지";
				        			noClass = "s1_1";
				        		}
				        		
				        		listHtml += '<tr>';
				        		listHtml += '	<td class="w1"><span class="' + noClass + '">' + noText + '</span></td>';
				        		listHtml += '	<td class="w3 lnk" onclick="javascript:boardDetail('+boardSeq+')" title="상세보기">';
				        		listHtml += '		<div class="l_con">  ';
				        		if (thumbnailPath != "") {
				        			listHtml += '		<div class="pt_wrap"><div class="pt_box"><img src="${RES_PATH}' + thumbnailPath + '" class="pt1" alt="첨부 사진 이미지"></div></div> ';
				        		}
				        		listHtml += '			<div class="tt_wrap">';
				        		listHtml += '				<span class="sp1">' + title + '</span>';
				        		listHtml += '				<span class="sp2">' + content + '</span>';
				        		listHtml += '				<span class="sp_w"><span class="sp3">' + regDate + '</span><span class="sp4">조회 : ' + hits + '건</span></span>';
				        		listHtml += '			</div>';
				        		listHtml += '		</div>';
				        		listHtml += '	</td>';
				        		listHtml += '	<td class="w2">';
			        			if (fileCnt > 0) {
				        			listHtml += '		<button class="dw" title="다운로드" onclick="javascript:redirectZipfileDownload(\'${HOME}\','+boardSeq+', '+fileCnt+', \'' + title + '\', \'/admin/board/learning/list\');">다운로드</button>';
				        		}
			        			listHtml += '	</td>';
				        		listHtml += '</tr>';
							});
				        	
				        	if (list.length > 0) {
				        		$("#boardList").html(listHtml);
					        	$("#pageNationArea").html(data.pageNav);
				        	} else {
				        		$("#boardList").html('<tr><td colspan="3">registrasi된 게시글이 없습니다.</td></tr>');
				        	}
			        	} else {
			        		//TODO
			        	}
			        	
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function boardDetail(boardSeq) {
				if (typeof boardSeq !=  "undefined" && boardSeq != "") {
					location.href = '${HOME}/admin/board/learning/detail?seq='+boardSeq
				}
			}
			
		</script>
	</head>
	<body>
		<div id="container" class="container_table adm_bd">
			<div class="contents sub">
				<div class="left_mcon aside_l" id="boardMenuArea"></div><!-- 게시판manajemen 메뉴 목록 -->
				<div class="sub_con">
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
		            <a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<div class="tt_wrap">
						<h3 class="am_tt"><span class="tt" id="boardTitleValue"></span></h3>
						<div class="wrap">
							<button class="ic_v4" onClick="location.href='${HOME}/admin/board/learning/create'" title="학습자료실 registrasi">registrasi</button>
						</div>               
					</div>
					<form id="searchFrm" name="searchFrm" method="get" onsubmit="return false;">
							<input type="hidden" id="page" name="page"> 
						<div class="sch_wrap">
						    <span class="tt custom">tanggal registrasi구간</span>
						    <input type="text" class="ip_date custom" id="startDate" name="search_start_date" placeholder="시작일">
						    <span class="tt">~</span>
						    <input type="text" class="ip_date" id="endDate" name="search_end_date" placeholder="종료일">
						    <input type="text" class="ip_search custom" name="search_text" placeholder="제목">
						    <button class="btn_search1" onclick="javascript:boardListView(1);"></button>
						</div>
					</form>	
					
					<div class="rfr_con">   
						<table class="mlms_tb3 top-custom">
							<thead class="custom_thead">
								<tr>
									<th class="th01 bd01 w1">No.</th>
									<th class="th01 bd01 w3">제목</th>
									<th class="th01 bd01 w1">파일</th>
								</tr>
							</thead>
							<tbody id="boardList">
								<tr>
									<td colspan="3">registrasi된 게시글이 없습니다.</td>
								</tr>
							</tbody>
						</table>
					</div> 
					<div class="pagination" id="pageNationArea"></div>
				</div>
			</div>
		</div>
	</body>
</html>