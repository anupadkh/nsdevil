<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
    	<script src="${HOME}/resources/smarteditor_v2.9/js/service/HuskyEZCreator.js"></script> 
		<script type="text/javascript">
			var editor_object = [];
			$(document).ready(function(){
				autosize($("textarea"));
				nhn.husky.EZCreator.createInIFrame({
					oAppRef: editor_object,
				    elPlaceHolder: "content",
		            sSkinURI : "${HOME}/resources/smarteditor_v2.9/SmartEditor2Skin_ko_KR.html"     ,
		            htParams : {
	                    // 툴바 사용 여부 (true:사용/ false:사용하지 않음)
	                    bUseToolbar : true,            
	                    // 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
	                    bUseVerticalResizer : true,    
	                    // 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
	                    bUseModeChanger : true
				    },
		            fOnAppLoad:function(){
		            	editor_object.getById["content"].exec("SE_FIT_IFRAME", [0,400]);	   
					}
				 });
			});
			
			var fileCnt = 0;
			function fileSelect() {
				var div = $("<div>").addClass(".wrap");
				var fileInput = $("<input>").attr("type", "file").attr("id", "uploadFile"+fileCnt).attr("name", "uploadFile").addClass("blind-position");
				$("#fileUploadArea").append(fileInput);
				
				$(fileInput).change(function(){
					var file = $(this).val();
					if (file != "") {
						var attachFileName = file.substring(file.lastIndexOf('\\') + 1); //파일명
						var ext = attachFileName.substring(attachFileName.lastIndexOf('.') + 1).toLowerCase(); //파일 확장자
						var fileMaxSize = 10; //10MB
						var fileSize = this.files[0].size/1024/1024;
						if (fileSize > fileMaxSize) {
							alert("10MB이하의 파일만 업로드 가능합니다.");
							fileValueInit(fileInput);
							return false;
						}
						
						//파일 확장자 유효성검사
						if (/(jpg|png|bmp|mp3|mp4|pdf|hwp|ppt|zip)$/i.test(ext) == false) {
							alert("파일의 확장자를 확인해주세요.(jpg, png, bmp, mp3, mp4 , pdf, hwp, ppt, zip)");
							fileValueInit(fileInput);
							return false;
						}
						$("#fileUploadArea").append('<div class="wrap"><span class="sp1">'+attachFileName+'</span><button class="btn_c" title="삭제하기" onclick="removeAttachFile(this,'+(fileCnt++)+')">X</button></div>');
					}
				});
				$(fileInput).click();
			}
			
			function removeAttachFile(t, num) {
				$(t).parent().remove();
				$("#uploadFile"+num).remove();
			}
			
			function submitBoard() {
				editor_object.getById["content"].exec("UPDATE_CONTENTS_FIELD", []);
				
				var title = $("#title").val();
				if (isEmpty(title) || isBlank(title)){
					alert("제목을 입력해주세요");
					return false;
				}
				var content = $("#content").val();
				if (content == "<p><br></p>") {
					alert("isi을 입력해주세요");
					return false;
				}
				
				$("#boardFrm").attr("action", "${HOME}/ajax/admin/board/hotnews/create").ajaxForm({
					beforeSend: function () {
						$.blockUI();
					},
					type: "POST",
					dataType:"json",
					success:function(data){
						if (data.status == "200") {
							alert("registrasi이 완료되었습니다.");
							location.href='${HOME}/admin/board/hotnews/list';
						} else {
							alert("registrasi에 실패하였습니다. [" + data.status + "]");
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						document.write(xhr.responseText);
				    },
			        complete:function() {
			            $.unblockUI();
			        }
				}).submit();
			}
		</script>
	</head>
	<body>
		<form id="boardFrm" name="boardFrm" onsubmit="return false;" enctype="multipart/form-data">
			<div id="container" class="container_table adm_bd">
				<div class="contents sub">
					<div class="left_mcon aside_l" id="boardMenuArea"></div><!-- 게시판manajemen 메뉴 목록 -->
					<div class="sub_con">
						<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
		            	<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
						<div class="tt_wrap">
							<h3 class="am_tt"><span class="tt" id="boardTitleValue"> 글 registrasi</span></h3>
						</div>
						<div class="rfr_con">   
							<table class="mlms_tb4">
								<tbody>
									<tr>
										<th scope="row"><span class="sp1">제목</span></th>
										<td>
											<input type="text" class="ip_pt_custom" placeholder="제목을 입력해주세요." id="title" name="title" maxlength="100">
										</td>
									</tr>
									<tr>
										<th scope="row"><span class="sp1">isi</span></th>
										<td class="tarea free_textarea">
											<textarea class="tarea01 margin-none" style="height: 100px;" id="content" name="content"></textarea>
										</td>
									</tr>
									<tr>
										<th rowspan="2" scope="row"><span class="sp1">파일첨부</span></th>
										<td>
											<input type="text" class="ip_pt2" value="파일을 첨부해주세요." readonly="readonly">
											<button class="btn1" onclick="fileSelect();">pilih</button>
										</td>
									</tr>
									<tr>
										<td class="f_add1" id="fileUploadArea"></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="btn_wrap_r">
							<button class="bt_2" onclick="submitBoard();">registrasi</button>
							<button class="bt_3" onclick="location.href='${HOME}/admin/board/hotnews/list'">batal</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</body>
</html>