<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<style>
		.main.adm_ccschd .main_con .tabcontent2 .bt_wrap .bt_3:hover{border-radius:4px;width:200px;padding:0px;transition:.7s;}
	</style>
<script src="${JS}/lib/timepicki.js"></script>
<script src="${JS}/schedule.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("body").addClass("full_schd");
		
		$('.free_textarea').on( 'keyup', 'textarea', function (e){
			$(this).css('height', 'auto' );
			$(this).height( this.scrollHeight );
		});
		$('.free_textarea').find( 'textarea' ).keyup();
		$('.timepicker1').timepicki();
		$('.timepicker2').timepicki({custom_classes:"time2"});
		getSchedule("${S_CURRICULUM_SEQ}","${RES_PATH}");
		getCurriculum();
	});
	
	//교육과정 가져오기
	function getCurriculum() {
		$.ajax({
			type : "GET",
			url : "${HOME}/ajax/pf/lesson/curriculumInfo",
			data : {},
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {

					$("span[data-name=curr_name]").text(data.basicInfo.curr_name);
					$("span[data-name=academic_name]").text(": " + data.basicInfo.academic_name);
                    
				} else {

				}
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}
	
	//삭제 후 재registrasi
	function deleteAndRegister(curr_seq) {
		if(!confirm("입력한 waktu표가 삭제됩니다.\n계속 진행하시겠습니까?")) {
			return;
		}
		$.ajax({
	        type: "POST",
	        url: "${HOME}/ajax/pf/lesson/deleteAllSchedule",
	        data: {
	        	"curr_seq": curr_seq
	        },
	        dataType: "json",
	        success: function(data, status) {
	        	if (data.status == "200") {
	        		location.href="./scheduleMod";
				} else {
					alert("오류가 발생했습니다.");
				}
	        },
	        error: function(xhr, textStatus) {
	            alert("오류가 발생했습니다.");
	        }
	    });
	}

</script>
</head>

<body class="full_schd">
<!-- s_main_con -->
<div class="main_con adm_ccschd">
<!-- s_메뉴 접기 버튼 -->	
<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
<!-- e_메뉴 접기 버튼 -->	
	
<!-- s_tt_wrap -->                
<div class="tt_wrap">
			    <h3 class="am_tt">
			    	<span class="tt" data-name="curr_name"></span>
			    	<span class="tt_s" data-name="academic_name"></span>
			    </h3>                    
			</div>
<!-- e_tt_wrap -->

<!-- s_tab_wrap_cc --> 
<div class="tab_wrap_cc">
<!-- s_해당 탭 페이지 class에 active tambahkan --> 
	<button class="tab01 tablinks" onclick="pageMoveCurriculumView('${S_CURRICULUM_SEQ}');">rencan kurikulum</button>	         
	<button class="tab01 tablinks active" onclick="location.href='${HOME}/admin/lesson/schedule'">waktu표manajemen</button>	
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/unit'">단원manajemen</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/lessonPlanCs'">penambahan bab pembelajaran</button>
	<button class="tab01 tablinks" onclick="">만족도 조사</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/currAssignMent'">종합 과제</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/soosiGrade'">수시 성적</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/grade'">종합 성적</button>	
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/report'">운영보고서</button>
<!-- e_해당 탭 페이지 class에 active tambahkan -->
</div>
 
<!-- s_mpf_tabcontent2 -->  
 <div class="tabcontent2">
<!-- s_tt_wrap -->                
<div class="tt_wrap">
    <button class="btn_dw1" onclick="location.href='${HOME}/pf/lesson/scheduleExcelDown'">waktu표 다운로드</button>
<!-- s_sch_wrap -->
<div class="sch_wrap">
<!-- s_wrap_p1_uselectbox -->
<!--
                     <div class="wrap_p1_uselectbox">
		                        <div class="uselectbox">
		                                <span class="uselected">Keseluruhan 교육과정</span>
		                                <span class="uarrow">▼</span>
			
		                            <div class="uoptions">
		                                    <span class="uoption opt1 firstseleted">Keseluruhan 교육과정</span>
		                                    <span class="uoption opt2 ">인체의 구조 Ⅰ</span>
		                                    <span class="uoption opt3 ">호흡기학</span>
		                                    <span class="uoption opt4 ">소화기학 Ⅱ</span>
		                                    <span class="uoption opt5 ">교육과정</span>
		                                    <span class="uoption opt6 ">교육과정</span>
		                                    <span class="uoption opt7 ">교육과정</span>
		                                    <span class="uoption opt8 ">교육과정</span>
		                            </div>
		                        </div>  								
                       </div>
-->
<!-- e_wrap_p1_uselectbox -->

<button onclick="createLesson();" class="btn_view full">수업 registrasi</button>
<button onclick="location.href='./scheduleM'" class="btn_view full">달력 보기</button>
<button onclick="location.href='./scheduleSearch'" class="btn_search1" title="Pencarian"></button>                
            
</div>
<!-- e_sch_wrap -->
</div>
<!-- e_tt_wrap -->


 <div class="mlms_tb_wrap">         
<!-- s_mlms_tb1 -->                                                          
<table class="mlms_tb1">
                    <thead>
                        <tr>
                            <th class="th01 bd01 w1">waktu</th>
                            <th class="th01 bd01 w2">hari dan tanggal</th>
                            <th class="th01 bd01 w2"> pengajar</th>
                            <th class="th01 bd01 w4">nama dosen pengajar</th>
                            <th class="th01 bd01 w4_1">topik pembelajaran</th>
                            <!-- <th class="th01 bd01 w3">cara pengajaran</th> -->
                            <th class="th01 bd01 w2">manajemen</th>
                        </tr>
                    </thead>
                    <tbody id="scheduleList"></tbody>
<%--
                        <tr>
                            <td class="td_1"><span class="tt01">1</span></td>
                            <td class="td_1"><span class="tt_t">8 / 25</span></td>
                            <td class="td_1"><span class="tt01">1</span></td>
                            <td class="td_1 t_l"><span class="a_mp"><span class="pt01"><img src="${IMG}/ph_r1.png" alt="사진" class="pt_img"></span><span class="ssp1">nama dosen pengajar (내과)</span></span></td>
                            <td class="td_1 t_l">해부학 수업</td>
<!-- cara pengajaran - sp01 : lecture, sp02 : 형성, sp03 : pelatihan --> 
                            <td class="td_1"><span class="sp sp01">lecture</span></td>
                            <td class="td_1"><button class="btn_mdf open1">perbaiki</button></td>
                        </tr>
                        <tr>
                            <td class="td_1"><span class="tt01">5</span></td>
                            <td class="td_1"><span class="tt_t">8 / 25</span></td>
                            <td class="td_1"><span class="tt01">1</span></td>
                            <td class="zero01 bd01 t_l"><span class="a_mp"><span class="pt01"><img src="${IMG}/ph_3.png" alt="사진" class="pt_img"></span><span class="ssp1">홍길동 (내과)</span></span></td>
<!-- 미registrasi 시  class : nonregi tambahkan -->
                            <td class="td_1 t_l nonregi">호흡기학 (미registrasi)</td>
                            <td class="td_1"></td>
                            <td class="td_1"><button class="btn_mdf open1">perbaiki</button></td>
                        </tr>
--%>
</table>
<!-- e_mlms_tb1 -->
</div>


<div class="bt_wrap" id="regButton">
    <!--<button class="bt_3a" onclick="location.href='./scheduleMod'">registrasi</button>-->
    <button class="bt_3" onclick="javascript:deleteAndRegister('${S_CURRICULUM_SEQ}');" style="width:200px;">Keseluruhan삭제 - 재registrasi</button>
</div>
</div>
<!-- e_mpf_tabcontent2 -->
<jsp:include page="schedulePopup.jsp">
	<jsp:param name="pageName" value="schedule" />
</jsp:include>
</div>
</body>
</html>