<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <style>
    	.tabcontent1 .tab_table td {border-top : solid 1px rgba(204, 232, 255, 1)}
    </style>
<script type="text/javascript">	
$(document).ready(function(){
    getCurriculum();
    
    var list_count = 0;
    //Metode evaluasi 값 변경 시
	$(document).on("DOMSubtreeModified", "span[name=eval_method_option]", function () {
        if(list_count == 0){
        	if (typeof $(this).find("input").val() == "undefined")
        		return false;
        	
        	var obj = $(this);
        	$.ajax({
                type: "POST",
                url: "${HOME}/ajax/pf/lesson/curriculum/summativeEvaluationCodeList",
                data: {
                    "l_se_code" : $(this).find("input").val()                  
                },
                dataType: "json",
                success: function(data, status) {
                    var htmls = '<span class="uoption"><input type="hidden" value="">pilih</span>';            
                    $.each(data.summativeEvaluationList,function(index){                                	
                    	htmls += '<span class="uoption"><input type="hidden" value="'+this.se_code+'">'+this.se_name+'</span>';       
                    });
                    
                    $(obj).closest("tr").find("div[name=eval_domain_item]").html(htmls);
                    $(obj).closest("tr").find("div[name=eval_domain_item] span").eq(0).click();
                   
                },
                error: function(xhr, textStatus) {
                    //alert("오류가 발생했습니다.");
                    document.write(xhr.responseText);
                }                            
        	});                              	
        }else{
            list_count = 0;
        }
	});
	  
	var mo1 = document.getElementById('m_pop1');
	var button = document.getElementsByClassName('close1')[0];
	var b = document.getElementsByClassName('open1');
	var i;
	    for (i = 0; i < b.length; i++){
	    
	b[i].onclick = function() {
		$("#mpf_add").empty();
		$("#select_pf").empty();
        $("#selectMpfCount").text(0);
        $("#mpf_name").val("");
		curriculumMpfList();
	    mo1.style.display = "block";
	}

	button.onclick = function() {
	    mo1.style.display = "none";
	}

	window.onclick = function(event) {
	    if (event.target == mo1) {
	        mo1.style.display = "none";
	    }
	}
	}
	
	var mo3 = document.getElementById('m_pop3');
	var button = document.getElementsByClassName('close3')[0];
	var b = document.getElementsByClassName('open3');
	var i;
	    for (i = 0; i < b.length; i++){
	    
	b[i].onclick = function() {
		GraduationCapabilityList();
	    mo3.style.display = "block";
	}

	button.onclick = function() {
	    mo3.style.display = "none";
	}

	window.onclick = function(event) {
	    if (event.target == mo4) {
	        mo4.style.display = "none";
	    }
	}
	}
	var mo4 = document.getElementById('m_pop4');
	var button = document.getElementsByClassName('close4')[0];
	var b = document.getElementsByClassName('open4');
	var i;
	    for (i = 0; i < b.length; i++){
	    
	b[i].onclick = function() {
	    mo4.style.display = "block";
	}

	button.onclick = function() {
	    mo4.style.display = "none";
	}

	window.onclick = function(event) {
	    if (event.target == mo4) {
	        mo4.style.display = "none";
	    }
	}
	}
	    
    //교수 tambahkan pilih 체크, 해제 이벤트
    $("#mpf_add").on("change","input[name='chk']",function(){
        var user_seq = $("#mpf_add tr:eq("+$(this).val()+") input[name='user_seq']").val();
        if($(this).prop("checked")){
            var pf_name = $("#mpf_add tr:eq("+$(this).val()+") td[name='pf_name']").text();
            var pf_department = $("#mpf_add tr:eq("+$(this).val()+") td[name='pf_department']").text(); 
            var pf_specialty = $("#mpf_add tr:eq("+$(this).val()+") td[name='pf_specialty']").text(); 
            var pf_picture = $("#mpf_add tr:eq("+$(this).val()+") input[name='picture']").val();
            var email = $("#mpf_add tr:eq("+$(this).val()+") td[name='email']").text();
            var tel = $("#mpf_add tr:eq("+$(this).val()+") td[name='pf_tel']").text();
            var pf_level = $("#mpf_add tr:eq("+$(this).val()+") input[name='pf_level']").val();
            var html = '';
            
            html += '<div class="a_mp" name="user_seq_'+user_seq+'"><input type="hidden" name="user_seq" value="'+user_seq+'">';
            html += '<span class="pt01"><img src="'+pf_picture+'" alt="사진" class="pt_img"></span>';
            html += '<span class="ssp1">'+pf_name+'('+pf_specialty+')</span>';
            html += '<input type="hidden" name="pf_name" value="'+pf_name+'"><input type="hidden" name="pf_specialty" value="'+pf_specialty+'">';
            html += '<input type="hidden" name="pf_department" value="'+pf_department+'"><input type="hidden" name="email" value="'+email+'">';
            html += '<input type="hidden" name="tel" value="'+tel+'"><input type="hidden" name="pf_level" value="'+pf_level+'">';
            html += '<button type="button" class="btn_c" name="pf_del_btn" onClick="delMpf('+user_seq+','+$(this).val()+', this);">X</button></div>';
            
            $("#select_pf").append(html);
        }else{
            $("#select_pf div[name='user_seq_"+user_seq+"']").remove();
        }

        selectMpfCount();
    });
    
    //syarat kelulusan tambahkan pilih 체크, 해제 이벤트
    $("#gcAddList").on("change","input[name='chk']",function(){
        var fc_seq = $("#gcAddList tr:eq("+$(this).val()+") input[name='popup_fc_seq']").val();
        if($(this).prop("checked")){
            var fc_name = $("#gcAddList tr:eq("+$(this).val()+") td[name='fc_name']").text();
            
            var htmls = '<div class="a_mp" name="fc_seq_'+fc_seq+'">'+fc_name+'<button class="btn_c" title="삭제하기" onClick="delFc('+$(this).val()+', this);">X</button></div>';
            
            
            $("#gcSelectItem").append(htmls);
        }else{
            $("#gcSelectItem div[name='fc_seq_"+fc_seq+"']").remove();
        }

        selectGCCount();
    });
    
    
    $(document).on( 'keyup', 'textarea', function (e){
        $(this).css('height', 'auto' );
        $(this).height( this.scrollHeight );
      });
      $(document).find( 'textarea' ).keyup();
      
	$(document).on("click",".btn_l", function(){
		if($("#leftMenuDiv").css("display") == "none"){
			$("button[name=zoom]").text("축소");
		}else{
			$("button[name=zoom]").text("memperlebar/memperluas");
		}
	});
	
	$(document).on("change",".lm_text",function(){
		var total = 0;
		$.each($(".lm_text"), function(index){
			if(!isEmpty($(this).val()))
				total += parseInt($(this).val());
		});
		$("input[name=lm_total]").val(total);
	});
	
	$(document).on("change",".fe_text",function(){
		var total = 0;
		$.each($(".fe_text"), function(index){
			if(!isEmpty($(this).val()))
				total += parseInt($(this).val());
		});
		$("input[name=fe_total]").val(total);
	});
	
	$(document).on("change",".period",function(){
		var total = 0;
		$.each($(".period"), function(index){
			if(!isEmpty($(this).val()))
				total += parseInt($(this).val());
		});
		$("input[name=total_period_cnt]").val(total);
		dateDiff();
	});
});

//교육과정 가져오기
function getCurriculum(){
	$.ajax({
        type: "GET",
        url: "${HOME}/ajax/pf/lesson/curriculum",
        data: {},
        dataType: "json",
        success: function(data, status) {
        	if (data.status == "200") {
        		
        		if(!isEmpty(data.basicInfo.curr_start_date))
        			$("button[name=saveBtn]").text("perbaiki");
        		else
        			$("button[name=saveBtn]").text("registrasi");
        		
        		$("#addPfList tr").remove();
        		
        		var htmls="";
        		
                $("span[name=curr_name]").html(data.basicInfo.curr_name);                            
                $("span[name=aca_system_name]").html("("+ data.basicInfo.aca_system_name + ")");
        		$("td[name=curr_code]").html(data.basicInfo.curr_code);
        		$("td[name=curr_name]").html(data.basicInfo.curr_name);
        		$("td[name=aca_name]").html(data.basicInfo.aca_name);
        		$("td[name=aca_system_name]").html(data.basicInfo.aca_system_name);
        		$("td[name=complete_name]").html(data.basicInfo.complete_name);
        		$("td[name=administer_name]").html(data.basicInfo.administer_name);
        		$("td[name=grade]").html(data.basicInfo.grade);
        		$("td[name=target_name]").html(data.basicInfo.target_name);
        		$("input[name=curr_start_date]").val(data.basicInfo.curr_start_date);
        		$("input[name=curr_end_date]").val(data.basicInfo.curr_end_date);
        		$("textarea[name=curr_summary]").val(data.basicInfo.curr_summary.split('<br/>').join("\r\n"));
        		$("textarea[name=reference_data]").val(data.basicInfo.reference_data.split('<br/>').join("\r\n"));
        		$("textarea[name=management_plan]").val(data.basicInfo.management_plan.split('<br/>').join("\r\n"));
        		$("textarea[name=curr_outcome]").val(data.basicInfo.curr_outcome.split('<br/>').join("\r\n"));
        		$("input[name=grade_method_code][value='"+data.basicInfo.grade_method_code+"']").prop("checked",true);
        		
        		$("input[name=classroom]").val(data.basicInfo.classroom);
        		$("input[name=lecture_period]").val(data.basicInfo.lecture_period);
        		$("input[name=unlecture_period]").val(data.basicInfo.unlecture_period);
        		$("input[name=etc_period]").val(data.basicInfo.etc_period);
                $("input[name=period_cnt]").val(data.basicInfo.period_cnt);
                $("input[name=total_period_cnt]").val(data.basicInfo.total_period);
                $("#weekList").find("input[value='"+data.basicInfo.curr_week+"']").closest("span").click();
                //Dosen Penanggung jawab
                $("#addPfList").empty();


                //주당평균waktu 구하기
                dateDiff();
                
        		$.each(data.mpfList,function(){
        			htmls+='<tr class="tr01">';
                    htmls+='<td class="ta_c">책임<br>교수<input type="hidden" name="pf_user_seq" value="'+this.user_seq+'"/></td>';	                            
                    htmls+='<td class="ta_c">'+this.name+'<input type="hidden" name="pf_level" value="'+this.professor_level+'"/></td>';
                    htmls+='<td class="ta_c">'+this.department_name+'</td>';
                    htmls+='<td class="ta_c">'+this.specialty+'</td>';
                    htmls+='<td class="ta_c ls_1">'+this.tel+'</td>';
                    htmls+='<td class="ta_c ls_1"><a class="mailto_1" href="" target="_top">'+this.email+'</a></td>';
                    htmls+='<td class="ta_c w1_1"></td></tr>';
        		});
                $("#addPfList").append(htmls);
                htmls="";
                
                //부Dosen Penanggung jawab
        		$.each(data.dpfList,function(){
        			htmls+='<tr class="tr01">';
                    htmls+='<td class="ta_c">부책임<br>교수<input type="hidden" name="pf_user_seq" value="'+this.user_seq+'"/></td>';
                    htmls+='<td class="ta_c">'+this.name+'<input type="hidden" name="pf_level" value="'+this.professor_level+'"/></td>';
                    htmls+='<td class="ta_c">'+this.department_name+'</td>';
                    htmls+='<td class="ta_c">'+this.specialty+'</td>';
                    htmls+='<td class="ta_c ls_1">'+this.tel+'</td>';
                    htmls+='<td class="ta_c ls_1"><a class="mailto_1" href="" target="_top">'+this.email+'</a></td>';
                    htmls+='<td class="ta_c w1_1"></td></tr>';
        		});
        		$("#addPfList").append(htmls);
        		htmls="";
        		
                //일반교수
        		$.each(data.gpfList,function(){
                    htmls+='<tr class="tr01">';
                    htmls+='<td class="ta_c">교수<input type="hidden" name="pf_user_seq" value="'+this.user_seq+'"/></td>';
                    htmls+='<td class="ta_c">'+this.name+'<input type="hidden" name="pf_level" value="'+this.professor_level+'"/></td>';
                    htmls+='<td class="ta_c">'+this.department_name+'</td>';
                    htmls+='<td class="ta_c">'+this.specialty+'</td>';
                    htmls+='<td class="ta_c ls_1">'+this.tel+'</td>';
                    htmls+='<td class="ta_c ls_1"><a class="mailto_1" href="" target="_top">'+this.email+'</a></td>';
                    htmls+='<td class="ta_c w1_1"><input type="checkbox" name="pfchk" class="chk01" value="'+this.user_seq+'"></td></tr>';
                });
        		$("#addPfList").append(htmls);			        					
        		htmls='';
        				
        		var lessonMethod_code_arry = new Array();
        		var lessonMethod_name_arry = new Array();
        		var lessonMethod_cnt_arry = new Array();
        		var lm_sum = 0;
        		
        		$.each(data.lessonMethodList, function(index){
        			lessonMethod_code_arry[index] = this.code;
        			lessonMethod_name_arry[index] = this.code_name;
        			lessonMethod_cnt_arry[index] = this.lp_cnt;
        			lm_sum += this.lp_cnt;
        		});
        		
        		var for_int = 0;
        		if(lessonMethod_code_arry.length < 4)
        			for_int = 1;
        		else if((lessonMethod_code_arry.length)%4==0)
        			for_int = parseInt(lessonMethod_code_arry.length/4)+1;
        		else
                    for_int = parseInt((lessonMethod_code_arry.length+1)/4)+1;
        		
        		for(var i=0;i<for_int*4;i+=4){
        			htmls+='<tr class="tr01">';
        			if(i==0) htmls+= '<td rowspan="'+(for_int*2)+'" class="th01 bd01 bd02 w6">tidak ada perkuliahan<br>상세</td>';
        			
        			if(lessonMethod_name_arry.length > i) htmls+= '<td class="th01 w8">'+lessonMethod_name_arry[i]+'</td>';
                    else htmls+= '<td class="th01 w8"></td>';
                    if(lessonMethod_name_arry.length > (i+1)) htmls+= '<td class="th01 w8">'+lessonMethod_name_arry[i+1]+'</td>';
                    else htmls+= '<td class="th01 w8"></td>';
                    if(lessonMethod_name_arry.length > (i+2)) htmls+= '<td class="th01 w8">'+lessonMethod_name_arry[i+2]+'</td>';
                    else htmls+= '<td class="th01 w8"></td>';
                    if(lessonMethod_name_arry.length > (i+3)) htmls+= '<td class="th01 w8">'+lessonMethod_name_arry[i+3]+'</td>';
                    else htmls+= '<td class="th01 w8">subtotal</td>';
                    
                    
                    
                    htmls+= '</tr><tr class="tr01">';
                    if(lessonMethod_code_arry.length > i){                                	
                        htmls+= '<td class="w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" name="'+lessonMethod_code_arry[i]+'" class="ip04 lm_text" value="'+lessonMethod_cnt_arry[i]+'"></td>';
                    }
                    else{
                        htmls+= '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="-"></td>';
                    }                                
                    if(lessonMethod_code_arry.length > (i+1)){
                    	 htmls+= '<td class="w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" name="'+lessonMethod_code_arry[i+1]+'" class="ip04 lm_text" value="'+lessonMethod_cnt_arry[i+1]+'"></td>';
                    }
                    else
                        htmls+= '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="-"></td>';
                    if(lessonMethod_code_arry.length > (i+2)){
                    	 htmls+= '<td class="w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" name="'+lessonMethod_code_arry[i+2]+'" class="ip04 lm_text" value="'+lessonMethod_cnt_arry[i+2]+'"></td>';
                    }
                    else
                        htmls+= '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="-"></td>';
                    if(lessonMethod_code_arry.length > (i+3)){
                    	 htmls+= '<td class="w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" name="'+lessonMethod_code_arry[i+3]+'" class="ip04 lm_text" value="'+lessonMethod_cnt_arry[i+3]+'"></td>';
                    }
                    else
                        htmls+= '<td class="w6 ta_c"><input type="text" readonly name="lm_total" class="ip04" value="'+lm_sum+'"></td>';
                    htmls+= '</tr>';
        		}
        		$("#lessonMethodList").empty();
        		$("#lessonMethodList").html(htmls);
        		/* 
        		if(lessonMethod_code_arry.length%4==0){
        			htmls+='<tr class="tr01">';
                    htmls+= '<td class="th01 w8"></td>';
                    htmls+= '<td class="th01 w8"></td>';
                    htmls+= '<td class="th01 w8"></td>'; 
                    htmls+= '<td class="th01 w8">subtotal</td>';                                
                    htmls+= '</tr><tr class="tr01">';
                    htmls+= '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="-"></td>';
                    htmls+= '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="-"></td>';
					htmls+= '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="-"></td>';
                    htmls+= '<td class="w6 ta_c"><input type="text" readonly name="lm_total" class="ip04" value="'+lm_sum+'"></td>';
                    htmls+= '</tr>';
        		}        		
*/ 
                //형성penilaian 리스트        		
        		htmls = '';
        		var fe_sum = 0;
        		var fe_name_arry = new Array();
        		var fe_cnt_arry = new Array();
        		var fe_code_array = new Array();
        		
        		$.each(data.formationEvaluationCs, function(index){
        			fe_code_array[index] = this.code;
        			fe_name_arry[index] = this.code_name;
        			fe_cnt_arry[index] = this.cnt;
        			fe_sum += fe_cnt_arry[index]*1;
        		});
        		
        		for_int = 0;
        		if(fe_name_arry.length < 5)
        			for_int = 1;
        		else if(fe_name_arry.length%5==0)
        			for_int = parseInt(fe_name_arry.length/5)+1;
        		else
        			for_int = parseInt((fe_name_arry.length+1)/5)+1;

        		for(var i=0;i<for_int*5;i+=5){
        			htmls+='<tr class="tr01">';
        			if(fe_name_arry.length > i) htmls+='<td class="th01 w6">'+fe_name_arry[i]+'</td>';
                    else htmls+='<td class="th01 w6"></td>';

        			if(fe_name_arry.length > (i+1)) htmls+='<td class="th01 w8">'+fe_name_arry[i+1]+'</td>';
				    else htmls+='<td class="th01 w8"></td>';
					    
				    if(fe_name_arry.length > (i+2)) htmls+='<td class="th01 w8">'+fe_name_arry[i+2]+'</td>';
                    else htmls+='<td class="th01 w8"></td>';
                        
					if(fe_name_arry.length > (i+3)) htmls+='<td class="th01 w8">'+fe_name_arry[i+3]+'</td>';
					else htmls+='<td class="th01 w8"></td>';
					    
				    if(fe_name_arry.length > (i+4)) htmls+='<td class="th01 w8">'+fe_name_arry[i+4]+'</td>';
                    else htmls+='<td class="th01 w8">subtotal</td>';                         
                    htmls+='</tr>';
                    
                    htmls+='<tr class="tr01">';
                    if(fe_name_arry.length > i) htmls+='<td class="bd01 w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" class="ip04 fe_text" name="'+fe_code_array[i]+'" value="'+fe_cnt_arry[i]+'"></td>';
                    else htmls+='<td class="bd01 w6 ta_c"></td>';

                    if(fe_name_arry.length > (i+1)) htmls+='<td class="bd01 w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" class="ip04 fe_text" name="'+fe_code_array[i+1]+'" value="'+fe_cnt_arry[i+1]+'"></td>';
                    else htmls+='<td class="bd01 w6 ta_c"></td>';
                        
                    if(fe_name_arry.length > (i+2)) htmls+='<td class="bd01 w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" class="ip04 fe_text" name="'+fe_code_array[i+2]+'" value="'+fe_cnt_arry[i+2]+'"></td>';
                    else htmls+='<td class="bd01 w6 ta_c"></td>';
                        
                    if(fe_name_arry.length > (i+3)) htmls+='<td class="bd01 w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" class="ip04 fe_text" name="'+fe_code_array[i+3]+'" value="'+fe_cnt_arry[i+3]+'"></td>';
                    else htmls+='<td class="bd01 w6 ta_c"></td>';
                        
                    if(fe_name_arry.length > (i+4)) htmls+='<td class="bd01 w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" readonly class="ip04 fe_text" name="'+fe_code_array[i+4]+'" value="'+fe_cnt_arry[i+4]+'"></td>';
                    else htmls+='<td class="bd01 w6 ta_c"><input type="text" class="ip04" readonly name="fe_total" value="'+fe_sum+'"></td>';                     
                    
                    htmls+='</tr>';                                
        		}
        		$("#feAdd").empty();
        		$("#feAdd").html(htmls);
        		
        		/* htmls = '';
        		if(fe_name_arry.length%4==0){
        			htmls='<tr class="tr01">';
        			htmls+='<td class="th01 w6"></td>';
					htmls+='<td class="th01 w8"></td>';
					htmls+='<td class="th01 w8"></td>';
					htmls+='<td class="th01 w8"></td>';
					htmls+='<td class="th01 w8">subtotal</td>';                         
                    htmls+='</tr>';
                    
                    htmls+='<tr class="tr01">';

                    htmls+='<td class="bd01 w6 ta_c"></td>';
                    htmls+='<td class="bd01 w6 ta_c"></td>';
                    htmls+='<td class="bd01 w6 ta_c"></td>';
                    htmls+='<td class="bd01 w6 ta_c"></td>';
                    htmls+='<td class="bd01 w6 ta_c"><input type="text" class="ip04" readonly name="fe_total" value="'+fe_sum+'"></td>';                     
                    
                    htmls+='</tr>';   
        		} 
        		$("#feAdd").append(htmls);*/
        		htmls='';
            
        		var importance = 0;
        		//총괄penilaian기준
        		$.each(data.summativeEvaluationCs, function(index){
        			htmls += '<tr class="tr01">';
                    htmls += '<td class="w6 ta_c"><input type="hidden" name="eval_method_option" value="'+this.eval_method_code+'"/>'+this.eval_method_name+'</td>';
                    htmls += '<td class="w6 ta_c"><input type="hidden" name="eval_domain_code" value="'+this.eval_domain_code+'"/>'+this.eval_domain_name+'</td>';
                    htmls += '<td class="w6_1 ta_c"><input type="text" class="ip03" onfocusout="calImportance(this)" name="importance" value="'+this.importance+'"><span class="sp02">%</span></td>';
                    htmls += '<td class="w8_1 ta_c pd0 free_textarea"><textarea class="tarea01" rows="1" name="eval_method_text" placeholder="Metode dan Kriteria Evaluasi" style="height: 49px;">'+this.eval_method_text.split('<br/>').join("\r\n")+'</textarea></td>';
                    htmls += '<td class="w8_1 ta_c pd0 free_textarea"><textarea class="tarea01" rows="1" name="eval_etc" placeholder="Keterangan" style="height: 49px;">'+this.eval_etc.split('<br/>').join("\r\n")+'</textarea></td>';
                    htmls += '<td class="ta_c w1_1"><input type="hidden" name="se_seq" value="'+this.se_seq+'"/><input type="checkbox" name="eval_chk" class="chk01"></td>';
                    importance += this.importance;
        		});
                $("#AddEvalList").empty();
                $("#AddEvalList").append(htmls);
                $("td[data-name=totalEval]").text(importance+"%");
                htmls='';
                
                //syarat kelulusan
                $.each(data.mpCurriculumGraduationCapabilityList ,function(index){
                	htmls+='<tr class="tr01">';
                    htmls+='<td class=""><input type="hidden" name="fc_seq" value="'+this.fc_seq+'"/>'+this.fc_name+'</td>';
                    htmls+='<td class="pd0 free_textarea"><textarea class="tarea01" rows="1" style="height: 49px;" placeholder="* Silakan masukkan konten ." name="process_result">'+this.process_result.split('<br/>').join("\r\n")+'</textarea></td>';
                    htmls+='<td class="ta_c pd0 free_textarea"><textarea class="tarea03" rows="1" style="height: 49px;" name="teaching_method">'+this.teaching_method.split('<br/>').join("\r\n")+'</textarea></td>';
                    htmls+='<td class="pd0 free_textarea"><textarea class="tarea03" rows="1" style="height: 49px;" name="evaluation_method">'+this.evaluation_method.split('<br/>').join("\r\n")+'</textarea></td>';
                    htmls+='<td class="ta_c w1_1"><input type="checkbox" name="fcchk" class="chk01"></td></tr>';
                });

                $("#mpFinishCapabilityList").empty();
                $("#mpFinishCapabilityList").append(htmls);
                htmls='';
                
                /* 
                //nilai 기준
                var htmls1='<tr class="tr01">';
                var htmls2='<tr class="tr01">';
                var sumGrade = 0;
                $.each(data.gradeStandard, function(index){
                	htmls1+='<td class="th01 w9_1"><input type="hidden" name="grade_standard_code" value="'+this.code+'"/>'+this.code_name+'</td>';
                    htmls2+='<td class="bd01 ta_c"><input type="text" class="ip02_1_1" onfocusout="gradeCalImportance(this);" name="grade_importance" value="'+this.grade_importance+'"><span class="sp02_1">%</span></td>';
                    sumGrade += this.grade_importance;
                });
                htmls1+='<td class="th01 w9_2">계</td></tr>';
                htmls2+='<td class="bd01 ta_c" name="totalGradeImportance">'+sumGrade+'%</td></tr>';
                $("#gradeStandardList").html(htmls1+htmls2); */
                $.each($("textarea"),function(){
                	$(this).height($(this).prop("scrollHeight"));
                });
        	} else {
        		
        	}
        },
        error: function(xhr, textStatus) {
            document.write(xhr.responseText);
        },
        beforeSend:function() {
            $.blockUI();
        },
        complete:function() {
            $.unblockUI();
        }
    });
}

//이전교육과정 가져오기
function getPreCurriculum(curr_code){
	if(isEmpty(curr_code)){
		alert("이전 교육과정을 조회할 kode kurikulum Akademik가 입력되지 않았습니다.");
		return;
	}
	$.ajax({
        type: "GET",
        url: "${HOME}/ajax/pf/lesson/preCurriculum",
        data: {
        	"curr_code" : curr_code
        },
        dataType: "json",
        success: function(data, status) {
        	if (data.status == "200") {
        		if(!confirm(data.preCurrInfo.year + "년 과정계획서로 변경하시겠습니까?"))
        			return;			        		
        		
        		if(!isEmpty(data.basicInfo.curr_start_date))
        			$("button[name=saveBtn]").text("perbaiki");
        		else
        			$("button[name=saveBtn]").text("registrasi");
        					        		
        		var htmls="";			        		
                
        		$("textarea[name=curr_summary]").val(data.basicInfo.curr_summary.split('<br/>').join("\r\n"));
        		$("textarea[name=reference_data]").val(data.basicInfo.reference_data.split('<br/>').join("\r\n"));
        		$("textarea[name=management_plan]").val(data.basicInfo.management_plan.split('<br/>').join("\r\n"));
        		$("textarea[name=curr_outcome]").val(data.basicInfo.curr_outcome.split('<br/>').join("\r\n"));			        		
        		
        		$("input[name=lecture_period]").val(data.basicInfo.lecture_period);
        		$("input[name=unlecture_period]").val(data.basicInfo.unlecture_period);
        		$("input[name=etc_period]").val(data.basicInfo.etc_period);
                $("input[name=period_cnt]").val(data.basicInfo.period_cnt);
                $("input[name=total_period_cnt]").val(data.basicInfo.total_period);
                $("#weekList").find("input[value='"+data.basicInfo.curr_week+"']").closest("span").click();
                
        		htmls="";
        		
                var lessonMethod_code_arry = new Array();
        		var lessonMethod_name_arry = new Array();
        		var lessonMethod_cnt_arry = new Array();
        		var lm_sum = 0;
        		
        		$.each(data.lessonMethodList, function(index){
        			lessonMethod_code_arry[index] = this.code;
        			lessonMethod_name_arry[index] = this.code_name;
        			lessonMethod_cnt_arry[index] = this.lp_cnt;
        			lm_sum += this.lp_cnt;
        		});
        		
        		var for_int = 0;
        		if(lessonMethod_code_arry.length < 4)
        			for_int = 1;
        		else if((lessonMethod_code_arry.length)%4==0)
        			for_int = parseInt(lessonMethod_code_arry.length/4)+1;
        		else
                    for_int = parseInt((lessonMethod_code_arry.length+1)/4)+1;
        		
        		for(var i=0;i<for_int*4;i+=4){
        			htmls+='<tr class="tr01">';
        			if(i==0) htmls+= '<td rowspan="'+(for_int*2)+'" class="th01 bd01 bd02 w6">tidak ada perkuliahan<br>상세</td>';
        			
        			if(lessonMethod_name_arry.length > i) htmls+= '<td class="th01 w8">'+lessonMethod_name_arry[i]+'</td>';
                    else htmls+= '<td class="th01 w8"></td>';
                    if(lessonMethod_name_arry.length > (i+1)) htmls+= '<td class="th01 w8">'+lessonMethod_name_arry[i+1]+'</td>';
                    else htmls+= '<td class="th01 w8"></td>';
                    if(lessonMethod_name_arry.length > (i+2)) htmls+= '<td class="th01 w8">'+lessonMethod_name_arry[i+2]+'</td>';
                    else htmls+= '<td class="th01 w8"></td>';
                    if(lessonMethod_name_arry.length > (i+3)) htmls+= '<td class="th01 w8">'+lessonMethod_name_arry[i+3]+'</td>';
                    else htmls+= '<td class="th01 w8">subtotal</td>';
                    
                    
                    
                    htmls+= '</tr><tr class="tr01">';
                    if(lessonMethod_code_arry.length > i){                                	
                        htmls+= '<td class="w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" name="'+lessonMethod_code_arry[i]+'" class="ip04 lm_text" value="'+lessonMethod_cnt_arry[i]+'"></td>';
                    }
                    else{
                        htmls+= '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="-"></td>';
                    }                                
                    if(lessonMethod_code_arry.length > (i+1)){
                    	 htmls+= '<td class="w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" name="'+lessonMethod_code_arry[i+1]+'" class="ip04 lm_text" value="'+lessonMethod_cnt_arry[i+1]+'"></td>';
                    }
                    else
                        htmls+= '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="-"></td>';
                    if(lessonMethod_code_arry.length > (i+2)){
                    	 htmls+= '<td class="w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" name="'+lessonMethod_code_arry[i+2]+'" class="ip04 lm_text" value="'+lessonMethod_cnt_arry[i+2]+'"></td>';
                    }
                    else
                        htmls+= '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="-"></td>';
                    if(lessonMethod_code_arry.length > (i+3)){
                    	 htmls+= '<td class="w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" name="'+lessonMethod_code_arry[i+3]+'" class="ip04 lm_text" value="'+lessonMethod_cnt_arry[i+3]+'"></td>';
                    }
                    else
                        htmls+= '<td class="w6 ta_c"><input type="text" readonly name="lm_total" class="ip04" value="'+lm_sum+'"></td>';
                    htmls+= '</tr>';
        		}
        		$("#lessonMethodList").empty();
        		$("#lessonMethodList").html(htmls);
        		/* 
        		if(lessonMethod_code_arry.length%4==0){
        			htmls+='<tr class="tr01">';
                    htmls+= '<td class="th01 w8"></td>';
                    htmls+= '<td class="th01 w8"></td>';
                    htmls+= '<td class="th01 w8"></td>'; 
                    htmls+= '<td class="th01 w8">subtotal</td>';                                
                    htmls+= '</tr><tr class="tr01">';
                    htmls+= '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="-"></td>';
                    htmls+= '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="-"></td>';
					htmls+= '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="-"></td>';
                    htmls+= '<td class="w6 ta_c"><input type="text" readonly name="lm_total" class="ip04" value="'+lm_sum+'"></td>';
                    htmls+= '</tr>';
        		}
*/ 
                //형성penilaian 리스트        		
        		htmls = '';
        		var fe_sum = 0;
        		var fe_name_arry = new Array();
        		var fe_cnt_arry = new Array();
        		var fe_code_array = new Array();
        		
        		$.each(data.formationEvaluationCs, function(index){
        			fe_code_array[index] = this.code;
        			fe_name_arry[index] = this.code_name;
        			fe_cnt_arry[index] = this.cnt;
        			fe_sum += fe_cnt_arry[index]*1;
        		});
        		
        		for_int = 0;
        		if(fe_name_arry.length < 5)
        			for_int = 1;
        		else if(fe_name_arry.length%5==0)
        			for_int = parseInt(fe_name_arry.length/5)+1;
        		else
        			for_int = parseInt((fe_name_arry.length+1)/5)+1;

        		for(var i=0;i<for_int*5;i+=5){
        			htmls+='<tr class="tr01">';
        			if(fe_name_arry.length > i) htmls+='<td class="th01 w6">'+fe_name_arry[i]+'</td>';
                    else htmls+='<td class="th01 w6"></td>';

        			if(fe_name_arry.length > (i+1)) htmls+='<td class="th01 w8">'+fe_name_arry[i+1]+'</td>';
				    else htmls+='<td class="th01 w8"></td>';
					    
				    if(fe_name_arry.length > (i+2)) htmls+='<td class="th01 w8">'+fe_name_arry[i+2]+'</td>';
                    else htmls+='<td class="th01 w8"></td>';
                        
					if(fe_name_arry.length > (i+3)) htmls+='<td class="th01 w8">'+fe_name_arry[i+3]+'</td>';
					else htmls+='<td class="th01 w8"></td>';
					    
				    if(fe_name_arry.length > (i+4)) htmls+='<td class="th01 w8">'+fe_name_arry[i+4]+'</td>';
                    else htmls+='<td class="th01 w8">subtotal</td>';                         
                    htmls+='</tr>';
                    
                    htmls+='<tr class="tr01">';
                    if(fe_name_arry.length > i) htmls+='<td class="bd01 w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" class="ip04 fe_text" name="'+fe_code_array[i]+'" value="'+fe_cnt_arry[i]+'"></td>';
                    else htmls+='<td class="bd01 w6 ta_c"></td>';

                    if(fe_name_arry.length > (i+1)) htmls+='<td class="bd01 w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" class="ip04 fe_text" name="'+fe_code_array[i+1]+'" value="'+fe_cnt_arry[i+1]+'"></td>';
                    else htmls+='<td class="bd01 w6 ta_c"></td>';
                        
                    if(fe_name_arry.length > (i+2)) htmls+='<td class="bd01 w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" class="ip04 fe_text" name="'+fe_code_array[i+2]+'" value="'+fe_cnt_arry[i+2]+'"></td>';
                    else htmls+='<td class="bd01 w6 ta_c"></td>';
                        
                    if(fe_name_arry.length > (i+3)) htmls+='<td class="bd01 w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" class="ip04 fe_text" name="'+fe_code_array[i+3]+'" value="'+fe_cnt_arry[i+3]+'"></td>';
                    else htmls+='<td class="bd01 w6 ta_c"></td>';
                        
                    if(fe_name_arry.length > (i+4)) htmls+='<td class="bd01 w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" readonly class="ip04 fe_text" name="'+fe_code_array[i+4]+'" value="'+fe_cnt_arry[i+4]+'"></td>';
                    else htmls+='<td class="bd01 w6 ta_c"><input type="text" class="ip04" readonly name="fe_total" value="'+fe_sum+'"></td>';                     
                    
                    htmls+='</tr>';                                
        		}
        		$("#feAdd").empty();
        		$("#feAdd").html(htmls);
        		
        		/* htmls = '';
        		if(fe_name_arry.length%4==0){
        			htmls='<tr class="tr01">';
        			htmls+='<td class="th01 w6"></td>';
					htmls+='<td class="th01 w8"></td>';
					htmls+='<td class="th01 w8"></td>';
					htmls+='<td class="th01 w8"></td>';
					htmls+='<td class="th01 w8">subtotal</td>';                         
                    htmls+='</tr>';
                    
                    htmls+='<tr class="tr01">';

                    htmls+='<td class="bd01 w6 ta_c"></td>';
                    htmls+='<td class="bd01 w6 ta_c"></td>';
                    htmls+='<td class="bd01 w6 ta_c"></td>';
                    htmls+='<td class="bd01 w6 ta_c"></td>';
                    htmls+='<td class="bd01 w6 ta_c"><input type="text" class="ip04" readonly name="fe_total" value="'+fe_sum+'"></td>';                     
                    
                    htmls+='</tr>';   
        		} 
        		$("#feAdd").append(htmls);*/
        		htmls='';
            
        		var importance = 0;
        		//총괄penilaian기준
        		$.each(data.summativeEvaluationCs, function(index){
        			htmls += '<tr class="tr01">';
                    htmls += '<td class="w6 ta_c"><input type="hidden" name="eval_method_option" value="'+this.eval_method_code+'"/>'+this.eval_method_name+'</td>';
                    htmls += '<td class="w6 ta_c"><input type="hidden" name="eval_domain_code" value="'+this.eval_domain_code+'"/>'+this.eval_domain_name+'</td>';
                    htmls += '<td class="w6_1 ta_c"><input type="text" class="ip03" onfocusout="calImportance(this)" name="importance" value="'+this.importance+'"><span class="sp02">%</span></td>';
                    htmls += '<td class="w8_1 ta_c pd0 free_textarea"><textarea class="tarea01" rows="1" name="eval_method_text" placeholder="Metode dan Kriteria Evaluasi" style="height: 49px;">'+this.eval_method_text.split('<br/>').join("\r\n")+'</textarea></td>';
                    htmls += '<td class="w8_1 ta_c pd0 free_textarea"><textarea class="tarea01" rows="1" name="eval_etc" placeholder="Keterangan" style="height: 49px;">'+this.eval_etc.split('<br/>').join("\r\n")+'</textarea></td>';
                    htmls += '<td class="ta_c w1_1"><input type="hidden" name="se_seq" value="'+this.se_seq+'"/><input type="checkbox" name="eval_chk" class="chk01"></td>';
                    importance += this.importance;
        		});
                $("#AddEvalList").empty();
                $("#AddEvalList").append(htmls);
                $("td[data-name=totalEval]").text(importance+"%");
                htmls='';
                
                //syarat kelulusan
                $.each(data.mpCurriculumGraduationCapabilityList ,function(index){
                	htmls+='<tr class="tr01">';
                    htmls+='<td class=""><input type="hidden" name="fc_seq" value="'+this.fc_seq+'"/>'+this.fc_name+'</td>';
                    htmls+='<td class="pd0 free_textarea"><textarea class="tarea01" rows="1" style="height: 49px;" placeholder="* Silakan masukkan konten ." name="process_result">'+this.process_result.split('<br/>').join("\r\n")+'</textarea></td>';
                    htmls+='<td class="ta_c pd0 free_textarea"><textarea class="tarea03" rows="1" style="height: 49px;" name="teaching_method">'+this.teaching_method.split('<br/>').join("\r\n")+'</textarea></td>';
                    htmls+='<td class="pd0 free_textarea"><textarea class="tarea03" rows="1" style="height: 49px;" name="evaluation_method">'+this.evaluation_method.split('<br/>').join("\r\n")+'</textarea></td>';
                    htmls+='<td class="ta_c w1_1"><input type="checkbox" name="fcchk" class="chk01"></td></tr>';
                });

                $("#mpFinishCapabilityList").empty();
                $("#mpFinishCapabilityList").append(htmls);
                htmls='';
                
                /* 
                //nilai 기준
                var htmls1='<tr class="tr01">';
                var htmls2='<tr class="tr01">';
                var sumGrade = 0;
                $.each(data.gradeStandard, function(index){
                	htmls1+='<td class="th01 w9_1"><input type="hidden" name="grade_standard_code" value="'+this.code+'"/>'+this.code_name+'</td>';
                    htmls2+='<td class="bd01 ta_c"><input type="text" class="ip02_1_1" onfocusout="gradeCalImportance(this);" name="grade_importance" value="'+this.grade_importance+'"><span class="sp02_1">%</span></td>';
                    sumGrade += this.grade_importance;
                });
                htmls1+='<td class="th01 w9_2">계</td></tr>';
                htmls2+='<td class="bd01 ta_c" name="totalGradeImportance">'+sumGrade+'%</td></tr>';
                $("#gradeStandardList").html(htmls1+htmls2); */
                $.each($("textarea"),function(){
                	$(this).height($(this).prop("scrollHeight"));
                });
        	} else if(data.status=="201"){
        		alert("이전 과정계획서가 없습니다.");			        		
        	}
        },
        error: function(xhr, textStatus) {
            document.write(xhr.responseText);
        },
        beforeSend:function() {
            $.blockUI();
        },
        complete:function() {
            $.unblockUI();
        }
    });
}

//교수 가져오기
function curriculumMpfList(){
    $("#mpf_add").empty();
    $("#selectMpfCount").text(0);
    
    var pf = '';
    $("input[name*=pf_user_seq]").each(function(index){
        if(index == 0 )
            pf+= $(this).val();
        else
            pf+=","+$(this).val(); 
    });
    
    
    $.ajax({
        type: "POST",
        url: "${HOME}/ajax/mpf/list",
        data: {
            "pf_seq" : pf,
            "department_code" : $("#department_code").attr("value"),
            "mpf_name" : $("#mpf_name").val()                    
        },
        dataType: "json",
        success: function(data, status) {
            var html = '';            
            $.each(data.pf_list,function(index){
            	var picture_path = "";
            	if(isEmpty(this.picture_path))
            		picture_path = "${IMG}/ph_1.png";
           		else
           			picture_path = "${RES_PATH}"+this.picture_path;
                html += "<tr class='tr01'>";                            
                html += "<td class='ta_c w1' name='pf_name'>"+this.name+"<input type='hidden' name='picture' value='"+picture_path+"'/>";
                html += "<input type='hidden' name='user_seq' value='"+this.user_seq+"'/><input type='hidden' name='pf_position' value='"+this.position+"'/></td>";
                html += "<td class='ta_c w2' onClick='pf_select('"+index+"');' name='pf_id'>"+this.professor_id+"</td>";
                html += "<td class='ta_c w3' name='pf_department'>"+this.department+"</td>";
                html += "<td class='ta_c w4' name='pf_specialty'>"+this.specialty+"</td>";
                html += "<td class='ta_c ls_1 w5' name='pf_tel'>"+this.tel+"</td>";
                html += "<td class='ta_c ls_1 w6' name='email'><a class='mailto_1' href='' target='_top'>"+this.email+"</a></td>";
                html += "<td class='ta_c w8'><input type='hidden' name='pf_level' value='3'/>";
                html += "<label class='chk01'>";
                if($("#select_pf input[value='"+this.user_seq+"']").length!=0)
                	html += "<input type='checkbox' name='chk' checked value='"+index+"'>";
               	else
               		html += "<input type='checkbox' name='chk' value='"+index+"'>";
                html += "<span class='slider round'>pilih</span>";
                html += "</label></td></tr>";                
            });
            
            $("#mpf_add").html(html);
        },
        error: function(xhr, textStatus) {
            //alert("오류가 발생했습니다.");
            document.write(xhr.responseText);
        }
    }); 
}

//팝업에서 pilih된 교수 삭제 X 클릭
function delMpf(user_seq, index, obj){
    $(obj).parent("div").remove();
    //$("#select_pf div[name='user_seq_"+user_seq+"']").remove();
    $("#mpf_add input[name='chk']").eq(index).prop("checked",false);
    selectMpfCount();
}

//pilih된 교수 카운트
function selectMpfCount(){
    var cnt = $("#select_pf .a_mp").length;
    $("#selectMpfCount").text(cnt);
}

//pilih된 교수 registrasi
function popupMpfSubmit(){
    var check_len= $("#select_pf div.a_mp").length;
        
    if(check_len == 0){
        alert("registrasi할 교수를 pilih하세요");
        return;
    }
    
    var htmls = "";
    $.each($("#select_pf div.a_mp"), function(){
    	htmls+='<tr class="tr01">';
        htmls+='<td class="ta_c">교수<input type="hidden" name="pf_user_seq" value="'+$(this).find("input[name=user_seq]").val()+'"/></td>';
        htmls+='<td class="ta_c">'+$(this).find("input[name=pf_name]").val()+'<input type="hidden" name="pf_level" value="'+$(this).find("input[name=pf_level]").val()+'"/></td>';
        htmls+='<td class="ta_c">'+$(this).find("input[name=pf_department]").val()+'</td>';
        htmls+='<td class="ta_c">'+$(this).find("input[name=pf_specialty]").val()+'</td>';
        htmls+='<td class="ta_c ls_1">'+$(this).find("input[name=tel]").val()+'</td>';
        htmls+='<td class="ta_c ls_1"><a class="mailto_1" href="#" target="_top">'+$(this).find("input[name=email]").val()+'</a></td>';
        htmls+='<td class="ta_c w1_1"><input type="checkbox" class="chk01" name="pfchk" value="'+$(this).find("input[name=user_seq]").val()+'"></td></tr>';
    });
    $("#addPfList").append(htmls);
    
    $("#mpf_add").empty();
    $("#select_pf").empty();
    $("#m_pop1").hide();
    $("#selectMpfCount").text(0);
}

//교수 hapus yang dipilih
function selectPfDel(){            	            	
	if($("input[name='pfchk']:checked").length == 0){
		alert("삭제할 교수를 pilih하세요");
		return false;
	}
	
	if(!confirm("삭제하시겠습니까?"))
		return false;
	
	$.each($("input[name='pfchk']:checked"), function(){
		$(this).closest("tr").remove();
	});            	
}

//tambahkan된 syarat kelulusan hapus yang dipilih
function selectGCDel(){                             
    if($("#mpFinishCapabilityList input[name='fcchk']:checked").length == 0){
        alert("삭제할 syarat kelulusan을 pilih하세요");
        return false;
    }
    
    if(!confirm("삭제하시겠습니까?"))
        return false;
    
    $.each($("#mpFinishCapabilityList input[name='fcchk']:checked"), function(){
        $(this).closest("tr").remove();
    });             
}

//syarat kelulusan pilih된거 registrasi
function selectGcSubmit(){
	var check_len= $("#gcAddList input[name='chk']:checked").length;
    
    if(check_len == 0){
        alert("registrasi할 syarat kelulusan을 pilih하세요");
        return;
    }
    
    var htmls = "";
    $.each($("#gcAddList input[name='chk']:checked"), function(){
    	
    	htmls+='<tr class="tr01">';
        htmls+='<td class=""><input type="hidden" name="fc_seq" value="'+$(this).closest("tr").find("input[name=popup_fc_seq]").val()+'"/>'+$(this).closest("tr").find("td[name=fc_name]").text()+'</td>';
        htmls+='<td class="pd0 free_textarea"><textarea class="tarea01" rows="1" style="height: 49px;" placeholder="* Silakan masukkan konten ." name="process_result"></textarea></td>';
        htmls+='<td class="ta_c pd0 free_textarea"><textarea class="tarea03" rows="1" style="height: 49px;" name="teaching_method"></textarea></td>';
        htmls+='<td class="pd0 free_textarea"><textarea class="tarea03" rows="1" style="height: 49px;" name="evaluation_method"></textarea></td>';
        htmls+='<td class="ta_c w1_1"><input type="checkbox" name="fcchk" class="chk01"></td></tr>';                    
    });
    
    $("#mpFinishCapabilityList").append(htmls);
    
    $("#gcAddList").empty();
    $("#gcSelectItem").empty();
    $("#m_pop3").hide();
    $("#gcSelectCount").text(0);
}

//syarat kelulusan pilih된거 삭제 X 버튼
function delFc(index, obj){
    $(obj).parent("div").remove();
    $("#gcAddList input[name='chk']").eq(index).prop("checked", false);
}

//pilih된 syarat kelulusan 카운트
function selectGCCount(){
    var cnt = $("#gcSelectItem .a_mp").length;
    $("#gcSelectCount").text(cnt);
}


//hari dan tanggal로 total peiode (minggu) 계산
function dateDiff(){
	var start_date = $("input[name=curr_start_date]").val();
	var end_date = $("input[name=curr_end_date]").val();
	
	//$(element).data('DateTimePicker').minDate(end_date)
	$('input[name=curr_end_date]').datetimepicker({
		minDate: start_date
	});
	 
	if(isEmpty(start_date) || isEmpty(end_date)){
		return false;
	}
	
	var arr1 = start_date.split('-');
	var arr2 = end_date.split('-');
	
	var date1 = new Date(arr1[0],arr1[1],arr1[2]);
	var date2 = new Date(arr2[0],arr2[1],arr2[2]);
	
	var diff=date2-date1;
	var day = diff / (1000*60*60*24);
	//var month = day * 30;
	//var year = month * 12;

	var week = parseInt(day/7);

	if(day%7 > 0)
		week+=1;
	
	var htmls = "";
	htmls+= '<span class="uoption"><input type="hidden" value="'+(week-1)+'"/> '+(week-1)+'주</span>';
	htmls+= '<span class="uoption"><input type="hidden" value="'+week+'"/> '+week+'주</span>';
	htmls+= '<span class="uoption"><input type="hidden" value="'+(week+1)+'"/> '+(week+1)+'주</span>';
	$("#weekList").html(htmls);
	 
	$("#weekList").find("input[value='"+week+"']").closest("span").click(); 

	
	if(!isEmpty($("input[name=total_period_cnt]").val())){
		//주당평균waktu 구하기
        var period_avg = ($("input[name=total_period_cnt]").val()/week)+"";
        if(period_avg.indexOf(".") > 0)
            period_avg = period_avg.substring(0,period_avg.indexOf(".")+2);
        else if(week==0)
        	period_avg = 0;
        $("input[name=period_avg]").val(period_avg);    
    }
    
	
}

//Tambahkan kriteria evaluasi
function addEval(){
	$("button[name=evalDelete]").hide();
	$("button[name=evalAdd]").hide();
	var evalCodeList = "";
	$.ajax({
        type: "POST",
        url: "${HOME}/ajax/pf/lesson/curriculum/summativeEvaluationCodeList",
        data: {
            "l_se_code" : ""
        },
        dataType: "json",
        success: function(data, status) {
                        
            $.each(data.summativeEvaluationList,function(index){
            	evalCodeList += '<span class="uoption"><input type="hidden" value="'+this.se_code+'">'+this.se_name+'</span>';       
            });
            var htmls = "";
            htmls += '<tr class="tr01">';
            htmls += '<td class="">';
            htmls += '<div class="wrap2_lms_uselectbox">';
            htmls += '<div class="uselectbox" name="eval_method_code">';
            htmls += '<span class="uselected" name="eval_method_option"><input type="hidden" value="">pilih</span><span class="uarrow">▼</span>';
            htmls += '<div class="uoptions" name="eval_method_item">';
            htmls += '<span class="uoption"><input type="hidden" value="">pilih</span>';
            htmls += evalCodeList;
            htmls += '</div>';
            htmls += '</div>';
            htmls += '</div>';
            htmls += '</td>';
            htmls += '<td class="">';
            htmls += '<div class="wrap2_lms_uselectbox">';
            htmls += '<div class="uselectbox" name="eval_domain_code">';
            htmls += '<span class="uselected" name="eval_domain_option"><input type="hidden" value="">pilih</span><span class="uarrow">▼</span>';
            htmls += '<div class="uoptions" name="eval_domain_item">';
            htmls += '</div>';
            htmls += '</div>';
            htmls += '</div>';
            htmls += '</td>';
            htmls += '<td class="w6_1 ta_c"><input type="text" class="ip03" onfocusout="calImportance(this)" name="temp_importance" value=""><span class="sp02">%</span></td>';
            htmls += '<td colspan="3" class="w8_1 ta_c pd0 btntd">';
            htmls += '<button class="btn01" onClick="eval_commit(this);">simpan</button>';
            htmls += '<button class="btn02" onClick="eval_delete(this);">batal</button>';              
            htmls += '</td></tr>';
            $("#AddEvalList").append(htmls);
            
        },
        error: function(xhr, textStatus) {
            //alert("오류가 발생했습니다.");
            document.write(xhr.responseText);
        }                            
    });        
}

//Tambahkan kriteria evaluasi - simpan
function eval_commit(obj){
	var htmls = "";
	var eval_method_code = $(obj).closest("tr").find("span[name=eval_method_option] input").val();
	var eval_method_name = $(obj).closest("tr").find("span[name=eval_method_option]").text();
	var eval_domain_code = $(obj).closest("tr").find("span[name=eval_domain_option] input").val();
	var eval_domain_name = $(obj).closest("tr").find("span[name=eval_domain_option]").text();
	var importance = $(obj).closest("tr").find("input[name=temp_importance]").val();
	
	//Metode evaluasi pilih 체크
	if(isEmpty(eval_method_code)){
		alert("Metode evaluasi을 pilih하세요");return false;
	}
	
	//penilaianArea pilih 체크
	if(isEmpty(eval_domain_code)){
        alert("penilaianArea을 pilih하세요");return false;
    }
           	
	
	htmls += '<tr class="tr01">';
	htmls += '<td class="w6 ta_c"><input type="hidden" name="eval_method_option" value="'+eval_method_code+'"/>'+eval_method_name+'</td>';
	htmls += '<td class="w6 ta_c"><input type="hidden" name="eval_domain_code" value="'+eval_domain_code+'"/>'+eval_domain_name+'</td>';
	htmls += '<td class="w6_1 ta_c"><input type="text" class="ip03" onfocusout="calImportance(this)" name="importance" value="'+importance+'"><span class="sp02">%</span></td>';
	htmls += '<td class="w8_1 ta_c pd0 free_textarea"><textarea class="tarea01" rows="1" name="eval_method_text" placeholder="Metode dan Kriteria Evaluasi" style="height: 49px;"></textarea></td>';
	htmls += '<td class="w8_1 ta_c pd0 free_textarea"><textarea class="tarea01" rows="1" name="eval_etc" placeholder="Keterangan" style="height: 49px;"></textarea></td>';
	htmls += '<td class="ta_c w1_1"><input type="hidden" name="se_seq" value=""/><input type="checkbox" name="eval_chk" class="chk01"></td>';
	
	
	eval_delete(obj);
	$("#AddEvalList").append(htmls);
    $("button[name=evalDelete]").show();
    $("button[name=evalAdd]").show();
}

//penilaian기준 simpan batal
function eval_delete(obj){
	$(obj).closest("tr").remove();
    $("button[name=evalDelete]").show();
    $("button[name=evalAdd]").show();
}

//penilaian기준 hapus yang dipilih
function select_eval_delete(){
	if($("input[name=eval_chk]:checked").length == 0){
		alert("삭제할 penilaian기준을 pilih하세요");
		return false;            		
	}
	
	if(!confirm("삭제하시겠습니까?"))
		return false;
		
	$.each($("input[name=eval_chk]:checked"), function(){
		$(this).closest("tr").remove();
	});

	var importance_sum = 0;
	$.each($("input[name=importance]"), function(){
    	if(!isNaN($(this).val()))
    	   importance_sum += $(this).val()*1;
    });
	
   	$("td[data-name=totalEval]").text(importance_sum+"%");
}

//penting 계산
function calImportance(obj){
	if(obj.value.length==0){       
        return;
    }
    if(isNaN(obj.value)){
        $(obj).val("");
        $(obj).focus();
        return false;
        //alert("숫자만 입력하세요");
    }else{
        $(obj).val($(obj).val().replace(/ /g,""));
    }
    
    var importance_sum = 0;
    
    //penting 100 초과인지 체크
    //simpan버튼 처리된 penting
    $.each($("input[name=importance]"), function(){
    	if(!isNaN($(this).val()))
    	   importance_sum += $(this).val()*1;
    });
    //simpan버튼 처리 안된 penting
    $.each($("input[name=temp_importance]"), function(){
    	if(!isNaN($(this).val()))
    		importance_sum += $(this).val()*1;
    });
    
    if(importance_sum > 100){
        alert("penting의 총 합이 100% 를 초과하였습니다.");
        $(obj).val("");
        $(obj).focus();
    }else{
    	$("td[data-name=totalEval]").text(importance_sum+"%");
    }
}

//penting 계산
function gradeCalImportance(obj){
    if(obj.value.length==0){       
        return;
    }
    if(isNaN(obj.value)){
        $(obj).val("");
        $(obj).focus();
        return false;
        //alert("숫자만 입력하세요");
    }else{
        $(obj).val($(obj).val().replace(/ /g,""));
    }
    
    var importance_sum = 0;
    
    //penting 100 초과인지 체크
    //simpan버튼 처리된 penting
    $.each($("input[name=grade_importance]"), function(){
        if(!isNaN($(this).val()))
           importance_sum += $(this).val()*1;
    });
    
    if(importance_sum > 100){
        alert("nilai 비율의 합이 100% 를 초과하였습니다.");
        $(obj).val("");
        $(obj).focus();
        $("td[name=totalGradeImportance]").text((importance_sum-$(this).val()*1)+"%");
    }else{
    	$("td[name=totalGradeImportance]").text(importance_sum+"%");
    }
}

//syarat kelulusan 가져오기
function GraduationCapabilityList(){
    $("#gcAddList").empty();
    $("#gcSelectItem").empty();
    $("#gcSelectCount").text(0);
    
    var fc_seq = '';
    $("#mpFinishCapabilityList input[name=fc_seq]").each(function(index){
        if(index == 0 )
        	fc_seq+= $(this).val();
        else
        	fc_seq+=","+$(this).val(); 
    });
    
    
    $.ajax({
        type: "POST",
        url: "${HOME}/ajax/pf/lesson/graduationCapabilityList",
        data: {  
        	"fc_seq" : fc_seq
        },
        dataType: "json",
        success: function(data, status) {
            var html = '';            
            $.each(data.graduationCapabilityList, function(index){
            	var fc_code = "";
            	if(!isEmpty(this.fc_code))
            		fc_code = "("+this.fc_code+")";
                html += '<tr class="tr01">';
                html += '<td class="w1 ta_c" name="fc_name"><input type="hidden" name="popup_fc_seq" value="'+this.fc_seq+'"/>'+fc_code+this.fc_name+'</td>';
                html += '<td class="ta_c w6">';
                html += '<label class="chk01">';
                html += '<input type="checkbox" name="chk" value="'+index+'">';
                html += '<span class="slider round">pilih</span>';
                html += '</label></td></tr>';                
            });
            
            $("#gcAddList").html(html);
        },
        error: function(xhr, textStatus) {
            //alert("오류가 발생했습니다.");
            document.write(xhr.responseText);
        }
    }); 
}

function curriculumSubmit(){
	if(!confirm("simpan하시겠습니까?"))
		return false;
	
	if(!isEmpty($("input[name=unlecture_period]").val())){
    	if($("input[name=unlecture_period]").val() != $("input[name=lm_total]").val()){
    		alert("tidak ada perkuliahan waktu과 tidak ada perkuliahan 상세의 총 waktu이 다릅니다.");
    		return;
    	}
	}
		
	
	$.each($("textarea"),function(){
		$(this).val($(this).val().replace(/\n/g, "<br/>"));	
	});
	
	var lm_array = new Array();
	var fe_array = new Array();
	
	$.each($("#lessonMethodList input.lm_text"), function(index){            	
		var lmInfo = new Object();
		lmInfo.lm_code=$(this).attr("name");
		lmInfo.period=$(this).val();
		lm_array.push(lmInfo);
	});
	
	$.each($("#feAdd input.fe_text"), function(index){            	
		var feInfo = new Object();
		feInfo.fe_code=$(this).attr("name");
		feInfo.period=$(this).val();
		fe_array.push(feInfo);
	});
	

	var lmList = new Object();
	var feList = new Object();
	
	lmList.list = lm_array;
	feList.list = fe_array;

	$("input[name=lm_json]").val(JSON.stringify(lmList));
	$("input[name=fe_json]").val(JSON.stringify(feList));
	
	$("#curr_week input").attr("name", "curr_week");
	
	$("#curriculumForm").ajaxForm({
        type: "POST",
        url: "${HOME}/ajax/pf/lesson/curriculum/modify",
        dataType: "json",
        success: function(data, status){
            if (data.status == "200") {
                alert("simpan이 완료되었습니다.");   
                $("input[name=lm_json]").val("");
            	$("input[name=fe_json]").val("");
                getCurriculum();
            } else {
                alert("실패하였습니다.");
                $.unblockUI();                          
            }                        
        },
        error: function(xhr, textStatus) {
            document.write(xhr.responseText); 
            $.unblockUI();                      
        },beforeSend:function() {
            $.blockUI();                        
        },complete:function() {
            $.unblockUI();                      
        }                       
    });     
    $("#curriculumForm").submit();
}

function detailLesson(code, name){
	
	//tidak ada perkuliahan 상세 없을경우 0으로 들어온다.
	if(code=="0")
		return;
	
	$.ajax({
        type: "POST",
        url: "${HOME}/ajax/pf/lesson/lessonMethodSujectList",
        data: {
            "code" : code,                   
        },
        dataType: "json",
        success: function(data, status) {
            var html = '';
            var lesson_subject = "";
            $.each(data.lessonMethodSujectList,function(index){
            	lesson_subject = this.lesson_subject;
            	if(isEmpty(lesson_subject))
            		lesson_subject = "수업명 미registrasi";
            
                html += '<div class="con_s2">';
                html += '<span class="tt1" title="'+this.lesson_subject+'">'+lesson_subject+'</span>';
                html += '<div class="a_mp">';      
                html += '<span class="pt01"><img src="" alt="사진" class="pt_img"></span><span class="ssp1">'+this.name+'('+this.specialty+')</span>';
                html += '</div></div>';                
            });
			$("#lessonMethodTitle").text(name);
            $("#lessonDetailAdd").empty();
            $("#lessonDetailAdd").append(html);
            $("#m_pop4").show();
        },
        error: function(xhr, textStatus) {
            //alert("오류가 발생했습니다.");
            document.write(xhr.responseText);
        }
    }); 
}

//Durasi belajar dan manajemen waktu Mengimpor Isi Rencana Pelajaran
function getLessonData(){            	
	$.ajax({
        type: "POST",
        url: "${HOME}/ajax/pf/lesson/curriculum/lessonPlanInfo",
        data: {             
        },
        dataType: "json",
        success: function(data, status) {
            var htmls = '';
            
            $("input[name=lecture_period]").val(data.periodInfo.lecture_cnt);
    		$("input[name=unlecture_period]").val(data.periodInfo.unlecture_cnt);
            $("input[name=period_cnt]").val(data.periodInfo.period_seq);
            $("input[name=total_period_cnt]").val(data.periodInfo.period_cnt);

            //주당평균waktu 구하기
            dateDiff();

    		$("#lessonMethodList").empty();                     	
           	var lessonMethod_code_arry = new Array();
    		var lessonMethod_name_arry = new Array();
    		var lessonMethod_cnt_arry = new Array();
    		var lm_sum = 0;
    		
    		$.each(data.lessonMethodInfo, function(index){
    			lessonMethod_code_arry[index] = this.code;
    			lessonMethod_name_arry[index] = this.code_name;
    			lessonMethod_cnt_arry[index] = this.lp_cnt;
    			lm_sum += this.lp_cnt;
    		});
    		
    		var for_int = 0;
    		if(lessonMethod_code_arry.length <= 4)
    			for_int = 1;
    		else if((lessonMethod_code_arry.length)%4==0)
    			for_int = parseInt(lessonMethod_code_arry.length/4)+1;
    		else
                   for_int = parseInt((lessonMethod_code_arry.length+1)/4)+1;
					        		
    		for(var i=0;i<for_int*4;i+=4){
    			htmls+='<tr class="tr01">';
    			if(i==0) htmls+= '<td rowspan="'+(for_int*2+2)+'" class="th01 bd01 bd02 w6">tidak ada perkuliahan<br>상세</td>';
    			if(lessonMethod_name_arry.length > i) htmls+= '<td class="th01 w8">'+lessonMethod_name_arry[i]+'</td>';
                   else htmls+= '<td class="th01 w8"></td>';
                   if(lessonMethod_name_arry.length > (i+1)) htmls+= '<td class="th01 w8">'+lessonMethod_name_arry[i+1]+'</td>';
                   else htmls+= '<td class="th01 w8"></td>';
                   if(lessonMethod_name_arry.length > (i+2)) htmls+= '<td class="th01 w8">'+lessonMethod_name_arry[i+2]+'</td>';
                   else htmls+= '<td class="th01 w8"></td>';
                   if(lessonMethod_name_arry.length > (i+3)) htmls+= '<td class="th01 w8">'+lessonMethod_name_arry[i+3]+'</td>';
                   else htmls+= '<td class="th01 w8">subtotal</td>';
                   
                   
                   
                htmls+= '</tr><tr class="tr01">';
                   if(lessonMethod_code_arry.length > i){                                	
                       htmls+= '<td class="w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" name="'+lessonMethod_code_arry[i]+'" class="ip04 lm_text" value="'+lessonMethod_cnt_arry[i]+'"></td>';
                   }
                   else{
                       htmls+= '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="-"></td>';
                   }                                
                   if(lessonMethod_code_arry.length > (i+1)){
                   	 htmls+= '<td class="w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" name="'+lessonMethod_code_arry[i+1]+'" class="ip04 lm_text" value="'+lessonMethod_cnt_arry[i+1]+'"></td>';
                   }
                   else
                       htmls+= '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="-"></td>';
                   if(lessonMethod_code_arry.length > (i+2)){
                   	 htmls+= '<td class="w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" name="'+lessonMethod_code_arry[i+2]+'" class="ip04 lm_text" value="'+lessonMethod_cnt_arry[i+2]+'"></td>';
                   }
                   else
                       htmls+= '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="-"></td>';
                   if(lessonMethod_code_arry.length > (i+3)){
                   	 htmls+= '<td class="w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" name="'+lessonMethod_code_arry[i+3]+'" class="ip04 lm_text" value="'+lessonMethod_cnt_arry[i+3]+'"></td>';
                   }
                   else
                       htmls+= '<td class="w6 ta_c"><input type="text" readonly name="lm_total" class="ip04" value="'+lm_sum+'"></td>';
                htmls+= '</tr>';
    		}
    		$("#lessonMethodList").append(htmls);
            /* 
    		if(lessonMethod_code_arry.length%4==0){
    			htmls='<tr class="tr01">';
                htmls+= '<td class="th01 w8"></td>';
                htmls+= '<td class="th01 w8"></td>';
                htmls+= '<td class="th01 w8"></td>'; 
                htmls+= '<td class="th01 w8">subtotal</td>';                                
                htmls+= '</tr><tr class="tr01">';
                htmls+= '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="-"></td>';
                htmls+= '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="-"></td>';
				htmls+= '<td class="w6 ta_c"><input type="text" readonly class="ip04" value="-"></td>';
                htmls+= '<td class="w6 ta_c"><input type="text" readonly name="lm_total" class="ip04" value="'+lm_sum+'"></td>';
                htmls+= '</tr>';
    		}
    		$("#lessonMethodList").append(htmls);
    		 */
    		//형성penilaian 리스트        		
    		htmls = '';
    		var fe_sum = 0;
    		var fe_name_arry = new Array();
    		var fe_cnt_arry = new Array();
    		var fe_code_array = new Array();
    		
    		$.each(data.feInfo, function(index){
    			fe_code_array[index] = this.code;
    			fe_name_arry[index] = this.code_name;
    			fe_cnt_arry[index] = this.fe_cnt;
    			fe_sum += fe_cnt_arry[index]*1;
    		});
    		
    		for_int = 0;
    		if(fe_name_arry.length <= 5)
    			for_int = 1;
    		else if((fe_name_arry.length)%5==0)
    			for_int = parseInt(fe_name_arry.length/5)+1;
    		else
    			for_int = parseInt((fe_name_arry.length+1)/5)+1;


    		for(var i=0;i<for_int*5;i+=5){
    			htmls+='<tr class="tr01">';
    			if(fe_name_arry.length > i) htmls+='<td class="th01 w6">'+fe_name_arry[i]+'</td>';
                else htmls+='<td class="th01 w6"></td>';

    			if(fe_name_arry.length > (i+1)) htmls+='<td class="th01 w8">'+fe_name_arry[i+1]+'</td>';
			    else htmls+='<td class="th01 w8"></td>';
				    
			    if(fe_name_arry.length > (i+2)) htmls+='<td class="th01 w8">'+fe_name_arry[i+2]+'</td>';
                else htmls+='<td class="th01 w8"></td>';
                    
				if(fe_name_arry.length > (i+3)) htmls+='<td class="th01 w8">'+fe_name_arry[i+3]+'</td>';
				else htmls+='<td class="th01 w8"></td>';
				    
			    if(fe_name_arry.length > (i+4)) htmls+='<td class="th01 w8">'+fe_name_arry[i+4]+'</td>';
                else htmls+='<td class="th01 w8">subtotal</td>';                         
                htmls+='</tr>';
                
                htmls+='<tr class="tr01">';
                if(fe_name_arry.length > i) htmls+='<td class="bd01 w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" class="ip04 fe_text" name="'+fe_code_array[i]+'" value="'+fe_cnt_arry[i]+'"></td>';
                else htmls+='<td class="bd01 w6 ta_c"></td>';

                if(fe_name_arry.length > (i+1)) htmls+='<td class="bd01 w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" class="ip04 fe_text" name="'+fe_code_array[i+1]+'" value="'+fe_cnt_arry[i+1]+'"></td>';
                else htmls+='<td class="bd01 w6 ta_c"></td>';
                    
                if(fe_name_arry.length > (i+2)) htmls+='<td class="bd01 w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" class="ip04 fe_text" name="'+fe_code_array[i+2]+'" value="'+fe_cnt_arry[i+2]+'"></td>';
                else htmls+='<td class="bd01 w6 ta_c"></td>';
                    
                if(fe_name_arry.length > (i+3)) htmls+='<td class="bd01 w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" class="ip04 fe_text" name="'+fe_code_array[i+3]+'" value="'+fe_cnt_arry[i+3]+'"></td>';
                else htmls+='<td class="bd01 w6 ta_c"></td>';
                    
                if(fe_name_arry.length > (i+4)) htmls+='<td class="bd01 w6 ta_c"><input type="text" onkeyup="onlyNumCheck(this);" readonly class="ip04 fe_text" name="'+fe_code_array[i+4]+'" value="'+fe_cnt_arry[i+4]+'"></td>';
                else htmls+='<td class="bd01 w6 ta_c"><input type="text" class="ip04" readonly name="fe_total" value="'+fe_sum+'"></td>';                     
                
                htmls+='</tr>';                                
    		}
    		$("#feAdd").empty();
    		$("#feAdd").append(htmls);
    		/* 
    		if(fe_name_arry.length%4==0){
    			htmls='<tr class="tr01">';
    			htmls+='<td class="th01 w6"></td>';
				htmls+='<td class="th01 w8"></td>';
				htmls+='<td class="th01 w8"></td>';
				htmls+='<td class="th01 w8"></td>';
				htmls+='<td class="th01 w8">subtotal</td>';                         
                htmls+='</tr>';
                
                htmls+='<tr class="tr01">';

                htmls+='<td class="bd01 w6 ta_c"></td>';
                htmls+='<td class="bd01 w6 ta_c"></td>';
                htmls+='<td class="bd01 w6 ta_c"></td>';
                htmls+='<td class="bd01 w6 ta_c"></td>';
                htmls+='<td class="bd01 w6 ta_c"><input type="text" class="ip04" readonly name="fe_total" value="'+fe_sum+'"></td>';                     
                
                htmls+='</tr>';   
    		}
    		htmls='';
    		$("#feAdd").append(htmls); */
        },
        error: function(xhr, textStatus) {
            //alert("오류가 발생했습니다.");
            document.write(xhr.responseText);
        }
    }); 
}

function currConfirm(confirmFlag){
	if(confirmFlag == "Y"){
		if(!confirm("해당 교육과정을 확정하시겠습니까?\n확정시 책임/부Dosen Penanggung jawab가 과정계획서, waktu표, 단원을 perbaiki할 수 없습니다."))
			return;	
	} else{
		if("${acaState.aca_curr_confirm_flag}" == "Y"){
			alert("해당 교육과정은 학사에 배정확정되었습니다.\n확정 batal할 수 없습니다.");
			return;
		}
			
		if(!confirm("확정 batal 하시겠습니까?"))
			return;	
	}
	$.ajax({
        type: "POST",
        url: "${HOME}/ajax/admin/lesson/curriculum/confirm",
        data: {
            "confirm_flag" : confirmFlag,                   
        },
        dataType: "json",
        success: function(data, status) {
            if(data.status=="200"){
            	if(confirmFlag == "Y"){
            		alert("확정 완료 되었습니다.");
            	}else{
            		alert("확정 batal 되었습니다.");	
            	}    
            	window.location.reload();
            }else{
            	if(confirmFlag == "Y"){
            		alert("확어 실패하였습니다.");
            	}else{
            		alert("확정 batal 실패하였습니다.");	
            	}
            }
        },
        error: function(xhr, textStatus) {
            //alert("오류가 발생했습니다.");
            document.write(xhr.responseText);
        }
    });
	
}
</script>
</head>
<body>

<!-- s_main_con -->
<div class="main_con adm_ccschd">
<!-- s_메뉴 접기 버튼 -->	
<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
<!-- e_메뉴 접기 버튼 -->	
	
<!-- s_tt_wrap -->                
<div class="tt_wrap">
			    <h3 class="am_tt">
			    	<span class="tt" data-name="curr_name"></span>
			    	<span class="tt_s" data-name="academic_name"></span>
			    </h3>                    
			</div>
<!-- e_tt_wrap -->
    
<!-- s_tab_wrap_cc --> 
<div class="tab_wrap_cc">
<!-- s_해당 탭 페이지 class에 active tambahkan --> 
	<button class="tab01 tablinks active" onclick="pageMoveCurriculumView('${S_CURRICULUM_SEQ}');">rencan kurikulum</button>	         
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/schedule'">waktu표manajemen</button>	
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/unit'">단원manajemen</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/lessonPlanCs'">penambahan bab pembelajaran</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/survey'">만족도 조사</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/currAssignMent'">종합 과제</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/soosiGrade'">수시 성적</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/grade'">종합 성적</button>	
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/report'">운영보고서</button>
<!-- e_해당 탭 페이지 class에 active tambahkan -->
</div>
<!-- e_tab_wrap_cc --> 

<!-- s_tabcontent -->
        <div id="tab001" class="tabcontent tabcontent1">   
            <div class="stab_title">
                <!-- s_단원manajemen 탭  생성될 때, class에 on tambahkan -->  
                <button onclick="getPreCurriculum($('td[name=curr_code]').text());" class="btn_tt on">Impor rencana kurikulum yang dibuat sebelumnya</button>
                <button onclick="$('.btn_l').click();" name="zoom" class="btn_tt">memperlebar/memperluas</button>
            </div>       
            
            <form id="curriculumForm" onSubmit="return false;">            
            <input type="hidden" name="lm_json">
            <input type="hidden" name="fe_json">
            <!-- s_scroll_wrap --> 
            <div class="scroll_wrap">

                <div class="t_title">dasar dasar kurikulum</div>   
                <table class="tab_table ttb1">
                    <tbody>
                        <tr class="tr01">
                            <td class="th01 w1">교육과정<br>코드</td>
                            <td class="w2" name="curr_code"></td>
                            <td class="th01 w1">교육<br>과정명</td>
                            <td colspan="3" class="w3" name="curr_name"></td>
                        </tr>
                        <tr class="tr01">
                            <td class="th01 w1">개설<br>semester</td>
                            <td class="w2" name="aca_name"></td>
                            <td class="th01 w1">학사<br>체계</td>
                            <td class="w2" name="aca_system_name"></td>
                            <td class="th01 w1">lecture실</td>
                            <td class="ip_td w2"><input type="text" class="ip_tt" name="classroom" placeholder="* lecture실 입력"></td>
                        </tr>
                        <tr class="tr01">
                            <td class="th01 w1">이수<br>구분</td>
                            <td class="w2" name="complete_name"></td>
                            <td class="th01 w1">manajemen<br>구분</td>
                            <td class="w2" name="administer_name"></td>
                            <td class="th01 w1">nilai</td>
                            <td class="w2" name="grade"></td>
                        </tr>
                        <tr class="tr01">
                            <td class="th01 bd01 w1">Target</td>
                            <td class="bd01 w2" name="target_name"></td>
                            <td class="th01 bd01 w1">pelatihan</td>
                            <td class="bd01 w2"></td>
                            <td class="th01 bd01 w1"></td>
                            <td class="bd01 w2">-</td>
                        </tr>
                    </tbody>
                </table> 

                <div class="t_title">Informasi Instruktur</div>
                <button class="btn_tt1" onClick="selectPfDel();" >hapus yang dipilih</button>
                <button class="btn_tt2 open1">교수 tambahkan</button>
                <table class="tab_table ttb1">
                    <thead>
	                    <tr class="tr01">
	                        <td class="th01 w1" style="width:9%;">posisi </td>
	                        <td class="th01 w4" style="width:10%;">nama </td>
	                        <td class="th01 w4" style="width:14%;">departement</td>
	                        <td class="th01 w4" style="width:15%;">jurusan khusus</td>
	                        <td class="th01 w4" style="width:16%;">kontak</td>
	                        <td class="th01 w5" style="width:30%;">E-mail</td>
	                        <td class="th01 w1_1" style="width:7%;">manajemen</td>
	                    </tr>                    
                    </thead>
                    <tbody id="addPfList">
                        
                    </tbody>
                </table> 

                <div class="t_title">Durasi belajar dan manajemen waktu</div>
                <button class="btn_tt2" onClick="getLessonData();" >Mengimpor Isi Rencana Pelajaran</button>
                <table class="tab_table ttb1 mg_1">
                    <tbody>
                        <tr class="tr01">
                            <td class="th01 w6">periode</td>
                            <td colspan="3" class="w7">
                                <input type="text" class="dateyearpicker-input_1 ip_date" onchange="dateDiff();" placeholder="시작하는 날" name="curr_start_date">
                                <span class="sign">~</span>
                                <input type="text" class="dateyearpicker-input_1 ip_date" onchange="dateDiff();" placeholder="끝나는 날" name="curr_end_date">
                            </td>
                            <td class="th01 w6">total peiode (minggu)</td>
                            <td class="w6">
                                <div class="wrap1_lms_uselectbox">
                                    <div class="uselectbox">
                                        <span class="uselected" id="curr_week"><input type="hidden" name="curr_week" value=""></span>
                                        <span class="uarrow">▼</span>
                                        <div class="uoptions"" id="weekList">
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="tr01">
                            <td class="th01 w6">Jam rata-rata per minggu</td>
                            <td class="w6 pd1">
                                <input type="text" class="ip02" value="" readonly name="period_avg">
                                <span class="sp02">waktu</span>
                                <span class="sign2">/</span>
                                <input type="text" class="ip01" readonly value="1"><span class="sp02">주</span>
                            </td>
                            <td class="th01 w6">waktu</td>
                            <td class="w6"><input type="text" class="ip03" value="" name="period_cnt" onkeyup="onlyNumCheck(this);"><span class="sp02">waktu</span></td>
                            <td class="th01 w6">durasi pembelajaran</td>
                            <td class="w6">
                                <input type="text" class="ip03 period" value="" name="etc_period" onkeyup="onlyNumCheck(this);"><span class="sp02">waktu</span>
                            </td>
                        </tr>
                        <tr class="tr01">
                            <td class="th01 w6">lecture</td>
                            <td class="w6">
                                <input type="text" class="ip03 period" value="" name="lecture_period" onkeyup="onlyNumCheck(this);"><span class="sp02">waktu</span>
                            </td>
                            <td class="th01 w6">tidak ada perkuliahan</td>
                            <td class="w6">
                                <input type="text" class="ip03 period" value="" name="unlecture_period" onkeyup="onlyNumCheck(this);"><span class="sp02">waktu</span>
                            </td>
                            <td class="th01 w6">Total jumlah jam</td>
                            <td class="w6">
                                <input type="text" class="ip03" value="" readonly name="total_period_cnt"><span class="sp02">waktu</span>
                            </td>
                        </tr>
                    </tbody> 
                </table>                   
                <table class="tab_table ttb1 b_2">
                    <tbody id="lessonMethodList">
                        
                    </tbody>
                </table>

                <div class="t_title">Apakah evaluasi formasi dilakukan dan berapakali</div>
                <table class="tab_table ttb1">
                    <tbody id="feAdd">
                        
                    </tbody>
                </table>

                <div class="t_title">총괄 penilaian 기준</div>
                <button class="btn_tt1" name="evalDelete" onClick="select_eval_delete();">hapus yang dipilih</button>
                <button class="btn_tt2 open2" name="evalAdd" onClick="addEval();">Tambahkan kriteria evaluasi</button>
                <table class="tab_table ttb1">
                    <thead>
                        <tr class="tr01">
                            <td class="th01 w6">Metode evaluasi</td>
                            <td class="th01 w8">penilaianArea</td>
                            <td class="th01 w6_1">penting</td>
                            <td class="th01 w8_1">Metode dan Kriteria Evaluasi</td>
                            <td class="th01 w8_1">Keterangan</td>
                            <td class="th01 w1_1">manajemen</td>
                        </tr>
                    </thead>
                    <tbody id="AddEvalList">                    
                        
                    </tbody>
                        <tr class="tr01">
                            <td colspan="2" class="th01 bd01 w6 ta_c">Total</td>
                            <td class="bd01 w6_1 ta_c" data-name="totalEval"></td>
                            <td colspan="3" class="w8_1 ta_c pd0 free_textarea"></td>
                        </tr>
                </table>
<!-- 
                <div class="t_title"><span class="tt1">nilai 기준  : </span>
                    <input type="radio" name="grade_method_code" value="01" class="ip01">
                    <span class="rd_sp">상대penilaian</span>

                    <input type="radio" name="grade_method_code" value="02" class="ip01">
                    <span class="rd_sp">절대penilaian</span>

                    <input type="radio" name="grade_method_code" value="00" class="ip01">
                    <span class="rd_sp">이용안함</span>
                </div>
                <table class="tab_table ttb1">
                    <tbody id="gradeStandardList">
                    	
                    </tbody>
                </table> -->

                <div class="t_title">교육과정 개요 및 목적</div>
                <table class="tab_table ttb1">
                    <tbody>
                        <tr class="tr01">
	                        <td class="pd0 bd01 free_textarea">
	                           <textarea class="tarea02" rows="1" style="height: 60px;" placeholder="* Silakan masukkan konten ." name="curr_summary"></textarea>
	                        </td>
                        </tr>
                    </tbody>
                </table>

                <div class="t_title">Buku teks, bahan tambahan, bahan referensi</div>
                <table class="tab_table ttb1">
                    <tbody>
                        <tr class="tr01">
	                        <td class="pd0 bd01 free_textarea">
	                            <textarea class="tarea02" rows="1" style="height: 60px;" placeholder="* Silakan masukkan konten ." name="reference_data"></textarea>
	                        </td>
                        </tr>
                    </tbody>
                </table>

                <div class="t_title">Rencana dan peraturan operasi mandiri (referensi khusus untuk kursus)</div>
                <table class="tab_table ttb1">
                    <tbody>
                        <tr class="tr01">
	                        <td class="pd0 bd01 free_textarea">
	                           <textarea class="tarea02" rows="1" style="height: 60px;" placeholder="* Silakan masukkan konten ." name="management_plan"></textarea>
	                        </td>
                        </tr>
                    </tbody>
                </table>

				<div class="t_title">proses kinerja</div>
                <table class="tab_table ttb1">
                    <tbody>
                        <tr class="tr01">
	                        <td class="pd0 bd01 free_textarea">
	                           <textarea class="tarea02" rows="1" style="height: 60px;" placeholder="* Silakan masukkan konten ." name="curr_outcome"></textarea>
	                        </td>
                        </tr>
                    </tbody>
                </table>
                
                <div class="t_title">Kompetensi Kelulusan dan proses kinerja</div><button class="btn_tt1" onClick="selectGCDel();">hapus yang dipilih</button><button class="btn_tt2 open3">syarat kelulusan tambahkan</button>
                <table class="tab_table ttb1">
                    <thead>
                        <tr class="tr01">
                            <td class="th01 w5">syarat kelulusan</td>
                            <td class="th01 w10">proses kinerja</td>
                            <td class="th01 w4">cara pengajaran</td>
                            <td class="th01 w4">Metode evaluasi</td>
                            <td class="th01 w1_1">manajemen</td>
                        </tr>                       
                    </thead>
                    <tbody id="mpFinishCapabilityList">
                        
                    </tbody>
                </table>    

            </div> 
            <!-- e_scroll_wrap -->
            </form>

            <div class="bt_wrap">
                <!-- <button class="bt_1">임시 simpan</button> -->
                <button class="bt_2" name="saveBtn" onclick="curriculumSubmit();">registrasi</button>
                
               	<!-- 교육과정 확정체크 -->
                <c:choose> 
                	<c:when test='${currFlagInfo.curr_confirm_flag == "N" }'>
                		<button class="bt_3" name="confirmBtn" onclick="currConfirm('Y');">확정</button>
                	</c:when>
                	<c:otherwise>
                		<button class="bt_3" name="confirmCancelBtn" onclick="currConfirm('N');">확정batal</button>
                	</c:otherwise>                
                </c:choose>
                <!-- <button class="bt_3">batal</button> -->
            </div>

		</div> 
        <!-- e_tabcontent -->
   </div>
   
        <!-- s_ 팝업 : 교수 tambahkan -->
        <div id="m_pop1" class="pop_up_pfadd mo1">
            <input type="hidden" id="popupType" value=""> 
            <div class="pop_wrap">
                <button class="pop_close close1" type="button">X</button>
                <p class="t_title">교수 tambahkan</p>
                
                <!-- s_pop_ipwrap1 -->
                <div class="pop_ipwrap1">
                    
                    <!-- s_wrap2_lms_uselectbox -->
                    <div class="wrap2_lms_uselectbox">
                        <div class="uselectbox" id="department_code" value="">
                            <span class="uselected">jurusan khusus</span><span class="uarrow">▼</span>
                            <div class="uoptions">
                                <span class="uoption firstseleted" value="">pilih</span>
                                <c:forEach var="specialtyCodeList" items="${specialtyCodeList}">
                                    <span class="uoption" value="${specialtyCodeList.code}">${specialtyCodeList.code_name}</span>
                                </c:forEach>                                  
                            </div>
                        </div>                                  
                    </div>
                <!-- e_wrap2_lms_uselectbox --> 

                    <span class="ip_tt">nama </span>
                    <div class="pop_date">
                        <input type="text" class="ip_search" id="mpf_name" placeholder="nama 을 입력하세요."
                        	 onkeypress="javascript:if(event.keyCode==13){curriculumMpfList(); return false;}">
                        <button type="button" class="btn_search1" onClick="curriculumMpfList();">Pencarian</button>
                    </div>
                </div>
                <!-- e_pop_ipwrap1 -->                  
                                                                        
                <!-- s_table_b_wrap -->
                <div class="table_b_wrap">  
                    <!-- s_pop_twrap0 -->
                    <div class="pop_twrap0">
                        <!-- s_box_tt -->   
                        <div class="box_tt">                         
                            <span class="th01 w1">nama </span>
                            <span class="th01 w2">사번</span>
                            <span class="th01 w3">departement</span>
                            <span class="th01 w4">jurusan khusus</span>
                            <span class="th01 w5">kontak</span>
                            <span class="th01 w6">E-mail</span>
                            <span class="th01 w7">pilih</span>
                        </div>
                        <!-- s_box_tt --> 
                    </div>
                    <!-- e_pop_twrap0 -->
                    <!-- s_pop_twrap1 -->
                    <div class="pop_twrap1">   
                        <table class="tab_table ttb1">
                            <tbody id="mpf_add">
                                <!-- <tr class="tr01">                            
                                    <td class="ta_c w1">가교수</td>
                                    <td class="ta_c w2">5432</td>
                                    <td class="ta_c w3">내과</td>
                                    <td class="ta_c w4">호흡기내과</td>
                                    <td class="ta_c ls_1 w5">010-1234-5678</td>
                                    <td class="ta_c ls_1 w6"><a class="mailto_1" href="mailto:testid0000@gmail.com" target="_top">testid0000@gmail.com</a></td>
                                    <td class="ta_c w8"><label class="chk01"><input type="checkbox"><span class="slider round">pilih</span></label></td>
                                </tr> -->
                            </tbody>
                        </table>
                    </div>        
                    <!-- e_pop_twrap1 -->
                    <!-- s_pht -->
                    <div class="pht">
                        <div class="wrap2">
                            <span class="tt_1">pilih된 교수님</span><span class="tt_2" id="selectMpfCount"></span>
                        </div>                    
                        <div class="con_wrap"> 
                            <div class="con_s2" id="select_pf"> 
                              
                            </div>
                        </div>         
                    </div>
                    <!-- e_pop_table -->
                </div>                       
                <!-- e_pht --> 

                <div class="t_dd">
                    <div class="pop_btn_wrap2">
                        <button type="button" type="button" class="btn01" onclick="popupMpfSubmit();">registrasi</button>
                        <button type="button" type="button" class="btn02" onclick="$('#m_pop1').hide();">batal</button>
                    </div>
                </div>
            </div>  
        </div>
        <!-- e_ 팝업 : 교수 tambahkan -->
    
		<!-- s_ 팝업 : syarat kelulusan tambahkan -->
		<div id="m_pop3" class="pop_up_gradadd mo3">  
			<div class="pop_wrap">
			<button class="pop_close close3" type="button">X</button>     
			
			<p class="t_title">syarat kelulusan tambahkan</p>       
			                                                               
				 <!-- s_table_b_wrap -->
				<div class="table_b_wrap">  
				<!-- s_pop_twrap0 -->
				
					<div class="pop_twrap0">
						<!-- s_box_tt -->   
						<div class="box_tt">                         
							<span class="th01 w1">syarat kelulusan</span>
							<span class="th01 w5">pilih</span>
						</div>
						<!-- s_box_tt --> 
					</div>
					<!-- e_pop_twrap0 -->
					
					<!-- s_pop_twrap1 -->
					<div class="pop_twrap1">   
						<table class="tab_table ttb1">
							<tbody id="gcAddList">
								          
							</tbody>
						</table>
					</div>        
					<!-- e_pop_twrap1 -->    
				
					<!-- s_pht -->
					<div class="pht">   
						<div class="wrap2">
						<span class="tt_1">pilih된 syarat kelulusan</span><span class="tt_2" id="gcSelectCount"></span>                  
						</div>
					                    
						<div class="con_wrap"> 
							<div class="con_s2" id="gcSelectItem"> 
								<!-- s_ syarat kelulusan 1set -->
								<div class="a_mp">syarat kelulusan syarat kelulusan syarat kelulusan syarat kelulusan syarat kelulusan 1<button class="btn_c" title="삭제하기">X</button></div>
								<!-- e_ syarat kelulusan 1set --> 
							</div>
						</div>
					     
					</div>
					<!-- e_pop_table -->                 
				                     
				</div>               
				<!-- e_pht --> 
			
				<div class="t_dd">
				               
				<div class="pop_btn_wrap2">
					<button type="button" class="btn01" onclick="selectGcSubmit();">registrasi</button>
					<button type="button" class="btn02" onClick="$('.close3').click().change();">batal</button>                    
				</div>
				                
				</div>
			</div>  
		</div>
		<!-- e_ 팝업 : syarat kelulusan tambahkan -->

<!-- s_ 팝업 : tidak ada perkuliahan 상세 -->
<div id="m_pop4" class="pop_up_nonlec mo4">  
          <div class="pop_wrap">
                <button class="pop_close close4" type="button" onClick="$('#m_pop4').hide();">X</button>     

                <p class="t_title" id="lessonMethodTitle"></p>       
                                                                        
 <!-- s_table_b_wrap -->
<div class="table_b_wrap">  
<!-- s_pht -->
<div class="pht">   
                    
<div class="conwrap" id="lessonDetailAdd"> 

</div>
         
         </div>        
<!-- e_pht --> 
 </div>  
</div>
</div>
<!-- e_ 팝업 : tidak ada perkuliahan 상세 -->
</body>