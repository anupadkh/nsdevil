<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<script src="${JS}/schedule.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("body").addClass("full_schd");
		
    	$('.free_textarea').on( 'keyup', 'textarea', function (e){
			$(this).css('height', 'auto' );
			$(this).height( this.scrollHeight );
		});
		$('.free_textarea').find( 'textarea' ).keyup();
		
		bindPopupEvent("#m_pop2", ".open2");
		
		getSchedule("${S_CURRICULUM_SEQ}");
		getCurriculum();
	});
	
	//교육과정 가져오기
	function getCurriculum() {
		$.ajax({
			type : "GET",
			url : "${HOME}/ajax/pf/lesson/curriculumInfo",
			data : {},
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {

					$("span[data-name=curr_name]").text(data.basicInfo.curr_name);
					$("span[data-name=academic_name]").text(": " + data.basicInfo.academic_name);
                    
				} else {

				}
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}
	
	function fileSelected() {
		if (fileCheck($("#xlsFile"))) {
			$(":input[name='fileName']").val($("#xlsFile").val());
		} else {
			$(":input[name='fileName']").val("");
			var fileElement = $("#xlsFile");
			fileElement.replaceWith(fileElement.clone(true));
		}
	}
	
	function fileCheck(element) {
		if (!element.val()) {
			alert("파일을 pilih하세요");
			return false;
		}
		
		var ext = element.val().substr(element.val().lastIndexOf(".") + 1).toLowerCase();
		
		if (ext != "xlsx") {
			alert("xlsx파일만 업로드 가능합니다.");
			return false;
		}
		return true;
	}
	
	function uploadXls() {
		
		if (!fileCheck($("#xlsFile"))) {
			return;
		}
		
		$("#uploadForm").ajaxSubmit({
			type: "POST",
			url: "${HOME}/ajax/pf/lesson/uploadScheduleXls",
			dataType: "json",
			success: function(data, status) {
				if (data.status == "200") {
					alert("registrasi이 완료되었습니다.");
					$(":input[name='fileName']").val("");
					var fileElement = $("#xlsFile");
					fileElement.replaceWith(fileElement.clone(true));
					$("#m_pop2").hide();
					getSchedule("${S_CURRICULUM_SEQ}");
				} else {
					alert("오류가 발생했습니다." + data.msg);
				}
			},
			error: function(xhr, textStatus) {
				//document.write(xhr.responseText);
				alert("오류가 발생했습니다.");
				$.unblockUI();
			},beforeSend:function() {
				$.blockUI();
			},complete:function() {
				$.unblockUI();
			}
		});
	}
	
</script>
</head>

<body class="mpf empty">

<div class="main_con adm_ccschd">
<!-- s_메뉴 접기 버튼 -->	
<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
<!-- e_메뉴 접기 버튼 -->	
	
<!-- s_tt_wrap -->                
<div class="tt_wrap">
			    <h3 class="am_tt">
			    	<span class="tt" data-name="curr_name"></span>
			    	<span class="tt_s" data-name="academic_name"></span>
			    </h3>                    
			</div>
<!-- e_tt_wrap -->

<!-- s_tab_wrap_cc --> 
<div class="tab_wrap_cc">
<!-- s_해당 탭 페이지 class에 active tambahkan --> 
	<button class="tab01 tablinks" onclick="pageMoveCurriculumView('${S_CURRICULUM_SEQ}');">rencan kurikulum</button>	         
	<button class="tab01 tablinks active" onclick="location.href='${HOME}/admin/lesson/schedule'">waktu표manajemen</button>	
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/unit'">단원manajemen</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/lessonPlanCs'">penambahan bab pembelajaran</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/survey'">만족도 조사</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/currAssignMent'">종합 과제</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/soosiGrade'">수시 성적</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/grade'">종합 성적</button>	
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/report'">운영보고서</button>
<!-- e_해당 탭 페이지 class에 active tambahkan -->
</div>
 
<!-- s_mpf_tabcontent2 --> 
 <div class="tabcontent2">
<!-- s_tt_wrap -->                
<div class="tt_wrap">
    <button class="btn_dw2" onclick="javascript:location.href='${HOME}/pf/lesson/scheduleExcelTmplDown';">엑셀양식다운로드</button>
    <button class="btn_up1 open2">엑셀일괄registrasi</button>
<!-- s_sch_wrap -->
<div class="sch_wrap">
<span class="mpf_tt_s">* 최초 엑셀일괄registrasi 후 waktu표 perbaiki이 가능합니다. <br>( 이후, 월별 / 주별 / 일별 waktu표 뷰어 제공 )</span>
</div>
<!-- e_sch_wrap -->                       
</div>
<!-- e_tt_wrap -->
     
<div class="wrap_mlms_tb1">          
<!-- s_mlms_tb1 -->                                                          
<table class="mlms_tb1">
                    <thead>
                        <tr>
                            <th class="th01 bd01 w1">waktu</th>
                            <th class="th01 bd01 w2">hari dan tanggal</th>
                            <th class="th01 bd01 w2"> pengajar</th>
                            <th class="th01 bd01 w4">nama dosen pengajar</th>
                            <th class="th01 bd01 w4_1">topik pembelajaran</th>
                            <th class="th01 bd01 w2">manajemen</th>
                        </tr>
                    </thead>
                    <tbody id="scheduleList"></tbody>
<%--
                    <tbody>    
                        <tr>
                            <td class="td_1"><span class="tt01">1</span></td>
                            <td class="td_1"></td>
                            <td class="td_1"></td>
                            <td class="td_1 t_l"></td>
                            <td class="td_1 t_l"></td>
                            <td class="td_1"></td>
                            <td class="td_1"><button class="btn_mdf">perbaiki</button></td>
                        </tr>
                        <tr>
                            <td class="td_1"><span class="tt01">2</span></td>
                            <td class="td_1"></td>
                            <td class="td_1"></td>
                            <td class="td_1 t_l"></td>
                            <td class="td_1 t_l"></td>
                            <td class="td_1"></td>
                            <td class="td_1"><button class="btn_mdf">perbaiki</button></td>
                        </tr>
                    </tbody>
--%>
</table>
<!-- e_mlms_tb1 -->
</div>

<div class="bt_wrap">
    <!--<button class="bt_3a" onclick="location.href='mpf_schd.html'">registrasi</button>-->
    <button class="bt_3_1" onclick="location.href='${HOME}/admin/lesson/schedule'">batal</button>
</div>
</div>
<!-- e_mpf_tabcontent2 -->
</div>
<!-- s_ 엑셀 일괄 registrasi 팝업 -->
<div id="m_pop2" class="pop_up_ex pop_up_ex_1 mo1" >
    
     <div class="pop_wrap">
                <p class="popup_title">교육과정 waktu표 엑셀 업로드</p>
                
                <button class="pop_close close1" type="button">X</button>
                   
                <button class="btn_exls_down_2" onclick="downloadTmpl('${HOME}','schedule_template.xlsx');">엑셀 양식 다운로드</button>    
                   
                    <div class="t_title_1">
                        <span class="sp_wrap_1">waktu표 registrasi 엑셀양식을 먼저 다운로드 받으신 뒤,<br>다운로드 받은 엑셀파일양식에 waktu표 정보를 입력하여 파일 registrasi 하시면<br>Keseluruhan waktu표가 일괄 업로드 됩니다.</span>
                    </div> 
                    
          <div class="pop_ex_wrap"> 
                    <div class="sub_fwrap_ex_1">
                    	<form id="uploadForm" onSubmit="return false;">
	                       <span class="ip_tt">file excel</span>
	                       <input type="text" name="fileName" readonly class="ip_sort1_1" value="" onClick="$('#xlsFile').click();">
	                       <button class="btn_r1_1" onClick="$('#xlsFile').click();">파일pilih</button>
	                       <input type="file" style="display:none;" id="xlsFile" name="xlsFile" onChange="fileSelected();">
                       </form>
                    </div>
               
                <!-- s_t_dd --> 
                <div class="t_dd">
                   
                    <div class="pop_btn_wrap">
                     <button type="button" class="btn01" onclick="uploadXls();">registrasi</button>
                     <button type="button" class="btn02 close">batal</button>                    
                    </div>
                
                </div>
               <!-- e_t_dd -->                  
                                   
           </div>                                   
     </div>                 
</div>
<!-- e_ 엑셀 일괄 registrasi 팝업 -->
<jsp:include page="schedulePopup.jsp">
	<jsp:param name="pageName" value="schedule" />
</jsp:include>
</body>
</html>