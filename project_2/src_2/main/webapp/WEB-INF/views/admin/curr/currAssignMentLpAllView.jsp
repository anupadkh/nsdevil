<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
	<html>
	<head>
		<style>
			.pop_up_hnonregi{display: none;position: fixed;width:100%;height: 100%;margin: 0 auto;padding: 0 0 0 0;top:0;left:0;background: rgba(17, 17, 17,.5);overflow: auto;z-index:100012000;box-sizing: content-box;}			
			.tabcontent5.rl .tab_wraps{display:flex;width: 98%;font-size: 15px;line-height: 17px;color: rgba(8, 99, 172, 1);padding: 0;margin: 13px 0 10px 0;border-bottom: dashed 1px rgba(91, 170, 233, 1);float: right;box-sizing: border-box;}
			.tabcontent5.rl .tab_wraps .h_tt{display:inline-block;flex-grow: 1;height: 30px;line-height: 30px;font-size: 15px;text-align: center;color: rgb(69, 125, 170);border-right: solid 0px rgb(147, 182, 209);border-left: solid 0px rgb(147, 182, 209);background: rgba(247, 251, 253, 1);margin: 0 0 12px 0;padding: 0;letter-spacing: -1px;box-sizing: border-box;float: left;cursor: pointer;font-weight: 500;word-spacing: 3px;}
			.tabcontent5.rl .tab_wraps .h_tt.on, .tabcontent5 .tab_wraps .h_tt:hover{color: rgb(1, 59, 116);transition:.7s;font-weight: 600;}
			.tabcontent5.rl .tab_wraps .h_tt.n2{border-right: solid 2px rgb(162, 205, 239);border-left: solid 2px rgb(162, 205, 239);}	
			.tabcontent5.rl .mlms_tb1.all tr td .num{font-size: 13px;display: inline-block;color: rgb(65, 65, 65);box-shadow: 0px 2px 0px rgba(69, 189, 255, 0.5);padding: 3px 0px 3px 0px;margin: 0 0 2px 0;cursor: pointer;}
			.tabcontent5.rl .mlms_tb1.all tr td .num:hover{background: rgba(224, 249, 255,.7);border-radius: 4px;color: rgb(2, 62, 108);}	
		</style>
    	<script src="${HOME}/resources/smarteditor_v2.9/js/service/HuskyEZCreator.js"></script>       
		<script type="text/javascript">					
			$(document).ready(function() {
				bindPopupEvent("#m_pop1", ".open1");
				bindPopupEvent("#m_pop2", ".open2");
				getAssignMent();	
				getCurriculum();
			});	
			
			//교육과정 가져오기
			function getCurriculum() {
				$.ajax({
					type : "GET",
					url : "${HOME}/ajax/pf/lesson/curriculumInfo",
					data : {},
					dataType : "json",
					success : function(data, status) {
						if (data.status == "200") {

							$("span[data-name=curr_name]").text(data.basicInfo.curr_name);
							$("span[data-name=academic_name]").text(": " + data.basicInfo.academic_name);
	                        
						} else {

						}
					},
					error : function(xhr, textStatus) {
						document.write(xhr.responseText);
					},
					beforeSend : function() {
						$.blockUI();
					},
					complete : function() {
						$.unblockUI();
					}
				});
			}		
				       
	        function getAssignMent(){
	        	$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/pf/lesson/currAssignMent/lpAll/list",
	                data: {                  
	                },
	                dataType: "json",
	                success: function(data, status) {
	                	if(data.status == "200"){
		                   var htmls = "";
		                   $("#asgmtListAdd").empty();
		                   $("span[data-name=assignCnt]").text(data.list.length);
		                   $.each(data.list, function(index){
		                	   var group_yn = "";
		                	   htmls = '<tr>'
		                	   +'<td class="td_1">'+(index+1)+'</td>'
		                	   +'<td class="td_1">'+this.lesson_date+'</td>'
		                	   +'<td class="td_t">'+this.lesson_subject+'</td>'
		                	   +'<td class="td_1">'+this.name+'</td>'
		                	   +'<td class="td_1">'+this.start_date+'</td>'
		                	   +'<td class="td_1">'+this.end_date+'</td>'
		                	   +'<td class="td_t open1" onclick="detailAsgmt('+this.asgmt_seq+');">'+this.asgmt_name+'</td>'
		                	   +'<td class="td_1">○</td>'
		                	   +'<td class="td_1">'+this.submit_cnt+'</td>';

		                	   if(this.st_cnt-this.submit_cnt > 0)
		                		   htmls+='<td class="t_1"><span class="num open2" onClick="getNotSubmitStudentList('+this.asgmt_seq+',\''+this.lesson_subject+'\',\''+this.name+'\',\''+this.asgmt_name+'\');">'+(this.st_cnt-this.submit_cnt)+'</span></td>';
	                		   else
	                			   htmls+='<td class="t_1"><span class="num">-</span></td>';
	 		                   htmls+='</tr>';
	 		                   
	 		                   $("#asgmtListAdd").append(htmls);
		                   });
	                	}
	                },
	                error: function(xhr, textStatus) {
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                }
	            }); 
	        }   
	        
	        function detailAsgmt(asgmt_seq){
	        	$.ajax({
                    type: "POST",
                    url: "${HOME}/ajax/pf/lesson/assignMent/detailView",
                    data: {
                    	"asgmt_seq" : asgmt_seq
                    },
                    dataType: "json",
                    success: function(data, status) {
                        if(data.status=="200"){
                        	var htmls = "", start_day = "", end_day = "";
                            $("#m_pop1").show();
                            
                            switch(data.asgmt.start_day){
	                            case 0 : start_day="일";break;
	                            case 1 : start_day="월";break;
	                            case 2 : start_day="화";break;
	                            case 3 : start_day="수";break;
	                            case 4 : start_day="목";break;
	                            case 5 : start_day="금";break;
	                            case 6 : start_day="토";break;
                            }
                            
                            switch(data.asgmt.end_day){
	                            case 0 : end_day="일";break;
	                            case 1 : end_day="월";break;
	                            case 2 : end_day="화";break;
	                            case 3 : end_day="수";break;
	                            case 4 : end_day="목";break;
	                            case 5 : end_day="금";break;
	                            case 6 : end_day="토";break;
	                        }
                        
                            $("span[name=popupAsgmtName]").text(data.asgmt.asgmt_name);
                            $("span[name=popupAsgmtStartDate]").text(data.asgmt.start_date+"("+start_day+")");
                            $("span[name=popupAsgmtEndDate]").text(data.asgmt.end_date+"("+end_day+")");

                            if(data.asgmt.group_flag=="Y"){
                            	$("span[name=popupAsgmtGroupYN]").removeClass("tt_3_1");
                            	$("span[name=popupAsgmtGroupYN]").addClass("tt_3_2");
                            	$("span[name=popupAsgmtGroupYN]").text("그룹과제");
                            }
                            else{
                            	$("span[name=popupAsgmtGroupYN]").removeClass("tt_3_2");
                                $("span[name=popupAsgmtGroupYN]").addClass("tt_3_1");
                                $("span[name=popupAsgmtGroupYN]").text("개별과제");
                            }
                            $("div[name=popupAsgmtContent]").html(data.asgmt.content); 
                         
                            $.each(data.asgmtAttach, function(){
                            	htmls+='<li class="li_1" style="cursor:pointer;" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');">'+this.file_name+'</li>';
                            });
                            $("ul[name=asgmtAttachList]").html(htmls);
                            
                            
                        }else{
                            alert("실패");
                        }
                        
                    },
                    error: function(xhr, textStatus) {
                        //alert("오류가 발생했습니다.");
                        document.write(xhr.responseText);
                    }
                }); 
	        	
	        }
	        
	        function getNotSubmitStudentList(asgmt_seq, lesson_subject, name, asgmt_name){
				 $.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/pf/assignMent/notSubmitSTList",
		            data: {   
		            	"asgmt_seq" : asgmt_seq,
		            	"curr_seq" : "${S_CURRICULUM_SEQ}"
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){			            		
		            		var htmls = "";
		            		$.each(data.stList, function(){
		            			var picture = "";
		            			if(isEmpty(this.picture_path))
		            				picture = "${IMG}/ph_3.png"
	            				else
	            					picture = "${RES_PATH}"+this.picture_path;
	            					
	            				htmls+='<div class="a_mp"> <span class="pt01"><img src="'+picture+'" alt="사진" class="pt_img"></span>'
	            				+'<span class="ssp1">'+this.name+'</span> </div>';	
		            		});
		            		$("#notSubmitListAdd").html(htmls);
		            		$("span[data-name=notStCnt]").text(data.stList.length);  
		            		$("span[data-name=lessonSubject]").text(lesson_subject);
		            		$("span[data-name=pfName]").text(name);
		            		$("span[data-name=asgmt_name]").text(asgmt_name);
		            	}else{
		            		alert("과제 미제출 리스트 가져오기 실패");
		            	}
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        }); 
			}
		</script>
	</head>
	<body>
		
		<div class="main_con">
			<!-- s_메뉴 접기 버튼 -->	
			<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
			<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
			<!-- e_메뉴 접기 버튼 -->	
				
			<!-- s_tt_wrap -->                
			<div class="tt_wrap">
			    <h3 class="am_tt">
			    	<span class="tt" data-name="curr_name"></span>
			    	<span class="tt_s" data-name="academic_name"></span>
			    </h3>                    
			</div>
			<!-- e_tt_wrap -->
			    
			<!-- s_tab_wrap_cc --> 
			<div class="tab_wrap_cc">
			<!-- s_해당 탭 페이지 class에 active tambahkan --> 
				<button class="tab01 tablinks" onclick="pageMoveCurriculumView('${S_CURRICULUM_SEQ}');">rencan kurikulum</button>	         
				<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/schedule'">waktu표manajemen</button>	
				<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/unit'">단원manajemen</button>
				<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/lessonPlanCs'">penambahan bab pembelajaran</button>
				<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/survey'">만족도 조사</button>
				<button class="tab01 tablinks active" onclick="location.href='${HOME}/admin/lesson/currAssignMent'">종합 과제</button>
				<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/soosiGrade'">수시 성적</button>
				<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/grade'">종합 성적</button>	
				<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/report'">운영보고서</button>
			<!-- e_해당 탭 페이지 class에 active tambahkan -->
			</div>
			<!-- e_tab_wrap_cc --> 
			
		<!-- s_tabcontent -->
		<div id="tab001" class="tabcontent tabcontent5 rl">
		             
			<div class="tab_wraps">
			    <button onclick="location.href='${HOME}/admin/lesson/currAssignMent/create'" class="h_tt n1">교과정 과제리스트</button>
			    <button onclick="location.href='${HOME}/admin/lesson/currAssignMent/view'" class="h_tt n2">교과정 과제제출내역조회</button>
				<button onclick="location.href='${HOME}/admin/lesson/currAssignMent/lpAll'" class="h_tt n3 on">Keseluruhan 수업과제내역조회</button>
			</div> 
		
			<!-- s_hw_wrap-->
			<div class="hw_wrap_l">           
				<div class="tt_wrap">
				   <span class="sp_tt">registrasi된 과제</span>
				   <span class="sp_n" data-name="assignCnt"></span>
				   <span class="sp_sign">건</span>
				</div>	
			</div>
			<!-- e_hw_wrap-->
			<table class="mlms_tb1 all">
				<thead>
					<tr>
						<th class="th01 bd01 w1" style="width:5%;">No.</th>
						<th class="th01 bd01 w2" style="width:12%;">수업일</th>
						<th class="th01 bd01 w2" style="width:12%;">수업명</th>
						<th class="th01 bd01 w2" style="width:12%;">nama dosen pengajar</th>
						<th class="th01 bd01 w2" style="width:12%;">과제오픈일</th>
						<th class="th01 bd01 w2" style="width:12%;">과제마감일</th>
						<th class="th01 bd01 w3" style="width:17%;">과제명</th>
						<th class="th01 bd01 w4" style="width:6%;">그룹<br>과제</th>
						<th class="th01 bd01 w4" style="width:6%;">제출<br>현황</th>
						<th class="th01 bd01 w4" style="width:6%;">미registrasi<br>학생</th>
					</tr>
				</thead>
				<tbody id="asgmtListAdd">
					
				</tbody>
			</table>
		</div>
</div>
		<!-- s_ 과제 registrasi 내역 팝업 -->
		<div id="m_pop1" class="pop_up_h pop_up_h_1 mo1">
		    
		     <div class="pop_wrap">
		                <p class="popup_title">과제 registrasi 내역</p>
		                
		                <button class="pop_close close1" type="button" onClick="$('#m_pop1').hide();">X</button>
		                
		                  
		                  
		           <div class="t_title" style="margin-top:10px;">
		              <div class="wrap">
		                   <span class="tt_1">과제 오픈 일</span><span class="tt_2" name="popupAsgmtStartDate"></span>
		                   <span class="tt_1">제출 마감 일</span><span class="tt_2" name="popupAsgmtEndDate"></span>
		                   <span class="tt_3_1" name="popupAsgmtGroupYN" style="width:110px;"></span><!-- s_ 그룹과제 class tt_3_2 -->
		               </div>
		               <div class="wrap">
		                   <span class="tt_1" style="float:left;">과제 명</span>
		                   <span class="tt_2_1" name="popupAsgmtName"></span>                  
		               </div>
		          </div> 
		                    
		          <div class="con_wrap"> 
		                <div class="con_s" name="popupAsgmtContent" style="height:250px;"> 
		                        
		                </div>
		         </div>
		         <ul class="h_list" name="asgmtAttachList">                    
		         </ul>    
		                <!-- s_t_dd --> 
		                <div class="t_dd">
		                   
		                    <div class="pop_btn_wrap">
		                     <button type="button" class="btn01" onclick="$('#m_pop1').hide();">확인</button>                
		                    </div>
		                
		                </div>
		               <!-- e_t_dd -->                  
		                                 
		     </div>                 
		</div>
		<!-- e_ 과제 registrasi 내역 팝업 -->    
	
		<!-- s_ 미registrasi 학생 팝업 -->
		<div id="m_pop2" class="pop_up_hnonregi mo2">
			<div class="pop_wrap">
	
				<p class="t_title">미registrasi 학생</p>
	
				<!-- s_table_b_wrap -->
				<div class="table_b_wrap">
					<p class="t_title1">
						<span class="sp_un">수업명</span><span class="sp_num" data-name="lessonSubject"></span>
					</p>
					<p class="t_title1">
						<span class="sp_un">nama dosen pengajar</span><span class="sp_num" data-name="pfName"></span>
					</p>
					<p class="t_title1">
						<span class="sp_un">과제명</span><span class="sp_num" data-name="asgmt_name"></span>
					</p>
					<p class="t_title2">
						<span class="sp_un">미registrasi</span><span class="sp_num" data-name="notStCnt"></span><span
							class="sp_un">명</span>
					</p>
	
					<!-- s_pht -->
					<div class="pht">
						<div class="con_wrap">
							<div class="con_s2" id="notSubmitListAdd">								
							</div>
						</div>
					</div>
					<!-- e_pop_table -->
	
				</div>
				<!-- e_pht -->
	
				<div class="t_dd">
	
					<div class="pop_btn_wrap2">
						<button type="button" class="btn01 close2">닫기</button>
					</div>
	
				</div>
			</div>
		</div>
		<!-- e_ 미registrasi 학생 팝업 -->
	
		<!-- s_팝업 -->
	</body>
</html>