<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<script type="text/javascript">
	$(document).ready(function() {
		$("body").addClass("search");
		getScheduleSearch("${S_CURRICULUM_SEQ}");
		getCurriculum();
	});
	
	//교육과정 가져오기
	function getCurriculum() {
		$.ajax({
			type : "GET",
			url : "${HOME}/ajax/pf/lesson/curriculumInfo",
			data : {},
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {

					$("span[data-name=curr_name]").text(data.basicInfo.curr_name);
					$("span[data-name=academic_name]").text(": " + data.basicInfo.academic_name);
                    
				} else {

				}
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}
	
	//waktu표 목록
	function getScheduleSearch(curr_seq) {
		
		if (curr_seq == "") {
			alert("교육과정을 pilih해 주세요.");
			history.back(-1);
			return;
		}
		
	    $.ajax({
	        type: "POST",
	        url: "${HOME}/ajax/pf/lesson/getSchedule",
	        data: {
	        	"curr_seq": curr_seq,
	        	"search_keyword": $.trim($(":input[name='search_keyword']").val())
	        },
	        dataType: "json",
	        success: function(data, status) {
	        	var html = "";
				
	        	$("span[data-name='curr_name']").html(data.basicInfo.curr_name);
	        	$("span[data-name='aca_system_name']").html("("+ data.basicInfo.aca_system_name + ")");
	        	$("#searchCount").html(data.schedule.length);
	        	var prevTitleDay = "";
	        	
	            $.each(data.schedule, function() {
	            	
	            	if (prevTitleDay != this.lesson_date) {
	            		prevTitleDay = this.lesson_date.toLocaleString("ko-KR", {  weekday: "long" });
	            		html += '<tr><td colspan="3" class="th02 bd01">' + this.lesson_date + ' (' + this.lesson_date.toLocaleString("ko-KR", {  weekday: "long" }) + ')</td></tr>';
	            	}
	            	
	            	var userPic = "${IMG}/ph_3.png";
	            	
	            	if (this.picture_path != null) {
	            		userPic = "${RES_PATH}"+this.picture_path;
	            	}
	            	
	            	var lesson_subject = this.lesson_subject;
	            	var nonregiClass = "";
	            	
	            	if (lesson_subject == "") {
	            		nonregiClass = "nonregi";
	            		lesson_subject = "(미registrasi)";
	            	}
	            	
	            	var department = "";
	            	if (this.department != "") {
	            		department = "(" + this.department + ")";
	            	}
	            	
	            	var lessonTime = "";
	            	
	            	if (this.start_time_12h != null) {
	            		lessonTime = this.start_time_12h;
	            		if (this.end_time_12h != null) {
	            			lessonTime += lessonTime + " ~ " + this.end_time_12h;
	            		}
	            	}
	            	
	            	html += '<tr>'
						+ '<td class="td_1 w1">'
						+ '	<span class="wrap_num">' + this.period_seq + '</span>'
						+ '	<span class="tt01">' + this.period + ' pengajar</span>'
						+ '</td>'
						+ '<td class="td_1 w2">'
						+ '	<div class="tt_wrap_s">'
						+ '	<span class="tt ' + nonregiClass + '">' + lesson_subject + '</span>'
						+ '	<span class="tt_dt">' + lessonTime + '</span>'
						+ '	</div>'
						+ '</td>'
						+ '<td class="td_1 w3"><span class="a_mp"><span class="pt01"><img src="' + userPic + '" alt="사진" class="pt_img"></span><span class="ssp1">' + this.pf_name + '</span><span class="ssp2">' + department + '</span></span></td>'
	                	'</tr>';
	            });
	            $("#scheduleSearchList").html(html);
	            
	            if (data.schedule.length == 0) {
		        	$(".mlms_tb_wrap .mlms_tb1").addClass("class_x");
		        	$(".mlms_tb_wrap .mlms_tb1").html("<tr><td></td></tr>");
		        	$("#regButton").html("<button class=\"bt_3a\" onclick=\"location.href='./scheduleMod'\">registrasi</button>");
	            }
	            
	            bindPopupEvent("#m_pop1", ".open1");
	        },
	        error: function(xhr, textStatus) {
	            alert("오류가 발생했습니다.");
	            //document.write(xhr.responseText);
	        }
	    });
	}
	
	//엔터키 Pencarian
	function startSearch(event) {
		if (event.keyCode == 13) {
			getScheduleSearch("${S_CURRICULUM_SEQ}");
		}
	}
</script>
</head>

<body class="search">

<!-- s_main_con -->
<div class="main_con adm_ccschd">
<!-- s_메뉴 접기 버튼 -->	
<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
<!-- e_메뉴 접기 버튼 -->	
	
<!-- s_tt_wrap -->                
<div class="tt_wrap">
			    <h3 class="am_tt">
			    	<span class="tt" data-name="curr_name"></span>
			    	<span class="tt_s" data-name="academic_name"></span>
			    </h3>                    
			</div>
<!-- e_tt_wrap -->

<!-- s_tab_wrap_cc --> 
<div class="tab_wrap_cc">
<!-- s_해당 탭 페이지 class에 active tambahkan --> 
	<button class="tab01 tablinks" onclick="pageMoveCurriculumView('${S_CURRICULUM_SEQ}');">rencan kurikulum</button>	         
	<button class="tab01 tablinks active" onclick="location.href='${HOME}/admin/lesson/schedule'">waktu표manajemen</button>	
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/unit'">단원manajemen</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/lessonPlanCs'">penambahan bab pembelajaran</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/survey'">만족도 조사</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/currAssignMent'">종합 과제</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/soosiGrade'">수시 성적</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/grade'">종합 성적</button>	
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/report'">운영보고서</button>
<!-- e_해당 탭 페이지 class에 active tambahkan -->
</div>
 
<!-- s_mpf_tabcontent2 --> 
 <div class="tabcontent2">
<!-- s_tt_wrap -->                
<div class="tt_wrap">
    <button class="btn_dw1 schd_mv" onclick="location.href='./scheduleM'">waktu표 월별 보기</button>
<!-- s_sch_wrap -->
<div class="sch_wrap">

<input type="search" name="search_keyword" class="ip_search" value="" onkeyup="startSearch(event)">
<button class="btn_search2" onclick="getScheduleSearch(${S_CURRICULUM_SEQ})">Pencarian</button>
            
</div>
<!-- e_sch_wrap -->                       
</div>
<!-- e_tt_wrap -->
 
<!-- s_mlms_tb1 -->                                                          
                   <table class="mlms_tb1 sch top">
                        <tr>
                            <td class="th01 bd01">
                            <div class="sch_wrap_s">총<span id="searchCount" class="num">15</span>건 Pencarian</div>
                            </td>
                        </tr>
                   </table>
                   
             <div class="wrap_schtb">      
                   <table class="mlms_tb1 sch">
                   <tbody id="scheduleSearchList"></tbody>
                   </table>
             </div>
<!-- e_mlms_tb1 -->

</div>
<!-- e_mpf_tabcontent2 -->
</div>
</body>
</html>