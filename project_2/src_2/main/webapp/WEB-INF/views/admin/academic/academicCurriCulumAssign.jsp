<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<script>
			$(document).ready(function() {

				bindPopupEvent("#m_pop101", ".open101");
				getAcademicTitle();
				getCurriculumList($("input[name=assign]:checked").val());
				$("footer").removeClass("ind");
			});
			
			function getAcademicTitle(){
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicReg/titleInfo",
		            data: {                      
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		if(!isEmpty(data.acaInfo.academic_name))
		                		$("#academicNameTitle").text(data.acaInfo.academic_name);
		                	else
		                		$("#academicNameTitle").hide();
		            	
		            		var aca_state = "";
		                	switch(data.acaInfo.aca_state){
	                            case "01" : aca_state = "개설(비공개)";break;
	                            case "02" : aca_state = "개설(공개)";break;
	                            case "03" : aca_state = "학사 중(수업 중)";break;
	                            case "04" : aca_state = "학사종료(미확정)";break;
	                            case "05" : aca_state = "학사종료(성적산정)";break;
	                            case "06" : aca_state = "학사종료(성적발표)";break;
	                            case "07" : aca_state = "성적발표 종료(미확정)";break;
	                            case "08" : aca_state = "학사최종종료(확정)";break;                            
	                        }
			            	$("#academicState").text(aca_state); //타이틀 status akademik
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
			
			function getCurriculumList(assignYN){
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicReg/curriculumAssign/list",
		            data: {        
		            	"assignYN" : assignYN
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		if(data.confirmFlag.curr_confirm_flag == "Y"){
		            			$("button[name=saveBtn]").hide();
		            			$("button[name=confirmBtn]").hide();
		            			$("button[name=cancleBtn]").show();
		            		}else{
		            			$("button[name=saveBtn]").show();
		            			$("button[name=confirmBtn]").show();
		            			$("button[name=cancleBtn]").hide();
		            		}
		            		
			            	$("#totalCount").text(data.currCount.curr_cnt);
			            	$("#assignCount").text(data.currCount.assign_cnt);
			            	var htmls = "";
			            	$("#currListAdd").empty();
			            	$.each(data.currList, function(index){
			            		var administerName = "";
			            		var targetName = "";
			            		//manajemen Klasifikasi
			            		switch(this.administer_code){
				            		case "01" : administerName = "Proses Integrasi "; break;
				            		case "02" : administerName = "Praktek"; break;
				            		case "03" : administerName = "Belajar Mandiri"; break;
			            		}
			            		
			            		//Target
			            		switch(this.target_code){
				            		case "01" : targetName = "all required"; break;
				            		case "02" : targetName = "신청"; break;
			            		}
			            		
			            		htmls = '<tr>';
			            		htmls += '<td class="td_1 w1 bd01">';
			            		if(isEmpty(this.aca_seq))
			            			htmls += '<input type="checkbox" class="chk01" name="curr_chk" value="'+this.curr_seq+'">';
			            		htmls += '</td>';
			            		htmls += '<td class="td_1 w2 bd01">'+this.curr_code+'</td>';
			            		htmls += '<td class="td_1 w4 bd01" onClick="curriculumModify('+this.curr_seq+');" style="color:rgba(3, 134, 242, 1);cursor:pointer;">'+this.curr_name+'</td>';
			            		htmls += '<td class="td_1 w3 bd01">'+this.mpf_name+'</td>';
			            		htmls += '<td class="td_1 w2 bd01">'+this.code_name+'</td>';
			            		htmls += '<td class="td_1 w1 bd01">'+this.grade+'</td>';
			            		htmls += '<td class="td_1 w2 bd01">'+administerName+'</td>';
			            		htmls += '<td class="td_1 w2_1 bd01">'+targetName+'</td>';
			            		htmls += '</tr>';
			            		$("#currListAdd").append(htmls);
			            	});			            	
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
			
			function curriculumAssignInsert(){
				var msg = "pilih하신 교육과정을\n"+$("#academicNameTitle").text()+"에 \n배정하시겠습니까?";
				
				if($("#currListAdd input[name=curr_chk]:checked").length == 0){
					alert("배정할 교육과정을 하나이상 pilih해주세요");
					return;
				}
				var array = new Array();
				$.each($("#currListAdd input[name=curr_chk]:checked"), function(){
					array.push($(this).val());
				});
				
				if(!confirm(msg))
					return;
							
				$.ajaxSettings.traditional = true;//배열 형태로 서버쪽 전송을 위한 설정
				
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicReg/curriculumAssign/assignInsert",
		            data: {        
		            	"curr_seq" : array
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		alert("배정 완료");
		            		getCurriculumList($("input[name=assign]:checked").val())  	
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
			
			function curriculumConfirmInsert(flag){
				var confirmMsg = "";
				var statusMsg = "";
				
				if(flag=="Y"){
					confirmMsg = "배정된 교육과정을 확정상태로 변경하시겠습니까?";
					statusMsg = "확정 완료 하였습니다.";
				}else{
					confirmMsg = "확정 batal 하시겠습니까?";
					statusMsg = "확정 batal 완료 하였습니다.";
				}
								
				if(!confirm(confirmMsg)){
					return;
				}
				
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicReg/academicRegBasicInfo/confirm",
		            data: {        
		            	"flag" : flag
		            	,"confirm_name" : "curriculum"
		                ,"aca_seq" : $("#aca_seq").val()
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		alert(statusMsg);
		            		location.href='${HOME}/admin/academic/academicReg/curriculumAssign';
		            		//getCurriculumList($("input[name=assign]:checked").val());
		            	}else if(data.status=="201"){
		            		$("#state").html($("#academicNameTitle").text() + "는<br><span class='sp_s'>"+$("#academicState").text()+"</span> 상태입니다.");
		            		$("#pop_pop4").show();
		            	}else{
		            		alert("확정되지 않은 교육과정이 있습니다.\n수업manajemen->rencan kurikulum에서 확정버튼을 눌러주세요.\n"+data.msg);
		            	}
	            		$('#pop_pop3').hide();
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
			
			function popupShow(){
				$("#popState").html($("#academicNameTitle").text()+"<br>교육과정 배정 확정을 batal하시겠습니까?");
				$('#pop_pop3').show();								
			}
			
			function curriculumModify(curr_seq){
				if(confirm("menejemen kurikulum akadmik 페이지로 이동하시겠습니까?"))				
				   post_to_url("${HOME}/admin/academic/curriculum/create", {"curr_seq":curr_seq});
			}
			
			function getAcademic(){
		    	post_to_url('${HOME}/admin/academic/academicReg/academicRegBasicInfo/create', {"aca_seq" : "${S_ACA_SEQ}"});
		    }
			
			function pageMoveScore(type, aca_seq){
		    	$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/admin/academic/academicReg/setAcaSeq",
	                data: {
	                	"aca_seq" : aca_seq
	                    },
	                dataType: "json",
	                success: function(data, status) {
	                	if(type=="curr")
	               			location.href="${HOME}/admin/academic/academicReg/currScore";
		     			else if(type=="soosi")
		     				location.href="${HOME}/admin/academic/academicReg/soosiScore";
		     			else if(type=="fe")
		     				location.href="${HOME}/admin/academic/academicReg/feScore";
	                },
	                error: function(xhr, textStatus) {
	                    alert("실패");
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                },beforeSend:function() {
	                	$.blockUI();
	                },
	                complete:function() {
	                	$.unblockUI();
	                }
	            });
		    }
		</script>
	</head>
	
	<body>
		<input type="hidden" name="aca_seq" id="aca_seq" value="${S_ACA_SEQ }">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
		<!-- s_container_table -->
		<div id="container" class="container_table">
			<!-- s_contents -->
			<div class="contents main">
	
				<!-- s_left_mcon -->
				<div class="left_mcon aside_l grd">
					<div class="sub_menu adm_grd">
						<div class="title">manajemen akademik</div>
						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">sistem management akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">학사코드manajemen</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">menejemen kurikulum akadmik</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">menejemen kurikulum akadmik</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청manajemen</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on" onClick="location.href='${HOME}/admin/academic/academicReg/list'">management registration akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">Manajemen kompetensi kelulusan</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- e_left_mcon -->
	
				<!-- s_main_con -->
				<div class="main_con adm_grd">
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					 <!-- s_tt_wrap -->                
					<div class="tt_wrap">
					    <h3 class="am_tt">
					          <span class="tt">management registration akademik<span class="sign1"> &gt; </span>Penetapan Kurikulum</span>
					    </h3>   
					    <span class="sp_state">status akademik :  <span class="tt" id="academicState"></span></span>      
						<h4 class="h4_tt" id="academicNameTitle"></h4>           
					</div>
					<!-- e_tt_wrap -->
					    
					<!-- s_adm_content3 --> 
					<div class="adm_content3">
					<!-- s_tt_wrap -->                
					
					<!-- s_wrap_wrap --> 
					<div class="wrap_wrap">
					
						<div class="tab_wrap_cc">
							<button class="tab01 tablinks" onclick="getAcademic();">informasi dasar</button>
							<button class="tab01 tablinks active" onclick="location.href='${HOME}/admin/academic/academicReg/curriculumAssign'">Penetapan Kurikulum</button>
							<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/studentAssign'">학생배정</button>
							<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/schedule'">waktu표</button>
							<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/SFSurvey/list'">만족도조사</button>
							<button class="tab01 tablinks" onclick="pageMoveScore('curr','${S_ACA_SEQ }');">종합성적</button>
							<button class="tab01 tablinks" onclick="pageMoveScore('soosi','${S_ACA_SEQ }');">nilai yang diinginkan</button>										
							<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/currReport'">운영보고서</button>						
						</div>
					
						<!-- s_top_tt_wrap --> 
						<div class="top_tt_wrap">
						    <span class="sp03"><span class="sign">■</span>informasi dasar로 registrasi된 교육과정 Keseluruhan 리스트를 제공합니다. 배정할 교육과정을 pilih해 주세요.</span>
						    <span class="sp03"><span class="sign">■</span>교육과정 registrasi정보 perbaiki이 필요할 시, <span class="sp">manajemen akademik &#62; menejemen kurikulum akadmik</span> 메뉴에서 perbaiki해 주세요.</span>
						    <span class="sp03"><span class="sign">■</span>교육과정 명 클릭 시 <span class="sp">manajemen akademik &#62; menejemen kurikulum akadmik</span> 메뉴로 이동됩니다.</span>
						</div>
						<!-- e_top_tt_wrap -->
					
						<!-- s_stitle_wrap --> 
						<div class="stitle_wrap">
						    <!-- <span class="stitle">예과 2tahun ajaran 1semester</span> -->
						    <span class="r_wrap">
						    	<span class="sp01">총 교육과정 : </span>
						    	<span class="sp_n" id="totalCount"></span>
						    	<span class="sp02">건</span>
						    	<span class="sp_sign">/</span>
						    	<span class="sp01">배정된 교육과정 : </span>
						    	<span class="sp_n" id="assignCount"></span>
						    	<span class="sp02">건</span>
						    </span>
						</div>
						<!-- e_stitle_wrap --> 
					
							<c:choose>
								<c:when test='${aca_state.curr_confirm_flag eq "Y" }'>
									<input class="chk01" onClick="getCurriculumList('Y');" name="assign" type="radio" value="Y" checked="checked" style="display:none;">
								</c:when>
								<c:otherwise>
									<div class="sp_wrap1">
									    <input class="chk01" onClick="getCurriculumList('N');" name="assign" type="radio" value="N" checked="checked">
									    <span class="sp04">Keseluruhan 보기</span>
									    <input class="chk01" onClick="getCurriculumList('Y');" name="assign" type="radio" value="Y">
									    <span class="sp04">배정된 교육과정만 보기</span>
								    </div>
								</c:otherwise>								
							</c:choose>
					
		                <table class="mlms_tb assign th">
	                        <tr>
	                            <td rowspan="2" class="th01 w1">배정<br>pilih</td>
	                            <td colspan="2" class="th01 bd01 w5">교육과정</td>
	                            <td rowspan="2" class="th01 w3">책임<br>교수</td>
	                            <td rowspan="2" class="th01 w2">이수<br>구분</td>  
	                            <td rowspan="2" class="th01 w1">nilai</td>
	                            <td rowspan="2" class="th01 w2">manajemen<br>구분</td>
	                            <td rowspan="2" class="th01 w2">Target</td>
	                        </tr>
	                        <tr>
	                            <td class="th01 w2">코드</td>
	                            <td class="th01 w4">Nama Mata Kuliah</td>
	                        </tr>
	                   </table>
					<!-- s_tb_wrap --> 
					              <div class="tb_wrap">       
					                   <table class="mlms_tb assign" id="currListAdd">
					                   </table>
					             </div>
					<!-- e_tb_wrap --> 
					
					</div> 
					<!-- e_wrap_wrap -->      
					   
					<div class="bt_wrap_1">
					    <button class="bt_1_1" name="confirmBtn" onclick="curriculumConfirmInsert('Y');">교육과정 배정 확정</button>
					    <button class="bt_2" name="saveBtn" onClick="curriculumAssignInsert();">교육과정 배정 simpan</button>
					    
					    <div class="bt_swrap">
					    	<button class="bt_0" name="cancleBtn" onclick="popupShow();">교육과정 배정 확정 batal</button>
					    
								<!-- s_pop_pop3 -->
					            <div id="pop_pop3">
					
					                <div class="spop2_2_2_text show" id="spop2_2_3">
					                    <span class="sp">교육과정 확정batal</span>
					                    <span class="sp1" id="popState"></span>
					                    
					                   <button onclick="curriculumConfirmInsert('N');" class="btn01">예</button>
					                   <button class="btn02" onClick="$('#pop_pop3').hide();">아니오</button>
					                   
					                   <span onclick="$('#pop_pop3').hide();" class="pop_close_s" title="닫기" >X</span>
					                </div>
					            </div>
								<!-- e_pop_pop3 --> 
								<!-- s_pop_pop4 -->
					            <div id="pop_pop4">
					
					                <div class="spop2_2_2_text show" id="spop2_2_4">
					                    <span class="sp">교육과정 확정batal</span>
					                    <span class="sp_t">[ 확정batal 불가 안내 ]</span>
					                    <span class="sp1" id="state"></span>
					                    <span class="sp2">해당 학사가 <span class="sp_s">개설</span> 상태인 경우에만<br>확정batal 할 수 있습니다.<br>manajemen자에게 문의해 주세요.</span>
					                    
					                   <button onclick="$('#pop_pop4').hide();" class="btn01">확인</button>
					                   
					                   <span onclick="$('#pop_pop4').hide();" class="pop_close_s" title="닫기">X</span>
					                </div>
					            </div>
								<!-- e_pop_pop4 --> 
					     </div> 
					     
					</div>
					
					</div>
					<!-- e_adm_content3 -->
				</div>
			
			</div>
			<!-- e_contents -->
		</div>
		<!-- e_container_table -->	
	</body>
</html>