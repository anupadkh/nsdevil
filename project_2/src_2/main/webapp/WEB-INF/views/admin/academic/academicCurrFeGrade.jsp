<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<script>
			$(document).ready(function() {
				
				getAcademicTitle();
				getGrade();
			});
			
			function getAcademicTitle(){
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicReg/titleInfo",
		            data: {
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		if(!isEmpty(data.acaInfo.academic_name))
		                		$("#academicNameTitle").text(data.acaInfo.academic_name);
		                	else
		                		$("#academicNameTitle").hide();
		            	
		            		var aca_state = "";
		                	switch(data.acaInfo.aca_state){
	                            case "01" : aca_state = "개설(비공개)";break;
	                            case "02" : aca_state = "개설(공개)";break;
	                            case "03" : aca_state = "학사 중(수업 중)";break;
	                            case "04" : aca_state = "학사종료(미확정)";break;
	                            case "05" : aca_state = "학사종료(성적산정)";break;
	                            case "06" : aca_state = "학사종료(성적발표)";break;
	                            case "07" : aca_state = "성적발표 종료(미확정)";break;
	                            case "08" : aca_state = "학사최종종료(확정)";break;                            
	                        }
			            	$("#academicState").text(aca_state); //타이틀 status akademik
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
			
			function getGrade(){
				var curr_seq = $("#curr_seq").attr("data-value");
				var reg_yn = "";
				
				//성적 미registrasi pilih했을 경우
				if(curr_seq=="N"){
					reg_yn="N";
					curr_seq="";
				}
				
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academicReg/feScore/list",
		            data: {   
		            	"curr_seq" : curr_seq
		            	,"reg_yn" : reg_yn		            	
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		var htmls = "";
		            		
		            		$("#stListAdd").empty();
		            		$("#currListAdd").empty();
		            		$("#scoreListAdd").empty();
						
		            		$.each(data.stList, function(index){
		            			htmls+='<tr name="'+this.id+'">'
								+'<td class="w2"><a href="#" class="go_grd open2" title="해당 학생의 성적표 보기">'+this.id+'</a></td>'
								+'<td class="w3"><a href="#" class="go_grd open2" title="해당 학생의 성적표 보기">'+this.name+'</a></td>'
								+'<td class="w1_1 color1"><span class="ts"></span></td>'
								+'<td class="w1_2 color1"><span class="ts"></span></td>'
								+'</tr>';
		            		});
		            		$("#stListAdd").html(htmls);
		            		/* 
		            		$.each(data.stGradeInfo, function(index){
								$("tr[name="+this.id+"] span.ts").text(this.grade+" / "+this.avg_score);
		            		});
		            		 */
		            		
		            		var currCntHtml = "";
		            		var currInfoHtml = "";
		            		var currPeriodHtml = "";
		            		var currCreateHtml = "";
		            		var scoreState = "";
		            		var scoreClass = "";
		            		$.each(data.feCurrList, function(index){
		            			if(index==0){
		            				currCntHtml+="<tr>";
		            				currInfoHtml+= "<tr>";
				            		currPeriodHtml+= "<tr>";
				            		currCreateHtml+= "<tr>";
		            			}
		            			
		            			currCntHtml+='<td class="th01 w1 bd01"><span class="ts">'+(index+1)+'</span></td>';
		            			currInfoHtml+='<td class="th01 title"><a href="#" class="go_grd" title="해당교육과정 형성penilaian 상세 보기">'+this.curr_name+'</a></td>';
		            			if(this.fe_cnt != 0){
		            				currPeriodHtml+='<td class="th01"><span class="ts open3">'+this.fe_cnt+'회</span></td>';
		            				currCreateHtml+='<td class="th01"><button class="regi2 open1">자필penilaian<br>결과registrasi</button></td>';
		            			}else{
	            					currPeriodHtml+='<td class="th01"><span class="ts">'+this.fe_cnt+'회</span></td>';
	            					currCreateHtml+='<td class="th01">해당 없음</td>';
		            			}
		            			
		            			if(data.feCurrList.length == (index+1)){
		            				currCntHtml+="</tr>";
		            				currInfoHtml+= "</tr>";
				            		currPeriodHtml+= "</tr>";
				            		currCreateHtml+= "</tr>";
		            			}
		            		});
		            		
		            		$("#currListAdd").html(currCntHtml+currInfoHtml+currPeriodHtml+currCreateHtml);
		            		
		            		htmls='';
		            		$.each(data.stList, function(stindex){
		            			var id= this.id;
		            			$.each(data.feCurrList, function(currindex){
		            				if(currindex==0)
		            					htmls='<tr>';
		            					
		            				if(this.fe_cnt==0)
		            					htmls+='<td class="td_1 color2"><span class="ts" data-name="'+id+'_'+this.curr_seq+'"><span class="sp_c">/ 1</span></span></td>';
	            					else
	            						htmls+='<td class="td_1"><span class="ts_1" data-name="'+id+'_'+this.curr_seq+'">해당 없음</span></td>';
	            						
		            				if(data.feCurrList.length == (currindex+1))
										htmls+='</tr>';
		            			});
		            			$("#scoreListAdd").append(htmls);
		            		});
		            		/* 
		            		$.each(data.scoreList, function(index){
		            			$("span[data-name="+this.id+"_"+this.curr_seq+"]").text(this.score+" / "+this.final_grade+ " / " + this.change_score);
		            		}); */
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
			
			function gradeExcelDown(){
				var gradeArray = new Array();
				var currArray = new Array();
				
				$.each($("#stListAdd tr"), function(index){
					var listInfo = new Object();
					listInfo.id = $(this).find("td").eq(0).text();
					listInfo.name = $(this).find("td").eq(1).text();
					listInfo.grInfo = $(this).find("td").eq(2).text();
					var score = "";
					
					//학생꺼 점수들 가져온다.
					$.each($("#scoreListAdd tr:eq("+index+") td"), function(sIndex){
						score += $(this).text();
						
						//마지막 데이터 빼고 구분자 넣어준다.
						if($("#scoreListAdd tr:eq("+index+") td").length != (sIndex+1))
							score += "|";
					});
					listInfo.grade = score;
					gradeArray.push(listInfo);							
				});

				var no = "";
				var curr_name = "";
				var gradeInfo = "";
				$.each($("#currListAdd tr:eq(0) td"), function(index){
					no += $(this).text();
					
					if($("#currListAdd tr:eq(0) td").length != (index+1))
						no += "|";
				});
				
				$.each($("#currListAdd tr:eq(1) td"), function(index){
					curr_name += $(this).text();
					
					if($("#currListAdd tr:eq(1) td").length != (index+1))
						curr_name += "|";
				});
				
				$.each($("#currListAdd tr:eq(2) td"), function(index){
					gradeInfo += $(this).text();
					
					if($("#currListAdd tr:eq(2) td").length != (index+1))
						gradeInfo += "|";
				});
				
				var scoreList = new Object();
				scoreList.list = gradeArray;
				
				var jsonInfo = JSON.stringify(scoreList);
				
				post_to_url("${HOME}/admin/academic/academicReg/currScore/excelDown", {"stList":jsonInfo, "no":no, "curr_name" : curr_name, "gradeInfo":gradeInfo});				
				
			}
			
			function pageMove(curr_seq, curr_name){
				post_to_url("${HOME}/admin/academic/academicReg/currScore/create", {"curr_seq":curr_seq, "curr_name":curr_name});
			}
			
			function getAcademic(){
		    	post_to_url('${HOME}/admin/academic/academicReg/academicRegBasicInfo/create', {"aca_seq" : "${S_ACA_SEQ}"});
		    }
			
			function pageMoveScore(type, aca_seq){
		    	$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/admin/academic/academicReg/setAcaSeq",
	                data: {
	                	"aca_seq" : aca_seq
	                    },
	                dataType: "json",
	                success: function(data, status) {
	                	if(type=="curr")
	               			location.href="${HOME}/admin/academic/academicReg/currScore";
		     			else if(type=="soosi")
		     				location.href="${HOME}/admin/academic/academicReg/soosiScore";
		     			else if(type=="fe")
		     				location.href="${HOME}/admin/academic/academicReg/feScore";
	                },
	                error: function(xhr, textStatus) {
	                    alert("실패");
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                },beforeSend:function() {
	                	$.blockUI();
	                },
	                complete:function() {
	                	$.unblockUI();
	                }
	            });
		    }
			
		</script>
	</head>
	
	<body>
		<input type="hidden" name="aca_seq" id="aca_seq" value="${S_ACA_SEQ }">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
		<!-- s_container_table -->
		<div id="container" class="container_table">
			<!-- s_contents -->
			<div class="contents main">
	
				<!-- s_left_mcon -->
				<div class="left_mcon aside_l grd">
					<div class="sub_menu adm_grd">
						<div class="title">manajemen akademik</div>
						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">sistem management akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">학사코드manajemen</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">menejemen kurikulum akadmik</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">menejemen kurikulum akadmik</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청manajemen</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on" onClick="location.href='${HOME}/admin/academic/academicReg/list'">management registration akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">Manajemen kompetensi kelulusan</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- e_left_mcon -->
	
				<!-- s_main_con -->
				<div class="main_con adm_grd">
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<!-- s_tt_wrap -->
					<div class="tt_wrap">
						<h3 class="am_tt">
							<span class="tt">management registration akademik<span class="sign1"> &gt; </span>형성penilaian</span>
						</h3>
						<span class="sp_state">status akademik : <span class="tt" id="academicState"></span></span>
						<h4 class="h4_tt" id="academicNameTitle"></h4>
					</div>
					<!-- e_tt_wrap -->
	
					<!-- s_adm_content1 -->
					<div class="adm_content3">
						<!-- s_tt_wrap -->
	
						<!-- s_wrap_wrap -->
						<div class="wrap_wrap">
	
							<div class="tab_wrap_cc">							
								<button class="tab01 tablinks" onclick="getAcademic();">informasi dasar</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/curriculumAssign'">Penetapan Kurikulum</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/studentAssign'">학생배정</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/schedule'">waktu표</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/SFSurvey/list'">만족도조사</button>					
								<button class="tab01 tablinks" onclick="pageMoveScore('curr','${S_ACA_SEQ }');">종합성적</button>
								<button class="tab01 tablinks" onclick="pageMoveScore('soosi','${S_ACA_SEQ }');">nilai yang diinginkan</button>
								<button class="tab01 tablinks active" onclick="pageMoveScore('fe','${S_ACA_SEQ }');">형성penilaian</button>		
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/currReport'">운영보고서</button>									
							</div>
	
							<!-- s_wrap_wrap -->
							<div class="wrap_wrap">
	
								<!-- s_sch_wrap -->
								<div class="sch_wrap f1">
									<!-- s_rwrap -->
									<div class="rwrap">
										<!-- s_wrap_s -->
										<div class="wrap_s">
											<div class="wrap_s1_uselectbox">
												<div class="uselectbox">
													<span class="uselected" id="curr_seq" data-value="">Keseluruhan</span> <span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;">
														<span class="uoption opt1" data-value="">Keseluruhan 종합성적</span> 
														<span class="uoption opt1" data-value="N">성적 미registrasi</span>
														<c:forEach var="currList" items="${currList}">
															<span class="uoption" data-value="${currList.curr_seq}">${currList.curr_name}</span>
						                                </c:forEach>
														
													</div>
												</div>
											</div>
											<button class="btn_tt1" onClick="getGrade();">Pencarian</button>
											<button class="btn_down2" onClick="gradeExcelDown();">성적표 다운로드</button>
	
											<!-- s_r_wrap -->
											<div class="r_wrap">
												<div class="sttwrap">
													<span class="sp01">총</span>
													<span class="sp_n" data-name="totalCurr"></span>
													<span class="sp02">개 교육과정</span>
												</div>
												<div class="sttwrap">
													<span class="twrap3">
														<span class="sp01">형성penilaian</span>
														<span class="sp02">시행</span>
														<span class="sp_n"></span>
														<span class="sp02">건</span>
													</span>
												</div>
											</div>
											<!-- e_r_wrap -->
										</div>
										<!-- e_wrap_s -->
	
									</div>
									<!-- e_rwrap -->
								</div>
								<!-- e_sch_wrap -->


							<div class="scroll_wraps">
								<div class="fwrap1">
									<table class="mlms_tbs">
										<tr>
											<td colspan="2" rowspan="3" class="th01 bd01 w4">
												<span class="ts1">Nama Mata Kuliah</span> 
												<span class="ts2">학생정보 </span> 
												<svg class="svg1">
													<line x1="0" y1="0" x2="100%" y2="100%"></line>
                            					</svg>
                            				</td>
											<td class="th01 bd01 w1" colspan="2"><span class="ts">N0.</span></td>
										</tr>
										<tr>
											<td class="th01 bd01 w1 title" colspan="2"><span class="ts">Nama Mata Kuliah</span></td>
										</tr>
										<tr>
											<td class="th01 bd01 w1" colspan="2"><span class="ts" style="letter-spacing:-2px;">형성penilaian 횟수<br>(지필시험 등 포함)</span></td>
										</tr>
										<tr>
											<td class="th01 w2"><span class="ts">학번</span></td>
											<td class="th01 w3"><span class="ts">이름</span></td>
											<td class="th01 w1_1 color1"><span class="ts">총점</span></td>
											<td class="th01 w1_2 color1"><span class="ts">석차</span></td>
										</tr>
										
										<tbody id="stListAdd">									
										</tbody>	
										
									</table>
								</div>
								<div class="fwrap2">
									<table class="mlms_tb">
										<tbody id="currListAdd">
									
										</tbody>
										<tbody id="scoreListAdd">
									</table>
								</div>
							</div>

						</div>
							<!-- e_wrap_wrap -->
						</div>
					</div>
				</div>
			</div>
		</div>			
	</body>
</html>