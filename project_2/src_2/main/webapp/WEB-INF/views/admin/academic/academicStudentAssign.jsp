<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<script>
			$(document).ready(function() {
				acasystemStageOfList(1);
				getAssignStCnt();
				getAcademicTitle();
			});
			
			function getAcademicTitle(){
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicReg/titleInfo",
		            data: {                      
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		if(!isEmpty(data.acaInfo.academic_name))
		                		$("#academicNameTitle").text(data.acaInfo.academic_name);
		                	else
		                		$("#academicNameTitle").hide();
		            	
		            		var aca_state = "";
		                	switch(data.acaInfo.aca_state){
	                            case "01" : aca_state = "개설(비공개)";break;
	                            case "02" : aca_state = "개설(공개)";break;
	                            case "03" : aca_state = "학사 중(수업 중)";break;
	                            case "04" : aca_state = "학사종료(미확정)";break;
	                            case "05" : aca_state = "학사종료(성적산정)";break;
	                            case "06" : aca_state = "학사종료(성적발표)";break;
	                            case "07" : aca_state = "성적발표 종료(미확정)";break;
	                            case "08" : aca_state = "학사최종종료(확정)";break;                            
	                        }
			            	$("#academicState").text(aca_state); //타이틀 status akademik
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
			
			function getSTList(type){
				var searchType = "";
				
				if($("button[name=searchBtn]").hasClass("on")){
					searchType = "search";
				}else{
					searchType = "assign";
				}
				
				var sid = "", eid = "", id="", st_name = "", year = "", laca_seq = "", maca_seq = "", saca_seq = "", aca_system_seq = "";
				
				if(type == 1){
					sid=$("input[name=sid]").val();
					eid=$("input[name=eid]").val();
				}else if(type == 2){
					id=$("input[name=id]").val();
					st_name=$("input[name=st_name]").val();
				}else if(type == 3){
					laca_seq = $("#l_seq").attr("data-value");
					maca_seq = $("#m_seq").attr("data-value");
					saca_seq = $("#s_seq").attr("data-value");
					aca_system_seq = $("#aca_seq").attr("data-value");
					year = $("#year").attr("data-value");
				}
				
		    	$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/admin/academic/academicReg/studentAssign/stlist",
	                data: {
	                	"sid" : sid
	                	,"eid" : eid
	                	,"id" : id
	                	,"st_name" : st_name
	                	,"year" : year
	                	,"laca_seq" : laca_seq
	                	,"maca_seq" : maca_seq
	                	,"saca_seq" : saca_seq
	                	,"aca_system_seq" : aca_system_seq
	                	,"searchType" : searchType
	                    },
	                dataType: "json",
	                success: function(data, status) {
	                	if(data.status == "200"){
	                		if(searchType == "search")
	                			$("span[data-name=searchStCount]").text(data.list.length);
	                		else
	                			$("span[data-name=searchStCount]").text(0);
	                		//ts1 재학, ts2 휴학, ts3 졸업, ts4 퇴학
	                		
	                		var htmls = '';
	                		var st_attend = "";
	                		var st_attend_class = ["ts1","ts2","ts2","ts4","ts3","ts1"];
	                			                		
	                		$("#stListAdd").empty();
	                		
	                		$.each(data.list, function(index){
		                		
		                		if(isEmpty(this.st_attend_code))
		                			st_attend = st_attend_class[this.st_attend_code];
		                		
	                			htmls = '<tr><td class="td_1">';
	                			if("${aca_state.student_confirm_flag}"=="N")
									htmls+='<input type="checkbox" class="ip_chk1a" name="chk" value="'+this.user_seq+'">';
								htmls+='</td><td class="td_1">'+(index+1)+'</td>'
									+'<td class="td_1">'+this.id+'</td>'
									+'<td class="td_1">'+this.name+'</td>'
									+'<td class="td_1">'+this.year+'</td>'
									+'<td class="td_1">'+this.laca_name+'</td>'
									+'<td class="td_1">'+this.maca_name+'</td>'
									+'<td class="td_1">'+this.saca_name+'</td>'
									+'<td class="td_1">'+this.aca_name+'</td>'
									+'<td class="td_1">'
									+'<span class="'+st_attend+'">'+this.code_name+'</span>'
									+'</td>'
									+'</tr>'	
								$("#stListAdd").append(htmls);
	                		});
	                		
	                	}else{
	                		alert("학생 리스트 가져오기 실패");
	                	}
	                },
	                error: function(xhr, textStatus) {
	                    alert("학생 리스트 가져오는중 오류가 발생하였습니다.");
	                    document.write(xhr.responseText);
	                },beforeSend:function() {
	                	$.blockUI();
	                },
	                complete:function() {
	                	$.unblockUI();
	                }
	            });
		    }
			
			function getAssignStCnt(){				
		    	$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/admin/academic/academicReg/studentAssign/assignStCnt",
	                data: {
	                    },
	                dataType: "json",
	                success: function(data, status) {
	                	if(data.status == "200"){
	                		$("span[data-name=assignStCount]").text(data.cnt);	                		
	                	}else{
	                		alert("배정된 학생 수 가져오기 실패");
	                	}
	                },
	                error: function(xhr, textStatus) {
	                    alert("배정된 학생 수 가져오는중 오류가 발생하였습니다.");
	                    document.write(xhr.responseText);
	                },beforeSend:function() {
	                	$.blockUI();
	                },
	                complete:function() {
	                	$.unblockUI();
	                }
	            });
		    }

			function acasystemStageOfList(level, l_seq, m_seq, s_seq){
                if(level == ""){
                    alert("분류 없음");
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "${HOME}/ajax/admin/academic/academicSystem/acaStageOfList",
                    data: {
                        "level" : level,
                        "l_seq" : l_seq,                       
                        "m_seq" : m_seq,
                        "s_seq" : s_seq
                        },
                    dataType: "json",
                    success: function(data, status) {
                        var htmls = '';
                        
                        if(level == 1){
                        	htmls = '<span class="uoption firstseleted" data-value="" onClick="lSeqSet(0);">fakultas</span>';
                        	$.each(data.list, function(index){
                                htmls +=  '<span class="uoption" data-value="'+this.aca_system_seq+'" onClick="lSeqSet('+this.aca_system_seq+');">'+this.aca_system_name+'</span>';
                            });
                            $("#lacaListAdd").html(htmls); 
                            
                            if(!isEmpty(getCookie("l_seq"))){
                            	if($("#lacaListAdd span[data-value="+getCookie("l_seq")+"]").length != 0)
                            		$("#lacaListAdd span[data-value="+getCookie("l_seq")+"]").click();
                            	else
                            		$("#lacaListAdd span:eq(0)").click();
                            }else{
                            	$("#lacaListAdd span:eq(0)").click();
                            }
                        }                      
                        else if(level == 2){
                        	htmls = '<span class="uoption firstseleted" data-value="" onClick="mSeqSet(0);">jurusan</span>';
                        	$.each(data.list, function(index){
                                htmls +=  '<span class="uoption" data-value="'+this.aca_system_seq+'" onClick="mSeqSet('+this.aca_system_seq+');">'+this.aca_system_name+'</span>';
                            });
                            $("#macaListAdd").html(htmls);
                            
                            if(!isEmpty(getCookie("m_seq"))){
                            	if($("#macaListAdd span[data-value="+getCookie("m_seq")+"]").length != 0)
                            		$("#macaListAdd span[data-value="+getCookie("m_seq")+"]").click();
                            	else
                            		$("#macaListAdd span:eq(0)").click();
                            }else{
                            	$("#macaListAdd span:eq(0)").click();
                            }
                        }else if(level == 3){
                        	htmls = '<span class="uoption firstseleted" data-value="" onClick="sSeqSet(0);">tahun ajaran</span>';
                        	$.each(data.list, function(index){
                                htmls +=  '<span class="uoption" data-value="'+this.aca_system_seq+'" onClick="sSeqSet('+this.aca_system_seq+');">'+this.aca_system_name+'</span>';
                            });
                            $("#sacaListAdd").html(htmls);
                            
                            if(!isEmpty(getCookie("s_seq"))){
                            	if($("#sacaListAdd span[data-value="+getCookie("s_seq")+"]").length != 0)
                            		$("#sacaListAdd span[data-value="+getCookie("s_seq")+"]").click();
                            	else
                            		$("#sacaListAdd span:eq(0)").click();
                            }else{
                            	$("#sacaListAdd span:eq(0)").click();
                            }                            
                        }else{
                        	htmls = '<span class="uoption firstseleted" data-value="">periode</span>';
                        	$.each(data.list, function(index){
                                htmls +=  '<span class="uoption" data-value="'+this.aca_system_seq+'">'+this.aca_system_name+'</span>';
                            });
                            $("#acaListAdd").html(htmls);

                            if(!isEmpty(getCookie("aca_seq"))){
                            	if($("#acaListAdd span[data-value="+getCookie("aca_seq")+"]").length != 0)
                            		$("#acaListAdd span[data-value="+getCookie("aca_seq")+"]").click();
                            	else
                            		$("#acaListAdd span:eq(0)").click();
                            }else{
                            	$("#acaListAdd span:eq(0)").click();
                            }
                        }
                    },
                    error: function(xhr, textStatus) {
                        //alert("오류가 발생했습니다.");
                        document.write(xhr.responseText);
                    },beforeSend:function() {
                    },
                    complete:function() {
                    }
                }); 
           	}
			
			function lSeqSet(seq){
				acasystemStageOfList(2, seq);
                $("#m_seq").text("jurusan");
                $("#m_seq").attr("data-value","");
                $("#s_seq").text("tahun ajaran");
                $("#s_seq").attr("data-value","");
                $("#aca_seq").text("periode");
                $("#aca_seq").attr("data-value","");
			}
			
			function mSeqSet(seq){
				acasystemStageOfList(3, "", seq);
                $("#s_seq").text("tahun ajaran");
                $("#s_seq").attr("value","");
                $("#aca_seq").text("periode");
                $("#aca_seq").attr("data-value","");
			}
			
			function sSeqSet(seq){
				acasystemStageOfList(4, "", "", seq);
                $("#aca_seq").text("periode");
                $("#aca_seq").attr("data-value","");
			}
			
			function pageMoveScore(type, aca_seq){
		    	$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/admin/academic/academicReg/setAcaSeq",
	                data: {
	                	"aca_seq" : aca_seq
	                    },
	                dataType: "json",
	                success: function(data, status) {
	                	if(type=="curr")
	               			location.href="${HOME}/admin/academic/academicReg/currScore";
		     			else if(type=="soosi")
		     				location.href="${HOME}/admin/academic/academicReg/soosiScore";
		     			else if(type=="fe")
		     				location.href="${HOME}/admin/academic/academicReg/feScore";
	                },
	                error: function(xhr, textStatus) {
	                    alert("실패");
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                },beforeSend:function() {
	                	$.blockUI();
	                },
	                complete:function() {
	                	$.unblockUI();
	                }
	            });
		    }
			
			function getAcademic(){
		    	post_to_url('${HOME}/admin/academic/academicReg/academicRegBasicInfo/create', {"aca_seq" : "${S_ACA_SEQ}"});
		    }
			
			function tabChange(type){
				if(type=="1"){
					$("button[name=searchBtn]").addClass("on");
					$("button[name=assignBtn]").removeClass("on");
					$("button[name=assignSubmit]").show();
					$("button[name=assignCancel]").hide();
					$("div.bt_wrap").hide();
				}else{
					$("button[name=searchBtn]").removeClass("on");
					$("button[name=assignBtn]").addClass("on");
					$("button[name=assignSubmit]").hide();
					$("button[name=assignCancel]").show();
					$("div.bt_wrap").show();
				}
				getSTList();
			}
			
			function checkAll(){
			      if( $("#th_checkAll").is(':checked') ){
			        $("input[name=chk]").prop("checked", true);
			      }else{
			        $("input[name=chk]").prop("checked", false);
			      }
			}
			
			function assignStSubmit(){
				
				if($("input[name=chk]:checked").length == 0){
					alert("배정할 학생을 pilih해주세요.");
					return;
				}
				
				if(!confirm("pilih한 학생들을 배정하시겠습니까?")){
					return;
				}
				
				var user_seq = "";
				
				$.each($("input[name=chk]:checked"), function(index){
					if(index != 0)
						user_seq += ",";

					user_seq += $(this).val();
				});
				
				$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/admin/academic/academicReg/studentAssign/insert",
	                data: {
	                	"user_seq" : user_seq
	                    },
	                dataType: "json",
	                success: function(data, status) {
	                	if(data.status=="200"){
	                		alert("배정 완료하였습니다.");
	                		tabChange(2);
	        				getAssignStCnt();
	                	}else{
	                		alert("배정 실패하였습니다.");
	                	}	                		
	                },
	                error: function(xhr, textStatus) {
	                    alert("실패");
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                },beforeSend:function() {
	                	$.blockUI();
	                },
	                complete:function() {
	                	$.unblockUI();
	                }
	            });
			}
			
			function assignStCancel(){
				if($("input[name=chk]:checked").length == 0){
					alert("배정 batal할 학생을 pilih해주세요.");
					return;
				}
				
				if(!confirm("pilih한 학생들을 배정batal 하시겠습니까?")){
					return;
				}
				
				var user_seq = "";
				
				$.each($("input[name=chk]:checked"), function(index){
					if(index != 0)
						user_seq += ",";

					user_seq += $(this).val();
				});
				
				$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/admin/academic/academicReg/studentAssign/cancel",
	                data: {
	                	"user_seq" : user_seq
	                },
	                dataType: "json",
	                success: function(data, status) {
	                	if(data.status=="200"){
	                		alert("배정 batal 완료하였습니다.");
	                		tabChange(2);
	        				getAssignStCnt();
	                	}else{
	                		alert("배정 batal 실패하였습니다.");
	                	}	                		
	                },
	                error: function(xhr, textStatus) {
	                    alert("실패");
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                },beforeSend:function() {
	                	$.blockUI();
	                },
	                complete:function() {
	                	$.unblockUI();
	                }
	            });
			}
			
			function confirmSubmit(){
				
				if($("#stListAdd tr").length == 0){
					alert("확정할 학생을 pilih해주세요.");
					return;
				}
				
				if(!confirm("확정 하시겠습니까?")){
					return;
				}
				
				var user_seq = "";
				
				$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/admin/academic/academicReg/studentAssign/confirm",
	                data: {
	                	
	                },
	                dataType: "json",
	                success: function(data, status) {
	                	if(data.status=="200"){
	                		alert("확정 완료하였습니다.");
	                		location.reload();
	                	}else{
	                		alert("확정 실패하였습니다.");
	                	}	                		
	                },
	                error: function(xhr, textStatus) {
	                    alert("실패");
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                },beforeSend:function() {
	                	$.blockUI();
	                },
	                complete:function() {
	                	$.unblockUI();
	                }
	            });
			}
			
			function cancelConfirm(){
								
				if(!confirm("확정 batal 하시겠습니까?")){
					return;
				}
				
				var user_seq = "";
				
				$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/admin/academic/academicReg/studentAssign/cancelConfirm",
	                data: {
	                	
	                },
	                dataType: "json",
	                success: function(data, status) {
	                	if(data.status=="200"){
	                		alert("확정 batal 완료하였습니다.");
	                		location.reload();
	                	}else{
	                		alert("확정 batal 실패하였습니다.");
	                	}	                		
	                },
	                error: function(xhr, textStatus) {
	                    alert("실패");
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                },beforeSend:function() {
	                	$.blockUI();
	                },
	                complete:function() {
	                	$.unblockUI();
	                }
	            });
			}
		</script>
	</head>
	
	<body>
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
		<!-- s_container_table -->
		<div id="container" class="container_table">
			<!-- s_contents -->
			<div class="contents main">
	
				<!-- s_left_mcon -->
				<div class="left_mcon aside_l grd">
					<div class="sub_menu adm_grd">
						<div class="title">manajemen akademik</div>
						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">sistem management akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">학사코드manajemen</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">menejemen kurikulum akadmik</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">menejemen kurikulum akadmik</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청manajemen</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on" onClick="location.href='${HOME}/admin/academic/academicReg/list'">management registration akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">Manajemen kompetensi kelulusan</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- e_left_mcon -->
	
				<!-- s_main_con -->
				<div class="main_con user adm_grd">
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<!-- s_tt_wrap -->
					<div class="tt_wrap">
						<h3 class="am_tt">
							<span class="tt">management registration akademik<span class="sign1"> &gt; </span>informasi dasar</span>
						</h3>
						<span class="sp_state">status akademik : <span class="tt" id="academicState"></span></span>
						<h4 class="h4_tt" id="academicNameTitle"></h4>
					</div>
					<!-- e_tt_wrap -->
	
					<!-- s_adm_content3 -->
					<div class="adm_content3 user add">
						<!-- s_tt_wrap -->
	
						<!-- s_wrap_wrap -->
						<div class="wrap_wrap">
							
							<div class="tab_wrap_cc">											
								<c:choose>
									<c:when test="${S_ACA_SEQ ne null}">		
										<button class="tab01 tablinks" onclick="getAcademic();">informasi dasar</button>
										<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/curriculumAssign'">Penetapan Kurikulum</button>
										<button class="tab01 tablinks active" onclick="location.href='${HOME}/admin/academic/academicReg/curriculumAssign'">학생배정</button>
										<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/schedule'">waktu표</button>
										<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/SFSurvey/list'">만족도조사</button>
										<button class="tab01 tablinks" onclick="pageMoveScore('curr','${S_ACA_SEQ }');">종합성적</button>
										<button class="tab01 tablinks" onclick="pageMoveScore('soosi','${S_ACA_SEQ }');">nilai yang diinginkan</button>										
										<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/currReport'">운영보고서</button>		
									</c:when>
									<c:otherwise>
										<button class="tab01 tablinks" onclick="">informasi dasar</button>
										<button class="tab01 tablinks" onclick="alert('pilih된 학사가 없습니다.\n학사 informasi dasar를 먼저 registrasi하세요.');">Penetapan Kurikulum</button>
										<button class="tab01 tablinks active" onclick="alert('pilih된 학사가 없습니다.\n학사 informasi dasar를 먼저 registrasi하세요.');">학생배정</button>
										<button class="tab01 tablinks" onclick="alert('pilih된 학사가 없습니다.\n학사 informasi dasar를 먼저 registrasi하세요.');">waktu표</button>
										<button class="tab01 tablinks" onclick="alert('pilih된 학사가 없습니다.\n학사 informasi dasar를 먼저 registrasi하세요.');">수업만족도</button>										
										<button class="tab01 tablinks" onclick="alert('pilih된 학사가 없습니다.\n학사 informasi dasar를 먼저 registrasi하세요.');">종합성적</button>
										<button class="tab01 tablinks" onclick="alert('pilih된 학사가 없습니다.\n학사 informasi dasar를 먼저 registrasi하세요.');">nilai yang diinginkan</button>
										<button class="tab01 tablinks" onclick="alert('pilih된 학사가 없습니다.\n학사 informasi dasar를 먼저 registrasi하세요.');">운영보고서</button>	
									</c:otherwise>
								</c:choose>
							</div>
	
							<span class="tt1">※ 학생 registrasi</span>
							<!-- s_sch_wrap -->
							<div class="sch_wrap sys">
								<!-- s_rwrap -->
								<div class="rwrap">
	
									<!-- s_wrap_s -->
									<div class="wrap_s a1st">
										<div class="t1">
											<span class="tts">학번구간Pencarian (1)</span>
										</div>
	
										<div class="wrap_ss">
											<div class="ss1">
	
												<span class="tt3">학번</span> 
												<input class="ip_tt_1" value="" name="sid" type="text" onkeypress="javascript:if(event.keyCode==13){getSTList(1); return false;}"> 
												<span class="sign">~</span> 
												<input class="ip_tt_1" value="" name="eid" type="text" onkeypress="javascript:if(event.keyCode==13){getSTList(1); return false;}">
												<button class="btn_tt2" onClick="getSTList(1)">Pencarian</button>
											</div>
										</div>
	
									</div>
									<!-- e_wrap_s -->
	
									<!-- s_wrap_s -->
									<div class="wrap_s a1st">
										<div class="t1">
											<span class="tts">학생개별Pencarian (2)</span>
										</div>
	
										<div class="wrap_ss">
											<div class="ss1">	
												<span class="tt3">학번</span> 
												<input class="ip_tt_1" value="" name="id" type="text" onkeypress="javascript:if(event.keyCode==13){getSTList(2); return false;}"> 
												<span class="tt3">이름</span> 
												<input class="ip_tt_1" value="" name="st_name" type="text" onkeypress="javascript:if(event.keyCode==13){getSTList(2); return false;}">
												<button class="btn_tt2" onClick="getSTList(2)">Pencarian</button>
											</div>
										</div>
	
									</div>
									<!-- e_wrap_s -->
	
									<!-- s_wrap_s -->
									<div class="wrap_s a1st add1">
										<div class="t1">
											<span class="tts add1a">다른 sistem akademik</span>
											<span class="tts add1a">학생그룹tambahkan (3)</span>
										</div>
	
										<div class="wrap_ss">
											<div class="ss1">
												<div class="wrap_s1_uselectbox">
													<div class="uselectbox">
														<span class="uselected" data-value="" id="year">pilih</span> <span class="uarrow">▼</span>
														<div class="uoptions" style="display: none;">
															<span class="uoption" data-value="">pilih</span>
															<c:forEach var="academicYearList" items="${academicYearList}">
			                                                    <span class="uoption" data-value="${academicYearList.year}">${academicYearList.year}</span>
			                                                </c:forEach>
														</div>
													</div>
												</div>
	
												<div class="wrap_s2_uselectbox">
													<div class="uselectbox">
														<span class="uselected" id="l_seq" data-value="">fakultas</span> <span class="uarrow">▼</span>

														<div class="uoptions" id="lacaListAdd">
														</div>
													</div>
												</div>
	
												<div class="wrap_s2_uselectbox">
													<div class="uselectbox">
														<span class="uselected" id="m_seq" data-value="">jurusan</span> <span class="uarrow">▼</span>
							
														<div class="uoptions" id="macaListAdd">
														</div>
													</div>
												</div>
	
												<div class="wrap_s2_uselectbox">
													<div class="uselectbox">
														<span class="uselected" id="s_seq" data-value="">tahun ajaran</span> <span class="uarrow">▼</span>
							
														<div class="uoptions" id="sacaListAdd">
														</div>
													</div>
												</div>
	
												<div class="wrap_s1_uselectbox">
													<div class="uselectbox">
														<span class="uselected" id="aca_seq" data-value="">periode</span> <span class="uarrow">▼</span>
							
														<div class="uoptions" id="acaListAdd">
														</div>
													</div>
												</div>
	
												<button class="btn_tt2" onClick="getSTList(3)">Pencarian</button>
											</div>
										</div>
									</div>
									<!-- e_wrap_s -->
	
								</div>
								<!-- e_rwrap -->
							</div>
							<!-- s_sch_wrap -->
	
							<!-- s_btnwrap_s1 -->
							<div class="btnwrap_s1">
								<button class="tt_g on" onClick="tabChange(1);" name="searchBtn">
									<span class="g_t2">Pencarian결과</span>
									<span class="g_t3">총</span>
									<span class="g_num" data-name="searchStCount"></span>
									<span class="g_t3">명</span>
								</button>
	
								<button class="tt_g" onClick="tabChange(2);" name="assignBtn">
									<span class="g_t2">배정된 학생</span>
									<span class="g_t3">총</span>
									<span class="g_num" data-name="assignStCount"></span>
									<span class="g_t3">명</span>
								</button>
							</div>
							<!-- e_btnwrap_s1 -->
						</div>
						<!-- e_wrap_wrap -->
	
						<!-- s_wrap_wrap -->
						<div class="wrap_wrap">
							<!-- s_btnwrap_s2 -->
							<div class="btnwrap_s2 sys">
								<c:if test='${aca_state.student_confirm_flag eq "N" }'>
									<button class="btnr1p" name="assignSubmit" onclick="assignStSubmit();">pilih 배정하기</button>
									<button class="btnr1px" style="display:none;" onclick="assignStCancel();" name="assignCancel">pilih 배정batal</button>
								</c:if>
							</div>
							<!-- e_btnwrap_s2 -->
	
							<table class="mlms_tb assign">
								<thead>
									<tr>
										<th rowspan="2" class="th01 bd01 w1">
											<div class="th_wrap pd1">
												Keseluruhan<br>pilih
											</div> <input type="checkbox" class="ip_chk1a" name="checkAll" id="th_checkAll" onclick="checkAll();">
										</th>
										<th rowspan="2" class="th01 bd01 w1">No.</th>
										<th rowspan="2" class="th01 bd01 w3">
											<div class="th_wrap pd1">학번</div>
											<button class="down">▼</button>
										</th>
										<th rowspan="2" class="th01 bd01 w5">
											<div class="th_wrap pd1">이름</div>
											<button class="down">▼</button>
										</th>
										<th colspan="5" class="th01 bd01 w2">sistem akademik</th>
										<th rowspan="2" class="th01 bd01 w2">
											<div class="th_wrap pd1">
												학사<br>상태
											</div>
											<button class="down">▼</button>
										</th>
									</tr>
									<tr>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">tahun</div>
											<button class="down">▼</button>
										</th>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">fakultas</div>
											<button class="down">▼</button>
										</th>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">jurusan</div>
											<button class="down">▼</button>
										</th>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">tahun ajaran</div>
											<button class="down">▼</button>
										</th>
										<th class="th01 bd01 w2">
											<div class="th_wrap pd2">periode</div>
											<button class="down">▼</button>
										</th>
									</tr>
								</thead>
								<tbody id="stListAdd">
									
								</tbody>
							</table>
	
						</div>
						<!-- e_wrap_wrap -->
						
						<c:if test='${aca_state.student_confirm_flag eq "N" }'>
							<div class="bt_wrap" style="display:none;">
								<button class="bt_2" onclick="confirmSubmit();">배정 확정</button>
							</div>
						</c:if>
						<c:if test='${aca_state.student_confirm_flag eq "Y" }'>
							<div class="bt_wrap" style="display:none;">
								<button class="bt_3" onclick="cancelConfirm();">확정 batal</button>
							</div>
						</c:if>
					</div>
					<!-- e_adm_content3 -->
				</div>
				<!-- e_main_con -->
	
			</div>
			<!-- e_contents -->
		</div>
		<!-- e_container_table -->	
	</body>
</html>