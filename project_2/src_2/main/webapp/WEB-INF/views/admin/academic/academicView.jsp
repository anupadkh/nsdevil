<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
	<script>
		$(document).ready(function() {
			acasystemStageOfList(1);
			
			getAcademicList(1);
		});
		
		function getAcaSystem(level, seq){
			if(level == 1){
				$("#m_seq span.uselected").html("Keseluruhan");
                $("#m_seq").attr("value","");
                acasystemStageOfList(2, seq);
			}else if(level == 2){
                acasystemStageOfList(3, $("#l_seq").attr("value"), seq);
			}	
            $("#s_seq span.uselected").html("Keseluruhan");
            $("#s_seq").attr("value","");			
		}
		
		//sistem akademik 분류별 가져오기
	    function acasystemStageOfList(level, l_seq, m_seq, s_seq){
	        if(level == ""){
	            alert("분류 없음");
	            return;                 
	        }
	        $.ajax({
	            type: "POST",
	            url: "${HOME}/ajax/admin/academic/academicSystem/acaStageOfList",
	            data: {
	                "level" : level,
	                "l_seq" : l_seq,
	                "m_seq" : m_seq,
	                "s_seq" : s_seq                        
	            },
	            dataType: "json",
	            success: function(data, status) {
	                var htmls = '<span class="uoption firstseleted" onClick="getAcaSystem('+level+',\'\');" value="">Keseluruhan</span>';
	                $.each(data.list, function(index){
	                    htmls +=  '<span class="uoption" onClick="getAcaSystem('+level+','+this.aca_system_seq+');" value="'+this.aca_system_seq+'">'+this.aca_system_name+'</span>';                         
	                });
	                if(level == 1){
	                    $("#l_seq_add span").remove();
	                    $("#l_seq_add").append(htmls);                          
	                }
	                else if(level == 2){
	                    $("#m_seq_add span").remove();
	                    $("#m_seq_add").append(htmls);
	                }
	                else{
	                    $("#s_seq_add span").remove();
	                    $("#s_seq_add").append(htmls);
	                }
	            },
	            error: function(xhr, textStatus) {
	                document.write(xhr.responseText); 
	                $.unblockUI();                        
	            },beforeSend:function() {
	                $.blockUI();                        
	            },complete:function() {
	                $.unblockUI();                         
	            }                   
	        });                 
	    }
		
	    function getAcademicList(page){              
            $.ajax({
                type: "POST",
                url: "${HOME}/ajax/admin/academic/academicReg/list",
                data: {
                    "page" : page,
                    "l_seq" : $("#l_seq").attr("value"),                       
                    "m_seq" : $("#m_seq").attr("value"),
                    "s_seq" : $("#s_seq").attr("value"),
                    "academic_name" : $("#aca_name").val(),
                    "aca_state" : $("#aca_state").attr("value"),
                    "year" : $("#year").attr("value")
                    },
                dataType: "json",
                success: function(data, status) {
                    $("#academic_list_add tr").remove();   
                    
                    $.each(data.list, function(index){
                        var htmls = '';
                        var grade = '';
                        var aca_state = '';                       
                        switch(this.aca_state){
                            case "01" : aca_state = "개설<br>(비공개)";break;
                            case "02" : aca_state = "개설<br>(공개)";break;
                            case "03" : aca_state = "학사 중<br>(수업 중)";break;
                            case "04" : aca_state = "학사종료<br>(미확정)";break;
                            case "05" : aca_state = "학사종료<br>(성적산정)";break;
                            case "06" : aca_state = "학사종료<br>(성적발표)";break;
                            case "07" : aca_state = "성적발표 종료<br>(미확정)";break;
                            case "08" : aca_state = "학사최종종료<br>(확정)";break;                            
                        }
                    
                        htmls += '<tr>';
                        htmls += '<td class="td_1"><input type="checkbox" class="chk01" name="checkRow" value="'+this.aca_seq+'"></td>';
                        htmls += '<td class="td_1">'+this.year+'</td>';
                        htmls += '<td class="td_1">'+this.l_aca_name+'</td>';
                        htmls += '<td class="td_1">'+this.m_aca_name+'</td>';
                        htmls += '<td class="td_1">'+this.s_aca_name+'</td>';
                        htmls += '<td class="td_1">'+this.aca_system_name+'</td>';
                        htmls += '<td class="td_1 ft13">'+this.start_date+'~<br>'+this.end_date+'</td>';
                        htmls += '<td class="td_1 ft13 fcolor01" onClick="getAcademic('+this.aca_seq+');" style="cursor:pointer;">'+this.aca_name+'</td>';
                        htmls += '<td class="td_1 fcolor01">'+aca_state+'</td>';
                        if(this.curr_cnt == 0)
                        	htmls += '<td class="td_1 fcolor02">미배정</td>';
                       	else
                       		htmls += '<td class="td_1">'+this.curr_cnt+'</td>';
                       	if(this.curr_plan_cnt == 0)
                       		htmls += '<td class="td_1">-</td>';
                    	else
                  			htmls += '<td class="td_1">'+this.curr_plan_cnt+'</td>';
                  			
                  		htmls += '<td class="td_1"> <button class="btn2_1" onclick="pageMoveScore(\'curr\','+this.aca_seq+');">manajemen</button></td>';
                  		htmls += '<td class="td_1"> <button class="btn2_1" onclick="pageMoveScore(\'soosi\','+this.aca_seq+');">manajemen</button></td>';
                  		htmls += '<td class="td_1"><button class="btn2_1" onclick="pageMoveScore(\'fe\','+this.aca_seq+');">manajemen</button></td>';
                 		htmls += '<td class="td_1"><button class="btn2_2" onclick="">조회</button></td>';                        
                        htmls += '</tr>';                        
                        $("#academic_list_add").append(htmls);
                    }); 
                    $("#pagingBtnAdd").html(data.pageNav);
                },
                error: function(xhr, textStatus) {
                    alert("실패");
                    //alert("오류가 발생했습니다.");
                    document.write(xhr.responseText);
                },beforeSend:function() {
                },
                complete:function() {
                }
            }); 
        }
	    
	    function getAcademic(aca_seq){
	    	post_to_url('${HOME}/admin/academic/academicReg/academicRegBasicInfo/create', {"aca_seq" : aca_seq});
	    }
	    
	    function pageMoveScore(type, aca_seq){
	    	$.ajax({
                type: "POST",
                url: "${HOME}/ajax/admin/academic/academicReg/setAcaSeq",
                data: {
                	"aca_seq" : aca_seq
                    },
                dataType: "json",
                success: function(data, status) {
                	if(type=="curr")
               			location.href="${HOME}/admin/academic/academicReg/currScore";
	     			else if(type=="soosi")
	     				location.href="${HOME}/admin/academic/academicReg/soosiScore";
	     			else if(type=="fe")
	     				location.href="${HOME}/admin/academic/academicReg/feScore";
                },
                error: function(xhr, textStatus) {
                    alert("실패");
                    //alert("오류가 발생했습니다.");
                    document.write(xhr.responseText);
                },beforeSend:function() {
                	$.blockUI();
                },
                complete:function() {
                	$.unblockUI();
                }
            });
	    }
	    
	    function reset(){
	    	$("#year_add span").eq(0).click().change();
	    	$("#l_seq_add span").eq(0).click().change();
	    	$("#aca_state_add span").eq(0).click().change();
	    	$("#aca_name").val("");
	    	getAcademicList(1);
	    }
	    
	    function deleteAca(){
	    	if($("input[name=checkRow]:checked").length==0){
	    		alert("삭제할 학사를 pilih해주세요.");
	    		return;
	    	}
	    	
	    	if(!confirm("삭제 하시겠습니까?"))
	    		return;
	    	
	    	var aca_seq = "";
	    	
	    	$.each($("input[name=checkRow]:checked"), function(index){
	    		if(index != 0)
	    			aca_seq += ",";

	    		aca_seq += $(this).val();
	    	});
	    	
	    	$.ajax({
                type: "POST",
                url: "${HOME}/ajax/admin/academic/academicReg/academicRegBasicInfo/delete",
                data: {
                	"aca_seq" : aca_seq
                    },
                dataType: "json",
                success: function(data, status) {
                	if(data.status == "200"){
                		alert("삭제 하였습니다");
                		getAcademicList(1);
                		
                	}else if(data.status == "001"){
                		var msg = "";
                		$.each(data.list, function(index){
                			var curr_msg = "";
                			var state_msg = "";
                			if(this.curr_cnr != 0)
                				curr_msg = " 배정된 교육과정이 있습니다."
                			if(this.aca_state != "01")
                				state_msg = " status akademik가 개설(비공개)인 학사만 삭제할 수 있습니다."
                			msg+= "gelar akademik : " + this.academic_name + curr_msg + state_msg +"\n";
                		});
                		alert(msg);
                		getAcademicList(1);
                		$("#th_checkAll").prop("checkec",false);
                	}
                },
                error: function(xhr, textStatus) {
                    alert("실패");
                    //alert("오류가 발생했습니다.");
                    document.write(xhr.responseText);
                },beforeSend:function() {
                	$.blockUI();
                },
                complete:function() {
                	$.unblockUI();
                }
            });
	    }
	    
	    function checkAll() {
	    	if ($("#th_checkAll").is(':checked')) {
	    		$("input[name=checkRow]").prop("checked", true);	    		
	    	}else {
	    		$("input[name=checkRow]").prop("checked", false);	    		
	    	}
	    }
	    
	    function academicExcelDown(){	    	
	    	location.href='${HOME}/admin/academic/academicReg/excelDown?l_seq='+$("#l_seq").attr("value")
	    			+'&m_seq='+$("#m_seq").attr("value")
	    			+'&s_seq='+$("#s_seq").attr("value")
	    			+'&academic_name='+$("#aca_name").val()
	    			+'&aca_state='+$("#aca_state").attr("value")
	                +'&year='+$("#year").attr("value");           
	    }
	</script>
	</head>
	<body>
		<!-- s_container_table -->
		<div id="container" class="container_table">
			<div class="contents main">
	
				<!-- s_left_mcon -->
				<div class="left_mcon aside_l grd">
					<div class="sub_menu adm_grd">
						<div class="title">manajemen akademik</div>
						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">sistem management akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">학사코드manajemen</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">menejemen kurikulum akadmik</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">menejemen kurikulum akadmik</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청manajemen</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on" onClick="location.href='${HOME}/admin/academic/academicReg/list'">management registration akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">Manajemen kompetensi kelulusan</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- e_left_mcon -->
	
				<!-- s_main_con -->
				<div class="main_con adm_grd">
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
	
					<!-- s_tt_wrap -->
					<div class="tt_wrap">
						<h3 class="am_tt">
							<span class="tt">management registration akademik</span>
						</h3>
					</div>
					<!-- e_tt_wrap -->
	
					<!-- s_adm_content1 -->
					<div class="adm_content3">
						<!-- s_tt_wrap -->
	
						<!-- s_wrap_wrap -->
						<div class="wrap_wrap">
							<!-- s_sch_wrap -->
							<div class="sch_wrap">
								<!-- s_rwrap -->
								<div class="rwrap">
									<!-- s_wrap_s -->
									<div class="wrap_s">
										<span class="tt2">tahun</span>
	
										<div class="wrap_s1_uselectbox">
											<div class="uselectbox" id="year" value="">
												<span class="uselected">pilih</span> <span class="uarrow">▼</span>
												<div class="uoptions" style="display: none;" id="year_add">
													<span class="uoption" value="">Keseluruhan</span>
													<c:forEach var="academicYearList" items="${academicYearList}">
	                                                    <span class="uoption" value="${academicYearList.year}">${academicYearList.year}</span>
	                                                </c:forEach>
												</div>
											</div>
										</div>
	
										<span class="tt2">fakultas</span>
	
										<div class="wrap_s1_uselectbox">
											<div class="uselectbox" id="l_seq" value="">
												<span class="uselected">Keseluruhan</span> <span class="uarrow">▼</span>
												<div class="uoptions" style="display: none;" id="l_seq_add">
													<span class="uoption" value="">Keseluruhan</span>
												</div>
											</div>
										</div>
	
										<span class="tt2">jurusan</span>
	
										<div class="wrap_s1_uselectbox">
											<div class="uselectbox" id="m_seq" value="">
												<span class="uselected">Keseluruhan</span><span class="uarrow">▼</span>
												<div class="uoptions" style="display: none;" id="m_seq_add">
													
												</div>
											</div>
										</div>
	
										<span class="tt2">tahun ajaran</span>
	
										<div class="wrap_s1_uselectbox">
											<div class="uselectbox" id="s_seq" value="">
												<span class="uselected">Keseluruhan</span><span class="uarrow">▼</span>
												<div class="uoptions" style="display: none;" id="s_seq_add">
													
												</div>
											</div>
										</div>
									</div>
									<!-- e_wrap_s -->
	
									<!-- s_wrap_s -->
									<div class="wrap_s">
										<span class="tt2_1">status akademik</span>
	
										<div class="wrap_s3_uselectbox">
											<div class="uselectbox" id="aca_state" value="">											 
												<span class="uselected">Keseluruhan</span> <span class="uarrow">▼</span>
												<div class="uoptions" style="display: none;" id="aca_state_add">
													<span class="uoption firstseleted" value="">Keseluruhan</span>
													<span class="uoption" value="01">개설(비공개)</span>
													<span class="uoption" value="02">개설(공개)</span>
													<span class="uoption" value="03">학사 중(수업 중)</span>
													<span class="uoption" value="04">학사종료(미확정)</span>
													<span class="uoption" value="05">학사종료(성적산정)</span>
													<span class="uoption" value="06">학사종료(성적발표)</span>
													<span class="uoption" value="07">성적발표 종료(미확정)</span>												
													<span class="uoption" value="08">학사최종종료(확정)</span>
												</div>
											</div>
										</div>
	
										<span class="tt2">gelar akademik</span> <input class="ip_tt_1" id="aca_name" value="" type="text"
										 onkeypress="javascript:if(event.keyCode==13){getAcademicList(1); return false;}">
	
									</div>
									<!-- e_wrap_s -->
	
								</div>
								<!-- e_rwrap -->
							</div>
							<!-- e_sch_wrap -->
	
							<!-- s_btnwrap_s1 -->
							<div class="btnwrap_s1">
								<button class="btn_tt1" onClick="getAcademicList(1);">Pencarian</button>
								<button class="btn_tt2" onClick="reset();">pengaturan ulang</button>
							</div>
							<!-- e_btnwrap_s1 -->
	
						</div>
						<!-- e_wrap_wrap -->
	
						<!-- s_wrap_wrap -->
						<div class="wrap_wrap">
							<!-- s_btnwrap_s2 -->
							<div class="btnwrap_s2">
								<button class="btn_tt2" onClick="deleteAca();">hapus yang dipilih</button>
	
								<!-- s_btnwrap_s2_s -->
								<div class="btnwrap_s2_s">
									<button class="btn_tt1" onclick="post_to_url('${HOME}/admin/academic/academicReg/academicRegBasicInfo/create');">registrasi</button>
									<button class="btn_down1" onClick="academicExcelDown();">mengunduh excel</button>
								</div>
								<!-- e_btnwrap_s2_s -->
	
							</div>
							<!-- e_btnwrap_s2 -->
	
							<table class="mlms_tb curri">
								<thead>
									<tr>
			                            <th class="th01 b_2 w1"><input type="checkbox" class="chk01" name="checkAll" id="th_checkAll" onclick="checkAll();"></th>
			                            <th class="th01 bd01 w2">tahun</th>
			                            <th class="th01 bd01 w2_n1">fakultas</th>
			                            <th class="th01 bd01 w2_n2">jurusan</th>
			                            <th class="th01 bd01 w2">tahun ajaran</th>  
			                            <th class="th01 bd01 w2">periode</th>
			                            <th class="th01 bd01 w3">periode<br>설정</th>
			                            <th class="th01 bd01 w2_n3">gelar akademik</th>
			                            <th class="th01 bd01 w3">학사<br>상태</th> 
			                            <th class="th01 bd01 w2">교육<br>과정<br>배정</th>
			                            <th class="th01 bd01 w2">교육<br>과정<br>계획서</th>
			                            <th class="th01 bd01 w2">종합<br>성적</th>
										<th class="th01 bd01 w2">수시<br>성적</th>
										<th class="th01 bd01 w2">형성<br>penilaian<br>점수</th>
			                            <th class="th01 b_1 w2 pd1">운영<br>보고서</th>
			                        </tr>
								</thead>
								<tbody id="academic_list_add">
									<%-- <tr>
										<td class="td_1"><input type="checkbox" class="chk01"
											name="checkRow" value="${content.IDX}"></td>
										<td class="td_1">2017</td>
										<td class="td_1">의과<br>대학
										</td>
										<td class="td_1">예과</td>
										<td class="td_1">1tahun ajaran</td>
										<td class="td_1">1semester</td>
										<td class="td_1 ft13">2017.02.27~<br>2017.07.15
										</td>
										<td class="td_1 ft13 fcolor01">2017tahun ajaran 예과 2tahun ajaran 1semester</td>
										<td class="td_1 fcolor01">개설(비공개)<br>
										<button class="btn1">변경</button></td>
										<!-- s_미배정 fcolor02-->
										<td class="td_1 fcolor02">미배정</td>
										<td class="td_1">-</td>
										<td class="td_1 ft13 fcolor01">제출periode<br>설정
										</td>
										<td class="td_1">-</td>
										<td class="td_1">-</td>
										<td class="td_1">-</td>
									</tr>
									<tr>
										<td class="td_1"><input type="checkbox" class="chk01"
											name="checkRow" value="${content.IDX}"></td>
										<td class="td_1">2017</td>
										<td class="td_1">의과<br>대학
										</td>
										<td class="td_1">예과</td>
										<td class="td_1">1tahun ajaran</td>
										<td class="td_1">1semester</td>
										<td class="td_1 ft13">2017.09.01~<br>2017.12.15
										</td>
										<td class="td_1 ft13 fcolor01">2017tahun ajaran 예과 2tahun ajaran 2semester</td>
										<td class="td_1 fcolor01">개설<br>
										<button class="btn1">변경</button></td>
										<td class="td_1">4</td>
										<td class="td_1">4</td>
										<!-- s_제출periode설정 fcolor01-->
										<td class="td_1 ft13">2017.011.21~<br>2017.11.25
										</td>
										<td class="td_1">-</td>
										<td class="td_1"><button class="btn2">조회</button></td>
										<td class="td_1"><button class="btn2">조회</button></td>
									</tr>
									<tr>
										<td class="td_1"><input type="checkbox" class="chk01"
											name="checkRow" value="${content.IDX}"></td>
										<td class="td_1">2017</td>
										<td class="td_1">의과<br>대학
										</td>
										<td class="td_1">예과</td>
										<td class="td_1">1tahun ajaran</td>
										<td class="td_1">1semester</td>
										<td class="td_1 ft13">2017.09.01~<br>2017.12.15
										</td>
										<td class="td_1 ft13 fcolor01">2017tahun ajaran 예과 2tahun ajaran 2semester</td>
										<td class="td_1 fcolor01">개설<br>
										<button class="btn1">변경</button></td>
										<td class="td_1">4</td>
										<td class="td_1">4</td>
										<td class="td_1 ft13">2017.011.21~<br>2017.11.25
										</td>
										<td class="td_1"><span class="sp1">201</span><span
											class="sp_sign">/</span><span class="sp2">204</span></td>
										<td class="td_1"><button class="btn2">조회</button></td>
										<td class="td_1"><button class="btn2">조회</button></td>
									</tr> --%>
								</tbody>
							</table>
	
						</div>
						<!-- e_wrap_wrap -->
                        <div class="pagination" id="pagingBtnAdd">
                            <ul>                    
                                <li><a href="#" title="처음" class="arrow bba"></a></li>
                                <li><a href="#" title="이전" class="arrow ba"></a></li>
                             
                                <li><a href="#" title="다음" class="arrow na"></a></li>
                                <li><a href="#" title="맨끝" class="arrow nna"></a></li>
                            </ul>
                        </div>
	
						<div class="bt_wrap">
							<button class="bt_1">임시 simpan</button>
							<button class="bt_2" onclick="location.href='#'">simpan</button>
							<button class="bt_3">batal</button>
						</div>
	
					</div>
					<!-- e_adm_content1 -->
				</div>
				<!-- e_main_con -->
	
			</div>
			<!-- e_contents -->
		</div>
		<!-- e_container_table -->	
	</body>
</html>