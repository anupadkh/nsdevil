<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>	
		<script>
			$(document).ready(function() {
				getCurriculumInfo();
			});
			
			function getCurriculumInfo(){
	            
	            $.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/admin/academic/curriculum/view/info",
	                data: {
	                    "curr_seq" : "${curr_seq}"                   
	                },
	                dataType: "json",
	                success: function(data, status) {
                    	var aca_name = '<span class="view1">'+data.currList.l_aca_name+'<span class="sign1">'
                    	+' &gt; </span> '+data.currList.m_aca_name+'<span class="sign1"> &gt; </span> '+data.currList.s_aca_name+'</span>';
                    	var targat_name = "";
                    	var grade = "";
                    	
                    	if(data.currList.target_code == "01")
                    		target_name = "all required";
                    	else if(data.currList.target_code == "02")
                    		target_name = "신청";
                    	
                    	if(data.currList.grade_code == "Y")
                    		grade = data.currList.grade;
                    	else
                    		grade = "미부여";
                    	
                    	
                    	$("span[data-name=curr_code]").text(data.currList.curr_code);
                    	$("span[data-name=curr_name]").text(data.currList.curr_name);
                    	$("div[data-name=aca_system_name]").html(aca_name);
                    	$("span[data-name=complete_name]").text(data.currList.complete_name);
                    	$("span[data-name=administer_name]").text(data.currList.administer_name);
                    	$("span[data-name=grade]").text(grade);
                    	$("span[data-name=target_name]").text(target_name);
                    	
                    	var htmls = "";
                    	$.each(data.mpfList, function(index){
                    		htmls = '<tr>'
							+'<td class="v1 td_1">Dosen Penanggung jawab</td>'
							+'<td class="v1 td_1">'+this.name+'</td>'
							+'<td class="v1 td_1">'+this.department_name+'</td>'
							+'<td class="v1 td_1">'+this.specialty+'</td>'
							+'<td class="v1 td_1">'+this.tel+'</td>'
							+'<td class="v1 td_1">'+this.email+'</td>'
							+'</tr>';
							$("#pfListAdd").append(htmls);								
                    	});
                    	
                    	$.each(data.pfList, function(index){
                    		htmls = '<tr>'
							+'<td class="v1 td_1">부Dosen Penanggung jawab</td>'
							+'<td class="v1 td_1">'+this.name+'</td>'
							+'<td class="v1 td_1">'+this.department_name+'</td>'
							+'<td class="v1 td_1">'+this.specialty+'</td>'
							+'<td class="v1 td_1">'+this.tel+'</td>'
							+'<td class="v1 td_1">'+this.email+'</td>'
							+'</tr>';
							$("#pfListAdd").append(htmls);								
                    	});	       
	                },
	                error: function(xhr, textStatus) {
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                }
	            }); 
	        }
			

		   function curriculumModify(curr_seq){
			   post_to_url("${HOME}/admin/academic/curriculum/create", {"curr_seq":curr_seq});
		   }
			   
		</script>
	</head>
	
	<body>
		<!-- s_container_table -->
		<div id="container" class="container_table">
			<!-- s_contents -->
			<div class="contents main">
	
				<!-- s_left_mcon -->
				<div class="left_mcon aside_l grd">
					<div class="sub_menu adm_grd">
						<div class="title">manajemen akademik</div>
						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">sistem management akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">학사코드manajemen</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on">menejemen kurikulum akadmik</span>
							</div>
							<ul class="panel on">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2 on">menejemen kurikulum akadmik</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청manajemen</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicReg/list'">management registration akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">Manajemen kompetensi kelulusan</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- e_left_mcon -->
	
				<!-- s_main_con -->
				<div class="main_con adm_grd">
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
	
					<!-- s_tt_wrap -->
					<div class="tt_wrap">
						<h3 class="am_tt">
							<span class="tt">menejemen kurikulum akadmik( perbaiki )</span>
						</h3>
	
						<button class="btn_tt1" onclick="location.href='${HOME}/admin/academic/curriculum/list'">목록</button>
					</div>
					<!-- e_tt_wrap -->
	
					<!-- s_adm_content1 -->
					<div class="adm_content2 ">
						<!-- s_tt_wrap -->
                        <form id="curriculumForm" onSubmit="return false;">
                        
						<!-- s_wrap_wrap -->
						<div class="wrap_wrap">
						
							<!-- s_sch_wrap -->
							<div class="sch_wrap regi">

								<h4>informasi yang dibutuhkan</h4>
								<!-- s_rwrap -->
								<div class="rwrap frwrap">
									<!-- s_wrap_s -->
									<div class="wrap_s a1">

										<span class="tt2">*Kode perkuliahan</span>
										<div class="wrap_fss1">
											<span class="view1" data-name="curr_code"></span>
										</div>

										<span class="tt2 bd1">*Nama Mata Kuliah</span>
										<div class="wrap_fss1">
											<span class="view1" data-name="curr_name"></span>
										</div>

									</div>
									<!-- e_wrap_s -->

									<!-- s_wrap_s -->
									<div class="wrap_s a1">
										<span class="tt2">*sistem akademik</span>
										<div class="wrap_fss" data-name="aca_system_name">
											
										</div>
									</div>
									<!-- e_wrap_s -->

								</div>
								<!-- e_rwrap -->

								<h4>informasi dasar</h4>
								<!-- s_rwrap -->
								<div class="rwrap frwrap">
									<!-- s_wrap_s -->
									<div class="wrap_s a1">
										<span class="tt2">kategori penuntasan</span>
										<div class="wrap_fss1">
											<span class="view1" data-name="complete_name"></span>
										</div>
										<span class="tt2 bd1">nilai</span>
										<div class="wrap_fss1">
											<span class="view1" data-name="grade">
											</span>
										</div>
									</div>
									<!-- e_wrap_s -->
									<!-- s_wrap_s -->
									<div class="wrap_s a1">
										<span class="tt2">manajemen Klasifikasi</span>
										<div class="wrap_fss1">
											<span class="view1" data-name="administer_name"></span>
										</div>

										<span class="tt2 bd1">Target</span>
										<div class="wrap_fss1">
											<span class="view1" data-name="target_name">
											</span>
										</div>
									</div>
									<!-- e_wrap_s -->
								</div>
								<!-- e_rwrap -->

								<h4>Dosen Penanggung jawab / 부책임 Informasi Instruktur</h4>
								<!-- s_rwrap -->
								<div class="rwrap frwrap">
									<table class="mlms_tb curri">
										<thead>
											<tr>
												<th class="v1 th01 bd01 wv1">posisi</th>
												<th class="v1 th01 bd01 wv1">nama </th>
												<th class="v1 th01 bd01 wv1">departement</th>
												<th class="v1 th01 bd01 wv2">jurusan khusus</th>
												<th class="v1 th01 bd01 wv2">kontak</th>
												<th class="v1 th01 bd01 wv2">E-mail</th>
											</tr>
										</thead>
										<tbody id="pfListAdd">
											
										</tbody>
									</table>

								</div>
								<!-- e_rwrap -->
							</div>
							<!-- e_sch_wrap -->

						</div>
						<!-- e_wrap_wrap -->
	                    </form>
						<div class="bt_wrap" style="margin-bottom:15px;">
							<button class="bt_2" name="saveBtn" onclick="curriculumModify('${curr_seq}');">perbaiki</button>
						</div>
	
					</div>
					<!-- e_adm_content1 -->
				</div>
				<!-- e_main_con -->
	
			</div>
			<!-- e_contents -->
		</div>
		<!-- e_container_table -->		
	</body>
</html>