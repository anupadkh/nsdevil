<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
    <head>
    	<script src="${HOME}/resources/smarteditor_v2.9/js/service/HuskyEZCreator.js"></script>
        <script>
			var editor_object = [];
        
	        $(document).ready(function(){
	        	bindPopupEvent("#m_pop2", ".open2");
	        	bindPopupEvent("#m_pop3", ".open3");
	        	bindPopupEvent("#m_pop4", ".open4");
	        	
	        	$(".vpanel").hide();
	            $(".vfold").click(function(){
		    		$(".vfold").hide();
		    		$(".vpanel").show();
		        });
	            $(".vexpand").click(function(){
		    		$(".vpanel").hide();
		    		$(".vfold").show();
		        });
	         	
	            getApplicationStList(1);
	        });		   
	
	        function getApplicationStList(page){
	        	$.ajax({
            		type: "POST",
            		url: "${HOME}/ajax/admin/academic/courseApplication/stlist",
            		data: {
            			"ca_seq" : "${ca_seq}",
            			"page" : page
            			},
           			dataType: "json",
           			success: function(data, status) {
           				var htmls = "";
           				
           				htmls += ''
           				$.each(data.list, function(index){
           					var bk_state = "미확인";
           					var bk_class = "btntt3";
           					var cas_state = "미승인";
           					if(this.deposit_state == "Y"){
           						bk_state = "입금<br>확인";
           						bk_class = "btntt2";
           					}else if(this.deposit_state == "C"){
           						bk_state = "입금<br>batal";
           						bk_class = "btntt3";
           					}
				
           					if(this.cas_state == "Y"){
           						cas_state = "승인";
           					}
           					
           					htmls +='<tr>'
           					+'<td class="td_1">'+this.row_num+'</td>'
           					+'<td class="td_1">'+this.id+'</td>'
           					+'<td class="td_1">'+this.name+'</td>'
           					+'<td class="td_1">'+this.tel+'</td>'
           					+'<td class="td_1">'+this.email+'</td>'
           					+'<td class="td_1">김가나다라</td>'
           					+'<td class="td_1"><span class="tts">(국민은행)</span>' 
           					+'<span class="tts">000-0000-000-0000</span></td>'
           					+'<td class="td_1">'
           					+'<button class="'+bk_class+' mdf open2" onClick="depositPopup('+this.cas_seq+');">'+bk_state+'</button>'
           					+'</td>'
           					+'<td class="td_1">';
           					if(this.cas_state == "Y")
       							htmls+='<button class="btntt2 mdf open4" onClick="casCanCelPopup('+this.cas_seq+');">'+cas_state+'</button>';
       						else
           						htmls+='<button class="btntt1 mdf open3" onClick="casPopup('+this.cas_seq+');">'+cas_state+'</button>';
           					htmls+='</td>'
           					+'</tr>';           					
           				});           					
           				
           				$("#stListAdd").html(htmls);
           				$("#pagingBtnAdd").html(data.pageNav);
        			},
        			error : function(xhr, textStatus) {
        				document.write(xhr.responseText);
        				$.unblockUI();
        			},
        			beforeSend : function() {
        				$.blockUI();
        			},
        			complete : function() {
        				$.unblockUI();
        			}
     			}); 
	        }
	        
	        //입금상태 팝업 띄우기
	        function depositPopup(cas_seq){
	        	$("#m_pop2").show();
	        	$("#depositState span").hide();
	        	$("#m_pop2 input[name=cas_seq]").val(cas_seq);	  
	        	$("#m_pop2 input[name=deposit_state]").val("");      	
	        }

	        //입금 상태 변경 버튼 클릭 시
	        function depositState(state){
	        	$("#depositState span").hide();
	        	
	        	$("#m_pop2 input[name=deposit_state]").val(state);
	        	
	        	if(state=='Y')
	        		$("#depositState span.tta").show();
	        	else if(state=='N')
	        		$("#depositState span.ttb").show();
	        	else if(state=='C')
	        		$("#depositState span.ttc").show();	        	
	        }
	        
	        //입금상태 변경 simpan
	        function updateDepositState(){
	        	if(isEmpty($("#m_pop2 input[name=deposit_state]").val())){
	        		alert("변경할 상태를 pilih해주세요.");
	        		return;
	        	}

	        	var page = 1;
	        	$.each($("#pagingBtnAdd").find("a"), function(){
					if($(this).hasClass("active"))
						page = $(this).text();
				});
	        	
	        	$.ajax({
            		type: "POST",
            		url: "${HOME}/ajax/admin/academic/courseApplication/updateDeposit",
            		data: {
            			"cas_seq" : $("#m_pop2 input[name=cas_seq]").val(),
            			"deposit_state" : $("#m_pop2 input[name=deposit_state]").val()
            			},
           			dataType: "json",
           			success: function(data, status) {
           				if(data.status=="200"){
           					alert("입금상태 변경 완료하였습니다.");
           					getApplicationStList(page);
           					$("#m_pop2").hide();
           				}else if(data.status=="201"){
           					alert("이미 승인되었습니다.\n승인 batal후 변경하여 주세요.");
           				}else{
           					alert("입금상태 변경 실패하였습니다.");           					
           				}
        			},
        			error : function(xhr, textStatus) {
        				document.write(xhr.responseText);
        				$.unblockUI();
        			},
        			beforeSend : function() {
        				$.blockUI();
        			},
        			complete : function() {
        				$.unblockUI();
        			}
     			}); 
	        }
	        
	        //승인 상태 팝업
	        function casPopup(cas_seq){
	        	$("#m_pop3").show();
	        	$("#m_pop3 input[name=cas_seq]").val(cas_seq);
	        }
	        
	        function updateCasState(){
	        	if(!confirm("최종 승인 상태로 변경하시겠습니까?")){
	        		return;
	        	}
	        		
	        	var page = 1;
	        	$.each($("#pagingBtnAdd").find("a"), function(){
					if($(this).hasClass("active"))
						page = $(this).text();
				});
	        	
	        	$.ajax({
            		type: "POST",
            		url: "${HOME}/ajax/admin/academic/courseApplication/updateCasState",
            		data: {
            			"cas_seq" : $("#m_pop3 input[name=cas_seq]").val()
            			},
           			dataType: "json",
           			success: function(data, status) {
           				if(data.status=="200"){
           					alert("승인 상태로 변경을 완료하였습니다.");
           					getApplicationStList(page);
           					$("#m_pop3").hide();
           				}else if(data.status=="201"){
           					alert("입금상태를 확인해주세요.");
           				}else{
           					alert("승인 상태 변경을 실패하였습니다.");           					
           				}
        			},
        			error : function(xhr, textStatus) {
        				document.write(xhr.responseText);
        				$.unblockUI();
        			},
        			beforeSend : function() {
        				$.blockUI();
        			},
        			complete : function() {
        				$.unblockUI();
        			}
     			}); 
	        }
	        
	        //승인 batal 팝업
	        function casCanCelPopup(cas_seq){
	        	$("#m_pop4").show();
	        	$("#m_pop4 input[name=cas_seq]").val(cas_seq);	        	
	        }
	        
	        function updateCasCancelState(){
	        	if(!confirm("최종 승인 batal 상태로 변경하시겠습니까?")){
	        		return;
	        	}
	        		
	        	var page = 1;
	        	$.each($("#pagingBtnAdd").find("a"), function(){
					if($(this).hasClass("active"))
						page = $(this).text();
				});
	        	
	        	$.ajax({
            		type: "POST",
            		url: "${HOME}/ajax/admin/academic/courseApplication/updateCasCancelState",
            		data: {
            			"cas_seq" : $("#m_pop4 input[name=cas_seq]").val()
            			},
           			dataType: "json",
           			success: function(data, status) {
           				if(data.status=="200"){
           					alert("승인 batal 상태로 변경을 완료하였습니다.");
           					getApplicationStList(page);
           					$("#m_pop4").hide();
           				}else if(data.status=="201"){
           					alert("입금상태를 확인해주세요.");
           				}else{
           					alert("승인 batal로 상태 변경을 실패하였습니다.");           					
           				}
        			},
        			error : function(xhr, textStatus) {
        				document.write(xhr.responseText);
        				$.unblockUI();
        			},
        			beforeSend : function() {
        				$.blockUI();
        			},
        			complete : function() {
        				$.unblockUI();
        			}
     			}); 
	        }
		</script>
	</head>

	<body>
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
			<!-- s_container_table -->
			<div id="container" class="container_table">
				<!-- s_contents -->
				<div class="contents main">
	
					<!-- s_left_mcon -->
					<div class="left_mcon aside_l grd">
						<div class="sub_menu adm_grd">
							<div class="title">manajemen akademik</div>
							<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">sistem management akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">학사코드manajemen</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on">menejemen kurikulum akadmik</span>
							</div>
							<ul class="panel on">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">menejemen kurikulum akadmik</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2 on">수강신청manajemen</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicReg/list'">management registration akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">Manajemen kompetensi kelulusan</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
						</div>
					</div>
					<!-- e_left_mcon -->
	
					<!-- s_main_con -->
					<div class="main_con adm_grd">
						<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
	
						<!-- s_tt_wrap -->
						<div class="tt_wrap">
							<h3 class="am_tt"><span class="tt">수강신청registrasi</span></h3>
						</div>
						<!-- e_tt_wrap -->
						
						<!-- s_adm_content2 -->
						<div class="adm_content2">
							<!-- s_tt_wrap -->
		
							<!-- s_wrap_wrap -->
							<div class="wrap_wrap">
								<div class="btnwrap_s1f">   
								     <button class="ic_v3" onclick="location.href='${HOME}/admin/academic/courseApplication/list'">수강신청 목록</button>
								</div>
								<!-- s_sch_wrap -->
								<div class="sch_wrap regi enroll">
		
									<!-- s_rwrap -->
									<div class="rwrap frwrap">
		
										<!-- s_wrap_s -->
										<div class="wrap_s a1">
											<span class="tt2">sistem akademik</span>
											<div class="wrap_fss1">
												<div class="ttswrap">
													<span class="tts">${caList.laca_system_name }
														<span class="sign1"> &gt; </span>
														${caList.maca_system_name }<span class="sign1"> &gt; 
														</span>${caList.saca_system_name }<span class="sign1">
														&gt; </span>${caList.aca_system_name }
													</span>
												</div>
											</div>											
										</div>
										
										<!-- s_wrap_s -->
										<div class="wrap_s a1">
										<span class="tt2">gelar akademik</span>
											<div class="wrap_fss">
												<div class="ttswrap">
													<span class="tts">${caList.academic_name  }</span>
												</div>
											</div>
										<!-- e_wrap_s -->
										</div>
										<!-- s_wrap_s -->
										<div class="wrap_s a1">
											<span class="tt2">Nama Mata Kuliah</span>
											<div class="wrap_fss">
												<div class="ttswrap">
													<span class="tts">${caList.curr_name } </span>
												</div>
											</div>
										</div>
										<!-- e_wrap_s -->
		
										<!-- s_wrap_s -->
										<div class="wrap_s a1">
											<span class="tt2">kategori penuntasan</span>
											<div class="wrap_fss1">
												<div class="ttswrap">
													<span class="tts">${currList.complete_name }</span>
												</div>
											</div>
											<span class="tt2 bd1">manajemen Klasifikasi</span>
											<div class="wrap_fss1">
												<div class="ttswrap">
													<span class="tts">${currList.administer_name }</span>
												</div>
											</div>
										</div>
										<!-- e_wrap_s -->
		
										<!-- s_wrap_s -->
										<div class="wrap_s a1">
											<span class="tt2">nilai</span>
											<div class="wrap_fss1">
												<div class="ttswrap">
													<span class="tts">${currList.grade }</span>
												</div>
											</div>
											<span class="tt2 bd1">신청비용</span>
											<div class="wrap_fss1">
												<div class="ttswrap">
													<span class="tts s1" data-name="req_charge">250,000 원</span> 
												</div>
											</div>
										</div>
										<!-- e_wrap_s -->
		
										<!-- s_wrap_s -->
										<div class="wrap_s a1">
											<span class="tt2">계좌번호</span>
											<div class="wrap_fss">
												<div class="ttswrap">
													<span class="tts s1">(${caList.bank_name }) ${caList.account_num } ${caList.account_holder }</span>
												</div>
											</div>
										</div>
										<!-- e_wrap_s -->
											
										<!-- s_wrap_s -->
										<div class="wrap_s a1">
											<span class="tt2">Dosen Penanggung jawab</span>
											<div class="wrap_fss">
												<div class="ttswrap">
													<c:forEach var="mpfList" items="${mpfList}" step="1" varStatus="status">
						                                <div class="a_mp">															
															<span class="tts">${mpfList.name } (${mpfList.department_name })</span>
														</div>
													</c:forEach> 
												</div>
											</div>
										</div>
										<!-- e_wrap_s -->
		
										<!-- s_wrap_s -->
										<div class="wrap_s a1">
											<span class="tt2">부Dosen Penanggung jawab</span>
											<div class="wrap_fss">
												<div class="ttswrap">
													<c:forEach var="pfList" items="${pfList}" step="1" varStatus="status">
						                                <div class="a_mp">															
															<span class="tts">${pfList.name } (${pfList.department_name })</span>
														</div>
													</c:forEach> 
												</div>
											</div>
										</div>
										<!-- e_wrap_s -->
		
									</div>
									<!-- e_rwrap -->
		
									<!-- s_rwrap -->
									<div class="rwrap frwrap a2">
									
										<div class="wrap_s">
								             <span class="tt2">접수periode</span>
								             <div class="wrap_fss3 s1">         
								                  <input type="text" class="dateyearpicker-input_1 ip_date dis" value="${caList.accept_start_date }"  disabled>   
								                  <span class="tt">~</span>
								                  <input type="text" class="dateyearpicker-input_1 ip_date dis" value="${caList.accept_end_date }"  disabled>     
											 </div>            
								             <span class="tt2 bd1">접수결과발표</span>
								             <div class="wrap_fss3 s2">         
												 <div class="ttswrap"> 
												     <input type="text" class="dateyearpicker-input_1 ip_date dis" value="${caList['a​nnouncement_date'] }"  disabled>
												 </div>
											 </div>
								       </div>
								<!-- e_wrap_s -->
								<!-- s_wrap_s -->        
								        <div class="wrap_s">
								             <span class="tt2">수강periode</span>
								             <div class="wrap_fss3 s1">         
								                  <input class="dateyearpicker-input_1 ip_date s1 dis" value="${currList.curr_start_date }" type="text" disabled>   
								                  <span class="tt">~</span>
								                  <input class="dateyearpicker-input_1 ip_date s1 dis" value="${currList.curr_end_date }" type="text" disabled>  
												  <input class="ip_tt tt7 s3 dis" value="${currList.curr_week }" type="text" disabled> <span class="unit s1">주</span>
												  <input class="ip_tt tt7 s3 dis" value="${currList.period_cnt }" type="text" disabled> <span class="unit s1">waktu</span>
											 </div>            
								             <span class="tt2 bd1">수강정원</span>
								             <div class="wrap_fss3 s2">         
												 <div class="ttswrap"> 
												     <input class="ip_tt tt7 s5 dis" value="${caList.fixed_num }" type="text" disabled> <span class="unit s1">명</span>
												 </div>
											 </div>
								       </div>										
		
										<!-- s_wrap_s -->
										<div class="wrap_s a4">
											<span class="tt2">제목</span>
											<div class="wrap_fss">
												<span class="ip_pt1">[ ${caList.curr_name } ] 수강신청 접수 안내 </span>
												<button class="vfold">자세히 보기</button>
											</div>
										</div>
										<!-- e_wrap_s -->
		
										<!-- s_wrap_s -->
										<div class="wrap_s a4 vpanel">
											<span class="tt2">isi</span>
											<div class="tarea free_textarea">
												<div class="tarea01">${caList.content }</div>
												<button class="vexpand">isi 접기</button>
											</div>
										</div>
										<!-- e_wrap_s -->
		
										<!-- s_wrap_s -->
										<div class="wrap_s a4 vpanel">
											<span class="tt2">파일첨부</span>
											<div class="wrap_fss">
												<div class="f_add1">
												<c:forEach var="attachList" items="${attachList}">
													<div class="wrap">                       
														<span class="sp1" onclick="fileDownload('${HOME}', '${RES_PATH}${attachList.file_path }', '', '${attachList.file_name }');">${attachList.file_name }</span>   											    		
										    		</div>
                                                </c:forEach> 
											</div>
											</div>
										</div>
										<!-- e_wrap_s -->
									</div>
									<!-- e_rwrap -->
		
								</div>
								<!-- e_sch_wrap -->
		
								<!-- s_ttswrap -->
								<div class="sttwrap">
									<span class="tt">Keseluruhan지원</span><span class="num">${stCntInfo.total_cnt }</span><span class="unit">명</span> 
									<span class="tt">입금미확인</span><span class="num">${stCntInfo.bk_n_cnt }</span><span class="unit">명</span>
									<span class="tt">입금확인</span><span class="num">${stCntInfo.bk_y_cnt }</span><span class="unit">명</span>
									<span class="tt">승인확인</span><span class="num">${stCntInfo.cas_cnt }</span><span class="unit">명</span>
								</div>
								<!-- e_ttswrap -->
								<table class="mlms_tb deposit">
									<thead>
										<tr>
											<th class="th01 bd01 wn1">No</th>
											<th class="th01 bd01 wn3">학번</th>
											<th class="th01 bd01 wn2">이름</th>
											<th class="th01 bd01 wn3">휴대폰번호</th>
											<th class="th01 bd01 wn4">e-mail</th>
											<th class="th01 bd01 wn2">입금자</th>
											<th class="th01 bd01 wn4">환불계좌정보</th>
											<th class="th01 bd01 wn5">입금<br>확인
											</th>
											<th class="th01 bd01 wn5">최종<br>승인
											</th>
										</tr>
									</thead>
									<tbody id="stListAdd">
																				
									</tbody>
								</table>
								<!-- s_pagination -->
								<div class="pagination" id="pagingBtnAdd">
									<ul>
										<li><a href="#" title="처음" class="arrow bba"></a></li>
										<li><a href="#" title="이전" class="arrow ba"></a></li>
										<li><a href="#" title="다음" class="arrow na"></a></li>
										<li><a href="#" title="맨끝" class="arrow nna"></a></li>
									</ul>
								</div> 
								<!-- e_pagination -->
							</div>
							<!-- e_wrap_wrap -->
		
							<div class="bt_wrap">
								<button class="bt_2" onclick="post_to_url('${HOME}/admin/academic/courseApplication/insert', {'ca_seq': ${ca_seq }});">perbaiki</button>
							</div>
		
						</div>
						<!-- e_adm_content2 -->
					</div>
					<!-- e_main_con -->
	
				</div>
				<!-- e_contents -->
			</div>
			<!-- e_container_table -->
			
			<!-- s_팝업 : 입금 상태 변경 -->
			<div id="m_pop2" class="popup_deposit_1 mo2">
				<input type="hidden" name="cas_seq">
				<input type="hidden" name="deposit_state">
				<!-- s_pop_wrap -->
				<div class="pop_wrap">
					<button class="pop_close close2" type="button">X</button>
		
					<p class="t_title">입금확인 상태 변경</p>
		
					<!-- s_pop_swrap -->
					<div class="pop_swrap">
		
						<div class="swrap">
							<span class="tt">입금확인 상태를 변경하시겠습니까?</span>
						</div>
		
						<div class="swrap">
							<button class="btntta" onClick="depositState('Y')">입금확인</button>
							<button class="btnttb" onClick="depositState('N')">미확인</button>
							<button class="btnttc" onClick="depositState('C')">입금batal</button>
						</div>
		
						<div class="swrap" id="depositState">
							<span class="tta" style="display:none;">입금확인 상태로 변경</span> 
							<span class="ttb" style="display:none;">미확인 상태로 변경</span> 
							<span class="ttc" style="display:none;">입금batal 상태로 변경</span>
						</div>
		
					</div>
					<!-- e_pop_swrap -->
					<div class="t_dd">
		
						<div class="pop_btn_wrap">
							<button type="button" class="btn01" onclick="updateDepositState();">변경</button>
							<button type="button" class="btn02" onClick="$('#m_pop2').hide();">batal</button>
						</div>
		
					</div>
				</div>
			</div>
			<!-- e_pop_wrap -->
			<!-- e_팝업 : 입금 상태 변경 -->
		
			<!-- s_팝업 : 승인 상태 변경1 -->
			<div id="m_pop3" class="popup_deposit_1_s1 mo3">
				<input type="hidden" name="cas_seq">
				<!-- s_pop_wrap -->
				<div class="pop_wrap">
					<button class="pop_close close3" type="button">X</button>
		
					<p class="t_title">승인 상태 변경</p>
		
					<!-- s_pop_swrap -->
					<div class="pop_swrap">
		
						<div class="swrap">
							<span class="tt"><span class="ss">최종 승인</span>하시겠습니까?</span>
						</div>
		
					</div>
					<!-- e_pop_swrap -->
					<div class="t_dd">
		
						<div class="pop_btn_wrap">
							<button type="button" class="btn01" onclick="updateCasState();">확인</button>
							<button type="button" class="btn02" onClick="$('#m_pop3').hide();">batal</button>
						</div>
		
					</div>
				</div>
			</div>
			<!-- e_pop_wrap -->
			<!-- e_팝업 : 승인 상태 변경1 -->
		
			<!-- s_팝업 : 승인 상태 변경2 -->
			<div id="m_pop4" class="popup_deposit_1_s2 mo4">
				<input type="hidden" name="cas_seq">
				<!-- s_pop_wrap -->
				<div class="pop_wrap">
					<button class="pop_close close4" type="button">X</button>
		
					<p class="t_title">승인 상태 변경</p>
		
					<!-- s_pop_swrap -->
					<div class="pop_swrap">
		
						<div class="swrap">
							<span class="tt"><span class="ss">승인을 batal</span>하시겠습니까?</span>
						</div>
		
					</div>
					<!-- e_pop_swrap -->
					<div class="t_dd">
		
						<div class="pop_btn_wrap">
							<button type="button" class="btn01"
								onclick="updateCasCancelState();">확인</button>
							<button type="button" class="btn02" onClick="$('#m_pop4').hide();">batal</button>
						</div>
		
					</div>
				</div>
			</div>
			<!-- e_pop_wrap -->
			<!-- e_팝업 : 승인 상태 변경2 -->
		
			<!-- s_팝업 -->
</body>
</html>