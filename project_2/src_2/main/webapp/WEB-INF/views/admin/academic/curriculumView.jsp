<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
    <head>
        <script>
        
	        $(document).ready(function(){
	        	layerPopupCloseInit(['div.uoptions']);
	        	acasystemStageOfList(1);
	        	setTimeout(getCurriculumList(1), 500);
	        	
	        	var mo1 = document.getElementById('m_pop1');
	        	var button = document.getElementsByClassName('close1')[0];
	        	var b = document.getElementsByClassName('open1');
	        	
	        	for (var i = 0; i < b.length; i++){ 
	        		b[i].onclick = function() {
	        			mo1.style.display = "block";
	        		}
	        		button.onclick = function() {
	        			mo1.style.display = "none";
	        		}
	        		window.onclick = function(event) {
	        			if (event.target == mo1) {
	        				mo1.style.display = "none";	        				
	        			}	        			
	        		}	        		
	        	}
	        	
	        });		   

	        $(document).on("click",".close1", function(){
	        	$("#xls_filename").val("");
	        	$("#xlsFile").val("");	        	
	        });
	        var list_count = 0; //콤보박스 변경시 이벤트 두번 실행되서 임시로.
		    //kategori utama값 변경 시
			$(document).on("DOMSubtreeModified", "#l_seq" ,function(){
				if($("#l_seq").attr("value") != "" && list_count == 0){
				    acasystemStageOfList(2, $("#l_seq").attr("value"));
				    $("#m_seq span.uselected").html("Keseluruhan");
				    $("#m_seq").attr("value","");
				    $("#s_seq span.uselected").html("Keseluruhan");
                    $("#s_seq").attr("value","");
				    list_count++;
				}else{
					list_count = 0;
				}
			});
		 
            //jurusan 값 변경 시
            $(document).on("DOMSubtreeModified", "#m_seq" ,function(){            	
                if($("#m_seq").attr("value") != "" && list_count == 0){
                    acasystemStageOfList(3, $("#l_seq").attr("value"), $("#m_seq").attr("value"));
                    $("#s_seq span.uselected").html("Keseluruhan");
                    $("#s_seq").attr("value","");
                    list_count++;
                }else{
                    list_count = 0;
                }
            });

            $(document).on("click",".order", function(){
            	if($(this).hasClass("up")){
            	    getCurriculumList(1,$(this).attr("name") + " DESC"); 
            	    $(".order").not(this).html("▼");
            	    $(".order").removeClass("up");
                    $(this).html("▲");
            	}else{
            		getCurriculumList(1,$(this).attr("name") + " ASC");
            		$(this).addClass("up");
                    $(".order").not(this).removeClass("up");
                    $(".order").html("▼");
            	}
            });

            function acasystemStageOfList(level, l_seq, m_seq, s_seq){
            	if(level == ""){
            		alert("분류 없음");
            		return;
            	}
            	$.ajax({
            		type: "POST",
            		url: "${HOME}/ajax/admin/academic/academicSystem/acaStageOfList",
            		data: {
            			"level" : level,
            			"l_seq" : l_seq,                	   
            			"m_seq" : m_seq,
            			"s_seq" : s_seq
            			},
           			dataType: "json",
           			success: function(data, status) {
           				var htmls = '<span class="uoption firstseleted" value="">Keseluruhan</span>';
           				$.each(data.list, function(index){
           					htmls +=  '<span class="uoption" value="'+this.aca_system_seq+'">'+this.aca_system_name+'</span>';
           				});
           				if(level == 1){
           					$("#l_seq_add span").remove();
           					$("#l_seq_add").append(htmls);        
           				}	                   
           				else if(level == 2){
           					$("#m_seq_add span").remove();
           					$("#m_seq_add").append(htmls);         
           				}
           				else{
           					$("#s_seq_add span").remove();
           					$("#s_seq_add").append(htmls);
        				}
        			},
        			error: function(xhr, textStatus) {
        				//alert("오류가 발생했습니다.");
        				document.write(xhr.responseText);
       				},beforeSend:function() {
  					},
  					complete:function() {
    				}
     			}); 
		   }
		   
		   function checkAll() {
			   if ($("#th_checkAll").is(':checked')) {
				   $("input[name=checkRow]").prop("checked", true);				   
			   }else {
				   $("input[name=checkRow]").prop("checked", false);				   
			   }			   
		   }
		   
		   function reset(){
			   $("#l_seq span.uselected").html("Keseluruhan");
               $("#l_seq").attr("value","");
               $("#m_seq span.uselected").html("Keseluruhan");
               $("#m_seq").attr("value","");
               $("#s_seq span.uselected").html("Keseluruhan");
               $("#s_seq").attr("value","");
               $("#curr_code").val("");
               $("#curr_name").val("");
               $("#mpf_name").val();
               $("#complete_code span.uselected").html("pilih");
               $("#complete_code").attr("value","");
               $("#administer_code span.uselected").html("pilih");
               $("#administer_code").attr("value","");
               $("#target_code span.uselected").html("pilih");
               $("#target_code").attr("value","");
               getCurriculumList(1);
		   }
		   		   
		   function getCurriculumList(page, order){			   
			   $.ajax({
                   type: "POST",
                   url: "${HOME}/ajax/admin/academic/curriculum/list",
                   data: {
                	   "page" : page,
                       "l_seq" : $("#l_seq").attr("value"),                       
                       "m_seq" : $("#m_seq").attr("value"),
                       "s_seq" : $("#s_seq").attr("value"),
                       "curr_code" : $("#curr_code").val().trim(),
                       "curr_name" : $("#curr_name").val().trim(),
                       "mpf_name" : $("#mpf_name").val().trim(),
                       "complete_code" : $("#complete_code").attr("value"),
                       "administer_code" : $("#administer_code").attr("value"),
                       "target_code" : $("#target_code").attr("value"),
                       "order" : order
                       },
                   dataType: "json",
                   success: function(data, status) {
                	   $("#add_data tr").remove();   
                	   
                       $.each(data.list, function(index){
                           var htmls = '';
                           var grade = '';
                           var target = '';
                           switch(this.grade_code){
	                           case "Y" : grade = "부여";break;
	                           case "N" : grade = "미부여";break;
	                       }
                           switch(this.target_code){
	                           case "01" : target = "all required";break;
	                           case "02" : target = "신청";break;
	                       }
                    	   htmls += '<tr><td class="td_1"><input type="checkbox" class="chk01" name="checkRow" value="'+this.curr_seq+'"></td>';
                    	   htmls += '<td class="td_1">'+this.l_aca_name+'</td>';
                    	   htmls += '<td class="td_1">'+this.m_aca_name+'</td>';
                    	   htmls += '<td class="td_1">'+this.s_aca_name+'</td>';
                    	   htmls += '<td class="td_1">'+this.curr_code+'</td>';
                    	   htmls += '<td class="td_1">'+this.nsd_curr_code+'</td>';
                    	   htmls += '<td class="td_1">'+this.reg_date+'</td>';
                    	   htmls += '<td class="td_1">'+this.curr_name+'</td>';
                    	   htmls += '<td class="td_1">'+this.complete_name+'</td>';
                    	   htmls += '<td class="td_1">'+this.administer_name+'</td>';
                    	   htmls += '<td class="td_1">'+target+'</td>';
                    	   htmls += '<td class="td_1">'+grade+'</td>';
                    	   htmls += '<td class="td_1">'+this.mpf_name+'</td>';
                    	   htmls += '<td class="td_1">';
                    	   htmls += '<button class="btn_tt4" type="button" onClick="curriculumModify('+this.curr_seq+');">perbaiki</button>';
                    	   if(this.confirm_flag=="Y")
                    		   htmls += '<button class="btn_c" type="button" title="이력보기">이력</button>';
                   		   else
                   			   htmls += '<button class="btn_c" type="button" title="삭제하기" onclick="currOneDelete('+this.curr_seq+');">X</button>';
                    	   htmls += '</td></tr>';
                           $("#add_data").append(htmls);
                       }); 
                       $("#pagingBtnAdd").html(data.pageNav);
                   },
                   error: function(xhr, textStatus) {
                	   alert("실패");
                       //alert("오류가 발생했습니다.");
                       document.write(xhr.responseText);
                   },beforeSend:function() {
                   },
                   complete:function() {
                   }
               }); 
		   }
		   
		 //교육과정 삭제하기
		   function currOneDelete(curr_seq){
				   
			   if(confirm("삭제하시겠습니까?")){
				   $.ajax({
	                   type: "POST",
	                   url: "${HOME}/ajax/admin/academic/curriculum/delete",
	                   data: {
	                       "curr_seqs" : curr_seq
	                       },
	                   dataType: "json",
	                   success: function(data, status) {
	                	   if(data.status == 200){
	                		   alert("삭제 완료 되었습니다.");
	                		   getCurriculumList(1);
	                           $(".order").removeClass("up");
	                           $(".order").html("▼");
	                	   }else if(data.status == 201){
	                		   alert("Nama Mata Kuliah : " + data.msg + " \n배정 이력이 있어 삭제가 불가능합니다.");   
	                	   }else{
	                		   alert("삭제 실패 하였습니다.");
	                	   }
	                   },
	                   error: function(xhr, textStatus) {
	                       alert("실패");
	                       //alert("오류가 발생했습니다.");
	                       document.write(xhr.responseText);
	                   },beforeSend:function() {
	                   },
	                   complete:function() {
	                   }
	               }); 
			   }
		   }
		   
		   //교육과정 체크박스로 삭제하기
		   function currDelete(){
			   var curr_seqs = "";
			   $.each($("input[name=checkRow]:checked"), function(index){
				    if(index != 0)
				    	curr_seqs += ",";
				    
				    curr_seqs += $(this).val();
			   });
			   
			   if(curr_seqs == ""){
				   alert("삭제할 데이터를 pilih해주세요");
				   return false;
			   }
				   
			   if(confirm("삭제하시겠습니까?")){
				   $.ajax({
	                   type: "POST",
	                   url: "${HOME}/ajax/admin/academic/curriculum/delete",
	                   data: {
	                       "curr_seqs" : curr_seqs
	                       },
	                   dataType: "json",
	                   success: function(data, status) {
	                	   if(data.status == 200){
	                		   alert("삭제 완료 되었습니다.");
	                		   getCurriculumList(1);
	                           $(".order").removeClass("up");
	                           $(".order").html("▼");
	                	   }else if(data.status == 201){
	                		   alert("Nama Mata Kuliah : " + data.msg + " \n배정 이력이 있어 삭제가 불가능합니다.");   
	                	   }else{
	                		   alert("삭제 실패 하였습니다.");
	                	   }
	                   },
	                   error: function(xhr, textStatus) {
	                       alert("실패");
	                       //alert("오류가 발생했습니다.");
	                       document.write(xhr.responseText);
	                   },beforeSend:function() {
	                	   
	                   },
	                   complete:function() {
	                	   
	                   }
	               }); 
			   }
		   }
		   
		   function curriculumModify(curr_seq){
			   post_to_url("${HOME}/admin/academic/curriculum/create/view", {"curr_seq":curr_seq});
		   }
		   
		   function curriculumTemplateDown(){			   
			   location.href="${HOME}/admin/academic/curriculum/templateXlxsDown";
		   }
		   
		   function file_nameChange(){    
			    if($("#xlsFile").val() != ""){
			        var fileValue = $("#xlsFile").val().split("\\");
			        var fileName = fileValue[fileValue.length-1]; // 파일명
			        
			        var fileLen = fileName.length; 
			        var lastDot = fileName.lastIndexOf('.'); //마지막 . 위치 가져온다.
			        var fileExt = fileName.substring(lastDot+1, fileLen).toLowerCase(); //마지막 . 위치로 확장자 자름

			        if(fileExt != "xlsx") {
			        	alert("xlsx 파일을 registrasi해주세요");
			        	$("#xlsFile").val("");
			        	return;			        	
			        }
			        
			        $("#xls_filename").val(fileName);
			    }
			}
		   

		   function xlsUp(){
		       if (isEmpty($("#xlsFile").val())) {
		           alert("파일을 pilih해주세요");
		           return;
		       }
		       		       
		       $("#xlsForm").ajaxForm({
		           type: "POST",
		           url: "${HOME}/ajax/admin/curriculum/excel/create",
		           dataType: "json",
		           success: function(data, status){
		        	  
		               if (data.status == "200") {
                           alert("simpan 완료 되었습니다.");
                           $("#xlsFile").val("");
                           $("#xls_filename").val("");
                           $('.avgrund-overlay').trigger('click');
                           getCurriculumList(1);
		               }else{
		            	   alert(data.status);
		               }
		           },
		           error: function(xhr, textStatus) {
		               alert(textStatus);
		               //document.write(xhr.responseText);
                       $.unblockUI();
		           },
		           beforeSend:function() {
		               $.blockUI();
		           },
		           complete:function() {
		               $.unblockUI();
		           }
		       }); 
		       $("#xlsForm").submit();
		   }

		   function curriculumXlsxDown(){
		       location.href="${HOME}/admin/academic/curriculum/curriculumListXlxsDown?l_seq="+$("#l_seq").attr("value") +                       
               "&m_seq=" + $("#m_seq").attr("value") +
               "&s_seq=" + $("#s_seq").attr("value") +
               "&curr_code=" + $("#curr_code").val().trim() +
               "&curr_name=" + $("#curr_name").val().trim() +
               "&mpf_name=" + $("#mpf_name").val().trim() +
               "&complete_code=" + $("#complete_code").attr("value") +
               "&administer_code=" + $("#administer_code").attr("value") +
               "&target_code=" + $("#target_code").attr("value")
		       
		   }
		</script>
	</head>

	<body>
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
			<!-- s_container_table -->
			<div id="container" class="container_table">
				<!-- s_contents -->
				<div class="contents main">
	
					<!-- s_left_mcon -->
					<div class="left_mcon aside_l grd">
						<div class="sub_menu adm_grd">
							<div class="title">manajemen akademik</div>
							<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">sistem management akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">학사코드manajemen</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on">menejemen kurikulum akadmik</span>
							</div>
							<ul class="panel on">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2 on">menejemen kurikulum akadmik</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청manajemen</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicReg/list'">management registration akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">Manajemen kompetensi kelulusan</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
						</div>
					</div>
					<!-- e_left_mcon -->
	
					<!-- s_main_con -->
					<div class="main_con adm_grd">
						<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
						<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
	
						<!-- s_tt_wrap -->
						<div class="tt_wrap">
							<h3 class="am_tt"><span class="tt">menejemen kurikulum akadmik</span></h3>
						</div>
						<!-- e_tt_wrap -->
	
						<!-- s_adm_content1 -->
						<div class="adm_content2">
							<!-- s_tt_wrap -->
	
							<!-- s_wrap_wrap -->
							<div class="wrap_wrap">
								<!-- s_sch_wrap -->
								<div class="sch_wrap">
									<!-- s_rwrap -->
									<div class="rwrap">
										<!-- s_wrap_s -->
										<div class="wrap_s">
											<span class="tt2">fakultas</span>
	
											<div class="wrap_s1_uselectbox">
												<div class="uselectbox" id="l_seq" value="">
													<span class="uselected">Keseluruhan</span><span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;" id="l_seq_add">
														<span class="uoption firstseleted">Keseluruhan</span>
													</div>
												</div>
											</div>
	
											<span class="tt2">jurusan</span>
	
											<div class="wrap_s1_uselectbox">
												<div class="uselectbox" id="m_seq" value="">
													<span class="uselected">Keseluruhan</span><span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;" id="m_seq_add">
														<span class="uoption firstseleted">Keseluruhan</span>
													</div>
												</div>
											</div>
	
											<span class="tt2">tahun ajaran</span>
	
											<div class="wrap_s1_uselectbox">
												<div class="uselectbox" id="s_seq" value="">
													<span class="uselected">Keseluruhan</span> <span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;" id="s_seq_add">
														<span class="uoption firstseleted">Keseluruhan</span>
													</div>
												</div>
											</div>
										</div>
										<!-- e_wrap_s -->
	
										<!-- s_wrap_s -->
										<div class="wrap_s">
											<span class="tt2">Kode perkuliahan</span> <input class="ip_tt" id="curr_code" name="curr_code" placeholder="Kode perkuliahan" type="text"> 
											<span class="tt2">Nama Mata Kuliah</span> <input class="ip_tt" id="curr_name" name="curr_name" placeholder="Nama Mata Kuliah" type="text"> 
											<span class="tt2">Dosen Penanggung jawab</span> <input class="ip_tt" id="mpf_name" name="mpf_name" placeholder="Dosen Penanggung jawab이름" type="text">											
										</div>
										<!-- e_wrap_s -->
	
										<!-- s_wrap_s -->
										<div class="wrap_s">
											<span class="tt2">kategori penuntasan</span>
	
											<div class="wrap_s1_uselectbox">
												<div class="uselectbox" id="complete_code" value="">
	                                                <span class="uselected">pilih</span> <span class="uarrow">▼</span>
	                                                <div class="uoptions" style="display: none;">
	                                                    <span class="uoption" value="">pilih</span>
	                                                    <c:forEach var="completeCodeList" items="${completeCodeList}">
	                                                        <span class="uoption" value="${completeCodeList.code}">${completeCodeList.code_name}</span>
	                                                    </c:forEach>   
	                                                </div>
	                                            </div>
											</div>
	
											<span class="tt2">manajemen Klasifikasi</span>
	
											<div class="wrap_s1_uselectbox">
												<div class="uselectbox" id="administer_code" value="">
                                                    <span class="uselected">pilih</span> <span class="uarrow">▼</span>
                                                    <div class="uoptions" style="display: none;">
                                                        <span class="uoption" value="">pilih</span>
	                                                    <c:forEach var="administerCodeList" items="${administerCodeList}">
	                                                        <span class="uoption" value="${administerCodeList.code}">${administerCodeList.code_name}</span>
	                                                    </c:forEach>                                                                                                      
                                                </div>
                                            </div>
											</div>
	
											<span class="tt2">Target</span>
	
											<div class="wrap_s1_uselectbox">
												<div class="uselectbox" id="target_code" value="">
													<span class="uselected">pilih</span> <span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;">
                                                        <span class="uoption" value="">pilih</span>
														<span class="uoption firstseleted" value="01">all required</span>
														<span class="uoption" value="02">신청</span>
													</div>
												</div>
											</div>
										</div>
										<!-- e_wrap_s -->
									</div>
									<!-- e_rwrap -->
								</div>
								<!-- e_sch_wrap -->
	
								<!-- s_btnwrap_s1 -->
								<div class="btnwrap_s1">
									<button class="btn_tt1" onClick="getCurriculumList(1);">Pencarian</button>
									<button class="btn_tt2" onClick="reset();">pengaturan ulang</button>
								</div>
								<!-- e_btnwrap_s1 -->
	
							</div>
							<!-- e_wrap_wrap -->
	
							<!-- s_wrap_wrap -->
							<div class="wrap_wrap">
								<!-- s_btnwrap_s2 -->
								<div class="btnwrap_s2">
									<button class="btn_tt2" onClick="currDelete();">hapus yang dipilih</button>
	
									<!-- s_btnwrap_s2_s -->
									<div class="btnwrap_s2_s">
										<button class="btn_tt1" type="button" onclick="post_to_url('${HOME}/admin/academic/curriculum/create')">registrasi</button>
										<button class="btn_up1 open1">엑셀업로드</button>
										<button class="btn_down1" onClick="curriculumXlsxDown(); return false;">mengunduh excel</button>
									</div>
									<!-- e_btnwrap_s2_s -->
	
								</div>
								<!-- e_btnwrap_s2 -->
	
								<table class="mlms_tb curri">
									<thead>
										<tr>
											<th class="th01 b_2 w1"><input type="checkbox" class="chk01" name="checkAll" id="th_checkAll" onclick="checkAll();"></th>
											<th class="th01 bd01 w2"><div class="th_wrap pd1">fakultas</div><button type="button" name="l_seq" class="down order">▼</button></th>
											<th class="th01 bd01 w2"><div class="th_wrap pd1">jurusan</div><button type="button" name="m_seq" class="down order">▼</button></th>
											<th class="th01 bd01 w2"><div class="th_wrap pd1">tahun ajaran</div><button type="button" name="aca_system_seq" class="down order">▼</button></th>
											<th class="th01 bd01 w2"><div class="th_wrap">교육<br>과정<br>코드</div><button type="button" name="curr_code" class="down order">▼</button></th>
											<th class="th01 bd01 w2"><div class="th_wrap pd2">manajemen<br>코드</div><button type="button" name="nsd_curr_code" class="down order">▼</button></th>
											<th class="th01 bd01 w2"><div class="th_wrap pd1">tanggal registrasi</div><button type="button" name="reg_date" class="down order">▼</button></th>											
											<th class="th01 bd01 w3"><div class="th_wrap pd2">교육<br>과정명</div><button type="button" name="curr_name" class="down order">▼</button></th>
											<th class="th01 bd01 w2"><div class="th_wrap pd2">이수<br>구분</div><button type="button" name="complete_code" class="down order">▼</button></th>
											<th class="th01 bd01 w2"><div class="th_wrap pd2">manajemen<br>구분</div><button type="button" name="administer_code" class="down order">▼</button></th>
											<th class="th01 bd01 w2"><div class="th_wrap pd1">Target</div><button type="button" name="target_code" class="down order">▼</button></th>
											<th class="th01 bd01 w2"><div class="th_wrap pd1">nilai</div><button type="button" name="grade_code" class="down order">▼</button></th>																						
											<th class="th01 bd01 w2"><div class="th_wrap pd2">책임<br>교수</div><button type="button" name="mpf_name" class="down order">▼</button></th>
											<th class="th01 b_1 w4 pd1">manajemen</th>
										</tr>
									</thead>
									<tbody id="add_data">
										<!-- <tr>
											<td class="td_1"><input type="checkbox" class="chk01"
												name="checkRow" value=""></td>
											<td class="td_1">의과<br>대학
											</td>
											<td class="td_1">예과</td>
											<td class="td_1">1tahun ajaran</td>
											<td class="td_1">100001</td>
											<td class="td_1">2017.<br>10/25
											</td>
											<td class="td_1">A00001</td>
											<td class="td_1">인체생리학</td>
											<td class="td_1">하책임</td>
											<td class="td_1">전공<br>필수
											</td>
											<td class="td_1">4</td>
											<td class="td_1">통합<br>과정
											</td>
											<td class="td_1">Keseluruhan<br>필수
											</td>
											<td class="td_1">
												<button class="btn_tt4">perbaiki</button>
												<button class="btn_c" title="삭제하기">X</button>
											</td>
										</tr>
										<tr>
											<td class="td_1"></td>
											<td class="td_1">의과<br>대학
											</td>
											<td class="td_1">예과</td>
											<td class="td_1">1tahun ajaran</td>
											<td class="td_1">100001</td>
											<td class="td_1">2017.<br>10/25
											</td>
											<td class="td_1">A00001</td>
											<td class="td_1">의학윤리</td>
											<td class="td_1">하책임</td>
											<td class="td_1">교양<br>필수
											</td>
											<td class="td_1">4</td>
											<td class="td_1">pelatihan<br>과정
											</td>
											<td class="td_1">신청</td>
											<td class="td_1">
												<button class="btn_tt4">perbaiki</button>
												<button class="btn_c" title="삭제하기">이력</button>
											</td>
										</tr> -->
									</tbody>
								</table>
	
							</div>
							<!-- e_wrap_wrap -->
							<div class="pagination" id="pagingBtnAdd">
								<ul>                    
				                    <li><a href="#" title="처음" class="arrow bba"></a></li>
				                    <li><a href="#" title="이전" class="arrow ba"></a></li>
				                 
				                    <li><a href="#" title="다음" class="arrow na"></a></li>
				                    <li><a href="#" title="맨끝" class="arrow nna"></a></li>
				                </ul>
							</div>
						</div>
						<!-- e_adm_content1 -->
					</div>
					<!-- e_main_con -->
	
				</div>
				<!-- e_contents -->
			</div>
			<!-- e_container_table -->
	
	
		<!-- s_ 엑셀 일괄 registrasi 팝업 -->
		<div id="m_pop1" class="pop_up_ex pop_up_ex_1 mo1">
	
			<div class="pop_wrap">
				<p class="popup_title">교육과정 엑셀업로드</p>
	
				<button class="pop_close close1" type="button">X</button>
	
				<button type="button" class="btn_exls_down_2" onClick="curriculumTemplateDown();">엑셀 양식 다운로드</button>
	
				<div class="t_title_1">
					<span class="sp_wrap_1">교육과정 registrasi 엑셀양식을 먼저 다운로드 받으신 뒤,<br>다운로드
						받은 엑셀파일양식에 교육과정 정보를 입력하여 파일 registrasi하시면 Keseluruhan 교육과정이 일괄registrasi됩니다.<br>※ registrasi 전
						sistem akademik가 반드시 registrasi되어야 합니다.<br>※ 신규 교육과정 registrasi 시에만 엑셀일괄registrasi이 가능합니다. (기존
						교육과정 perbaiki은 불가)
					</span>
				</div>
	
				<div class="pop_ex_wrap">
					<div class="sub_fwrap_ex_1">
					    <form id="xlsForm" onSubmit="return false;">
							<span class="ip_tt">file excel</span> 
							<input type="text" readonly class="ip_sort1_1" id="xls_filename" value="" onClick="$('#xlsFile').click();">
                            <button class="btn_r1_1" onClick="$('#xlsFile').click();">파일pilih</button>         
                            <input type="file" style="display:none;" id="xlsFile" name="xlsFile" onChange="file_nameChange();">
                        </form>
					</div>
	
					<!-- s_t_dd -->
					<div class="t_dd">
	
						<div class="pop_btn_wrap">
							<button type="button" class="btn01" onclick="xlsUp();">registrasi</button>
							<button type="button" class="btn02" onclick="$('.close1').click();">batal</button>
						</div>
	
					</div>
					<!-- e_t_dd -->
	
				</div>
			</div>
		</div>
		<!-- e_ 엑셀 일괄 registrasi 팝업 -->
	
	</body>
</html>