<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>	
		<script>
			$(document).ready(function() {
				var mo1 = document.getElementById('m_pop1');
	            var button = document.getElementsByClassName('close1')[0];
	            var b = document.getElementsByClassName('open1');
	            for (var i = 0; i < b.length; i++){
	                b[i].onclick = function() {
	                    mo1.style.display = "block";
	                }
	                
	                button.onclick = function() {
	                    mo1.style.display = "none";
	                }               
	                
	                window.onclick = function(event) {
	                    if (event.target == mo1) {
	                        mo1.style.display = "none";                     
	                    }               
	                }               
	            }
	            
                layerPopupCloseInit(['div.uoptions']);
				//sistem akademik kategori utama가져옴.
				
				//신청 pilih 값에 따라 비용 입력란 보여주기
				$(".u2open").click(function() {
					$("input[name='req_charge']").css("display", "inline-block");
				});
				
				$(".u1open").click(function() {
					$("input[name='req_charge']").css("display", "none");
                    $("input[name='req_charge']").val("");					
				});				
				
				$(".grade2").click(function() {
					$("input[name='grade']").css("display", "inline-block");
				});
				
				$(".grade1").click(function() {
					$("input[name='grade']").css("display", "none");
                    $("input[name='grade']").val("");					
				});		
				
				//automatic manual 값 변경 시
				$("input[name='auto_flag']").change(function(){
					if($(this).val()=="Y"){
						$("#curr_code").attr("readonly", true);
						$("#curr_code").val("");
					}
					else{						
		                if(isEmpty($("#curr_seq").val()) || isBlank($("#curr_seq").val())){
	                        $("#curr_code").attr("readonly", false);
	                        $("#curr_code").focus();	
		                }
					}
				});	
					            
	            //Kode perkuliahan 커서 아웃 시
	            /* $("#curr_code").focusout(function(){
	            	curriculumCodeCheck();
	            }); */
	            	            
	            //교수 팝업 클릭 시 
	            $(".open1").click(function(){
	            	$("#department_code").attr("value","");
	            	$("#department_code span:eq(0)").html("pilih");
	            	$("#mpf_name").val("");
	        		$("#mpf_add").empty();
	        		$("#select_pf").empty();
	                $("#selectMpfCount").text(0);
	            	$("#popupType").val($(this).attr("id"));	            	
	            	curriculumMpfList();	                    
	            });
	                 
	            //교수 tambahkan pilih 체크, 해제 이벤트
	            $("#mpf_add").on("change","input[name='chk']",function(){
	            	var popupType = $("#popupType").val();
	                var user_seq = $("#mpf_add tr:eq("+$(this).val()+") input[name='user_seq']").val();
	                if($(this).prop("checked")){
	                    var pf_name = $("#mpf_add tr:eq("+$(this).val()+") td[name='pf_name']").text();
	                    var pf_department = $("#mpf_add tr:eq("+$(this).val()+") td[name='pf_department']").text(); 
	                    var pf_specialty = $("#mpf_add tr:eq("+$(this).val()+") td[name='pf_specialty']").text(); 
	                    var pf_picture = $("#mpf_add tr:eq("+$(this).val()+") input[name='picture']").val();
	                    var email = $("#mpf_add tr:eq("+$(this).val()+") td[name='email']").text();
                        var tel = $("#mpf_add tr:eq("+$(this).val()+") td[name='pf_tel']").text();
                        var pf_level = $("#mpf_add tr:eq("+$(this).val()+") input[name='pf_level']").val();
                        
	                    var html = '';
	                    
	                    html += '<div class="a_mp" name="user_seq_'+user_seq+'"><input type="hidden" name="'+popupType+'_user_seq" value="'+user_seq+'">';
	                    html += '<span class="pt01"><img src="'+pf_picture+'" alt="사진" class="pt_img"></span>';
	                    if(isEmpty(pf_department))
	                    	html += '<span class="ssp1">'+pf_name+'</span>';
                    	else
                    		html += '<span class="ssp1">'+pf_name+'('+pf_department+')</span>';
	                    html += '<input type="hidden" name="pf_name" value="'+pf_name+'"><input type="hidden" name="pf_specialty" value="'+pf_specialty+'">';
                        html += '<input type="hidden" name="pf_department" value="'+pf_department+'"><input type="hidden" name="email" value="'+email+'">';
                        html += '<input type="hidden" name="tel" value="'+tel+'"><input type="hidden" name="pf_level" value="'+pf_level+'">';
	                    html += '<button type="button" class="btn_c" name="pf_del_btn" onClick="delMpf('+user_seq+','+$(this).val()+', this);">X</button></div>';
	                    
	                    $("#select_pf").append(html);
	                }else{
	                    $("#select_pf div[name='user_seq_"+user_seq+"']").remove();
	                }

	                selectMpfCount();
	            });
	            
	            if(!isEmpty($("#autoFlag").val()) || !isBlank($("#autoFlag").val())){
	            	$("input[name='auto_flag'][value='"+$("#autoFlag").val()+"']").prop("checked",true);   
	            }
			});
			
			function lSeqSet(seq){
				acasystemStageOfList(2, seq);
                $("#m_seq span.uselected").html("jurusan");
                $("#m_seq").attr("value","");
                $("#s_seq span.uselected").html("tahun ajaran");
                $("#s_seq").attr("value","");
			}
			
			function mSeqSet(seq){
				acasystemStageOfList(3, "", seq);
                $("#s_seq span.uselected").html("tahun ajaran");
                $("#s_seq").attr("value","");
			}
			
			function acasystemStageOfList(level, l_seq, m_seq, s_seq){
                if(level == ""){
                    alert("분류 없음");
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "${HOME}/ajax/admin/academic/academicSystem/acaStageOfList",
                    data: {
                        "level" : level,
                        "l_seq" : l_seq,                       
                        "m_seq" : m_seq,
                        "s_seq" : s_seq
                        },
                    dataType: "json",
                    success: function(data, status) {
                        var htmls = '<span class="uoption firstseleted" value="">pilih</span>';
                        
                        if(level == 1){
                        	$.each(data.list, function(index){
                                htmls +=  '<span class="uoption" value="'+this.aca_system_seq+'" onClick="lSeqSet('+this.aca_system_seq+');">'+this.aca_system_name+'</span>';
                            });
                            $("#l_seq_add span").remove();
                            $("#l_seq_add").append(htmls);        
                        }                      
                        else if(level == 2){
                        	$.each(data.list, function(index){
                                htmls +=  '<span class="uoption" value="'+this.aca_system_seq+'" onClick="mSeqSet('+this.aca_system_seq+');">'+this.aca_system_name+'</span>';
                            });
                            $("#m_seq_add span").remove();
                            $("#m_seq_add").append(htmls);         
                        }
                        else{
                        	$.each(data.list, function(index){
                                htmls +=  '<span class="uoption" value="'+this.aca_system_seq+'">'+this.aca_system_name+'</span>';
                            });
                            $("#s_seq_add span").remove();
                            $("#s_seq_add").append(htmls);
                        }
                    },
                    error: function(xhr, textStatus) {
                        //alert("오류가 발생했습니다.");
                        document.write(xhr.responseText);
                    },beforeSend:function() {
                    },
                    complete:function() {
                    }
                }); 
           }
			
			function curriculumMpfList(){
				$("#mpf_add").empty();
                $("#selectMpfCount").text(0);
                
                var pf = '';
                $("input[name*=pf_user_seq]").each(function(index){
                    if(index == 0 )
                        pf+= $(this).val();
                    else
                        pf+=","+$(this).val(); 
                });
                
                
				$.ajax({
                    type: "POST",
                    url: "${HOME}/ajax/mpf/list",
                    data: {
                        "pf_seq" : pf,
                        "department_code" : $("#department_code").attr("value"),
                        "mpf_name" : $("#mpf_name").val()                    
                    },
                    dataType: "json",
                    success: function(data, status) {
                        var html = '';            
                        $.each(data.pf_list,function(index){
                        	var picture_path = "";
                        	if(isEmpty(this.picture_path))
                        		picture_path = "${IMG}/ph_1.png";
                       		else
                       			picture_path = "${RES_PATH}"+this.picture_path;
                            html += "<tr class='tr01'>";                            
                            html += "<td class='ta_c w1' name='pf_name'>"+this.name+"<input type='hidden' name='picture' value='"+picture_path+"'/>";
                            html += "<input type='hidden' name='user_seq' value='"+this.user_seq+"'/><input type='hidden' name='pf_position' value='"+this.position+"'/></td>";
                            html += "<td class='ta_c w2' onClick='pf_select('"+index+"');' name='pf_id'>"+this.professor_id+"</td>";
                            html += "<td class='ta_c w3' name='pf_department'>"+this.department+"</td>";
                            html += "<td class='ta_c w4' name='pf_specialty'>"+this.specialty+"</td>";
                            html += "<td class='ta_c ls_1 w5' name='pf_tel'>"+this.tel+"</td>";
                            html += "<td class='ta_c ls_1 w6' name='email'><a class='mailto_1' href='' target='_top'>"+this.email+"</a></td>";
                            html += "<td class='ta_c w8'>";
                            html += "<label class='chk01'>";
                            if($("#select_pf input[value='"+this.user_seq+"']").length!=0)
                            	html += "<input type='checkbox' name='chk' checked value='"+index+"'>";
                           	else
                           		html += "<input type='checkbox' name='chk' value='"+index+"'>";
                            html += "<span class='slider round'>pilih</span>";
                            html += "</label></td></tr>";                
                        });
                        
                        $("#mpf_add").html(html);
                    },
                    error: function(xhr, textStatus) {
                        //alert("오류가 발생했습니다.");
                        document.write(xhr.responseText);
                    }
                }); 
			}
			
			
			//kode kurikulum Akademik 중복 체크			
			function curriculumCodeCheck(){
				var curr_code = $("#curr_code").val();
				var chk = "N";
                if(isEmpty(curr_code) || isBlank(curr_code)){
                	return chk;
                }
                
				var chk = "N";
				$.ajax({
                    type: "POST",
                    url: "${HOME}/ajax/admin/academic/curriculum/currcode/count",
                    data: {
                        "curr_code" : curr_code,
                        "curr_seq" : $("#curr_seq").val()
                    },
                    dataType: "json",
                    success: function(data, status) { 
                    	if(data.chk == "Y"){
                    		$("#curr_code").val("");
                    		chk = "Y";                    		
                    		alert("입력한 kode kurikulum Akademik는 이미 있는 코드 입니다.\n 다른 코드를 입력해주세요.");
                    		$("#curr_code").focus();
                    	}
                    },error: function(xhr, textStatus) {
                        document.write(xhr.responseText); 
                        $.unblockUI();   
                    },beforeSend:function() {
                        $.blockUI();
                    },complete:function() {
                        $.unblockUI();   
                    }
                }); 
				
				return chk;
			}
			
            //simpan - perbaiki
            function curriculumSubmit(){
            	
            	var data = $('#curriculumForm').serializeArray();
            	data.push({name:"aca_system_seq", value:$("#s_seq").attr("value")});
            	data.push({name:"complete_code", value:$("#complete_code").attr("value")});
            	data.push({name:"grade_code", value:$("#grade_code").attr("value")});
            	data.push({name:"administer_code", value:$("#administer_code").attr("value")});
            	data.push({name:"target_code", value:$("#target_code").attr("value")});
            	                
            	var curr_seq = $("#curr_seq").val();

            	//교육과정 중복 체크 한다.
/*                 if(curriculumCodeCheck() == "Y"){
                	return false;
                } */

                var curr_code = $("#curr_code").val();
                var auto_flag = $("input[name='auto_flag']:checked").val();
                var curr_name = $("#curr_name").val();
                var s_seq = $("#s_seq").attr("value"); //tahun ajaran
                
                //manual일때 교육과정 입력했는지 체크
            	if(auto_flag == "N"){
            		if(isEmpty(curr_code) || isBlank(curr_code)){
                        alert("kode kurikulum Akademik를 입력해주세요.");
                        $("#curr_code").focus();
                        return false;
                    }
            	}
            	
            	//informasi yang dibutuhkan 체크
            	if(isEmpty(curr_name) || isBlank(curr_name)){
                    alert("Nama Mata Kuliah을 입력해주세요.");
                    $("#curr_name").focus();
                    return false;
                }
            	
            	//DB에 tahun ajaran 시퀀스만 simpan되므로 tahun ajaran만 체크한다.
            	if(isEmpty(s_seq) || isBlank(s_seq)){
                    alert("sistem akademik를 pilih해주세요.");
                    return false;
                }
            	
            	$.ajax({
            		type: "POST",
            		url: "${HOME}/ajax/admin/academic/curriculum/create",
            		data: data,
            		dataType: "json",
            		success: function(data, status) { 
            			if(data.status==200){
            				if(isEmpty($("#curr_seq").val())){
            					alert("simpan 완료하였습니다.");
            				}
            				else{
            					alert("perbaiki 완료하였습니다.");
            				}
   						    post_to_url("${HOME}/admin/academic/curriculum/create/view", {"curr_seq":data.curr_seq});
            			}else{
            				alert("simpan 실패하였습니다.");
            			}
            		},error: function(xhr, textStatus) {
            			document.write(xhr.responseText); 
            			$.unblockUI();   
            		},beforeSend:function() {
            			$.blockUI();
            		},complete:function() {
            			$.unblockUI();   
            		}
                }); 
            }            
            
            
            
            function reset(){
            	if(isEmpty("${currList.curr_seq}")){
            		if(confirm("작성된 isi을 batal 하시겠습니까?")){
            			location.href='${HOME}/admin/academic/curriculum/list'
            			return;
            		}
            	}else{
            		if(confirm("perbaiki된 isi을 batal 하시겠습니까?")){
            			location.href='${HOME}/admin/academic/curriculum/list';
            			return;
            		}
            	}
            		
            		
            	$("input[name='auto_flag'][value='Y']").prop("checked",true).change();
            	$("#curr_seq").val("");
            	$("#curr_code").val("");
            	$("#curr_name").val("");
                $("#l_seq span.uselected").html("fakultas");
                $("#l_seq").attr("value","");
            	$("#m_seq span.uselected").html("jurusan");
                $("#m_seq").attr("value","");
                $("#s_seq span.uselected").html("tahun ajaran");
                $("#s_seq").attr("value","");
                $("#grade_code span.uselected").html("pilih");
                $("#grade_code").attr("value","");
                $("#administer_code span.uselected").html("pilih");
                $("#administer_code").attr("value","");
                $("#target_code span.uselected").html("pilih");
                $("#target_code").attr("value","");
                $("#complete_code span.uselected").html("pilih");
                $("#complete_code").attr("value","");
                $("#popupMpfSubmit").empty();
                $("#popupPfSubmit").empty();
            }
            
           
	        //pilih된 교수 삭제 X 클릭
	        function delMpf(user_seq, index, obj){
	        	$(obj).parent("div").remove();
	            //$("#select_pf div[name='user_seq_"+user_seq+"']").remove();
	            $("#mpf_add input[name='chk']").eq(index).prop("checked",false);
	            selectMpfCount();
	        }

	        //pilih된 교수 registrasi
	        function popupMpfSubmit(){
	        	var popupType = $("#popupType").val();
	            var check_len= $("#select_pf div.a_mp").length;
	            	
	            if(check_len == 0){
	                alert("registrasi할 교수를 pilih하세요");
	                return;
	            }
	            
	            if(popupType == "mpf")
	            	$("#popupMpfSubmit").append($("#select_pf").html());	            
	            else
	            	$("#popupPfSubmit").append($("#select_pf").html());
	            
	            $("#mpf_add").empty();
	            $("#select_pf").empty();
	            $("#m_pop1").hide();
	            $("#selectMpfCount").text(0);
	        }
	
	        //pilih된 교수 카운트
	        function selectMpfCount(){
	            var cnt = $("#select_pf .a_mp").length;
	            $("#selectMpfCount").text(cnt);
	        }
		</script>
	</head>
	
	<body>
		<!-- s_container_table -->
		<div id="container" class="container_table">
			<!-- s_contents -->
			<div class="contents main">
	
				<!-- s_left_mcon -->
				<div class="left_mcon aside_l grd">
					<div class="sub_menu adm_grd">
						<div class="title">manajemen akademik</div>
						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">sistem management akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">학사코드manajemen</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on">menejemen kurikulum akadmik</span>
							</div>
							<ul class="panel on">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2 on">menejemen kurikulum akadmik</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청manajemen</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicReg/list'">management registration akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">Manajemen kompetensi kelulusan</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- e_left_mcon -->
	
				<!-- s_main_con -->
				<div class="main_con adm_grd">
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
	
					<!-- s_tt_wrap -->
					<div class="tt_wrap">
						<h3 class="am_tt">
							<c:choose>
								<c:when test='${currList.curr_seq ne null and currList.curr_seq ne ""}'>
							<span class="tt">menejemen kurikulum akadmik( perbaiki )</span>
								</c:when>
								<c:otherwise>
							<span class="tt">menejemen kurikulum akadmik( registrasi )</span>
								</c:otherwise>
							</c:choose>
						</h3>
	
						<button class="btn_tt1" onclick="location.href='${HOME}/admin/academic/curriculum/list'">목록</button>
					</div>
					<!-- e_tt_wrap -->
	
					<!-- s_adm_content1 -->
					<div class="adm_content2 ">
						<!-- s_tt_wrap -->
                        <form id="curriculumForm" onSubmit="return false;">
                        
						<!-- s_wrap_wrap -->
						<div class="wrap_wrap">
							<!-- s_sch_wrap -->
							<div class="sch_wrap regi">
	
								<h4>informasi yang dibutuhkan</h4>
								<!-- s_rwrap -->
								<div class="rwrap frwrap">
									<!-- s_wrap_s -->
									<div class="wrap_s a1">
	
										<span class="tt2">Kode perkuliahan</span>
										<div class="wrap_fss1">
											<div class="radio_wrap">
                                                <input type="hidden" id="curr_seq" name="curr_seq" value="${currList.curr_seq }">
											    <input type="hidden" id="autoFlag" value="${currList.auto_flag }">
												<label>automatic <input type="radio" name="auto_flag" value="Y" class="ip_r1" checked></label>
											</div>
											<div class="radio_wrap">
												<label>manual <input type="radio" name="auto_flag" value="N" class="ip_r1"></label> 
												<input class="ip_tt tt1" readonly name="curr_code" id="curr_code" maxlength="10" value="${currList.curr_code }" type="text">
											</div>
										</div>
	
										<span class="tt2 bd1">Nama Mata Kuliah</span> 
										<div class="wrap_fss1">
											<input class="ip_tt tt2" name="curr_name" id="curr_name" value="${currList.curr_name }" placeholder="Nama Mata Kuliah" type="text">
										</div>
	
									</div>
									<!-- e_wrap_s -->
	
									<!-- s_wrap_s -->
									<div class="wrap_s a1">
										<span class="tt2">sistem akademik</span>
										<div class="wrap_fss">    
											<div class="wrap_s1_uselectbox">
												<div class="uselectbox" id="l_seq" value="${currList.l_seq }">
													<span class="uselected">
													    <c:if test="${currList.l_aca_name != '' }">${currList.l_aca_name }</c:if>
	                                                    <c:if test="${currList.l_aca_name == '' }">fakultas</c:if>
													</span> <span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;" id="l_seq_add">
													    <span class="uoption" value="">fakultas</span>
														<c:forEach var="lseqList" items="${lseqList}">
	                                                        <span class="uoption" value="${lseqList.aca_system_seq}" onClick="lSeqSet('${lseqList.aca_system_seq}');">${lseqList.aca_system_name}</span>
	                                                    </c:forEach>    
													</div>
												</div>
											</div>
		
											<div class="wrap_s1_uselectbox">
												<div class="uselectbox" id="m_seq" value="${currList.m_seq }">
													
													<span class="uselected">
													    <c:if test="${currList.m_aca_name != '' }">${currList.m_aca_name }</c:if>
	                                                    <c:if test="${currList.m_aca_name == '' }">jurusan</c:if>
													</span> <span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;" id="m_seq_add">
													    <span class="uoption" value="">jurusan</span>
	                                                    <c:forEach var="mseqList" items="${mseqList}">
	                                                        <span class="uoption" value="${mseqList.aca_system_seq}" onClick="mSeqSet('${mseqList.aca_system_seq}');">${mseqList.aca_system_name}</span>
	                                                    </c:forEach>   
													</div>
												</div>
											</div>
		
											<div class="wrap_s1_uselectbox">
												<div class="uselectbox" id="s_seq" value="${currList.aca_system_seq }">
													<span class="uselected">
													    <c:if test="${currList.s_aca_name != '' }">${currList.s_aca_name }</c:if>
													    <c:if test="${currList.s_aca_name == '' }">tahun ajaran</c:if>
													</span> <span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;" id="s_seq_add">
													    <span class="uoption" value="">tahun ajaran</span>
	                                                    <c:forEach var="sseqList" items="${sseqList}">
	                                                        <span class="uoption" value="${sseqList.aca_system_seq}">${sseqList.aca_system_name}</span>
	                                                    </c:forEach> 
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- e_wrap_s -->	
									
								</div>
								<!-- e_rwrap -->
								
								
								<h4>informasi dasar</h4>
								<!-- s_rwrap -->
								
								<div class="rwrap frwrap">
									<!-- s_wrap_s -->
									<div class="wrap_s a1">
										<span class="tt2">kategori penuntasan</span>
										<div class="wrap_fss1"> 
											<div class="wrap_s3_uselectbox">
												<div class="uselectbox" id="complete_code" value="${currList.complete_code }">
													<span class="uselected">
													    <c:if test="${currList.complete_name != '' }">${currList.complete_name }</c:if>
	                                                    <c:if test="${currList.complete_name == '' }">pilih</c:if>		  
	                                                </span>										     
													<span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;">
													    <span class="uoption" value="">pilih</span>
														<c:forEach var="completeCodeList" items="${completeCodeList}">
	                                                        <span class="uoption" value="${completeCodeList.code}">${completeCodeList.code_name}</span>
	                                                    </c:forEach>   
													</div>
												</div>
											</div>
										</div>
	
										<span class="tt2 bd1">nilai</span>
										<div class="wrap_fss1"> 
											<div class="wrap_s4_uselectbox" style="width:95px;">
												<div class="uselectbox" id="grade_code" value="${currList.grade_code }">
													<span class="uselected">
													    <c:if test="${currList.grade_code == 'N' }">미부여</c:if>
	                                                    <c:if test="${currList.grade_code == 'Y' }">부여</c:if>
	                                                    <c:if test="${currList.grade_code == '' }">pilih</c:if>    
	                                                </span>
													<span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;">
													    <span class="uoption" value="">pilih</span>
														<span class="uoption grade1" value="N">미부여</span>
														<span class="uoption grade2" value="Y">부여</span>
													</div>
												</div>
											</div>
											<c:if test="${currList.grade_code == 'Y' }">
												<input class="ip_tt tt3 u2" value="${currList.grade }" name="grade" placeholder="nilai 입력  " type="text" style="text-align:right;padding-right:5px;"  onkeyup="onlyNumCheck(this);" title="숫자만 입력하세요.">
											</c:if>
											<c:if test="${currList.grade_code != 'Y' }">
												<input class="ip_tt tt3 u2" value="" name="grade" placeholder="nilai 입력  " type="text" style="text-align:right;padding-right:5px;display:none;"  onkeyup="onlyNumCheck(this);" title="숫자만 입력하세요.">
											</c:if>
										</div>
									</div>
									
									<!-- e_wrap_s -->
									<!-- s_wrap_s -->
									<div class="wrap_s">
										<span class="tt2">manajemen Klasifikasi</span>
										<div class="wrap_fss1">    
											<div class="wrap_s3_uselectbox">
												<div class="uselectbox" id="administer_code" value="${currList.administer_code }">
													<span class="uselected">
													    <c:if test="${currList.administer_name != '' }">${currList.administer_name }</c:if>
	                                                    <c:if test="${currList.administer_name == '' }">pilih</c:if>     
	                                                </span>
	                                                <span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;">
	                                                    <span class="uoption" value="">pilih</span>
													    <c:forEach var="administerCodeList" items="${administerCodeList}">
	                                                        <span class="uoption" value="${administerCodeList.code}">${administerCodeList.code_name}</span>
	                                                    </c:forEach>  												                                                      
													</div>
												</div>
											</div>
										</div>						
										<span class="tt2 bd1">Target</span>
										
										<div class="wrap_fss1">    
											<div class="wrap_s4_uselectbox">
												<div class="uselectbox" id="target_code" value="${currList.target_code }">
													<span class="uselected">
													    <c:if test="${currList.target_code == '01' }">all required</c:if>
													    <c:if test="${currList.target_code == '02' }">신청</c:if>
	                                                    <c:if test="${currList.target_code == '' }">pilih</c:if>    
	                                                </span> 
	                                                <span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;">
	                                                    <span class="uoption" value="">pilih</span>
														<span class="uoption u1open firstseleted" value="01">all required</span>
														<span class="uoption u2open" value="02">신청</span>
													</div>
												</div>
											</div>
											<c:if test="${currList.target_code == '02' }">
	                                        	<input class="ip_tt tt3 u2" value="${currList.req_charge }" name="req_charge" placeholder="(pilih) 신청 비용 " type="text" style="text-align:right;padding-right:5px;"  onkeyup="number_check(this);" title="숫자만 입력하세요.">
	                                       	</c:if>
	                   						<c:if test="${currList.target_code != '02' }">
	                                        	<input class="ip_tt tt3 u2" value="" name="req_charge" placeholder="(pilih) 신청 비용 " type="text" style="text-align:right;padding-right:5px;display:none;"  onkeyup="number_check(this);" title="숫자만 입력하세요.">
	                                       	</c:if>
                                        </div>
                                    </div>
									<!-- e_wrap_s -->
								</div>
								<!-- e_rwrap -->
	
                                <h4>Dosen Penanggung jawab</h4>
                                <!-- s_rwrap -->
                                <div class="rwrap frwrap">
    
                                    <!-- s_wrap_s -->
                                    <div class="wrap_s a1">                                       
                                       <span class="tt2">Dosen Penanggung jawab</span>
                                       <div class="wrap_fss"> 
                                       		<input class="ip_tt2 tt2 open1" id="mpf" type="button">
                                       </div>
                                    </div>
                                    <!-- e_wrap_s -->
                                </div>
                                <!-- e_rwrap -->
    
                                <!-- s_ con_wrap -->
                                <div class="con_wrap" id="popupMpfSubmit" style="height:150px;">
                                
	                                <c:forEach var="mpfList" items="${mpfList}" step="1" varStatus="status">
		                                <div class="a_mp" name="user_seq_${mpfList.user_seq }"><input type="hidden" name="mpf_user_seq" value="${mpfList.user_seq}">
											<span class="pt01">
											<c:choose>
												<c:when test='${mpfList.picture_path ne null and mpfList.picture_path ne ""}'>
													<img src="${mpfList.picture_path}" alt="사진" class="pt_img">
												</c:when>
												<c:otherwise>
													<img src="${DEFAULT_PICTURE_IMG}" alt="사진" class="pt_img">
												</c:otherwise>
											</c:choose>
											</span>
											<span class="ssp1">${mpfList.name }(${mpfList.department_name })</span>
											<button type="button" class="btn_c" name="pf_del_btn" onClick="delMpf(${mpfList.user_seq},${status.index } , this);">X</button>
										</div>
									</c:forEach>        

                                    <!-- s_ 교수님 사진 1set -->
                                    <!-- <div class="a_mp">
                                        <span class="pt01"><img src="../img/ph_3.png" alt="사진"
                                            class="pt_img"></span><span class="ssp1">가교수(이비인후과)</span>
                                        <button class="btn_c" title="삭제하기">X</button>
                                    </div> -->
                                    <!-- e_ 교수님 사진 1set -->
                                    
                                </div>
                                <!-- e_ con_wrap -->
                                
                                
                                <h4>부Dosen Penanggung jawab</h4>
                                <!-- s_rwrap -->
                                <div class="rwrap frwrap">
    
                                    <!-- s_wrap_s -->
                                    <div class="wrap_s a1">                                       
                                       <span class="tt2">부Dosen Penanggung jawab</span>
                                       <div class="wrap_fss"> 
                                       	<input class="ip_tt2 tt2 open1" id="pf" type="button">
                                       </div>
                                    </div>
                                    <!-- e_wrap_s -->
                                </div>
                                <!-- e_rwrap -->
    
                                <!-- s_ con_wrap -->
                                <div class="con_wrap" id="popupPfSubmit" style="height:150px;">
                                    <c:forEach var="pfList" items="${pfList}" step="1" varStatus="status">
                                        <div class="a_mp" name="user_seq_${pfList.user_seq }"><input type="hidden" name="pf_user_seq" value="${pfList.user_seq}">
                                            <span class="pt01">
                                            <c:choose>
												<c:when test='${pfList.picture_path ne null and pfList.picture_path ne ""}'>
													<img src="${pfList.picture_path}" alt="사진" class="pt_img">
												</c:when>
												<c:otherwise>
													<img src="${DEFAULT_PICTURE_IMG}" alt="사진" class="pt_img">
												</c:otherwise>
											</c:choose>
											</span>
                                            <span class="ssp1">${pfList.name }(${pfList.department_name })</span>
                                            <button type="button" class="btn_c" name="pf_del_btn" onClick="delMpf(${pfList.user_seq},${status.index } , this);">X</button>
                                        </div>
                                    </c:forEach>
                                    <!-- s_ 교수님 사진 1set -->
                                    <!-- <div class="a_mp">
                                        <span class="pt01"><img src="../img/ph_3.png" alt="사진"
                                            class="pt_img"></span><span class="ssp1">가교수(이비인후과)</span>
                                        <button class="btn_c" title="삭제하기">X</button>
                                    </div> -->
                                    <!-- e_ 교수님 사진 1set -->
                                    
                                </div>
                                <!-- e_ con_wrap -->
	
							</div>
							<!-- e_sch_wrap -->
	
						</div>
						<!-- e_wrap_wrap -->
	                    </form>
						<div class="bt_wrap" style="margin-bottom:15px;">
							<c:choose>
								<c:when test='${currList.curr_seq ne null and currList.curr_seq ne ""}'>
								<button class="bt_2" name="saveBtn" onclick="curriculumSubmit();">perbaiki</button>
								</c:when>
								<c:otherwise>
								<button class="bt_2" name="saveBtn" onclick="curriculumSubmit();">simpan</button>
								</c:otherwise>
							</c:choose>
							<button class="bt_3" onclick="reset();">batal</button>
						</div>
	
					</div>
					<!-- e_adm_content1 -->
				</div>
				<!-- e_main_con -->
	
			</div>
			<!-- e_contents -->
		</div>
		<!-- e_container_table -->
		
		<!-- s_ 팝업 : 교수 tambahkan -->
        <div id="m_pop1" class="pop_up_pfadd mo1">
            <input type="hidden" id="popupType" value=""> 
            <div class="pop_wrap">
                <button class="pop_close close1" type="button">X</button>
                <p class="t_title">교수 tambahkan</p>
                
                <!-- s_pop_ipwrap1 -->
                <div class="pop_ipwrap1">
                    
                    <!-- s_wrap2_lms_uselectbox -->
                    <div class="wrap2_lms_uselectbox">
                        <div class="uselectbox" id="department_code" value="">
                            <span class="uselected">jurusan khusus</span><span class="uarrow">▼</span>
                            <div class="uoptions">
                                <span class="uoption firstseleted" value="">pilih</span>
                                <c:forEach var="specialtyCodeList" items="${specialtyCodeList}">
                                    <span class="uoption" value="${specialtyCodeList.code}">${specialtyCodeList.code_name}</span>
                                </c:forEach>                                  
                            </div>
                        </div>                                  
                    </div>
                <!-- e_wrap2_lms_uselectbox --> 

                    <span class="ip_tt">nama </span>
                    <div class="pop_date">
                        <input type="text" class="ip_search" id="mpf_name" placeholder="nama 을 입력하세요."
                         onkeypress="javascript:if(event.keyCode==13){curriculumMpfList(); return false;}">
                        <button type="button" class="btn_search1" onClick="curriculumMpfList();">Pencarian</button>
                    </div>
                </div>
                <!-- e_pop_ipwrap1 -->                  
                                                                        
				<!-- s_table_b_wrap -->
				<div class="table_b_wrap">  
					<!-- s_pop_twrap0 -->
					<div class="pop_twrap0">
						<!-- s_box_tt -->   
						<div class="box_tt">                         
							<span class="th01 w1">nama </span>
							<span class="th01 w2">사번</span>
							<span class="th01 w3">departement</span>
							<span class="th01 w4">jurusan khusus</span>
							<span class="th01 w5">kontak</span>
							<span class="th01 w6">E-mail</span>
							<span class="th01 w7">pilih</span>
						</div>
                        <!-- s_box_tt --> 
                    </div>
                    <!-- e_pop_twrap0 -->
                    <!-- s_pop_twrap1 -->
                    <div class="pop_twrap1">   
                        <table class="tab_table ttb1">
                            <tbody id="mpf_add">
                                <!-- <tr class="tr01">                            
									<td class="ta_c w1">가교수</td>
									<td class="ta_c w2">5432</td>
									<td class="ta_c w3">내과</td>
									<td class="ta_c w4">호흡기내과</td>
									<td class="ta_c ls_1 w5">010-1234-5678</td>
									<td class="ta_c ls_1 w6"><a class="mailto_1" href="mailto:testid0000@gmail.com" target="_top">testid0000@gmail.com</a></td>
									<td class="ta_c w8"><label class="chk01"><input type="checkbox"><span class="slider round">pilih</span></label></td>
                                </tr> -->
                            </tbody>
                        </table>
                    </div>        
                    <!-- e_pop_twrap1 -->
                    <!-- s_pht -->
                    <div class="pht">
                        <div class="wrap2">
                            <span class="tt_1">pilih된 교수님</span><span class="tt_2" id="selectMpfCount"></span>
                        </div>                    
                        <div class="con_wrap"> 
                            <div class="con_s2" id="select_pf"> 
                              
                            </div>
                        </div>         
                    </div>
                    <!-- e_pop_table -->
                </div>                       
                <!-- e_pht --> 

                <div class="t_dd">
                    <div class="pop_btn_wrap2">
                        <button type="button" type="button" class="btn01" onclick="popupMpfSubmit();">registrasi</button>
                        <button type="button" type="button" class="btn02" onclick="$('#m_pop1').hide();">batal</button>
                    </div>
                </div>
            </div>  
        </div>
        <!-- e_ 팝업 : 교수 tambahkan -->
	</body>
</html>