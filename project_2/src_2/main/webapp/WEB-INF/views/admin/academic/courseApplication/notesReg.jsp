<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
    <head>
    	<script src="${HOME}/resources/smarteditor_v2.9/js/service/HuskyEZCreator.js"></script>     
        <script>
        	var editor_object = [];
	        $(document).ready(function(){
	        	nhn.husky.EZCreator.createInIFrame({
	 		       oAppRef: editor_object,
	 		       elPlaceHolder: "content",
	                sSkinURI : "${HOME}/resources/smarteditor_v2.9/SmartEditor2Skin_ko_KR.html"     ,
	                htParams : {
	 		           // 툴바 사용 여부 (true:사용/ false:사용하지 않음)
	 		           bUseToolbar : true,            
	 		           // 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
	 		           bUseVerticalResizer : true,    
	 		           // 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
	 		           bUseModeChanger : true
	 		       }
	 		   });
	        	
	        	//파일 registrasi
		        $(document).on("change", "input[name=file]", function(){
		        	$("#fileListAdd").empty();
		        	$("#fileInputAdd input").not("#file"+fileIndex).remove();
		        	
		        	var fileExt = $(this).val().split('.').pop().toLowerCase(); //마지막 . 위치로 확장자 자름
		        	
					if($.inArray(fileExt, ["jpg","jpeg","gif","png","bmp","mp3","mp4","xls","xlsx","txt","hwp","pdf","zip"]) == -1) {
						alert("jpg,jpeg,gif,png,bmp,mp3,mp4,xls,xlsx,txt,hwp,pdf,zip 파일만 업로드 할수 있습니다.");
						$("#file"+fileIndex).remove();
						return;
					}
		 				
					var htmls = '';
					var fileValue = $(this).val().split("\\");
					var fileName = fileValue[fileValue.length-1]; // 파일명
					htmls += '<li class="wrap"><span class="sp1">'+fileName+'</span><button class="btn_c" title="삭제하기" onClick="fileDelete('+fileIndex+', this);">X</button></li>';
		   			$("#fileListAdd").append(htmls); 
		        });		
	        	
		        getRefundInfo();
	        });		   
		   		   
	        var fileIndex = 0;
    		//파일 인풋 tambahkan해서 해당 인풋 클릭하게 만든다.
    		function fileAdd(){
                fileIndex++;
    			$("#fileInputAdd").append('<input type="file" name="file" id="file'+fileIndex+'" style="display:none;"/>');
    			$("#fileInputAdd input").last().click();
    		}		
    		
    		function getRefundInfo(){
								
            	$.ajax({
            		type: "POST",
            		url: "${HOME}/ajax/admin/academic/courseApplication/notes/list",
            		data: {
            			},
           			dataType: "json",
           			success: function(data, status) {
           				if(data.status=="200"){
           					var htmls = "";
           					$("#content").val(data.list.content);
           					
           					if(!isEmpty(data.list.file_path)){
           						htmls += '<li class="wrap"><span class="sp1" onclick="fileDownload(\'${HOME}\', \'${RES_PATH}'+data.list.file_path+'\', \'\', \''+data.list.file_name+'\');">'+data.list.file_name+'</span>'
           						+'<button class="btn_c" title="삭제하기" onClick="fileDelete('+fileIndex+', this);">X</button></li>';
           						$("#fileListAdd").html(htmls);           						
           					}
           				}
        			},
        			error: function(xhr, textStatus) {
        				//alert("오류가 발생했습니다.");
        				document.write(xhr.responseText);
       				},beforeSend:function() {
  					},
  					complete:function() {
    				}
     			}); 
		  	}
    		
    		function notesSubmit(){
    			editor_object.getById["content"].exec("UPDATE_CONTENTS_FIELD", []);
    			$("#notesForm").ajaxForm({
                    type: "POST",
                    url: "${HOME}/ajax/admin/academic/courseApplication/notes/insert",
                    dataType: "json",
                    success: function(data, status){
                        if (data.status == "200") {
                            alert("simpan이 완료되었습니다.");
                            location.href='${HOME}/admin/academic/courseApplication/notes';
                        } else {
                            alert(data.status);
                            $.unblockUI();                          
                        }
                        
                    },
                    error: function(xhr, textStatus) {
                        document.write(xhr.responseText); 
                        $.unblockUI();                      
                    },
        			uploadProgress: function(event, position, total, percentComplete) {
        			    $("#progressbar").width(percentComplete + '%');
        			    $("#statustxt").html(percentComplete + '%');
        			    if(percentComplete>50) {
        			    	$("#statustxt").css('color','#fff');
        			    }
        			},beforeSend:function() {
                        $.blockUI();                        
                    },complete:function() {
                        $.unblockUI();                      
                    }                       
                });     
    			$("#notesForm").submit();   			
    		}
		</script>
	</head>

	<body>
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
			<!-- s_container_table -->
			<div id="container" class="container_table">
				<!-- s_contents -->
				<div class="contents main">
	
					<!-- s_left_mcon -->
					<div class="left_mcon aside_l grd">
						<div class="sub_menu adm_grd">
							<div class="title">manajemen akademik</div>
							<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">sistem management akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">학사코드manajemen</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on">menejemen kurikulum akadmik</span>
							</div>
							<ul class="panel on">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">menejemen kurikulum akadmik</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2 on">수강신청manajemen</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicReg/list'">management registration akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">Manajemen kompetensi kelulusan</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
						</div>
					</div>
					<!-- e_left_mcon -->
	
					<!-- s_main_con -->
					<div class="main_con adm_grd">
						<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
	
						<!-- s_tt_wrap -->
						<div class="tt_wrap">
							<h3 class="am_tt"><span class="tt">유의사항manajemen</span></h3>
						</div>
						<!-- e_tt_wrap -->
	
						<!-- s_adm_content2 -->
						<div class="adm_content2">
							<!-- s_tt_wrap -->
		
							<!-- s_wrap_wrap -->
							<div class="wrap_wrap">
		
								<!-- s_btnwrap_s1f -->
								<div class="btnwrap_s1f">
									<button class="ic_v3" onclick="location.href='${HOME}/admin/academic/courseApplication/list'">수강신청 목록</button>
								</div>
								<!-- e_btnwrap_s1f -->
		
								<!-- s_rfr_con -->
								<div class="rfr_con">
									<form id="notesForm" onsubmit="return false;" enctype="multipart/form-data">
										<table class="mlms_tb4">
											<tbody>
												<tr>
													<th scope="row"><span class="sp1">isi</span></th>
													<td class="tarea free_textarea">
														<textarea class="tarea01" style="height: 450px !important;" id="content" name="content"></textarea>
													</td>
												</tr>
												<tr>
													<th rowspan="2" scope="row"><span class="sp1">파일첨부</span></th>
			
													<td>
														<input class="ip_pt2" value="파일을 첨부해주세요." type="text">
														<button class="btn1" onClick="fileAdd();">pilih</button>
														<div id="fileInputAdd" style="display:none;">
														</div>
													</td>
												</tr>
												<tr>
													<td>
														<ul class="f_add1" id="fileListAdd">		
																	
														</ul>
													</td>
												</tr>
											</tbody>
										</table>
									</form>
								</div>
								<!-- e_rfr_con -->
		
								<!-- s_btn_wrap_r --> 
								<div class="btn_wrap_r">
									<button class="bt_2" onclick="notesSubmit();">registrasi</button>
									<button class="bt_3" onclick="location.href='${HOME}/admin/academic/courseApplication/list'">batal</button>
								</div>
								<!-- e_btn_wrap_r -->
							</div>
							<!-- e_wrap_wrap -->
		
						</div>
						<!-- e_adm_content2 -->
					</div>
					<!-- e_main_con -->
	
				</div>
				<!-- e_contents -->
			</div>
			<!-- e_container_table -->		
	
	</body>
</html>