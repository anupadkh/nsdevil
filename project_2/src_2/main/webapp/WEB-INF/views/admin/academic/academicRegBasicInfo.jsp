<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<script>
			$(document).ready(function() {
				if(isEmpty($("#aca_seq").val())){
					$("button[name=assignBtn]").hide();
					$("button[name=assignCancelBtn]").hide();
					$("button.open101").hide();
				}
				
				//$(".uselected, .uarrow").css("pointer-events", "none");
				if(!isEmpty($("#aca_seq").val())){
					$(".con").show();
					$("div.wrap_s1_uselectbox").hide();
					$("#l_seq_name").show();
					$("#m_seq_name").show();
					$("#s_seq_name").show();
					$("#xs_seq_name").show();
					$("#input_year").show();
					getAcademic();
					getAssignStCnt();
				}else{
					acasystemStageOfList(1);
				}
				
				bindPopupEvent("#m_pop101", ".open101");
			});
		
			function getAssignStCnt(){				
		    	$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/admin/academic/academicReg/studentAssign/assignStCnt",
	                data: {
	                    },
	                dataType: "json",
	                success: function(data, status) {
	                	if(data.status == "200"){
	                		$("#total").text(data.cnt);
	                		$("#attend").text(data.info.attend);
	                		$("#leave").text(data.info.leave);
	                		$("#grad").text(data.info.grad);
	                		$("#etc").text(data.info.etc);							
	                	}else{
	                		alert("배정된 학생 수 가져오기 실패");
	                	}
	                },
	                error: function(xhr, textStatus) {
	                    alert("배정된 학생 수 가져오는중 오류가 발생하였습니다.");
	                    document.write(xhr.responseText);
	                },beforeSend:function() {
	                	$.blockUI();
	                },
	                complete:function() {
	                	$.unblockUI();
	                }
	            });
		    }
			
			function getAcaSystem(level, seq){
				if(level == 1){
					$("#m_seq span.uselected").html("pilih");
		            $("#m_seq").attr("value","");
		            acasystemStageOfList(2, seq);
				}else if(level == 2){
		            acasystemStageOfList(3, $("#l_seq").attr("value"), seq);
			        $("#s_seq span.uselected").html("pilih");
			        $("#s_seq").attr("value","");
				}else if(level == 3)
					acasystemStageOfList(4, $("#l_seq").attr("value"), $("#m_seq").attr("value"), seq);
				/* else if(level == 4)
					getStudentCount(seq); */
					
				$("#xs_seq span.uselected").html("pilih");
		        $("#xs_seq").attr("value","");
			}
			
			//sistem akademik 분류별 가져오기
		    function acasystemStageOfList(level, l_seq, m_seq, s_seq){
		        if(level == ""){
		            alert("분류 없음");
		            return;                 
		        }
		        $.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicSystem/acaStageOfList",
		            data: {
		                "level" : level,
		                "l_seq" : l_seq,
		                "m_seq" : m_seq,
		                "s_seq" : s_seq                        
		            },
		            dataType: "json",
		            success: function(data, status) {
		                var htmls = '<span class="uoption firstseleted" onClick="getAcaSystem('+level+',\'\');" value="">pilih</span>';
		                $.each(data.list, function(index){
		                    htmls +=  '<span class="uoption" onClick="getAcaSystem('+level+','+this.aca_system_seq+');" value="'+this.aca_system_seq+'">'+this.aca_system_name+'</span>';                         
		                });
		                if(level == 1){
		                    $("#l_seq_add span").remove();
		                    $("#l_seq_add").append(htmls);                          
		                }
		                else if(level == 2){
		                    $("#m_seq_add span").remove();
		                    $("#m_seq_add").append(htmls);
		                }
		                else if(level == 3){
		                    $("#s_seq_add span").remove();
		                    $("#s_seq_add").append(htmls);
		                }else{
		                	$("#xs_seq_add span").remove();
		                    $("#xs_seq_add").append(htmls);
		                }
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });                 
		    }
			
			function getStudentCount(seq){
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicReg/academicRegBasicInfo/studentCount",
		            data: {
		                "aca_system_seq" : seq                        
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	$("#attend").text("");
		            	$("#leave").text("");
		            	$("#total").text("");
		                if(data.status=="200"){
		                	$("#attend").text(data.studentCount.attend);
			            	$("#leave").text(data.studentCount.leave);	
			            	$("#total").text(data.studentCount.total+"명");
		                }
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
			
			function academicInsert(){
				if(isEmpty($("#academic_name").val())){
					alert("학사 명을 입력해주세요");
					return;
				}
					
				if(isEmpty($("#aca_seq").val())){
					if(isEmpty($("#year").attr("value"))){
						alert("tahun를 pilih해주세요");
						return;
					}				
	
					if(isEmpty($("#xs_seq").attr("value"))){
						alert("sistem akademik를 pilih해주세요");
						return;
					}
				}
				
				if(isEmpty($("#start_date").val())){
					alert("시작일을 입력해주세요");
					return;
				}	
				
				if(isEmpty($("#end_date").val())){
					alert("종료일을 입력해주세요");
					return;
				}
				
				
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicReg/academicRegBasicInfo/insert",
		            data: {
		                "aca_seq" : $("#aca_seq").val() 
		                ,"academic_name" : $("#academic_name").val() 
		                ,"year" : $("#year").attr("value")
		                ,"aca_system_seq" : $("#xs_seq").attr("value")
		                ,"start_date" : $("#start_date").val()
		            	,"end_date" : $("#end_date").val()
		            	,"aca_state" : $("#aca_state").val()
		            },
		            dataType: "json",
		            success: function(data, status) {
		                if(data.status=="200"){
		                	alert("simpan 완료");
		                	post_to_url('${HOME}/admin/academic/academicReg/academicRegBasicInfo/create',{"aca_seq" : data.aca_seq});
		                }else if(data.status=="201"){
		                	alert("해당 tahun에 해당 sistem akademik는 \n 이미 registrasi되어 있습니다.");
		                }		                	
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });				
			}
			
			function academicAssign(flag){
				
				var confirmMsg = "";
				var statusMsg = "";
				
				if(flag=="Y"){
					confirmMsg = "해당 학사를 확정상태로 변경하시겠습니까?";
					statusMsg = "확정 완료";
				}else{
					confirmMsg = "확정 batal 하시겠습니까?";
					statusMsg = "확정 batal 완료";
				}
				
				if(isEmpty($("#aca_seq").val())){
					alert("학사를 먼저 simpan해주세요.");
					return;
				}
				
				if(!confirm(confirmMsg)){
					return;
				}
				
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicReg/academicRegBasicInfo/confirm",
		            data: {
		                "flag" : flag
		                ,"confirm_name" : "bagicInfo"
		                ,"aca_seq" : $("#aca_seq").val()
		            },
		            dataType: "json",
		            success: function(data, status) {
		                if(data.status=="200"){
	                		alert(statusMsg);
		                	getAcademic();
		                }else if(data.status=="201"){
		                	alert("status akademik가 [개설] 상태인 학사만 batal 가능합니다.");
		                }		                	
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });	
			}
			
			function getAcademic(){
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicReg/academicRegBasicInfo/list",
		            data: {
		                "aca_seq" : $("#aca_seq").val() 
		            },
		            dataType: "json",
		            success: function(data, status) {
		                if(data.status=="200"){
		                	var aca_state = "";
		                	switch(data.acaInfo.aca_state){
	                            case "01" : aca_state = "개설(비공개)";break;
	                            case "02" : aca_state = "개설(공개)";break;
	                            case "03" : aca_state = "학사 중(수업 중)";break;
	                            case "04" : aca_state = "학사종료(미확정)";break;
	                            case "05" : aca_state = "학사종료(성적산정)";break;
	                            case "06" : aca_state = "학사종료(성적발표)";break;
	                            case "07" : aca_state = "성적발표 종료(미확정)";break;
	                            case "08" : aca_state = "학사최종종료(확정)";break;                            
	                        }
		                	if(!isEmpty(data.acaInfo.academic_name))
		                		$("#academicNameTitle").text(data.acaInfo.academic_name);
		                	else
		                		$("#academicNameTitle").hide();
		                	
			            	$("#academicState").text(aca_state); //타이틀 status akademik
		                	$("#aca_state_name").text(aca_state);
		                	$("#academic_name").val(data.acaInfo.academic_name);
			                $("#start_date").val(data.acaInfo.start_date);
			            	$("#end_date").val(data.acaInfo.end_date);
			            	$("#aca_state").val(data.acaInfo.aca_state);
			            	$("#l_seq_name").val(data.acaInfo.l_aca_name);
			            	$("#m_seq_name").val(data.acaInfo.m_aca_name);
			            	$("#s_seq_name").val(data.acaInfo.s_aca_name);
			            	$("#xs_seq_name").val(data.acaInfo.aca_system_name);
			            	$("#input_year").val(data.acaInfo.year);
			            	//확정상태 확인
			            	if(data.acaInfo.confirm_flag=="N"){
								$("button[name=assignBtn]").show();
			            		$("button[name=saveBtn]").show();
			            		$("button[name=cancelBtn]").show();	
								$("button[name=assignCancelBtn]").hide ();		            		
			            	}else{
								$("button[name=assignBtn]").hide();
			            		$("button[name=saveBtn]").hide();
			            		$("button[name=cancelBtn]").hide();		
								$("button[name=assignCancelBtn]").show();
			            	}
			            		
		                }	                	
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });				
			}
			
			function popupOpen(){
				$("#thisState").text($("#academicState").text());
				targetMemo($("#aca_state").val());
			}
			
			function changeAcaState(state){
				$("#submitStateName").text($("#acaStateChangeValue span[value="+state+"]").text());
				targetMemo(state);
			}
			
			function getChangeState(){
				
				
				var state = $("div.wrap_s3_uselectbox div.uselectbox").attr("value");
				var stateName = $("div.wrap_s3_uselectbox span.uselected").text();

				if(!confirm(stateName + " 상태로 변경하시겠습니까?")){
					return;
				}
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicReg/academicRegBasicInfo/stateUpdate",
		            data: {
		                "aca_seq" : $("#aca_seq").val()
		                ,"aca_state" : state
		            },
		            dataType: "json",
		            success: function(data, status) {
		                if(data.status=="200"){
		                	alert("상태 변경 완료");
		                	getAcademic();
		                }else if(data.status=="201"){
		                	alert("informasi dasar, Penetapan Kurikulum, waktu표가 \n확정상태인 경우에만 학사중 상태로\n변경 가능합니다.");		                	
		                }             	
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
			
			//상태에 따라 메시지 바뀜.
			function targetMemo(state){
				var mpf="Dosen Penanggung jawab";
				var pf = "교수";
				var st = "학생";
				var ad = "행정";
				if(state=="01" || state=="02"){
					$("#target1").text(mpf);
					$("#memo1").text("rencan kurikulum registrasi");
					$("#target2").text(pf);
					$("#memo2").text("수업계획서 registrasi 가능");
					$("#target3").text(st);
					$("#memo3").text("접근 불가");		
					$("#target3").show();
					$("#memo3").show();			
				}else if(state=="03"){
					$("#target1").text(mpf);
					$("#memo1").text("rencan kurikulum perbaiki");
					$("#target2").text(pf);
					$("#memo2").text("수업계획서 perbaiki 가능");
					$("#target3").text(st);
					$("#memo3").text("교육과정/수업계획서/waktu표 조회");		
					$("#target3").show();
					$("#memo3").show();
				}else if(state=="04"){
					$("#target1").text(mpf);
					$("#memo1").text("rencan kurikulum perbaiki");
					$("#target2").text(pf);
					$("#memo2").text("수업계획서 perbaiki 가능");
					$("#target3").text(st);
					$("#memo3").text("접근 불가");		
					$("#target3").show();
					$("#memo3").show();
				}else if(state=="05"){
					$("#target1").text(ad);
					$("#memo1").text("최종 성적표 registrasi");
					$("#target2").text(pf);
					$("#memo2").text("rencan kurikulum, 수업계획서 perbaiki, 성적 registrasi");
					$("#target3").text(st);
					$("#memo3").text("수업계획서 및 과제 제출 불가");		
					$("#target3").show();
					$("#memo3").show();
				}else if(state=="06"){
					$("#target1").text(st);
					$("#memo1").text("성적 조회 가능");
					$("#target2").text(pf);
					$("#memo2").text("rencan kurikulum, 수업계획서 isi perbaiki 가능");
					$("#target3").hide();
					$("#memo3").hide();
				}else if(state=="07"){
					$("#target1").text(ad);
					$("#memo1").text("성적 perbaiki 가능");
					$("#target2").text(pf);
					$("#memo2").text("rencan kurikulum, 수업계획서 perbaiki 불가");
					$("#target3").hide();
					$("#memo3").hide();
				}else if(state=="08"){
					$("#target1").text(ad);
					$("#memo1").text("manajemen akademik 접근 조회뫈 가능");
					$("#target2").text(pf+","+st);
					$("#memo2").text("종료된 학사 노출 안됨 접근 불가");
					$("#target3").hide();
					$("#memo3").hide();
				}
				
			}
			
			function pageMoveScore(type, aca_seq){
		    	$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/admin/academic/academicReg/setAcaSeq",
	                data: {
	                	"aca_seq" : aca_seq
	                    },
	                dataType: "json",
	                success: function(data, status) {
	                	if(type=="curr")
	               			location.href="${HOME}/admin/academic/academicReg/currScore";
		     			else if(type=="soosi")
		     				location.href="${HOME}/admin/academic/academicReg/soosiScore";
		     			else if(type=="fe")
		     				location.href="${HOME}/admin/academic/academicReg/feScore";
	                },
	                error: function(xhr, textStatus) {
	                    alert("실패");
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                },beforeSend:function() {
	                	$.blockUI();
	                },
	                complete:function() {
	                	$.unblockUI();
	                }
	            });
		    }
		</script>
	</head>
	
	<body>
		<input type="hidden" name="aca_seq" id="aca_seq" value="${S_ACA_SEQ }">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
		<!-- s_container_table -->
		<div id="container" class="container_table">
			<!-- s_contents -->
			<div class="contents main">
	
				<!-- s_left_mcon -->
				<div class="left_mcon aside_l grd">
					<div class="sub_menu adm_grd">
						<div class="title">manajemen akademik</div>
						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">sistem management akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">학사코드manajemen</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">menejemen kurikulum akadmik</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">menejemen kurikulum akadmik</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청manajemen</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on" onClick="location.href='${HOME}/admin/academic/academicReg/list'">management registration akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">Manajemen kompetensi kelulusan</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- e_left_mcon -->
	
				<!-- s_main_con -->
				<div class="main_con adm_grd">
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<!-- s_tt_wrap -->
					<div class="tt_wrap">
						<h3 class="am_tt">
							<span class="tt">management registration akademik<span class="sign1"> &gt; </span>informasi dasar</span>
						</h3>
						<span class="sp_state">status akademik : <span class="tt" id="academicState"></span></span>
						<h4 class="h4_tt" id="academicNameTitle"></h4>
					</div>
					<!-- e_tt_wrap -->
	
					<!-- s_adm_content1 -->
					<div class="adm_content3">
						<!-- s_tt_wrap -->
	
						<!-- s_wrap_wrap -->
						<div class="wrap_wrap">
	
							<div class="tab_wrap_cc">											
								<c:choose>
									<c:when test="${S_ACA_SEQ ne null}">		
										<button class="tab01 tablinks active" onclick="getAcademic();">informasi dasar</button>
										<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/curriculumAssign'">Penetapan Kurikulum</button>
										<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/studentAssign'">학생배정</button>
										<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/schedule'">waktu표</button>
										<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/SFSurvey/list'">만족도조사</button>
										<button class="tab01 tablinks" onclick="pageMoveScore('curr','${S_ACA_SEQ }');">종합성적</button>
										<button class="tab01 tablinks" onclick="pageMoveScore('soosi','${S_ACA_SEQ }');">nilai yang diinginkan</button>
										<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/currReport'">운영보고서</button>		
									</c:when>
									<c:otherwise>
										<button class="tab01 tablinks active" onclick="">informasi dasar</button>
										<button class="tab01 tablinks" onclick="alert('pilih된 학사가 없습니다.\n학사 informasi dasar를 먼저 registrasi하세요.');">Penetapan Kurikulum</button>
										<button class="tab01 tablinks" onclick="alert('pilih된 학사가 없습니다.\n학사 informasi dasar를 먼저 registrasi하세요.');">학생배정</button>
										<button class="tab01 tablinks" onclick="alert('pilih된 학사가 없습니다.\n학사 informasi dasar를 먼저 registrasi하세요.');">waktu표</button>
										<button class="tab01 tablinks" onclick="alert('pilih된 학사가 없습니다.\n학사 informasi dasar를 먼저 registrasi하세요.');">수업만족도</button>										
										<button class="tab01 tablinks" onclick="alert('pilih된 학사가 없습니다.\n학사 informasi dasar를 먼저 registrasi하세요.');">종합성적</button>
										<button class="tab01 tablinks" onclick="alert('pilih된 학사가 없습니다.\n학사 informasi dasar를 먼저 registrasi하세요.');">nilai yang diinginkan</button>
										<button class="tab01 tablinks" onclick="alert('pilih된 학사가 없습니다.\n학사 informasi dasar를 먼저 registrasi하세요.');">운영보고서</button>	
									</c:otherwise>
								</c:choose>
							</div>
	
							<!-- s_sch_wrap -->
							<div class="sch_wrap regi">
								<!-- s_rwrap -->
								<div class="rwrap">
	
									<!-- s_wrap_s -->
									<div class="wrap_s">
										<span class="tt">gelar akademik</span> 
										<div class="con">
											<input class="ip_tt_1" name="academic_name" id="academic_name" value="" type="text">
										</div>
									</div>
									<!-- e_wrap_s -->
	
									<!-- s_wrap_s -->
									<div class="wrap_s">
            							<span class="tt">tahun</span>
										<div class="wrap_s1_uselectbox" style="margin-right:12px;">
											<div class="uselectbox" id="year" value="">
												<span class="uselected">pilih</span><span class="uarrow">▼</span>
												<div class="uoptions" style="display: none;" id="">
													<span class="uoption firstseleted">pilih</span>
													<span class="uoption" value="${thisYear }">${thisYear }</span>
													<span class="uoption" value="${nextYear }">${nextYear }</span>
												</div>
											</div>
										</div>
										<div class="con" style="display:none;">
											<input class="ip_tt_2" value="" type="text" id="input_year" disabled style="display:none;">
           								</div>      
									</div>
									<!-- e_wrap_s -->
	
									<!-- s_wrap_s -->
									<div class="wrap_s">
										<span class="tt">sistem akademik</span> 
										<div class="wrap_s1_uselectbox" style="margin-right:12px;">
											<div class="uselectbox" id="l_seq" value="">
												<span class="uselected">pilih</span><span class="uarrow">▼</span>
												<div class="uoptions" style="display: none;" id="l_seq_add">
													<span class="uoption firstseleted">Keseluruhan</span>
												</div>
											</div>
										</div>
										
										<div class="wrap_s1_uselectbox" style="margin-right:12px;">
											<div class="uselectbox" id="m_seq" value="">
												<span class="uselected">pilih</span><span class="uarrow">▼</span>
												<div class="uoptions" style="display: none;" id="m_seq_add">
													
												</div>
											</div>
										</div>
										
										<div class="wrap_s1_uselectbox" style="margin-right:12px;">
											<div class="uselectbox" id="s_seq" value="">											
												<span class="uselected">pilih</span><span class="uarrow">▼</span>
												<div class="uoptions" style="display: none;" id="s_seq_add">
													
												</div>
											</div>
										</div>	
										
										<div class="wrap_s1_uselectbox" style="margin-right:12px;">
											<div class="uselectbox" id="xs_seq" value="">											
												<span class="uselected">pilih</span><span class="uarrow">▼</span>
												<div class="uoptions" style="display: none;" id="xs_seq_add">
													
												</div>
											</div>
										</div>							
										<div class="con" style="display:none;">
											<input class="ip_tt_2" value="" id="l_seq_name" type="text" disabled style="display:none;">
								            <input class="ip_tt_2" value="" id="m_seq_name" type="text" disabled style="display:none;">
								            <input class="ip_tt_2" value="" id="s_seq_name" type="text" disabled style="display:none;">
								            <input class="ip_tt_2" value="" id="xs_seq_name" type="text" disabled style="display:none;">
							            </div>		
									</div>
									<!-- e_wrap_s -->
									
									<!-- s_wrap_s -->
									<div class="wrap_s bd01">
										<span class="tt">periode설정</span> 
										<input type="text" class="dateyearpicker-input_1 ip_date" id="start_date" value="">
										<span class="sign">~</span> 
										<input type="text" class="dateyearpicker-input_1 ip_date" id="end_date" value="">
									</div>
									<!-- e_wrap_s -->
	
									<!-- s_wrap_s -->
									<div class="wrap_s bd01">
										<span class="tt">Target 학생그룹</span> <span class="tt_con" id="total"></span>
										<div class="btn_wrap">
											<span class="sp01">( 재학 : </span><span class="sp_n" id="attend"></span><span class="sp02">명, </span>
											<span class="sp01">휴학 : </span><span class="sp_n" id="leave"></span><span class="sp02">명, </span>
											<span class="sp01">졸업 : </span><span class="sp_n" id="grad">0</span><span class="sp02">명, </span>
											<span class="sp01">dan yang lainnya : </span><span class="sp_n" id="etc">0</span><span class="sp02">명 )</span>
										</div>
										<span class="sp03">현재 재학 중 학생만 Target그룹으로 지정 됨. ( 학사periode 중 학생registrasi정보 상태 값이 변경될 시, 변동이력만 표기됩니다. )</span>
									</div>
									<!-- e_wrap_s -->
	
									<!-- s_wrap_s -->
									<div class="wrap_s">
										<span class="tt">status akademik</span> 
										<input type="hidden" id="aca_state" value="01">
										<span class="tt_con" id="aca_state_name">개설(비공개)</span>
										<button class="btn_chng open101" onClick="popupOpen();">status akademik 변경</button>
									</div>
									<!-- e_wrap_s -->
	
								</div>
								<!-- e_rwrap -->
							</div>
							<!-- e_sch_wrap -->
	
						</div>
						<!-- e_wrap_wrap -->
	
						<div class="bt_wrap">
							<button class="bt_0" name="assignBtn" onclick="academicAssign('Y');" style="display:none;">확정</button>
							<button class="bt_2" name="saveBtn" onclick="academicInsert();">simpan</button>
							<button class="bt_3" name="cancelBtn" onclick="location.href='${HOME}/admin/academic/academicReg/list'">batal</button>
							<button class="bt_0" name="assignCancelBtn" onclick="academicAssign('N');" style="display:none;">확정 batal</button>
							
							<!-- s_pop_pop2 -->
							<div id="pop_pop2">
	
								<div class="spop2_2_2_text show" id="spop2_2_3">
									<span class="sp1">학사 informasi dasar가 perbaiki되었습니다. simpan하시겠습니까?</span>
	
									<button onclick="" class="btn01">simpan</button>
									<button onclick="loca" class="btn02">batal</button>
	
									<span onclick="myFunction1()" class="pop_close_s" title="닫기">X</span>
								</div>
							</div>
							<!-- e_pop_pop2 -->
						</div>
	
					</div>
					<!-- e_adm_content1 -->
				</div>
				<!-- e_main_con -->
	
			</div>
			<!-- e_contents -->
		</div>
		<!-- e_container_table -->
	
	
		<!-- s_status akademik 변경 팝업 -->
		<div id="m_pop101" class="pop_up_grd_chng mo101">
	
			<div class="pop_wrap">
				<p class="popup_title">status akademik 변경</p>
	
				<button class="pop_close close101" type="button">X</button>
	
				<div class="tt01">상태별 업무 Process</div>
				<div class="tt02">
					<span class="sp01">현재 status akademik</span><span class="sp02" id="thisState"></span>
				</div>
	
				<div class="wrap">
					<span class="tt03">변경</span>
	
					<div class="wrap_s3_uselectbox">
						<div class="uselectbox">
							<span class="uselected" value="">개설(비공개)</span> <span class="uarrow">▼</span>
							<div class="uoptions" style="display: none;" id="acaStateChangeValue">
								<span class="uoption firstseleted" value="01" onClick="changeAcaState('01');">개설(비공개)</span>
								<span class="uoption" value="02" onClick="changeAcaState('02');">개설(공개)</span>
								<span class="uoption" value="03" onClick="changeAcaState('03');">학사 중(수업 중)</span>
								<span class="uoption" value="04" onClick="changeAcaState('04');">학사종료(미확정)</span>
								<span class="uoption" value="05" onClick="changeAcaState('05');">학사종료(성적산정)</span>
								<span class="uoption" value="06" onClick="changeAcaState('06');">학사종료(성적발표)</span>
								<span class="uoption" value="07" onClick="changeAcaState('07');">성적발표 종료(미확정)</span>                                                
								<span class="uoption" value="08" onClick="changeAcaState('08');">학사최종종료(확정)</span>
							</div>
						</div>
					</div>
	
					<div class="sp_wrap">
						<span class="sp01" id="target1"></span>
						<span class="sp02" id="memo1"></span>
						<span class="sp01" id="target2"></span>
						<span class="sp02" id="memo2"></span>
						<span class="sp01" id="target3"></span>
						<span class="sp02" id="memo3"></span>
					</div>
				</div>
	
				<div class="pop_tt_wrap">
					<span class="sp01" id="submitStateName"></span>
					<span class="sp02">상태로 	변경합니다. 변경하시겠습니까?</span>
				</div>
	
				<div class="pop_tt_wrap tts">
					<span class="sp1">waktu표는 manajemen자만 perbaiki할 수 있게 됩니다.</span>
					<span class="sp1">학사periode이 만료되면 automatic으로 학사종료 (미확정)상태로 변경됩니다.</span>
					<span class="sp1"> </span>
					<span class="sp1">이전 상태로 pilih하여 변경할 수 있습니다.
					    <br>변경은 순차적으로 가능하며,
					    <br>변경된 이력이 있을 경우에만 이전 상태로 되돌릴 수 있습니다.
					    <br>(다음 상태로 전환은 불가)
					</span>
				</div>
	
				<!-- s_t_dd -->
				<div class="t_dd">
					<div class="pop_btn_wrap">
						<button type="button" class="btn01" onclick="getChangeState();">변경</button>
						<button type="button" class="btn02" onclick="$('.close101').click();">batal</button>
					</div>
				</div>
				<!-- e_t_dd -->
	
			</div>
	
		</div>
		<!-- e_status akademik 변경 팝업 -->
	
	</body>
</html>