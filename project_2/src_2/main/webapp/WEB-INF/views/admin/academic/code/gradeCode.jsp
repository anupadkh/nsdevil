<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<script>
	$(document).ready(function() {
		$(".mdf_wrap_b").hide();
	    $(document).on("click",".ip01a", function(){
	        $(this).parent("div.mdf_wrap_a").hide();
	        $(this).parent().parent().children("div.mdf_wrap_b").show();
	        $(this).closest("td").find("input.ip01b").focus();
	    });
	    
		$(document).on("click", ".btn_x", function(){
			var gs_seq = $(this).closest("tr").find("input[name=gs_seq]").val();
			
			if(confirm("삭제하시겠습니까?")){
				if(isEmpty(gs_seq))
					$(this).closest("tr").remove();
				else{						
					$.ajax({
						type : "POST",
						url : "${HOME}/ajax/admin/academic/codeManagement/deleteGradeCode",
						data : {
							"gs_seq" : gs_seq
						},
						dataType : "json",
						success : function(data, status) {
							if(data.status == "200"){
								alert("삭제 완료 하였습니다.");
								getCode();
							}else{
								alert("삭제 실패하였습니다.");
							}
						},
						error : function(xhr, textStatus) {
							document.write(xhr.responseText);
							$.unblockUI();
						},
						beforeSend : function() {
							$.blockUI();
						},
						complete : function() {
							$.unblockUI();
						}
					});
				}
			}	        		
	    });
		
		$(document).on("click", ".btn_add", function(){
			var htmls = '';
			htmls += '<tr>'
				+'<td class="td_1">'
				+'<div class="mdf_wrap_b" style="">'
				+'<input type="hidden" name="gs_seq" value="">'
				+'<input type="text" class="ip01b" name="grade_code" value="">'
				+'<button class="btn_x" title="삭제하기">X</button>'
				+'</div>'
				+'</td>'
				+'<td class="td_1">'
				+'<div class="mdf_wrap_b" style="">'
				+'<input type="text" class="ip01b" name="grade_percent" onkeyup="onlyNumCheck(this);" value="">'
				+'<button class="btn_x" title="삭제하기">X</button>'
				+'</div>'
				+'</td>'
				+'</tr>';	
			$("#codeListAdd tr").last().before(htmls);
		});
				
		getCode();
	});

	function getCode() {
		$.ajax({
			type : "POST",
			url : "${HOME}/ajax/admin/academic/codeManagement/gradeCodeList",
			data : {
			},
			dataType : "json",
			success : function(data, status) {
				var htmls = "";

				$.each(data.gradeCodeList, function(index){
					htmls += '<tr>'
						+'<td class="td_1">'
						+'<div class="mdf_wrap_a">'
						+'<input type="text" class="ip01a" value="'+this.grade_code+'">'
						+'</div>'
						+'<div class="mdf_wrap_b" style="display:none;">'
						+'<input type="hidden" name="gs_seq" value="'+this.gs_seq+'">'
						+'<input type="text" class="ip01b" name="grade_code" value="'+this.grade_code+'">'
						+'<button class="btn_x" title="삭제하기">X</button>'
						+'</div>'
						+'</td>'
						+'<td class="td_1">'
						+'<div class="mdf_wrap_a">'
						+'<input type="text" class="ip01a" value="'+this.grade_percent+'">'
						+'</div>'
						+'<div class="mdf_wrap_b" style="display:none;">'
						+'<input type="text" class="ip01b" name="grade_percent" onkeyup="onlyNumCheck(this);" value="'+this.grade_percent+'">'
						+'<button class="btn_x" title="삭제하기">X</button>'
						+'</div>'
						+'</td>'
						+'</tr>';					
				});
				/* 
				htmls+='<tr name="add">'
					+'<td class="td_1" colspan="2">'
					+'<button class="btn_add" title="tambahkan"></button>'
					+'</td>'
					+'</tr>';
					 */
				$("#codeListAdd").html(htmls);
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
				$.unblockUI();
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}
	
	function insertCode(){
		if(!confirm("simpan하시겠습니까?")){
			return;
		}		
		
		var flag = true;
		
		$.each($("#codeListAdd tr").not("tr[name=add]"), function(index){
			if(isEmpty($(this).find("input[name=grade_code]").val())){
				alert("nilai기준명을 입력해주세요.");
				$(this).find("input[name=grade_code]").focus();
				flag=false;
				return false;
			}			
			
			if(isEmpty($(this).find("input[name=grade_percent]").val())){
				alert("비율을 입력해주세요.");
				$(this).find("input[name=grade_percent]").focus();
				flag=false;
				return false;
			}						
		});
		
		if(flag == false)
			return;
		
		$("#codeForm").ajaxForm({
			type : "POST",
			url : "${HOME}/ajax/admin/academic/codeManagement/gradeCodeInsert",
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {
					alert("registrasi이 완료 되었습니다.");
					getCode();
				}else {
					alert("registrasi 실패 하였습니다.");
				}
			},
			error : function(xhr, textStatus) {
				alert(textStatus);
				//document.write(xhr.responseText);
				$.unblockUI();
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
		$("#codeForm").submit();
		
	}
</script>
</head>

<body>
	<!-- s_container_table -->
	<div id="container" class="container_table">
		<!-- s_contents -->
		<div class="contents main">

			<!-- s_left_mcon -->
			<div class="left_mcon aside_l grd">
				<div class="sub_menu adm_grd">
					<div class="title">manajemen akademik</div>

					<!-- s_학사코드 메뉴 -->
					<div class="boardwrap">
						<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">sistem management akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on">학사코드manajemen</span>
							</div>
							<ul class="panel on">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2 on">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">menejemen kurikulum akadmik</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">menejemen kurikulum akadmik</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청manajemen</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicReg/list'">management registration akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">Manajemen kompetensi kelulusan</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
					</div>
					<!-- e_학사코드 메뉴 -->

				</div>
			</div>
			<!-- e_left_mcon -->

			<!-- s_main_con -->
			<div class="main_con adm_grd">
				<!-- s_메뉴 접기 버튼 -->
				<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
				<!-- e_메뉴 접기 버튼 -->

				<!-- s_tt_wrap -->
				<div class="tt_wrap">
					<h3 class="am_tt">
						<span class="tt">학사코드 manajemen - nilai기준</span>
					</h3>
				</div>
				<!-- e_tt_wrap -->

				<!-- s_adm_content2n -->
				<div class="adm_content2n">
					<!-- s_tt_wrap -->

					<!-- s_wrap_wrap -->
					<div class="wrap_wrap">

						<!-- s_wrap_s -->
						<div class="wrap_s">
							<div class="wrap_s1_uselectbox">
								<div class="uselectbox">
									<span class="uselected">nilai기준</span> <span class="uarrow">▼</span>
									<div class="uoptions" style="display: none;">
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/completeCode'">kategori penuntasan</span>
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/administerCode'">manajemen Klasifikasi</span>
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/lessonMethodCode'">tidak ada perkuliahan유형</span>
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/feCode'">형성penilaian</span>
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/gradeCode'">nilai기준</span>
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/seCode'">총괄penilaian기준</span>
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/unitCode'">수업 Area/tingkat</span>
									</div>
								</div>
							</div>
							<button class="btn_tt1">Pencarian</button>

						</div>
						<!-- e_wrap_s -->


						<form id="codeForm"  onSubmit="return false;">
							<table class="mlms_tb s1">
								<thead>
									<tr>
			                          <th class="th01 b_2 w1">nilai기준</th>
			                          <th class="th01 b_2 w1">절대penilaian (%)</th>
									</tr>
								</thead>
								<tbody id="codeListAdd">								
									
								</tbody>
							</table>
						</form>
						
						
					</div>
					<!-- e_wrap_wrap -->

					<div class="bt_wrap">
						<button class="bt_2" onclick="insertCode();">simpan</button>
						<!-- <button class="bt_3">batal</button> -->
					</div>

				</div>
				<!-- e_adm_content2n -->
			</div>
			<!-- e_main_con -->

		</div>
		<!-- e_contents -->
	</div>
	<!-- e_container_table -->

</body>
</html>