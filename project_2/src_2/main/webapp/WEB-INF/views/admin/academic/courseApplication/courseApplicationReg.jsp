<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
    <head>
    	<script src="${HOME}/resources/smarteditor_v2.9/js/service/HuskyEZCreator.js"></script>
        <script>
			var editor_object = [];
        
	        $(document).ready(function(){	        	
	        	layerPopupCloseInit(['div.uoptions']);;	  
				
				nhn.husky.EZCreator.createInIFrame({
			       oAppRef: editor_object,
			       elPlaceHolder: "content",
	               sSkinURI : "${HOME}/resources/smarteditor_v2.9/SmartEditor2Skin_ko_KR.html"     ,
	               htParams : {
			           // 툴바 사용 여부 (true:사용/ false:사용하지 않음)
			           bUseToolbar : true,            
			           // 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
			           bUseVerticalResizer : true,    
			           // 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
			           bUseModeChanger : true
			       }
				});
									          	
	          	$(document).on("focusout", "input[name=req_charge]", function(){
	          		$(this).val(numberWithCommas($(this).val()));
	          	});	
	          	
	          	$(document).on("focusin", "input[name=req_charge]", function(){
	          		$(this).val($(this).val().replace(/(,)/g,""));
	          	});	
	          	
	         	//파일 registrasi
		        $(document).on("change", "input[name=file]", function(){
		        	var fileExt = $(this).val().split('.').pop().toLowerCase(); //마지막 . 위치로 확장자 자름			
					if($.inArray(fileExt, ["jpg","jpeg","gif","png","bmp","mp3","mp4","xls","xlsx","txt","hwp","pdf","zip"]) == -1) {
						alert("jpg,jpeg,gif,png,bmp,mp3,mp4,xls,xlsx,txt,hwp,pdf,zip 파일만 업로드 할수 있습니다.");
						$("#file"+fileIndex).remove();
						return;
					}
		 				
					var htmls = '';
					var fileValue = $(this).val().split("\\");
					var fileName = fileValue[fileValue.length-1]; // 파일명
					htmls += '<div class="wrap">';                                           
					htmls += '<span class="sp1">'+fileName+'</span>';    
		    		htmls += '<button class="btn_c" onClick="fileDelete('+fileIndex+', this);" title="삭제하기">X</button> ';
		    		htmls += '</div>';
		   			$("#fileListAdd").append(htmls); 
		        });		
	         	
	         	//신청비용 콤마 넣기
	         	if(!isEmpty("${currList.req_charge }"))
	         		$("input[name=req_charge]").val(numberWithCommas("${currList.req_charge }"));
	         	
	         	if(!isEmpty("${caList.ca_seq }"))
	         		$("button[name=saveBtn]").text("perbaiki");
	         			
	        });		   
	
	        function fileDelete(index, obj){
				$(obj).closest("div.wrap").remove();
				if(!isEmpty(index))
					$("#file"+index).remove();
			}
	        
	        function setGradeInput(flag){
	        	if(flag=="" || flag=="N"){
					$("input[name='grade']").hide();
                    $("input[name='grade']").val("");					        		
	        	}else{
					$("input[name='grade']").show();
                    $("input[name='grade']").val("");				
	        	}
	        }
	        
	        function acaSystemSet(level, seq){
	       		if(level==1){
	       			if(isEmpty(seq)){
	       				$("#m_seq_add span").remove();
	                    $("#m_seq").attr("data-value","");
	                    $("#m_seq").text("jurusan");
	       				$("#s_seq_add span").remove();
	                    $("#s_seq").attr("data-value","");
	                    $("#s_seq").text("tahun ajaran");
	       				$("#aca_seq").attr("data-value","");
	       				$("#aca_seq").text("");
	       				reset("aca");	
	       			}else if(seq != $("#l_seq").attr("data-value")){
	       				acasystemStageOfList(2, seq);	
	       				$("#m_seq").attr("data-value","");
	                    $("#m_seq").text("jurusan");
	       				$("#s_seq_add span").remove();
	                    $("#s_seq").attr("data-value","");
	                    $("#s_seq").text("tahun ajaran");
	       				$("#aca_seq").attr("data-value","");
	       				$("#aca_seq").text("");
	       				reset("aca");	
	       			}	       			
	       		}else if(level==2){
	       			if(isEmpty(seq)){
	       				$("#s_seq_add span").remove();
	                    $("#s_seq").attr("data-value","");
	                    $("#s_seq").text("tahun ajaran");
	       				$("#aca_seq").attr("data-value","");
	       				$("#aca_seq").text("");
	       				reset("aca");	       				
	       			}else if(seq != $("#m_seq").attr("data-value")){
		       			acasystemStageOfList(3, $("#l_seq").attr("data-value"), seq);
	                    $("#s_seq").attr("data-value","");
	                    $("#s_seq").text("tahun ajaran");
	       				$("#aca_seq").attr("data-value","");
	       				$("#aca_seq").text("");
	       				reset("aca");	       
	       			}	
	       		}else if(level==3){
	       			if(isEmpty(seq)){
	       				$("#acaListAdd").empty();
	       				$("#aca_seq").attr("data-value","");
	       				$("#aca_seq").text("");
	       				reset("aca");
	       			}
	       			else if(seq != $("#s_seq").attr("data-value")){
	       				getAcaList(seq);
	       				$("#aca_seq").attr("data-value","");
	       				$("#aca_seq").text("");
	       				reset("aca");
	       			}
	       		}
	       	}
	        
            function acasystemStageOfList(level, l_seq, m_seq, s_seq){
            	
            	if(level == ""){
            		alert("분류 없음");
            		return;
            	}
            	$.ajax({
            		type: "POST",
            		url: "${HOME}/ajax/admin/academic/academicSystem/acaStageOfList",
            		data: {
            			"level" : level,
            			"l_seq" : l_seq,                	   
            			"m_seq" : m_seq,
            			"s_seq" : s_seq
            			},
           			dataType: "json",
           			success: function(data, status) {
           				var cate = "";
           				if(level == 1)
           					cate = "fakultas";
           				else if(level == 2)
           					cate = "jurusan";
           				else if(level == 3)
           					cate = "tahun ajaran";
           				
           				
           				var htmls = '<span class="uoption" onClick="acaSystemSet('+level+',\'\');" data-value="">'+cate+'</span>';
           				$.each(data.list, function(index){
           					htmls +=  '<span class="uoption" onClick="acaSystemSet('+level+','+this.aca_system_seq+');" data-value="'+this.aca_system_seq+'">'+this.aca_system_name+'</span>';
           				});

           				if(level == 1){
           					$("#l_seq_add span").remove();
           					$("#l_seq_add").html(htmls);        
           				}	                   
           				else if(level == 2){
           					$("#m_seq_add span").remove();
           					$("#m_seq_add").html(htmls);         
           				}
           				else{
           					$("#s_seq_add span").remove();
           					$("#s_seq_add").html(htmls);
        				}
        			},
        			error: function(xhr, textStatus) {
        				//alert("오류가 발생했습니다.");
        				document.write(xhr.responseText);
       				},beforeSend:function() {
  					},
  					complete:function() {
    				}
     			}); 
		    }
		   		  
            //sistem akademik에 따른 학사 가져오기
			function getAcaList(seq){
            	if(isEmpty(seq)){
            		reset("aca");
            		return;
            	}
            		
            	$("#acaListAdd").empty();
            	
            	$.ajax({
            		type: "POST",
            		url: "${HOME}/ajax/admin/academic/courseApplication/acaList",
            		data: {
            			"aca_system_seq" : seq
            			},
           			dataType: "json",
           			success: function(data, status) {
           				if(data.status=="200"){
           					var htmls = "";
           					htmls += '<span class="uoption" onClick="getCurrList(\'\');" data-value=""></span>'
           					$.each(data.acaList, function(index){
           						htmls +='<span class="uoption" onClick="getCurrList('+this.aca_seq+');" data-value="'+this.aca_seq+'">'+this.academic_name+'</span>';
           					});
           					
           					$("#acaListAdd").html(htmls);
           				}
        			},
        			error: function(xhr, textStatus) {
        				//alert("오류가 발생했습니다.");
        				document.write(xhr.responseText);
       				},beforeSend:function() {
  					},
  					complete:function() {
    				}
     			}); 
		   }
            
			//학사로 교육과정 리스트 가져오기
			function getCurrList(seq){
				if(isEmpty(seq)){
        			reset("aca");
        			return;
				}else if(seq == $("#aca_seq").attr("data-value")){
            		return;
            	}
				
            	$.ajax({
            		type: "POST",
            		url: "${HOME}/ajax/admin/academic/courseApplication/currList",
            		data: {
            			"aca_seq" : seq
            			},
           			dataType: "json",
           			success: function(data, status) {
           				if(data.status=="200"){
           					var htmls = "";
           					htmls += '<span class="uoption" onClick="getCurrInfo(\'\');" data-value=""></span>'
           					$.each(data.currList, function(index){
           						htmls +='<span class="uoption" onClick="getCurrInfo('+this.curr_seq+');" data-value="'+this.curr_seq+'">'+this.curr_name+'</span>';
           					});
           					
           					$("#currListAdd").html(htmls);
           				}
        			},
        			error: function(xhr, textStatus) {
        				//alert("오류가 발생했습니다.");
        				document.write(xhr.responseText);
       				},beforeSend:function() {
  					},
  					complete:function() {
    				}
     			}); 
		  	}
			
			function getCurrInfo(curr_seq){
				if(isEmpty(curr_seq)){
        			reset("curr");
        			return;
				}else if(curr_seq == $("#curr_seq").attr("data-value")){
            		return;
            	}			
				
        		$.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/admin/academic/courseApplication/currInfo",
			        data: {
			        	"curr_seq" : curr_seq
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	
			        	if (data.status == "200") {
			        		var htmls="";
			        		var grade_name = "";
			        		if(data.currList.grade_code == "Y"){
			        			grade_name = "부여";
			        			$("input[name=req_charge]").show();
			        		}else if(data.currList.grade_code == "N"){
			        			grade_name = "미부여";
			        			$("input[name=req_charge]").hide();
			        		}
			        		
			        		$("span[data-name=complete_name]").text(data.currList.complete_name);
			        		$("span[data-name=grade]").text(data.currList.grade);
			        		$("span[data-name=administer_name]").text(data.currList.administer_name);
			        		$("input[name=req_charge]").val(numberWithCommas(data.currList.req_charge));
			        		$("input[name=curr_start_date]").val(data.currList.curr_start_date);
			        		$("input[name=curr_end_date]").val(data.currList.curr_end_date);
			        		$("input[name=curr_week]").val(data.currList.curr_week);
			        		$("input[name=period_cnt]").val(data.currList.period_cnt);
	                        
			        		$.each(data.mpfList,function(index){
			        			var user_name = "";
			        						        						        			
			        			if(isEmpty(this.department_name))
			        				user_name = this.name;
			        			else
			        				user_name = this.name + "("+this.department_name+")";
			        			
			        			htmls+='<span class="tts">'+user_name+'</span>';
			        		});

							$("#mpfListAdd").empty();
							$("#mpfListAdd").html(htmls);
	                        htmls="";
	                        
	                        //부Dosen Penanggung jawab
			        		$.each(data.pfList,function(index){
			        			var user_name = "";
			        			
			        			if(isEmpty(this.department_name))
			        				user_name = this.name;
			        			else
			        				user_name = this.name + "("+this.department_name+")";
			        			
			        			htmls+='<span class="tts">'+user_name+'</span>';
			        		});
	                        
							$("#pfListAdd").empty();
							$("#pfListAdd").html(htmls);
			        	} else {
			        		
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
        	}
			
			//simpan - perbaiki
            function curriculumSubmit(){
            	var l_seq = $("#l_seq").attr("data-value");
            	var m_seq = $("#m_seq").attr("data-value");
            	var s_seq = $("#s_seq").attr("data-value");
            	var aca_seq = $("#aca_seq").attr("data-value");
            	var curr_seq = $("#curr_seq").attr("data-value");
            	var l_seq = $("#l_seq").attr("data-value");

            	if(isEmpty(l_seq)){
            		alert("sistem akademik fakultas를 pilih해주세요.");
            		return;            		
            	}
            	
            	if(isEmpty(m_seq)){
            		alert("sistem akademik jurusan를 pilih해주세요.");
            		return;            		
            	}
            	
            	if(isEmpty(s_seq)){
            		alert("sistem akademik tahun ajaran를 pilih해주세요.");
            		return;            		
            	}
            	
            	if(isEmpty(aca_seq)){
            		alert("학사를 pilih해주세요");
            		return;            		
            	}
            	
            	if(isEmpty(curr_seq)){
            		alert("교육과정을 pilih해주세요");
            		return;  		
            	}
            	
            	if(isEmpty($("input[name=req_charge]").val())){
            		alert("신청 비용을 입력하세요");
            		return;
            	}

            	if(isEmpty($("input[name=accept_start_date]").val())){
            		alert("접수periode 시작일읍 입력하세요.");
            		return;
            	}

            	if(isEmpty($("input[name=accept_end_date]").val())){
            		alert("접수periode 종료일읍 입력하세요.");
            		return;
            	}
            	
            	if(isEmpty($("input[name=fixed_num]").val())){
            		alert("수강정원을 입력하세요");
            		return;
            	}
            	
            	$("input[name=req_charge]").val($("input[name=req_charge]").val().replace(/(,)/g,""));
            	
            	
    			editor_object.getById["content"].exec("UPDATE_CONTENTS_FIELD", []);
    			            	            	
				var form = document.getElementById("applicationForm");
				
				var aca_system_seq_input = document.createElement("input");
				
				aca_system_seq_input.type = "hidden";
				aca_system_seq_input.name = "aca_system_seq";
				aca_system_seq_input.value = s_seq;				
				form.appendChild(aca_system_seq_input);

				var curr_seq_input = document.createElement("input");

				curr_seq_input.type = "hidden";
				curr_seq_input.name = "curr_seq";
				curr_seq_input.value = curr_seq;				
				form.appendChild(curr_seq_input);

				var aca_seq_input = document.createElement("input");

				aca_seq_input.type = "hidden";
				aca_seq_input.name = "aca_seq";
				aca_seq_input.value = aca_seq;				
				form.appendChild(aca_seq_input);
				            	          
            	$("#applicationForm").ajaxForm({
                    type: "POST",
            		url: "${HOME}/ajax/admin/academic/courseApplication/insert",
                    dataType: "json",
                    success: function(data, status){
                        if (data.status == "200") {
                        	if(isEmpty($("#ca_seq").val())){
            					alert("simpan 완료하였습니다.");
            				}
            				else{
            					alert("perbaiki 완료하였습니다.");
            				}
                        	post_to_url('${HOME}/admin/academic/courseApplication/view', {'ca_seq': data.ca_seq});
                        } else {
                            alert(data.status);
                            $.unblockUI();                          
                        }
                        
                    },
                    error: function(xhr, textStatus) {
                        document.write(xhr.responseText); 
                        $.unblockUI();                      
                    },
        			uploadProgress: function(event, position, total, percentComplete) {
        			    $("#progressbar").width(percentComplete + '%');
        			    $("#statustxt").html(percentComplete + '%');
        			    if(percentComplete>50) {
        			    	$("#statustxt").css('color','#fff');
        			    }
        			},beforeSend:function() {
                        $.blockUI();                        
                    },complete:function() {
                        $.unblockUI();                      
                    }                       
                });     
    			$("#applicationForm").submit();  
    			
            } 
			
            var fileIndex = 0;
    		//파일 인풋 tambahkan해서 해당 인풋 클릭하게 만든다.
    		function fileAdd(){
                fileIndex++;
    			$("#fileInputAdd").append('<input type="file" name="file" id="file'+fileIndex+'" style="display:none;"/>');
    			$("#fileInputAdd input").last().click();
    		}
			
			function reset(type){
				if(type == "aca")
					$("#curr_seq").attr("data-value","").text("");
				
				$("input[name=req_charge]").val("");
				$("input[name=bank_name]").val("");
				$("input[name=account_num]").val("");
				$("input[name=account_holder]").val("");
				$("#popupMpfSubmit").empty();
				$("#popupPfSubmit").empty();
				$("input[name=accept_start_date]").val("");
				$("input[name=accept_end_date]").val("");
				$("input[name=application_num]").val("");
				$("input[name=curr_start_date]").val("");
				$("input[name=curr_end_date]").val("");
				$("input[name=curr_week]").val("");
				$("input[name=period_cnt]").val("");
				$("input[name=fixed_num]").val("");
			}
		</script>
	</head>

	<body>
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
			<!-- s_container_table -->
			<div id="container" class="container_table">
				<!-- s_contents -->
				<div class="contents main">
	
					<!-- s_left_mcon -->
					<div class="left_mcon aside_l grd">
						<div class="sub_menu adm_grd">
							<div class="title">manajemen akademik</div>
							<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">sistem management akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">학사코드manajemen</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on">menejemen kurikulum akadmik</span>
							</div>
							<ul class="panel on">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">menejemen kurikulum akadmik</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2 on">수강신청manajemen</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicReg/list'">management registration akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">Manajemen kompetensi kelulusan</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
						</div>
					</div>
					<!-- e_left_mcon -->
	
					<!-- s_main_con -->
					<div class="main_con adm_grd">
						<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
	
						<!-- s_tt_wrap -->
						<div class="tt_wrap">
							<h3 class="am_tt"><span class="tt">수강신청registrasi</span></h3>
						</div>
						<!-- e_tt_wrap -->
						<form id="applicationForm" onsubmit="return false;" enctype="multipart/form-data">
							<input type="hidden" name="ca_seq" value="${caList.ca_seq }">
								<!-- s_adm_content2 -->
						<div class="adm_content2">
							<!-- s_tt_wrap -->
		
							<!-- s_wrap_wrap -->
							<div class="wrap_wrap">
								<div class="btnwrap_s1f">   
								     <button class="ic_v3" onclick="location.href='${HOME}/admin/academic/courseApplication/list'">수강신청 목록</button>
								</div>
								<!-- s_sch_wrap -->
								<div class="sch_wrap regi enroll">
		
									<!-- s_rwrap -->
									<div class="rwrap frwrap">
		
										<!-- s_wrap_s -->
										<div class="wrap_s a1">
											<span class="tt2">sistem akademik</span>
											<div class="wrap_fss">
												<div class="wrap_s1_uselectbox">
													<div class="uselectbox">
														<span class="uselected" id="l_seq" data-value="${caList.l_seq }">${caList.laca_system_name }</span> <span class="uarrow">▼</span>
														<div class="uoptions" style="display: none;" id="l_seq_add">
															<span class="uoption" data-value="">fakultas</span>
															<c:forEach var="lseqList" items="${lseqList}">
		                                                        <span class="uoption" data-value="${lseqList.aca_system_seq}" onClick="acaSystemSet(1,'${lseqList.aca_system_seq}');">${lseqList.aca_system_name}</span>
		                                                    </c:forEach>
														</div>
													</div>
												</div>
		
												<div class="wrap_s1_uselectbox">
													<div class="uselectbox">
														<span class="uselected" id="m_seq" data-value="${caList.m_seq }">${caList.maca_system_name }</span> <span class="uarrow">▼</span>
														<div class="uoptions" style="display: none;" id="m_seq_add">
															<span class="uoption" data-value="">jurusan</span>
															<c:forEach var="mseqList" items="${mseqList}">
		                                                        <span class="uoption" data-value="${mseqList.aca_system_seq}" onClick="acaSystemSet(2,'${mseqList.aca_system_seq}');">${mseqList.aca_system_name}</span>
		                                                    </c:forEach>
														</div>
													</div>
												</div>
		
												<div class="wrap_s1_uselectbox">
													<div class="uselectbox">
														<span class="uselected" id="s_seq" data-value="${caList.s_seq }">${caList.saca_system_name }</span> <span class="uarrow">▼</span>
														<div class="uoptions" style="display: none;" id="s_seq_add">
															<span class="uoption" data-value="">tahun ajaran</span>
															<c:forEach var="sseqList" items="${sseqList}">
		                                                        <span class="uoption" data-value="${sseqList.aca_system_seq}" onClick="acaSystemSet(3,'${sseqList.aca_system_seq}');">${sseqList.aca_system_name}</span>
		                                                    </c:forEach>	
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- e_wrap_s -->
		
										<!-- s_wrap_s -->
										<div class="wrap_s a1">
		
											<span class="tt2">gelar akademik</span>
		
											<div class="wrap_fss">
												<div class="wrap_s1a_uselectbox">
													<div class="uselectbox">
														<span class="uselected" id="aca_seq" data-value="${caList.aca_seq }">${caList.academic_name  }</span> <span class="uarrow"></span>
														<div class="uoptions" style="display: none;" id="acaListAdd">
															<c:forEach var="acaSelectList" items="${acaSelectList}">
		                                                        <span class="uoption" data-value="${acaSelectList.aca_seq}" >${acaSelectList.academic_name }</span>
		                                                    </c:forEach>	
														</div>
													</div>
												</div>
											</div>
		
										</div>
										<!-- e_wrap_s -->
		
									</div>
									<!-- e_rwrap -->
		
									<!-- s_rwrap -->
									<div class="rwrap frwrap a2">
		
										<!-- s_wrap_s -->
										<div class="wrap_s a1">
		
											<span class="tt2">Nama Mata Kuliah</span>
		
											<div class="wrap_fss">
												<div class="wrap_s1a_uselectbox">
													<div class="uselectbox">
														<span class="uselected" id="curr_seq" data-value="${caList.curr_seq }">${caList.curr_name }</span><span class="uarrow"></span>
														<div class="uoptions" style="display: none;" id="currListAdd">
															<c:forEach var="currSelectList" items="${currSelectList}">
		                                                        <span class="uoption" data-value="${currSelectList.curr_seq}" >${currSelectList.curr_name}</span>
		                                                    </c:forEach>	
														</div>
													</div>
												</div>
											</div>
		
										</div>
										<!-- e_wrap_s -->
		
										<!-- s_wrap_s -->
										<div class="wrap_s a1">
											<span class="tt2">kategori penuntasan</span>
											<div class="wrap_fss1">
												<div class="ttswrap">
													<span class="tts" data-name="complete_name">${currList.complete_name }</span>
												</div>
											</div>
											<span class="tt2 bd1">nilai</span>
											<div class="wrap_fss1">
												<div class="ttswrap">
													<span class="tts" data-name="grade">${currList.grade }</span>
												</div>
											</div>
										</div>
										<!-- e_wrap_s -->
										<!-- s_wrap_s -->
										<div class="wrap_s a1">
											<span class="tt2">manajemen Klasifikasi</span>
											<div class="wrap_fss1">
												<div class="ttswrap">
													<span class="tts" data-name="administer_name">${currList.administer_name }</span>
												</div>
											</div>
		
											<span class="tt2 bd1">신청비용</span>
											<div class="wrap_fss1">
												<input class="ip_tt tt5" placeholder="금액 숫자만 입력" type="text" onkeyup="onlyNumCheck(this);" name="req_charge" style="text-align:right;padding-right:5px;" value="">
												<span class="unit">원</span>
											</div>
										</div>
										<!-- e_wrap_s -->
										<!-- s_wrap_s -->
										<div class="wrap_s a1">
											<span class="tt2">계좌안내</span>
											<div class="wrap_fss5">
												<input class="ip_tt tt6 s1" name="bank_name" placeholder="은행명" type="text" value="${caList.bank_name }"> 
												<input class="ip_tt tt6 s2" name="account_num" placeholder="계좌번호" type="text" value="${caList.account_num }"> 
												<input class="ip_tt tt6 s3" name="account_holder" placeholder="예금주" type="text" value="${caList.account_holder }">
											</div>
										</div>
										<!-- e_wrap_s -->
									</div>
									<!-- e_rwrap -->
		
									<!-- s_rwrap -->
									<div class="rwrap frwrap a2">
		
										<!-- s_wrap_s -->
										<div class="wrap_s a1">
											<span class="tt2_1">Dosen Penanggung jawab</span>
											<div class="wrap_fss">
												<div class="ttswrap" id="mpfListAdd">
													<c:forEach var="mpfList" items="${mpfList}" step="1" varStatus="status">
						                                <div class="a_mp">															
															<span class="tts">${mpfList.name } (${mpfList.department_name })</span>
														</div>
													</c:forEach> 
												</div>
											</div>
										</div>
										<!-- e_wrap_s -->
									</div>
									<!-- e_rwrap -->
		
									<!-- s_rwrap -->
									<div class="rwrap frwrap a2">
		
										<!-- s_wrap_s -->
										<div class="wrap_s a1">
											<span class="tt2_1">부Dosen Penanggung jawab</span>
											<div class="wrap_fss">
												<div class="ttswrap" id="pfListAdd">
													<c:forEach var="mpfList" items="${pfList}" step="1" varStatus="status">
						                                <div class="a_mp">															
															<span class="tts">${pfList.name } (${pfList.department_name })</span>
														</div>
													</c:forEach> 
												</div>
											</div>
										</div>
										<!-- e_wrap_s -->
									</div>
									<!-- e_rwrap -->
		
									<!-- s_rwrap -->
									<div class="rwrap frwrap a2">
				
										<!-- s_wrap_s -->
										<div class="wrap_s">
											<span class="tt2">접수periode</span>
											<div class="wrap_fss3 s1">
												<input type="text" name="accept_start_date" class="dateyearpicker-input_1 ip_date" placeholder="시작일" value="${caList.accept_start_date }"> 
												<span class="tt">~</span> 
												<input type="text" name="accept_end_date" class="dateyearpicker-input_1 ip_date" placeholder="종료일" value="${caList.accept_end_date }">
											</div>
											<span class="tt2 bd1">접수결과발표</span>
											<div class="wrap_fss3 s2">
												<div class="ttswrap">
													<input type="text" name="a​nnouncement_date" class="dateyearpicker-input_1 ip_date" style="width:91%;" placeholder="시작일" value="${caList['a​nnouncement_date'] }">
												</div>
											</div>
										</div>
										<!-- e_wrap_s -->
										<!-- s_wrap_s -->
										<div class="wrap_s">
											<span class="tt2">수강periode</span>
											<div class="wrap_fss3 s1">
												<input class="dateyearpicker-input_1 ip_date s1 dis" name="curr_start_date" value="${currList.curr_start_date }" type="text" disabled> 
												<span class="tt">~</span> 
												<input class="dateyearpicker-input_1 ip_date s1 dis" name="curr_end_date" value="${currList.curr_end_date }" type="text" disabled> 
												<input class="ip_tt tt7 s3 dis" name="curr_week" value="${currList.curr_week }" type="text" disabled>
												<span class="unit s1">주</span> 
												<input class="ip_tt tt7 s3 dis" name="period_cnt" value="${currList.period_cnt }" type="text" disabled> 
												<span class="unit s1">waktu</span>
											</div>
											<span class="tt2 bd1">수강정원</span>
											<div class="wrap_fss3 s2">
												<div class="ttswrap">
													<input class="ip_tt tt7 s5 s2" name="fixed_num" style="min-width:90px;width:75%;" value="${caList.fixed_num }" type="text"> 
													<span class="unit s1">명</span>
												</div>
											</div>
										</div>
										<!-- e_wrap_s -->
									</div>
									<!-- e_rwrap -->
		
									<!-- s_rwrap -->
									<div class="rwrap frwrap a2">			
										<!-- s_wrap_s -->
										<div class="wrap_s a1 vpanel">
											<span class="tt2">과목개요</span>
											<div class="tarea free_textarea">
												<textarea class="tarea01" style="height: 100px;" id="content" name="content">${caList.content }</textarea>
											</div>
										</div>
										<!-- e_wrap_s -->
		
										<!-- s_wrap_s -->
										<div class="wrap_s a1 vpanel">
											<span class="tt2">파일첨부</span>
											<div class="wrap_fss">
												<input class="ip_pt2" value="파일을 첨부해주세요." type="text">
												<button class="btn1" onClick="fileAdd();">pilih</button>
											</div>
											<div id="fileInputAdd" style="display:none;">
											</div>
										</div>
										<!-- e_wrap_s -->
		
										<!-- s_wrap_s -->
										<div class="wrap_s a1 vpanel">
											<div class="f_add1" id="fileListAdd">
												<c:forEach var="attachList" items="${attachList}">
													<div class="wrap">      
														<input type="hidden" value="${attachList.caa_seq}" name="caa_seq"/>                                     
														<span class="sp1" onclick="fileDownload('${HOME}', '${RES_PATH}${attachList.file_path }', '', '${attachList.file_name }');">${attachList.file_name }</span>    
											    		<button class="btn_c" onClick="fileDelete('', this);" title="삭제하기">X</button>
										    		</div>
                                                </c:forEach>
											</div>
										</div>
										<!-- e_wrap_s -->
		
									</div>
									<!-- e_rwrap -->
		
								</div>
								<!-- e_sch_wrap -->
		
							</div>
							<!-- e_wrap_wrap -->
		
							<div class="bt_wrap">
								<button class="bt_2" name="saveBtn" onclick="curriculumSubmit();">registrasi</button>
								<button class="bt_3" onClick="if(confirm('목록 화면 으로 돌아갑니다.')) location.href='${HOME}/admin/academic/courseApplication/list'">batal</button>
							</div>
		
						</div>
						<!-- e_adm_content2 -->
						</form>
					</div>
					<!-- e_main_con -->	
				</div>
				<!-- e_contents -->
			</div>
			<!-- e_container_table -->	
	</body>
</html>