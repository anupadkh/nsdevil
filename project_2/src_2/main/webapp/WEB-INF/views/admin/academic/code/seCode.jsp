<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<script>
	$(document).ready(function() {
		$(".mdf_wrap_b").hide();
	    $(document).on("click",".ip01a", function(){
	        $(this).parent("div.mdf_wrap_a").hide();
	        $(this).parent().parent().children("div.mdf_wrap_b").show();
	        $(this).closest("td").find("input.ip01b").focus();
	    });
	    
		$(document).on("click", ".btn_x", function(){
			var type = $(this).attr("name");
			if(type == "L"){
				if(confirm("상위 코드 삭제시 하위코드도 삭제됩니다.\n삭제하시겠습니까?")){
					deleteCode($(this).closest("tbody").attr("name"), "", "L");
				}
			}else{
				if(confirm("삭제하시겠습니까?")){				
					deleteCode($(this).closest("tbody").attr("name"), $(this).siblings("input[name=se_code]").val(), "S");
				}	  
			}
	    });
		
		$(document).on("click", ".se_add", function(){
			var htmls = '';
			var tr_cnt = $(this).closest("tbody").find("tr").length;
			var rowSpan = parseInt($(this).closest("tbody").find("tr:first").find("td:first").attr("rowspan"));
			
			$(this).closest("tbody").find("tr:first").find("td:first").attr("rowspan",rowSpan+1);
			
			if(tr_cnt == 1){
								
				htmls = '<tr name="add">'
				+'<td class="td_1">'
    			+'<button class="btn_add se_add" title="tambahkan"></button>'
    			+'</td></tr>';    			
				$(this).closest("tbody").append(htmls);
				
				htmls = '<div class="mdf_wrap_b" style="">'
				+'<input type="hidden" name="se_code" value="">'
				+'<input type="text" class="ip01b" name="se_name" value="">'
				+'<button class="btn_x" name="S" title="삭제하기">X</button>'
				+'</div>';
				$(this).closest("td").empty().html(htmls);
			}else{
				htmls += '<tr>'
					+'<td class="td_1">'
					+'<div class="mdf_wrap_b" style="">'
					+'<input type="hidden" name="se_code" value="">'
					+'<input type="text" class="ip01b" name="se_name" value="">'
					+'<button class="btn_x" name="S" title="삭제하기">X</button>'
					+'</div>'
					+'</td>'
					+'</tr>';
				$(this).closest("tbody").find("tr").last().before(htmls);
			}			
			
		});
		
		$(document).on("click", ".l_se_add", function(){
			var htmls = '';
			htmls += '<tbody name=""><tr name="add">'
	    	+'<td class="td_1">'
	    	+'<div class="mdf_wrap_b">'         
	    	+'<input type="hidden" name="l_se_code" value="">'   
	    	+'<input type="text" class="ip01b" name="l_se_name" value="">'				    	
	    	+'<button class="btn_x" name="L" title="삭제하기">X</button>'
	    	+'</div>'					
	    	+'</td>'
	    	+'<td class="td_1">'
	    	+'</td>'
	    	+'</tr></tbody>';
			$("#codeListAdd").find("tbody:last").before(htmls);			
		});  
		
		getCode();
	});

	function deleteCode(l_se_code, se_code, type){
		$.ajax({
			type : "POST",
			url : "${HOME}/ajax/admin/academic/codeManagement/deleteSeCode",
			data : {
				"l_se_code" : l_se_code
				,"se_code" : se_code
				,"type" : type
			},
			dataType : "json",
			success : function(data, status) {
				if(data.status == "200"){
					alert("삭제 완료 하였습니다.");
					getCode();
				}else if(data.status == "201"){
					alert("해당 코드는 또는 하위 코드가 사용중입니다.\n삭제할 수 없습니다.");
				}else if(data.status == "202"){
					alert("해당 코드는 사용중입니다.\n삭제할 수 없습니다.");
				}else{
					alert("삭제 실패하였습니다.");
				}
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
				$.unblockUI();
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}
	
	function getCode() {
		$.ajax({
			type : "POST",
			url : "${HOME}/ajax/admin/academic/codeManagement/seCodeList",
			data : {
			},
			dataType : "json",
			success : function(data, status) {
				var htmls = "";

				var pre_se_code = "";
				
				var se_index = 0;

				if(data.status == "200"){
					
				$("#codeListAdd tbody").remove();
					$.each(data.seCodeList, function(index){
	
						if(pre_se_code != this.l_se_code){
							htmls += '<tbody name="'+this.l_se_code+'"><tr name="">'
					    	+'<td rowspan="'+(this.row_cnt+1)+'" class="td_1">'				
					    	+'<div class="mdf_wrap_a">'         
					    	+'<input type="text" class="ip01a" value="'+this.l_se_name+'">'
					    	+'</div>'
					    	+'<div class="mdf_wrap_b" style="display:none;">'
					    	+'<input type="hidden" name="l_se_code" value="'+this.l_se_code+'">'
					    	+'<input type="text" class="ip01b" name="l_se_name" value="'+this.l_se_name+'">'				    	
					    	+'<button class="btn_x" name="L" title="삭제하기">X</button>'
					    	+'</div>'					
					    	+'</td>'
					    	+'<td class="td_1">';
					    	if(isEmpty(this.se_code)){
					    		htmls+='<button class="btn_add se_add" title="tambahkan"></button>';
					    	}else{
					    		htmls+='<div class="mdf_wrap_a">'         
						    	+'<input type="text" class="ip01a" value="'+this.se_name+'">'
						    	+'</div>'
						    	+'<div class="mdf_wrap_b" style="display:none;">'   
						    	+'<input type="hidden" name="se_code" value="'+this.se_code+'">'      
						    	+'<input type="text" class="ip01b" name="se_name" value="'+this.se_name+'">'
						    	+'<button class="btn_x" name="S" title="삭제하기">X</button>'
						    	+'</div>';	
					    	}				    	
					    	htmls +='</td>'
					    	+'</tr>';
					    	se_index = 1;
						}else{					
							htmls += '<tr>'
							    +'<td class="td_1">'
							    +'<div class="mdf_wrap_a">'      
							    +'<input type="text" class="ip01a" value="'+this.se_name+'">'
							    +'</div>'
							    +'<div class="mdf_wrap_b" style="display:none;">'      
						    	+'<input type="hidden" name="se_code" value="'+this.se_code+'">'   
							    +'<input type="text" class="ip01b" name="se_name" value="'+this.se_name+'">'
							    +'<button class="btn_x" name="S" title="삭제하기">X</button>'
							    +'</div>'
							    +'</td>'
							    +'</tr>';
							se_index++;
						}
	
						if(se_index==this.row_cnt && this.row_cnt!=0){
							htmls += '<tr name="add">'
								+'<td class="td_1">'
				    			+'<button class="btn_add se_add" title="tambahkan"></button>'
				    			+'</td></tr></tbody>';						
						}
						
						pre_se_code=this.l_se_code;
						
					});
					
					htmls += '<tbody name="add"><tr>'
						+'<td class="td_1" >'
		    			+'<button class="btn_add l_se_add" title="tambahkan"></button>'
	                    +'</td>'
	                    +'<td class="td_1" >'
	                    +'</td>'
	                    +'</tr></tbody>';
	                    
					$("#codeListAdd").append(htmls);						
				}
								
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
				$.unblockUI();
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}
	
	function insertCode(){
		if(!confirm("simpan하시겠습니까?")){
			return;
		}

		var lseArray = new Array();
		var seArray = new Array();
		
		$.each($("#codeListAdd tbody"), function(index){
			var l_se_code = $(this).attr("name");
			if(l_se_code == "add")
				return;
			
			var l_listInfo = new Object();
			l_listInfo.l_se_code = l_se_code;
			l_listInfo.l_se_name = $(this).find("input[name=l_se_name]").val();				
			
			$.each($(this).find("tr"), function(s_index){
				//tambahkan 행인거 제외
				if($(this).attr("name") == "add")
					return;

				//각 상위 tbody 의 첫행을 삭제할 때 tr 인거 제외
				if($(this).find("input[name=se_name]").length == 0)
					return;
				
				var listInfo = new Object();
				listInfo.se_code = $(this).find("input[name=se_code]").val();
				listInfo.se_name = $(this).find("input[name=se_name]").val();
				listInfo.l_se_code = l_se_code;
				
				seArray.push(listInfo);
			});
			lseArray.push(l_listInfo);
		});
				
		var list = new Object();
		list.l_se_list = lseArray;
		list.se_list = seArray;
		
		var jsonInfo = JSON.stringify(list);
				
		$.ajax({
			type : "POST",
			url : "${HOME}/ajax/admin/academic/codeManagement/seCodeInsert",
			data : {
				"list" : jsonInfo
			},
			dataType : "json",
			success : function(data, status) {
				if(data.status == "200"){
					alert("simpan이 완료되었습니다.");
					getCode();
				}
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
				$.unblockUI();
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
		
	}
</script>
</head>

<body>
	<!-- s_container_table -->
	<div id="container" class="container_table">
		<!-- s_contents -->
		<div class="contents main">

			<!-- s_left_mcon -->
			<div class="left_mcon aside_l grd">
				<div class="sub_menu adm_grd">
					<div class="title">manajemen akademik</div>

					<!-- s_학사코드 메뉴 -->
					<div class="boardwrap">
						<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">sistem management akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on">학사코드manajemen</span>
							</div>
							<ul class="panel on">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2 on">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">menejemen kurikulum akadmik</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">menejemen kurikulum akadmik</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청manajemen</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicReg/list'">management registration akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">Manajemen kompetensi kelulusan</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
					</div>
					<!-- e_학사코드 메뉴 -->

				</div>
			</div>
			<!-- e_left_mcon -->

			<!-- s_main_con -->
			<div class="main_con adm_grd">
				<!-- s_메뉴 접기 버튼 -->
				<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
				<!-- e_메뉴 접기 버튼 -->

				<!-- s_tt_wrap -->
				<div class="tt_wrap">
					<h3 class="am_tt">
						<span class="tt">학사코드 manajemen - 총괄penilaian기준</span>
					</h3>
				</div>
				<!-- e_tt_wrap -->

				<!-- s_adm_content2n -->
				<div class="adm_content2n">
					<!-- s_tt_wrap -->

					<!-- s_wrap_wrap -->
					<div class="wrap_wrap">

						<!-- s_wrap_s -->
						<div class="wrap_s">
							<div class="wrap_s1_uselectbox">
								<div class="uselectbox">
									<span class="uselected">총괄penilaian기준</span> <span class="uarrow">▼</span>
									<div class="uoptions" style="display: none;">
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/completeCode'">kategori penuntasan</span>
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/administerCode'">manajemen Klasifikasi</span>
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/lessonMethodCode'">tidak ada perkuliahan유형</span>
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/feCode'">형성penilaian</span>
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/gradeCode'">nilai기준</span>
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/seCode'">총괄penilaian기준</span>
										<span class="uoption" onclick="location.href='${HOME}/admin/academic/codeManagement/unitCode'">수업 Area/tingkat</span>
									</div>
								</div>
							</div>
							<button class="btn_tt1">Pencarian</button>

						</div>
						<!-- e_wrap_s -->


						<form id="codeForm"  onSubmit="return false;">
							<table class="mlms_tb s1" id="codeListAdd">
			                    <thead>
			                        <tr>
			                            <th colspan="2" class="th01 b_2 w0">총괄penilaian기준</th>
			                        </tr>
			                        <tr>
			                            <th class="th01 b_2 w1">Metode evaluasi</th>
			                            <th class="th01 b_2 w1">penilaianArea</th>
			                        </tr>
			                    </thead> 
			                    


			                    	
               				</table>
						</form>
						
						
					</div>
					<!-- e_wrap_wrap -->

					<div class="bt_wrap">
						<button class="bt_2" onclick="insertCode();">simpan</button>
						<!-- <button class="bt_3">batal</button> -->
					</div>

				</div>
				<!-- e_adm_content2n -->
			</div>
			<!-- e_main_con -->

		</div>
		<!-- e_contents -->
	</div>
	<!-- e_container_table -->

</body>
</html>