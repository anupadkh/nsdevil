<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<script>
			$(document).ready(function() {
				bindPopupEvent("#m_pop1", ".open1");
				bindPopupEvent("#m_pop2", ".open2");
				
				getAcademicTitle();
				getGrade();
			});
			
			function getAcademicTitle(){
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicReg/titleInfo",
		            data: {                      
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		if(!isEmpty(data.acaInfo.academic_name))
		                		$("#academicNameTitle").text(data.acaInfo.academic_name);
		                	else
		                		$("#academicNameTitle").hide();
		            	
		            		var aca_state = "";
		                	switch(data.acaInfo.aca_state){
	                            case "01" : aca_state = "개설(비공개)";break;
	                            case "02" : aca_state = "개설(공개)";break;
	                            case "03" : aca_state = "학사 중(수업 중)";break;
	                            case "04" : aca_state = "학사종료(미확정)";break;
	                            case "05" : aca_state = "학사종료(성적산정)";break;
	                            case "06" : aca_state = "학사종료(성적발표)";break;
	                            case "07" : aca_state = "성적발표 종료(미확정)";break;
	                            case "08" : aca_state = "학사최종종료(확정)";break;                            
	                        }
			            	$("#academicState").text(aca_state); //타이틀 status akademik
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
			
			function getGrade(){
				var curr_seq = $("#curr_seq").attr("data-value");
				var reg_yn = "";
				
				//성적 미registrasi pilih했을 경우
				if(curr_seq=="N"){
					reg_yn="N";
					curr_seq="";
				}
				
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicReg/soosiScore/list",
		            data: {   
		            	"curr_seq" : curr_seq
		            	,"reg_yn" : reg_yn		            	
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		var htmls = "";
								
							$("span[data-name=totalCurr]").text(data.currCntInfo.curr_cnt);
							$("span[data-name=submitCurr]").text(data.currCntInfo.reg_cnt);
							$("span[data-name=nonCurr]").text(data.currCntInfo.noreg_cnt);
								
		            		
		            		$("#stListAdd").empty();
		            		$("#currListAdd").empty();
		            		$("#scoreListAdd").empty();
		            		$.each(data.stList, function(index){
		            			htmls+='<tr name="'+this.id+'">'
								+'<td class="w2"><input type="hidden" name="st_user_seq" value="'+this.user_seq+'"><a href="javascript:openStGradePopup('+this.user_seq+',\''+this.id+'\',\''+this.name+'\');" class="go_grd open2" title="해당 학생의 성적표 보기">'+this.id+'</a></td>'
								+'<td class="w3"><a href="javascript:openStGradePopup('+this.user_seq+',\''+this.id+'\',\''+this.name+'\');" class="go_grd open2" title="해당 학생의 성적표 보기">'+this.name+'</a></td>'
								+'<td class="w1_1 color1"><span class="ts"></span></td>'
								+'<td class="w1_2 color1"><span class="ts"></span></td>'
								+'</tr>';
		            		});
		            		$("#stListAdd").html(htmls);
		            		
		            		$.each(data.stGradeInfo, function(index){
		            			$("tr[name="+this.id+"] span.ts:eq(0)").text(this.score);
		            			$("tr[name="+this.id+"] span.ts:eq(1)").text(this.rank);
		            		});
		            		
		            		
		            		var currCntHtml = "";
		            		var currInfoHtml = "";
		            		var currPeriodHtml = "";
		            		var currCreateHtml = "";
		            		var scoreState = "";
		            		var scoreClass = "";
		            		$.each(data.currList, function(index){
		            			if(index==0){
		            				currCntHtml+="<tr>";
		            				currInfoHtml+= "<tr>";
				            		currPeriodHtml+= "<tr>";
				            		currCreateHtml+= "<tr>";
		            			}
		            			
		            			if(this.soosi_score_cnt==0){
		            				scoreState = "registrasi";
		            				scoreClass = "regi open1";
		            			}else{
		            				scoreState = "성적perbaiki";
		            				scoreClass = "mdf";
		            			}
		            			
		            			currCntHtml+='<td class="th01 w1 bd01"><span class="ts">'+(index+1)+'</span></td>';
		            			currInfoHtml+='<td class="th01 title"><a href="javascript:pageMove('+this.curr_seq+',\''+this.curr_name+'\');" class="go_grd" title="종합성적 상세 보기">'+this.curr_name+'</a></td>';
		            			currPeriodHtml+='<td class="th01"><span class="ts">'+this.soosi_score_cnt+'회</span></td>';
		            			
		            			 if(this.soosi_score_cnt==0)
		            				currCreateHtml+='<td class="th01"><button class="regi open1" onClick="popup('+this.curr_seq+',\''+this.curr_name+'\');">registrasi</button></td>';
	            				else
	            					currCreateHtml+='<td class="th01"><button class="mdf" onclick="javascript:pageMove('+this.curr_seq+',\''+this.curr_name+'\');">성적perbaiki</button></td>';
		            			
		            			if(data.currList.length == (index+1)){
		            				currCntHtml+="</tr>";
		            				currInfoHtml+= "</tr>";
				            		currPeriodHtml+= "</tr>";
				            		currCreateHtml+= "</tr>";
		            			}
		            		});
		            		
		            		$("#currListAdd").html(currCntHtml+currInfoHtml+currPeriodHtml+currCreateHtml);
		            		
		            		htmls='';
		            		$.each(data.stList, function(stindex){
		            			var id= this.id;
		            			$.each(data.currList, function(currindex){
		            				if(currindex==0)
		            					htmls='<tr>';
		            					
		            				if(this.score_cnt==0)
		            					htmls+='<td class="td_1"><span class="ts" data-name="'+id+'_'+this.curr_seq+'"></span></td>';
	            					else
	            						htmls+='<td class="td_1"><span class="ts" data-name="'+id+'_'+this.curr_seq+'">해당 없음</span></td>';
	            						
		            				if(data.currList.length == (currindex+1))
										htmls+='</tr>';
		            			});
		            			$("#scoreListAdd").append(htmls);
		            		});
		            		
		            		$.each(data.scoreList, function(index){
		            			$("span[data-name="+this.id+"_"+this.curr_seq+"]").text(this.score+" / "+this.question_cnt);
		            		});
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
			
			function gradeExcelDown(){
				var gradeArray = new Array();
				var currArray = new Array();
				
				$.each($("#stListAdd tr"), function(index){
					var listInfo = new Object();
					listInfo.id = $(this).find("td").eq(0).text();
					listInfo.name = $(this).find("td").eq(1).text();
					listInfo.grInfo = $(this).find("td").eq(2).text();
					listInfo.rank = $(this).find("td").eq(3).text();
					var score = "";
					
					//학생꺼 점수들 가져온다.
					$.each($("#scoreListAdd tr:eq("+index+") td"), function(sIndex){
						score += $(this).text();
						
						//마지막 데이터 빼고 구분자 넣어준다.
						if($("#scoreListAdd tr:eq("+index+") td").length != (sIndex+1))
							score += "|";
					});
					listInfo.grade = score;
					gradeArray.push(listInfo);							
				});

				var no = "";
				var curr_name = "";
				var srcCnt = "";
				$.each($("#currListAdd tr:eq(0) td"), function(index){
					no += $(this).text();
					
					if($("#currListAdd tr:eq(0) td").length != (index+1))
						no += "|";
				});
				
				$.each($("#currListAdd tr:eq(1) td"), function(index){
					curr_name += $(this).text();
					
					if($("#currListAdd tr:eq(1) td").length != (index+1))
						curr_name += "|";
				});
				
				$.each($("#currListAdd tr:eq(2) td"), function(index){
					srcCnt += $(this).text();
					
					if($("#currListAdd tr:eq(2) td").length != (index+1))
						srcCnt += "|";
				});
				
				var scoreList = new Object();
				scoreList.list = gradeArray;
				
				var jsonInfo = JSON.stringify(scoreList);
				
				post_to_url("${HOME}/admin/academic/academicReg/soosiScore/excelDown", {"stList":jsonInfo, "no":no, "curr_name" : curr_name, "srcCnt":srcCnt});				
				
			}
			
			function pageMove(curr_seq, curr_name){
				post_to_url("${HOME}/admin/academic/academicReg/soosiScore/create", {"curr_seq":curr_seq, "curr_name":curr_name});
			}
			
			function pageMoveScore(type, aca_seq){
		    	$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/admin/academic/academicReg/setAcaSeq",
	                data: {
	                	"aca_seq" : aca_seq
	                    },
	                dataType: "json",
	                success: function(data, status) {
	                	if(type=="curr")
	               			location.href="${HOME}/admin/academic/academicReg/currScore";
		     			else if(type=="soosi")
		     				location.href="${HOME}/admin/academic/academicReg/soosiScore";
		     			else if(type=="fe")
		     				location.href="${HOME}/admin/academic/academicReg/feScore";
	                },
	                error: function(xhr, textStatus) {
	                    alert("실패");
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                },beforeSend:function() {
	                	$.blockUI();
	                },
	                complete:function() {
	                	$.unblockUI();
	                }
	            });
		    }
			
			function popup(curr_seq, curr_name){
				$("#pop_curr_seq").attr("data-value",curr_seq).text(curr_name);
				
			}
			
			function file_nameChange(){    
			    if($("#xlsFile").val() != ""){
			        var fileValue = $("#xlsFile").val().split("\\");
			        var fileName = fileValue[fileValue.length-1]; // 파일명
			        
			        var fileLen = fileName.length; 
			        var lastDot = fileName.lastIndexOf('.'); //마지막 . 위치 가져온다.
			        var fileExt = fileName.substring(lastDot+1, fileLen).toLowerCase(); //마지막 . 위치로 확장자 자름

			        if(fileExt != "xlsx") {
			        	alert("xlsx 파일을 registrasi해주세요");
			        	$("#xlsFile").val("");
			        	return;			        	
			        }
			        
			        $("#xls_filename").val(fileName);
			    }
			}
			
			
			function excelSubmit() {
				if(isEmpty($("#pop_curr_seq").attr("data-value"))){
					alert("성적 registrasi할 교육과정을 pilih해주세요");
					return;
				}

				if (isEmpty($("#xlsFile").val())) {
					alert("파일을 pilih해주세요");
					return;
				}
				var form = document.getElementById("xlsForm");
				
				var hiddenField = document.createElement("input");
				
				hiddenField.type = "hidden";
				hiddenField.name = "curr_seq";
				hiddenField.value = $("#pop_curr_seq").attr("data-value");
				
				form.appendChild(hiddenField);
				
				$("#xlsForm").ajaxForm({
					type : "POST",
					url : "${HOME}/ajax/admin/academicReg/soosiScore/excel/create",
					dataType : "json",
					success : function(data, status) {
						if (data.status == "200") {
							alert("엑셀업로드가 완료 되었습니다.");
							$("#xlsFile").val("");
							$("#xls_filename").val("");
							getGrade();
						} else {
							alert(data.status);
						}
					},
					error : function(xhr, textStatus) {
						alert(textStatus);
						//document.write(xhr.responseText);
						$.unblockUI();
					},
					beforeSend : function() {
						$.blockUI();
					},
					complete : function() {
						$.unblockUI();
					}
				});
				$("#xlsForm").submit();
			}
			
			function getAcademic(){
		    	post_to_url('${HOME}/admin/academic/academicReg/academicRegBasicInfo/create', {"aca_seq" : "${S_ACA_SEQ}"});
		    }
			
			function excelTmplDown(){
				if(isEmpty($("#pop_curr_seq").attr("data-value"))){
					alert("양식다운로드할 교육과정을 pilih하세요");
					return;
				}
				location.href='${HOME}/admin/academic/academicReg/soosiScore/TmplExcelDown?curr_seq='+$("#pop_curr_seq").attr("data-value");
			}			

			function openStGradePopup(user_seq, id, name){
				
				$("input[name=user_seq]").val(user_seq);
				$("span[data-name=stInfo]").text(id + " " + name);
				$("#popupType span:eq(1)").click();
				$("#m_pop2").show();
				getAcaSoosiGrade();
				setNextPreSt(id);
			}
			
		</script>
	</head>
	
	<body>
		<input type="hidden" name="aca_seq" id="aca_seq" value="${S_ACA_SEQ }">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
		<!-- s_container_table -->
		<div id="container" class="container_table">
			<!-- s_contents -->
			<div class="contents main">
	
				<!-- s_left_mcon -->
				<div class="left_mcon aside_l grd">
					<div class="sub_menu adm_grd">
						<div class="title">manajemen akademik</div>
						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">sistem management akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">학사코드manajemen</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">menejemen kurikulum akadmik</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">menejemen kurikulum akadmik</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청manajemen</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on" onClick="location.href='${HOME}/admin/academic/academicReg/list'">management registration akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">Manajemen kompetensi kelulusan</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- e_left_mcon -->
	
				<!-- s_main_con -->
				<div class="main_con adm_grd">
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<!-- s_tt_wrap -->
					<div class="tt_wrap">
						<h3 class="am_tt">
							<span class="tt">management registration akademik<span class="sign1"> &gt; </span>nilai yang diinginkanmanajemen</span>
						</h3>
						<span class="sp_state">status akademik : <span class="tt" id="academicState"></span></span>
						<h4 class="h4_tt" id="academicNameTitle"></h4>
					</div>
					<!-- e_tt_wrap -->
	
					<!-- s_adm_content1 -->
					<div class="adm_content3">
						<!-- s_tt_wrap -->
	
						<!-- s_wrap_wrap -->
						<div class="wrap_wrap">
	
							<div class="tab_wrap_cc">							
								<button class="tab01 tablinks" onclick="getAcademic();">informasi dasar</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/curriculumAssign'">Penetapan Kurikulum</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/studentAssign'">학생배정</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/schedule'">waktu표</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/SFSurvey/list'">만족도조사</button>
								<button class="tab01 tablinks" onclick="pageMoveScore('curr','${S_ACA_SEQ }');">종합성적</button>
								<button class="tab01 tablinks active" onclick="pageMoveScore('soosi','${S_ACA_SEQ }');">nilai yang diinginkan</button>												
							<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/currReport'">운영보고서</button>							
							</div>
	
							<!-- s_wrap_wrap -->
							<div class="wrap_wrap">
	
								<!-- s_sch_wrap -->
								<div class="sch_wrap f1">
									<!-- s_rwrap -->
									<div class="rwrap">
										<!-- s_wrap_s -->
										<div class="wrap_s">
											<div class="wrap_s1_uselectbox">
												<div class="uselectbox">
													<span class="uselected" id="curr_seq" data-value="">Keseluruhan nilai yang diinginkan</span> <span class="uarrow">▼</span>
													<div class="uoptions" style="display: none;width:300px;">
														<span class="uoption opt1" data-value="">Keseluruhan nilai yang diinginkan</span> 
														<span class="uoption opt1" data-value="N">성적 미registrasi</span>
														<c:forEach var="currList" items="${currList}">
															<span class="uoption" data-value="${currList.curr_seq}">${currList.curr_name}</span>
						                                </c:forEach>
														
													</div>
												</div>
											</div>
											<button class="btn_tt1" onClick="getGrade();">Pencarian</button>
											<button class="btn_down2" onClick="gradeExcelDown();">성적표 다운로드</button>
	
											<!-- s_r_wrap -->
											<div class="r_wrap">
												<div class="sttwrap">
													<span class="sp01">총</span>
													<span class="sp_n" data-name="totalCurr"></span>
													<span class="sp02">개 교육과정</span>
												</div>
												<div class="sttwrap">
													<span class="twrap1"> 
														<span class="sp01">수시 성적registrasi</span>
														<span class="sp_n" data-name="submitCurr"></span>
														<span class="sp02">건</span>
													</span> 
													<span class="sp_sign">/</span> 
													<span class="twrap2">
														<span class="sp01">미registrasi</span>
														<span class="sp_n" data-name="nonCurr"></span>
														<span class="sp02">건</span>
													</span>
												</div>
											</div>
											<!-- e_r_wrap -->
										</div>
										<!-- e_wrap_s -->
	
									</div>
									<!-- e_rwrap -->
								</div>
								<!-- e_sch_wrap -->
	
								<div class="scroll_wraps">
									<div class="fwrap1">
										<table class="mlms_tbs">
											<tr>
												<td colspan="2" rowspan="3" class="th01 bd01 w4"><span
													class="ts1">Nama Mata Kuliah</span> <span class="ts2">학생정보 </span> <svg
														class="svg1">
															<line x1="0" y1="0" x2="100%" y2="100%"></line>
							                              </svg></td>
												<td class="th01 bd01 w1" colspan="2"><span class="ts">N0.</span>
												</td>
											</tr>
											<tr>
												<td class="th01 bd01 w1 title" colspan="2"><span
													class="ts">Nama Mata Kuliah</span></td>
											</tr>
											<tr>
												<td class="th01 bd01 w1" colspan="2"><span class="ts">시험횟수</span></td>
											</tr>
					                        <tr>
					                          <td class="th01 w2"><span class="ts">학번</span></td>
					                          <td class="th01 w3"><span class="ts">이름</span></td>
					                          <td class="th01 w1_1 color1"><span class="ts">총점</span></td>
											  <td class="th01 w1_2 color1"><span class="ts">석차</span></td>
					                        </tr>
											<tbody id="stListAdd">									
											</tbody>											
										</table>
									</div>
									<div class="fwrap2">
										<table class="mlms_tb">
											<tbody id="currListAdd">
										
											</tbody>
											<tbody id="scoreListAdd">
														
											</tbody>
									
																			
										</table>
									</div>
								</div>
	
	
							</div>
							<!-- e_wrap_wrap -->
						</div>
					</div>
				</div>
			</div>
		</div>			
		<!-- s_nilai yang diinginkan 엑셀registrasi 팝업 -->
	<div id="m_pop1" class="pop_up_ex pop_up_ex_1 mo1">

		<div class="pop_wrap">
			<p class="popup_title">nilai yang diinginkan 엑셀registrasi</p>

			<button class="pop_close close1" type="button">X</button>

			<button class="btn_exls_down_3" onClick="excelTmplDown();">nilai yang diinginkan registrasi 엑셀 양식 다운로드</button>

			<div class="t_title_1">
				<span class="sp_wrap_1">nilai yang diinginkan registrasi 엑셀 양식을 먼저 다운로드 받으세요.<br>엑셀파일에
					nilai yang diinginkan을 기입하고, 해당 교육과정을 pilih하여 엑셀파일을 registrasi하시면<br>성적이 한번에 업로드 됩니다.
				</span>
			</div>

			<div class="sub_fwrap_ex_1">
				<span class="ip_tt">교육과정pilih</span>
				<div class="wrap2_3_uselectbox grd">
					<div class="uselectbox">
						<span class="uselected" id="pop_curr_seq" data-value=""></span> <span class="uarrow">▼</span>
						<div class="uoptions" style="display: none;">
							
						</div>
					</div>
				</div>
			</div>

			<div class="pop_ex_wrap">
				<div class="sub_fwrap_ex_1">
					<form id="xlsForm" onSubmit="return false;">>
						<span class="ip_tt">file excel</span> 
						<input type="text" readonly class="ip_sort1_1" id="xls_filename" value="" onClick="$('#xlsFile').click();">
						<button class="btn_r1_1" onClick="$('#xlsFile').click();">파일pilih</button>         
						<input type="file" style="display:none;" id="xlsFile" name="xlsFile" onChange="file_nameChange();">
					</form>				
				</div>

				<!-- s_t_dd -->
				<div class="t_dd">

					<div class="pop_btn_wrap">
						<button type="button" class="btn01" onclick="excelSubmit();">registrasi</button>
						<button type="button" class="btn02" onClick="$('.close1').click();">batal</button>
					</div>

				</div>
				<!-- e_t_dd -->

			</div>
		</div>
	</div>
	<!-- e_nilai yang diinginkan 엑셀registrasi 팝업 -->
		<jsp:include page="stGradePopup.jsp"/>
</body>
</html>