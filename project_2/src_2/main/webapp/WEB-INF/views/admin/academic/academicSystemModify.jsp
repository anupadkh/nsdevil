<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<script>
			$(document).ready(function() {		   	
			   	
			   	//화면 로드시 sistem akademik 가져온다.
			   	getAcademicLv(1);
			             
			   	//sembunyikan tampilan form pendaftaran pilih 변경 시
			   	$("#chk_btn").change(function(){		                
			   		$("#iptab tbody").remove();
			   		if($(this).prop("checked")){
			   			getAcadecmiNoaddList();
			   			$(".add_btn_tr td:eq(0)").children().hide();
			   			$("#btn_div").show();
			   		}else{
			   			$(".add_btn_tr td:eq(0)").children().show();
			   			getAcademicLv(1);
			   			$("#btn_div").hide();
			   		}
			   	});	    	
		   	
				/* s_perbaiki 모드 변환  */
				$(document).on("click", ".mdf_b", function() {
					$(this).hide();
					$(this).parent().children('div.mdf_a').show();					
				});
				
				//삭제버튼 클릭
				$(document).on("click", ".btn_c", function() {
					if(confirm("삭제하시겠습니까?\nregistrasi된 하위 분류도 삭제됩니다.")){
						var aca_system_seq = $(this).parents("tr").attr("name");
						
						$.ajax({
							type: "POST",						
							url: "${HOME}/ajax/admin/academic/academicSystem/delete",
							data: {
								"aca_system_seq" : aca_system_seq
							},
							dataType: "json",
							success: function(data, status) {
								if (data.status == "200") {
									alert("성공");
									$("#iptab tbody").remove();
									getAcademicLv(1);
								}else if(data.status == "201"){
									if(data.level == "1" || data.level == "2" || data.level == "3")
										alert("해당 sistem akademik의 하위sistem akademik로 registrasi된 학사가 있습니다.\nregistrasi perkuliahan되지 않은 sistem akademik만 삭제/perbaiki할 수 있습니다.");
									else
										alert("해당 sistem akademik로 registrasi된 학사가 있습니다.\nregistrasi perkuliahan되지 않은 sistem akademik만 삭제/perbaiki할 수 있습니다.");
								}
							},
							error: function(xhr, textStatus) {
								document.write(xhr.responseText);
								$.unblockUI();
							},beforeSend:function() {
								$.blockUI();
							},complete:function() {
								$.unblockUI();
							}
						}); 
					}
				});
				
				//tambahkan 버튼 클릭 시
				$(document).on("click", ".add_btn", function(){    
			       	
					var lv = $(this).attr("name");
					var aca_system_name = $(this).prev("input").val();
					var l_seq;
					var m_seq;
					var s_seq;
					if(isEmpty(aca_system_name) || isBlank(aca_system_name)){
						alert("이름을 입력해주세요\n(한글 4~10자)");
						$(this).prev("input").focus();
						return false;
					}
					//상위 시퀀스들 가져온다.
					//jurusan일때
					if(lv != "lv1"){
						l_seq = parseInt($(this).closest("tbody").attr("name"));
						//tahun ajaran일때
						if(lv != "lv2"){
							m_seq = parseInt($(this).siblings("input[name='m_seq']").val());
							//periode일때
							if(lv != "lv3"){
								s_seq = parseInt($(this).siblings("input[name='s_seq']").val());
							}
						}  
					}    
					academicInsert(lv, aca_system_name, l_seq, m_seq, s_seq);
					$(this).prev("input").val("");
				});
				
				//perbaiki 버튼 클릭 시
				$(document).on("click", ".modify_btn", function(){	 
					var this_element = $(this);
					var aca_system_seq = $(this).parents("tr").attr("name");
					var aca_system_name = $(this).siblings(":input[name=modify_aca_name]").val();
					if(aca_system_name == ""){
						alert("체계 명을 입력해주세요");
						return;
					}
					$.ajax({
						type: "POST",	
						url: "${HOME}/ajax/admin/academic/academicSystem/name/modify",
						data: {
							"aca_system_seq" : aca_system_seq,
							"aca_system_name" : aca_system_name
						},
						dataType: "json",
						success: function(data, status) {
							if (data.status == "200") {
								alert("성공");
								//성공하면 숨어있는 인풋창에도 넣어준다.
								$(this_element).parent().parent().children(':input[name=aca_system_name]').val(aca_system_name);
								$(this_element).parent().parent().children('div.mdf_a').hide();
								$(this_element).parent().parent().children('input.mdf_b').show();
							}else if(data.status == "201"){
								if(data.level == "1" || data.level == "2" || data.level == "3")
									alert("해당 sistem akademik의 하위sistem akademik로 registrasi된 학사가 있습니다.\nregistrasi perkuliahan되지 않은 sistem akademik만 삭제/perbaiki할 수 있습니다.");
								else
									alert("해당 sistem akademik로 registrasi된 학사가 있습니다.\nregistrasi perkuliahan되지 않은 sistem akademik만 삭제/perbaiki할 수 있습니다.");
							}
						},
						error: function(xhr, textStatus) {
							document.write(xhr.responseText);
							$.unblockUI();
						},beforeSend:function() {
							$.blockUI();
						},complete:function() {
							$.unblockUI();
						}
					}); 
				});			
				
				//업 버튼 클릭
				$(document).on("click",".up",function(){  
					
					var this_seq = $(this).siblings("input[name='seq_order']").val().split("_")[0]; //순서변경 하려는 시퀀스
					var order = $(this).siblings("input[name='seq_order']").val().split("_")[1]; //순서 번호   
					var l_seq = $(this).siblings("input[name='seq_order']").val().split("_")[2]; //kategori utama 
					var lv = $(this).parent().parent("td").attr("name").split("_")[0];
					var m_seq = "";
					var this_order = "";
					var pre_All_seq = "";
					var td_htmls = new Array();
					var pre_td_htmls = new Array();
					
					if(lv == "lv2"){
						$.each($("#iptab tr[name='"+l_seq+"'] td[name^='lv2']"),function(index){
							if($(this).attr("name") == "lv2_"+this_seq){   
								this_order = index;           
							}
						});
						if(this_order != 0){
							//변경하려는 input 값에 order 값 바꿔준다...
							$("#iptab tr[name='"+l_seq+"'] td[name^='lv2']:eq('"+(this_order)+"') input[name='seq_order']").val(this_seq+"_"+this_order+"_"+l_seq);
							pre_All_seq = $("#iptab tr[name='"+l_seq+"'] td[name^='lv2']:eq('"+(this_order-1)+"') input[name='seq_order']").val().split("_");
							pre_All_seq[1] = (this_order+1);
							$("#iptab tr[name='"+l_seq+"'] td[name^='lv2']:eq('"+(this_order-1)+"') input[name='seq_order']").val(pre_All_seq[0]+"_"+pre_All_seq[1]+"_"+pre_All_seq[2]);
						}
					}
					
					if(lv == "lv3"){
						//lv3 순서 변경 시 상위(jurusan) 시퀀스 가져온다.
						m_seq = $(this).siblings("input[name='seq_order']").val().split("_")[3]; //jurusan
		                     
						$.each($("#iptab tr[name='"+l_seq+"'] td[name^='lv3_"+m_seq+"']"),function(index){
							if($(this).attr("name") == "lv3_"+m_seq+"_"+this_seq){
								this_order = index;                             
							}
						});
						if(this_order != 0){
							//변경하려는 input 값에 order 값 바꿔준다...
							$("#iptab tr[name='"+l_seq+"'] td[name^='lv3_"+m_seq+"']:eq('"+(this_order)+"') input[name='seq_order']").val(this_seq+"_"+this_order+"_"+l_seq+"_"+m_seq);
							pre_All_seq = $("#iptab tr[name='"+l_seq+"'] td[name^='lv3_"+m_seq+"']:eq('"+(this_order-1)+"') input[name='seq_order']").val().split("_");
							pre_All_seq[1] = (this_order+1);
							$("#iptab tr[name='"+l_seq+"'] td[name^='lv3_"+m_seq+"']:eq('"+(this_order-1)+"') input[name='seq_order']").val(pre_All_seq[0]+"_"+pre_All_seq[1]+"_"+pre_All_seq[2]+"_"+pre_All_seq[3]);							
						}
					}
					
					if(this_order == 0){
						alert("맨 처음 순서입니다.");
						return;
					}
		                 
					var td_index = 0; 
					var this_tr_index = 0; //변경할려는 시작 tr 인덱스
					var pre_td_index = 0;
					var pre_tr_index = 0; //위에놈 시작 tr 인덱스
					
					//반복문 돌면서 변경할려는 html 소스 가져온다....
					$.each($("#iptab tr[name='"+l_seq+"']"),function(index){
						//변경하려는 시퀀스 포함된 td 찾기
						if($(this).children("td[name*='"+this_seq+"']").length != 0){  
							if(td_index == 0)
								this_tr_index = index;
							td_htmls[td_index] = $(this).html();
							td_index++;
						}       
					});
					
					//변경하려는 시퀀스 이하 지운다
					$("td[name*='"+this_seq+"']").remove();
					
					//반복문 돌면서 위에놈 html 소스 가져온다....
					$.each($("#iptab tr[name='"+l_seq+"']"),function(index){
						//변경하려는 시퀀스 포함된 td 찾기
						if($(this).children("td[name*='"+pre_All_seq[0]+"']").length != 0){
							if(pre_td_index == 0)
								pre_tr_index = index;
							$.each($("#iptab tr[name='"+l_seq+"']:eq('"+index+"') td"), function(i_index){ 		
								if(lv =="lv2" && $(this).attr("name").indexOf("lv1") == -1){
									pre_td_htmls[pre_td_index] += $(this)[0].outerHTML;
								}else if(lv == "lv3" && $(this).attr("name").indexOf("lv1") == -1 && $(this).attr("name").indexOf("lv2") == -1){
									pre_td_htmls[pre_td_index] += $(this)[0].outerHTML;
								}  
							}); 
							pre_td_index++;
						}   
					});
					
					//위에놈 시퀀스 이하 다지운다
					$("td[name*='"+pre_All_seq[0]+"']").remove();
					
					for(var i=0; i<td_htmls.length;i++){
						$("#iptab tr[name='"+l_seq+"']:eq('"+pre_tr_index+"')").append(td_htmls[i]);
						pre_tr_index++;
					}
					
					for(var i=0; i<pre_td_htmls.length;i++){  
						$("#iptab tr[name='"+l_seq+"']:eq('"+pre_tr_index+"')").append(pre_td_htmls[i]);
						pre_tr_index++;
					}
				});
				
				$(document).on("click",".down",function(){ 					
					var this_seq = $(this).siblings("input[name='seq_order']").val().split("_")[0]; //순서변경 하려는 시퀀스
					var order = $(this).siblings("input[name='seq_order']").val().split("_")[1]; //순서 번호   
					var l_seq = $(this).siblings("input[name='seq_order']").val().split("_")[2]; //kategori utama 
					var lv = $(this).parent().parent("td").attr("name").split("_")[0];
					var m_seq = "";
					var this_order = "";
					var pre_All_seq = "";
					var td_htmls = new Array();
					var pre_td_htmls = new Array();
					var last_index = 0;
					if(lv == "lv2"){
						last_index = $("#iptab tr[name='"+l_seq+"'] td[name^='lv2']").length - 1;
						
						$.each($("#iptab tr[name='"+l_seq+"'] td[name^='lv2']"),function(index){
						    if($(this).attr("name") == "lv2_"+this_seq){   
						        this_order = index;           
						    }
						});
		
						if(this_order != last_index){
						    //변경하려는 input 값에 order 값 바꿔준다...
						    $("#iptab tr[name='"+l_seq+"'] td[name^='lv2']:eq('"+(this_order)+"') input[name='seq_order']").val(this_seq+"_"+(this_order+2)+"_"+l_seq);
						    pre_All_seq = $("#iptab tr[name='"+l_seq+"'] td[name^='lv2']:eq('"+(this_order+1)+"') input[name='seq_order']").val().split("_");
						    pre_All_seq[1] = this_order+1;
						    $("#iptab tr[name='"+l_seq+"'] td[name^='lv2']:eq('"+(this_order+1)+"') input[name='seq_order']").val(pre_All_seq[0]+"_"+pre_All_seq[1]+"_"+pre_All_seq[2]);                            
						}
				    }
		                                     
					if(lv == "lv3"){
						//lv3 순서 변경 시 하위(jurusan) 시퀀스 가져온다.
						m_seq = $(this).siblings("input[name='seq_order']").val().split("_")[3]; //jurusan
						
						last_index = $("#iptab tr[name='"+l_seq+"'] td[name^='lv3_"+m_seq+"']").length - 1;
						
						$.each($("#iptab tr[name='"+l_seq+"'] td[name^='lv3_"+m_seq+"']"),function(index){
							if($(this).attr("name") == "lv3_"+m_seq+"_"+this_seq){
								this_order = index;                             
							}
						});
						
						if(this_order != last_index){
							//변경하려는 input 값에 order 값 바꿔준다...
							$("#iptab tr[name='"+l_seq+"'] td[name^='lv3_"+m_seq+"']:eq('"+(this_order)+"') input[name='seq_order']").val(this_seq+"_"+(this_order+2)+"_"+l_seq+"_"+m_seq);
							pre_All_seq = $("#iptab tr[name='"+l_seq+"'] td[name^='lv3_"+m_seq+"']:eq('"+(this_order+1)+"') input[name='seq_order']").val().split("_");
							pre_All_seq[1] = this_order+1;
							$("#iptab tr[name='"+l_seq+"'] td[name^='lv3_"+m_seq+"']:eq('"+(this_order+1)+"') input[name='seq_order']").val(pre_All_seq[0]+"_"+pre_All_seq[1]+"_"+pre_All_seq[2]+"_"+pre_All_seq[3]);
						}
					}
					
					if(this_order == last_index){
						alert("맨 마지막 순서입니다.");
						return;
					}
					
					var td_index = 0; 
					var this_tr_index = 0; //변경할려는 시작 tr 인덱스
					var pre_td_index = 0;
					var pre_tr_index = 0; //위에놈 시작 tr 인덱스
										
					//반복문 돌면서 변경할려는 html 소스 가져온다....
					$.each($("#iptab tr[name='"+l_seq+"']"),function(index){
						//변경하려는 시퀀스 포함된 td 찾기
						if($(this).children("td[name*='"+pre_All_seq[0]+"']").length != 0){  
							if(pre_td_index == 0)
								pre_tr_index = index;
							pre_td_htmls[pre_td_index] = $(this).html();
							pre_td_index++;
						} 
					});
					
					//반복문 돌면서 위에놈 html 소스 가져온다....
					$.each($("#iptab tr[name='"+l_seq+"']"),function(index){
						//변경하려는 시퀀스 포함된 td 찾기
						if($(this).children("td[name*='"+this_seq+"']").length != 0){
							if(td_index == 0)
								this_tr_index = index;
							
							$.each($("#iptab tr[name='"+l_seq+"']:eq('"+index+"') td"), function(i_index){ 
								if(lv =="lv2" && $(this).attr("name").indexOf("lv1") == -1){
									td_htmls[td_index] += $(this)[0].outerHTML;
								}else if(lv == "lv3" && $(this).attr("name").indexOf("lv1") == -1 && $(this).attr("name").indexOf("lv2") == -1){
										td_htmls[td_index] += $(this)[0].outerHTML;
								} 
							});      
							td_index++;
						}   						
					});
					
					//변경하려는 시퀀스 이하 지운다
					$("td[name*='"+this_seq+"']").remove();
					//위에놈 시퀀스 이하 다지운다
					$("td[name*='"+pre_All_seq[0]+"']").remove();
					for(var i=0; i<pre_td_htmls.length;i++){ 
						$("#iptab tr[name='"+l_seq+"']:eq('"+this_tr_index+"')").append(pre_td_htmls[i]);  						
						this_tr_index++;						
					}
					
					for(var i=0; i<td_htmls.length;i++){
						$("#iptab tr[name='"+l_seq+"']:eq('"+this_tr_index+"')").append(td_htmls[i]);
						this_tr_index++;						
					}					
				});					
			});
			
			//sistem akademik simpan
			function academicInsert(lv, aca_system_name, l_seq, m_seq, s_seq){
				$.ajax({
					type: "POST",
					url: "${HOME}/ajax/admin/academic/academicSystem/create",
					data: {
						"lv" : lv,
						"aca_system_name" : aca_system_name,
						"l_seq" : l_seq,
						"m_seq" : m_seq,
						"s_seq" : s_seq,
						"aca_system_order" : 0						
					},
					dataType: "json",		
					success: function(data, status) {						
						if (data.status == "200") {
							alert("성공");							
							$("#iptab tbody").remove();
							getAcademicLv(1);							
						} else {
							alert('실패');							
						}						
					},
					error: function(xhr, textStatus) {
						document.write(xhr.responseText);
						$.unblockUI();						
					},beforeSend:function() {
						$.blockUI();						
					},complete:function() {
						$.unblockUI();						
					}					
				});  				
			}
			
			//tambahkan버튼 있는버전 가져온다
			function getAcademicLv(){
				$.ajax({
                    type: "GET",
                    url: "${HOME}/ajax/admin/academic/academicSystem/list",
                    data: {  
                    },
                    dataType: "json",
                    success: function(data, status) {
                        var pre_aca_system_seq = 0;
                        var pre_l_seq=0;
                        var pre_m_seq=0;
                        var htmls = "";   
                        var l_cnt = 0;
                        var m_cnt = 0;
                        var s_cnt = 0;
                        
                        $.each(data.list, function(index){
                            
                            if(pre_aca_system_seq != this.aca_system_seq){
                                l_cnt = 0;
                                htmls += '<tbody class="lv1" name="'+this.aca_system_seq+'">';
                                    
                                htmls += '<tr class="tr02" name="'+this.aca_system_seq+'">';
                                htmls += '<td rowspan="'+this.add_l_seq_count+'" class="bg_m1">';
                                htmls += '<div class="mdf_wrap">';
                                htmls += '<input class="ip01 mdf_b" name="aca_system_name" value="'+this.aca_system_name+'" type="text" style="display: block">';  
                                htmls += '<div class="mdf_a" style="display: none">                 ';
                                htmls += '<input type="text" class="ip01s" name="modify_aca_name" value="'+this.aca_system_name+'">';
                                htmls += '<input type="button" value="perbaiki" class="btn_tt4 modify_btn">';
                                htmls += '<button type="button" class="btn_c del_btn" title="삭제하기">X</button>';
                                htmls += '</div>';
                                htmls += '</div> ';
                                htmls += '</td>';
                                htmls += '<td colspan="4" class="ta_c bg_m2">';
                                htmls += '<input class="ip01s add_aca" type="text">';
                                htmls += '<input type="button" value="tambahkan" class="btn_tt3 add_btn" name="lv2">';
                                htmls += '</td>';
                                htmls += '</tr>';
                            }
                            
                            if(pre_l_seq != this.l_seq && this.l_seq != 0){
                                m_cnt = 1;
                                htmls += '<tr class="tr02 lv2" name="'+this.l_seq+'">';
                                htmls += '<td rowspan="'+this.add_m_seq_count+'" class="ta_c w13 bg_m2">';                          
                                htmls += '<div class="mdf_wrap">';
                                htmls += '<input class="ip01 mdf_b" name="aca_system_name" value="'+this.l_aca_name+'" type="text" style="display: block">';   
                                htmls += '<div class="mdf_a" style="display: none">';
                                htmls += '<input type="text" class="ip01s" name="modify_aca_name" value="'+this.l_aca_name+'">';               
                                htmls += '<input type="button" value="perbaiki" class="btn_tt4 modify_btn">';
                                htmls += '<button type="button" class="btn_c del_btn" title="삭제하기">X</button>';
                                htmls += '</div></div></td>';
                                htmls += '<td colspan="3" class="ta_c w13 bg_m2">';
                                htmls += '<input type="hidden" name="l_seq" value="'+this.aca_system_seq+'">';
                                htmls += '<input type="hidden" name="m_seq" value="'+this.l_seq+'">';
                                htmls += '<input type="text" class="ip01s add_aca">';
                                htmls += '<input type="button" value="tambahkan" class="btn_tt3 add_btn" name="lv3">';
                                htmls += '</td>';
                                htmls += '</tr>';
                            }
                            
                            if(pre_m_seq != this.m_seq && this.m_seq != 0){
                                s_cnt = 1;
                                htmls += '<tr class="tr02 lv3" name="'+this.m_seq+'">';
                                htmls += '<td rowspan="'+this.add_s_seq_count+'" class="ta_c bg_m3">';
                                htmls += '<div class="mdf_wrap">';
                                htmls += '<input class="ip01 mdf_b" name="aca_system_name" value="'+this.m_aca_name+'" type="text" style="display: block">';
                                htmls += '<div class="mdf_a" style="display: none">';
                                htmls += '<input type="text" class="ip01s" name="modify_aca_name" value="'+this.m_aca_name+'">';
                                htmls += '<input type="button" value="perbaiki" class="btn_tt4 modify_btn">';
                                htmls += '<button type="button" class="btn_c del_btn" title="삭제하기">X</button>';
                                htmls += '</div></div></td>';
                                htmls += '<td class="ta_c bg_m3">';
                                htmls += '<input type="hidden" name="l_seq" value="'+this.aca_system_seq+'">';
                                htmls += '<input type="hidden" name="m_seq" value="'+this.l_seq+'">';
                                htmls += '<input type="hidden" name="s_seq" value="'+this.m_seq+'">';
                                htmls += '<input type="text" class="ip01s add_aca">';
                                htmls += '<input type="button" value="tambahkan" class="btn_tt3 add_btn" name="lv4">';
                                htmls += '</td>';
                                htmls += '<td class="bg_m3"><a class="ttx t" href="#"></a></td>';
                                htmls += '</tr>';
                            }
                            
                            if(this.s_seq != 0){
                                htmls += '<tr class="tr02 lv4" name="'+this.s_seq+'">';
                                htmls += '<td class="ta_c bg_m3">';
                                htmls += '<div class="mdf_wrap">';
                                htmls += '<input class="ip01 mdf_b" name="aca_system_name" value="'+this.s_aca_name+'" type="text" style="display: block">';
                                htmls += '<div class="mdf_a" style="display: none">';
                                htmls += '<input type="text" class="ip01s" name="modify_aca_name" value="'+this.s_aca_name+'">';
                                htmls += '<input type="button" value="perbaiki" class="btn_tt4 modify_btn">';
                                htmls += '<button type="button" class="btn_c del_btn" title="삭제하기">X</button>';
                                htmls += '</div></div></td>';
                                htmls += '<td class="bg_m3">';
                                if(this.academic_cnt != 0)
    								htmls += '<a class="ttx t" href="#">'+this.academic_cnt+'</a>';
    							else
    								htmls += '<a class="ttx t" href="">-</a>'; 
                                
                                htmls += '</td>';
                                htmls += '</tr>';
                            }
                            if(this.add_l_seq_count+2 == l_cnt)
                                htmls += '</tbody>';
                                
                            pre_aca_system_seq = this.aca_system_seq;
                            pre_l_seq = this.l_seq;
                            pre_m_seq = this.m_seq;   
                            l_cnt++;
                            s_cnt++;
                        });   
                        $("#iptab").append(htmls);                  
                    },
                    error: function(xhr, textStatus) {
                        document.write(xhr.responseText);
                        $.unblockUI();
                    },beforeSend:function() {
                        $.blockUI();
                    },complete:function() {
                        $.unblockUI();                      
                    }                   
                });     
			}
						
			//tambahkan 버튼 없는 리스트 가져오기
			function getAcadecmiNoaddList(){
				$.ajax({
					type: "GET",
					url: "${HOME}/ajax/admin/academic/academicSystem/list",
					data: {  
					},
					dataType: "json",
					success: function(data, status) {
						var pre_aca_system_seq = 0;
						var pre_l_seq=0;
						var pre_m_seq=0;
						$.each(data.list, function(index){
							var htmls = "";   
							//aca_system_seq = kategori utama시퀀스
							htmls += '<tr class="tr02" name="'+this.aca_system_seq+'">';
							if(pre_aca_system_seq != this.aca_system_seq)
								htmls += '<td rowspan="'+this.l_seq_count+'" name="lv1_'+this.aca_system_seq+'" class="bg_m1">'+this.aca_system_name+'</td>';
							// seq_order = 본인시퀀스 _ 순서 _ fakultas시퀀스 _ jurusan 시퀀스    
							
							//l_seq = jurusan 시퀀스
							if(pre_l_seq != this.l_seq){
								htmls += '<td rowspan="'+this.m_seq_count+'" name="lv2_'+this.l_seq+'" class="ta_c w13 bg_m2">'+this.l_aca_name;
								htmls += '<div class="ud_wrap">';      
								htmls += '<input type="hidden" name="seq_order" value="'+this.l_seq+'_'+this.l_aca_order+'_'+this.aca_system_seq+'">';
								htmls += '<input type="button" value="▲" class="up">';
								htmls += '<input type="button" value="▼" class="down">';
								htmls += '</div></td>';								
							}
							
							//m_seq = tahun ajaran 시퀀스
							if(pre_m_seq != this.m_seq){
								htmls += '<td rowspan="'+this.s_seq_count+'" name="lv3_'+this.l_seq+'_'+this.m_seq+'" class="ta_c bg_m2">'+this.m_aca_name;
								htmls += '<div class="ud_wrap">';
								htmls += '<input type="hidden" name="seq_order" value="'+this.m_seq+'_'+this.m_aca_order+'_'+this.aca_system_seq+'_'+this.l_seq+'">';
								htmls += '<input type="button" value="▲" class="up">';
                                htmls += '<input type="button" value="▼" class="down">';
								htmls += '</div></td>';								
							}
							
							htmls += '<td class="ta_c bg_m2 bg_m2" name="lv4_'+this.l_seq+'_'+this.m_seq+'_'+this.s_seq+'">'+this.s_aca_name;
							htmls += '</td>';
							htmls += '<td class="bg_m2" name="lv5_'+this.l_seq+'_'+this.m_seq+'_'+this.s_seq+'">';
							if(this.academic_cnt != 0)
								htmls += '<a class="ttx t" href="#">'+this.academic_cnt+'</a>';
							else
								htmls += '<a class="ttx t" href="">-</a>'; 
							htmls += '</td>';
							htmls += '</tr>';
							
							$("#iptab").append(htmls);
							pre_aca_system_seq = this.aca_system_seq;
							pre_l_seq = this.l_seq;
							pre_m_seq = this.m_seq;							
						});						
					},
					error: function(xhr, textStatus) {
						document.write(xhr.responseText);
						$.unblockUI();
					},beforeSend:function() {
						$.blockUI();
					},complete:function() {
						$.unblockUI();						
					}					
				}); 				
			}	
			
			function formSubmit(){
				if(!confirm("simpan하시겠습니까?"))
					return;	
				
				$("#acasystemForm").ajaxForm({
					type: "POST",
					url: "${HOME}/ajax/admin/academic/academicSystem/order/modify",
					dataType: "json",
					success: function(data, status){
						if (data.status == "200") {
							alert("성공");							
						} else {
							alert("실패");
							$.unblockUI();							
						}
						
					},
					error: function(xhr, textStatus) {
						document.write(xhr.responseText); 
						$.unblockUI();						
					},beforeSend:function() {
						$.blockUI();						
					},complete:function() {
						$.unblockUI();						
					}    					
				});		
				$("#acasystemForm").submit();				
			}
		</script>	
	</head>
	<body>
		<!-- s_container_table -->
		<div id="container" class="container_table">
			<!-- s_contents -->
			<div class="contents main">
				<!-- s_left_mcon -->
				<div class="left_mcon aside_l grd">
					<div class="sub_menu adm_grd">
				        <div class="title">manajemen akademik</div>
						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt on" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">sistem management akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">학사코드manajemen</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">menejemen kurikulum akadmik</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">menejemen kurikulum akadmik</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청manajemen</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicReg/list'">management registration akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">Manajemen kompetensi kelulusan</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- e_left_mcon -->

				<!-- s_main_con -->
				<div class="main_con adm_grd">
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>

					<!-- s_tt_wrap -->
					<div class="tt_wrap">
						<h3 class="am_tt">
							<span class="tt">sistem management akademik</span>
							<span class="tt_s">registrasi된 sistem akademik로 교육과정이 생성되면 해당 sistem akademik는 더 이상 perbaiki할 수 없습니다.
							<br>신규 sistem akademik가 필요한 경우 sistem akademik를 tambahkan로 registrasi해 주세요.
							</span>
						</h3>
					</div>
					<!-- e_tt_wrap -->

					<!-- s_adm_content1 -->
					<div class="adm_content1">
						<!-- s_tt_wrap -->
                        <form id="acasystemForm" onSubmit="return false;" >
						<!-- s_mlms_tb ttb_u -->
						
						<table class="mlms_tb ttb_u" id="iptab" style="margin-bottom:15px;">
                            <thead class="mainTbody">
								<tr class="tr02">
									<td class="th01 w1">kategori utama( ex. 의과대학 )</td>
									<td class="th01 w1">jurusan</td>
									<td class="th01 w1">tahun ajaran ( ex. tahun ajaran )</td>
									<td class="th01 w1">periode</td>
									<td class="th01 w2">학사<br>registrasi
									</td>
								</tr>
								<tr class="tr02 add_btn_tr">
									<td class="ta_c">
									    <input type="text" class="ip01s" value="">
										<input type="button" class="btn_tt3 add_btn" value="tambahkan" name="lv1">
									</td>
									<td colspan="4" class="color1 ta_c">
									    <span class="tts3">sembunyikan tampilan form pendaftaran</span> 
									    <input type="checkbox" id="chk_btn" class="chk01_1">
									</td>
								</tr>						
							</thead>
						</table>
						<!-- e_mlms_tb ttb_u -->
                        
                        </form>
						<div class="bt_wrap" id="btn_div" style="display:none;margin-bottom:15px;">
							<input type="button" value="simpan" class="bt_2" onclick="formSubmit();">
							<input type="button" value="batal" class="bt_3" onclick="if($('#chk_btn').prop('checked')){ $('#chk_btn').prop('checked',false).trigger('change');}">
						</div>

					</div>
					<!-- e_adm_content1 -->
				</div>
				<!-- e_main_con -->

			</div>
			<!-- e_contents -->
		</div>
		<!-- e_container_table -->
	</body>
</html>