<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<style>
.codeInput{width: 98%;
    height: 34px;
    line-height: 34px;
    padding-left: 5px;}
.codeInput:focus{width: 98%;
    height: 32px;
    line-height: 32px;
    padding-left: 5px;
    border:solid 1px rgb(154, 211, 255);}
</style>
<script>
	$(document).ready(function() {		
		if("${code_cate}" == "osce_code_name"){
			var name = "코드1(${code_name})";
			$("ul[data-name=codeMenu] li:eq(1) a").addClass("on");
			$("span[data-name=codeNameTitle]").text(name);
		}else if("${code_cate}" == "cpx_code_name"){			
			var name = "코드2(${code_name})";
			$("ul[data-name=codeMenu] li:eq(2) a").addClass("on");
			$("span[data-name=codeNameTitle]").text(name);
		}else{
			var name = "코드3(${code_name})";
			$("ul[data-name=codeMenu] li:eq(3) a").addClass("on");
			$("span[data-name=codeNameTitle]").text(name);				
		}
	});
	
	function updateMenuName(){
		if(isEmpty($("#menuName").val())){
			alert("변경할 메뉴명을 입력하세요");
			$("#menuName").focus();	
			return;
		}
		
		if(!confirm("메뉴명을 변경하시겠습니까?")){
			return;
		}
			
		$.ajax({
			type : "POST",
			url : "${HOME}/ajax/admin/academic/codeManagement/code/nameChange",
			data : {
				"code_cate" : "${code_cate}"
				,"name" : $("#menuName").val()
			},
			dataType : "json",
			success : function(data, status) {
				if(data.status == "200"){
					alert("메뉴명이 변경 되었습니다.");
					if("${code_cate}" == "osce_code_name"){
						location.href="${HOME}/admin/academic/codeManagement/code1";
					}else if("${code_cate}" == "cpx_code_name"){
						location.href="${HOME}/admin/academic/codeManagement/code2";
					}else{
						location.href="${HOME}/admin/academic/codeManagement/code3";		
					}
					
				}
				
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
				$.unblockUI();
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
	}
	
	function backPage(){
		if("${code_cate}" == "osce_code_name"){
			location.href="${HOME}/admin/academic/codeManagement/code1";
		}else if("${code_cate}" == "cpx_code_name"){
			location.href="${HOME}/admin/academic/codeManagement/code2";
		}else{
			location.href="${HOME}/admin/academic/codeManagement/code3";		
		}
	}
</script>
</head>

<body>
	<!-- s_container_table -->
	<div id="container" class="container_table">
		<!-- s_contents -->
		<div class="contents main">

			<!-- s_left_mcon -->
			<div class="left_mcon aside_l grd">
				<div class="sub_menu adm_grd">
					<div class="title">manajemen akademik</div>

					<!-- s_학사코드 메뉴 -->
					<div class="boardwrap">
						<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">sistem management akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on">학사코드manajemen</span>
							</div>
							<ul class="panel on" data-name="codeMenu">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>	
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">menejemen kurikulum akadmik</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">menejemen kurikulum akadmik</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청manajemen</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicReg/list'">management registration akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">Manajemen kompetensi kelulusan</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
					</div>
					<!-- e_학사코드 메뉴 -->

				</div>
			</div>
			<!-- e_left_mcon -->

			<!-- s_main_con -->
			<div class="main_con adm_grd">
				<!-- s_메뉴 접기 버튼 -->
				<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
				<!-- e_메뉴 접기 버튼 -->

				<!-- s_tt_wrap -->
				<div class="tt_wrap">
					<h3 class="am_tt">
						<span class="tt" data-name="codeNameTitle"><span class="sign1"> &gt; </span>메뉴명 perbaiki</span>
					</h3>
				</div>
				<!-- e_tt_wrap -->

				<!-- s_adm_content2n -->
				<div class="adm_content2n">
					<!-- s_tt_wrap -->

					<!-- s_wrap_wrap -->
					<div class="wrap_wrap">

						<!-- s_mn_wrap -->
						<div class="mn_wrap">
							<!-- s_wrap_s1 -->
							<div class="wrap_s1 mn">
								<span class="tt1">현재 메뉴명</span> 
								<span class="tt2" data-name="codeName">${code_name}</span>
							</div>
							<!-- e_wrap_s1 -->

							<!-- s_wrap_s1 -->
							<div class="wrap_s1 mn">
								<span class="tt1">변경 메뉴명 입력: (최대 8자까지 입력 가능)</span> 
								<input type="text" class="ip_tt1" id="menuName" value="" maxlength="8">
							</div>
							<!-- e_wrap_s1 -->
						</div>
						<!-- e_mn_wrap -->

					</div>
					<!-- e_wrap_wrap -->

					<div class="bt_wrap">
						<button class="bt_2" onClick="updateMenuName();">simpan</button>
						<button class="bt_3" onClick="backPage();">batal</button>
					</div>

				</div>
				<!-- e_adm_content2n -->

			</div>
			<!-- e_main_con -->

		</div>
		<!-- e_contents -->
	</div>
	<!-- e_container_table -->

</body>
</html>