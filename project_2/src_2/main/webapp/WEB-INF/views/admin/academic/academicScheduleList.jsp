<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<script>
			$(document).ready(function() {
				getAcademicTitle();
				getSchdCurrList();
				$("footer").removeClass("ind");
			});
			
			function getAcademicTitle(){
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicReg/titleInfo",
		            data: {                      
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		if(!isEmpty(data.acaInfo.academic_name))
		                		$("#academicNameTitle").text(data.acaInfo.academic_name);
		                	else
		                		$("#academicNameTitle").hide();
		            	
		            		var aca_state = "";
		                	switch(data.acaInfo.aca_state){
	                            case "01" : aca_state = "개설(비공개)";break;
	                            case "02" : aca_state = "개설(공개)";break;
	                            case "03" : aca_state = "학사 중(수업 중)";break;
	                            case "04" : aca_state = "학사종료(미확정)";break;
	                            case "05" : aca_state = "학사종료(성적산정)";break;
	                            case "06" : aca_state = "학사종료(성적발표)";break;
	                            case "07" : aca_state = "성적발표 종료(미확정)";break;
	                            case "08" : aca_state = "학사최종종료(확정)";break;                            
	                        }
			            	$("#academicState").text(aca_state); //타이틀 status akademik
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
			
			function getSchdCurrList(){
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicReg/schedule/currList",
		            data: {        
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		if(data.confirmFlag.schedule_confirm_flag == "Y"){
		            			$("button[name=confirmBtn]").hide();
		            			$("button[name=cancleBtn]").show();
		            		}else{
		            			$("button[name=confirmBtn]").show();
		            			$("button[name=cancleBtn]").hide();
		            		}
			            	var htmls = "";
			            	$("#schdListAdd").empty();
			            	$.each(data.currList, function(index){
			            		var confirm = "-";
			            		if(this.schedule_confirm_flag == "Y")
			            			confirm = "O";
			            		htmls = '<tr>';
			            		htmls += '<td class="td_1 w1 bd01">'+(index+1)+'</td>';
			            		htmls += '<td class="td_1 w2 bd01"><a href="#" class="go_schd" title="menejemen jadwal  페이지로 가기">'+this.curr_code+'</a></td>';
			            		htmls += '<td class="td_1 w4 bd01"><a href="#" class="go_schd" title="menejemen jadwal  페이지로 가기">'+this.curr_name+'</a></td>';
			            		htmls += '<td class="td_1 w3 bd01">'+this.mpf_name+'</td>';
			            		htmls += '<td class="td_1 w3 bd01">'+this.curr_start_date+'<br>'+this.curr_end_date+'</td>';
			            		htmls += '<td class="td_1 w1 bd01">'+this.period_cnt+'</td>';
			            		htmls += '<td class="td_1 w1_1 bd01">'+confirm+'</td>';
			            		htmls += '</tr>';
			            		$("#schdListAdd").append(htmls);
			            	});			            	
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
			
			function schdConfirmUpdate(flag){
				var confirmMsg = "";
				var statusMsg = "";
				
				if(flag=="Y"){
					confirmMsg = "현재 waktu표를 확정상태로 변경하시겠습니까?";
					statusMsg = "확정 완료";
				}else{
					confirmMsg = "확정 batal 하시겠습니까?";
					statusMsg = "확정 batal 완료";
				}
								
				if(!confirm(confirmMsg)){
					return;
				}
				
				$.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/admin/academic/academicReg/academicRegBasicInfo/confirm",
		            data: {        
		            	"flag" : flag
		            	,"confirm_name" : "schedule"
		                ,"aca_seq" : $("#aca_seq").val()
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		alert(statusMsg);
		            		getSchdCurrList();
		            	}else if(data.status=="201"){
		                	alert("status akademik가 [개설] 상태인 학사만 batal 가능합니다.");
		            	}
		            },
		            error: function(xhr, textStatus) {
		                document.write(xhr.responseText); 
		                $.unblockUI();                        
		            },beforeSend:function() {
		                $.blockUI();                        
		            },complete:function() {
		                $.unblockUI();                         
		            }                   
		        });
			}
						
			function curriculumModify(curr_seq){
				if(confirm("menejemen kurikulum akadmik 페이지로 이동하시겠습니까?"))				
				   post_to_url("${HOME}/admin/academic/curriculum/create", {"curr_seq":curr_seq});
			}
			
			function getAcademic(){
		    	post_to_url('${HOME}/admin/academic/academicReg/academicRegBasicInfo/create', {"aca_seq" : "${S_ACA_SEQ}"});
		    }
			
			function pageMoveScore(type, aca_seq){
		    	$.ajax({
	                type: "POST",
	                url: "${HOME}/ajax/admin/academic/academicReg/setAcaSeq",
	                data: {
	                	"aca_seq" : aca_seq
	                    },
	                dataType: "json",
	                success: function(data, status) {
	                	if(type=="curr")
	               			location.href="${HOME}/admin/academic/academicReg/currScore";
		     			else if(type=="soosi")
		     				location.href="${HOME}/admin/academic/academicReg/soosiScore";
		     			else if(type=="fe")
		     				location.href="${HOME}/admin/academic/academicReg/feScore";
	                },
	                error: function(xhr, textStatus) {
	                    alert("실패");
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                },beforeSend:function() {
	                	$.blockUI();
	                },
	                complete:function() {
	                	$.unblockUI();
	                }
	            });
		    }
		</script>
	</head>
	
	<body>
		<input type="hidden" name="aca_seq" id="aca_seq" value="${S_ACA_SEQ }">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
		<!-- s_container_table -->
		<div id="container" class="container_table">
			<!-- s_contents -->
			<div class="contents main">
	
				<!-- s_left_mcon -->
				<div class="left_mcon aside_l grd">
					<div class="sub_menu adm_grd">
						<div class="title">manajemen akademik</div>
						<div class="boardwrap">
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/academicSystem/modify'">sistem management akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						    <div class="title1">
								<span class="sp_tt">학사코드manajemen</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/completeCode" class="tt_2">학사코드</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code1" class="tt_2">코드1(${sessionScope.code1.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code2" class="tt_2">코드2(${sessionScope.code2.code_name})</a></li>
								<li class="wrap"><a href="${HOME }/admin/academic/codeManagement/code3" class="tt_2">코드3(${sessionScope.code3.code_name})</a></li>
							</ul>
							<div class="title1">
								<span class="sp_tt">menejemen kurikulum akadmik</span>
							</div>
							<ul class="panel">
								<li class="wrap"><a href="${HOME}/admin/academic/curriculum/list" class="tt_2">menejemen kurikulum akadmik</a></li>
								<li class="wrap"><a href="${HOME}/admin/academic/courseApplication/list" class="tt_2">수강신청manajemen</a></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt on" onClick="location.href='${HOME}/admin/academic/academicReg/list'">management registration akademik</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
							<div class="title1 wrapx">
								<span class="sp_tt" onClick="location.href='${HOME}/admin/academic/graduationCapability/modify'">Manajemen kompetensi kelulusan</span>
							</div>
							<ul class="panel">
								<li class="wrap xx"></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- e_left_mcon -->
				
				<!-- s_main_con -->
				<div class="main_con adm_grd">
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
					 <!-- s_tt_wrap -->                
					<div class="tt_wrap">
					    <h3 class="am_tt">
					          <span class="tt">management registration akademik<span class="sign1"> &gt; </span>waktu표</span>
					    </h3>   
					    <span class="sp_state">status akademik :  <span class="tt" id="academicState"></span></span> 
						<h4 class="h4_tt" id="academicNameTitle"></h4>                
					</div>
					<!-- e_tt_wrap -->
					    
					<!-- s_adm_content3 --> 
					<div class="adm_content3">
					<!-- s_tt_wrap -->               
					
						<!-- s_wrap_wrap --> 
						<div class="wrap_wrap">
						
							<div class="tab_wrap_cc">
								<button class="tab01 tablinks" onclick="getAcademic();">informasi dasar</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/curriculumAssign'">Penetapan Kurikulum</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/studentAssign'">학생배정</button>
								<button class="tab01 tablinks active" onclick="location.href='${HOME}/admin/academic/academicReg/schedule'">waktu표</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/SFSurvey/list'">만족도조사</button>			
								<button class="tab01 tablinks" onclick="pageMoveScore('curr','${S_ACA_SEQ }');">종합성적</button>
								<button class="tab01 tablinks" onclick="pageMoveScore('soosi','${S_ACA_SEQ }');">nilai yang diinginkan</button>
								<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/academic/academicReg/currReport'">운영보고서</button>	
							</div>
							
							<!-- s_top_tt_wrap --> 
							<div class="top_tt_wrap schd">
							    <span class="sp03">Kode perkuliahan 또는 Nama Mata Kuliah을 pilih하시면 해당 교육과정 waktu표manajemen 화면으로 바로 이동됩니다.</span>
							</div>
							<!-- e_top_tt_wrap -->
	
	                		<table class="mlms_tb schd th">
	                        	<tr>
		                            <td rowspan="2" class="th01 w1">no</td>
		                            <td colspan="2" class="th01 bd01 w5">교육과정</td>
		                            <td rowspan="2" class="th01 w3">책임<br>교수</td>
		                            <td rowspan="2" class="th01 w3">periode</td>  
		                            <td rowspan="2" class="th01 w1">시수</td>
		                            <td rowspan="2" class="th01 w1_1">확정<br>여부</td>
		                        </tr>
		                        <tr>
		                            <td class="th01 w2">코드</td>
		                            <td class="th01 w4">Nama Mata Kuliah</td>
		                        </tr>
	                   		</table>
							<!-- s_tb_wrap --> 
							<div class="tb_wrap">       
								<table class="mlms_tb schd" id="schdListAdd"> 
									
								</table>
							</div>
							<!-- e_tb_wrap --> 	
						</div> 
						<!-- e_wrap_wrap -->
						
						<div class="bt_wrap_1">    
							<div class="bt_swrap">
								<button class="bt_1_1" name="confirmBtn" onclick="schdConfirmUpdate('Y');">waktu표 확정</button>
								<button class="bt_0" name="cancleBtn" onclick="schdConfirmUpdate('N');" style="display:none;">waktu표 확정 batal</button>
							</div> 
						</div>
					</div>
					<!-- e_adm_content3 -->
				</div>
			
			</div>
			<!-- e_contents -->
		</div>
		<!-- e_container_table -->	
	</body>
</html>