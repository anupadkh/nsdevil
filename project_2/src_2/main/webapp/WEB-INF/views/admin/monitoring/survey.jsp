<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
	<style>   
		tr.user-hover:hover{background: rgba(167, 217, 255, 0.3);cursor: pointer;transition: .7s;color: rgb(0, 130, 238);}
	</style>
<script type="text/javascript">	
$(document).ready(function(){
   
});


function getCurrList(page){
	$.ajax({
        type: "POST",
        url: "${HOME}/ajax/admin/lesson/monitoring/curriculum",
        data: {
        	"page" : page
        	,"year" : $("#year").attr("data-value")
        	,"aca_seq" : $("#academic").attr("data-value")
        	,"laca" : $("#l_seq").attr("data-value")
        	,"maca" : $("#m_seq").attr("data-value")
        	,"saca" : $("#s_seq").attr("data-value")			        	
        	,"aca_system_seq" : $("#aca_seq").attr("data-value")
        },
        dataType: "json",
        success: function(data, status) {
        	var htmls = "";
        	if(data.status=="200"){
        		$.each(data.list, function(index){
        			htmls+='<tr class="user-hover" onClick="pageMoveCurriculumView('+this.curr_seq+');">'
						+'<td class="td_1">'+this.year+'>'+this.aca_system_name+'</td>'
						+'<td class="td_1">'+this.laca_name+'</td>'
						+'<td class="td_1">'+this.maca_name+'</td>'
						+'<td class="td_1">'+this.saca_name+'</td>'
						+'<td class="td_1">'+this.curr_code+'</td>'
						+'<td class="td_1">'+this.curr_name+'</td>'
						+'<td class="td_1">'+this.mpf_name+'</td>'
						+'<td class="td_1">'+this.code_name+'</td>'
						+'<td class="td_1">'+this.grade+'</td>'
						+'<td class="td_1">'+this.period_cnt+'</td>'
						+'<td class="td_1">'+this.max_period_seq+'</td>'
						+'<td class="td_1">'+this.curr_sf_chk+'</td>'
						+'</tr>';
        		});
        		$("#mtCurrListAdd").html(htmls);
        		$("#pagingBtnAdd").html(data.pageNav);
        		
        	}else{
        		alert("교육과정 리스트 가져오기 실패");
        	}
        },
        error: function(xhr, textStatus) {
            alert("오류가 발생했습니다.");
        }
    });					
}

function pageMoveCurriculumView(curr_seq){
	$.ajax({
        type: "POST",
        url: "${M_HOME}/ajax/pf/common/setCurrLpSeq",
        data: {   
        	"lp_seq" : "0",           	
        	"curr_seq" : curr_seq
        },
        dataType: "json",
        success: function(data, status) {
       		var win = window.open('${HOME}/admin/lesson/survey','_blank');
       		win.focus();
        },
        error: function(xhr, textStatus) {
            //alert("오류가 발생했습니다.");
            document.write(xhr.responseText);
        }
	});
}
</script>
</head>
<body>
<input type="hidden" name="pageName" value="monitoring">
<div class="main_con adm_ccschd adm_grd">
<!-- s_메뉴 접기 버튼 -->	
<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
<a class="btn_l"><span>좌측 메뉴 접기/펴기</span></a>
<!-- e_메뉴 접기 버튼 -->	
<div class="tt_wrap">
    <h3 class="am_tt"><span class="tt"></span></h3>                    
</div>
<!-- s_tab_wrap_cc --> 
<div class="tab_wrap_cc" data-name="menuBtn">
<!-- s_해당 탭 페이지 class에 active tambahkan --> 
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson'">rencan kurikulum</button>	         
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/monitoring/schedule'">waktu표manajemen</button>	
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/monitoring/unit'">단원manajemen</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/monitoring/unitSchedule'">penambahan bab pembelajaran</button>
	<button class="tab01 tablinks active" onclick="location.href='${HOME}/admin/lesson/monitoring/survey'">만족도 조사</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/monitoring/asgmt'">종합 과제</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/monitoring/soosiGrade'">수시 성적</button>
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/monitoring/grade'">종합 성적</button>	
	<button class="tab01 tablinks" onclick="location.href='${HOME}/admin/lesson/monitoring/report'">운영보고서</button>
<!-- e_해당 탭 페이지 class에 active tambahkan -->
</div>

		<div class="adm_content2" style="width:98%;float:right;">
			<!-- s_tt_wrap -->

			<!-- s_wrap_wrap -->

			<!-- e_wrap_wrap -->

			<!-- s_wrap_wrap -->
			<div class="wrap_wrap" style="padding:0;">
				<!-- s_btnwrap_s2 -->

				<!-- e_btnwrap_s2 -->

				<table class="mlms_tb curri">
					<thead>
						<tr>
							<th class="th01 bd01 w2">개설semester</th>
							<th class="th01 bd01 w2">fakultas</th>
							<th class="th01 bd01 w2">jurusan</th>
							<th class="th01 bd01 w2">tahun ajaran</th>
							<th class="th01 bd01 w2">교육<br>과정<br>코드</th>
							<th class="th01 bd01 w3">교육<br>과정명</th>
							<th class="th01 bd01 w2">책임<br>교수</th>
							<th class="th01 bd01 w2">이수<br>구분</th>
							<th class="th01 bd01 w2">nilai</th>
							<th class="th01 bd01 w2">시수<br>(계획)</th>
							<th class="th01 bd01 w2">waktu</th>
							<th class="th01 b_1 w4 pd1">작성<br>여부</th>
						</tr>
					</thead>
					<tbody id="mtCurrListAdd">
						
					</tbody>
				</table>

			</div>
			<!-- e_wrap_wrap -->

			<!-- s_pagination -->
			<div class="pagination" id="pagingBtnAdd">
			
			</div>
			<!-- e_pagination -->

		</div>
	</div>
</body>