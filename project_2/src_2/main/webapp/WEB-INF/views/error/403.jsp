<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="Description" content="의과대학 LMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="width=1100">
		<title>MLMS</title>
		<link rel="icon" type="img/png" href="${IMG}/favicon.png">
		<link rel="shortcut icon" href="${IMG}/favicon.png">
		<link rel="icon" href="${IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${IMG}/favicon.ico"> 

		<style>
			body{background: rgba(243,243,243,1.00);font-family: 'Malgun Gothic','Dotum','sans-serif';letter-spacing: 0px;}
			#unconnect{display: table;width: 800px;height: auto;margin: 50px auto;background: url(${IMG}/unconnect.png) no-repeat 50% 3% rgba(212, 212, 212, 1);border-radius: 30px;}
			#unconnect .tt:before{content: "!";color: rgba(0,0,0,1.00);font-size: 18px;font-weight: bold;padding: 0 9px;margin: 0 5px 0 0;background: rgba(255, 242, 97, 1);border:solid 2px rgba(0, 0, 0, 1);border-radius: 19px;}
			#unconnect .tt{display: table;width: 100%;height: 30px;line-height: 29px;font-size: 17px;text-indent:80px;letter-spacing: 0px;color: rgba(255, 42, 45, 1);margin: 330px 0 0 0;}
			#unconnect .err_tt{display: table;width: 75%;height: auto;min-height: 70px;background: rgba(255, 255, 255, 1);margin: 10px auto 0 auto;padding:15px 25px 7px 25px;box-shadow: inset 2px 2px 2px rgba(140, 140, 140, 1);color: rgba(63,63,63,1.00);font-size: 15px;text-align: left;line-height: 1.5;}
			#unconnect .tt span.ts{font-size: 15px;letter-spacing: -.5px;}
			#unconnect .err_tt .t1{width: 100%; font-weight: bold;margin: 0 auto 5px auto;}
			#unconnect .err_tt .t2{width: 100%; font-weight:normal;margin: 0 auto 13px auto;}
			#unconnect .btn1{display: block; width: 200px;height:35px;line-height: 33px;border-radius: 20px;0;margin: 15px auto 70px auto;font-size: 17px; background: rgba(0,0,0,1.00);color: rgba(255,214,0,1.00);box-shadow:0px 1px 0px rgba(255,214,0,1.00);border:0px;cursor: pointer;}
			#unconnect .btn1:hover{background: rgba(255, 227, 83, 1);color: rgba(0,0,0,1.00) ;box-shadow:0px 1px 0px rgba(0,0,0,1.00);transition: .5s;}
		</style>
	</head>

	<body>
		<div id="unconnect">
			<p class="tt">Unable to Connect <span class="ts">[ 접근 할 수 없는 페이지 ]</span></p>
			<div class="err_tt">
				<!-- 오류 isi 예시 -->   
				<p class="t1">Issue:</p>
				<p class="t2">The Backburner Servers cannot connect to the Manager when using its IP address. Error (0x274d) is given</p>
				<p class="t1">Causes:</p>
				<p class="t2">This can happen when you're computer is set to prefer IPv6 addressing over IPv4</p>
				<p class="t1">Solution:</p>
				<p class="t2">Use the Microsoft Easy Fix "Prefer IPv4 over IPv6 in prefix policies"</p>  
			</div>
			<button class="btn1" onclick="javascript:history.go(-1); return false;">◀ Go back</button>
		</div>
	</body>
</html>