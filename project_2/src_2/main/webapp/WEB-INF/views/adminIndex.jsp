<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="Description" content="의과대학 LMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="width=1100">
		
		<title>MLMS</title>
		
		<link rel="icon" type="img/png" href="${IMG}/favicon.png">
		<link rel="shortcut icon" href="${IMG}/favicon.png">
		<link rel="icon" href="${IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${IMG}/favicon.ico"> 
		<link rel="stylesheet" href="${CSS}/admin_style.css" type="text/css" >
		<link rel="stylesheet" href="${CSS}/datetime.css" type="text/css" >
		
		<script src="${JS}/lib/jquery-1.11.1.js"></script>
		<script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.form.min.js"></script>
		<script src="${JS}/lib/flexslider.js"></script>
		<script src="${JS}/lib/slied_js.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/common.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function(){
				topPopupInit();
				initPageTopButton();
				customSelectBoxInit();
				$(document).mouseup(function(e){
					layerPopupCloseInit(['div[name="myProfilePopupDiv"]']);
				});
				
				getNoticeList();
				getHotnewsList();
				getLearningList();
				getQnaCnt();				
			    getAcademicStageInfo();
			    acasystemStageOfList(1);
				
		        $(".pfwrap").hide();
				$(".pfbtn").click(function(){
			        $(".stwrap").hide();
					$(".pfwrap").show();
			        $(".pfbtn").addClass('on');
					$(".stbtn").removeClass('on');
			    });
			    $(".stbtn").click(function(){
			        $(".pfwrap").hide();
					$(".stwrap").show();
			        $(".stbtn").addClass('on');
					$(".pfbtn").removeClass('on');
			    });
			});
			

			function acasystemStageOfList(level, l_seq, m_seq, s_seq){
                if(level == ""){
                    alert("분류 없음");
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "${HOME}/ajax/admin/academic/academicSystem/acaStageOfList",
                    data: {
                        "level" : level,
                        "l_seq" : l_seq,                       
                        "m_seq" : m_seq,
                        "s_seq" : s_seq
                        },
                    dataType: "json",
                    success: function(data, status) {
                        var htmls = '';
                        
                        if(level == 1){
                        	htmls = '<span class="uoption firstseleted" data-value="" onClick="lSeqSet(0);">pilih</span>';
                        	$.each(data.list, function(index){
                                htmls +=  '<span class="uoption" data-value="'+this.aca_system_seq+'" onClick="lSeqSet('+this.aca_system_seq+');">'+this.aca_system_name+'</span>';
                            });
                            $("#lacaListAdd").html(htmls); 
                            
                            if(!isEmpty(getCookie("l_seq"))){
                            	if($("#lacaListAdd span[data-value="+getCookie("l_seq")+"]").length != 0)
                            		$("#lacaListAdd span[data-value="+getCookie("l_seq")+"]").click();
                            	else
                            		$("#lacaListAdd span:eq(0)").click();
                            }else{
                            	$("#lacaListAdd span:eq(0)").click();
                            }
                        }                      
                        else if(level == 2){
                        	htmls = '<span class="uoption firstseleted" data-value="" onClick="mSeqSet(0);">jurusan</span>';
                        	$.each(data.list, function(index){
                                htmls +=  '<span class="uoption" data-value="'+this.aca_system_seq+'" onClick="mSeqSet('+this.aca_system_seq+');">'+this.aca_system_name+'</span>';
                            });
                            $("#macaListAdd").html(htmls);
                            
                            if(!isEmpty(getCookie("m_seq"))){
                            	if($("#macaListAdd span[data-value="+getCookie("m_seq")+"]").length != 0)
                            		$("#macaListAdd span[data-value="+getCookie("m_seq")+"]").click();
                            	else
                            		$("#macaListAdd span:eq(0)").click();
                            }else{
                            	$("#macaListAdd span:eq(0)").click();
                            }
                        }else if(level == 3){
                        	htmls = '<span class="uoption firstseleted" data-value="" onClick="sSeqSet(0);">tahun ajaran</span>';
                        	$.each(data.list, function(index){
                                htmls +=  '<span class="uoption" data-value="'+this.aca_system_seq+'" onClick="sSeqSet('+this.aca_system_seq+');">'+this.aca_system_name+'</span>';
                            });
                            $("#sacaListAdd").html(htmls);
                            
                            if(!isEmpty(getCookie("s_seq"))){
                            	if($("#sacaListAdd span[data-value="+getCookie("s_seq")+"]").length != 0)
                            		$("#sacaListAdd span[data-value="+getCookie("s_seq")+"]").click();
                            	else
                            		$("#sacaListAdd span:eq(0)").click();
                            }else{
                            	$("#sacaListAdd span:eq(0)").click();
                            }                            
                        }else{
                        	htmls = '<span class="uoption firstseleted" data-value="">periode</span>';
                        	$.each(data.list, function(index){
                                htmls +=  '<span class="uoption" data-value="'+this.aca_system_seq+'">'+this.aca_system_name+'</span>';
                            });
                            $("#acaListAdd").html(htmls);

                            if(!isEmpty(getCookie("aca_seq"))){
                            	if($("#acaListAdd span[data-value="+getCookie("aca_seq")+"]").length != 0)
                            		$("#acaListAdd span[data-value="+getCookie("aca_seq")+"]").click();
                            	else
                            		$("#acaListAdd span:eq(0)").click();
                            }else{
                            	$("#acaListAdd span:eq(0)").click();
                            }
                        }
                    },
                    error: function(xhr, textStatus) {
                        //alert("오류가 발생했습니다.");
                        document.write(xhr.responseText);
                    },beforeSend:function() {
                    },
                    complete:function() {
                    }
                }); 
           	}
			
			function lSeqSet(seq){
				acasystemStageOfList(2, seq);
                $("#m_seq").text("jurusan");
                $("#m_seq").attr("data-value","");
                $("#s_seq").text("tahun ajaran");
                $("#s_seq").attr("data-value","");
                $("#aca_seq").text("periode");
                $("#aca_seq").attr("data-value","");
			}
			
			function mSeqSet(seq){
				acasystemStageOfList(3, "", seq);
                $("#s_seq").text("tahun ajaran");
                $("#s_seq").attr("value","");
                $("#aca_seq").text("periode");
                $("#aca_seq").attr("data-value","");
			}
			
			function sSeqSet(seq){
				acasystemStageOfList(4, "", "", seq);
                $("#aca_seq").text("periode");
                $("#aca_seq").attr("data-value","");
			}
			
			function getNoticeList() {
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/common/SLife/notice/list",
			        data: {},
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var list = data.list;
				        	var listHtml = '';
				        	var cnt = 0;
				        	$(list).each(function(index){
				        		var boardSeq = this.board_seq;
				        		var title = this.title;
				        		var board_cate_code = this.board_cate_code;
				        		var board_cate_name = "";
				        		if(board_cate_code == "99" || board_cate_code == "98" || board_cate_code == "97"){
					        		if(board_cate_code == "99"){
					        			board_cate_name = "긴급공지";
					        		}else if(board_cate_code == "98"){
					        			board_cate_name = "시스템<br>점검";
					        		}else if(board_cate_code == "97"){
					        			board_cate_name = "dan yang lainnya";
					        		}
					        		
				
									listHtml = '<div class="open1 con2" onClick="getNoticeDetail('+boardSeq+');">'
									+'<span class="sp1">'+board_cate_name
									+'</span> <span class="sp2">'+title+'</span>'
									+'</div>';
									$("#noticeListAdd").append(listHtml);
									cnt++;
				        		}
				        	});
							if(cnt == 0){
								listHtml = '<div class="con">공지사항의 <br> 긴급공지 / 시스템점검 / dan yang lainnya 공지를 registrasi하시면<br> 로그인 시 공지팝업으로<br>Keseluruhan 공지됩니다.<br></div>';
							}
			        	} else {
			        		//TODO
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function getHotnewsList() {
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/common/SLife/hotnews/list",
			        data: {},
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var list = data.list;
				        	var listHtml = '';
				        	var cnt = 0;
				        	$(list).each(function(){
				        		var boardSeq = this.board_seq;
				        		var title = this.title;
				        		var thumbnail_path = this.thumbnail_path;
				        		if(isEmpty(thumbnail_path)){
				        			listHtml = '<div class="cont_wrap imgx">'
				        			+'<div class="s_wrap2">'
				        			+'<div class="tt" onclick="javascript:hotnewsDetail('+boardSeq+');">'+title+'</div>'
				        			+'<div class="con">'+this.content+'</div>'
				        			+'<div class="wrap_s1"><span class="tm">'+this.reg_date+'</span><span class="vw">'+this.hits+'</span></div>'
				        			+'</div>'
				        			+'</div>';
				        		}else{
				        			listHtml = '<div class="cont_wrap">'
				        			+'<div class="s_wrap1">'
				        			+'<div class="wrap1">'
				        			+'<img src="${RES_PATH}'+thumbnail_path+'" alt="핫뉴스 이미지" class="img_r1">'
				        			+'</div>'
				        			+'<div class="tt" onclick="javascript:hotnewsDetail('+boardSeq+');">'+title+'</div>'
				        			+'<div class="wrap_s1">'
				        			+'<span class="tm">'+this.reg_date+'</span><span class="vw">'+this.hits+'</span>'
				        			+'</div>'
				        			+'</div>'
				        			+'</div>';
				        		}
				        		
							
								if (cnt < 2) {
									$("#hotNewListAdd").append(listHtml);
								}
								cnt++;
				        	});
			        	} else {
			        		//TODO
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function getLearningList() {
			    $.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/admin/board/learning/list",
			        data: {},
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var list = data.list;
				        	var listHtml = '';
				        	var cnt = 0;
				        	$(list).each(function(index){
				        		var boardSeq = this.board_seq;
				        		var title = this.title;
				        		var thumbnail_path = this.thumbnail_path;
				        		if(isEmpty(thumbnail_path)){
				        			$("#learningBoardListAdd").addClass("imgx");				        			
				        			listHtml = '<div class="s_wrap2">'
					        			+'<div class="wrap2">'
					        			+'<div class="tt" onClick="javascript:boardDetail('+boardSeq+');">'+title+'</div>'					 				   
					        			+'<div class="con">'+this.content+'</div>'
					        			+'<div class="wrap_s1"><span class="tm">'+this.reg_date+'</span><span class="vw">'+this.hits+'</span></div>'
					        			+'</div>'			   
					        			+'</div>';
				        			
				        		}else{
				        			listHtml = '<div class="s_wrap1">'
					        			+'<div class="wrap1">'
					        			+'<img src="${RES_PATH}'+thumbnail_path+'" alt="학습자료 이미지" class="img_r1">'
					        			+'</div>'
					        			+'<div class="wrap2">'
					        			+'<div class="tt" onClick="javascript:boardDetail('+boardSeq+');">'+title+'</div>'				 				   
					        			+'<div class="con">'+this.content+'</div>'
					        			+'<div class="wrap_s1"><span class="tm">'+this.reg_date+'</span><span class="vw">'+this.hits+'</span></div>'
					        			+'</div>'			   
					        			+'</div>';
				        		}				        		
							
								if (index == 0) {
									$("#learningBoardListAdd").append(listHtml);
								}
								
				        	});
			        	} else {
			        		//TODO
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function noticeDetail(boardSeq) {
				if (typeof boardSeq !=  "undefined" && boardSeq != "") {
					location.href = '${HOME}/admin/board/notice/detail?seq='+boardSeq
				}
			}
			
			function getNoticeDetail(boardSeq) {
				$.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/common/SLife/notice/detail",
			        data: {
			        	"board_seq":boardSeq
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var noticeInfo = data.notice_info;
			        		var attachList = data.attach_list;
			        		
			        		$("#noticeContent").html(noticeInfo.content);
			        		$("span[data-name=noticeAttachCnt]").text(attachList.length);
			        		if (attachList.length > 0) {
			        			$("button.add_file").attr("onClick",'javascript:redirectZipfileDownload(\'${HOME}\','+boardSeq+', '+attachList.length+', \'' + noticeInfo.title + '\', \'/common/SLife/notice/list\');');				        		
			        		}else{
			        			$("button.add_file").attr("onClick","alert('첨부파일이 없습니다.')");
			        		}
			        		$("#m_pop1").show();
			        		$("#noticeMoveBtn").attr("onClick","noticeDetail("+boardSeq+")");			        				
			        	} else {
			        		alert("공지사항 불러오기에 실패하였습니다. [" + data.status + "]");
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function getQnaCnt() {

				var now = new Date(); 
				var nowDayOfWeek = now.getDay(); 
				var nowDay = now.getDate(); 
				var nowMonth = now.getMonth(); 
				var nowYear = now.getYear(); 
				nowYear += (nowYear < 2000) ? 1900 : 0; 
				var weekStartDate = new Date(nowYear, nowMonth, nowDay - nowDayOfWeek); 
				var weekEndDate = new Date(nowYear, nowMonth, nowDay + (6 - nowDayOfWeek)); 
				
				$.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/common/SLife/admin/qnaCnt",
			        data: {
			        	"week_sdate" : dateToString(weekStartDate)
			        	,"week_edate" : dateToString(weekEndDate)
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {   
			        		$("span[data-name=qnaCnt]").text(data.qnaCnt);
			        		$("span[data-name=acaScheduleCnt]").text(data.acaScheduleCnt);
			        	} else {
			        		alert("1:1문의, 학사일정 건 수 실패하였습니다. [" + data.status + "]");
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function hotnewsDetail(boardSeq) {
				if (typeof boardSeq !=  "undefined" && boardSeq != "") {
					location.href = '${HOME}/common/SLife/hotnews/detail?seq='+boardSeq
				}
			}
			
			function boardDetail(boardSeq) {
				if (typeof boardSeq !=  "undefined" && boardSeq != "") {
					location.href = '${HOME}/admin/board/osce/detail?seq='+boardSeq
				}
			}
			
			if ('${overlapLogin}' == 'Y') {
				alert("다른 사용자가 로그인을 하였습니다.");
				location.href = '${HOME}/login';
			}
			
			function loginSubmit() {
			    
				var id = $("#id").val();
				if (isEmpty(id) || isBlank(id)) {
					alert("아이디를 입력해주세요");
					$("#id").focus();
					return;
				}
				
				var pwd = $("#pwd").val();
				if (isEmpty(pwd) || isBlank(pwd)) {
					alert("비밀번호를 입력해주세요");
					$("#pwd").focus();
					return;
				}
			    
			    $.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/login",
			        data: {
			            "id" : id,
			            "pwd" : pwd
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	if (data.user_level == 1) { //manajemen자
			            		location.href = "${HOME}/admin/academic/academicSystem/modify";
			            	}
			            	location.href = "${HOME}";
			            } else if (data.status == "102") {
			            	alert("사용할 수 없는 계정입니다.");
			        	}else {
			                alert("잘못된 ID 또는 비밀번호 입니다.");
			            }
			        },
			        error: function(xhr, textStatus) {
			            //alert("오류가 발생했습니다.");
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			//이용자manajemen
			function searchUser() {
				
				if($(".pfbtn").hasClass('on'))
					location.href="${HOME}/admin/user/pf/list?user_name="+$("#pf_name").val()+"&propessor_id="+$("#propessor_id").val();
				else
					location.href="${HOME}/admin/user/st/list?user_name="+$("#st_name").val()+"&user_id="+$("#st_id").val();
			}
			
			function getAcademicStageInfo(){
				$.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/common/getAcademicStageInfo",
			        data: {
			            "year" : $("#year").attr("data-value")
			            ,"l_seq" : $("#l_seq").attr("data-value")
			            ,"m_seq" : $("#m_seq").attr("data-value")
			            ,"s_seq" : $("#s_seq").attr("data-value")
			            ,"aca_system_seq" : $("#aca_seq").attr("data-value")
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	var htmls = '';
			            	$("#academicStageListAdd").empty();
			            	$.each(data.acaList, function(index){
			            		var total_period = this.max_lp; //총 시수
			            		var re_period = this.this_lp; //남은 시수
			            		var percent = 0;
			            		var progressPX = 0;
			            		
			            		if(total_period!=0){
			            			percent = Math.floor((total_period-re_period)/total_period*100);
			            			progressPX = percent * 2;
			            		}
			            		
			            		htmls = '<tr onClick="pageMoveLesson('+this.year+','+this.l_aca+','+this.m_aca+','+this.s_aca+','+this.aca_system_seq+');">'
								+'<td class="td1 w1">1</td>'
								+'<td class="td1 w2">'
								+'<div class="wrap_m1">'
								+'<span class="tt1">'+this.academic_name+'</span><span class="num">('+this.curr_cnt+')</span>'
								+'</div>'
								+'</td>'
								+'<td class="td1 w3">'
								+'<div class="wrap_prg">'
								+'<div class="prg">'
								+'<span class="bar" style="width:'+progressPX+'px;"></span>'
								+'</div>'
								+'<span class="sp1">'+percent+'%</span>'
								+'</div>'
								+'</td>'
								+'</tr>';
								$("#academicStageListAdd").append(htmls);
			            	});
			            	
			        	}else {
			                alert("학사리스트 가져오기 실패.");
			            }
			        },
			        error: function(xhr, textStatus) {
			            //alert("오류가 발생했습니다.");
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });			
			}
			
			function pageMoveLesson(year, l_aca, m_aca, s_aca, aca_system_seq){
				setCookie("acaYear", year, 1);
				setCookie("l_seq", l_aca, 1);
				setCookie("m_seq", m_aca, 1);				
				setCookie("s_seq", s_aca, 1);				
				setCookie("aca_seq", aca_system_seq, 1);		
				
				location.href='${HOME}/admin/lesson';
							
			}
		</script>
	</head>

	<body class="new1 adm">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
		    <p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a>
			<a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
		<!-- s_wrap -->
		<div id="wrap">
			<!-- s_top_pop -->
			<div id="top_pop">
				<div class="wrap">
					<div class="slide-wrap">
						<ul>
						    <li class="tt"><img src="${IMG}/pop001.png" alt="의과대학 의학교육 penilaian인증 인증 획득 4년"></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- e_top_pop -->
			
			<!-- s_header -->
			<header class="header">
				<div class="header_top">
					<div class="top_wrap">       
						<div class="top_m">
						<!-- //TODO 임시로 제외시킴 -->
						<!-- <a href="javascript:void(0);" id="topPopupBtn" class="a_m a1" onclick="javascript:topPopupToggle();">팝업닫기</a> -->
							<c:choose>
								<c:when test='${HOME ne ""}'>
									<a href="${HOME}" class="a_m a3">HOME</a>
								</c:when>
								<c:otherwise>
									<a href="/" class="a_m a3">HOME</a>
								</c:otherwise>
							</c:choose>
							<!-- //TODO 임시로 제외시킴 -->
							<!-- <a href="#" class="a_m a4">사이트맵</a> -->
							<c:choose>
								<c:when test="${sessionScope.S_USER_ID ne null}">
									<a href="${HOME}/logout" class="a_m a5">로그아웃</a>
								</c:when>
								<c:otherwise>
									<a href="${HOME}/login" class="a_m a5">로그인</a>
								</c:otherwise>
							</c:choose>
							<a href="${HOME_URL }" class="a_m a6">기관Home</a>
						</div>
						<c:if test="${sessionScope.S_USER_ID ne null}">
							<div class="a_mp a7" onclick="myProfilePopup()">
								<c:choose>
									<c:when test='${sessionScope.S_USER_PICTURE_PATH ne null and sessionScope.S_USER_PICTURE_PATH ne ""}'>
										<span class="pt01"><img src="${sessionScope.S_USER_PICTURE_PATH}" alt="나의 대표 이미지" class="pt_img"></span>
									</c:when>
									<c:otherwise>
										<span class="pt01"><img src="${DEFAULT_PICTURE_IMG}" alt="나의 대표 이미지" class="pt_img"></span>
									</c:otherwise>
								</c:choose>
								
								<span class="ssp1">${sessionScope.S_USER_NAME} (${sessionScope.S_USER_DEPARTMENT_NAME})</span>
							</div>
							<button class="logout" onClick="location.href='${HOME}/logout'">로그아웃</button>
							<!-- s_pop_pop9 -->
							<div id="pop_pop9" name="myProfilePopupDiv">
								<div class="spop9 show" id="spop9">
									<span class="tt">${sessionScope.S_USER_NAME} (${sessionScope.S_USER_DEPARTMENT_NAME})</span>
									<span class="tt">${sessionScope.S_USER_EMAIL}</span>
									<button onclick="location.href='${HOME}/common/my'" class="btn btn05">내 정보 perbaiki</button>
									<span onclick="$('#pop_pop9').hide();" class="pop_close_s" title="닫기">X</span>
								</div>
							</div>
							<!-- e_pop_pop9 --> 
						</c:if>
					</div>
				</div>
				<!-- s_gnbwrap -->			
				<div class="gnbwrap">
					<!-- s_gnb_swrap -->  
					<div class="gnb_swrap">
						<h1 class="logo">
						<c:choose>
							<c:when test='${HOME ne ""}'>
								<a href="${HOME}" title="홈으로 가기"><img src="${TOP_LOGO}" alt="의과대학 로고"></a>
							</c:when>
							<c:otherwise>
								<a href="/" title="홈으로 가기"><img src="${TOP_LOGO}" alt="의과대학 로고">	</a>
							</c:otherwise>
						</c:choose>
						</h1>
					</div>
					<!-- e_gnb_swrap --> 
					
					<!-- s_gnb_fwrap -->  
					<div class="gnb_fwrap">
						<div class="gnb_swrap2">
							<ul id="gnb" class="gnb">
								<c:forEach items="${sessionScope.S_ALLOW_MENU_LIST}" var="allowMenuList" step="1">
									<li><a href="${HOME}${allowMenuList.url}" onclick="javascript:pageLinkCheck('${HOME}','${HOME}${allowMenuList.url}'); return false;" <c:if test='${allowMenuList.now_menu_yn eq "Y" && allowMenuList.url ne ""}'>class="on"</c:if>>${allowMenuList.name}</a></li>
								</c:forEach>
							</ul>
						</div>
					</div>
					<!-- e_gnb_fwrap --> 
				</div>
				<!-- e_gnbwrap -->      
			</header>   
			
			<!-- s_container_table  기관 홍보배너가 없을 때 class nonbn 삽입 -->
			<div id="container" class="container_table nonbn">
				<!-- s_contents -->
				<div class="contents index">
	
					<!-- s_div1 -->
					<div class="div1">
						<div class="ind_tt_wrap">
							<h3 class="tt1">수업manajemen</h3>
	
	
							<div class="sch_wrap">
								<!-- s_wrap1_lms_uselectbox -->
								<div class="wrap1_lms_uselectbox" style="width:65px;">
									<div class="uselectbox">
										<span class="uselected" id="year" data-value="${academicYearList[0].year }">${academicYearList[0].year }</span> <span class="uarrow">▼</span>
	
										<div class="uoptions">
											<c:forEach var="academicYearList" items="${academicYearList}">
												<span class="uoption" data-value="${academicYearList.year}">${academicYearList.year}</span>
											</c:forEach>
									</div>
									</div>
								</div>
								<!-- e_wrap1_lms_uselectbox -->
								
								<!-- s_wrap1_lms_uselectbox -->
								<div class="wrap1_lms_uselectbox" style="width:90px;">
									<div class="uselectbox">
										<span class="uselected" id="l_seq" data-value=""></span> <span class="uarrow">▼</span>
	
										<div class="uoptions" id="lacaListAdd" style="width:250px;">
										</div>
									</div>
								</div>
								<!-- e_wrap1_lms_uselectbox -->
								
								<!-- s_wrap1_lms_uselectbox -->
								<div class="wrap1_lms_uselectbox" style="width:90px;">
									<div class="uselectbox">
										<span class="uselected" id="m_seq" data-value=""></span> <span class="uarrow">▼</span>
	
										
										<div class="uoptions" id="macaListAdd" style="width:150px;">
										</div>
									</div>
								</div>
								<!-- e_wrap1_lms_uselectbox -->		
	
								<!-- s_wrap1_lms_uselectbox -->
								<div class="wrap1_lms_uselectbox" style="width:90px;">
									<div class="uselectbox">
										<span class="uselected" id="s_seq" data-value=""></span> <span class="uarrow">▼</span>
	
										<div class="uoptions" id="sacaListAdd" style="width:100px;">
										</div>
									</div>
								</div>
								<!-- e_wrap1_lms_uselectbox -->
								
								<!-- s_wrap1_lms_uselectbox -->
								<div class="wrap1_lms_uselectbox" style="width:90px;">
									<div class="uselectbox">
										<span class="uselected" id="aca_seq" data-value=""></span> <span class="uarrow">▼</span>
	
										<div class="uoptions" id="acaListAdd" style="width:100px;">
										</div>
									</div>
								</div>
								<!-- e_wrap1_lms_uselectbox -->
								<button class="btn_sch" style="width:50px;" onClick="getAcademicStageInfo();">Pencarian</button>
							</div>
	
						</div>
	
						<div class="cont_wrap">
							<table class="maintb01" id="academicStageListAdd">
								
							</table>
						</div>
	
					</div>
					<!-- e_div1 -->
	
					<!-- s_div2a -->
					<div class="div2a">
						<div class="ind_tt_wrap0">
							<div class="twrap" onClick="location.href='${HOME}/common/SLife/qna/list'">
								<span class="tt1">1 : 1 문의</span>
								<span class="num" data-name="qnaCnt"></span>
								<span class="tt2">건</span>
							</div>
							<div class="twrap" onClick="location.href='${HOME}/admin/acaSchedule/scheduleManagement'">
								<span class="tt1">이번 주 학사일정</span>
								<span class="num" data-name="acaScheduleCnt"></span>
								<span class="tt2">건</span>
							</div>
						</div>
	
						<div class="ind_tt_wrap">
							<h3 class="tt1">이용자 Pencarian</h3>
	
							<div class="sch_wrap">
								<button class="stbtn on">학생</button>
								<button class="pfbtn">교수</button>
							</div>
						</div>
	
						<div class="con">
							<div class="sbox1">
								<!-- s_학생 Pencarian -->
								<div class="stwrap">
									<input class="tts1" placeholder="학번" type="text" id="st_id"> 
									<input class="tts1" placeholder="학생nama " type="text" id="st_name">
								</div>
								<!-- e_학생 Pencarian -->
								<!-- s_교수 Pencarian -->
								<div class="pfwrap">
									<input class="tts1" placeholder="사번" type="text" id="propessor_id"> 
									<input class="tts1" placeholder="교수nama " type="text" id="pf_name">
								</div>
								<!-- e_교수 Pencarian -->
	
								<div class="wrap3">
									<button class="btn_sch" onClick="searchUser();">Pencarian</button>
								</div>
							</div>
						</div>
					</div>
					<!-- e_div2a -->
	
					<!-- s_div2b -->
					<!-- s_registrasi 권한 class에 regi tambahkan, perbaiki 권한 class에 mdf tambahkan -->
					<div class="div2b regi" id="noticeListAdd">
						<div class="ind_tt_wrap">
							<h3 class="tt1">U-LMS 공지</h3>
	
							<div class="sch_wrap">
								<button class="btn_adm_r1" onClick="location.href='${HOME}/admin/board/notice/create'">registrasi</button>
							</div>
	
						</div>
					</div>
					<!-- e_div2b -->
	
					<!-- s_div3 -->
					<div class="div3">
						<div class="ind_tt_wrap">
							<h3 class="tt1">학습자료실</h3>
	
							<div class="sch_wrap">
								<button class="btn_adm_r1" onClick="location.href='${HOME}/admin/board/learning/create'">학습자료실 registrasi</button>
							</div>
						</div>
						<!-- s_이미지 없을 때 class imgx tambahkan, 이미지 유뮤에 따라 text의 수가 달라지므로 각각 다른 class에 삽입 -->
						<div class="cont_wrap" id="learningBoardListAdd">
						</div>
	
					</div>
					<!-- e_div3 -->
	
					<!-- s_div4 -->
					<div class="div4">
						<div class="ind_tt_wrap">
							<h3 class="tt1">Hot News</h3>
	
							<div class="sch_wrap">
								<button class="btn_adm_r1" onClick="location.href='${HOME}/admin/board/hotnews/create'">Hot News registrasi</button>
							</div>
						</div>
	
						<div class="cont_wrap_box" id="hotNewListAdd">
							
						</div>
	
					</div>
					<!-- e_div4 -->
	
				</div>
				<!-- e_contents -->
			</div>
			<!-- e_container_table -->
	
			<a href="#" id="backtotop" title="To top" class="totop"></a>
			
			<!-- s_footer -->
			<footer id="footer" class="ind">
				<div class="footer_con">           
					<div class="footer_scon">
						<ul class="f_menu">
							<li><a href="#" onclick="javascript:alert('개인정보처리방침'); return false;" title="새창" class="fmn1">개인정보처리방침</a></li>
							<li><a href="#" onclick="javascript:alert('이메일무단수집거부'); return false;" title="새창" class="fmn2"><span>|</span>이메일무단수집거부</a></li>                
						</ul>
						<p class="addr">${ADDRESS }</p>
						<a href="${HOME}" title="홈으로 가기" class="logo_wrap"><img src="${BOTTOM_LOGO}" alt="의과대학 로고" class="b_logo"></a>
					</div>
				</div>
			</footer>
			<!-- e_footer -->
		</div>
		<!-- e_wrap -->
		
		<!-- s_ 팝업 : 공지 안내 -->
		<div id="m_pop1" class="popup_main_notice mo1">
			<!-- s_pop_wrap -->
			<div class="pop_wrap">
				<button class="pop_close close1" type="button" onClick="$('#m_pop1').hide();">X</button>
	
				<p class="t_title">공지 안내</p>
	
				<!-- s_pop_swrap -->
				<div class="pop_swrap">
	
					<div class="swrap" id="noticeContent">
						
					</div>
	
				</div>
				<!-- e_pop_swrap -->
	
				<div class="add_file_wrap">
					<button class="add_file">
						첨부된 파일 <span class="num" data-name="noticeAttachCnt"></span>건<span class="dw"></span>
					</button>
				</div>
	
				<div class="t_dd">
	
					<div class="pop_btn_wrap">
						<button type="button" class="btn02" onclick="" id="noticeMoveBtn">공지사항 게시판 이동</button>
						<button type="button" class="btn01" onClick="$('#m_pop1').hide();">확인</button>
					</div>
				</div>
			</div>
		</div>
		<!-- e_pop_wrap -->
		<!-- e_ 팝업 : 공지 안내 -->
</body>
</html>