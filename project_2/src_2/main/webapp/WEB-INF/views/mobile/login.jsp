<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta property="og:title" content="MLMS">
		<meta property="og:image" content="img/favicon.png">
		<meta property="og:description" content="의과대학 LMS">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="initial-scale=1, width=device-width, minimal-ui">
		<title>MLMS</title>
		<link rel="icon" type="img/png" href="${IMG}/favicon.png">
		<link rel="shortcut icon" href="${IMG}/favicon.png">
		<link rel="icon" href="${IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="img/favicon.ico">
		<link rel="stylesheet" href="${CSS}/css_m1.css" type="text/css">
		<link rel="stylesheet" href="${CSS}/swiper.css">
		
		<script src="${JS}/lib/jquery-1.11.1.js"></script>
		<script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.form.min.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/lib/swiper.js"></script>
		<script src="${JS}/mobileCommon.js"></script>
		
		<script type="text/javascript">
			if ('${overlapLogin}' == 'Y') {
				alert("다른 사용자가 로그인을 하였습니다.");
				location.href = '${M_HOME}/';
			}
		
			$(document).ready(function() {
				initPageTopButton();
				initSwiperContainer();
			});
			
			function loginSubmit() {
			    
				var id = $("#id").val();
				if (isEmpty(id) || isBlank(id)) {
					alert("아이디를 입력해주세요");
					$("#id").focus();
					return;
				}
				
				var pwd = $("#pwd").val();
				if (isEmpty(pwd) || isBlank(pwd)) {
					alert("비밀번호를 입력해주세요");
					$("#pwd").focus();
					return;
				}
			    
			    $.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/login",
			        data: {
			            "id" : id,
			            "pwd" : pwd
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	if (data.user_level == 1 || data.user_level == 2) { //manajemen자, 행정
								location.href = "${HOME}";
			            	} else if (data.user_level == 3) { //교수
				            	location.href = "${M_HOME}/pf/main";
			            	} else if (data.user_level == 4) { //학생
				            	location.href = "${M_HOME}/st/main";
			            	}
			            } else if (data.status == "102") {
			            	alert("사용할 수 없는 계정입니다.");
			            } else {
			                alert("잘못된 ID 또는 비밀번호 입니다.");
			            }
			        },
			        error: function(xhr, textStatus) {
			            //alert("오류가 발생했습니다.");
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
		</script>
	</head>
	
	<body class="index color1">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#login">주 메뉴 바로가기</a> <a href="#login">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
		<!-- s_wrap -->
		<div id="wrap">
			<header class="header">
				<h1 class="logo">
					<a href="${M_HOME}/" title="홈으로 가기"><img src="${MOBILE_TOP_LOGO}" alt="의과대학 로고"></a>
				</h1>
			</header>
			<div id="container" class="container_table">
				<div class="contents index">
					<!-- s_wrap_slide -->
					<div class="wrap_slide swiper-container">
						<div class="swiper-wrapper">
						    <div class="swiper-slide"><img src="${SLIDE_IMG_1}" alt="메인 비주얼 이미지"></div>
						    <div class="swiper-slide"><img src="${SLIDE_IMG_2}" alt="메인 비주얼 이미지"></div>
						    <div class="swiper-slide"><img src="${SLIDE_IMG_3}" alt="메인 비주얼 이미지"></div>
					    </div>
						<!-- Add Pagination -->
						<div class="swiper-pagination"></div>
						<!-- Add Arrows -->
						<div class="swiper-button-next"></div>
						<div class="swiper-button-prev"></div>
					</div>
					<!-- e_wrap_slide -->
	
					<div class="title_wrap">
						<h2 class="title">${UNIVERSITY_NAME }</h2>
					</div>
					<div id="login" class="idpw_wrap">
						<input type="text" id="id" placeholder="학번 / 사번" onkeypress="javascript:if(event.keyCode==13){loginSubmit(); return false;}">
						<input type="password" id="pwd" placeholder="비밀번호" onkeypress="javascript:if(event.keyCode==13){loginSubmit(); return false;}">
						<button type="button" id="login_btn" onclick="loginSubmit();">로그인</button>
					</div>
				</div>
			</div>
			<footer id="footer" class="index">
				<div class="footer_con">
					<a href="${M_HOME}/pcVersion" title="PC버전 홈으로 가기" class="pc_view">PC버전</a>
					<a href="${M_HOME}/" title="홈으로 가기" class="footer_title">${UNIVERSITY_NAME }</a>
				</div>
			</footer>
		</div>
	</body>
</html>