<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<style>
			.mm_ttx{display: table;width: 100%;height: auto;line-height: 18px;font-family:inherit;font-size: 14px;color: rgba(85, 85, 85, 1);text-align: left;text-indent: 0;letter-spacing: -1px;box-sizing:border-box;overflow: visible;text-overflow: ellipsis;margin: 1px auto 0 auto;transition: .7s;cursor:default;border-bottom-right-radius: 0;z-index: 20;box-shadow: inset 1px 1px 1px rgba(255, 252, 185, 1);background: url(../img/mm_bg_long.png) no-repeat 100% 100% rgba(255, 253, 215, 1);}
			.tarea01{display: table;width: 100%;min-height: auto;line-height: 18px;margin: 5px auto 10px auto;padding: 2px 10px 0 10px;text-align:left;word-wrap:break-word;text-indent: 0;letter-spacing: 0px;font-size: 14px;color: rgba(0, 139, 173, 1);overflow: visible;word-break: break-all;box-sizing: border-box;border: solid 1px rgba(255, 253, 216, 0);background: rgba(255, 253, 215, 0);overflow-y: visible;height: auto;cursor:default;}
		</style>
		<script type="text/javascript">
			
			
			$(document).ready(function() {
				getLessonData();
				
				if("${lpPFCheck}" != "Y")
					$("body").addClass("pf");
				
			});
		
			function getLessonData(){
				$.ajax({
	                type: "POST",
	                url: "${M_HOME}/ajax/pf/lessonData/list",
	                data: {                 
	                },
	                dataType: "json",
	                success: function(data, status) {                	
	                	if(data.status == "200"){
	                		if(!isEmpty(data.lessonData)){
	                			if(!isEmpty(data.lessonData.memo))
	                				$("#memoAdd").html('<div class="tarea01 tt_t" name="memo">'+data.lessonData.memo+'</div>');
	                		}
	                		titleSetting(data.lesson_plan_info);
	                		
	                		if(!isEmpty(data.lessonData)){
	                			if(data.lessonData.content == "<p><br></p>"){
		                			$("div[name=lessonContent]").hide();
		                		} else{
									$("div[name=lessonContent]").html(data.lessonData.content);
		                		}
	                		}else{
	                			$("div[name=lessonContent]").hide();
	                		}
	                		
							var htmls = '';
							
							//이미지 width 제한
							$("div[name=lessonContent] img").css("max-width", $(document).width()-40 );
							$("span[name=lessonDataCnt]").text(data.lessonAttachList.length);
							$.each(data.lessonAttachList, function(index){
								htmls = "";
								if(this.attach_type == "I"){
							    	var filename = this.file_name.split("||")[0];
							    	
							    	if(this.file_name.split("||").length != 1){
							    		htmls = '<div class="filewrap">'
											+'<div class="wrap_s dw"  onClick="getFileViewer(\''+this.lesson_attach_seq+'\');">'
											+'<div class="preview1">'
											+'<img src="${RES_PATH}'+this.file_path.split("||")[0]+'" alt="'+filename+'">'
											+'</div>'
											+'<div class="sp_wrap">'
											+'<span class="sp_1">'+this.explan+'</span>' 
											+'<span class="sp_2">사진['+this.file_name.split("||").length+'장]</span>'
											+'</div>'
											+'</div>'
											+'<button class="dw" onClick="multiFileDownload(\'${HOME}\',\''+this.file_path+'\',\''+this.file_name+'\');"></button>'
											+'</div>';					
							    	}else{
							    		htmls = '<div class="filewrap">'
											+'<div class="wrap_s dw"  onClick="getFileViewer(\''+this.lesson_attach_seq+'\');">'
											+'<div class="preview1">'
											+'<img src="${RES_PATH}'+this.file_path+'" alt="'+this.file_name+'">'
											+'</div>'
											+'<div class="sp_wrap">'
											+'<span class="sp_1">'+this.explan+'</span>' 
											+'<span class="sp_2">사진</span>'
											+'</div>'
											+'</div>'
											+'<button class="dw" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');"></button>'
											+'</div>';					
							    	}								
							    }else if(this.attach_type == "A"){
				                    	htmls = '<div class="filewrap">'
				    						+'<div class="wrap_s dw" onClick="getFileViewer(\''+this.lesson_attach_seq+'\');">'
				    						+'<div class="preview1">'
				    						+'<audio width="100" controls="">'
				    						+'<source src="${RES_PATH}'+this.file_path+'" type="audio/mp3">'
				    						+'</audio>'
				    						+'</div>'
				    						+'<div class="sp_wrap">'
				    						+'<span class="sp_1">'+this.explan+'</span>' 
				    						+'<span class="sp_2">오디오</span>'
				    						+'</div>'
				    						+'</div>'
				    						+'<button class="dw" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');"></button>'
				    						+'</div>';				                    	
			                    }else if(this.attach_type == "V"){
				                    	htmls = '<div class="filewrap">'
				    						+'<div class="wrap_s dw" onClick="getFileViewer(\''+this.lesson_attach_seq+'\');">'
				    						+'<div class="preview1">'
				    						+'<video width="100" controls="">'
				    						+'<source src="${RES_PATH}'+this.file_path+'" type="video/mp4">'
				    						+'</video>'
				    						+'</div>'
				    						+'<div class="sp_wrap">'
				    						+'<span class="sp_1">'+this.explan+'</span>' 
				    						+'<span class="sp_2">동영상</span>'
				    						+'</div>'
				    						+'</div>'
				    						+'<button class="dw" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');"></button>'
				    						+'</div>';								        
			                    }else if(this.attach_type=="Y"){	
			                    	htmls = '<div class="filewrap">'
			        					+'<div class="wrap_s dw" onClick="getFileViewer(\''+this.lesson_attach_seq+'\');">'
			        					+'<div class="preview1">'
			        					+'<div class="video_container">'
			        					+'<iframe src="'+this.youtube_url+'" allowfullscreen></iframe>'
			        					+'</div>'
			        					+'</div>'
			        					+'<div class="sp_wrap">'
			        					+'<span class="sp_1">'+this.explan+'</span>' 
			        					+'<span class="sp_2">유튜브</span>'
			        					+'</div>'
			        					+'</div>'
			        					+'</div>';
							    }else{
				                    	htmls='<div class="wrap_s dw">'
				        					+'<div class="sp_wrap">'
				        					+'<span class="sp_1" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');">'+this.file_name+'</span>'
				        					+'</div>'
				        					+'<button class="dw" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');"></button>'
				        					+'</div>';
			                    }
								$("#fileListAdd").append(htmls);
							});
	                	}
	                },
	                error: function(xhr, textStatus) {
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                }
	            }); 
			}
			
			function titleSetting(lessonPlanInfo) {
				var periodStr = lessonPlanInfo.period; // pengajar
				var periodList = periodStr.split(',');
				var startPeriod = periodList[0];
				if (periodList.length > 1) {
					periodStr = startPeriod + "~" + periodList[periodList.length-1] + " pengajar";  
				} else {
					periodStr = startPeriod + " pengajar";
				}
				
            	//수업일
            	var lessonDate = lessonPlanInfo.lesson_date;
            	lessonDate = lessonDate.replace("-", "월 ") + "일";
				
            	//수업waktu
				var ampmText = lessonPlanInfo.ampm;
        		if(ampmText == "pm"){
        			ampmText = "오후";
        		} else {
        			ampmText = "오전";
        		}
        		
        		var startTime = lessonPlanInfo.start_time;
				var endTime = lessonPlanInfo.end_time;
            	
        		var lessonPlanDateTitle = lessonDate + " " + periodStr;//mm월 xx일 x pengajar
        		var lessonPlanTimeTitle = ampmText + " " + startTime + "~" + endTime;//오전/오후 xx:xx~xx:xx
        		
            	var titleHtml = '<span class="cc_s1">' + lessonPlanDateTitle + '</span><span class="cc_s2">(' + lessonPlanTimeTitle + ')</span>';
            	var titleDiv = $("div[name='titleDiv']").addClass("cc").html(titleHtml);
            	
            	//교육과정 명
            	var currName = lessonPlanInfo.curr_name;
            	
            	//수업계획서 명
            	var lessonSubject = lessonPlanInfo.lesson_subject;
            	
            	$("#currName").html(currName);
            	$("#lessonSubject").html(lessonSubject);
			}
			
			function getFormationEvaluationListView() {
				post_to_url("${M_HOME}/pf/formationEvaluation/list", {
					"lp_seq" : "${sessionScope.S_LP_SEQ}"
				});
			}
		
			function getAttendanceListView() {
				post_to_url("${M_HOME}/pf/attendance/list", {"lp_seq" : "${sessionScope.S_LP_SEQ}", "curr_seq" : "${sessionScope.S_CURRICULUM_SEQ}"});
			}
		
			function getLessonPlanView() {
				post_to_url("${M_HOME}/pf/lessonPlan", { "lp_seq" : "${sessionScope.S_LP_SEQ}", "curr_seq" : "${sessionScope.S_CURRICULUM_SEQ}" });
			}
			
			function getCurriculumView(){
				post_to_url("${M_HOME}/pf/curriculum", {"curr_seq" : "${sessionScope.S_CURRICULUM_SEQ}"});
			}

			//뷰어 페이지로 이동한다...
			function getFileViewer(lesson_attach_seq) {
				post_to_url("${M_HOME}/pf/lessonData/viewer", {
					"lesson_attach_seq" : lesson_attach_seq
				});
			}
			
			
  		</script>
	</head>
	<body class="color1">
		<!-- s_contents -->
		<div class="contents">
	
			<!-- s_top_tt-->
			<div class="top_tt">				
				<span class="cc_t1" id="currName"></span>
				<span class="cc_t2" id="lessonSubject"></span>
			</div>
			<!-- e_top_tt -->
	
			<!-- s_mn_wrap -->
			<div class="mn_wrap rfr">
				<div class="wrap_s">
					<span class="cc_mn cc_mn01" onClick="javascript:getCurriculumView();">교육과정</span>
					<span class="cc_mn cc_mn02" onClick="javascript:getLessonPlanView();">수업계획</span>
					<span class="cc_mn cc_mn03 on" onClick="location.href='${M_HOME}/pf/lessonData'">자료</span>
					<!-- 수업교수인지 체크 -->
					<c:if test='${lpPFCheck == "Y" }'>
						<span class="cc_mn cc_mn04" onClick="javascript:getFormationEvaluationListView();">형성penilaian</span>
					</c:if>
					<span class="cc_mn cc_mn05" onClick="location.href='${M_HOME}/pf/assignMent';">과제</span>
					<span class="cc_mn cc_mn06" onClick="javascript:getAttendanceListView();">absensi kehadiran</span>
				</div>
			</div>
			<!-- e_mn_wrap -->
	
			<!-- s_rfr_wrap -->
		<div class="rfrform_wrap">
			<div class="mm_ttx dis" id="memoAdd">
				<div class="tarea01 tt_t" name="memo"></div>
			</div>
			
			<div class="con" name="lessonContent"></div>

			<div class="sch_wrap5">
				<div class="wrap">
					<span class="swrap"> 
						<span>총</span>
						<span class="num" name="lessonDataCnt"></span>
						<span class="numt">건</span><span>의 자료 업로드</span>
					</span>
				</div>
			</div>

			<div class="box_s" id="fileListAdd">
			
			</div>

		</div>

	</div>
			<!-- e_rfr_wrap -->
		<!-- e_contents -->
	</body>
</html>
