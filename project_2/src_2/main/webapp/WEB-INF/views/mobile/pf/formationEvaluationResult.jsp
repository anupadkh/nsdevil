<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta property="og:title" content="MLMS">
		<meta property="og:image" content="${M_IMG}/favicon.png">
		<meta property="og:description" content="의과대학 LMS">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no, maximum-scale=1, width=device-width" />
		<title>MLMS</title>
		
		<link rel="icon" type="${M_IMG}/png" href="${M_IMG}/favicon.png">
		<link rel="shortcut icon" href="${M_IMG}/favicon.png">
		<link rel="icon" href="${M_IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${M_IMG}/favicon.ico">
		<link rel="stylesheet" href="${CSS}/css_m1.css" type="text/css">
		<link rel="stylesheet" href="${CSS}/swiper.css">
		<link rel="stylesheet" href="${CSS}/dev_css_m1.css">
		<script src="${JS}/lib/jquery-1.11.1.js"></script>
		<script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.form.min.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/lib/swiper.js"></script>
		<script src="${JS}/mobileCommon.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				getFormationEvaluationResult('N');
			});
			
			function getFormationEvaluationResult(absenceFlag) {
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/pf/formationEvaluation/quiz/result",
			        data: {
			        	"fe_seq" : "${fe_seq}"
			        	, "absence_flag" : absenceFlag
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	var resultInfo = data.fe_result_info;
			            	var resultList = data.fe_result_list;
			            	resultInfoSetting(resultInfo);
			            	resultListSetting(resultList, absenceFlag);
			            } else {
			            	alert("형성penilaian 결과 불러오기에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        timeout: 60000,
			        error: function(xhr, textStatus) {
			        	errorCase(textStatus);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function resultInfoSetting(resultInfo) {
				var examnieeCntY = resultInfo.examinee_cnt_y;//결시자
				var examnieeCntN = resultInfo.examinee_cnt_n;//응시자
				var feName = resultInfo.fe_name;
				var maxScore = resultInfo.max_score;
				var minScore = resultInfo.min_score;
				var avgScore = resultInfo.avg_score;

				$("#examnieeCntN").html(examnieeCntN);
				$("#examnieeCntY").html(examnieeCntY);
				$("#feName").html(feName);
				$("#maxScore").html(maxScore);
				$("#minScore").html(minScore);
				$("#avgScore").html(avgScore);
			}
			
			function resultListSetting(resultList, flag) {
				var resultHtml = "";
				$(resultList).each(function() {
					var id = this.id;
					var name = this.name;
					var picturePath = this.picture_path;
					var feScore = this.fe_score;
					if (isEmpty(picturePath)) {
						picturePath = "${IMG}/ph_3.png";
					}else{
						picturePath = "${RES_PATH}"+this.picture_path;
            		}
					
					resultHtml += '<div class="a_mp">';
					resultHtml += '	<span class="pt01"><img src="' + picturePath + '" alt="사진" class="pt_img"></span>';
					resultHtml += '	<span class="ssp1">' + name + ' (' + id + ')</span>';
					resultHtml += '	<span class="sc">' + feScore + '</span>';
					resultHtml += '</div>';
				});
				$("#resultListArea").html(resultHtml);
			}
			
			function changeTab(t, flag) {
				$(t).parent().find("div").removeClass("on");
				$(t).addClass("on");
				if (flag) {
					getFormationEvaluationResult('N');
				} else {
					getFormationEvaluationResult('Y');
				}
			}
			
			function getFormationEvaluationListView() {
				post_to_url("${M_HOME}/pf/formationEvaluation/list", {"lp_seq":"${sessionScope.S_LP_SEQ}"});
			}
		</script>
	</head>
	<body class="color1 bd_quiz">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
		<div id="skipnav">
			<a href="#gnb_q">주 메뉴 바로가기</a>
			<a href="#container">컨텐츠 바로가기</a>
		</div>
		<div id="wrap">
			<header class="header"> 
				<div class="wrap">
					<c:choose>
						<c:when test='${sessionScope.S_USER_PICTURE_PATH eq ""}'>
							<div class="icpt"><img src="${DEFAULT_PICTURE_IMG}" alt="사진 이미지"></div>
						</c:when>
						<c:otherwise>
							<div class="icpt"><img src="${sessionScope.S_USER_PICTURE_PATH}" alt="사진 이미지"></div>
						</c:otherwise>
					</c:choose>
					<div class="top_date quiz">
						<span class="sp_q1 qend" id="feName"></span>
					</div>
				</div>
			</header>
			<div id="container" class="container_table">
				<div class="contents">   
					<div class="ptb_wrap">	
						<div class="pr_quiz end">
							<div class="b_wrap">
								<div id="gnb_q" class="tt_wrap2">
									<div class="wrap_s1">
										<div class="wrap_ss">
											<div class="tt_a on" onclick="javascript:changeTab(this, true);"><span class="sp_t">응시</span><span class="sp_n" id="examnieeCntY">0</span><span class="sp_un">명</span></div>
											<div class="tt_b" onclick="javascript:changeTab(this, false);"><span class="sp_t">결시</span><span class="sp_n" id="examnieeCntN">0</span><span class="sp_un">명</span></div>
										</div>
									</div>
								</div>
								<div class="quiz_wrap_sc">
									<div class="wrap_s">
										<span class="quiz_sc"><span>최고점</span><span class="sp1" id="maxScore">0</span></span>
										<span class="quiz_sc"><span>최저점</span><span class="sp2" id="minScore">0</span></span>
										<span class="quiz_sc"><span>평균</span><span class="sp3" id="avgScore">0</span></span>
									</div>
								</div>
								<div class="pht">
									<div class="con_wrap">
										<div class="con_s2" id="resultListArea"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<a href="#" id="backtotop" title="To top" class="totop"></a>
			<div class="go_wrap">                   
				<div class="pop_btn_wrap">
					<button type="button" class="btn01" onclick="javascript:getFormationEvaluationListView();">닫기</button>           
				</div>
			</div>
		</div>
	</body>
</html>