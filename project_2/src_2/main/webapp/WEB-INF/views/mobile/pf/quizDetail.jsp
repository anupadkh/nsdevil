<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta property="og:title" content="MLMS">
		<meta property="og:image" content="${M_IMG}/favicon.png">
		<meta property="og:description" content="의과대학 LMS">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no, maximum-scale=1, width=device-width" />
		<title>MLMS</title>
		
		<link rel="icon" type="${M_IMG}/png" href="${M_IMG}/favicon.png">
		<link rel="shortcut icon" href="${M_IMG}/favicon.png">
		<link rel="icon" href="${M_IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${M_IMG}/favicon.ico">
		<link rel="stylesheet" href="${CSS}/css_m1.css" type="text/css">
		<link rel="stylesheet" href="${CSS}/swiper.css">
		<link rel="stylesheet" href="${CSS}/dev_css_m1.css">
		<script src="${JS}/lib/jquery-1.11.1.js"></script>
		<script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.form.min.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/lib/swiper.js"></script>
		<script src="${JS}/mobileCommon.js"></script>
		
		<script type="text/javascript">
		
			$(document).ready(function() {
				getQuizDetail();
			});
			function getQuizDetail() {
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/pf/formationEvaluation/quiz/detail",
			        data: {
			        	"fe_seq" : "${fe_seq}"
			        	, "quiz_seq" : "${quiz_seq}"
			        	, "curr_quiz_seq" : "${curr_quiz_seq}" //다음퀴즈 눌렀을때 현재 퀴즈 시퀀스
			        	, "quiz_state" : "${quiz_state}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	var quizInfo = data.quiz_info;
			            	var preQuizInfo = data.pre_quiz_info;
			            	var nextQuizInfo = data.next_quiz_info;
			            	var answerList = data.quiz_answer_list;
			            	quizInfoSetting(quizInfo);
			            	answerListSetting(answerList);
			            	preQuizSetting(preQuizInfo);
			            	nextQuizSetting(nextQuizInfo);
			            } else {
			            	alert("형성penilaian 불러오기에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        timeout: 60000,
			        error: function(xhr, textStatus) {
			        	errorCase(textStatus);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function quizInfoSetting(quizInfo) {
				var feName = quizInfo.fe_name; //수업계획서 제목
				var totalExamineeCnt = quizInfo.total_examinee_cnt; //Keseluruhan학생수
				var examineeCntY = quizInfo.examinee_cnt_y; //응시수
				var examineeCntN = quizInfo.examinee_cnt_n; //결시수
				var submitCntY = quizInfo.submit_cnt; //제출수
				if (examineeCntY == 0) {
					examineeCntY = totalExamineeCnt;
				}
				var question = quizInfo.quiz_question; //퀴즈 문항줄기
				var quizOrder = quizInfo.quiz_order; //퀴즈 번호
				var previewImgPath = quizInfo.file_path;
				var rightAnswerCnt = quizInfo.quiz_right_answer_cnt;
				$("#rightAnswerCnt").html(rightAnswerCnt);
				$("#wrongAnswerCnt").html(submitCntY-rightAnswerCnt);

				$("#feName").html(feName);
				$(".examineeCntY").html(examineeCntY);
				$(".examineeCntN").html(examineeCntN);
				$(".submitCntY").html(submitCntY);
				$(".submitCntN").html(examineeCntY - submitCntY);
				$("#quizQuestion").html(question);
				$("#quizOrder").html(quizOrder);
				if (previewImgPath != "") {
					$("#previewArea").removeClass("off");
				}
				$("#previewArea").html('<img class="m_preview" src="${RES_PATH}' + previewImgPath + '" alt="형성penilaian 이미지 미리보기">');
				
				if (submitCntY == examineeCntY) {
					var target = $("div.wrap_ss1");
					target.find(".unsubmission-span").remove();
					target.removeClass("wrap_ss1");
					target.addClass("wrap_ss2");
					target.find(".submission-text").html("전원 제출 완료");
				}
			}
			
			function answerListSetting(answerList) {
				var answerListHtml = "";
				var submitCnt = $(".submitCntY:eq(0)").text();
				
				$(answerList).each(function() {
					var answerOrder = this.answer_order;
					var answerFlag = this.answer_flag;
					var selectCnt = this.select_cnt;
					var content = this.content;
					var onClass = "";
					var progression = 0;
					
					if (answerFlag == 'Y') {
						onClass = "on";
					}

					if (selectCnt > 0) {
						// 답가지 pilih 백분률 값 = 제출한  수 / 해당 답가지를 pilih 한 수 * 100
						progression = Math.floor(selectCnt/submitCnt*100) || 0;
					}
					
					answerListHtml += '<tr class="n1">';
					answerListHtml += '	<td class="w1" name="' + onClass + '"><span class="num_box">' + answerOrder + '</span></td>';
					answerListHtml += '	<td class="w3_2">' + content + '</td>';
					answerListHtml += '</tr>';
					answerListHtml += '<tr class="n1 prg" style="display:none;">';
					answerListHtml += '	<td class="w1"><div class="wrap_s3"><span class="num">' + selectCnt + '</span><span class="sign">명</span></div></td>';
					answerListHtml += '	<td class="w3_2">';
					answerListHtml += '		<div class="prg"><span class="bar" style="width:' + (progression*2) + 'px;"></span></div>';// s_ProgressBar width 200px 백분률로 변환하여 값 삽입
					answerListHtml += '		<span class="num">' + progression + '</span><span class="sign">%</span>';
					answerListHtml += '	</td>';
					answerListHtml += '</tr>';
				});
				
				$("#answerListArea").html(answerListHtml);
			}
			
			function preQuizSetting(preQuizInfo) {
				if (preQuizInfo != null) {
					var quizState = preQuizInfo.quiz_state;
					var quizSeq = preQuizInfo.quiz_seq;
					$("#preQuizBtn").attr("onclick", "javascript:preQuiz(" + quizSeq + ", '" + quizState + "');");
				} else {
					$("#preQuizBtn").remove();
				}
			}
			
			function nextQuizSetting(nextQuizInfo) {
				if (nextQuizInfo != null) {
					var quizState = nextQuizInfo.quiz_state;
					var quizSeq = nextQuizInfo.quiz_seq;
					$("#nextQuizBtn").attr("onclick", "javascript:nextQuiz(${quiz_seq}, " + quizSeq + ", '" + quizState + "');");
				} else {
					$("#nextQuizBtn").attr("onclick", "javascript:nextQuiz(0, 0, 0);").html("형성penilaian종료").addClass("end");
				}
			}
			
			function preQuiz(quizSeq, quizState) {
				post_to_url("${M_HOME}/pf/formationEvaluation/quiz/detail", {"quiz_seq":quizSeq, "fe_seq":"${fe_seq}"});
			}
			
			function nextQuiz(currQuizSeq, quizSeq, quizState) {
				if (quizSeq != 0) {
					if (quizState == "WAIT") { //다음문제가 대기
						if ($(".submitCntN").html() > 0) { //미제출자가 있을경우
							if(!confirm("답안을 제출하지 않은 학생이 있습니다. 그대로 다음 문제로 이동하시겠습니까?\n(다음 문제로 이동 시, 미제출 학생은 다시 응시할 수 없습니다.)")) {
								return false;
							}
						}
					}
					post_to_url("${M_HOME}/pf/formationEvaluation/quiz/detail", {"curr_quiz_seq":currQuizSeq,"quiz_seq":quizSeq, "fe_seq":"${fe_seq}", "quiz_state":quizState});
				} else {
					//형성penilaian종료
					$.ajax({
				        type: "POST",
				        url: "${M_HOME}/ajax/pf/formationEvaluation/end",
				        data: {
				        	"fe_seq" : "${fe_seq}"
				        },
				        dataType: "json",
				        success: function(data, status) {
				            if (data.status == "200") {
				            	alert("형성penilaian가 종료 되었습니다. 형성penilaian 결과 화면으로 이동합니다.");
				            	post_to_url("${M_HOME}/pf/formationEvaluation/quiz/result", {"fe_seq":"${fe_seq}"});
				            } else {
				            	alert("형성penilaian가 정상적으로 종료되지 않았습니다. [" + data.status + "]");
				            	return false;
				            }
				        },
				        timeout: 60000,
				        error: function(xhr, textStatus) {
				        	errorCase(textStatus);
				        },
				        beforeSend:function() {
				            $.blockUI();
				        },
				        complete:function() {
				            $.unblockUI();
				        }
				    });
				}
			}
			
			function unsubmissionPopupOpen() {
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/pf/formationEvaluation/quiz/unsubmission/list",
			        data: {
			        	"quiz_seq" : "${quiz_seq}",
			        	"fe_seq" : "${fe_seq}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	var unsubmissionList = data.unsubmission_list;
			            	if (unsubmissionList.length < 1) {
			            		alert("미제출자가 없습니다.");
			            		return false;
			            	}
			            	var unsubmissionListHtml = "";
			            	$(unsubmissionList).each(function() {
			            		var stName = this.name;
			            		var stId = this.id;
			            		var stPicturePath = this.picture_path;
			            		if (isEmpty(stPicturePath)) {
			            			stPicturePath = "${DEFAULT_PICTURE_IMG}";
			            		}else{
			            			stPicturePath = "${RES_PATH}"+this.picture_path;
			            		}
			            		unsubmissionListHtml += '<div class="a_mp"><span class="pt01"><img src="' + stPicturePath + '" alt="사진" class="pt_img"></span><span class="ssp1">' + stName + ' ( ' + stId + ' )</span></div>';
			            	});
			            	
			            	$("#unsubmissionPopup #unsubmissionList").html(unsubmissionListHtml);
			            	$("#unsubmissionPopup #submitCntN").html(unsubmissionList.length);
			            	$("#unsubmissionPopup").show();
			            } else {
			            	alert("형성penilaian 불러오기에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        timeout: 60000,
			        error: function(xhr, textStatus) {
			        	errorCase(textStatus);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function showAnswer(obj){
				if(!$(obj).hasClass("on")){
					$(obj).addClass("on");
					$("td[name=on]").addClass("on");
					$("tr.prg").show();
				}else{
					$(obj).removeClass("on");
					$("td[name=on]").removeClass("on");
					$("tr.prg").hide();
				}
			}
			
			function unsubmissionPopupClose() {
				$("#unsubmissionPopup").hide();
			}
			
			function getFormationEvaluationListView() {
				post_to_url("${M_HOME}/pf/formationEvaluation/list", {"lp_seq":"${sessionScope.S_LP_SEQ}"});
			}
		</script>
	</head>
	<body class="color1 bd_quiz">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
		    <p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
		<div id="skipnav">
			<a href="#gnb_q">주 메뉴 바로가기</a>
			<a href="#container">컨텐츠 바로가기</a>
		</div>
		<div id="wrap">
			<header class="header"> 
				<div class="wrap">
					<c:choose>
						<c:when test='${sessionScope.S_USER_PICTURE_PATH eq ""}'>
							<div class="icpt"><img src="${DEFAULT_PICTURE_IMG}" alt="사진 이미지"></div>
						</c:when>
						<c:otherwise>
							<div class="icpt"><img src="${sessionScope.S_USER_PICTURE_PATH}" alt="사진 이미지"></div>
						</c:otherwise>
					</c:choose>
					
					<div class="top_date quiz">
						<span class="sp_q1" id="feName"></span>
					</div>
					<div class="wrap_goquiz"><div class="goquiz" title="형성penilaian 닫기" onclick="javascript:getFormationEvaluationListView();"></div></div>
				</div>
			</header>
			<div id="container" class="container_table">
				<div class="contents">   
					<div class="ptb_wrap">	
						<div class="pr_quiz">
							<button class="renew_wrap" onclick="javascript:getQuizDetail();"><span class="renew">새로고침</span></button>
							<button class="result_wrap" onClick="javascript:showAnswer(this);"><span class="result">결과확인</span></button>
							<div class="b_wrap">
								<div id="gnb_q" class="tt_wrap2">
									<div class="wrap_s1">
										<div class="wrap_ss">
											<div class="tt"><span class="sp_t">응시</span><span class="sp_n examineeCntY">0</span><span class="sp_un">명</span></div>
		                                    <div class="tt"><span class="sp_t">결시</span><span class="sp_n_1 examineeCntN">0</span><span class="sp_un">명</span></div>
										</div>
											
										<div class="wrap_ss ">
											<div class="tt tt_1"><span class="sp_t">답안</span><span class="sp_n submitCntY">0</span><span class="sp_un">명 제출함</span></div>
										</div>
										<div class="wrap_ss wrap_ss1">
		                                    <div class="tt"><span class="sp_n"><span class="an submitCntY">0</span><span class="sign">/</span><span class="examineeCntY">0</span></span><span class="sp_n nn open1 submitCntN unsubmission-span" onclick="javascript:unsubmissionPopupOpen();">5</span><span class="sp_un submission-text">명 제출하지 않음</span></div>
										</div>
									</div>
									<div class="wrap_s2">
										<div class="tt tt1"><span class="sp_n" id="rightAnswerCnt">0</span><span class="sp_t">O</span></div>
		                                <div class="tt tt2"><span class="sp_n" id="wrongAnswerCnt">0</span><span class="sp_t">X</span></div>
		                                <!-- <div class="tt tt2">
		                                	<input type="checkbox" onClick="showAnswer(this);" style="float: left;width: 25px;height: 25px;">
		                                	<span class="sp_un submission-text" style="padding: 0px 0px;">정답<br>표시</span>
		                                </div> -->
									</div>
								</div>
								<div class="pop_table">  
									<table class="pop_ipwrap1">
										<tbody>
											<tr>
												<td class="ip_tt"><span id="quizOrder">1</span></td>
												<td class="ip_output"><div id="quizQuestion"></div></td>
											</tr>          
										</tbody>
									</table>
								</div>
								<div class="m_preview_wrap off" id="previewArea"></div>
								<div class="m_q_tb_wrap1">
									<table class="m_q_tb1">
										<tbody id="answerListArea">
										</tbody>
									</table>  
								</div>	
							</div>
						</div>
					</div>	
				</div>
			</div>
			<a href="#" id="backtotop" title="To top" class="totop" ></a>
			<div class="go_wrap">                   
				<div class="pop_btn_wrap">
					<button class="bgo" id="preQuizBtn">이전 문제</button>
					<button class="fgo" id="nextQuizBtn">다음 문제</button>            
				</div>
			</div>
		</div>
		
		<!-- s_미제출자 -->	
		<div id="unsubmissionPopup" class="pop_up_sbmx mo1">
			<div class="pop_wrap">
				<p class="t_title"><span class="sp_un">미제출</span><span class="sp_nn" id="submitCntN">0</span><span class="sp_un">명</span></p>
				<button class="renew_wrap" onclick="javascript:unsubmissionPopupOpen();"><span class="renew"></span></button>
				<div class="table_b_wrap">
					<div class="pht">
						<div class="con_wrap">
							<div class="con_s2" id="unsubmissionList"></div>
						</div>
					</div>
				</div>   
				<div class="t_dd">
					<div class="pop_btn_wrap2">
						<button type="button" class="btn01 close1" onclick="javascript:unsubmissionPopupClose();">닫기</button>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>