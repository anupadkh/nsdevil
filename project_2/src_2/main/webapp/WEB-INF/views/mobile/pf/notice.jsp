<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			var page_num = 1;
			$(document).ready(function(){
				$("div[name=titleDiv]").html('<span class="list">알림</span>');		
				getMobileCurrLessonPlan(page_num);
			});
			$(window).scroll(function(){ 
				if($(window).scrollTop() == $(document).height() - $(window).height()){
					page_num++;
					getMobileCurrLessonPlan(page_num);						
				}
			});
			function getMobileCurrLessonPlan(){
				var url = "${HOME}/mobile/ajax/pf/notice/list";
			
				$.ajax({
			        type: "POST",
			        url: url,
			        data: {
			        	"page" : page_num
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	var htmls = "";
			        	
			            if (data.status == "200") {
			            	if(data.noticeList.length == 0){
			            		page_num--;
			            	}else{
				            	$.each(data.noticeList, function(index){
				            		var type = this.notice_type;
				            		var type_name = "";
				            		var type_title = "";
				            		var type_html = "";
				            		//tr 클래스 숫자 바꿔준다.
				            		var type_class = "";
				            		if(type=="board"){
				            			type_name = "공지";
				            			type_class = "1";
				            			type_html = '<span class="sp_1">'+this.sub_title+'</span>';
				            		}else if(type=="asgmtDeadline"){
				            			type_name = "과제마감";
				            			type_class = "2";
				            			if(this.state == 0)
				            				type_title = "「"+this.sub_title+"」 과제 제출 마감일 입니다.";
				            			else if(this.state > 0)
				            				type_title = "「"+this.sub_title+"」 과제가 제출 마감 되었습니다.";
				            			else
				            				type_title = "「"+this.sub_title+"」 과제가 제출 마감 "+Math.abs(this.state)+"일 전입니다.";
				            			type_html = '<span class="sp_1">'+type_title+'</span>';
				            		}else if(type=="asgmtAllSubmit"){
				            			type_name = "과제전원제출";
				            			type_class = "3";
				            			type_title = "「"+this.sub_title+"」 과제가 전원제출 완료 되었습니다.";
				            			type_html = '<span class="sp_1"><span class="tt_wrap1">'+this.sub_title+'</span><span class="tt_wrap2">'+type_title+'</span></span>';
				            		}else if(type=="memo"){
				            			type_name = "개인일정";
				            			type_class = "4";
				            			type_html = '<span class="sp_3">'+this.real_date+' '+this.start_time+'~'+this.end_time+'</span><span class="sp_2">'+this.full_title;
				            			
				            			//개인일정 장소 있으면
				            			if(!isEmpty(this.sub_title))
				            				type_html += '<br>장소 : ' + this.sub_title;
				            				
			            				type_html += '</span>';
				            		}else if(type=="UnlessonPlan"){
				            			type_name = "수업계획서 미registrasi";
				            			type_class = "5";
				            			type_title = "「"+this.full_title+"」수업계획서 미registrasi 상태입니다.";
				            			type_html = '<span class="sp_3">'+this.real_date+' '+this.start_time+'~'+this.end_time+'</span><span class="sp_2">'+type_title+'</span>';
				            		}else if(type=="asgmtNotSubmit"){
				            			type_name = "과제 미제출";
				            			type_class = "5";
				            			type_title = "「"+this.sub_title+"」 과제 미제출 상태입니다.";
				            			type_html = '<span class="sp_3">'+this.real_date+'</span><span class="sp_2">'+type_title+'</span>';
				            		}else if(type=="qna"){
				            			type_name = "1:1질의응답";
				            			type_class = "2";
				            			type_title = "에게 1:1질의를 받으셨습니다.";
				            			
				            			//end_time = 유저 레벨, sub_title = 유저 이름
				            			if(this.end_time == "4")
				            				type_title = " 학생 " + type_title;
				            			else if(this.end_time == "2" || this.end_time == "3")
				            				type_title = " 교수 " + type_title;
				            			
				            			type_html = '<span class="sp_3">'+this.real_date+'</span><span class="sp_2">'+this.sub_title+type_title+'</span>';
				            		}else if(type=="qnaAnswer"){
				            			type_name = "1:1질의응답 답변";
				            			type_class = "2";
				            			type_title = "에게 1:1질의 답변을 받으셨습니다.";
				            			
				            			//end_time = 유저 레벨, sub_title = 유저 이름
				            			if(this.end_time == "4")
				            				type_title = " 학생 " + type_title;
				            			else if(this.end_time == "2" || this.end_time == "3")
				            				type_title = " 교수 " + type_title;
				            			
				            			type_html = '<span class="sp_3">'+this.real_date+'</span><span class="sp_2">'+this.sub_title+type_title+'</span>';
				            		}
	
				            		switch(this.day){
					            		case 0 : dayOfWeek="일";break;
					            		case 1 : dayOfWeek="월";break;
					            		case 2 : dayOfWeek="화";break;
					            		case 3 : dayOfWeek="수";break;
					            		case 4 : dayOfWeek="목";break;
					            		case 5 : dayOfWeek="금";break;
					            		case 6 : dayOfWeek="토";break;
				            		}
				            					            					        			
									htmls ='<tr class="box_wrap tt'+type_class+'">';
									htmls +='<td class="box_s w1">';
									htmls +='<div class="wrap_s">';
									htmls +='</div>';
									htmls +='</td>';
									htmls +='<td class="box_s w2">';
									htmls +='<div class="wrap_s s2">';
									htmls +='<div class="wrap_ss">';
									htmls +='<span class="tt">'+type_name+'</span>';
									htmls +='<span class="tm tm1">'+this.date+'('+dayOfWeek+')</span>';
									htmls +='</div>';										
									htmls +='<div class="wrap_ss">';
									htmls += type_html;
									htmls +='</div>';
									htmls +='</div>';
									htmls +='</td>';
									htmls +='</tr>';
					            	$("#noticeListAdd").append(htmls);
				            	});
			            	}
			            } else {
			                alert("알림 리스트 가져오기 실패.");
			            }
			        },
			        error: function(xhr, textStatus) {
			            //alert("오류가 발생했습니다.");
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
		</script>
	</head>
	<body>
	
		<!-- s_contents -->
		<div class="contents">	
			<!-- s_ptb_wrap -->
			<div class="ptb_wrap">
				<!-- s_tb_nt1 -->
				<table class="tb_nt1" id="noticeListAdd">
	
				</table>
				<!-- e_tb_nt1 -->
			</div>
			<!-- e_ptb_wrap -->
	
		</div>
		<!-- e_contents -->
	</body>
</html>