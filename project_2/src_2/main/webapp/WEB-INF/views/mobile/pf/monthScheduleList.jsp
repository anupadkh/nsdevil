<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script src="${JS}/lib/mobile/fullcalendar-v3.6.2/js/moment.min.js"></script>
		<script src="${JS}/lib/mobile/fullcalendar-v3.6.2/js/fullcalendar.js"></script>
		<script src="${JS}/lib/mobile/fullcalendar-v3.6.2/js/locale-all.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$("body").addClass("bd_mm")
				$('#calendar1').fullCalendar({
					header: {
						left: 'prev,next today',
						center: 'title'
					},
					navLinks: true,
					locale: "ko",
					viewRender: function (view, element) {
					    var date = $('#calendar1').fullCalendar('getDate');
					    var dataDate = date.format('L').toString();
					    dataDate = dataDate.replace(/\./g,"-");
					    getScheduleList(dataDate);
					 },
					dayClick: function(date, allDay, jsEvent, view) {
					   	var year = date.format("YYYY");
					   	var month = date.format("MM");
					   	var day = date.format("DD");
					   	var dataDate = year + "-" + month + "-" + day;
					   	getScheduleList(dataDate);
				     }
				});
				
				$("#dtBox").DateTimePicker({
					dateFormat: "MM-dd-yyyy",
					timeFormat: "HH:mm",
					dateTimeFormat: "MM-dd-yyyy HH:mm:ss AA",
					addEventHandlers: function() {
						this.setDateTimeStringInInputField($("input"));
					}
				});
				
				$(document).on("change", "#date2", function(){
					if(isEmpty($(this).val()))
						return false;
					var currentDate = $(this).val();
					var date = new Date(currentDate);
		 			var year = date.getFullYear();
				   	var month = date.getMonth() + 1;
				   	var day = date.getDate();
				   	var dataDate = year + "-" + month + "-" + day;
				   	$("#calendar1").find(".fc-center h2").html(year+". "+month+".");
				   	$("#calendar1").fullCalendar('gotoDate', currentDate);
				    getScheduleList(dataDate);
				});
				
			});
			
			function getScheduleList(searchDate) {
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/pf/schedule/month/list",
			        data: {
			        	"search_date" : searchDate
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	var calendarList = data.calendar_schedule_list;
			            	var lessonPlanList = data.lesson_plan_list;
			            	calendarSetting(calendarList);
			            	lessonPlanSetting(lessonPlanList);
			            } else {
			            	alert("월간 수업계획서 목록을 불러오지 못하였습니다. [" + data.status + "]");
			            }
			            $("#calendar1 .fc-view-container table .fc-content-skeleton td").find("div").removeClass("on");
						$(".fc-content-skeleton td[data-date='" + searchDate + "']").find("div").addClass("on");
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			
			function calendarSetting(list) {
				$(list).each(function() {
					var targetDate = this.schedule_date;
					$(".fc-content-skeleton td[data-date='" + targetDate + "']").find("div").addClass("exist-schd");
				});
			}
			
			function lessonPlanSetting(list) {
				var lessonPlanListHtml = "";
				$(list).each(function() {
					var lpSeq = this.lp_seq;
					//지난수업
					var lessonFinishFlag = this.lesson_finish_flag;
					var lessonFinishYnClass = "box0"; //지난 수업이 아님
					if (lessonFinishFlag == "Y") {
						lessonFinishYnClass = "box1"; //지난 수업
					}
					// pengajar
					var periodStr = this.period; // pengajar
					var periodList = periodStr.split(',');
					var startPeriod = periodList[0];
					//수업waktu
					var ampmText = this.ampm;
            		if(ampmText == "pm"){
            			ampmText = "오후";
            		} else {
            			ampmText = "오전";
            		}
					var startTime = this.start_time;
					var endTime = this.end_time;
					//Nama Mata Kuliah
					var currName = this.curr_name;
					//수업계획서 주제
					var lessonSubject =this.lesson_subject;
					//형성penilaian 개수
					var feCnt = this.fe_cnt;
					lessonPlanListHtml += '<tr class="box_wrap ' + lessonFinishYnClass + '" onclick="javascript:getAttendanceView(' + lpSeq + ',' + this.curr_seq + ')">';
					lessonPlanListHtml += '	<td class="box_s w1">';
					lessonPlanListHtml += '		<div class="wrap_s s1">';
					if (periodList.length > 1) {
						var endPeriod = periodList[periodList.length-1];
						lessonPlanListHtml += '		<span class="num n2">' + startPeriod + '</span>';
						lessonPlanListHtml += '		<span class="num_sign">~</span>';
						lessonPlanListHtml += '		<span class="num n2">' + endPeriod + '</span>';
					} else {
						lessonPlanListHtml += '		<span class="num n1">' + startPeriod + '</span>';
					}
					lessonPlanListHtml += '		</div>';
					lessonPlanListHtml += '	</td>';
					lessonPlanListHtml += '	<td class="box_s w2">';
					lessonPlanListHtml += '		<div class="wrap_s s2">';
					lessonPlanListHtml += '			<div class="wrap_ss">';
					lessonPlanListHtml += '				<span class="tm tm1">' + ampmText + '</span><span class="tm tm2">' + startTime + '</span><span class="tm sign">~</span><span class="tm tm3">' + endTime + '</span>';
					lessonPlanListHtml += '			</div>';
					lessonPlanListHtml += '			<div class="wrap_ss">';
					lessonPlanListHtml += '				<span class="sp_1">' + currName + '</span>';
					lessonPlanListHtml += '				<span class="sp_2">' + lessonSubject + '</span>';
					lessonPlanListHtml += '			</div>';
					if (feCnt > 0) {
						lessonPlanListHtml += '		<span class="sp_quiz">형성penilaian</span>';
					}
					lessonPlanListHtml += '		</div>';
					lessonPlanListHtml += '	</td>';
					lessonPlanListHtml += '</tr>';
				});
				$("#lessonPlanListArea").html(lessonPlanListHtml);
			}
			
			function getAttendanceView(lpSeq, currSeq) {
				post_to_url("${M_HOME}/pf/attendance/list", {"lp_seq":lpSeq, "curr_seq":currSeq});
			}
		</script>
	</head>
	<body>
		<div class="contents">
			<div class="schd_m">
				<div id="calendar1"></div>
				<div class="mtb_wrap">	
					<table class="tb_m1" id="lessonPlanListArea">
					</table>
				</div>	
			</div>	
		</div>
	</body>
</html>