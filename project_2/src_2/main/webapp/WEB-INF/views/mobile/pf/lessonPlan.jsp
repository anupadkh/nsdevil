<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function() {
				getLessonPlanInfo();
				
				if("${lpPFCheck}" != "Y")
					$("body").addClass("pf");
			});
			
			function getLessonPlanInfo(){
				
		        $.ajax({
		            type: "POST",
		            url: "${HOME}/ajax/pf/lesson/lessonPlanBasicInfo",
		            data: {
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	var lesson_date = data.lessonPlanBasicInfo.lesson_date.split("/");
		            	// pengajar
						var periodStr = data.lessonPlanBasicInfo.period; // pengajar
						var periodList = periodStr.split(',');
						var startPeriod = periodList[0];
						if (periodList.length > 1) {
							periodStr = startPeriod + "~" + periodList[periodList.length-1] + " pengajar";  
						} else {
							periodStr = startPeriod + " pengajar";
						}
						
		            	//수업일
		            	var lessonDate = lesson_date[0]+"월 "+lesson_date[1]+"일";
						
		            	//수업waktu
						var ampmText = data.lessonPlanBasicInfo.ampm;
		        		if(ampmText == "pm"){
		        			ampmText = "오후";
		        		} else {
		        			ampmText = "오전";
		        		}
		        		
		        		var startTime = data.lessonPlanBasicInfo.start_time_12;
						var endTime = data.lessonPlanBasicInfo.end_time_12;
		            	
		        		var lessonPlanDateTitle = lessonDate + " " + periodStr;//mm월 xx일 x pengajar
		        		var lessonPlanTimeTitle = ampmText + " " + startTime + "~" + endTime;//오전/오후 xx:xx~xx:xx
		        		
		            	var titleHtml = '<span class="cc_s1">' + lessonPlanDateTitle + '</span><span class="cc_s2">(' + lessonPlanTimeTitle + ')</span>';
		            	var titleDiv = $("div[name='titleDiv']").addClass("cc").html(titleHtml);
		            	
		            	
		            	var aca_name = data.lessonPlanBasicInfo.year + " " + data.lessonPlanBasicInfo.m_aca_system_name + " " + data.lessonPlanBasicInfo.s_aca_system_name + " " + data.lessonPlanBasicInfo.aca_system_name;
		            	
						
		            	$("span[name=lesson_date]").text(data.lessonPlanBasicInfo.lesson_date + " [" + periodStr + "]");
		            	$("span[name=lesson_time]").text(ampmText + " " + data.lessonPlanBasicInfo.start_time_12 + " ~ " + data.lessonPlanBasicInfo.end_time_12);
		            	$("span[name=acaName]").text(aca_name);
		            	
		            	$("#currName").text(data.lessonPlanBasicInfo.curr_name);
		            	$("#lessonSubject").text(data.lessonPlanBasicInfo.lesson_subject);
		            	$("#emName").text(data.lessonPlanBasicInfo.evaluation_method_name);
		            	/* var htmls = data.lessonPlanBasicInfo.curr_name;
		            	htmls+='<span class="sign">&gt;</span>'+data.lessonPlanBasicInfo.lesson_subject;
		            	htmls+='<span class="sign">&gt;</span>'+lesson_date[0]+"월 " + lesson_date[1]+"일";
		            	htmls+="("+data.lessonPlanBasicInfo.start_time + " ~ " + data.lessonPlanBasicInfo.end_time+")"; 
		            	$("span[name=lesson_subject]").html(htmls); */
		            	$("div[name=learning_outcome]").text(data.lessonPlanBasicInfo.learning_outcome);
		            	$("td[name=lesson_code]").text(data.lessonPlanBasicInfo.lesson_code);
		            	$("span[name=period_cnt]").text(data.lessonPlanBasicInfo.period_cnt);
		            	$("span[name=classroom]").text(data.lessonPlanBasicInfo.classroom);
		            	$("span[name=domain_name]").text(data.lessonPlanBasicInfo.domain_name);
		            	$("span[name=level_name]").text(data.lessonPlanBasicInfo.level_name);
		            	
		            	var ranNum = 0;
		            	var htmls='';
		            	
		            	//핵심 임상표현
	                	$.each(data.lessonCoreClinicList,function(index){
	                        ranNum = Math.floor(Math.random()*7) + 1;
	                        
	                        htmls += '<div class="c_wrap">';
		                    htmls += '<span class="tt_c color0'+ranNum+'">'+this.code_name+'</span></div>';         
	                    });
	                    $("#coreClinicListAdd").html(htmls);

	                	htmls = '';
	                	//임상표현
	                	$.each(data.lessonClinicList,function(index){
	                        ranNum = Math.floor(Math.random()*7) + 1;
	                        
	                        htmls += '<div class="c_wrap">';
		                    htmls += '<span class="tt_c color0'+ranNum+'">'+this.code_name+'</span></div>';          
	                    });
	                    $("#clinicListAdd").html(htmls);
	                	
	                    htmls = '';
	                	//임상술기
	                	$.each(data.lessonOsceList,function(index){
	                        ranNum = Math.floor(Math.random()*7) + 1;
	                        
	                        htmls += '<div class="c_wrap">';
		                    htmls += '<span class="tt_c color0'+ranNum+'">'+this.osce_name+'</span></div>';        
		                       
	                    });
	                    $("#osceCodeListAdd").html(htmls);
	                    
	                	htmls = '';
	                	//진단명
	                    $.each(data.lessonDiagnosisList,function(index){
	                        ranNum = Math.floor(Math.random()*7) + 1;

	                        htmls += '<div class="c_wrap">';
		                    htmls += '<span class="tt_c color0'+ranNum+'">'+this.code_name+'</span></div>';  		                      
	                    });
	                	$("#diaCodeAdd").html(htmls);
		                
		              	//syarat kelulusan
		                htmls='';
		                $.each(data.lessonFinishCapabilityList,function(index){
		                    ranNum = Math.floor(Math.random()*7) + 1;

		                    htmls += '<div class="c_wrap">';
		                    htmls += '<span class="tt_c color0'+ranNum+'">('+this.fc_code+') '+this.fc_name+'</span></div>';
		                });
		                $("#fcListAdd").html(htmls);
		                
		                //형성penilaian
		                htmls='';
		              	var fe_cnt = 0;
		                $.each(data.formationEvaluationList,function(index){
		                    ranNum = Math.floor(Math.random()*6) + 2;
		                    
		                    var colorClass = "";
		                    if(this.fe_cate_code=="01")
		                    	colorClass = "color01";
		                    else
		                    	colorClass = "color0"+ranNum;
		                                    
		                    htmls += '<div class="c_wrap">';
		                    htmls += '<span class="tt_c '+colorClass+'">'+this.code_name+'</span></div>';
		                    fe_cnt++;                   
		                });
		                $("#feListAdd").html(htmls); 
		                $("div[name=fe_num]").text(fe_cnt);
		                
		                //tidak ada perkuliahan
	                    htmls='';
		                if(data.lessonMethodList.length == 0){
		                	htmls += '<div class="c_wrap">';
		                    htmls += '<span class="tt_c color0'+ranNum+'">lecture</span></div>';		    
		                }else{
		                    $.each(data.lessonMethodList, function(index){  
		                    	htmls += '<div class="c_wrap">';
			                    htmls += '<span class="tt_c color0'+ranNum+'">'+this.code_name+'</span></div>';		                                
		                    });
		                }
	        			$("#lessonMethodAdd").html(htmls);
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        }); 
		    }
						
			function getFormationEvaluationListView() {
				post_to_url("${M_HOME}/pf/formationEvaluation/list", {"lp_seq":"${sessionScope.S_LP_SEQ}"});
			}
			
			function getAttendanceListView() {
				post_to_url("${M_HOME}/pf/attendance/list", {"lp_seq":"${sessionScope.S_LP_SEQ}", "curr_seq":"${sessionScope.S_CURRICULUM_SEQ}"});
			}
			
			function getLessonPlanView(){
				post_to_url("${M_HOME}/pf/lessonPlan", {"lp_seq":"${sessionScope.S_LP_SEQ}", "curr_seq":"${sessionScope.S_CURRICULUM_SEQ}"});
			}

			function getCurriculumView(){
				post_to_url("${M_HOME}/pf/curriculum", {"curr_seq" : "${sessionScope.S_CURRICULUM_SEQ}"});
			}
		</script>
	</head>
	<body class="color1">
		<!-- s_contents -->
		<div class="contents">
	
			<!-- s_top_tt-->
			<div class="top_tt">
				<span class="cc_t1" id="currName"></span>
				<span class="cc_t2" id="lessonSubject"></span>
			</div>
			<!-- e_top_tt -->
	
			<!-- s_mn_wrap -->
			<div class="mn_wrap syllabus">
				<div class="wrap_s">
					<span class="cc_mn cc_mn01" onClick="javascript:getCurriculumView();">교육과정</span>
					<span class="cc_mn cc_mn02 on" onClick="javascript:getLessonPlanView();">수업계획</span>
					<span class="cc_mn cc_mn03" onClick="location.href='${M_HOME}/pf/lessonData'">자료</span>
					<!-- 수업교수인지 체크 -->
					<c:if test='${lpPFCheck == "Y" }'>
						<span class="cc_mn cc_mn04" onClick="javascript:getFormationEvaluationListView();">형성penilaian</span>
					</c:if>
					<span class="cc_mn cc_mn05" onClick="location.href='${M_HOME}/pf/assignMent';">과제</span>
					<span class="cc_mn cc_mn06" onClick="javascript:getAttendanceListView();">absensi kehadiran</span>
				</div>
			</div>
			<!-- e_mn_wrap -->
	
			<!-- s_ptb_wrap -->
			<div class="ptb_wrap">
				<!-- s_sbus -->
				<table class="sbus">
					<tbody>
						<tr class="tr02">
							<td class="th01 w1">lecture<br>코드</td>
							<td class="w2" name="lesson_code"></td>
							<td class="th01 w1">일정</td>
							<td class="w2"><span class="sp01" name="lesson_date"></span>
							<span class="sp02" name="lesson_time"></span></td>
						</tr>
						<tr class="tr02">
							<td class="th01">시수</td>
							<td class="w2"><span class="sp_tt" name="period_cnt"></span></td>
							<td class="th01">장소</td>
							<td class="w2"><span class="sp_tt1" name="classroom"></span></td>
						</tr>
						<tr class="tr02">
							<td class="th01 w1">Area</td>
							<td class="w2"><span class="sp_tt1" name="domain_name"></span></td>
							<td class="th01 w1">tingkat</td>
							<td class="w2"><span class="sp_tt1" name="level_name"></span></td>
						</tr>
						<tr class="tr02">
							<td class="th01 w1">penilaian<br>방법</td>
							<td colspan="3" class="td_cnt" id="emName"></td>
						</tr>
						<tr class="tr02">
							<td class="th01 w1">학습<br>방법</td>
							<td colspan="3" class="td_cnt" id="lessonMethodAdd">
								
							</td>
						</tr>
						<tr class="tr02">
							<td class="th01 w1">핵심 ${code2.code_name }</td>
							<td colspan="3" class="td_cnt" id="coreClinicListAdd">
								
							</td>
						</tr>
	
						<tr class="tr02">
							<td class="th01 w1">관련 ${code2.code_name }</td>
							<td colspan="3" class="td_cnt" id="clinicListAdd">
								
							</td>
						</tr>
						
						<tr class="tr02">
							<td class="th01 w1">${code1.code_name }</td>
							<td colspan="3" class="td_cnt" id="osceCodeListAdd">
								
							</td>
						</tr>
						
						<tr class="tr02">
							<td class="th01 w1">${code3.code_name }</td>
							<td colspan="3" class="td_cnt" id="diaCodeAdd">
								
							</td>
						</tr>
	
						<tr class="tr02">
							<td class="th01 w1">형성<br>penilaian<br>여부</td>
							<td colspan="2" class="td_fnum" id="feListAdd">
								
							</td>
							<td class="td_num">
								<div class="tt_num" name="fe_num"></div>
							</td>
						</tr>
						<tr class="tr02">
							<td class="th01 w1">졸업<br>역량</td>
							<td colspan="3" class="td_cnt" id="fcListAdd">
								
							</td>
						</tr>
					</tbody>
				</table>
				<!-- e_sbus -->
	
				<!-- s_hasil pembelajaran -->
				<div class="sbus_title">hasil pembelajaran</div>
				<div class="sbus_tcon" name="learning_outcome"></div>
				<!-- e_hasil pembelajaran -->
	
			</div>
			<!-- e_ptb_wrap -->
	
		</div>
		<!-- e_contents -->
	</body>
</html>
