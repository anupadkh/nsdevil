<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta property="og:title" content="MLMS">
		<meta property="og:image" content="img/favicon.png">
		<meta property="og:description" content="의과대학 LMS">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no, maximum-scale=1, width=device-width" />
		<title>MLMS</title>
		
		<link rel="icon" type="img/png" href="${M_IMG}/favicon.png">
		<link rel="shortcut icon" href="${M_IMG}/favicon.png">
		<link rel="icon" href="${M_IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${M_IMG}/favicon.ico">
		<link rel="stylesheet" href="${CSS}/css_m1.css" type="text/css">
		<link rel="stylesheet" href="${CSS}/swiper.css">
		<link rel="stylesheet" href="${CSS}/dev_css_m1.css">
		<script src="${JS}/lib/jquery-1.11.1.js"></script>
		<script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.form.min.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/lib/swiper.js"></script>
		<script src="${JS}/mobileCommon.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				getQuizThumbnail();
			});
			
			function getQuizThumbnail() {
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/pf/formationEvaluation/quiz/thumbnail",
			        data: {
			        	"fe_seq" : "${fe_seq}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	var quizInfo = data.quiz_info;
			            	var lessonSubject = quizInfo.lesson_subject;
			            	if ("${sessionScope.S_USER_PICTURE_PATH}" != "") {
			            		$("#professorPicture").attr("src", "${sessionScope.S_USER_PICTURE_PATH}");
			            	}
			            	
			            	//헤더 형성penilaian 명
			            	$("#headerLessonSubject").html(lessonSubject);
			            	
			            	var quizThumbnail = quizInfo.thumbnail_path;
			            	if (quizThumbnail != "") {
				            	$("#quizThumbnailArea").html('<img class="preview" src="${RES_PATH}' + quizThumbnail + '" alt="형성penilaian 이미지 미리보기">');
			            	}
			            	
			            	//형성penilaian 명
			            	$("#lessonSubject").html(lessonSubject);
			            	
			            	//Keseluruhan 문제 수
			            	var totalQuizCnt = quizInfo.quiz_cnt;
			            	$("#totalQuizCnt").html(totalQuizCnt);
			            	//Keseluruhan 응시자수
			            	var totalExamineeCnt = quizInfo.total_examinee_cnt;
			            	//응시자 수
			            	var examineeYCnt = quizInfo.examinee_y_cnt;
			            	//결시자 수
			            	var examineeNCnt = quizInfo.examinee_n_cnt;
			            	if (quizInfo.attendance_cnt == 0) {
			            		examineeNCnt = 0;
			            	}
			            	
			            	var testState = quizInfo.test_state;
			            	if (testState == "WAIT" || examineeYCnt == 0) {
			            		$("#examineeYCnt").html(totalExamineeCnt);
			            		$("#examineeNCnt").html("0");
			            	} else {
			            		$("#examineeYCnt").html(examineeYCnt);
			            		$("#examineeNCnt").html(examineeNCnt);
			            	}
			            } else {
			            	alert("형성penilaian 불러오기에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        timeout: 60000,
			        error: function(xhr, textStatus) {
			        	errorCase(textStatus);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			
			function attendanceCheck() {
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/pf/attendance/check",
			        data: {
			        	"fe_seq" : "${fe_seq}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	var attendanceCnt = data.attendance_info.attendance_check_cnt;
			            	if (attendanceCnt > 0) {
			            		quizStart('Y');
			            	} else {
			            		if(confirm("absensi kehadiran체크를 하지 않았습니다.\n형성penilaian를 시작 하시겠습니까?")){
			            			quizStart('N');	
			            		}
			            	}
			            } else {
			            	alert("absensi kehadiran 체크 확인에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        timeout: 60000,
			        error: function(xhr, textStatus) {
			        	errorCase(textStatus);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function quizStart(attendance_flag) {
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/pf/formationEvaluation/start",
			        data: {
			        	"fe_seq" : "${fe_seq}",
			        	"attendance_flag" : attendance_flag
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	post_to_url("${M_HOME}/pf/formationEvaluation/quiz/detail", {"quiz_seq":data.quiz_info.quiz_seq, "fe_seq":"${fe_seq}"});
			            } else {
			            	alert("absensi kehadiran 체크 확인에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        timeout: 60000,
			        error: function(xhr, textStatus) {
			        	errorCase(textStatus);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function getPreviewDetail() {
				post_to_url("${M_HOME}/pf/formationEvaluation/quiz/preview/detail", {"fe_seq":"${fe_seq}"});
			}
			
			function getFormationEvaluationListView() {
				post_to_url("${M_HOME}/pf/formationEvaluation/list", {"lp_seq":"${sessionScope.S_LP_SEQ}"});
			}
		</script>
	</head>
	<body class="color1 bd_quiz">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
		<div id="skipnav">
			<a href="#gnb_q">주 메뉴 바로가기</a>
			<a href="#container">컨텐츠 바로가기</a>
		</div>
		<div id="wrap">
			<header class="header">
				<div class="wrap">
					<div class="icpt"><img src="${DEFAULT_PICTURE_IMG}" alt="사진 이미지" id="professorPicture"></div>
					<div class="top_date quiz">
						<span class="sp_q1" id="headerLessonSubject"></span>
					</div>
					<div class="wrap_goquiz"><div class="goquiz" title="형성penilaian 닫기" onclick="javascript:getFormationEvaluationListView();"></div></div>
				</div>
			</header>
			<div id="container" class="container_table">
				<div class="contents">
					<div class="ptb_wrap">
						<div class="pr_quiz">
							<div class="preview_wrap" id="quizThumbnailArea"></div>
							<div class="b_wrap">
								<div id="gnb_q" class="tt_wrap">
									<div class="title" id="lessonSubject"></div>
									<div class="tt"><span class="sp_t">Keseluruhan 문제 수</span><span class="sp_n" id="totalQuizCnt">0</span><span class="sp_un">문항</span></div>
									<div class="tt"><span class="sp_t">응시자 수</span><span class="sp_n" id="examineeYCnt">0</span><span class="sp_un">명</span></div>
									<div class="tt"><span class="sp_t">결시</span><span class="sp_n" id="examineeNCnt">0</span><span class="sp_un">명</span></div>
								</div>
								<button class="btn_1" onclick="javascript:attendanceCheck();">형성penilaian 시작</button>
								<button class="btn_2" onclick="javascript:getPreviewDetail();">미리 보기</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<a href="#" id="backtotop" title="To top" class="totop" ></a>
		</div>
	</body>
</html>