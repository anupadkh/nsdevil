<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function() {
				getAttendanceList();
				
				if("${lpPFCheck}" != "Y")
					$("body").addClass("pf");
			});

			function getAttendanceList() {
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/pf/attendance/list",
			        data: {},
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	titleSetting(data.lesson_plan_info);
			            	attendanceListSetting(data.attendance_list);
			            	attendanceInfoSetting();
			            	if(data.atdCnt == 0){
			            		$("#attdcon").removeClass("attd_con_on").text("absensi kehadiran 체크 미완료");			            		
			            	}else{
			            		$("#attdcon").addClass("attd_con_on").text("absensi kehadiran 체크 완료");		
			            	}
			            } else {
			            	alert("absensi kehadiran 불러오기에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function titleSetting(lessonPlanInfo) {
				// pengajar
				var periodStr = lessonPlanInfo.period; // pengajar
				var periodList = periodStr.split(',');
				var startPeriod = periodList[0];
				if (periodList.length > 1) {
					periodStr = startPeriod + "~" + periodList[periodList.length-1] + " pengajar";  
				} else {
					periodStr = startPeriod + " pengajar";
				}
				
            	//수업일
            	var lessonDate = lessonPlanInfo.lesson_date;
            	lessonDate = lessonDate.replace("-", "월 ") + "일";
				
            	//수업waktu
				var ampmText = lessonPlanInfo.ampm;
        		if(ampmText == "pm"){
        			ampmText = "오후";
        		} else {
        			ampmText = "오전";
        		}
        		
        		var startTime = lessonPlanInfo.start_time;
				var endTime = lessonPlanInfo.end_time;
            	
        		var lessonPlanDateTitle = lessonDate + " " + periodStr;//mm월 xx일 x pengajar
        		var lessonPlanTimeTitle = ampmText + " " + startTime + "~" + endTime;//오전/오후 xx:xx~xx:xx
        		
            	var titleHtml = '<span class="cc_s1">' + lessonPlanDateTitle + '</span><span class="cc_s2">(' + lessonPlanTimeTitle + ')</span>';
            	var titleDiv = $("div[name='titleDiv']").addClass("cc").html(titleHtml);
            	
            	//교육과정 명
            	var currName = lessonPlanInfo.curr_name;
            	
            	//수업계획서 명
            	var lessonSubject = lessonPlanInfo.lesson_subject;
            	
            	$("#currName").html(currName);
            	$("#lessonSubject").html(lessonSubject);
			}
			
			function attendanceListSetting(list) {
				var attendanceList = list;
            	var attendanceListHtml = "";
            	var totalStCnt = attendanceList.length;
            	$(attendanceList).each(function(){
            		var userSeq = this.user_seq;
            		var stName = this.name;
            		var stId = this.id;
            		var userSeq = this.user_seq;
            		var picturePath = this.picture_path;
            		if (isEmpty(picturePath)) {
            			picturePath = "${DEFAULT_PICTURE_IMG}";
            		}else{
            			picturePath = "${RES_PATH}"+this.picture_path;
            		}
            		var attendanceState = this.attendance_state;
            		var attendanceClass = "";
            		var lateClass = "";
            		var absenceClass = "";

            		if (attendanceState == "00") {
            			absenceClass = "ui-selected";
            		} else if (attendanceState == "02") {
            			lateClass = "ui-selected";
            		} else {
            			attendanceClass = "ui-selected";
            			//TODO
            			//휴학 미정 manajemen자 페이지 기획서 나와 봐야 알수 있음.
            		}
            		
	            	attendanceListHtml += '<tr class="box_wrap">';
	            	attendanceListHtml += '	<input type="hidden" name="userSeq" value="' + userSeq + '">';
	            	attendanceListHtml += '	<td class="box_s w1">';
	            	attendanceListHtml += '		<div class="wrap_s">';
	            	attendanceListHtml += '			<div class="a_mp">';
	            	attendanceListHtml += '				<div class="pt01"><img src="' + picturePath + '" alt="registrasi된 사진 이미지" class="pt_img"></div>';
	            	attendanceListHtml += '				<div class="ssp1">';
	            	attendanceListHtml += '					<span class="ssp_s1" id="stName">' + stName + '</span>';
	            	attendanceListHtml += '					<span class="ssp_s2" id="stId">' + stId + '</span>';
	            	attendanceListHtml += '				</div>';
	            	attendanceListHtml += '			</div>';
	            	attendanceListHtml += '		</div>';
	            	attendanceListHtml += '	</td>';
	            	attendanceListHtml += '	<td class="box_s w2">';
	            	attendanceListHtml += '		<div class="wrap_s">';
	            	attendanceListHtml += '			<ul class="wrap_ss selectable attendanceStateUl">';
	            	attendanceListHtml += '				<li class="ui-widget-content ' + attendanceClass + '" attendanceState="03" onclick="javascript:changeAttendanceState(this);">absensi kehadiran</li>';
	            	attendanceListHtml += '				<li class="ui-widget-content ' + lateClass + '" attendanceState="02" onclick="javascript:changeAttendanceState(this);">지각</li>';
	            	attendanceListHtml += '				<li class="ui-widget-content ' + absenceClass + '" attendanceState="00" onclick="javascript:changeAttendanceState(this);">결석</li>';
	            	attendanceListHtml += '			</ul>';
	            	attendanceListHtml += '		</div>';
	            	attendanceListHtml += '	</td>';
	            	attendanceListHtml += '</tr>';
            	});
            	$("#attendanceListArea").html(attendanceListHtml);
			}
			
			function changeAttendanceState(t) {
				var target = $(t);
				$(target).parent().find("li").removeClass("ui-selected");
				$(target).addClass("ui-selected");
				//attendanceInfoSetting();
			}
			
			function attendanceInfoSetting() {
				var attendanceCnt = $("li[attendanceState='03'].ui-selected").length;
        		var lateCnt = $("li[attendanceState='02'].ui-selected").length;
        		var absenceCnt = $("li[attendanceState='00'].ui-selected").length;
        		var totalCnt = attendanceCnt + lateCnt + absenceCnt;
        		
        		$("#totalCnt").html(totalCnt);
        		$("#attendanceCnt").html(attendanceCnt);
        		$("#lateCnt").html(lateCnt);
        		$("#absenceCnt").html(absenceCnt);
        		$("#attdcon").removeClass("attd_con_on");
			}
			
			function attandanceSubmit() {
				if($("#attdcon").hasClass("attd_con_on")){
					if(!confirm("이미 absensi kehadiran체크를 하셨습니다.\nabsensi kehadiran perbaiki하시겠습니까?"))
						return;
				}
				$(".attendanceStateUl").each(function() {
					var trTarget = $(this).parent().parent().parent();
					var selectCnt = $(this).find("li.ui-selected").length;
					if (selectCnt < 1) {
						var stName = $(trTarget).find("#stName").html();
						var stId = $(trTarget).find("#stId").html();
						alert(stName+"("+stId+") 학생의 출결을 pilih해 주세요.");
						return false;
					}
				});
				
				var jsonArray = new Array() ;
				$(".attendanceStateUl").each(function() {
					var jsonObject = new Object();
					var trTarget = $(this).parent().parent().parent();
					var userSeq = $(trTarget).find("input[name='userSeq']").val();
					var attendanceState = $(this).find("li.ui-selected").attr("attendanceState");
					jsonObject.user_seq = userSeq;
					jsonObject.attendance_state = attendanceState;
					jsonArray.push(jsonObject);
				});
				
				var jsonArrayStr = JSON.stringify(jsonArray) ;
				
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/pf/attendance/list/modify",
			        data: {
			        	"attendance_list" : jsonArrayStr
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
		            		$("#attdcon").addClass("attd_con_on").text("absensi kehadiran 체크 완료");	
			            	alert("absensi kehadiran체크를 완료하였습니다.");
			            } else {
			            	alert("absensi kehadiran 체크 완료에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function getFormationEvaluationListView() {
				post_to_url("${M_HOME}/pf/formationEvaluation/list", {"lp_seq":"${sessionScope.S_LP_SEQ}"});
			}
			
			function getLessonPlanView(){
				post_to_url("${M_HOME}/pf/lessonPlan", {"lp_seq":"${sessionScope.S_LP_SEQ}", "curr_seq":"${sessionScope.S_CURRICULUM_SEQ}"});
			}
			
			function getCurriculumView(){
				post_to_url("${M_HOME}/pf/curriculum", {"curr_seq" : "${sessionScope.S_CURRICULUM_SEQ}"});
			}
		</script>
	</head>
	<body class="color1 bd_attd">
		<div class="contents">
			<div class="top_tt">
				<span class="cc_t1" id="currName"></span>
				<span class="cc_t2" id="lessonSubject"></span>
			</div>
			<div class="mn_wrap">
				<div class="wrap_s">
					<span class="cc_mn cc_mn01" onClick="javascript:getCurriculumView();">교육과정</span>
					<span class="cc_mn cc_mn02" onClick="javascript:getLessonPlanView();">수업계획</span>
					<span class="cc_mn cc_mn03" onClick="location.href='${M_HOME}/pf/lessonData'">자료</span>
					<!-- 수업교수인지 체크 -->
					<c:if test='${lpPFCheck == "Y" }'>
						<span class="cc_mn cc_mn04" onClick="javascript:getFormationEvaluationListView();">형성penilaian</span>
					</c:if>
					<span class="cc_mn cc_mn05" onClick="location.href='${M_HOME}/pf/assignMent';">과제</span>
					<span class="cc_mn cc_mn06 on" onClick="javascript:getAttendanceListView();">absensi kehadiran</span>
				</div>
			</div>
			<div class="attd_wrap">
				<div class="wrap_s">
					<span class="attd_mn"><span class="sp1" id="totalCnt">0</span><span>명</span></span>
					<span class="attd_mn"><span>absensi kehadiran : </span><span class="sp2" id="attendanceCnt">0</span><span>명</span></span>
					<span class="attd_mn"><span>지각 : </span><span class="sp3" id="lateCnt">0</span><span>명</span></span>
					<span class="attd_mn"><span>결석 : </span><span class="sp4" id="absenceCnt">0</span><span>명</span></span>
				</div>
			</div>
			<div class="ptb_wrap">	
				<table class="tb_attd1" id="attendanceListArea"></table>
			</div>
			<footer id="footer" class="attd">
			<!-- 수업교수인지 체크 -->
			<c:choose>
				<c:when test='${lpPFCheck == "Y" }'>
					<div id="attdcon" class="attdcon" onClick="javascript:attandanceSubmit();">absensi kehadiran 체크 완료</div>
				</c:when>
				<c:otherwise>
					<div id="attdcon" class="attdcon" onClick="alert('담당교수만 absensi kehadiran 체크 할 수 있습니다.');">absensi kehadiran 체크 완료</div>
				</c:otherwise>
			</c:choose>
			</footer>
			<c:if test=''>
				<!-- s_footer -->
			</c:if>
		</div>
	</body>
</html>
