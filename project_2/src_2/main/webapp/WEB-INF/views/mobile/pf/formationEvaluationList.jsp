<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function() {
				$("div[name='titleDiv']").addClass("cc");
				getFormationEvaluationList();
			});
			
			function getFormationEvaluationList() {
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/pf/formaationEvaluation/list",
			        data: {},
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	titleSetting(data.lesson_plan_info);
			            	feListSetting(data.formation_evaluation_list);
			            } else {
			            	alert("형성penilaian 불러오기에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        timeout: 60000,
			        error: function(xhr, textStatus) {
			        	errorCase(textStatus);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function titleSetting(lessonPlanInfo) {
				// pengajar
				var periodStr = lessonPlanInfo.period; // pengajar
				var periodList = periodStr.split(',');
				var startPeriod = periodList[0];
				if (periodList.length > 1) {
					periodStr = startPeriod + "~" + periodList[periodList.length-1] + " pengajar";  
				} else {
					periodStr = startPeriod + " pengajar";
				}
				
            	//수업일
            	var lessonDate = lessonPlanInfo.lesson_date;
            	lessonDate = lessonDate.replace("-", "월 ") + "일";
				
            	//수업waktu
				var ampmText = lessonPlanInfo.ampm;
        		if(ampmText == "pm"){
        			ampmText = "오후";
        		} else {
        			ampmText = "오전";
        		}
        		
        		var startTime = lessonPlanInfo.start_time;
				var endTime = lessonPlanInfo.end_time;
            	
        		var lessonPlanDateTitle = lessonDate + " " + periodStr;//mm월 xx일 x pengajar
        		var lessonPlanTimeTitle = ampmText + " " + startTime + "~" + endTime;//오전/오후 xx:xx~xx:xx
        		
            	var titleHtml = '<span class="cc_s1">' + lessonPlanDateTitle + '</span><span class="cc_s2">(' + lessonPlanTimeTitle + ')</span>';
            	var titleDiv = $("div[name='titleDiv']").addClass("cc").html(titleHtml);
            	
            	//교육과정 명
            	var currName = lessonPlanInfo.curr_name;
            	
            	//수업계획서 명
            	var lessonSubject = lessonPlanInfo.lesson_subject;
            	
            	$("#currName").html(currName);
            	$("#lessonSubject").html(lessonSubject);
			}
			
			function feListSetting(feList) {
				var feListAreaHtml = "";
				$(feList).each(function() {
					var feSeq = this.fe_seq;
					var cateCode = this.fe_cate_code;
					var feName = this.fe_name;
					if (feName.length >= 18) {
						feName = feName.substring(0, 18)+"...";
					}
        			var testState = this.test_state;
        			var quizTypeName = this.quiz_type;
        			var quizTotalCnt = this.quiz_total_cnt;
        			var solveEndQuizCnt = this.solve_end_quiz_cnt;
        			var maxScore = this.max_score;
        			var minScore = this.min_score;
        			var avgScore = this.avg_score;

        			if (cateCode == "01") {
    					if (testState == "WAIT") {
    						feListAreaHtml += '<tr class="box_wrap">';
    						feListAreaHtml += '	<td class="box_s">';
    						feListAreaHtml += '		<div class="wrap_s prg1">';
    						feListAreaHtml += '			<div class="wrap_ss">';
    						feListAreaHtml += '				<div class="tt1">' + feName + '</div>';
    						feListAreaHtml += '				<div class="prg_t">대기</div>';
    						feListAreaHtml += '			</div>';
    						feListAreaHtml += '			<div class="tt3"><span class="prg_s1">대기</span><span class="sp_wrap"><span class="sign">(</span><span class="num1">' + quizTotalCnt + '</span><span class="sign">문제 )</span></span></div>';
    						feListAreaHtml += '			<div class="quiz_wrap_sc">';
    						feListAreaHtml += '				<div class="wrap_s">';
    						feListAreaHtml += '					<button class="quiz_sc" onClick="javascript:getQuizThumbnail(' + feSeq + ', ' + quizTotalCnt +');">시작하기</button>';
    						feListAreaHtml += '				</div>';
    						feListAreaHtml += '			</div>';
    						feListAreaHtml += '		</div>';
    						feListAreaHtml += '	</td>';
    						feListAreaHtml += '</tr>';
    					} else if (testState == "START") {
    						feListAreaHtml += '<tr class="box_wrap">';
    						feListAreaHtml += '	<td class="box_s">';
    						feListAreaHtml += '		<div class="wrap_s prg2">';
    						feListAreaHtml += '			<div class="wrap_ss">';
    						feListAreaHtml += '				<div class="tt1">' + feName + '</div>';
    						feListAreaHtml += '				<div class="prg_t">진행중</div>';
    						feListAreaHtml += '			</div>';
    						feListAreaHtml += '			<div class="tt3"><span class="prg_s1">진행 중</span><span class="sp_wrap"><span class="num1">' + solveEndQuizCnt + '</span><span class="sign">/</span><span class="num1">' + quizTotalCnt + '</span></span></div>';
    						feListAreaHtml += '			<div class="quiz_wrap_sc">';
    						feListAreaHtml += '				<div class="wrap_s">';
    						feListAreaHtml += '					<button class="quiz_sc" onClick="javascript:getQuizOngoing(' + feSeq + ');">진행 중 &gt;&gt; </button>';
    						feListAreaHtml += '				</div>';
    						feListAreaHtml += '			</div>';
    						feListAreaHtml += '		</div>';
    						feListAreaHtml += '	</td>';
    						feListAreaHtml += '</tr>';
    					} else if (testState == "END") {
    						feListAreaHtml += '<tr class="box_wrap">';
    						feListAreaHtml += '	<td class="box_s">';
    						feListAreaHtml += '		<div class="wrap_s prg3">';
    						feListAreaHtml += '			<div class="wrap_ss">';
    						feListAreaHtml += '				<div class="tt1">' + feName + '</div>';
    						feListAreaHtml += '				<div class="prg_t">종료</div>';
    						feListAreaHtml += '			</div>';
    						feListAreaHtml += '			<div class="tt3"><span class="prg_s1">종료</span><span class="sp_wrap"><span class="sign">(</span><span class="num1">' + quizTotalCnt + '</span><span class="sign">문제 -</span><span class="num1">' + quizTotalCnt + '</span><span class="sign">점 만점 )</span></span></div>';
    						feListAreaHtml += '			<div class="quiz_wrap_sc" onclick="javascript:getQuizResult(' + feSeq + ');">';
    						feListAreaHtml += '				<div class="wrap_s">';
    						feListAreaHtml += '					<span class="quiz_sc">';
    						feListAreaHtml += '						<span>최고점</span><span class="sp1">' + maxScore + '</span>';
    						feListAreaHtml += '					</span>';
    						feListAreaHtml += '					<span class="quiz_sc">';
    						feListAreaHtml += '						<span>최저점</span><span class="sp2">' + minScore + '</span>';
    						feListAreaHtml += '					</span>';
    						feListAreaHtml += '					<span class="quiz_sc">';
    						feListAreaHtml += '						<span>평균</span><span class="sp3">' + avgScore + '</span>';
    						feListAreaHtml += '					</span>';
    						feListAreaHtml += '				</div>';
    						feListAreaHtml += '			</div>';
    						feListAreaHtml += '		</div>';
    						feListAreaHtml += '	</td>';
    						feListAreaHtml += '</tr>';
    					}
        			}
				});
				
				if (feList.length > 0) {
					$("#feListArea").html(feListAreaHtml);
				} else {
					//registrasi된 온라인 퀴즈가 없습니다.
					//TODO 디자인 없음
					$("#feListArea").html("");
				}
			}
			
			
			function getQuizThumbnail(feSeq, quizCnt) {
				if (quizCnt < 1) {
					alert("registrasi된 문제가 없습니다. 문제를 registrasi해주세요.");
					return false;
				}
				post_to_url("${M_HOME}/pf/formationEvaluation/quiz/thumbnail", {"fe_seq":feSeq});
			}
			
			function getQuizOngoing(feSeq) {
				post_to_url("${M_HOME}/pf/formationEvaluation/quiz/ongoing", {"fe_seq":feSeq});
			}
			
			function getFormationEvaluationListView() {
				post_to_url("${M_HOME}/pf/formationEvaluation/list", {"lp_seq":"${sessionScope.S_LP_SEQ}"});
			}
			
			function getAttendanceListView() {
				post_to_url("${M_HOME}/pf/attendance/list", {"lp_seq":"${sessionScope.S_LP_SEQ}", "curr_seq":"${sessionScope.S_CURRICULUM_SEQ}"});
			}
			
			function getQuizResult(feSeq) {
				post_to_url("${M_HOME}/pf/formationEvaluation/quiz/result", {"fe_seq":feSeq});
			}

			function getCurriculumView(){
				post_to_url("${M_HOME}/pf/curriculum", {"curr_seq" : "${sessionScope.S_CURRICULUM_SEQ}"});
			}
		
			function getLessonPlanView() {
				post_to_url("${M_HOME}/pf/lessonPlan", {"lp_seq" : "${sessionScope.S_LP_SEQ}", "curr_seq":"${sessionScope.S_CURRICULUM_SEQ}"});
			}
		</script>
	</head>
	<body>
		<div class="contents">
			<div class="top_tt">
				<span class="cc_t1" id="currName"></span>
				<span class="cc_t2" id="lessonSubject"></span>
			</div>
			<div class="mn_wrap quiz">
				<div class="wrap_s">
					<span class="cc_mn cc_mn01" onClick="javascript:getCurriculumView();">교육과정</span>
					<span class="cc_mn cc_mn02" onClick="javascript:getLessonPlanView();">수업계획</span>
					<span class="cc_mn cc_mn03" onClick="location.href='${M_HOME}/pf/lessonData'">자료</span>
					<span class="cc_mn cc_mn04 on" onClick="javascript:getFormationEvaluationListView();">형성penilaian</span>
					<span class="cc_mn cc_mn05" onClick="location.href='${M_HOME}/pf/assignMent'">과제</span>
					<span class="cc_mn cc_mn06" onClick="javascript:getAttendanceListView();">absensi kehadiran</span>
				</div>
			</div>
			<div class="ptb_wrap">	
				<table class="tb_quiz" id="feListArea"></table>
			</div>	
		</div>
	</body>
</html>