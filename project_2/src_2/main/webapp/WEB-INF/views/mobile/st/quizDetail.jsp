<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta property="og:title" content="MLMS">
		<meta property="og:image" content="${M_IMG}/favicon.png">
		<meta property="og:description" content="의과대학 LMS">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no, maximum-scale=1, width=device-width" />
		<title>MLMS</title>
		
		<link rel="icon" type="${M_IMG}/png" href="${M_IMG}/favicon.png">
		<link rel="shortcut icon" href="${M_IMG}/favicon.png">
		<link rel="icon" href="${M_IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${M_IMG}/favicon.ico">
		<link rel="stylesheet" href="${CSS}/css_m3.css" type="text/css">
		<link rel="stylesheet" href="${CSS}/swiper.css">
		<link rel="stylesheet" href="${CSS}/dev_css_m3.css">
		<script src="${JS}/lib/jquery-1.11.1.js"></script>
		<script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.form.min.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/lib/swiper.js"></script>
		<script src="${JS}/mobileCommon.js"></script>
		
		<script type="text/javascript">
		
			var nextTryCnt = 0;
			var answerCnt = 0;
			$(document).ready(function() {
				getQuizDetail();
			});
			function getQuizDetail() {
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/st/formationEvaluation/quiz/detail",
			        data: {
			        	"fe_seq" : "${fe_seq}"
			        	, "quiz_seq" : "${quiz_seq}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	var quizInfo = data.quiz_info;
			            	var answerList = data.quiz_answer_list;
			            	quizInfoSetting(quizInfo);
			            	answerListSetting(answerList);
			            	selectAnswerSetting();
			            	answerCnt = data.answer_cnt;
			            } else {
			            	alert("형성penilaian 불러오기에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        timeout: 60000,
			        error: function(xhr, textStatus) {
			        	errorCase(textStatus);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function quizInfoSetting(quizInfo) {
				var feName = quizInfo.fe_name; //수업계획서 제목
				var question = quizInfo.quiz_question; //퀴즈 문항줄기
				var quizOrder = quizInfo.quiz_order; //퀴즈 번호
				var previewImgPath = quizInfo.file_path;
				$("#feName").html(feName);
				$("#quizQuestion").html(question);
				$("#quizOrder").html(quizOrder);
				if (previewImgPath != "") {
					$("#previewArea").removeClass("off");
				}
				$("#previewArea").html('<img class="m_preview" src="${RES_PATH}' + previewImgPath + '" alt="형성penilaian 이미지 미리보기">');
			}
			
			function answerListSetting(answerList) {
				var answerListHtml = "";
				$(answerList).each(function() {
					var answerOrder = this.answer_order;
					var content = this.content;
					var quizAnswerSeq = this.quiz_answer_seq;
					var answerFlag = "";
					if(this.answer_chk=="Y"){
						answerFlag = "on";
					}
					answerListHtml += '<tr class="n1">';
					answerListHtml += '	<td class="w1 correctAnswerNumber '+answerFlag+'" onclick="javascript:answerCheck(this);"><span class="num_box" answer="'+quizAnswerSeq+'">' + answerOrder + '</span></td>';
					answerListHtml += '	<td class="w3_2">' + content + '</td>';
					answerListHtml += '</tr>';
				});
				$("#answerListArea").html(answerListHtml);
			}
			
			function answerCheck(t) {
				var target = $(t);
				if ($(target).hasClass("on")) {
					$(target).removeClass("on");
				} else {
					if($("td.correctAnswerNumber.on").length == answerCnt){
						if(answerCnt == 1)
							$("td.correctAnswerNumber").removeClass("on");
						else{
							alert("정답은 "+answerCnt+"개 입니다. 기존 정답을 pilih해제 후 pilih해주세요.");
							return;
						}
							
					}
					$(target).addClass("on");					
				}
				selectAnswerSetting();
			}
			
			function selectAnswerSetting() {
				var selectAnswerHtml = "";
				var selectAnswerStr = "";
				
				$("td.correctAnswerNumber").each(function() {
					if($(this).hasClass("on")) {
						var answerNumber = $(this).find("span.num_box").text();
						selectAnswerStr += "," + answerNumber;
					}
				});
				
				if (selectAnswerStr != "") {
					selectAnswerStr = selectAnswerStr.substring(1, selectAnswerStr.length);
				}
				
				selectAnswerHtml += '<span class="st_sp01">pilih답안</span>';
				
				if (selectAnswerStr != "") {
					selectAnswerHtml += '<span class="num" >' + selectAnswerStr + '</span>';
					selectAnswerHtml += '<span class="sign">번</span>';
				}
				$("#selectAnswerArea").html(selectAnswerHtml);
			}
			
			function quizSubmit() {
				var selectAnswerStr = "";
				$("td.correctAnswerNumber").each(function() {
					if($(this).hasClass("on")) {
						var answerNumber = $(this).find("span.num_box").attr("answer");
						selectAnswerStr += "," + answerNumber;
					}
				});
				
				if (selectAnswerStr != "") {
					selectAnswerStr = selectAnswerStr.substring(1, selectAnswerStr.length);
				}
				
				var answerSelectCnt = $("td.correctAnswerNumber.on").length;
				if (answerSelectCnt < 1 || selectAnswerStr == "") {
					alert("답을 pilih해 주세요.");
					return false;
				}
				
				if ($("td.correctAnswerNumber.on").length < answerCnt) {
					alert("답안이 모두 pilih되지 않았습니다. (" + answerCnt + "개)");
					return false;
				}
				
				
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/st/formationEvaluation/quiz/submit",
			        data: {
			        	"fe_seq" : "${fe_seq}"
			        	, "quiz_seq" : "${quiz_seq}"
			        	, "answer_str_list" : selectAnswerStr
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	getQuizWaitView("${fe_seq}");
			            }else if(data.status == "201"){
			            	alert("해당 퀴즈문제는 종료되어 제출할 수 없습니다.\n다음문제로 넘어갑니다.");
			            	getQuizWaitView("${fe_seq}");
			            }else {
			            	alert("형성penilaian 불러오기에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        timeout: 60000,
			        error: function(xhr, textStatus) {
			        	errorCase(textStatus);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function getQuizWaitView(feSeq) {
				post_to_url("${M_HOME}/st/formationEvaluation/quiz/wait", {"fe_seq":feSeq});
			}
			
			function getFormationEvaluationListView() {
				post_to_url("${M_HOME}/st/formationEvaluation/list", {"lp_seq":"${sessionScope.S_LP_SEQ}", "curr_seq" : "${sessionScope.S_CURRICULUM_SEQ}"});
			}
		</script>
	</head>
	<body class="color1 bd_quiz">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
		    <p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
		<div id="skipnav">
			<a href="#gnb_q">주 메뉴 바로가기</a>
			<a href="#container">컨텐츠 바로가기</a>
		</div>
		<div id="wrap">
			<header class="header"> 
				<div class="wrap">
					<c:choose>
						<c:when test='${sessionScope.S_USER_PICTURE_PATH eq ""}'>
							<div class="icpt"><img src="${DEFAULT_PICTURE_IMG}" alt="사진 이미지"></div>
						</c:when>
						<c:otherwise>
							<div class="icpt"><img src="${sessionScope.S_USER_PICTURE_PATH}" alt="사진 이미지"></div>
						</c:otherwise>
					</c:choose> 
					<div class="top_date quiz">
						<span class="sp_q1" id="feName"></span>
					</div>
					<div class="wrap_goquiz"><div class="goquiz" title="형성penilaian 닫기" onclick="javascript:getFormationEvaluationListView();"></div></div>
				</div>
			</header>
			<div id="container" class="container_table">
				<div class="contents">   
					<div class="ptb_wrap">	
						<div class="pr_quiz">
							<button class="renew_wrap" onclick="javascript:getQuizDetail();"><span class="renew">새로고침</span></button>
							<div class="st b_wrap">
								<div id="gnb_q" class="st_tt_wrap2">출력에 이상이 있을 경우 새로고침 버튼을 누르세요.</div>
								<div class="pop_table">  
									<table class="pop_ipwrap1">
										<tbody>
											<tr>
												<td class="ip_tt"><span id="quizOrder">1</span></td>
												<td class="ip_output"><div id="quizQuestion"></div></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="m_preview_wrap off" id="previewArea"></div>
								<div class="m_q_tb_wrap1">
									<table class="m_q_tb1">
										<tbody id="answerListArea">
										</tbody>
									</table>  
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<a href="#" id="backtotop" title="To top" class="totop" ></a>
			<div class="st_tt_wrap3">! 형성penilaian는 한번 제출하면 다시 풀 수 없습니다.</div>	
			<div class="go_wrap">          
				<div class="pop_btn_wrap">
					<div class="wrap_s3" id="selectAnswerArea">
						<span class="st_sp01">pilih답안</span>
					</div>
					<button class="sbmt" name="submitBtn" onclick="javascript:quizSubmit();">제출</button>
				</div>
			</div>
		</div>
	</body>
</html>