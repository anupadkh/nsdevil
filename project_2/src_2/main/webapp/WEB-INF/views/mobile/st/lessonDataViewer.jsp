<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta property="og:title" content="MLMS">
		<meta property="og:image" content="${M_IMG}/favicon.png">
		<meta property="og:description" content="의과대학 LMS">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no, maximum-scale=1, width=device-width" />
		<title>MLMS</title>
		
		<link rel="icon" type="${M_IMG}/png" href="${M_IMG}/favicon.png">
		<link rel="shortcut icon" href="${M_IMG}/favicon.png">
		<link rel="icon" href="${M_IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${M_IMG}/favicon.ico">
		<link rel="stylesheet" href="${CSS}/css_m1.css" type="text/css">
		<link rel="stylesheet" href="${CSS}/swiper.css">
		<link rel="stylesheet" href="${CSS}/dev_css_m1.css">
		<script src="${JS}/lib/jquery-1.11.1.js"></script>
		<script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.form.min.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/lib/swiper.js"></script>
		<script src="${JS}/mobileCommon.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				getLessonDataAttach();
				
				$(document).on("click","#contentsAdd img",function(){
					viewPic($(this).attr("src"));					
				});
			});
			
			function getLessonDataAttach(){
				$.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/pf/lessonData/viewer/list",
		            data: {
		            	"lesson_attach_seq" : $("#lesson_attach_seq").val(),
		            },
		            dataType: "json",
		            success: function(data, status) {
		                if(data.status=="200"){
		                	setFileHtml(data.lessonAttachViewerList);
		                }else{
		                    alert("실패");
		                }
		            },
		            error: function(xhr, textStatus) {
		                alert("오류가 발생했습니다.");	            
		                
		            }
		        });    
			}
			
			function setFileHtml(list){
				if(list.length == 0)
					return "";
				else{
					var titleHtml = '';
					var htmls = '';
					var fileName = list[0].file_name; // 파일명
					//이미지 여러개
					if(list[0].attach_type == "I"){
						titleHtml = '<span class="sp_1">사진 [<span class="num">'+list.length+'</span>장]</span> <span class="sp_2">'+list[0].explan+'</span>';
						htmls = '<div class="rfr_wrap v3"><div class="s_wrap1"><div class="wrap_slide swiper-container"><div class="swiper-wrapper">';
						
						$.each(list, function(){
							htmls+='<div class="swiper-slide"><img src="${RES_PATH}'+this.file_path+'" alt="자료 이미지"></div>';
						});
						
						htmls+='</div>';
						htmls+='<div class="swiper-pagination"></div>';
						htmls+='<div class="swiper-button-next"></div>';
						htmls+='<div class="swiper-button-prev"></div>';
						htmls+='</div>';
						htmls+='</div>';							
					}
					//파일 한개
					else if(list[0].attach_type == "A"){
                    	titleHtml='<span class="sp_1">동영상 파일<span class="sort">'+fileExt+'</span></span><span class="sp_2">'+list[0].explan+'</span>';
						htmls='<div class="rfr_wrap v1">';
						htmls+='<div class="s_wrap3">';
						htmls+='<div class="vwrap">';
						htmls+='<div class="video_wrap">';         
						htmls+='<audio controls>';
						htmls+='<source src="${RES_PATH}'+list[0].file_path+'" type="audio/mp3">';
						htmls+='</audio>';
						htmls+='</div>';
						htmls+='</div>';	
						htmls+='</div>';
                    }else if(list[0].attach_type == "V"){
                    	titleHtml='<span class="sp_1">동영상 파일<span class="sort">mp4</span></span><span class="sp_2">'+list[0].explan+'</span>';
						htmls='<div class="rfr_wrap v1">';
						htmls+='<div class="s_wrap3">';
						htmls+='<div class="vwrap">';
						htmls+='<div class="video_wrap">';         
						htmls+='<video controls>';
						htmls+='<source src="${RES_PATH}'+list[0].file_path+'" type="video/mp4">';
						htmls+='</video>';
						htmls+='</div>';
						htmls+='</div>';	
						htmls+='</div>';        	                    
					}
					//유투브
					else if(list[0].attach_type == "Y"){
						titleHtml = '<span class="sp_1">lecture자료<span class="sort">유튜브</span></span><span class="sp_2">'+list[0].explan+'</span>';
						htmls = '<div class="rfr_wrap v1">';
						htmls+='<div class="s_wrap3">';
						htmls+='<div class="vwrap ut">';
						htmls+='<div class="video_wrap">';  
						                                     
						htmls+='<div class="video-container">';
						htmls+='<iframe src="'+list[0].youtube_url+'" allowfullscreen ></iframe>';
						htmls+='</div>';							
						htmls+='</div>';
						htmls+='</div>';  	
						htmls+='</div>'; 
					}

					//수업자료 isi 있으면 tambahkan
					if(list[0].content != "<p><br></p>"){
						htmls+='<div class="s_wrap2">';		
						htmls+='<div class="rfr_tcon">';
						htmls+=list[0].content;
						htmls+='</div>';
						htmls+='</div>';		
					}
					
					htmls+='</div>';
					
					$("div[name=titleDiv]").html(titleHtml);
					$("#contentsAdd").html(htmls);
					setSwiper();
				}
			}
			
			function setSwiper(){
				var swiper = new Swiper('.swiper-container', {
					spaceBetween: 0,
					centeredSlides: true,
					pagination: {
						el: '.swiper-pagination',
						clickable: true,						
					},
					navigation: {
						nextEl: '.swiper-button-next',
						prevEl: '.swiper-button-prev',						
					},					
				});
			}
		</script>
	</head>
	<body class="bd_v color1">
		<input type="hidden" id="lesson_attach_seq" value="${lesson_attach_seq }">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a> <a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
		<!-- s_wrap -->
		<div id="wrap">
	
			<!-- s_header -->
			<header class="header">
				<div class="wrap">	
					<!-- s_top_date -->
					<div class="top_date r_v" name="titleDiv">
						
					</div>
					<!-- e_top_date -->	
				</div>	
			</header>
			<!-- e_header -->
	
			<!-- s_container_table -->
			<div id="container" class="container_table">
				<!-- s_contents -->
				<div class="contents" id="contentsAdd">
	
					
	
				</div>
				<!-- e_contents -->
			</div>
			<!-- e_container_table -->
	
			<a href="#" id="backtotop" title="To top" class="totop"></a>
	
			<!-- s_닫기 -->
			<div class="go_wrap">
	
				<div class="pop_btn_wrap">
					<button type="button" class="btn01"
						onclick="javascript:history.back(-1);">자료 닫기</button>
				</div>
	
			</div>
			<!-- e_닫기 -->
		</div>
		<!-- e_wrap -->
	</body>
</html>