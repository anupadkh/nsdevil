<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta property="og:title" content="MLMS">
		<meta property="og:image" content="img/favicon.png">
		<meta property="og:description" content="의과대학 LMS">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no, maximum-scale=1, width=device-width" />
		<title>MLMS</title>
		
		<link rel="icon" type="img/png" href="${M_IMG}/favicon.png">
		<link rel="shortcut icon" href="${M_IMG}/favicon.png">
		<link rel="icon" href="${M_IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${M_IMG}/favicon.ico">
		<link rel="stylesheet" href="${CSS}/css_m3.css" type="text/css">
		<link rel="stylesheet" href="${CSS}/swiper.css">
		<link rel="stylesheet" href="${CSS}/dev_css_m3.css">
		<script src="${JS}/lib/jquery-1.11.1.js"></script>
		<script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.form.min.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/lib/swiper.js"></script>
		<script src="${JS}/mobileCommon.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				getQuizResult();
			});
		
			function getQuizResult() {
				$.ajax({
			        type: "POST",
			        url: "${M_HOME}/ajax/st/formationEvaluation/quiz/result",
			        data: {
			        	"fe_seq" : "${fe_seq}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	var quizInfo = data.quiz_info;
			            	var lessonSubject = quizInfo.lesson_subject;
			            	if ("${sessionScope.S_USER_PICTURE_PATH}" != "") {
			            		$("#stPicture").attr("src", "${sessionScope.S_USER_PICTURE_PATH}");
			            	}
			            	//헤더 형성penilaian 명
			            	$("#headerLessonSubject").html(quizInfo.fe_name);
			            	var quizThumbnail = quizInfo.thumbnail_path;
			            	if (quizThumbnail != "") {
				            	$("#quizThumbnailArea").html('<img class="preview" src="${RES_PATH}' + quizThumbnail + '" alt="형성penilaian 이미지 미리보기">');
			            	}
			            	//형성penilaian 명
			            	$("#lessonSubject").html(lessonSubject);
			            	var feResult = data.fe_result;
			            	var myScore = feResult.my_fe_score; //나의 점수
			            	var avgScore = feResult.avg_fe_score; //평균 점수
			            	var totalScore = feResult.total_fe_score; //만점
			            	var progressionMyScore = Math.floor(myScore/totalScore*100) || 0; //나의 점수 progression
			            	var progressionAvgScore = Math.floor(avgScore/totalScore*100) || 0; //평균 점수 progression
			            	$("#myScore").html(myScore);
			            	$("#avgScore").html(avgScore);
			            	$("#progressionMyScore").css("width", progressionMyScore*2+"px");
			            	$("#progressionAvgScore").css("width", progressionAvgScore*2+"px");
			            } else {
			            	alert("형성penilaian 결과 불러오기에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        timeout: 60000,
			        error: function(xhr, textStatus) {
			        	errorCase(textStatus);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function getFormationEvaluationListView() {
				post_to_url("${M_HOME}/st/formationEvaluation/list", {"lp_seq":"${sessionScope.S_LP_SEQ}", "curr_seq" : "${sessionScope.S_CURRICULUM_SEQ}"});
			}
		</script>
	</head>
	<body class="color1 bd_quiz">
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
		<div id="skipnav">
			<a href="#gnb_q">주 메뉴 바로가기</a>
			<a href="#container">컨텐츠 바로가기</a>
		</div>
		<div id="wrap">
			<header class="header">
				<div class="wrap">
					<div class="icpt"><img src="${DEFAULT_PICTURE_IMG}" alt="사진 이미지" id="stPicture"></div>
					<div class="top_date quiz">
						<span class="sp_q1" id="headerLessonSubject"></span>
					</div>
					<div class="wrap_goquiz"><div class="goquiz" title="형성penilaian 닫기" onclick="javascript:getFormationEvaluationListView();"></div></div>
				</div>
			</header>
			<div id="container" class="container_table">
				<div class="contents">   
					<div class="ptb_wrap">	
						<div class="pr_quiz end">
					    <button class="renew_wrap" onclick="getQuizResult();"><span class="renew">새로고침</span></button>
							<div class="st b_wrap">
					            <div id="gnb_q" class="st_tt_wrap2">출력에 이상이 있을 경우 새로고침 버튼을 누르세요.</div>	
								<div class="qwrap_p1">나의 점수</div>
								<div class="qwrap_p2"><span class="num" id="myScore">0</span><span class="sign">점</span></div>
								<div class="prgwrap">
					                <div class="prg"><span class="bar" style="width:0px;" id="progressionMyScore"></span></div><!-- s_ProgressBar width 200px 백분률로 변환하여 값 삽입 -->
								</div>
								<div class="prgwrap">
					                <div class="prg_avrg"><span class="bar" style="width:0px;" id="progressionAvgScore"></span></div><!-- e_ProgressBar width 200px 백분률로 변환하여 값 삽입 -->
									<div class="wrap_ss">
									    <span class="prgsign">평균</span><span class="prgnum" id="avgScore">0</span><span class="prgsign">점</span>
									</div>
								</div>
							</div>
					   </div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>