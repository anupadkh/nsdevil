<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>		
		<script type="text/javascript">
			$(document).ready(function(){
				customSelectBoxInit();
				initPageTopButton();
				
				$("body").addClass("grade");
				$("div[name=titleDiv]").removeClass("top_date").addClass("top_title").empty().html('<span class="survey">성적</span>');
				
				getScoreList();
				getScoreNew();
			});			
			
			function changeTab(obj){
				//탭클릭 lp = 수업과제, curr = 과정과제
				if($(obj).attr("data-name") == "fe"){
					$("span[data-name=fe]").addClass("on");
					$("span[data-name=score]").removeClass("on");
					$("span[data-name=currScore]").removeClass("on");
				}else if($(obj).attr("data-name") == "score"){
					$("span[data-name=fe]").removeClass("on");
					$("span[data-name=score]").addClass("on");
					$("span[data-name=currScore]").removeClass("on");
				}else if($(obj).attr("data-name") == "currScore"){
					$("span[data-name=fe]").removeClass("on");
					$("span[data-name=score]").removeClass("on");
					$("span[data-name=currScore]").addClass("on");
				}
				getScoreList();
			}
			
			function getScoreNew(){
				 $.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/st/grade/new",
		            data: {                  
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		if(data.gradeNew.fe_new != 0){
		            			$("span[data-name=feNew]").show();
		            		}else{
		            			$("span[data-name=feNew]").hide();
		            		}
		            		if(data.gradeNew.curr_new != 0){
		            			$("span[data-name=currNew]").show();		            			
		            		}else{
		            			$("span[data-name=currNew]").hide();
		            		}
		            		if(data.gradeNew.oc_new != 0){
		            			$("span[data-name=ocNew]").show();		            			
		            		}else{
		            			$("span[data-name=ocNew]").hide();
		            		}
		            	}
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        }); 			
			}	
			
			function getScoreList(){
				var type = "";

				if($("span[data-name=fe]").hasClass("on")){
					type="fe";
					$("#gradeDiv").attr("class","stgrade_w1");
				}else if($("span[data-name=currScore]").hasClass("on")){
					type="curr";
					$("#gradeDiv").attr("class","stgrade_w3");
				}else{
					type="oc";
					$("#gradeDiv").attr("class","stgrade_w2");
				}
				
				$.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/st/grade/list",
		            data: {      
		            	"year" : "${year}"
		            	,"curr_seq" : $("#curr_seq").attr("data-value")
		            	,"type" : type
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status=="200"){
		            		var htmls = "";

		            		$("#scoreListAdd").empty();
		            		
		            		if(data.gradeList.length == 0){
		            			$("#scoreListAdd").addClass("regix4");
		            		}else{
		            			$("#scoreListAdd").removeClass("regix4");
		            		}
		            		
		            		
		            		if(type=="fe"){
			            		var fe_state = "";
			            		var fe_class = "";
			            		var fe_state_name = "";
			            		var period = "";
			            		var ampm = "";
			            		
			            		
			            		
								$.each(data.gradeList, function(index){
									if(this.test_state=="WAIT"){
										fe_class="att_bf";
										fe_state_name = "응시 전";
									}else if(this.test_state=="START"){
										fe_class="attend";		
										fe_state_name = "진행 중";
									}else{
										if(this.submit_yn=="Y"){
											fe_class="attend";
											fe_state_name = "응시";
										}else{
											fe_class="absent";
											fe_state_name = "결시";
										}
									}
									
									period =  this.period.split(",");
									
									if(period.length > 1)
										period = period[0]+"~"+period[period.length-1];
									else 
										period = this.period;
									
									if(this.ampm=="am")
										ampm = "오전";
									else
										ampm = "오후";
									
									htmls='<tr class="box_wrap" onClick="getFormationEvaluationListView('+this.lp_seq+','+this.curr_seq+');">'
										+'<td class="box_s w1">'
										+'<div class="wrap_s s1">'
										+'<span class="sp_s1">'
										+'<span class="num1">'+this.quiz_cnt+'</span>'
										+'<span class="tt">점</span></span>' 
										+'<span class="sp_s2">'
										+'<span class="sign">/</span>'
										+'<span class="num1">'+this.score+'</span>'
										+'<span class="tt">점</span></span>'
										+'</div>'
										+'</td>'
										+'<td class="box_s w2">'
										+'<div class="wrap_s">'
										+'<div class="wrap_ss">'
										+'<span class="tm num_s1">'+this.lesson_date+'</span>'
										+'<span class="'+fe_class+'">'+fe_state_name+'</span>'
										+'</div>'
										+'<div class="wrap_ss">'
										+'<span class="sp_1">'+this.lesson_subject+'</span>' 
										+'<span class="sp_2">'
										+'<span class="num1">'+period+'</span>'
										+'<span class="tt1"> pengajar</span>' 
										+'<span class="sign">|</span>'
										+'<span class="num2">'+ampm+' '+this.start_time+'~'+this.end_time+'</span>'
										+'<span class="tt2">'+name+'</span>'
										+'</span>'
										+'</div>'
										+'</div>'
										+'</td>'
										+'</tr>';
									$("#scoreListAdd").append(htmls);
								});
		            		}else if(type == "curr"){
		            			$.each(data.gradeList, function(index){
			            			htmls = '<tr class="box_wrap">'
			            		    	+'<td class="box_s w1">'
			            		    	+'<div class="wrap_s s1">'
			            		    	+'<span class="sp_ttb">'+this.final_grade+'</span>'
			            		    	//+'<span class="sp_s1"><span class="num1">'+this.change_score+'</span><span class="tt">점</span></span>'
			            		    	+'</div>'
			            		    	+'</td>'
			            		    	+'<td class="box_s w2">'
			            		    	+'<div class="wrap_s">'
			            		    	+'<div class="wrap_ss">'
			            		    	+'<span class="tt2">'+this.curr_name+'</span>'
			            		    	+'</div>'
			            		    	+'<div class="wrap_ss">'
			            		    	+'<span class="sp_2">'
			            		    	+'<span class="num2">'+this.curr_start_date+'~'+this.curr_end_date+'</span>'
			            		    	+'<span class="sign">–</span>'
			            		    	+'<span class="num1">'+this.grade+'</span>'
			            		    	+'<span class="tt1">nilai</span>'
			            		    	+'</span>'
			            		    	+'<span class="sp_2">'
			            		    	+'<span class="tt3">Dosen Penanggung jawab</span><span class="tt4">'+this.name+'</span>'
			            		    	+'</span>'
			            		    	+'</div>'
			            		    	+'</div>'
			            		    	+'</td>'
			            		    	+'</tr>';
									$("#scoreListAdd").append(htmls);
								});
		            		}else{
		            			$.each(data.gradeList, function(index){
		            				htmls = '<tr class="box_wrap">'

		            			    +'<td class="box_s w1">'
		            			    +'<div class="wrap_s s1">'
		            			    +'<span class="sp_s1"><span class="num1">'+this.question_cnt+'</span><span class="tt">점</span></span>'
		            			    +'<span class="sp_s2"><span class="sign">/</span><span class="num1">'+this.score+'</span><span class="tt">점</span></span>'
		            			    +'</div>'
		            			    +'</td>'
		            			    +'<td class="box_s w2">'
		            			    +'<div class="wrap_s">'
		            			    +'<div class="wrap_ss">'
		            			    +'<span class="tm num_s1">'+this.src_date+'</span>'
		            			    +'</div>'
		            			    +'<div class="wrap_ss">'
		            			    +'<span class="sp_1">'+this.curr_name+'</span>'
		            			    +'<span class="sp_1b">'+this.src_name+'</span>'
		            			    +'<div class="sp_wrap">'
		            			    +'<div class="dv"><span class="tt1">총문항</span><span class="num1">'+this.question_cnt+'</span></div>'
		            			    +'<div class="dv"><span class="tt1">정답</span><span class="num1">'+this.answer_cnt+'</span></div><div class="dv"><span class="tt1">점수</span><span class="num1">'+this.score+'점</span></div>'
		            			    +'<div class="dv"><span class="tt1">평균</span><span class="num1">'+this.avg_score+'</span></div><div class="dv"><span class="tt1">편차</span><span class="num1">'+this.deviation+'</span></div>'
		            			    +'</div>'
		            			    +'</div>'
		            			    +'</div>'
		            			    +'</td>'
		            			    +'</tr>';
									$("#scoreListAdd").append(htmls);		            				
		            			});
		            			
		            		}
		            	}
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        }); 					
			}			

			function getFormationEvaluationListView(lp_seq, curr_seq) {
				post_to_url("${M_HOME}/st/formationEvaluation/list", {"lp_seq":lp_seq, "curr_seq":curr_seq});
			}
		</script>
	</head>
	<body class="color2 inquiry v1">
	<!-- s_contents -->
	<div class="contents">
		<!-- Tab : cc_grade_list.html, 각각을 Page로 나눈 것 cc_grade_list1.html, cc_grade_list2.html, cc_grade_list3.html : 필요에 따라 pilih 사용 -->
		<!-- s_stgradetb_wrap -->
		<div class="stgradetb_wrapv">

			<!-- s_stgrade_w1 -->
			<div class="stgrade_w1" id="gradeDiv">

				<div class="stgrade_ttwrap">
					<span class="stgrade1 on tab" data-name="fe" onClick="changeTab(this);">형성penilaian<span class="newsign" data-name="feNew">N</span></span>
					<span class="stgrade2 tab" data-name="score" onClick="changeTab(this);">nilai yang diinginkan<span class="newsign" data-name="ocNew">N</span></span>
					<span class="stgrade3 tab" data-name="currScore" onClick="changeTab(this);">종합성적<span class="newsign" data-name="currNew">N</span></span>
				</div>

				<!-- s_sch_wrap-->
				<div class="sch_wrap">
					<span class="tts">Nama Mata Kuliah</span>
					<div class="wrap_uselectbox1">
						<div class="uselectbox">
							<span class="uselected" data-value="" id="curr_seq">Keseluruhan</span> <span class="uarrow">▼</span>
							<div class="uoptions">
								<span class="uoption firstseleted" data-value="">Keseluruhan</span>
								
								<c:forEach var="currList" items="${currList}">
									<span class="uoption" data-value="${currList.curr_seq}">${currList.curr_name}</span>
                                </c:forEach>
							</div>
						</div>
					</div>

					<button class="btn_search1" onclick="getScoreList();">Pencarian</button>
				</div>
				<!-- e_sch_wrap -->

				<div class="mtb_wrap">
					<!-- registrasi된 성적이 없습니다. class : regix4 삽입 -->
					<table class="tb_stgrade1" id="scoreListAdd">

						
					</table>
					<!-- e_tb_stgrade1 -->

				</div>

			</div>
			<!-- e_stgrade_w1 -->

		</div>
		<!-- e_stgradetb_wrap -->

	</div>
	<!-- e_contents -->
</body>
</html>