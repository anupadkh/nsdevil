<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<style>
			.s_wrap1{display:block;width:100%;margin: 15px auto 20px auto;padding: 0;}
			.mm_ttx{display: table;width: 100%;height: auto;line-height: 18px;font-family:inherit;font-size: 14px;color: rgba(85, 85, 85, 1);text-align: left;text-indent: 0;letter-spacing: -1px;box-sizing:border-box;overflow: visible;text-overflow: ellipsis;margin: 1px auto 0 auto;transition: .7s;cursor:default;border-bottom-right-radius: 0;z-index: 20;box-shadow: inset 1px 1px 1px rgba(255, 252, 185, 1);background: url(../img/mm_bg_long.png) no-repeat 100% 100% rgba(255, 253, 215, 1);}
			.tarea01{display: table;width: 100%;min-height: auto;line-height: 18px;margin: 5px auto 10px auto;padding: 2px 10px 0 10px;text-align:left;word-wrap:break-word;text-indent: 0;letter-spacing: 0px;font-size: 14px;color: rgba(0, 139, 173, 1);overflow: visible;word-break: break-all;box-sizing: border-box;border: solid 1px rgba(255, 253, 216, 0);background: rgba(255, 253, 215, 0);overflow-y: visible;height: auto;cursor:default;}
		</style>
		<script type="text/javascript">
			
			
			$(document).ready(function() {
				getLessonData();
				getLessonPlanTitleInfo();//상단 타이틀 입력
				
				$(document).on("click","div[name=lessonContent] img",function(){
					viewPic($(this).attr("src"));					
				});
			});
		
			function getLessonPlanTitleInfo(){
				
				$.ajax({
		            type: "POST",
		            url: "${M_HOME}/ajax/st/lessonPlanTitleInfo",
		            data: {                  
		            },
		            dataType: "json",
		            success: function(data, status) {
		            	if(data.status == "200"){
			            	var lesson_date = data.info.lesson_date.split("-");
			            	// pengajar
							var periodStr = data.info.period; // pengajar
							var periodList = periodStr.split(',');
							var startPeriod = periodList[0];
							if (periodList.length > 1) {
								periodStr = startPeriod + "~" + periodList[periodList.length-1] + " pengajar";  
							} else {
								periodStr = startPeriod + " pengajar";
							}
							
			            	//수업일
			            	var lessonDate = lesson_date[0]+"월 "+lesson_date[1]+"일";
							
			            	//수업waktu
							var ampmText = data.info.ampm;
			        		if(ampmText == "pm"){
			        			ampmText = "오후";
			        		} else {
			        			ampmText = "오전";
			        		}
			        		
			        		var startTime = data.info.start_time_12;
							var endTime = data.info.end_time_12;
			            	
			        		var lessonPlanDateTitle = lessonDate + " " + periodStr;//mm월 xx일 x pengajar
			        		var lessonPlanTimeTitle = ampmText + " " + data.info.start_time + "~" + data.info.end_time;//오전/오후 xx:xx~xx:xx
			        		
			            	var titleHtml = '<span class="cc_s1">' + lessonPlanDateTitle + '</span><span class="cc_s2">(' + lessonPlanTimeTitle + ')</span>';
			            	var titleDiv = $("div[name='titleDiv']").addClass("cc").html(titleHtml);
			            	
			            	var lessonInfoHtml = '<span class="cc_t1">'+data.info.lesson_subject+'</span>'
								+ '<span class="cc_t2"><span class="ts1">교수</span>';
	
							if(isEmpty(data.info.name)){
								lessonInfoHtml+='<span class="ts2">미배정</span>';
							}else{
								lessonInfoHtml+='<span class="ts2">'+data.info.name+'</span>'
								+ '<span class="sign">(</span><span class="ts2">'+data.info.code_name+'</span><span class="sign">)</span></span>';
							}
							
							if(data.info.attendance_state == "WAIT")
								lessonInfoHtml +='<span class="cc_t3">absensi kehadiran체크 전</span>';
							else if(data.info.attendance_state == "00")
								lessonInfoHtml +='<span class="absent">결석</span>';
							else if(data.info.attendance_state == "02")
								lessonInfoHtml +='<span class="late">지각</span>';
							else if(data.info.attendance_state == "03")
								lessonInfoHtml +='<span class="attend">absensi kehadiran</span>';
							else
								lessonInfoHtml +='<span class="cc_t3">absensi kehadiran 미입력</span>';
									
							$("#lessonInfo").html(lessonInfoHtml);
		            	}
		            },
		            error: function(xhr, textStatus) {
		                //alert("오류가 발생했습니다.");
		                document.write(xhr.responseText);
		            }
		        }); 
			}
			
			function getLessonData(){
				$.ajax({
	                type: "POST",
	                url: "${M_HOME}/ajax/pf/lessonData/list",
	                data: {                 
	                },
	                dataType: "json",
	                success: function(data, status) {                	
	                	if(data.status == "200"){
	                		if(!isEmpty(data.lessonData)){
	                			if(!isEmpty(data.lessonData.memo))
	                				$("#memoAdd").html('<div class="tarea01 tt_t" name="memo">'+data.lessonData.memo+'</div>');
	                		}
	                		
	                		if(!isEmpty(data.lessonData)){
	                			if(data.lessonData.content == "<p><br></p>"){
		                			$("div[name=lessonContent]").hide();
		                		} else{
									$("div[name=lessonContent]").html(data.lessonData.content);
		                		}
	                		}else{
	                			$("div[name=lessonContent]").hide();
	                		}
	                			
							var htmls = '';

							//이미지 width 제한
							$("div[name=lessonContent] img").css("max-width", $(document).width()-40 );
							$("span[name=lessonDataCnt]").text(data.lessonAttachList.length);
							$.each(data.lessonAttachList, function(index){
								htmls = "";
								if(this.attach_type == "I"){
							    	var filename = this.file_name.split("||")[0];
							    	
							    	if(this.file_name.split("||").length != 1){
							    		htmls = '<div class="filewrap">'
											+'<div class="wrap_s dw"  onClick="getFileViewer(\''+this.lesson_attach_seq+'\');">'
											+'<div class="preview1">'
											+'<img src="${RES_PATH}'+this.file_path.split("||")[0]+'" alt="'+filename+'">'
											+'</div>'
											+'<div class="sp_wrap">'
											+'<span class="sp_1">'+this.explan+'</span>' 
											+'<span class="sp_2">사진['+this.file_name.split("||").length+'장]</span>'
											+'</div>'
											+'</div>'
											+'<button class="dw" onClick="multiFileDownload(\'${HOME}\',\''+this.file_path+'\',\''+this.file_name+'\');"></button>'
											+'</div>';					
							    	}else{
							    		htmls = '<div class="filewrap">'
											+'<div class="wrap_s dw"  onClick="getFileViewer(\''+this.lesson_attach_seq+'\');">'
											+'<div class="preview1">'
											+'<img src="${RES_PATH}'+this.file_path+'" alt="'+this.file_name+'">'
											+'</div>'
											+'<div class="sp_wrap">'
											+'<span class="sp_1">'+this.explan+'</span>' 
											+'<span class="sp_2">사진</span>'
											+'</div>'
											+'</div>'
											+'<button class="dw" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');"></button>'
											+'</div>';					
							    	}								
							    }else if(this.attach_type == "A"){
				                    	htmls = '<div class="filewrap">'
				    						+'<div class="wrap_s dw" onClick="getFileViewer(\''+this.lesson_attach_seq+'\');">'
				    						+'<div class="preview1">'
				    						+'<audio width="100" controls="">'
				    						+'<source src="${RES_PATH}'+this.file_path+'" type="audio/mp3">'
				    						+'</audio>'
				    						+'</div>'
				    						+'<div class="sp_wrap">'
				    						+'<span class="sp_1">'+this.explan+'</span>' 
				    						+'<span class="sp_2">오디오</span>'
				    						+'</div>'
				    						+'</div>'
				    						+'<button class="dw" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');"></button>'
				    						+'</div>';				                    	
			                    }else if(this.attach_type == "V"){
				                    	htmls = '<div class="filewrap">'
				    						+'<div class="wrap_s dw" onClick="getFileViewer(\''+this.lesson_attach_seq+'\');">'
				    						+'<div class="preview1">'
				    						+'<video width="100" controls="">'
				    						+'<source src="${RES_PATH}'+this.file_path+'" type="video/mp4">'
				    						+'</video>'
				    						+'</div>'
				    						+'<div class="sp_wrap">'
				    						+'<span class="sp_1">'+this.explan+'</span>' 
				    						+'<span class="sp_2">동영상</span>'
				    						+'</div>'
				    						+'</div>'
				    						+'<button class="dw" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');"></button>'
				    						+'</div>';								        
			                    }else if(this.attach_type=="Y"){	
			                    	htmls = '<div class="filewrap">'
			        					+'<div class="wrap_s dw" onClick="getFileViewer(\''+this.lesson_attach_seq+'\');">'
			        					+'<div class="preview1">'
			        					+'<div class="video_container">'
			        					+'<iframe src="'+this.youtube_url+'" allowfullscreen></iframe>'
			        					+'</div>'
			        					+'</div>'
			        					+'<div class="sp_wrap">'
			        					+'<span class="sp_1">'+this.explan+'</span>' 
			        					+'<span class="sp_2">유튜브</span>'
			        					+'</div>'
			        					+'</div>'
			        					+'</div>';
							    }else{
				                    	htmls='<div class="wrap_s dw">'
				        					+'<div class="sp_wrap">'
				        					+'<span class="sp_1" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');">'+this.file_name+'</span>'
				        					+'</div>'
				        					+'<button class="dw" onClick="fileDownload(\'${HOME}\',\'${RES_PATH}'+this.file_path+'\',\''+this.file_name+'\');"></button>'
				        					+'</div>';
			                    }
								$("#fileListAdd").append(htmls);
							});
	                	}
	                },
	                error: function(xhr, textStatus) {
	                    //alert("오류가 발생했습니다.");
	                    document.write(xhr.responseText);
	                }
	            }); 
			}
			
						
			function getLessonPlanView() {
				post_to_url("${M_HOME}/st/lessonPlan", { "lp_seq" : "${sessionScope.S_LP_SEQ}", "curr_seq" : "${sessionScope.S_CURRICULUM_SEQ}" });
			}
			
			function getCurriculumView(){
				post_to_url("${M_HOME}/st/curriculum", {"curr_seq" : "${sessionScope.S_CURRICULUM_SEQ}"});
			}

			function getFormationEvaluationListView() {
				post_to_url("${M_HOME}/st/formationEvaluation/list", {"lp_seq" : "${sessionScope.S_LP_SEQ}", "curr_seq":"${sessionScope.S_CURRICULUM_SEQ}"});
			}
			
			//슬라이드? 셋팅
			function setSwiper(){
				var swiper = new Swiper('.swiper-container', {
					spaceBetween: 0,
				    centeredSlides: true,
				    autoplay:false,
					pagination: {
				        el: '.swiper-pagination',
				      },
				      navigation: {
				        nextEl: '.swiper-button-next',
				        prevEl: '.swiper-button-prev',
				      },
			    });			
			}   
			
			//뷰어 페이지로 이동한다...
			function getFileViewer(lesson_attach_seq) {
				post_to_url("${M_HOME}/st/lessonData/viewer", {
					"lesson_attach_seq" : lesson_attach_seq
				});
			}
			
			
  		</script>
	</head>
	<body class="color1">
		<!-- s_contents -->
		<div class="contents">
	
			<!-- s_top_tt-->
			<div class="top_tt" id="lessonInfo">
			</div>
			<!-- e_top_tt -->
	
			<!-- s_mn_wrap -->
			<div class="mn_wrap rfr">
				<div class="wrap_s">
					<span class="cc_mn cc_mn01" onClick="javascript:getCurriculumView();">교육과정</span>
					<span class="cc_mn cc_mn02" onClick="javascript:getLessonPlanView();">수업계획</span>
					<span class="cc_mn cc_mn03 on" onClick="location.href='${M_HOME}/st/lessonData'">자료</span>
					<span class="cc_mn cc_mn04" onClick="javascript:getFormationEvaluationListView();">형성penilaian</span>
					<span class="cc_mn cc_mn05" onClick="location.href='${M_HOME}/st/assignMent';">과제</span>
				</div>
			</div>
			<!-- e_mn_wrap -->
	
			<div class="rfrform_wrap">
			<div class="mm_ttx dis" id="memoAdd">
				<div class="tarea01 tt_t" name="memo"></div>
			</div>
			
			<div class="con" name="lessonContent"></div>

			<div class="sch_wrap5">
				<div class="wrap">
					<span class="swrap"> 
						<span>총</span>
						<span class="num" name="lessonDataCnt"></span>
						<span class="numt">건</span><span>의 자료 업로드</span>
					</span>
				</div>
			</div>

			<div class="box_s" id="fileListAdd">
			
			</div>

		</div>
	
		</div>
		<!-- e_contents -->
	</body>
</html>
