<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function() {
				boardHearderInit();
				pblListView(1);
			});
			
			function pblListView(page) {
				if (typeof page == "undefined") {
					page = 1;
				}
				$("input[name='page']").val(page);
				var searchText = $("#boardSearchText").val();
				if (!isEmpty(searchText) && !isBlank(searchText)){
					$(".sch_ts").show();
					$("#search_text").val(searchText);
				} else {
					$(".sch_ts").hide();
					$("#search_text").val('');
				}
				
			    $.ajax({
			        type: "GET",
			        url: "${M_HOME}/ajax/common/learning/pbl/list",
			        data: $("#searchFrm").serialize(),
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var list = data.list;
			        		var totalCnt = data.totalCnt;
				        	var listHtml = '';
				        	$(list).each(function(){
				        		var boardSeq = this.board_seq;
				        		var title = this.title;
				        		var userName = this.name;
				        		var departName = this.user_department_name;
				        		var userLevel = this.user_level;
				        		var userPicturePath = this.user_picture_path;
				        		var cateCode = this.board_cate_code;
				        		var regDate = this.reg_date;
				        		if (userPicturePath == "") {
				        			userPicturePath = "${DEFAULT_M_PICTURE_IMG}";
				        		} else {
				        			userPicturePath = "${RES_PATH}"+userPicturePath;
				        		}
				        		if (Number(userLevel) > 2) {
				        			userName = '<span class="ssp_s1">'+userName+'</span><span class="sign">(</span><span class="ssp_s2">'+departName+'</span><span class="sign">)</span>';
				        		} else {
				        			userName = '<span class="ssp_s1">manajemen자</span><span class="sign"></span><span class="ssp_s2"></span><span class="sign"></span>';
				        		}
				        		
				        		listHtml += '<tr onclick="boardDetail('+boardSeq+')">';
				        		listHtml += '	<td>';
				        		listHtml += '		<div class="box_s w3">';
				        		if (cateCode == "96") {
				        			listHtml += '		<span class="tts">공지</span>';
				        		}
				        		listHtml += '			<span class="tt">' + title + '</span>';
				        		listHtml += '	  	</div>';
				        		listHtml += '	  	<div class="w3">	  ';
				        		listHtml += '        	<div class="box_s w1">					';
				        		listHtml += '	        	<span class="ssp_num">' + regDate + '</span>';
				        		listHtml += '          	</div>';
				        		listHtml += '			<div class="w2">';
				        		listHtml += '				<div class="a_mp">	';
				        		listHtml += '					<div class="ssp_wrap">'+userName+'</div>';
				        		listHtml += '					<div class="pt01"><img src="'+userPicturePath+'" alt="registrasi된 사진 이미지" class="pt_img"></div>';
				        		listHtml += '				</div>';
				        		listHtml += '			</div>';
				        		listHtml += '		</div>';
				        		listHtml += '	</td>';
				        		listHtml += '</tr>';
				        		
				        	});
			        		$("#boardTotalCnt").html(totalCnt);
				        	if (list.length > 0) {
				        		$("#boardList").html(listHtml);
					        	$("#footerNationArea").attr("class", "");
					        	$("#footerNationArea").addClass("pagination");
					        	$("#footerNationArea").html(data.pageNav);
				        	} else {
				        		$("#boardList").html('<td><div class="box_s w3"><span class="tt">registrasi된 게시글이 없습니다.</span></div><div class="w3"></div></td>');
				        	}
			        	} else {
			        		//TODO
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function boardDetail(boardSeq) {
				if (typeof boardSeq !=  "undefined" && boardSeq != "") {
					location.href = '${M_HOME}/common/learning/pbl/detail?seq='+boardSeq
				}
			}
		</script>
	</head>
	<body class="color1">
		<form id="searchFrm" name="searchFrm" method="get" onsubmit="return false;">
			<input type="hidden" id="page" name="page" value="">
			<input type="hidden" id="search_text" name="search_text" value="">
		</form>
		<div class="contents">
			<div class="mtb_wrap">
				<div class="rfrsch">
				    <input class="ip_search" id="boardSearchText" placeholder="제목 + isi Pencarian" type="text">
				    <button class="btn_search1" onclick="pblListView(1);">Pencarian</button>
				</div>
						
				<div class="sch_wrap3">
					<div class="wrap">
					    <span class="swrap">
							<span class="sch_ts">Pencarian결과</span><span>총</span><span class="num" id="boardTotalCnt">0</span><span>건</span>
					    </span>
						<div class="renewwrap"><button class="renew" onclick="pblListView(1);"></button></div>
<!-- 						<button class="regi" onClick="location.href='cc_rfr_r1.html'">registrasi</button> -->
					</div>
				</div>
				<div class="rfrtb_wrap">	
					<table class="rfrtb1" id="boardList">
					</table>
				</div>
			</div>	
		</div>
	</body>
</html>
