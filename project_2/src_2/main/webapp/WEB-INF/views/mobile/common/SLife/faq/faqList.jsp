<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script src="${JS}/lib/jquery.ui.accordion.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				boardHearderInit();
				$("body").addClass("faq");
				boardListView(1);
			});
			
			function accordionInit() {
				$("#faqList").accordion({collapsible: true, active: false}).accordion("refresh");
			}
			
			
			function boardListView(page) {
				if (typeof page == "undefined") {
					page = 1;
				}
				$("input[name='page']").val(page);
				var searchText = $("#boardSearchText").val();
				if (!isEmpty(searchText) && !isBlank(searchText)){
					$(".sch_ts").show();
					$("#search_text").val(searchText);
				} else {
					$(".sch_ts").hide();
					$("#search_text").val('');
				}
				
			    $.ajax({
			        type: "GET",
			        url: "${M_HOME}/ajax/common/SLife/faq/list",
			        data: $("#searchFrm").serialize(),
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var list = data.list;
			        		var totalCnt = data.totalCnt;
				        	var listHtml = '';
				        	$(list).each(function(){
				        		var title = this.title;
				        		var content = this.content;
				        		listHtml += '<button class="acc"><span class="sp01">' + title + '</span></button>';
				        		listHtml += '<div class="panel acc1"><div class="d_wrap1">' + content + '</div></div>';
				        	});
				        	if (list.length > 0) {
				        		$("#faqList").html(listHtml);
					        	accordionInit();
				        	} else {
				        		$("#faqList").addClass("regix_custom");
				        	}
			        	} else {
			        		$("#qnaList").addClass("regix_custom");
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
		</script>
	</head>
	<body class="color1">
		<form id="searchFrm" name="searchFrm" method="get" onsubmit="return false;">
			<input type="hidden" id="page" name="page" value="">
			<input type="hidden" id="search_text" name="search_text" value="">
		</form>
		<div class="contents">
			<div class="mtb_wrap">
				<div class="rfrsch">
				    <input class="ip_search" id="boardSearchText" placeholder="제목 + isi Pencarian" type="text">
				    <button class="btn_search1" onclick="boardListView(1);">Pencarian</button>
				</div>
				<div class="mtb_wrap accwrap " id="faqList">
				</div>
			</div>	
		</div>
	</body>
</html>
