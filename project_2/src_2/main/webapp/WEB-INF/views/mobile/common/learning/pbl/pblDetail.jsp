<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function() {
				boardHearderInit();
				$("body").addClass("rfr").addClass("v1");
				$("div.sch_f1").remove();
				getBoardDetail();
			});
			
			function getBoardDetail() {
				$.ajax({
			        type: "GET",
			        url: "${M_HOME}/ajax/common/learning/pbl/detail",
			        data: {
			        	"board_seq":"${seq}"
			        },
			        dataType: "json",
			        success: function(data, status) {
			        	if (data.status == "200") {
			        		var pblInfo = data.pbl_info;
			        		var attachList = data.attach_list;
			        		var userName = pblInfo.name;
			        		var userLevel = pblInfo.user_level;
			        		var departName = pblInfo.user_department_name;
			        		var userPicturePath = pblInfo.user_picture_path;
			        		
			        		if (userPicturePath == "") {
			        			userPicturePath = "${DEFAULT_M_PICTURE_IMG}";
			        		} else {
			        			userPicturePath = "${RES_PATH}"+userPicturePath;
			        		}
			        		
			        		if (Number(userLevel) > 2) {
			        			userName = '<div class="ssp_wrap"><span class="ssp_s1">'+userName+'</span><span class="sign">(</span><span class="ssp_s2">'+departName+'</span><span class="sign">)</span></div>';
			        		} else {
			        			userName = '<div class="ssp_wrap"><span class="ssp_s1">manajemen자</span><span class="sign"></span><span class="ssp_s2"></span><span class="sign"></span></div>';
			        		}
			        		
			        		var headerHtml =  '<div class="a_mp">';
			        		headerHtml += '	<div class="pt01"><img src="'+userPicturePath+'" alt="registrasi된 사진 이미지" class="pt_img"></div>'; 
		        			headerHtml += userName
	        				headerHtml += '</div>';
	        				
	        				//Target
			        		var showTarget = pblInfo.show_target;
			        		var targetSpan = "";
			        		if (showTarget == "00") {
			        			targetSpan = '<span class="t_s1_all">Keseluruhan</span>';
			        		} else if (showTarget == "02") {
		        				targetSpan = '<span class="t_s1_all">학생 - Keseluruhan</span>';
			        		} else if (showTarget == "03") {
			        			var targetList = data.target_list;
			        			if (targetList.length > 0) {
			        				$(targetList).each(function(idx) {
			        					targetSpan += '<span class="t_s6_st">' + this.target_name + '</span>';
			        				});
			        			}
			        		}
			        		
			        		if (targetSpan != "") {
			        			$("#showTargetArea").append(targetSpan);
			        		}
	        				
			        		$("div.top_title").html(headerHtml);
			        		$("#title").html(pblInfo.title);
			        		$("#regDate").html(pblInfo.reg_date);
			        		$("#content").html(pblInfo.content);
			        		$("#hits").html(pblInfo.hits);
			        		$(".sch_ts").show();
			        		
			        		if (attachList.length > 0) {
			        			var attachHtml = '';
				        		$(attachList).each(function() {
			        				attachHtml += '<div class="wrap_s dw">';
				        			if(this.attach_type == "I") {
			    	        			attachHtml += '	<div class="preview1"><img src="${RES_PATH}'+this.file_path+'" alt="미리보기 이미지"></div>';
			            				attachHtml += '	<div class="sp_wrap"><span class="sp_1">' + this.file_name + '</span><span class="sp_2">사진</span></div>';
				        			} else {
						        		attachHtml += '	<div class="sp_wrap"><span class="sp_1">' + this.file_name + '</span></div>';
				        			}
					        		attachHtml += '	<button class="dw" onclick="fileDownload(\'${HOME}\', \'${RES_PATH}'+this.file_path+'\',  \''+this.file_name+'\')"></button>';
					        		attachHtml += '</div>';
				        		});
				        		$("#attachListArea").html(attachHtml);
				        		$("#attachCnt").html(attachList.length);
			        		} else {
			        			$("#attachListArea").html("");
			        			$("#attachCnt").parent().remove();
			        		}
			        	} else {
			        		alert("PBL 자료 불러오기에 실패하였습니다. [" + data.status + "]");
			        		location.href="${M_HOME}/common/learning/pbl/list";
			        	}
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
		</script>
	</head>
	<body class="color1">
		<div class="contents">
			<div class="mtb_wrap">
				<span class="tth" id="title"></span>
				<div class="sch_wrap4">
					<span class="ssp_num" id="regDate"></span>
					<div class="wrap">
						<span class="swrap"><span class="sch_ts">조회</span><span class="num" id="hits">0</span><span>건</span></span>
					</div>
				</div>
				<div class="h_list">
	            	<div class="wrap" id="showTargetArea">
						<span class="t1"></span>
	            	</div>
				</div>	
				<div class="rfrform_wrap">	
					<div class="con" id="content"></div>
					<div class="sch_wrap5">
						<div class="wrap">
							<span class="swrap"><span>총</span><span class="num" id="attachCnt">0</span><span class="numt">건</span><span>의 자료 업로드</span></span>
						</div>
					</div>
					<div class="box_s" id="attachListArea"></div>
				</div>
			</div>
		</div>
	</body>
</html>