<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<style>
	.wrap1_uselectbox1{width: 419px;padding: 0 !important;margin: 0;max-height: 40px;height: 40px !important;line-height: 40px;box-sizing: border-box;display: inline-block;border: solid 1px rgba(83, 173, 243, 1);background: rgba(249, 253, 255, 1);float: left;}
	.wrap1_uselectbox1:hover{border: solid 1px rgba(0, 171, 216, 1);background:rgba(227, 246, 255, 1);color:rgba(1, 96, 121,1);box-sizing: border-box;transition:.7s;}	
	.wrap1_uselectbox1 div.uselectbox{position: relative;top: 0;height: 3px;left: 0;display:inline-block;width:100%;cursor:pointer;text-align:left;clear:both;float:left;margin: 0;padding: 0;border: 0px;box-sizing: border-box;}	
	.wrap1_uselectbox1 span.uselected{width: 90%;background: rgba(255, 255, 255, 0);overflow:hidden;position:relative;top: 0px;float:left;height: 40px;line-height: 38px;font-size: 15px;z-index:1;color: rgba(36, 116, 185, 1);text-align:left;text-indent: 15px;padding:0;margin: 0;box-sizing: border-box;}	
	.wrap1_uselectbox1 span.uarrow{position:relative;top: 0;text-indent: 0;display: inline-block;width: 10%;float: right;height: 39px;line-height: 39px;z-index:1;box-sizing: border-box;text-align: left;font-size: 11px;background: rgba(255, 255, 255, 0);padding: 0;margin: 0;color: rgba(119, 181, 234, 1);}			
	.wrap1_uselectbox1 div.uoptions{position:absolute;top: 38px;left: -1px;width: 100%;height: auto;max-height: 145px;line-height: 38px;border: solid 1px rgba(83, 173, 243, 1);border-bottom-right-radius:5px;border-bottom-left-radius:5px;overflow-x: hidden;overflow-y: auto;background: rgb(236, 246, 255);padding: 0;display:none;color:rgba(1, 96, 121,1);opacity: 1;box-sizing: content-box;z-index: 9999;}	
	.wrap1_uselectbox1 span.uoption{position:relative;top: 0px;left:-1px;display:block;width: 100%;height: 40px;line-height: 40px;font-size: 15px;margin: -2px 0 0 0;padding:0 1px 0 1px;text-align:left;text-indent: 15px;border-bottom: solid 1px rgba(255, 255, 254, 1);opacity: 1;color: rgba(85, 85, 85, 1);}	
	.wrap1_uselectbox1 span.uoption:hover{color:#fff;background: rgba(117, 161, 216, 1);transition:.7s;}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		bindPopupEvent("#m_pop1", ".open1");
		getStList();
		getCurriculum();
		getSoosiGradeList();
	});
	
	//교육과정 가져오기
	function getCurriculum() {
		$.ajax({
			type : "GET",
			url : "${HOME}/ajax/pf/lesson/curriculumInfo",
			data : {},
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {

					$("span[data-name=curr_name]").text(data.basicInfo.curr_name);
					$("span[data-name=aca_system_name]").text("(" + data.basicInfo.aca_system_name + ")");
                    
				} else {

				}
			},
			error : function(xhr, textStatus) {
				document.write(xhr.responseText);
			}
		});
	}
	
	function gradeNumCheck(obj){				
		
		if(isNaN(obj.value) || isEmpty(obj.value)){
			$(obj).val("");
			$(obj).focus("");
		}else{			
			$(obj).val($(obj).val().replace(/ /g,""));
		}

		var question_cnt = 0;
		
		var src_seq = $(obj).closest("td").attr("name");
		
		question_cnt = parseInt($(obj).closest("tr").find("td[name="+src_seq+"] input[name=question_cnt]").val());
						
		if(question_cnt < parseInt($(obj).val())){
			alert("총문항 보다 정답수가 클 수 없습니다.");
			$(obj).val("");
			$(obj).focus();
			return;
		}				
	}
	
	function getSoosiGradeList(){
			
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/soosiGrade/create/gradelist",
            data: {
            	"curr_seq" : "${S_CURRICULUM_SEQ}"
            },
            dataType: "json",
            success: function(data, status) {
            	var htmls = '<span class="uoption" data-value="">신규 registrasi</span>';
           		$.each(data.soosiList, function(index){
					htmls += '<span class="uoption" data-value="'+this.src_seq+'">'+this.src_name+'</span>';
           		});         
           		$("#srcSeqListAdd").html(htmls);
            },
            error: function(xhr, textStatus) {
                alert("오류가 발생했습니다.");
            }
        });
	}
	
	function getStList() {
		
		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/pf/lesson/soosiGrade/create/list",
            data: {
            	"st_id" : $("#st_id").val()
            	,"curr_seq" : "${S_CURRICULUM_SEQ}"
            	,"st_name" : $("#st_name").val()
            	,"aca_seq" : "${acaState.aca_seq}"
            },
            dataType: "json",
            success: function(data, status) {
            	
            	if(data.status=="200"){
            		$("#scoreListAdd").empty();
            		$("#stListAdd").empty();
	            	$("span.g_num").text(data.stList.length);
	            	var defaultTitleHtml = "";
	            	var srNoHtml = "";
	            	var srNameHtml = "";
	            	var srDateHtml = "";
	            	var srInfoHtml = "";
	            		
	            	defaultTitleHtml='<tr>';	
					srNameHtml='<tr>';
					srDateHtml='<tr>';
					srInfoHtml='<tr>';
					
					$.each(data.soosiList, function(index){
						defaultTitleHtml+='<th colspan="5" class="color5 th01 ths2 b_1 br1 w5n">'+(index+1)+'차</th>';
						srNameHtml+='<th colspan="5" class="color5 th01 ths2 b_1 br1 w5n">'+this.src_name+'</th>';
						srDateHtml+='<th colspan="5" class="color5 th01 ths2 b_1 br1 w5n">'+this.src_date+'</th>';
						srInfoHtml+='<th class="color5 th01 ths1 b_1 w1n">총문항</th>'
							+'<th class="color5 th01 ths1 b_1 w1n">정답수</th>'
							+'<th class="color5 th01 ths1 b_1 w1n">점수</th>'
							+'<th class="color5 th01 ths1 b_1 w1n">평균</th>'
							+'<th class="color5 th01 ths1 b_1 br1 w1n">편차</th>';							
					});						
					
					defaultTitleHtml+='</tr>';
					srNameHtml+='</tr>';
					srDateHtml+='</tr>';
					srInfoHtml+='</tr>';
	            
					$("#titleListAdd").html(defaultTitleHtml+srNameHtml+srDateHtml+srInfoHtml);

					var htmls = "";
					$.each(data.stList , function(index){
						var picture = this.picture_path;
						if(isEmpty(picture))
							picture = "${IMG}/ph_3.png";
						else
							picture = "${RES_PATH}"+this.picture_path;
							
						htmls='<tr name="'+this.id+'">'
						+'<td class="td_1">'+(index+1)+'<input type="hidden" name="user_seq" value="'+this.user_seq+'"></td>'
						+'<td class="td_1">'+this.id+'</td>'
						+'<td class="td_1 t_l">'
						+'<span class="a_mp">'
						+'<span class="pt01">'
						+'<img src="'+picture+'" alt="registrasi된 사진 이미지" class="pt_img">'
						+'</span>'
						+'<span class="ssp1">'+this.name+'</span>'
						+'</span>'
						+'</td>'
						+'<td class="td_1">'+this.group_number+'</td></tr>';
						
						$("#stListAdd").append(htmls);
						htmls='<tr name="'+this.id+'">';
						$.each(data.soosiList, function(){
							htmls+='<td class="td_1" name="'+this.src_seq+'"><input type="text" class="ip03" readonly name="question_cnt" value=""></td>'
								+'<td class="td_1" name="'+this.src_seq+'"><input type="text" class="ip03" name="answer_cnt" onKeyup="gradeNumCheck(this);" value=""></td>'
								+'<td class="td_1" name="'+this.src_seq+'"><input type="text" class="ip03" name="score" onKeyup="onlyNumCheck(this);" value=""></td>'
								+'<td class="td_1" name="'+this.src_seq+'"><input type="text" class="ip03" readonly name="avg_score" value=""></td>'
								+'<td class="td_1 br1" name="'+this.src_seq+'"><input type="text" class="ip03" readonly name="deviation" value=""></td>';								
						});
						htmls+="</tr>";

						$("#scoreListAdd").append(htmls);
					});
					
					$.each(data.scoreList, function(index){
						$("#scoreListAdd tr[name="+this.id+"]").find("td[name="+this.src_seq+"]").find("input[name=question_cnt]").val(this.question_cnt);
						$("#scoreListAdd tr[name="+this.id+"]").find("td[name="+this.src_seq+"]").find("input[name=answer_cnt]").val(this.answer_cnt);
						$("#scoreListAdd tr[name="+this.id+"]").find("td[name="+this.src_seq+"]").find("input[name=score]").val(this.score);
						$("#scoreListAdd tr[name="+this.id+"]").find("td[name="+this.src_seq+"]").find("input[name=avg_score]").val(this.avg_score);
						$("#scoreListAdd tr[name="+this.id+"]").find("td[name="+this.src_seq+"]").find("input[name=deviation]").val(this.deviation);
					});
            	}
            },
            error: function(xhr, textStatus) {
                alert("오류가 발생했습니다.");
            },
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
        });
	}
	
	function submitGrade(){
		
		if(!confirm("simpan하시겠습니까?"))
			return;
		
		var arrayList = new Array();
		
		$.each($("#scoreListAdd tr"), function(index){
			var listInfo = new Object();
			var id = $(this).attr("name");
			var user_seq = $("#stListAdd tr:eq("+index+")").find("input[name=user_seq]").val();
			var src_seq = "";
			var answer_cnt = "";
			var score = "";
			var src_length = $(this).find("input[name=answer_cnt]").length;
			$.each($(this).find("input[name=answer_cnt]"), function(dIndex){
				src_seq += $(this).closest("td").attr("name");
				answer_cnt += $(this).val(); 
				score += $(this).closest("tr").find("td[name="+$(this).closest("td").attr("name")+"]").find("input[name=score]").val();
				if(src_length != (dIndex+1)){
					src_seq+="|";
					answer_cnt+="|";
					score+="|";
				}						
			});
			
			listInfo.user_seq=user_seq;
			listInfo.src_seq=src_seq;
			listInfo.answer_cnt=answer_cnt;
			listInfo.score=score;
			arrayList.push(listInfo);					
		});

		var scoreList = new Object();
		scoreList.list = arrayList;
		
		var jsonInfo = JSON.stringify(scoreList);

		$.ajax({
            type: "POST",
            url: "${HOME}/ajax/admin/academicReg/soosiScore/create/insert",
            data: {   
            	"scorelist" : jsonInfo
            	,"curr_seq" : "${S_CURRICULUM_SEQ}"
            },
            dataType: "json",
            success: function(data, status) {
            	if(data.status == "200"){
            		getStList();
	            	alert("simpan 완료 되었습니다.");	            	
            	}else{
            		alert("simpan 실패 하였습니다.");
            	}
            },
	        beforeSend:function() {
	            $.blockUI();
	        },
	        complete:function() {
	            $.unblockUI();
	        },
            error: function(xhr, textStatus) {
                //alert("오류가 발생했습니다.");
                //document.write(xhr.responseText);
            }
        }); 
	}
				
	function file_nameChange(){    
	    if($("#xlsFile").val() != ""){
	        var fileValue = $("#xlsFile").val().split("\\");
	        var fileName = fileValue[fileValue.length-1]; // 파일명
	        
	        var fileLen = fileName.length; 
	        var lastDot = fileName.lastIndexOf('.'); //마지막 . 위치 가져온다.
	        var fileExt = fileName.substring(lastDot+1, fileLen).toLowerCase(); //마지막 . 위치로 확장자 자름

	        if(fileExt != "xlsx") {
	        	alert("xlsx 파일을 registrasi해주세요");
	        	$("#xlsFile").val("");
	        	return;			        	
	        }
	        
	        $("#xls_filename").val(fileName);
	    }
	}
	
	
	function excelSubmit() {

		if (isEmpty($("#xlsFile").val())) {
			alert("파일을 pilih해주세요");
			return;
		}
		var form = document.getElementById("xlsForm");
		
		var hiddenField = document.createElement("input");
			
		hiddenField.type = "hidden";
		hiddenField.name = "curr_seq";
		hiddenField.value = "${S_CURRICULUM_SEQ}";
		
		var srcSeqField = document.createElement("input");
		
		srcSeqField.type = "hidden";
		srcSeqField.name = "src_seq";
		srcSeqField.value = $("#src_seq").attr("data-value");
		
		form.appendChild(hiddenField);
		form.appendChild(srcSeqField);
		
		$("#xlsForm").ajaxForm({
			type : "POST",
			url : "${HOME}/ajax/admin/academicReg/soosiScore/excel/create",
			dataType : "json",
			success : function(data, status) {
				if (data.status == "200") {
					alert("엑셀업로드가 완료 되었습니다.");
					$("#xlsFile").val("");
					$("#xls_filename").val("");
					getStList();
				} else {
					alert(data.status);
				}
			},
			error : function(xhr, textStatus) {
				alert(textStatus);
				//document.write(xhr.responseText);
				$.unblockUI();
			},
			beforeSend : function() {
				$.blockUI();
			},
			complete : function() {
				$.unblockUI();
			}
		});
		$("#xlsForm").submit();
	}
		
	function excelTmplDown(){
		location.href='${HOME}/pf/lesson/soosiGrade/TmplExcelDown?curr_seq=${S_CURRICULUM_SEQ}&aca_seq=${acaState.aca_seq}';
	}
	
	function excelDown(){
		var gradeArray = new Array();
		var currArray = new Array();
		
		$.each($("#stListAdd tr"), function(index){
			var listInfo = new Object();
			listInfo.id = $(this).find("td").eq(1).text();
			listInfo.name = $(this).find("td").eq(2).text();
			listInfo.groupNum = $(this).find("td").eq(3).text();
			var score = "";
			
			//학생꺼 점수들 가져온다.
			$.each($("#scoreListAdd tr:eq("+index+") td"), function(sIndex){
				score += $(this).find("input").val();
				
				//마지막 데이터 빼고 구분자 넣어준다.
				if($("#scoreListAdd tr:eq("+index+") td").length != (sIndex+1))
					score += "|";
			});
			listInfo.grade = score;
			gradeArray.push(listInfo);							
		});

		var no = "";
		var curr_name = "";
		var srcDate = "";
		$.each($("#titleListAdd tr:eq(0) th"), function(index){
			no += $(this).text();
			
			if($("#titleListAdd tr:eq(0) th").length != (index+1))
				no += "|";
		});
		
		$.each($("#titleListAdd tr:eq(1) th"), function(index){
			curr_name += $(this).text();
			
			if($("#titleListAdd tr:eq(1) th").length != (index+1))
				curr_name += "|";
		});
		
		$.each($("#titleListAdd tr:eq(2) th"), function(index){
			srcDate += $(this).text();
			
			if($("#titleListAdd tr:eq(2) th").length != (index+1))
				srcDate += "|";
		});
		
		var scoreList = new Object();
		scoreList.list = gradeArray;
		
		var jsonInfo = JSON.stringify(scoreList);
		
		post_to_url("${HOME}/admin/academic/academicReg/soosiScore/create/excelDown", {"stList":jsonInfo, "no":no, "curr_name" : curr_name, "srcDate":srcDate});			
	}
</script>
</head>

<body>

	<!-- s_tt_wrap -->
	<div class="tt_wrap">
		<h3 class="am_tt">
			<span class="tt" data-name="curr_name"></span>
			<span class="tt_s" data-name="aca_system_name"></span>
		</h3>
	</div>
	<!-- e_tt_wrap -->

	<!-- s_tab_wrap_cc -->
	<div class="tab_wrap_cc">
		<!-- s_해당 탭 페이지 class에 active tambahkan -->
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson'">rencan kurikulum</button>	         
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/schedule'">waktu표manajemen</button>	
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/unit'">단원manajemen</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/lessonPlanRegCs'">penambahan bab pembelajaran</button>
		<button class="tab01 tablinks" onclick="">만족도 조사</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/currAssignMent'">종합 과제</button>
		<button class="tab01 tablinks active" onclick="location.href='${HOME}/pf/lesson/soosiGrade'">수시 성적</button>
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/grade'">종합 성적</button>	
		<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/report'">운영보고서</button>
		<!-- e_해당 탭 페이지 class에 active tambahkan -->
	</div>
	<!-- e_tab_wrap_cc -->

	<!-- s_mpf_tabcontent4 -->
	<div class="mpf_tabcontent4 grd">

		<!-- s_tt_wrap -->
		<div class="tt_wrap3">
			<button class="btn_up1 open1" style="float:left;">엑셀일괄registrasi</button>
			<button class="btn_dw3" onClick="excelDown();" style="float:left;">성적 다운로드</button>

			<div class="sch_wrap" style="float:right;">
				<input type="text" class="ip_search2" placeholder="학번" id="st_id"
					onkeypress="javascript:if(event.keyCode==13){getStList(); return false;}">
				<input type="text" class="ip_search1" placeholder="학생 이름" id="st_name"
					onkeypress="javascript:if(event.keyCode==13){getStList(); return false;}">
				<button class="btn_search1" onClick="getStList();"></button>
			</div>
		</div>
		<!-- e_tt_wrap -->

		<!-- s_tt_wrap -->
		<div class="tt_wrap1">

			<div class="tt_g1">
				<span class="g_t2">Pencarian결과</span>
				<span class="g_num"></span>
				<span class="g_t3">명</span>
			</div>

		</div>
		<!-- e_tt_wrap -->

		<div class="scroll_wraps2">
			<div class="fwrap3">
				<table class="mlms_tb1 n2">
					<thead>
						<tr>
							<th class="color1 th01 b_2 w2n">No.</th>
							<th class="color1 th01 bd01 w4n">학번</th>
							<th class="color1 th01 bd01 w4n">학생</th>
							<th class="color1 th01 bd01 w3n">조</th>
						</tr>
					</thead>

					<tbody id="stListAdd">
					</tbody>
				</table>
			</div>
			<div class="fwrap4">
				<table class="mlms_tb1 n2">
					<thead id="titleListAdd">

					</thead>
					<tbody id="scoreListAdd">

					</tbody>
				</table>
			</div>
		</div>

		<div class="bt_wrap">		
			<c:if test='${acaState.aca_state == "03" || acaState.aca_state == "04" || acaState.aca_state == "05" || acaState.aca_state == "06" || acaState.aca_state == "07" }'>
				<button class="bt_2" onclick="submitGrade();">perbaiki</button>
			</c:if>
		</div>

	</div>
	<!-- s_ 엑셀 일괄 registrasi 팝업 -->
	<div id="m_pop1" class="pop_up_ex pop_up_ex_1 mo1">

		<div class="pop_wrap">
			<p class="popup_title">교육과정 nilai yang diinginkan 엑셀업로드</p>

			<button class="pop_close close1" type="button">X</button>

			<button class="btn_exls_down_2" onclick="javascript:excelTmplDown();">엑셀 양식 다운로드</button>

			<div class="t_title_1">
				<span class="sp_wrap_1">엑셀 양식을 다운로드 하시면, 해당 교육과정의 학생 리스트가
					automatic으로 기입된 양식을 확인하실 수 있습니다.<br>해당 학생의 과목별 점수를 기입하셔서 업로드 해 주세요
				</span>
			</div>

			<div class="pop_ex_wrap">
			
				<form id="xlsForm" onSubmit="return false;">
					<div class="sub_fwrap_ex_1">
						<span class="ip_tt">registrasi 구분</span>
						<div class="wrap1_uselectbox1">
							<div class="uselectbox" >
								<span class="uselected" id="src_seq" data-value="">신규 registrasi</span> <span class="uarrow">▼</span>
								<div class="uoptions" style="display: none;" id="srcSeqListAdd">
								</div>
							</div>
						</div>
					</div>
					<div class="sub_fwrap_ex_1"> 
						<span class="ip_tt">file excel</span> 
						<input type="text" readonly class="ip_sort1_1" id="xls_filename" value="" onClick="$('#xlsFile').click();">
						<button class="btn_r1_1" onClick="$('#xlsFile').click();">파일pilih</button>
						<input type="file" style="display: none;" id="xlsFile" name="xlsFile" onChange="file_nameChange();">
					</div>
					
				
				</form>
				<span class="t_title_2">registrasi파일명 : xlsx</span>

				<!-- s_t_dd -->
				<div class="t_dd">

					<div class="pop_btn_wrap">
						<c:if test='${acaState.aca_state == "03" || acaState.aca_state == "04" || acaState.aca_state == "05" || acaState.aca_state == "06" || acaState.aca_state == "07" }'>
							<button type="button" class="btn01" onclick="excelSubmit();">registrasi</button>
						</c:if>
						<button type="button" class="btn02" onClick="$('.close1').click();">batal</button>
					</div>

				</div>
				<!-- e_t_dd -->

			</div>
		</div>
	</div>
	<!-- e_ 엑셀 일괄 registrasi 팝업 -->
</body>
</html>