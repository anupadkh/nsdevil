<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			$(document).ready(function(){
				getFormationEvaluationList();
			});
			
			function getFormationEvaluationList() {
				$.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/pf/lesson/formationEvaluation/list",
			        data: {},
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	var typeO = 0; //온라인 퀴즈(01)
				        	var typeTotal = data.feList.length;
				        	var typeJGETotal = 0;//J:자필, G:구두, E:dan yang lainnya
				        	
				        	var feTitleInfo = data.feTitleInfo;
				        	
				        	$("#lpDate").html(feTitleInfo.lesson_date);//수업일자
				        	var lpName = ""; // [x pengajar] : 수업계획서주제
				        	var period = feTitleInfo.period; // pengajar
				        	if (period != "") {
				        		lpName += "["+period+" pengajar] : ";
				        	}
				        	var lessonSubject = feTitleInfo.lesson_subject;//수업계획서주제
				        	if (lessonSubject == "") {
				        		lessonSubject = "수업명(미registrasi)";
				        	}
				        	lpName += lessonSubject;
				        	$("#lpName").html(lpName)
				        	var lpCurrculumInfo = "(" + feTitleInfo.curr_name + " / " + feTitleInfo.aca_system_name + ")"; //(Nama Mata Kuliah / 학사정보)
				        	$("#lpCurrculumInfo").html(lpCurrculumInfo);
				        	
				        	
				        	if (feTitleInfo.fe_flag == "N") { //퀴즈 안함
				        		$("#feComent").html('<td class="on">온라인 퀴즈에 한해 서비스를 제공하고 있습니다.<br>교수님께서는 형성penilaian <span class="sp01">[진행안함] </span>으로 수업 계획서를 registrasi해 주셨습니다.<br></td>');
				        	} else {
				        		var html = "";
				        		$.each(data.feList, function() {
				        			var cateCode = this.fe_cate_code;
				        			var testState = this.test_state;
				        			var quizTypeName = this.quiz_type;
				        			var thumbnailTag = this.thumbnail_path;
				        			if (thumbnailTag != "") {
				        				thumbnailTag = '<img src="${RES_PATH}' + thumbnailTag + '" alt="표지 사진" class="pt1">';
				        			} else {
				        				thumbnailTag = ""
				        			}
				        			var quizTotalCnt = this.quiz_total_cnt;
				        			var solveEndQuizCnt = this.solve_end_quiz_cnt;
				        			var lessonDate = feTitleInfo.lesson_date;
				        			if (lessonDate != "") {
				        				lessonDate = (lessonDate).replace("/", "월") + "일";
				        			}
				        			var quizTestDateInfo = lessonDate + " " + period + " pengajar " + feTitleInfo.start_time + "~" + feTitleInfo.end_time; // m/d x pengajar 00:00~00:00
				        			var feName = this.fe_name;
				        			if (feName == "") {
				        				feName = lessonSubject + lessonDate + "퀴즈";
				        			}
				        			if (cateCode != "01") {
				        				html += '<div>';
				        				html += '	<table class="tab_table ttb1">';
				        				html += '		<tbody>';
				        				html += '			<tr class="tr01">';
				        				html += '				<td class="">';
				        				html += '					<span class="tt_1">' + quizTypeName + '</span>';
				        				html += '				</td>';
				        				html += '			</tr>';
				        				html += '			<tr class="tr01">';
				        				html += '				<td class="">';
				        				html += '					<div class="wrap_tt">';
				        				html += '						<span class="sp1">형성penilaian ' + quizTypeName + '는 온라인에서 제공되지 않습니다.</span>';
				        				html += '						<span class="sp2">( 온라인 퀴즈만 registrasi할 수 있습니다. )</span>';
				        				html += '					</div>';
				        				html += '				</td>';
				        				html += '			</tr>';
				        				html += '		</tbody>';
				        				html += '	</table>';
				        				html += '</div>';
				        			} else {
				        				if (testState == "WAIT") {
				        					html += '<div>';
				        					html += '	<table class="tab_table ttb1">';
				        					html += '  		<tbody>';
				        					html += '			<tr class="tr01"><td colspan="2" class=""><span class="tt_1">' + quizTypeName + '</span><span class="tt_2_1">대기</span></td></tr>';
				        					html += '  	      	<tr class="tr01">';
				        					html += '           	<td rowspan="2" class="th02 w1">';
				        					html += '               	<div class="in_wrap">';
				        					html += '                   	<div class="in_wrap_s">'+thumbnailTag+'</div>';
				        					html += '                   	<div class="tt_s">권장사이즈<br>560px X 200px</div>';
				        					html += '                   	<button class="btn06" onclick="javascript:thumbnailUpload(' + this.fe_seq + ');">표지 registrasi</button>';
				        					html += '            		</div>';
				        					html += '				</td>';
				        					html += '				<td class="save">';
				        					html += '               	<input type="text" id="feName" name="feName" class="ip_pt1" placeholder="퀴즈명을 registrasi해 주세요." maxlength="30" value="' + this.fe_name + '"><button class="btn_n1" title="simpan" onclick="javascript:feTitleSave(this, ' + this.fe_seq + ');">simpan</button>';
				        					if (quizTotalCnt > 0) {
				        						html += '               <button class="btn6_2 open4" title="대기 - 총 ' + quizTotalCnt + '문제" onclick="javascript:quizPreviewPopupOpen(' + this.fe_seq + ', false);">대기 - 총 ' + quizTotalCnt + '문제</button>';
				        					} else {
				        						html += '               <button class="btn2" title="문제registrasi 버튼을 눌러 문제를 registrasi해 주세요.">문제가 registrasi되지 않았습니다.</button>';
				        					}
				        					html += '				</td>';
				        					html += '			</tr>';
				        					html += '			<tr class="tr01">';
				        					html += '           	<td class="">';
				        					html += '               	<button class="btn4 open1" onclick="javascript:pastFormationEvaluationPopupOpen(' + this.fe_seq + ');">지난 수업 퀴즈 불러오기</button>';
				        					html += '               	<button class="btn5 open2" onclick="javascript:quizExcelUploadPopupOpen(' + this.fe_seq + ');">퀴즈 엑셀 일괄 업로드</button>';
				        					html += '               	<button class="btn6 open3_1" onclick="javascript:quizCreatePopupOpen(' + this.fe_seq + ', \'' + feName + '\', ' + quizTotalCnt + ');">문제 registrasi</button>';
				        					html += '               	<button class="btn6_1 open4" onclick="javascript:quizListPopupOpen(' + this.fe_seq + ', 0);">문제 목록</button>';
				        					html += '				</td>';
				        					html += '			</tr>';
				        					html += '		</tbody>';
				        					html += '	</table> ';
				        					html += '</div>';
				        				} else if (testState == "START") {
			        						// 퀴즈 진행 상태 백분률 값 = END 퀴즈 수 / 퀴즈 Keseluruhan 개수 * 100
				        					var progression = progression = Math.floor(solveEndQuizCnt/quizTotalCnt*100);
				        					html += '<table class="tab_table ttb1">';
				        					html += '	<tbody>';
				        					html += '		<tr class="tr01"><td colspan="2" class=""><span class="tt_1">' + quizTypeName + '</span><span class="tt_2_2">진행 중</span></td></tr>';
				        					html += '		<tr class="tr01">';
				        					html += '           <td class="th02 w1">';
				        					html += '               <div class="in_wrap">';
				        					html += '                   <div class="in_wrap_s">'+thumbnailTag+'</div>';
				        					html += '               	<div class="tt_s">권장사이즈<br>560px X 200px</div>';
				        					html += '				</div>';
				        					html += '           </td>';
				        					html += '			<td class="">';
				        					html += '               <div class="wrap_tt">';
				        					html += '                   <span class="sp1">' + quizTestDateInfo + '</span>';
				        					html += '                   <span class="sp1">' + feName + '</span>';
				        					html += '                	<button class="btn6_2 open4" title="문제 미리보기 – 총 ' + quizTotalCnt + '문제" onclick="javascript:quizPreviewPopupOpen('+this.fe_seq+', false);">문제 미리보기 – 총 ' + quizTotalCnt + '문제</button>';
				        					html += '                </div>';
				        					html += '                <div class="wrap_prg">';
				        					html += '                	<span class="sp1">풀이현황 : ' + solveEndQuizCnt + ' / ' + quizTotalCnt + '</span>';
				        					html += '               	<div class="prg"><span class="bar" style="width:' + (progression*2) + 'px;"></span></div>';
				        					html += '           	</div>';
				        					html += '       	</td>';
				        					html += '		</tr>';
				        					html += '	</tbody>';
				        					html += '</table>';
				        				} else if (testState == "END"){
				        					html += '<table class="tab_table ttb1">';
				        					html += '	<tbody>';
				        					html += '       <tr class="tr01"><td colspan="2" class=""><span class="tt_1">' + quizTypeName + '</span><span class="tt_2_3">종료</span></td></tr>';
				        					html += '       <tr class="tr01">';
				        					html += '           <td rowspan="2" class="th02 w1">';
				        					html += '               <div class="in_wrap">';
				        					html += '                   <div class="in_wrap_s">'+thumbnailTag+'</div>';
				        					html += '                   <div class="tt_s">권장사이즈<br>560px X 200px</div>';
				        					html += '               </div>';
				        					html += '           </td>';
				        					html += '           <td class="">';
				        					html += '               <div class="wrap_tt">';
				        					html += '                   <span class="sp1">' + quizTestDateInfo + '</span>';
				        					html += '                   <span class="sp1">' + feName + '</span>';
				        					html += '               </div>';
				        					html += '               <button class="btn1"><span>최고점 : ' + this.max_score+ '</span><span>최저점 :  ' + this.min_score+ '</span><span>평균 :  ' + this.avg_score+ '</span></button>';
				        					html += '           </td>';
				        					html += '       </tr>';
				        					html += '       <tr class="tr01">';
				        					html += '           <td class="">';
				        					html += '               <button class="btn8" onclick="javascript:quizResultPopupOpen(' + this.fe_seq + ');">학생별 점수 확인</button>';
				        					html += '               <button class="btn7" onclick="javascript:resultExcelDownload(' + this.fe_seq + ');">결과 엑셀 다운로드</button>';
				        					html += '               <button class="btn6_1 open4" onclick="javascript:quizPreviewPopupOpen(' + this.fe_seq + ', false);">문제 보기</button>';
				        					html += '           </td>';
				        					html += '       </tr>';
				        					html += '   </tbody>';
				        					html += '</table>';
				        				}
				        				
				        				typeO++;
				        			}
				        		});
				        		typeJGETotal = typeTotal - typeO;
				        		var feComent = '<td class="on">교수님께서는 형성penilaian를<span class="sp01">총 ' + typeTotal + '회</span>registrasi하셨습니다.';
				        		if (typeJGETotal > 0) {
				        			feComent = '<td class="on">교수님께서는 형성penilaian를<span class="sp01">총 ' + typeTotal + '회</span>registrasi하셨습니다. <br>온라인 퀴즈 외 퀴즈 ' + typeJGETotal + '회는 온라인 제공되지 않습니다.</td>';
				        		}
				        		$("#feComent").html(feComent);
				        		$("#quizListArea").append(html);
				        	}
			            } else {
			            	alert("형성penilaian 불러오기에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function quizCreatePopupOpen(feSeq, feName, currentQuizNumber) {
				$("#quizCreatePopup .t_title").html('문제 registrasi (' + feName + ')');
				$("#quizCreatePopup #quizQuestion").val("");
				$("#quizCreatePopup .ip_search").val("");
				$("#imgPreviewArea").html("");
				$("#answerViewCntText").html("5");
				$("#correctAnswerText").html("");
				$("#quizCreateFrm").find("#feSeq").val(feSeq);
				$("#quizCreateFrm").find("#quizAttachFile").remove();
				$("#quizCreateFrm").append('<input type="file" id="quizAttachFile" name="quizAttachFile" class="blind-position">');
				answerAreaInit("#quizCreatePopup #answerArea");
				$("#currentQuizNumber").html(++currentQuizNumber);
				$("#quizOrder").val(currentQuizNumber);
				$("#quizCreatePopup").show();
			}
			
			function answerAreaInit(target) {
				var answerAreaHtml = "";
				answerAreaHtml += '<tr>';
				answerAreaHtml += '		<td class="w1 correctAnswerNumber">1</td>';
				answerAreaHtml += '		<td class="free_textarea w2_tarea"><textarea class="tarea02" name="answerContent1" style="height: 40px;"></textarea></td> ';
				answerAreaHtml += '		<td class="w3"><input type="checkbox" class="chk_2"></td>';
				answerAreaHtml += '</tr>';
				answerAreaHtml += '<tr>';
				answerAreaHtml += '		<td class="w1 correctAnswerNumber">2</td>';
				answerAreaHtml += '		<td class="free_textarea w2_tarea"><textarea class="tarea02" name="answerContent2" style="height: 40px;"></textarea></td> ';
				answerAreaHtml += '		<td class="w3"><input type="checkbox" class="chk_2"></td>';
				answerAreaHtml += '</tr>';
				answerAreaHtml += '<tr>';
				answerAreaHtml += '		<td class="w1 correctAnswerNumber">3</td>';
				answerAreaHtml += '		<td class="free_textarea w2_tarea"><textarea class="tarea02" name="answerContent3" style="height: 40px;"></textarea></td>';
				answerAreaHtml += '		<td class="w3"><input type="checkbox" class="chk_2"></td>';
				answerAreaHtml += '</tr>';
				answerAreaHtml += '<tr>';
				answerAreaHtml += '		<td class="w1 correctAnswerNumber">4</td>';
				answerAreaHtml += '		<td class="free_textarea w2_tarea"><textarea class="tarea02" name="answerContent4" style="height: 40px;"></textarea></td>';
				answerAreaHtml += '		<td class="w3"><input type="checkbox" class="chk_2"></td>';
				answerAreaHtml += '</tr>';
				answerAreaHtml += '<tr>';
				answerAreaHtml += '		<td class="w1 correctAnswerNumber">5</td>';
				answerAreaHtml += '		<td class="free_textarea w2_tarea"><textarea class="tarea02" name="answerContent5" style="height: 40px;"></textarea></td>';
				answerAreaHtml += '		<td class="w3"><input type="checkbox" class="chk_2"></td>';
				answerAreaHtml += '</tr>';
				$(target).html(answerAreaHtml);
				autosize($("textarea"));
			}
			
			//문제 registrasi 팝업 닫기
			function quizCreatePopupClose() {
				$("#quizCreatePopup").hide();
			}
			
			function textareaHeightResize(t, h) {
        		$(t).height(h+"px");
			}
			
			//문제 registrasi 팝업 답가지 순서변경
			function answerOrderUpDown(selector, flag) {
				var orderUp = flag;
				var checkedCnt = $(selector + " #answerArea").find("input:checkbox:checked").length;
				var checkBoxTotalCnt = $(selector + " #answerArea").find("input:checkbox").length;
				if (checkedCnt == 0 || checkedCnt > 1) {
					alert("답가지를 1개만 체크해주세요.");
					return false;
				}
				
				var checkedTarget = $(selector + " #answerArea").find("input:checkbox:checked");
				var numberTarget = $(checkedTarget).parent().parent().find(".correctAnswerNumber");
				var currIdx = parseInt($(numberTarget).text());
				if ($(numberTarget).hasClass("on")) {
					changeAnswerFlag = true;
				}

				if (currIdx == 1 && orderUp == true) {
					alert("첫번째 답가지입니다. 위로 이동할 수 없습니다.");
					return false;
				}
				
				if (currIdx == checkBoxTotalCnt && orderUp == false) {
					alert("마지막 답가지입니다. 아래로 이동할 수 없습니다.");
					return false;
				}
				
				var currTarget = "";
				var moveTarget = "";
				
				if (orderUp == true) {
					currTarget = $(selector + " #answerArea > tr:eq("+ (currIdx-1) +")");
					moveTarget = $(selector + " #answerArea > tr:eq("+ (currIdx-2) +")");
					currIdx = currIdx-2;
				} else {
					currTarget = $(selector + " #answerArea > tr:eq("+ (currIdx-1) +")");
					moveTarget = $(selector + " #answerArea > tr:eq("+ (currIdx) +")");
				}
				
				var currTextarea = $(currTarget).find("textarea");
				var moveTextarea = $(moveTarget).find("textarea");
				
				var currTargetText = $(currTextarea).val();
				var moveTargetText = $(moveTextarea).val();

				var currTargetHeight = $(currTextarea).height();
				var moveTargetHeight = $(moveTextarea).height();
				
				$(currTextarea).val(moveTargetText);
				$(moveTextarea).val(currTargetText);
				
				$(selector + " #answerArea").find("input:checkbox:checked").prop("checked", false);
				
				$(selector + " #answerArea").find("input:checkbox:eq("+ (currIdx) +")").prop("checked", true);
				
				var currHasClass = $(currTarget).find(".correctAnswerNumber").hasClass("on");
				var moveHasClass = $(moveTarget).find(".correctAnswerNumber").hasClass("on");
				
				if (!currHasClass && moveHasClass) {
					$(currTarget).find(".correctAnswerNumber").addClass("on");
					$(moveTarget).find(".correctAnswerNumber").removeClass("on");
				} else if (currHasClass && !moveHasClass) {
					$(currTarget).find(".correctAnswerNumber").removeClass("on");
					$(moveTarget).find(".correctAnswerNumber").addClass("on");
				}
				
				textareaHeightResize(currTextarea, moveTargetHeight);
				textareaHeightResize(moveTextarea, currTargetHeight);

				correctAnswerTextSetting(selector);
			}
			
			//정답 텍스트 세팅
			function correctAnswerTextSetting(selector) {
				var correctAnswerText = "";
				$(selector + " .correctAnswerNumber.on").each(function(){
					correctAnswerText = correctAnswerText + $(this).html()+", "; 
				});
				if (correctAnswerText.length > 0) {
					correctAnswerText = correctAnswerText.substring(0, correctAnswerText.length-2);
				}
				$(selector + " #correctAnswerText").html(correctAnswerText);
			}
			
			//정답 넘버링, textarea naming 세팅
			function answerViewReSortSetting(selector) {
				var cnt = 0;
				$(selector + " #answerArea > tr").each(function(){
					cnt++;
					$(this).find(".correctAnswerNumber").html(cnt);
					$(this).find("textarea").attr("name", "answerContent"+cnt);
				});
				$(selector + " #answerViewCntText").html(cnt);
				correctAnswerTextSetting(selector);
			}
			
			//답가지 tambahkan
			function addAnswerView(selector) { 
				var answerViewHtml = "";
				answerViewHtml += '<tr>';
				answerViewHtml += '		<td class="w1 correctAnswerNumber"></td>';
				answerViewHtml += '		<td class="free_textarea w2_tarea"><textarea class="tarea02" name="" style="height: 40px;"></textarea></td>';
				answerViewHtml += '		<td class="w3"><input type="checkbox" class="chk_2"></td>';
				answerViewHtml += '</tr>';
				$(selector + " #answerArea").append(answerViewHtml);
				autosize($("textarea"));
				answerViewReSortSetting(selector);
			}
			
			//pilih한 답가지 정답으로 pilih
			$(document).on("click", ".correctAnswerNumber", function() {
				if ($(this).hasClass("on")) {
					$(this).removeClass("on");
				} else {
					$(this).addClass("on");
				}
				var tbodyName = $(this).parent().parent().attr("name");
				if (tbodyName == "create") {
					tbodyName = "#quizCreatePopup";
				} else {
					tbodyName = "#quizModifyPopup";
				}
				answerViewReSortSetting(tbodyName);
				correctAnswerTextSetting(tbodyName);
			});
			
			
			//답가지 삭제
			function removeAnswerView(selector) {
				var checkedCnt = $(selector + " #answerArea").find("input:checkbox:checked").length;
				if (checkedCnt < 1) {
					alert("삭제할 답가지를 pilih해 주세요.");
					return false;
				}
				
				var checkBoxTotalCnt = $(selector + " #answerArea").find("input:checkbox").length;
				if ((checkBoxTotalCnt - checkedCnt) < 2) {
					alert("답가지는 최소 2개 이상이어야합니다.");
					return false;
				}
				
				if(confirm("pilih한 답가지를 삭제하시겠습니까?")){
					$(selector + " #answerArea").find("input:checkbox:checked").each(function() {
						$(this).parent().parent().remove();
					});
					answerViewReSortSetting(selector);
				}
			}
			
			//퀴즈 제목 simpan
			function feTitleSave(t, feSeq) {
				var target = $(t).parent().find("#feName");
				var feName = $(target).val();
				if(isEmpty(feName) || isBlank(feName)) {
					alert("퀴즈 명을 입력해 주세요.");
					$(target).focus();
					return false;
				}
				
				if (feName.length > 30) {
					alert("퀴즈 명을 30자 이내로 입력해 주세요.");
					$(target).focus();
					return false;
				}
				
				$.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/pf/lesson/formationEvaluation/title/modify",
			        data: {
			        	"fe_seq" : feSeq, 
			        	"fe_name" : feName
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	alert("퀴즈명이 simpan되었습니다.");
			            	location.reload();
			            } else {
			            	alert("퀴즈명 simpan에 실패하였습니다. [" + data.status + "]");
			            	location.reload();
			            }
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			//파일pilih
			function fileSelect(selector, t) {
				$(selector+" #quizAttachFile").change(function(){
					var file = $(this).val();
					if (file != "") {
						var attachFileName = file.substring(file.lastIndexOf('\\') + 1); //파일명
						var ext = attachFileName.substring(attachFileName.lastIndexOf('.') + 1).toLowerCase(); //파일 확장자
						var fileMaxSize = 10; //10MB
						var fileSize = this.files[0].size/1024/1024;
						if (fileSize > fileMaxSize) {
							alert("10MB이하의 파일만 업로드 가능합니다.");
							fileValueInit(selector+" #quizAttachFile");
							return false;
						}
						
						//파일 확장자 유효성검사
						if (/(jpg|png)$/i.test(ext) == false) {
							alert("이미지만 업로드 가능합니다.(jpg, png)");
							fileValueInit(selector+" #quizAttachFile");
							return false;
						}
						
						var previewImgWrapDiv = $("<div>").addClass("preview_wrap");
						var previewImgRemoveBtn = $("<button>").addClass("close_xs").attr("onclick", "javascript:quizAttachFileRemove('" + selector + "');").html("X");
						
						var previewImg = $("<img>").attr("id","preview_img").attr("title","퀴즈 이미지 미리보기").addClass("preview"); //미리보기
						previewImg.attr("src", loadPreview(this, "preview_img"));
						
						$(previewImgWrapDiv).append(previewImgRemoveBtn);
						$(previewImgWrapDiv).append(previewImg);
						if (selector == "#quizModifyPopup") {
							if ($("#quizModifyPopup #quizAttachFileSeq").val() != "") {
								$("#quizModifyPopup #quizAttachFileDelYn").val("Y");
							}
						}
						$(selector+" #fileName").val(attachFileName);
						$(selector+" #imgPreviewArea").html("");
						$(selector+" #imgPreviewArea").append(previewImgWrapDiv);
					}
				});
				$(selector+" #quizAttachFile").click();
			}
			
			//업로드한 파일 삭제
			function quizAttachFileRemove(selector) {
				if (selector == "#quizModifyPopup") {
					$("#quizModifyPopup #quizAttachFileDelYn").val("Y");
				}
				
				$(selector+" #imgPreviewArea").html("");
				$(selector+" #fileName").val("");
				fileValueInit(selector+" #quizAttachFile");
			}
			
			
			//퀴즈 registrasi
			function quizCreate() {
				var title = $("#quizCreatePopup #quizQuestion").val();
				if (isEmpty(title) || isBlank(title)) {
					alert("지문을 입력해주세요.");
					$("#quizCreatePopup #quizQuestion").focus();
					return false;
				}
				
				var answerViewCnt = $("#quizCreatePopup #answerArea > tr").length;
				if (answerViewCnt < 2) {
					alert("답가지는 최소 2개 이상 되어야합니다.");
					return false;
				}
				$("#quizCreatePopup #answerViewCnt").val(answerViewCnt);
				
				
				var answerAreaTarget = $("#quizCreatePopup #answerArea > tr");
				var isEmptyAnswerViewTextValue = false; 
				$(answerAreaTarget).each(function() {
					var answerViewText = $(this).find("textarea").val();
					if (isEmpty(answerViewText) || isBlank(answerViewText)) {
						isEmptyAnswerViewTextValue = true;
						alert("답가지의 isi을 입력해주세요.");
						$(this).find("textarea").focus();
						return false;
					}
				});
				
				if (isEmptyAnswerViewTextValue) {
					return false;
				}
				
				correctAnswerTextSetting("#quizCreatePopup");
				var answerCnt = $(answerAreaTarget).find(".correctAnswerNumber.on").length;
				if (answerCnt < 1) {
					alert("정답을 한개 이상 pilih해주세요.\n(답가지 번호를 클릭하여 pilih해주세요.)");
					return false;
				}
				
				$("#quizCreatePopup #answerText").val($("#quizCreatePopup #correctAnswerText").html());
				
				$("#quizCreateFrm").attr("action", "${HOME}/ajax/pf/lesson/formationEvaluation/quiz/create").ajaxForm({
					beforeSend: function () {
						$.blockUI();
					},
					type: "POST",
					dataType:"json",
					success:function(data){
						if (data.status == "200") {
							alert("문제registrasi이 완료되었습니다.");
			            	location.reload();
						} else {
							alert("문제 registrasi에 실패하였습니다. [" + data.status + "]");
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						document.write(xhr.responseText);
				    },
			        complete:function() {
			            $.unblockUI();
			        }
				}).submit();
			}
		
			//문제 목록
			function quizListPopupOpen(feSeq, quizSeq) { //quizSeq : 0이면 checked 하지 않음
				$("#quizListPopup #totalQuizCnt").html("0");
				$("#quizControllBox").hide(); //문제 순서변경
				$("#quizListPopupCloseAndCreateOpenBtn").hide(); //문제 registrasi 버튼
				
				$.ajax({
			        type: "GET",
			        url: "${HOME}/ajax/pf/lesson/formationEvaluation/quiz/list",
			        data: {
			        	"fe_seq" : feSeq
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	
			            	quizDetailPopupClose();
			            	
			            	var feName = data.feInfo.fe_name;
			            	if (feName == "") {
			            		var lessonSubject = data.feInfo.lesson_subject;
				            	var lessonDate = data.feInfo.lesson_date;
			            		lessonDate = lessonDate.replace("/","월") + "일";
			            		feName = lessonSubject + " " + lessonDate + " 퀴즈";
			            	}
			            	feName = '문제 목록(' + feName + ')'
			            	
			            	
			            	$("#quizListPopup .t_title").html(feName);
			            	
			            	var testState = data.feInfo.test_state;
			            	if (testState == "WAIT") {
			            		$("#quizControllBox").show();
			            		$("#quizListPopupCloseAndCreateOpenBtn").show();
			            	}
			            	
			            	if (data.quizList.length < 1) {
			            		alert("문제가 registrasi되지 않았습니다.");
			            		$("#quizListPopup").hide();
			            		return false;
			            	}
			            	
			            	var quizTotalCnt = data.quizList.length;
			            	
			            	var quizListHtml = "";
			            	$(data.quizList).each(function (idx) {
			            		quizListHtml += '<tr>';
			            		quizListHtml += '	<td class="w1">' + (idx+1) + '</td>';
			            		if (testState == "WAIT") {
			            			quizListHtml += '<input type="hidden" class="blind-position" id="feSeq" name="" value="' + feSeq + '">';
			            			quizListHtml += '<input type="hidden" class="blind-position" id="quizSeq" name="" value="' + this.quiz_seq + '">';
			            			quizListHtml += '<input type="hidden" class="blind-position" id="quizTotalCnt" name="" value="' + data.quizList.length + '">';
			            			var quizQuestion = this.quiz_question;
			            			quizQuestion = quizQuestion.replace(/(\n|\r\n)/g, '<br>');
			            			quizListHtml += '	<td class="free_textarea w2"><span class="q_title" onclick="javascript:quizModifyPopupOpen(' + this.quiz_seq + ');">' + quizQuestion + '</span><span class="sp2">정답 : ' + this.answer_number + '</span></td>';
				            		quizListHtml += '	<td class="w3">';
				            		quizListHtml += '		<button class="btn_c" title="삭제하기" onclick="javascript:removeSingleQuiz(' + this.quiz_seq + ', this);">X</button>';
				            		if (this.quiz_seq == quizSeq) {
				            			quizListHtml += '		<input type="checkbox" name="checkgroup5" id="checkgroup5_1" class="chk_2" checked="checked">';
				            		} else {
				            			quizListHtml += '		<input type="checkbox" name="checkgroup5" id="checkgroup5_1" class="chk_2">';
				            		}
					            	quizListHtml += '	</td>';	
			            		} else {
			            			quizListHtml += '	<td class="free_textarea w2" colspan="2"><span class="q_title" onclick="javascript:quizDetailPopupOpen(' + this.quiz_seq + ');">' + this.quiz_question + '</span><span class="sp2">정답 : ' + this.answer_number + '</span></td>';
			            		}
				            	
				            	quizListHtml += '</tr>';
			            	});
			            	$("#quizListPopup #totalQuizCnt").html(quizTotalCnt);
			            	$("#quizPreviewListArea").html("");
			            	$("#quizPreviewListArea").append(quizListHtml);
			            	$("#quizListPopupCloseAndCreateOpenBtn").attr("onclick", "javascript:quizListPopupCloseAndCreateOpen(" + feSeq + ", '" + feName +"', " + quizTotalCnt + ");");
			            	$("#quizListPopup").show();
			            } else {
			            	alert("문제 목록 불러오기에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			//문제 보기 닫기
			function quizListPopupClose() {
				$("#quizListPopup").hide();
			}
			
			//문제 보기 닫고 문제 registrasi 열기
			function quizListPopupCloseAndCreateOpen(feSeq, feName, quizTotalCnt) {
				quizListPopupClose();
				quizCreatePopupOpen(feSeq, feName, quizTotalCnt);
			}
			
			//문제 순서 변경
			function quizOrderUpDown(flag) {
				var orderUp = flag;
				var checkedCnt = $("#quizPreviewListArea").find("input:checkbox:checked").length;
				var checkBoxTotalCnt = $("#quizPreviewListArea").find("input:checkbox").length;
				if (checkedCnt == 0 || checkedCnt > 1) {
					alert("문제를 1개만 체크해주세요.");
					return false;
				}
				
				var checkedTarget = $("#quizPreviewListArea").find("input:checkbox:checked");
				var numberTarget = $(checkedTarget).parent().parent().find("td:eq(0)");
				var currIdx = parseInt($(numberTarget).text());
				if ($(numberTarget).hasClass("on")) {
					changeAnswerFlag = true;
				}

				if (currIdx == 1 && orderUp == true) {
					alert("첫번째 문제입니다. 위로 이동할 수 없습니다.");
					return false;
				}
				
				if (currIdx == checkBoxTotalCnt && orderUp == false) {
					alert("마지막 문제입니다. 아래로 이동할 수 없습니다.");
					return false;
				}
				
				var target = $("#quizPreviewListArea").find("input:checkbox:checked");
				var feSeq = $(target).parent().parent().find("#feSeq").val();
				var quizSeq = $(target).parent().parent().find("#quizSeq").val();
				if (confirm("체크한 문제의 순서를 변경 하시겠습니까?")) {
					$.ajax({
				        type: "POST",
				        url: "${HOME}/ajax/pf/lesson/formationEvaluation/quiz/order/modify",
				        data: {
				        	"fe_seq" : feSeq
				        	, "quiz_seq" : quizSeq
				        	, "order_up_flag" : orderUp 
				        },
				        dataType: "json",
				        success: function(data, status) {
				            if (data.status == "200") {
				            	quizListPopupOpen(feSeq, quizSeq);
				            } else {
				            	alert("문제 순서 변경에 실패하였습니다. [" + data.status + "]");
				            }
				        },
				        error: function(xhr, textStatus) {
				            document.write(xhr.responseText);
				        },
				        beforeSend:function() {
				            $.blockUI();
				        },
				        complete:function() {
				            $.unblockUI();
				        }
				    });
				}
			}
			
			//문제 다중 삭제
			function removeMultiQuiz() {
				if (confirm("pilih한 문제를 삭제하시겠습니까?")) {
					var checkedQuizList = $("#quizPreviewListArea").find("input:checkbox:checked");
					var feSeq = 0;
					var quizSeqStr = "";
					$(checkedQuizList).each(function(idx) {
						if (idx == 0) {
							feSeq = $(this).parent().parent().find("#feSeq").val();
						}
						var quizSeq = $(this).parent().parent().find("#quizSeq").val();
						quizSeqStr = quizSeqStr + quizSeq + ",";
						if (checkedQuizList.length == (idx+1)) {
							quizSeqStr = quizSeqStr.substring(0, quizSeqStr.length-1);
						}
					});

					if (quizSeqStr == "") {
						alert("삭제할 문제를 1개 이상 pilih해주세요.");
						return false;
					}
					
					removeQuiz(quizSeqStr, feSeq, checkedQuizList.length);
				}
			}
			
			//문제 단일 삭제
			function removeSingleQuiz(quizSeq, t) {
				if (confirm("문제를 삭제하시겠습니까?")) {
					var feSeq = $(t).parent().parent().find("#feSeq").val();
					var quizSeqStr = $(t).parent().parent().find("#quizSeq").val();
					removeQuiz(quizSeqStr, feSeq, 1);
				}
			}
			
			//문제 삭제
			function removeQuiz(quizSeqStr, feSeq, selectCnt) {
				$.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/pf/lesson/formationEvaluation/quiz/remove",
			        data: {
			        	"fe_seq" : feSeq
			        	, "quiz_seq_str" : quizSeqStr
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	alert(selectCnt+"개의 퀴즈가 삭제 되었습니다.");
			            	quizListPopupOpen(feSeq, quizSeq);
			            } else {
			            	alert(selectCnt+"개의 퀴즈가 삭제에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}

			//문제 미리보기
			function quizPreviewPopupOpen(feSeq, closeFlag) {
				
				$.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/pf/lesson/formationEvaluation/quiz/preview",
			        data: {
			        	"fe_seq" : feSeq
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	if (closeFlag == true) {
			            		quizDetailPopupClose();
			            	}
			            		
			            	var quizPreviewInfo = data.quizPreviewInfo;
			            	var title = quizPreviewInfo.fe_name;
			            	var examineeCnt = quizPreviewInfo.examinee_cnt;//Keseluruhan 응시자 수
			            	var quizCnt = quizPreviewInfo.quiz_cnt;//문제 수
			            	var thumbnailPath = quizPreviewInfo.thumbnail_path;//표지
			            	var nextQuizSeq = quizPreviewInfo.next_quiz_seq;
			            	if (title == "") {
			            		var lessonSubject = quizPreviewInfo.lesson_subject;
			            		var lessonDate = quizPreviewInfo.lesson_date;
			            		lessonDate = lessonDate.replace("/","월") + "일";
			            		title = lessonSubject + " " + lessonDate + " 퀴즈";
			            	}
			            	
			            	if (thumbnailPath != "") {
			            		$("#quizPreviewPopup #thumbnailArea").html('<img class="preview" src="${RES_PATH}' + thumbnailPath + '" alt="퀴즈 이미지 미리보기">');	
			            	}
			            	
			            	$("#quizPreviewPopup .t_title").html(title);
			            	$("#quizPreviewPopup .title").html(title);
			            	$("#examineeCnt").html(examineeCnt);
			            	$("#quizCnt").html(quizCnt);
			            	$("#quizPreviewPopup").show();
			            	if (nextQuizSeq > 0) {
				            	$("#nextBtn").attr("onclick", "javascript:quizDetailPopupOpen(" + nextQuizSeq + ");");
			            	} else {
			            		$("#quizPreviewPopup .fgo").hide();
			            		$("#nextBtn").hide();
			            	}
			            } else {
			            	alert("문제 미리보기 불러오기에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			//미리보기 첫페이지 닫기
			function quizPreviewPopupClose() {
				$("#quizPreviewPopup").hide();
			}
			
			//문제 상세보기
			function quizDetailPopupOpen(quizSeq) {
				$.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/pf/lesson/formationEvaluation/quiz/detail",
			        data: {
			        	"quiz_seq" : quizSeq
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
							quizPreviewPopupClose();
							quizDetailPopupClose();
							quizListPopupClose();
			            	
			            	var quizInfo = data.quizInfo;
			            	var feSeq = quizInfo.fe_seq;
			            	var quizOrder = quizInfo.quiz_order;
			            	var quizQuestion = quizInfo.quiz_question;
			            	quizQuestion = quizQuestion.replace(/(\n|\r\n)/g, '<br>');
			            	var feName = quizInfo.fe_name;
			            	var preQuizSeq = quizInfo.pre_quiz_seq;
			            	var nextQuizSeq = quizInfo.next_quiz_seq;
			            	var testState = quizInfo.test_state;
			            	if (feName == "") {
			            		var lessonSubject = quizInfo.lesson_subject;
				            	var lessonDate = quizInfo.lesson_date;
			            		lessonDate = lessonDate.replace("/","월") + "일";
			            		feName = lessonSubject + " " + lessonDate + " 퀴즈";
			            	}
			            	
			            	$("#quizDetailPopup #quizNumber").html(quizOrder);
			            	$("#quizDetailPopup .t_title").html(feName);
			            	$("#quizDetailPopup #quizQuestion").html(quizQuestion);
			            	
			            	var quizAttachInfo = data.quizAttachInfo;
			            	$("#quizDetailPopup #imgPreviewArea").html("");
			            	if (quizAttachInfo != null) {
				            	var filePath = quizAttachInfo.file_path;
				            	$("#quizDetailPopup #imgPreviewArea").html('<div class="preview_wrap"><img class="preview" src="${RES_PATH}' + filePath + '" alt="퀴즈 이미지 미리보기"></div>');
			            	}
			            	
			            	var quizAnswerList = data.quizAnswerList;
			            	var answerViewHtml = "";
			            	$(quizAnswerList).each(function() {
			            		answerViewHtml += '<tr>';
			            		if (this.answer_flag == "Y") {
				            		answerViewHtml += '	<td class="w1 on">' + this.answer_order + '</td>';
			            		} else {
			            			answerViewHtml += '	<td class="w1">' + this.answer_order + '</td>';
			            		}
			            		
			            		var content = this.content;
			            		content = content.replace(/(\n|\r\n)/g, '<br>');
			            		
			            		answerViewHtml += '	<td class="w3_2 free_textarea">' + content + '</td>';
			            		answerViewHtml += '</tr>';
			            	});
			            	$("#quizDetailAnswerView").html(answerViewHtml);
			            	
			            	
			            	//이전
			            	if (preQuizSeq == 0) {
			            		$("#quizDetailPopup #preBtn").attr("onclick", "javascript:quizPreviewPopupOpen(" + feSeq + ", true);");	
			            	} else {
			            		$("#quizDetailPopup #preBtn").attr("onclick", "javascript:quizDetailPopupOpen(" + preQuizSeq + ");");
			            	}
			            	
			            	//다음
			            	if (nextQuizSeq == 0) {
			            		$("#quizDetailPopup .fgo").hide();
			            		$("#quizDetailPopup #nextBtn").hide();
			            	} else {
			            		$("#quizDetailPopup .fgo").show();
			            		$("#quizDetailPopup #nextBtn").show();
			            		$("#quizDetailPopup #nextBtn").attr("onclick", "javascript:quizDetailPopupOpen(" + nextQuizSeq + ");");
			            	}
			            	
			            	if (testState == "WAIT") {
			            		$("#quizDetailPopup #quizListBtn").attr("onclick", "javascript:quizListPopupOpen("+feSeq+", 0);")
			            		$("#quizDetailPopup #quizListBtn").show();
			            	} else {
			            		$("#quizDetailPopup #quizListBtn").hide();
			            	}
			            	
			            	$("#quizDetailPopup").show();
			            	
			            } else {
			            	alert("문제 불러오기에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			//상세 보기 닫기
			function quizDetailPopupClose() {
				$("#quizDetailPopup").hide();
			}
			
			//표지 registrasi
			function thumbnailUpload(feSeq) {
				$("#thumbnailFrm input[name='uploadFile']").change(function(){
					var file = $(this).val();
					if (file != "") {
						var attachFileName = file.substring(file.lastIndexOf('\\') + 1); //파일명
						var ext = attachFileName.substring(attachFileName.lastIndexOf('.') + 1).toLowerCase(); //파일 확장자
						var fileMaxSize = 10; //10MB
						var fileSize = this.files[0].size/1024/1024;
						if (fileSize > fileMaxSize) {
							alert("10MB이하의 파일만 업로드 가능합니다.");
							fileValueInit("#thumbnailFrm input[name='uploadFile']");
							return false;
						}
						
						//파일 확장자 유효성검사
						if (/(jpg|png)$/i.test(ext) == false) {
							alert("이미지만 업로드 가능합니다.(jpg, png)");
							fileValueInit("#thumbnailFrm input[name='uploadFile']");
							return false;
						}
						$("#thumbnailFrm input[name='feSeq']").val(feSeq);
						$("#thumbnailFrm").attr("action", "${HOME}/ajax/pf/lesson/formationEvaluation/thumbnail/upload").ajaxForm({
							beforeSend: function () {
								$.blockUI();
							},
							type: "POST",
							dataType:"json",
							success:function(data){
								fileValueInit("#thumbnailFrm input[name='uploadFile']");
								fileValueInit("#thumbnailFrm input[name='feSeq']");
								if (data.status == "200") {
									alert("표지 registrasi이 완료되었습니다.");
					            	location.reload();
								} else {
									alert("표지 registrasi에 실패하였습니다. [" + data.status + "]");
								}
							},
							error: function (jqXHR, textStatus, errorThrown) {
								document.write(xhr.responseText);
						    },
					        complete:function() {
					            $.unblockUI();
					        }
						}).submit();
						
					}
				});
				$("#thumbnailFrm input[name='uploadFile']").click();
			}
			
			
			function quizModifyPopupClose() {
				$("#quizModifyPopup").hide();
			}
			
			//퀴즈 perbaiki 상세 보기
			function quizModifyPopupOpen(quizSeq) {
				$.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/pf/lesson/formationEvaluation/quiz/detail",
			        data: {
			        	"quiz_seq" : quizSeq
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
							quizPreviewPopupClose();
							quizDetailPopupClose();
							quizListPopupClose();
							
							//퀴즈정보
							var quizInfo = data.quizInfo;
			            	var feSeq = quizInfo.fe_seq;
			            	var quizSeq = quizInfo.quiz_seq;
			            	var quizOrder = quizInfo.quiz_order;
			            	var quizQuestion = quizInfo.quiz_question;
			            	var feName = quizInfo.fe_name;
			            	
			            	if (feName == "") {
			            		var lessonSubject = quizInfo.lesson_subject;
				            	var lessonDate = quizInfo.lesson_date;
			            		lessonDate = lessonDate.replace("/","월") + "일";
			            		feName = lessonSubject + " " + lessonDate + " 퀴즈";
			            	}
			            	
			            	$("#quizModifyPopup #quizSeq").val(quizSeq);
			            	$("#quizModifyPopup #feSeq").val(feSeq);
			            	$("#quizModifyPopup #quizNumber").html(quizOrder);
			            	$("#quizModifyPopup #quizOrder").val(quizOrder);
			            	$("#quizModifyPopup .t_title").html('문제 perbaiki ('+feName+')');
			            	$("#quizModifyPopup #quizQuestionArea").html('<textarea class="tarea01" placeholder="지문을 입력하세요." id="quizQuestion" name="quizQuestion"></textarea>');
			            	$("#quizModifyPopup #quizQuestion").val(quizQuestion);
			            	$("#quizModifyPopup").find("#quizAttachFile").remove();
							$("#quizModifyPopup #quizModifyFrm").append('<input type="file" id="quizAttachFile" name="quizAttachFile" class="blind-position">');
							
							//퀴즈 자료 제시
			            	var quizAttachInfo = data.quizAttachInfo;
			            	$("#quizModifyPopup #fileName").val("");
			            	$("#quizModifyPopup #imgPreviewArea").html("");
			            	$("#quizModifyPopup #quizAttachFileSeq").val("");
			            	$("#quizModifyPopup #quizAttachFileDelYn").val("");
			            	
			            	if (quizAttachInfo != null) {
			            		$("#quizModifyPopup #quizAttachFileSeq").val(quizAttachInfo.quiz_attach_seq);
				            	$("#quizModifyPopup #fileName").val(quizAttachInfo.file_name);
				            	var imgPreviewHtml = "";
				            	imgPreviewHtml += '<div class="preview_wrap">';
				            	imgPreviewHtml += '	<button class="close_xs" onclick="javascript:quizAttachFileRemove(\'#quizModifyPopup\');">X</button>';
				            	imgPreviewHtml += '	<img class="preview" src="${RES_PATH}' + quizAttachInfo.file_path + '" alt="퀴즈 이미지 미리보기">';
				            	imgPreviewHtml += '</div>';
				            	$("#quizModifyPopup #imgPreviewArea").html(imgPreviewHtml);
			            	}
			            	
			            	//퀴즈 답가지
			            	var quizAnswerList = data.quizAnswerList;
			            	var answerViewHtml = "";
		            		var correctAnswerText = "";
		            		
			            	$(quizAnswerList).each(function(idx) {
			            		var idx = idx+1;
			            		var content = this.content;
			            		var answerFlag = this.answer_flag;
			            		answerViewHtml += '<tr>';
			            		if (answerFlag == "Y") {
			            			correctAnswerText += idx + ", ";
				    				answerViewHtml += '	<td class="w1 correctAnswerNumber on">' + idx + '</td>';
			            		} else {
			            			answerViewHtml += '	<td class="w1 correctAnswerNumber">' + idx + '</td>';
			            		}
			    				answerViewHtml += '		<td class="free_textarea w2_tarea"><textarea class="tarea02" name="answerContent'+idx+'">' + content + '</textarea></td>';
			    				answerViewHtml += '		<td class="w3"><input type="checkbox" class="chk_2"></td>';
			    				answerViewHtml += '</tr>';
			            	});
			            	
			            	if (correctAnswerText.length > 0) {
								correctAnswerText = correctAnswerText.substring(0, correctAnswerText.length-2);
							}
							
			            	$("#quizModifyPopup #answerViewCntText").html(quizAnswerList.length);
			            	$("#quizModifyPopup #correctAnswerText").html(correctAnswerText);
			            	$("#quizModifyPopup #answerArea").html("");
			            	$("#quizModifyPopup #answerArea").append(answerViewHtml);
							
							$("#quizModifyPopup").show();
							autosize($("textarea"));
			            } else {
			            	alert("문제 불러오기에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			//퀴즈 perbaiki
			function quizModify() {
				var title = $("#quizModifyPopup #quizQuestion").val();
				if (isEmpty(title) || isBlank(title)) {
					alert("지문을 입력해주세요.");
					$("#quizCreatePopup #quizQuestion").focus();
					return false;
				}
				
				var answerViewCnt = $("#quizModifyPopup #answerArea > tr").length;
				if (answerViewCnt < 2) {
					alert("답가지는 최소 2개 이상 되어야합니다.");
					return false;
				}
				$("#quizModifyPopup #answerViewCnt").val(answerViewCnt);
				
				
				var answerAreaTarget = $("#quizModifyPopup #answerArea > tr");
				var isEmptyAnswerViewTextValue = false; 
				$(answerAreaTarget).each(function() {
					var answerViewText = $(this).find("textarea").val();
					if (isEmpty(answerViewText) || isBlank(answerViewText)) {
						isEmptyAnswerViewTextValue = true;
						alert("답가지의 isi을 입력해주세요.");
						$(this).find("textarea").focus();
						return false;
					}
				});
				
				if (isEmptyAnswerViewTextValue) {
					return false;
				}
				
				correctAnswerTextSetting("#quizModifyPopup");
				var answerCnt = $(answerAreaTarget).find(".correctAnswerNumber.on").length;
				if (answerCnt < 1) {
					alert("정답을 한개 이상 pilih해주세요.\n(답가지 번호를 클릭하여 pilih해주세요.)");
					return false;
				}
				
				$("#quizModifyPopup #answerText").val($("#quizModifyPopup #correctAnswerText").html());
				
				$("#quizModifyFrm").attr("action", "${HOME}/ajax/pf/lesson/formationEvaluation/quiz/modify").ajaxForm({
					beforeSend: function () {
						$.blockUI();
					},
					type: "POST",
					dataType:"json",
					success:function(data){
						if (data.status == "200") {
							alert("문제perbaiki이 완료되었습니다.");
			            	location.reload();
						} else {
							alert("문제perbaiki에 실패하였습니다. [" + data.status + "]");
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						document.write(xhr.responseText);
				    },
			        complete:function() {
			            $.unblockUI();
			        }
				}).submit();
			}
			
			function pastFormationEvaluationPopupOpen(currFeSeq) {
				$("#pastListFrm #searchYear").val("");
				$("#pastListFrm #searchText").val("");
				$("#pastListFrm #currFeSeq").val(currFeSeq);
				getPastList();
			}
			
			function pastFormationEvaluationPopupClose() {
				$("#pastListFrm #searchYear").val("");
				$("#pastYear .uselected").html("Keseluruhan");
				$("#pastListFrm #searchText").val("");
				$("#pastFormationEvaluationPopup").hide();
			}

			//지난 수업 퀴즈 불러오기
			function getPastList() {
				$("#pastListFrm #searchYear").val($("#pastListFrm #pastYear").attr("value"));
				$.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/pf/lesson/formationEvaluation/past/list",
			        data: $("#pastListFrm").serialize(),
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	var yearAcademicList = data.yearAcademicList;
			            	var yearHtml = '<span class="uoption" value="">Keseluruhan</span>';
			            	$(yearAcademicList).each(function() {
			            		var year = this.year;
			            		yearHtml += '<span class="uoption" value="'+this.year+'">' + this.year + '</span>';
			            	});
			            	 
			            	$("#pastFormationEvaluationPopup #pastYear .uoptions").html(yearHtml);
			            	if ($("#pastListFrm #searchYear").val() == "") {
			            		$("#pastYear .uselected").html("Keseluruhan");
			            	}
			            	
							var pastList = data.pastList;
							var preCurrSeq = 0;
							var pastListHtml = "";
							$(pastList).each(function (idx){
								if (idx == 0 || preCurrSeq != this.curr_seq) {
									pastListHtml += '<div class="pop_table">';
					            	pastListHtml += '	<table class="tb_pop">';
					            	pastListHtml += '		<thead>';
					            	pastListHtml += '			<tr>';
					            	pastListHtml += '				<th scope="col" class="w1 style_7"> '+this.year+'년<span class="sign">&gt;</span>'+this.curr_name+'<span class="sign">&gt;</span>'+this.s_aca_system_name+'</th>';
					            	pastListHtml += '			</tr>';
					            	pastListHtml += '		</thead>';
					            	pastListHtml += '	</table>';
					            	pastListHtml += '	<div class="pop_twrap1">';
					            	pastListHtml += '		<table class="tb_pop bd02">';
					            	pastListHtml += '			<tbody>';
								}
								pastListHtml += '					<tr>';
				            	pastListHtml += '						<td class="bd01 bd02 w1"><input type="radio" name="rg1" id="rg1" value="' + this.fe_seq + '" class="radio01"></td>';
				            	pastListHtml += '						<td class="bd01 bd02 ic_td w2">';
				            	pastListHtml += '							<span class="sp1">' + this.fe_name + '</span>';
				            	pastListHtml += '							<span class="sp2">문제수 : ' + this.quiz_cnt + '</span>';
				            	pastListHtml += '						</td>';
				            	pastListHtml += '						<td class="bd01 bd02 w3">tanggal registrasi : ' + this.reg_date + '</td>';
				            	pastListHtml += '					</tr>';
				            	preCurrSeq = this.curr_seq;
				            	if (idx > 0 && preCurrSeq != this.curr_seq || (pastList.length-1) == idx) {
									pastListHtml += '			</tbody>';
					            	pastListHtml += '		</table>';
					            	pastListHtml += '	</div>';
					            	pastListHtml += '</div>';
								}
							});
			            	
			            	$("#pastFormationEvaluationPopup #pastList").html("");
			            	$("#pastFormationEvaluationPopup #pastList").html(pastListHtml);
			            	
			            	
			            	if (pastList.length < 1) {
			            		$("#pastFormationEvaluationPopup #pastList").parent().addClass("search_x");
			            	} else {
			            		$("#pastFormationEvaluationPopup #pastList").parent().removeClass("search_x");
			            	}
			            		
			            		
							$("#pastFormationEvaluationPopup").show();
			            } else {
			            	alert("지난 수업 퀴즈 불러오기에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function selectedQuizCopy() {
				if ($("#pastListFrm input:radio:checked").length == 0) {
					alert("퀴즈를 pilih해 주세요.");
					return false;
				}
				
				var feSeq = $("#pastListFrm input:radio:checked").val();
				var currFeSeq = $("#pastListFrm #currFeSeq").val();
				if(confirm("pilih한 퀴즈를 불러오기 하시겠습니까?")){
					$.ajax({
				        type: "POST",
				        url: "${HOME}/ajax/pf/lesson/formationEvaluation/quiz/copy",
				        data: {
				        	"fe_seq" : feSeq
				        	, "curr_fe_seq" : currFeSeq
				        },
				        dataType: "json",
				        success: function(data, status) {
				            if (data.status == "200") {
				            	alert("퀴즈를 불러왔습니다.");
				            	location.reload();
				            } else {
				            	alert("삭제에 실패하였습니다. [" + data.status + "]");
				            }
				        },
				        error: function(xhr, textStatus) {
				            document.write(xhr.responseText);
				        },
				        beforeSend:function() {
				            $.blockUI();
				        },
				        complete:function() {
				            $.unblockUI();
				        }
				    });
				};
				
			}
			
			function quizExcelUploadPopupOpen(feSeq) {
				$("#quizExcelUploadFrm #currFeSeq").val(feSeq);
				fileValueInit("#quizExcelUploadFrm #excelFile");
				fileValueInit("#quizExcelUploadFrm #dataZipFile");
				$("#quizExcelUploadFrm #excelFileName").val("");
				$("#quizExcelUploadFrm #dataZipFileName").val("");
				$("#quizExcelUploadPopup").show();
				
			}
			
			function quizExcelUploadPopupClose() {
				$("#quizExcelUploadPopup").hide();
			}
			
			function quizTemplateDownload() {
				location.href = "${HOME}/resources/file/quiz_template/quiz_template.xlsx"; 
			}
			
			//파일pilih
			function excelFileSelect() {
				$("#quizExcelUploadFrm #excelFile").change(function(){
					var file = $(this).val();
					if (file != "") {
						var attachFileName = file.substring(file.lastIndexOf('\\') + 1); //파일명
						var ext = attachFileName.substring(attachFileName.lastIndexOf('.') + 1).toLowerCase(); //파일 확장자
						var fileMaxSize = 10; //10MB
						var fileSize = this.files[0].size/1024/1024;
						if (fileSize > fileMaxSize) {
							alert("10MB이하의 파일만 업로드 가능합니다.");
							fileValueInit("#quizExcelUploadFrm #excelFile");
							return false;
						}
						
						//파일 확장자 유효성검사
						if (/(xlsx)$/i.test(ext) == false) {
							alert("엑셀파일만 업로드 가능합니다.(xlsx)");
							fileValueInit("#quizExcelUploadFrm #excelFile");
							return false;
						}
						
						$("#quizExcelUploadFrm #excelFileName").val(attachFileName);
					} else {
						fileValueInit("#quizExcelUploadFrm #excelFile");
						$("#quizExcelUploadFrm #excelFileName").val("");
					}
				});
				$("#quizExcelUploadFrm #excelFile").click();
			}
			
			function dataZipFileSelect() {
				$("#quizExcelUploadFrm #dataZipFile").change(function(){
					var file = $(this).val();
					if (file != "") {
						var attachFileName = file.substring(file.lastIndexOf('\\') + 1); //파일명
						var ext = attachFileName.substring(attachFileName.lastIndexOf('.') + 1).toLowerCase(); //파일 확장자
						var fileMaxSize = 100; //100MB
						var fileSize = this.files[0].size/1024/1024;
						if (fileSize > fileMaxSize) {
							alert("100MB이하의 파일만 업로드 가능합니다.");
							fileValueInit("#quizExcelUploadFrm #dataZipFile");
							return false;
						}
						
						//파일 확장자 유효성검사
						if (/(zip)$/i.test(ext) == false) {
							alert("압축파일만 업로드 가능합니다.(zip)");
							fileValueInit("#quizExcelUploadFrm #dataZipFile");
							return false;
						}
						
						$("#quizExcelUploadFrm #dataZipFileName").val(attachFileName);
					} else {
						fileValueInit("#quizExcelUploadFrm #dataZipFile");
						$("#quizExcelUploadFrm #dataZipFileName").val("");
					}
				});
				$("#quizExcelUploadFrm #dataZipFile").click();
			}
			
			
			function quizExcelUpload() {
				if ($("#quizExcelUploadFrm #excelFile").val() == "") {
					alert("file excel을 업로드해 주세요.");
					return false;
				}
				if(confirm("기존에 registrasi되어 있는 퀴즈는 삭제됩니다.\nregistrasi하시겠습니까?")) {
					$("#quizExcelUploadFrm").ajaxForm({
	                    type: "POST",
	                    url: "${HOME}/ajax/pf/lesson/formationEvaluation/quiz/excel/create",
	                    dataType: "json",
	                    success: function(data, status){
	                        if (data.status == "200") {
	                            alert("퀴즈 엑셀 일괄 업로드가 완료되었습니다.");
	                            location.reload();
	                        } else {
	                        	alert("퀴즈 엑셀 일괄 업로드에 실패하였습니다. [" + data.status + "]");
	                        }
	                    },
	                    error: function(xhr, textStatus) {
	                        document.write(xhr.responseText); 
	                        $.unblockUI();                      
	                    },beforeSend:function() {
	                        $.blockUI();                        
	                    },complete:function() {
	                        $.unblockUI();                      
	                    }                       
	                });     
	                $("#quizExcelUploadFrm").submit();
				}
			}
			
			function resultExcelDownload(feSeq) {
				$("#resultFrm input[name='feSeq']").val(feSeq);
				$("#resultFrm").attr("action", "${HOME}/pf/lesson/formationEvaluation/result/excel/download");
				$("#resultFrm").submit();
			}
			
			function quizResultPopupInit() {
				$("#quizResultPopup #professorPicture").attr("src", "${IMG}/profile_default.png");
				$("#quizResultPopup .title").html("");
				$("#quizResultPopup #absenceCntN").html("0");
            	$("#quizResultPopup #absenceCntY").html("0");
            	$("#quizResultPopup #maxScore").html("최고점 : 0");
            	$("#quizResultPopup #minScore").html("최저점 : 0");
            	$("#quizResultPopup #avgScore").html("평균 : 0");
			}
			
			//학생별 점수 확인
			function quizResultPopupOpen(feSeq) {
				$("#quizResultFrm #feSeq").val(feSeq);
				quizResultPopupInit();
				$.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/pf/lesson/formationEvaluation/result/list",
			        data: $("#quizResultFrm").serialize(),
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	var defaultPicturePath = "${IMG}/profile_default.png";
			            	var pfInfo = data.professor_info;
			            	var feInfo = data.fe_info;
			            	var resultList = data.result_list; 
			            	
			            	var professorPicture = pfInfo.picture_info;
			            	if (pfInfo.picture_info == "") {
			            		professorPicture = defaultPicturePath;  
			            	}
			            	
			            	$("#quizResultPopup #professorPicture").attr("src", professorPicture);
			            	
			            	$("#quizResultPopup .title").html(feInfo.fe_name);
			            	
			            	$("#quizResultPopup #absenceCntN").html(feInfo.absence_cnt_n);
			            	$("#quizResultPopup #absenceCntY").html(feInfo.absence_cnt_y);
			            	$("#quizResultPopup #maxScore").html("최고점 : " + feInfo.max_score);
			            	$("#quizResultPopup #minScore").html("최저점 : " + feInfo.min_score);
			            	$("#quizResultPopup #avgScore").html("평균 : " + feInfo.avg_score);
			            	
			            	var absenceFlag = $("#quizResultFrm #absenceFlag").val();
			            	if (absenceFlag == "N") {
			            		$("#quizResultPopup #absenceCntY").removeClass("on");
			            		$("#quizResultPopup #absenceCntN").addClass("on");
			            	} else {
			            		$("#quizResultPopup #absenceCntN").removeClass("on");
			            		$("#quizResultPopup #absenceCntY").addClass("on");
			            	}
			            	
			            	var resultListHtml = "";
			            	$(resultList).each(function() {
			            		var feScore = this.fe_score;
			            		var studentName = this.name;
			            		var studentId = this.id;
			            		var studentPicture = this.picture_path;
			            		if (isEmpty(studentPicture)) {
			            			studentPicture = "${IMG}/ph_3.png";
			            		}else{
			            			studentPicture = "${RES_PATH}"+this.picture_path;
			            		}
			            		
			            		resultListHtml += '<tr>';
			            		resultListHtml += '    <td class="w1">';
			            		resultListHtml += '        <div class="wrap_s1"><span class="pt01"><img src="' + studentPicture + '" alt="registrasi된 사진 이미지" class="pt_img"></span><span class="ssp1">' + studentId + '</span><span class="ssp2">' + studentName + '</span></div>';
			            		resultListHtml += '    </td>';
			            		resultListHtml += '    <td class="w2">';
			            		if (absenceFlag == "N") {
			            			resultListHtml += '        <div class="wrap_s2">' + feScore + '</div>';
			            		} else {
			            			resultListHtml += '        <div class="wrap_s2">결시</div>';
			            		}
			            		resultListHtml += '    </td>';
			            		resultListHtml += '</tr>';
			            	});
			            	
			            	if (resultList.length > 0) {
			            		$("#quizResultPopup #resultListArea").html(resultListHtml);
			            	} else {
			            		$("#quizResultPopup #resultListArea").html('<tr><td class="w1" colspan="2"><div class="wrap_s1"><span class="ssp1">학생별 점수 데이터가 없습니다.</span></div></td></tr>');;
			            	}
			            	$("#quizResultPopup").show();
			            } else {
			            	alert("학생별 점수확인 조회에 실패하였습니다. [" + data.status + "]");
			            }
			        },
			        error: function(xhr, textStatus) {
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
			
			function quizResultPopupClose() {
				$("#quizResultPopup").hide();
			}
			
			//학생별 점수 확인 응시/결시
			function changeAbsenceFlag(flag) {
				if (flag != "Y" && flag != "N") {
					flag = "N";
				}
				$("#quizResultPopup #absenceFlag").val(flag);
				var feSeq = $("#quizResultFrm #feSeq").val();
				quizResultPopupOpen(feSeq);
				
			}
		</script>
	</head>
	<body>
		<form id="resultFrm" name="resultFrm" method="post">
			<input type="hidden" name ="feSeq">
		</form>
		<form id="thumbnailFrm" name="thumbnailFrm" onsubmit="return false;" enctype="multipart/form-data">
			<input type="hidden" name="feSeq">
			<input type="file" class="blind-position" name="uploadFile">
		</form>
		<div class="main_con">
			<div class="tt_wrap">
				<h3 class="am_tt"><span class="h_date" id="lpDate"></span><span class="tt" id="lpName"></span><span class="tt_s" id="lpCurrculumInfo"></span></h3>
			</div>
			<div class="tab_wrap_cc">
				<button class="tab01 tablinks" onclick="pageMoveLessonPlanView(${S_LP_SEQ},${S_CURRICULUM_SEQ});">수업계획서</button>
				<button class="tab01 tablinks" onclick="pageMoveLessonData('${HOME}');">수업자료</button>
				<button class="tab01 tablinks active" onclick="pageLinkCheck('${HOME}', '${HOME}/pf/lesson/formationEvaluation'); return false;">형성penilaian</button>
				<button class="tab01 tablinks" onclick="pageLinkCheck('${HOME}', '${HOME}/pf/lesson/assignMent'); return false;">과제</button>
				<button class="tab01 tablinks" onclick="pageLinkCheck('${HOME}', '${HOME}/pf/lesson/attendance/view'); return false;">absensi kehadiran</button>
			</div>
			<div id="quizListArea" class="tabcontent tabcontent4" style="position:static;">   
				<table class="stab_wrap"> 
					<tbody>
						<tr class="set" id="feComent"></tr>
					</tbody>
				</table>
			</div> 
		</div>
		
		<!-- s_ 팝업 : 문제registrasi -->
		<div id="quizCreatePopup" class="pop_up_qr mo3_1" >
			<form id="quizCreateFrm" name="quizCreateFrm" onsubmit="return false;" enctype="multipart/form-data">
				<input type="hidden" id="answerText" name="answerText" value="">
				<input type="hidden" id="feSeq" name="feSeq" value="">
				<input type="hidden" id="answerViewCnt" name="answerViewCnt" value="">
				<input type="hidden" id="quizOrder" name="quizOrder" value="">
				<div class="pop_wrap viewer">
					<button class="pop_close close3_1" type="button" onclick="javascript:quizCreatePopupClose();">X</button>
					<p class="t_title">문제registrasi( 퀴즈명 )</p>
					<div class="pop_table">
						<table class="pop_ipwrap1">
							<tbody>
								<tr>
									<td class="ip_tt"><span id="currentQuizNumber">1</span></td>
									<td class="free_textarea"><textarea class="tarea01" style="height: 40px;" placeholder="지문을 입력하세요." id="quizQuestion" name="quizQuestion"></textarea></td>
								</tr>
								<tr>
									<td class="ip_tt"><span>퀴즈 자료</span></td>
									<td class="pop_date">
										<input type="text" class="ip_search" readonly="readonly" id="fileName" name="fileName" placeholder="이미지 최적 사이즈 : 가로 560px이상">
										<button class="btn_search1" onclick="javascript:fileSelect('#quizCreatePopup', this);">파일pilih</button>
										<span class="ip_tt ip_tts">( png, jpg – 용량 10MB 이하 )</span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<div id="imgPreviewArea"></div>
					
					<div class="table_b_wrap">
						<div class="pop_table table_wrap">
							<div class="tab_wrap_p"><span class="sp1">답가지 (<span class="sp_custom_1" id="answerViewCntText">5</span>)</span></div>
							<div class="tab_wrap_p">
								<span class="sp2">정답 : <span class="sp_custom_2" id="correctAnswerText"></span></span>
								<div class="wrap_s">
									<span class="sp3">답가지 순서변경</span>
									<button class="order up" type="button" onclick="javascript:answerOrderUpDown('#quizCreatePopup', true);">▲</button>
									<button class="order dw" type="button" onclick="javascript:answerOrderUpDown('#quizCreatePopup', false);">▼</button>
								</div>
							</div>
							<div  class="pop_twrap1 tabcontent_p tabcontent_p1">
								<table class="tb_pop tab_table ttb1">
									<tbody id="answerArea" name="create"></tbody>
								</table>
								<button class="btn01" type="button" onclick="javascript:addAnswerView('#quizCreatePopup');">답가지 tambahkan</button>
								<button class="btn01_x" type="button" onclick="javascript:removeAnswerView('#quizCreatePopup');">답가지 삭제</button>
							</div>
						</div>    
			
						<div class="t_dd">
							<div class="pop_btn_wrap">
								<button class="btn01 open4" type="button" onclick="javascript:quizCreatePopupClose();">batal</button>
								<button class="btn03" type="button" onclick="javascript:quizCreate();">문제registrasi</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		<!-- e_ 팝업 : 문제registrasi-->
		
		<!-- s_ 팝업 : 문제목록 -->
		<div id="quizListPopup" class="pop_up_qrl mo4">
			<div class="pop_wrap">
				<button class="pop_close close4" type="button" onclick="javascript:quizListPopupClose();">X</button>
				<p class="t_title">문제목록( 퀴즈명 )</p>
				<div class="table_b_wrap">	
					<div class="pop_table table_wrap">	
						<div class="tab_wrap_p">
							<span class="sp2">총 문제수 : <span class="sp_custom_2" id="totalQuizCnt"></span></span>
							<div class="wrap_s" id="quizControllBox">
								<button class="btn02" type="button" onclick="javascript:removeMultiQuiz();">pilih문제 삭제</button>
								<span class="sp3">문제 순서변경</span>
								<button class="order up" type="button" onclick="javascript:quizOrderUpDown(true);">▲</button>
								<button class="order dw" type="button" onclick="javascript:quizOrderUpDown(false);">▼</button>
							</div>
						</div>
						<div class="pop_twrap1 pop_quiz_preview_list">
							<table class="tb_pop">
								<tbody id="quizPreviewListArea"></tbody>
							</table>
						</div>
					</div>		
					<div class="t_dd">
						<div class="pop_btn_wrap">
							<button class="btn03" type="button" id="quizListPopupCloseAndCreateOpenBtn">문제registrasi</button>
						</div>
					</div>
				</div>
			</div>  
		</div>
		<!-- e_ 팝업 : 문제목록-->
		
		<!-- s_팝업 : 문제 미리보기 첫페이지 -->
		<div id="quizPreviewPopup" class="pop_up_qr mo3_2f">
			<div class="pop_wrap viewer">
				<button class="pop_close close3_2f" type="button" onclick="javascript:quizPreviewPopupClose();">X</button>
				<p class="t_title">퀴즈명</p>
				<div class="preview_wrap" id="thumbnailArea"></div>
				<div class="table_b_wrap">	
					<div class="tt_wrap custom">
						<div class="title"></div>
						<div class="tt"><span class="sp_t">Keseluruhan 문제 수</span><span class="sp_n" id="quizCnt">10</span><span class="sp_un">문항</span></div>
						<div class="tt"><span class="sp_t">Keseluruhan 응시자 수</span><span class="sp_n" id="examineeCnt">23</span><span class="sp_un">명</span></div>
					</div>   
					<div class="go_wrap">
						<a href="#" class="arrow02" id="preBtn">◀</a>
						<div class="pop_btn_wrap">
							<span class="bgo">이전</span>
							<span class="fgo">다음</span>
						</div>
						<a href="#" class="arrow01" id="nextBtn">▶</a>
					</div>
					<div class="t_dd">
						<div class="pop_btn_wrap"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- e_팝업 : 문제 미리보기 첫페이지 -->

		<!-- s_ 팝업 : 상세보기 -->
		<div id="quizDetailPopup" class="pop_up_qr mo3_2">
			<!-- s_이미지 삽입 시, class viewer tambahkan -->
			<div class="pop_wrap viewer">
				<button class="pop_close close3_2" type="button" onclick="javascript:quizDetailPopupClose();">X</button>
				<p class="t_title">퀴즈명</p>
				<div class="pop_table">
					<table class="pop_ipwrap1">
						<tbody>
							<tr>
								<td class="ip_tt"><span id="quizNumber">1</span></td>
								<td class="ip_output"><div id="quizQuestion">문항줄기</div></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div id="imgPreviewArea"></div>
				<div class="table_b_wrap">
					<div class="pop_table table_wrap">
						<div  class="pop_twrap1 m3_2">
							<table class="tb_pop tab_table ttb1">
								<tbody id="quizDetailAnswerView"></tbody>
							</table>
						</div>
					</div>
					<div class="go_wrap">
						<a href="#" class="arrow02" id="preBtn">◀</a>
						<div class="pop_btn_wrap">
							<span class="bgo">이전</span>
							<span class="fgo">다음</span>
						</div>
						<a href="#" class="arrow01" id="nextBtn">▶</a>
					</div>
					<div class="t_dd">
						<div class="pop_btn_wrap">
							<button class="btn01 open4" type="button" id="quizListBtn">문제목록</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- e_ 팝업 : 상세보기 -->
		
		<!-- s_ 팝업 : 문제perbaiki -->
		<div id="quizModifyPopup" class="pop_up_qr mo3_1" >
			<form id="quizModifyFrm" name="quizModifyFrm" onsubmit="return false;" enctype="multipart/form-data">
				<input type="hidden" id="answerText" name="answerText" value="">
				<input type="hidden" id="feSeq" name="feSeq" value="">
				<input type="hidden" id="answerViewCnt" name="answerViewCnt" value="">
				<input type="hidden" id="quizOrder" name="quizOrder" value="">
				<input type="hidden" id="quizSeq" name="quizSeq" value="">
				<input type="hidden" id="quizAttachFileSeq" name="quizAttachFileSeq" value="">
				<input type="hidden" id="quizAttachFileDelYn" name="quizAttachFileDelYn" value="">
				<div class="pop_wrap viewer">
					<button class="pop_close close3_1" type="button" onclick="javascript:quizModifyPopupClose();">X</button>
					<p class="t_title">문제registrasi( 퀴즈명 )</p>
					<div class="pop_table">
						<table class="pop_ipwrap1">
							<tbody>
								<tr>
									<td class="ip_tt"><span id="quizNumber">1</span></td>
									<td class="free_textarea" id="quizQuestionArea"></td>
								</tr>
								<tr>
									<td class="ip_tt"><span>퀴즈 자료</span></td>
									<td class="pop_date">
										<input type="text" class="ip_search" readonly="readonly" id="fileName" name="fileName" placeholder="이미지 최적 사이즈 : 가로 560px이상">
										<button class="btn_search1" onclick="javascript:fileSelect('#quizModifyPopup', this);">파일pilih</button>
										<span class="ip_tt ip_tts">( png, jpg – 용량 10MB 이하 )</span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					
					<div id="imgPreviewArea"></div>
					
					<div class="table_b_wrap">
						<div class="pop_table table_wrap">
							<div class="tab_wrap_p"><span class="sp1">답가지 (<span class="sp_custom_1" id="answerViewCntText">5</span>)</span></div>
							<div class="tab_wrap_p">
								<span class="sp2">정답 : <span class="sp_custom_2" id="correctAnswerText"></span></span>
								<div class="wrap_s">
									<span class="sp3">답가지 순서변경</span>
									<button class="order up" type="button" onclick="javascript:answerOrderUpDown('#quizModifyPopup', true);">▲</button>
									<button class="order dw" type="button" onclick="javascript:answerOrderUpDown('#quizModifyPopup', false);">▼</button>
								</div>
							</div>
							<div  class="pop_twrap1 tabcontent_p tabcontent_p1">
								<table class="tb_pop tab_table ttb1">
									<tbody id="answerArea" name="modify"></tbody>
								</table>
								<button class="btn01" type="button" onclick="javascript:addAnswerView('#quizModifyPopup');">답가지 tambahkan</button>
								<button class="btn01_x" type="button" onclick="javascript:removeAnswerView('#quizModifyPopup');">답가지 삭제</button>
							</div>
						</div>    
			
						<div class="t_dd">
							<div class="pop_btn_wrap">
								<button class="btn01 open4" type="button" onclick="javascript:quizModifyPopupClose();">batal</button>
								<button class="btn03" type="button" onclick="javascript:quizModify();">문제perbaiki</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		<!-- e_ 팝업 : 문제perbaiki-->
		
		<!-- s_ 팝업 : 지난 수업 퀴즈 불러오기 -->
		<div id="pastFormationEvaluationPopup" class="pop_up_q mo1">
			<form id="pastListFrm" name="pastListFrm" onsubmit="return false;">
				<input type="hidden" id="currFeSeq" name="currFeSeq" value="">
				<input type="hidden" id="searchYear" name="searchYear" value="">
				<div class="pop_wrap">
					<button class="pop_close close1" type="button"  onclick="javascript:pastFormationEvaluationPopupClose();">X</button>
					<p class="t_title">지난 수업 퀴즈 불러오기</p>
					<div class="pop_table">
						<div class="pop_ipwrap1">
							<div class="wrap2_lms_uselectbox">
								<div class="uselectbox" id="pastYear" value="">
									<span class="uselected">Keseluruhan</span>
									<span class="uarrow">▼</span>
									<div class="uoptions">
									</div>
								</div>
							</div>
							<span class="ip_tt">퀴즈명</span>
							<div class="pop_date">
								<input type="text" class="ip_search" id="searchText" name="searchText" placeholder="퀴즈명을 입력하세요.">
								<button class="btn_search1" onclick="javascript:getPastList();">Pencarian</button>
							</div>
						</div>
					</div>
					<div class="table_b_wrap">
						<div id="pastList">
						</div>
						<div class="t_dd">
							<div class="pop_btn_wrap"><button class="btn01" type="button" onclick="javascript:selectedQuizCopy();">pilih한 퀴즈 불러오기</button></div>
						</div>
					</div>
				</div>
			</form>
		</div>
		<!-- e_ 팝업 : 지난 수업 퀴즈 불러오기 -->
		
		<!-- s_ 엑셀 일괄 registrasi 팝업 -->
		<div id="quizExcelUploadPopup" class="pop_up_ex pop_up_ex_1 mo2">
			<form id="quizExcelUploadFrm" name= "quizExcelUploadFrm" onsubmit="return false;" enctype="multipart/form-data">
				<input type="hidden" id="currFeSeq" name="currFeSeq" value="">
				<input type="file" id="excelFile" name="excelFile" class="blind-position">
				<input type="file" id="dataZipFile" name="dataZipFile" class="blind-position">
			 	<div class="pop_wrap">
			    	<p class="popup_title">퀴즈 엑셀 일괄 업로드</p>
			    	<button class="pop_close close2" type="button" onclick="javascript:quizExcelUploadPopupClose();">X</button>
			    	<button class="btn_exls_down_2" onclick="javascript:quizTemplateDownload();">엑셀 양식 다운로드</button>
				    <div class="t_title_1">
				    	<span class="sp_wrap_1">퀴즈 registrasi 엑셀양식을 다운로드 받으신 뒤 isi을 기입하시고 registrasi해 주세요. <br>사진, 영상파일은 자료registrasi에 퀴즈 번호를 파일명으로 지정 후 registrasi해 주세요.<br>( 자료 zip 압축 파일은 없을 경우 registrasi하지 않습니다. )</span>
				    </div>
				    <div class="pop_ex_wrap">
				     	<div class="sub_fwrap_ex_1">
				        	<span class="ip_tt">file excel</span>
				        	<input type="text" class="ip_sort1_1" id="excelFileName" readonly="readonly" value="">
				        	<button class="btn_r1_1" onclick="javascript:excelFileSelect();">파일pilih</button>
				      	</div>
				      	<div class="sub_fwrap_ex_1"> 
				        	<span class="ip_tt">자료 zip 압축 파일</span>
				        	<input type="text" class="ip_sort1_1" id="dataZipFileName" readonly="readonly" value="">
				        	<button class="btn_r1_1" onclick="javascript:dataZipFileSelect();">파일pilih</button>
				      	</div>
				      	<div class="t_dd">
							<div class="pop_btn_wrap">
					          	<button type="button" class="btn01" onclick="javascript:quizExcelUpload();">registrasi</button>
					          	<button type="button" class="btn02" onclick="javascript:quizExcelUploadPopupClose();">batal</button>
				        	</div>
						</div>
				    </div>
				</div>
			</form>
		</div>
		<!-- e_ 엑셀 일괄 registrasi 팝업 -->
		
		<!-- s_ 팝업 : 학생별 점수 확인 -->
		<div id="quizResultPopup" class="pop_up_q_grchk mo5">
			<!-- s_이미지 삽입 시, class viewer tambahkan -->
			<form id="quizResultFrm" name= "quizResultFrm" onsubmit="return false;">
				<input type="hidden" id="feSeq" name="feSeq">
				<input type="hidden" id="absenceFlag" name="absenceFlag" value="N">
				<div class="pop_wrap viewer">
					<button class="pop_close close5" type="button" onclick="javascript:quizResultPopupClose();">X</button>
					<p class="t_title">학생별 점수 확인</p>
					<div class="pop_wrap1">
						<div class="a_mp"><span class="pt01"><img src="${IMG}/ph_r1.png" alt="registrasi된 사진 이미지" class="pt_img" id="professorPicture"></span></div>
						<div class="title">퀴즈 제목이 출력되는 자리입니다.</div>
					</div>
					<div class="sp_wrap">
						<span class="sp01">응시</span><span class="sp_n_wrap"><span class="sp_n btn_tab1 on" id="absenceCntN" onclick="javascript:changeAbsenceFlag('N');">0</span></span><span class="sp02">명</span>
						<span class="sp01">결시</span><span class="sp_n_wrap"><span class="sp_n btn_tab2" id="absenceCntY" onclick="javascript:changeAbsenceFlag('Y');">0</span></span><span class="sp02">명</span>
					</div>
					<button class="gr_wrap"><span id="maxScore">최고점 : 0</span><span id="minScore">최저점 : 0</span><span id="avgScore">평균 : 0</span></button>
					<div class="table_b_wrap">	
						<div class="pop_table table_wrap">
							<div  class="pop_twrap1 tb_tab1">
								<table class="tb_pop tab_table ttb1" id="resultListArea"></table>
							</div>
						</div>		
						<div class="t_dd">
							<div class="pop_btn_wrap">
								<button class="btn01" type="button" onclick="javascript:quizResultPopupClose();">닫기</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		<!-- e_ 팝업 : 학생별 점수 확인 -->
	</body>
</html>