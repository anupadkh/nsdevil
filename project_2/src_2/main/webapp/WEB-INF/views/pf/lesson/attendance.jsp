<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
	<script type="text/javascript">
		$(document).ready(function() {
			getAttendanceList();
			//수업계획서 타이틀 가져오기
            getLessonTitleInfo();
            
		});
		
	    function getLessonTitleInfo(){
            
            $.ajax({
                type: "POST",
                url: "${HOME}/ajax/pf/lesson/lessonPlanBasicInfo",
                data: {                  
                },
                dataType: "json",
                success: function(data, status) {
                	if(data.status == "200"){
	                    var lesson_date = data.lessonPlanBasicInfo.lesson_date.split("/");
	                    var aca_name = data.lessonPlanBasicInfo.year + " " + data.lessonPlanBasicInfo.l_aca_system_name + " " + data.lessonPlanBasicInfo.m_aca_system_name + " " + data.lessonPlanBasicInfo.aca_system_name;
	                    
	                    $("span[name=lesson_date]").text(data.lessonPlanBasicInfo.lesson_date);
	                    $("span[name=lesson_subject]").text("["+data.lessonPlanBasicInfo.period+" pengajar] "+data.lessonPlanBasicInfo.lesson_subject);                    
	                    $("span[name=curr_name]").text(" ("+data.lessonPlanBasicInfo.curr_name +" / "+ aca_name+")");                    
                	}
                },
                error: function(xhr, textStatus) {
                    //alert("오류가 발생했습니다.");
                    document.write(xhr.responseText);
                }
            }); 
        }
        
		function getAttendanceList(){
            $.ajax({
                type: "POST",
                url: "${HOME}/ajax/pf/lesson/attendance/list",
                data: {                 
                },
                dataType: "json",
                success: function(data, status) {
                	
                	if(data.status=="200"){
                		$("span[name=total]").text(data.attendanceCount.st_cnt+"명");
                		$("span[name=absence]").text(data.attendanceCount.absence);
                		$("span[name=lateness]").text(data.attendanceCount.lateness);
                		$("span[name=attendance]").text(data.attendanceCount.attendance);
                        
                		var htmls = "";                		
                		$.each(data.attendanceList,function(index){
                			var group = "";
                            var readonly_yn = "readonly";
                            
                            if(this.group_number != 0){
	                            if(this.group_leader_flag=="Y")
	                                group = this.group_number+"조-조장";
	                            else 
	                                group = this.group_number+"조";                            
                            }else{
                            	group = "-";
                            }
                            
                			htmls = '<tr><td class="td_1">'+(index+1)+'</td>';
                			htmls+='<td class="td_1">'+this.id+'</td>';
                			htmls+='<td class="td_1 t_l"><span class="a_mp"><span class="pt01">';
                			
                			if(!isEmpty(this.picture_name))
                				htmls+='<img src="${RES_PATH}'+this.picture_path+'/'+this.picture_name+'" class="pt_img">';
                				
                			htmls+='</span>';
                			htmls+='<span class="ssp1">'+this.name+'</span></span></td>';
                			
                				htmls+='<td class="td_1">'+group+'</td>';
                				
                			if(this.attendance_state == '03')
                				htmls+='<td class="td_1 time1">'+this.name+'</td>';
                			else
                				htmls+='<td class="td_1"></td>';
                				
               				if(this.attendance_state == '02')
               					htmls+='<td class="td_1 time2">'+this.name+'</td>';
               					
               				else
               					htmls+='<td class="td_1"></td>';
                                
               				if(this.attendance_state == '00')
               					htmls+='<td class="td_1 time3">'+this.name+'</td>';
               				else
               					htmls+='<td class="td_1"></td>';
               					
               				$("#attendanceListAdd").append(htmls);
                		});
                	}else{
                		alert("실패");
                	}
                	
                },
                error: function(xhr, textStatus) {
                    //alert("오류가 발생했습니다.");
                    document.write(xhr.responseText);
                }
            }); 
        }		
	</script>
	</head>
	<body>
		<!-- s_tt_wrap -->
		<div class="tt_wrap">
			<h3 class="am_tt">
				<span class="h_date" name="lesson_date"></span>
				<span class="tt" name="lesson_subject"></span>
				<span class="tt_s" name="curr_name"></span>
			</h3>
		</div>
		<!-- e_tt_wrap -->
	
		<!-- s_tab_wrap_cc -->
		<div class="tab_wrap_cc">
			<button class="tab01 tablinks" onclick="pageMoveLessonPlanView(${S_LP_SEQ},${S_CURRICULUM_SEQ});">수업계획서</button>
			<button class="tab01 tablinks" onclick="pageMoveLessonData('${HOME}');">수업자료</button>
			<button class="tab01 tablinks" onclick="pageLinkCheck('${HOME}', '${HOME}/pf/lesson/formationEvaluation'); return false;">형성penilaian</button>
			<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/assignMent'">과제</button>
			<button class="tab01 tablinks active" onclick="location.href='${HOME}/pf/lesson/attendance/view'">absensi kehadiran</button>
		</div>
		<!-- e_tab_wrap_cc -->
	   
        
		<!-- s_tabcontent -->
		<div id="tab001" class="tabcontent tabcontent6">   

		<!-- s_hw_wrap-->
		<div class="hw_wrap_l">         
		    <div class="tt_t">
			    <span class="sp1">총</span><span name="total"></span>
			    <span class="sp2">absensi kehadiran</span><span name="attendance"></span>
			    <span class="sp3">지각</span><span name="lateness"></span>
			    <span class="sp4">결석</span><span name="absence"></span>
		    </div>       
		</div>
		<!-- e_hw_wrap-->
		
		<table class="mlms_tb1">
			<thead>
			    <tr>
			        <th class="th01 b_2 w1">No.</th>
			        <th class="th01 bd01 w3">학번</th>
			        <th class="th01 bd01 w4">학생</th>
			        <th class="th01 bd01 w3">조</th>
			        <th class="th01 bd01 w2 time1">absensi kehadiran</th>
			        <th class="th01 bd01 w2 time2">지각</th>
			        <th class="th01 b_1 w2 time3">결석</th>
			    </tr>
			</thead>
			<tbody id="attendanceListAdd">  
			
<!-- s_absensi kehadiran: 해당 td 내 class:time1 -->
                        <!-- <tr>
                            <td class="td_1">1</td>
                            <td class="td_1">20156589</td>
                            <td class="td_1 t_l"><span class="a_mp"><span class="pt01"><img src="img/ph37.png" alt="registrasi된 사진 이미지" class="pt_img"></span><span class="ssp1">강푸르메</span></span></td>
                            <td class="td_1">1조-조장</td>
                            <td class="td_1 time1">나이름</td>
                            <td class="td_1">&nbsp;</td>
                            <td class="td_1"></td>
                        </tr> -->
<!-- e_absensi kehadiran: 해당 td 내 class:time1 -->
<!-- s_지각: 해당 td 내 class:time2 --> 
                        <!-- <tr>
                            <td class="td_1">2</td>
                            <td class="td_1">20156589</td>
                            <td class="td_1 t_l"><span class="a_mp"><span class="pt01"><img src="img/ph_3.png" alt="registrasi된 사진 이미지" class="pt_img"></span><span class="ssp1">나아름드리</span></span></td>
                            <td class="td_1">5조</td>
                            <td class="td_1"></td>
                            <td class="td_1 time2">마이름</td>
                            <td class="td_1">&nbsp;</td>
                        </tr> -->
<!-- e_지각: 해당 td 내 class:time2 --> 
<!-- s_결석: 해당 td 내 class:time3 --> 
                        <!-- <tr>
                            <td class="td_1 w1">3</td>
                            <td class="td_1">20156589</td>
                            <td class="td_1 t_l"><span class="a_mp"><span class="pt01"><img src="img/ph37.png" alt="registrasi된 사진 이미지" class="pt_img"></span><span class="ssp1">차이름</span></span></td>
                            <td class="td_1">3조</td>
                            <td class="td_1">&nbsp;</td>
                            <td class="td_1"></td>
                            <td class="td_1 time3">차이름</td>
                        </tr> -->
<!-- e_결석: 해당 td 내 class:time3 --> 
		    </tbody>
		</table>
		</div> 
		<!-- e_tabcontent -->
	</body>
</html>