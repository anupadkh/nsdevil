<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
	<script type="text/javascript">
		$(document).ready(function() {
			getCurriculum();
			//단원 리스트 가져온다.
	        getUnitList();
		});
		
		//교육과정 가져오기
		function getCurriculum() {
			$.ajax({
				type : "GET",
				url : "${HOME}/ajax/pf/lesson/curriculumInfo",
				data : {},
				dataType : "json",
				success : function(data, status) {
					if (data.status == "200") {

						$("#curr_name").text(data.basicInfo.curr_name);
						$("#aca_system_name").text("(" + data.basicInfo.aca_system_name + ")");
                        
					} else {

					}
				},
				error : function(xhr, textStatus) {
					document.write(xhr.responseText);
				},
				beforeSend : function() {
					$.blockUI();
				},
				complete : function() {
					$.unblockUI();
				}
			});
		}
		
		//단원 가져오기
	    function getUnitList(){
	        $.ajax({
	            type : "POST",
	            url : "${HOME}/ajax/mpf/lesson/unit/unitList",
	            data : {
	               
	            },
	            dataType : "json",
	            success : function(data, status) {
	                $("#unitTab tbody").remove();
	                var pre_unit_seq = "";

	                if(data.unitList.length == 0)
	                	$("button[name=unitTab]").hide();
	                else{
		                $.each(data.unitList, function(index) {   
		                    var htmls = "";                 
		                    if(this.type == "UNIT"){
		                        if(this.unit_seq != pre_unit_seq){
		                            htmls += '<tbody class="lv1" name="'+this.unit_seq+'"><tr class="tr02 lv2" name="'+this.unit_seq+'_'+this.lp_seq+'"><td class="pm0 free_textarea">';
		                            htmls += '<input type="hidden" name="unit_seq" value="Y_'+this.unit_seq+'"/>'+this.unit_name+'</td>';
		                            htmls += '<td class="pm0 free_textarea"><input type="hidden" name="lp_seq" value="Y_'+this.unit_seq+'_'+this.lp_seq+'"/>'+this.lesson_subject+'</td>';
		                            htmls += '<td class="ta_c">'+this.period_cnt+'</td>';
		                            htmls += '<td colspan="7" class="color1 ta_c"></td></tr></tbody>';
		                            $("#unitTab").append(htmls);
		                        }else{
		                            htmls += '<tr class="tr02 lv2" name="'+this.unit_seq+'_'+this.lp_seq+'"><td class="pm0 free_textarea"></td>';
		                            htmls += '<td class="pm0 free_textarea"><input type="hidden" name="lp_seq" value="Y_'+this.unit_seq+'_'+this.lp_seq+'_'+this.lp_seq+'"/>'+this.lesson_subject+'</td>';
		                            htmls += '<td class="ta_c">'+this.period_cnt+'</td>';
		                            htmls += '<td colspan="7" class="color1 ta_c"></td></tr>';
		                            $("#unitTab").append(htmls);
		                        }
		                    }
		                    else if(this.type == "TLO"){
		                        htmls += '<tr class="tr02 lv3" name="'+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+'"><td class="pm0 free_textarea" colspan="3"></td>';
		                        htmls += '<td colspan="4" class="color1 ta_c"><input type="hidden" name="tlo_seq" value="Y_'+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+'"/>';
		                        htmls += '<span class="tts3">TLO '+this.tlo_order_num+'</span></td>';
		                        htmls += '<td class="pm0 free_textarea">'+this.skill+'</td>';
		                        htmls += '<td class="pm0 free_textarea">'+this.teaching_method+'</td>';
		                        htmls += '<td class="pm0 free_textarea">'+this.evaluation_method+'</td></tr>';                       
		                        if($("#unitTab tr[name^='"+this.unit_seq+'_'+this.lp_seq+"_']").length == 0)
		                            $("#unitTab tr[name='"+this.unit_seq+'_'+this.lp_seq+"']").after(htmls);
		                        else
		                            $("#unitTab tr[name^='"+this.unit_seq+'_'+this.lp_seq+"_']").last().after(htmls);
		                    }else if(this.type == "ELO"){
		                        htmls += '<tr class="tr02 lv4" name="'+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+'_'+this.elo_seq+'">';
		                        htmls += '<td class="" colspan="3"><input type="hidden" name="elo_seq" value="Y_'+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+'_'+this.elo_seq+'"/></td>';
		                        htmls += '<td class="ta_c">ㄴ</td>';
		                        htmls += '<td class="ta_c">ELO '+this.elo_order_num+'</td>';
		                        htmls += '<td class="ta_c pm0">'+this.domain_name+'</td>';
		                        htmls += '<td class="ta_c pm0">'+this.level_name+'</td>';
		                        htmls += '<td colspan="3" class="pm0 free_textarea">'+this.content+'</td></tr>';
		                        if($("#unitTab tr[name^='"+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+"_']").length == 0)            
		                            $("#unitTab tr[name='"+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+"']").after(htmls);
		                        else
		                            $("#unitTab tr[name^='"+this.unit_seq+'_'+this.lp_seq+'_'+this.tlo_seq+"_']").last().after(htmls);  
		                    }
		                    pre_unit_seq = this.unit_seq;
		                });
	                }
	            },
	            error : function(xhr, textStatus) {
	                //alert("오류가 발생했습니다.");
	                document.write(xhr.responseText);
	            },
	            beforeSend : function() {
	            },
	            complete : function() {
	            }
	        });
	    }
	</script>
	</head>
	<body>
		<!-- s_tt_wrap -->
		<div class="tt_wrap">
			<h3 class="am_tt">
                <span class="tt" id="curr_name"></span>
                <span class="tt_s" id="aca_system_name"></span>
            </h3>
		</div>
		<!-- e_tt_wrap -->
	
		<!-- s_tab_wrap_cc -->
		<div class="tab_wrap_cc" style="float:left;margin-bottom:15px;">
			<button class="tab01 tablinks long" onclick="pageMoveCurriculumView(${S_CURRICULUM_SEQ});">rencan kurikulum</button>
			<button class="tab01 tablinks" onclick="location.href='${HOME}/pf/lesson/lessonPlanCs'">수업waktu표</button>
			<button class="tab01 tablinks active" onclick="location.href='${HOME}/pf/lesson/unitView'">단원manajemen</button>

		</div>
		<!-- e_tab_wrap_cc -->
	
		<!-- s_tabcontent -->
		<div id="tab001" class="tabcontent tabcontent1">	        
	         <table class="tab_table ttb1" id="unitTab">
		        <thead class="mainTbody">                   
		            <tr class="tr02">
		                <td class="th01 w11">단원명<br>(UNIT)</td>
		                <td class="th01 w11">수업제목<br>(시수)</td>
		                <td class="th01 w13">시수</td>
		                <td colspan="2" class="th01 w14">구분</td>
		                <td class="th01 w13">Area</td>
		                <td class="th01 w13">tingkat</td>
		                <td class="th01 w12">teknologi</td>
		                <td class="th01 w11">수업<br>방법</td>
		                <td class="th01 w11">penilaian<br>방법</td>
		            </tr>   
		        </thead>
		    </table>    
		</div>
		<!-- e_tabcontent -->
	</body>
</html>