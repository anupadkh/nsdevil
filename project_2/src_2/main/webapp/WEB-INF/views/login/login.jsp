<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="Publisher" content="MLMS">
		<meta name="Keywords" content="MLMS">
		<meta name="Description" content="의과대학 LMS">
		<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">
		<meta name="viewport" content="width=1100">
		
		<title>MLMS</title>
		
		<link rel="icon" type="${IMG}/png" href="${IMG}/favicon.png">
		<link rel="shortcut icon" href="${IMG}/favicon.png">
		<link rel="icon" href="${IMG}/favicon.ico">
		<link rel="apple-touch-icon" href="${IMG}/favicon.ico"> 
		<link rel="stylesheet" href="${CSS}/pf_style.css" type="text/css">
		<link rel="stylesheet" href="${CSS}/dev_pf_style.css" type="text/css">
		<script src="${JS}/lib/jquery-1.11.1.js"></script>
		<script src="${JS}/lib/jquery.cookie.js"></script>
		<script src="${JS}/lib/jquery.blockUI.js"></script>
		<script src="${JS}/common.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				topPopupInit();
				initPageTopButton();
				customSelectBoxInit();
				$(document).mouseup(function(e){
					layerPopupCloseInit(['div[name="myProfilePopupDiv"]']);
				});
			});
			
			function loginSubmit() {
			    
				var id = $("#id").val();
				if (isEmpty(id) || isBlank(id)) {
					alert("아이디를 입력해주세요");
					$("#id").focus();
					return;
				}
				
				var pwd = $("#pwd").val();
				if (isEmpty(pwd) || isBlank(pwd)) {
					alert("비밀번호를 입력해주세요");
					$("#pwd").focus();
					return;
				}
			    
			    $.ajax({
			        type: "POST",
			        url: "${HOME}/ajax/login",
			        data: {
			            "id" : id,
			            "pwd" : pwd
			        },
			        dataType: "json",
			        success: function(data, status) {
			            if (data.status == "200") {
			            	if (data.user_level == 1) { //manajemen자
			            		location.href = "${HOME}/admin/academic/academicSystem/modify";
			            	}
			            	location.href = "${HOME}";
			            } else if (data.status == "102") {
			            	alert("사용할 수 없는 계정입니다.");
			        	}else {
			                alert("잘못된 ID 또는 비밀번호 입니다.");
			            }
			        },
			        error: function(xhr, textStatus) {
			            //alert("오류가 발생했습니다.");
			            document.write(xhr.responseText);
			        },
			        beforeSend:function() {
			            $.blockUI();
			        },
			        complete:function() {
			            $.unblockUI();
			        }
			    });
			}
		</script>
	</head>

	<body>
		<noscript title="브라우저 자바스크립트 차단 해제 안내">
			<p>사용하시는 브라우저가 자바스크립트를 차단하지 않도록 설정을 바꾸어 주시면 편리하게 이용하실 수 있습니다.</p>
		</noscript>
	
		<!-- s_skipnav -->
		<div id="skipnav">
			<a href="#gnb">주 메뉴 바로가기</a>
			<a href="#container">컨텐츠 바로가기</a>
		</div>
		<!-- e_skipnav -->
	
		<!-- s_wrap -->
		<div id="wrap">
			<!-- s_top_pop -->
			<div id="top_pop">
				<div class="wrap">
					<div class="slide-wrap">
						<ul>
							<li class="tt"><img src="${IMG}/pop001.png" alt="의과대학 의학교육 penilaian인증 인증 획득 4년"></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- e_top_pop -->
		
			<!-- s_header -->
			<header class="header">
				<div class="header_top">
					<div class="top_wrap">
						<div class="top_m">
							<!-- //TODO 임시로 제외시킴 -->
							<!-- <a href="javascript:void(0);" id="topPopupBtn" class="a_m a1" onclick="javascript:topPopupToggle();">팝업닫기</a> -->
							<c:choose>
								<c:when test='${HOME ne ""}'>
									<a href="${HOME}" class="a_m a3">HOME</a>
								</c:when>
								<c:otherwise>
									<a href="/" class="a_m a3">HOME</a>
								</c:otherwise>
							</c:choose>
							<a href="${HOME_URL }" class="a_m a6">기관Home</a>
						</div>
					</div>
				</div>
				
				<!-- s_gnbwrap -->
				<div class="gnbwrap">
					<!-- s_gnb_swrap -->
					<div class="gnb_swrap">
						<h1 class="logo">
						<c:choose>
							<c:when test='${HOME ne ""}'>
								<a href="${HOME}" title="홈으로 가기"><img src="${TOP_LOGO}" alt="의과대학 로고"></a>
							</c:when>
							<c:otherwise>
								<a href="/" title="홈으로 가기"><img src="${TOP_LOGO}" alt="의과대학 로고">	</a>
							</c:otherwise>
						</c:choose>
						
						</h1>
					</div>
					<!-- e_gnb_swrap -->
					
					<!-- s_gnb_fwrap_i -->
					<div class="gnb_fwrap_i">
						<div class="gnb_swrap2">
							<ul id="gnb" class="gnb">
								<c:forEach items="${sessionScope.S_ALLOW_MENU_LIST}" var="allowMenuList" step="1">
									<li><a href="${HOME}${allowMenuList.url}" <c:if test='${allowMenuList.now_menu_yn eq "Y"}'>class="on"</c:if>>${allowMenuList.name}</a></li>
								</c:forEach>
							</ul>
						</div>
					</div>
					<!-- e_gnb_fwrap_i -->
				</div>
				<!-- e_gnbwrap -->
			</header>
			<!-- e_header -->
		
			<!-- s_container_table -->
			<div id="container" class="container_table">
				<!-- s_contents -->
				<div class="contents login">
					<!-- s_login_logo_wrap -->
					<div class="login_logo_wrap cku_login_logo_wrap">
						<img src="${LOGIN_LOGO}" alt="" class="logo_m">
					</div>
					<!-- e_login_logo_wrap -->
					
					<!-- s_idpw_wrap -->
					<div class="idpw_wrap">
						<input type="text" id="id" placeholder="학번 / 사번" onkeypress="javascript:if(event.keyCode==13){loginSubmit(); return false;}">
						<input type="password" id="pwd" placeholder="비밀번호" onkeypress="javascript:if(event.keyCode==13){loginSubmit(); return false;}">
						<button type="button" onclick="loginSubmit();">로그인</button>
					</div>   
					<!-- e_idpw_wrap -->
				</div>
				<!-- e_contents -->
			</div>
			<!-- e_container_table -->
		
			<a href="#" id="backtotop" title="To top" class="totop"></a>
		
			<!-- s_footer -->
			<footer id="footer" class="ind">
		        <div class="footer_con">           
		            <div class="footer_scon">   
		                <ul class="f_menu">
						    <li><a href="#" title="새창" class="fmn1">개인정보처리방침</a></li>
		                    <li><a href="#" title="새창" class="fmn2"><span>|</span>이메일무단수집거부</a></li>                
					    </ul>   
		                <p class="addr">${ADDRESS }</p>
		                <a href="${HOME}" title="홈으로 가기" class="logo_wrap"><img src="${BOTTOM_LOGO}" alt="의과대학 로고" class="b_logo"></a>
		            </div>
		        </div>
			</footer>
			<!-- e_footer -->
		</div>
		<!-- e_wrap -->
	</body>
</html>