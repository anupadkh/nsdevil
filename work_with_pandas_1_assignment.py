import pandas as pd

mypath = '/Users/anupadkh/Desktop/nsdevil/input.xlsx'

dfs = pd.read_excel(mypath, header=[0,1], sheet_name=None)
df = dfs['Original Data']
a = dfs['Original Data'].columns.levels[0] # Get Headers from Level 1

''' Reading the Column Names
for y in a:
    # print(y)
    m = df[y].columns
    v = df[y]
    for r in m:
        x = v[r]
        print (y + ':' + r)
        # print (list(x))

>>> df.columns.levels[0][0]
'Catergory'
>>> a = df.columns.levels[0][0]
>>> df[a].columns
Index(['Name', 'License No.', 'Institute', 'Year', 'Region', 'Site',
       'Appication'],
      dtype='object', name='Reg. No.')

'''
indices_detail ={
0:[0,2,3],
1:[0,2,4,5,6],
# 1:[0,2,4,5,6],
}
report_indices = {
0:[0,3],
1:[4,5]
}
'''
Catergory:License No.
Catergory:Institute
Catergory:Year
Catergory:Region
Catergory:Site
Catergory:Appication
Overall:Full Marks
Overall:Converted Full Marks
(upto 100)
Overall:Original
Score
Overall:Converted
Score
Overall:Ranking
in Overall
Overall:Ranking
 in the Year

Overall:Ranking
in the Institute
'''
