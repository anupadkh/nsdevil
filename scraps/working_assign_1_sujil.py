import pandas as pd

dfs = pd.read_excel('input.xlsx', header=[0,1], sheet_name=None)

df = pd.concat(dfs.values())

df = df.rename_axis([('Catergory','Reg. No.')]).reset_index()

df.columns.names = ['H1','H2']

df.to_excel("input_formatted.xlsx")
dfs = pd.read_excel('input_formatted.xlsx', header=[0,1], sheet_name=None)
df = pd.concat(dfs.values())
newdf = df[[ ('Catergory','Reg. No.'),('Catergory','Name'),('Catergory','Year'),('Overall','Original\nScore'),('Overall','Ranking \nin Overall')
         ,('Overall','Ranking\n in the Year'),('Subject 1','Original\nScore'),('Subject 2','Original\nScore'),('Subject 3','Original\nScore'),('Subject 4','Original\nScore'),('Subject 5','Original\nScore')
        ,('Subject 6','Original\nScore'),('Subject 7','Original\nScore'),('Subject 8','Original\nScore'),('Subject 9','Original\nScore'),('Subject 10','Original\nScore')
        ,('Subject 11','Original\nScore')]]
newdf.columns=['Reg. No.','Name','Year','Total','Ranking in whole Year','Ranking in same Year','Subject 1','Subject 2','Subject 3'
           ,'Subject 4','Subject 5','Subject 6','Subject 7','Subject 8','Subject 9','Subject 10','Subject 11']
df2 = pd.DataFrame([['Full Marks','','',df['Overall']['Full Marks'][0],'-','-'
                    ,df['Subject 1']['Full Marks'][0]
                    ,df['Subject 2']['Full Marks'][0]
                    ,df['Subject 3']['Full Marks'][0]
                    ,df['Subject 4']['Full Marks'][0]
                    ,df['Subject 5']['Full Marks'][0]
                    ,df['Subject 6']['Full Marks'][0]
                    ,df['Subject 7']['Full Marks'][0]
                    ,df['Subject 8']['Full Marks'][0]
                    ,df['Subject 9']['Full Marks'][0]
                    ,df['Subject 10']['Full Marks'][0]
                    ,df['Subject 11']['Full Marks'][0]
                   ]],
                  columns=list('ABCDEFGHIJKLMNOPQ'))
df2.columns=['Reg. No.','Name','Year','Total','Ranking in whole Year','Ranking in same Year','Subject 1','Subject 2','Subject 3'
           ,'Subject 4','Subject 5','Subject 6','Subject 7','Subject 8','Subject 9','Subject 10','Subject 11']
final = df2.append(newdf)
final.to_excel("Result_Institute.xlsx")
