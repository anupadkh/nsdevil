package com.nsdevil.knuprof.main;

import java.io.File;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DownloadController implements ApplicationContextAware {
	
	@Inject
	private FileSystemResource fileSystemResource;
	
	private WebApplicationContext context = null;

	@RequestMapping("/download")
	public ModelAndView download(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		File downloadFile = getFile(request);
		
		String orignalName = request.getParameter("orignalName");
		
		ModelAndView modelAndView = new ModelAndView("downloadView");
		modelAndView.addObject("downloadFile", downloadFile);
		
		if (orignalName != null && orignalName.trim().length() > 0) {
			modelAndView.addObject("orignalName", orignalName);
		}
		
		return modelAndView;
	}

	private File getFile(HttpServletRequest request) {
		
		String path = request.getParameter("path");
		
		if (path != null && path.trim().length() > 0) {
			path = context.getServletContext().getRealPath("resources/" + path + "/" + request.getParameter("fileName"));
		} else {
			path = fileSystemResource.getPath() + "/upload/" + request.getParameter("fileName");
		}
		return new File(path);
	} 
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.context = (WebApplicationContext)applicationContext;
	}

}