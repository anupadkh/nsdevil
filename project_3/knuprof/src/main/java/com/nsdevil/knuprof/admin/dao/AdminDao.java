package com.nsdevil.knuprof.admin.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nsdevil.knuprof.admin.excel.ExcelAchieveEducationVo;
import com.nsdevil.knuprof.admin.excel.ExcelAchieveServiceVo;

import com.nsdevil.knuprof.admin.excel.ExcelAchieveResearchVo;

public interface AdminDao {
	public List<Map<String,Object>> getProfessorList(Map<String,Object> param);

	public List<Map<String,Object>> getServiceHtmlData(Map<String,Object> param);
	
	public Map<String,Object> getCountAchieveRegistUser(Map<String,Object> param);
	
	public  List<Map<String,Object>> getAchieveRegistUserList(Map<String,Object> param);
	
	public Map<String,Object> addAchieveRegistService(ExcelAchieveServiceVo vo);
	
	public List<Map<String, Object>> getAchieveProfessorResult(Map<String, Object> param);
	public List<Map<String, Object>> getAchievePointInfo(Map<String, Object> param);

	public int upsertAchieveRegistEducation(ExcelAchieveEducationVo vo);
	
	public int upsertAchieveRegistResearch(ExcelAchieveResearchVo vo);
	public int deleteAchieveResearchResult(Map<String,Object> map);

	public List<Map<String,Object>> getAchieveResearchFileList(Map<String,Object> map);
	public HashMap<String, Object> achieveResearchUploadFile(HashMap<String, Object> param);
	public int deleteFile(Map<String,Object> param);
	public List<Map<String,Object>> getFileCountByResearchCode(Map<String,Object> map);

	public int getPeriodListTotalCount(Map<String, Object> param);
	public List<Map<String,Object>> getPeriodList(Map<String, Object> param);
	public Map<String, Object> addPeriod(Map<String, Object> param);
	public int updatePeriod(Map<String, Object> param);

	public Map<String, Object> addMyNotice(Map<String,Object> param);
	public int updateMyNotice(Map<String,Object> param);
	
	public List<Map<String, Object>> getAchieveGradeScoreInfo(Map<String, Object> param);
	
	public int upsertAchievePeriodCriterion(HashMap<String,Object> map);
	public List<Map<String,Object>> getPeriodCriterion(Map<String,Object> map);
	
	public int upsertAchievePeriodGrade(Map<String,Object> map);
	public Map<String,Object> getPeriodGrade(Map<String,Object> map);
	
	public int upsertAchievePeriodMinPoint(Map<String,Object> map);
	
	public  List<String>getPeriodYearInfo();
	
	public int updateGradeScoreInfo_education( Map<String,Object> map);
	public int updateGradeScoreInfo_research( Map<String,Object> map);
	public int updateGradeScoreInfo_service( Map<String,Object> map);
	public int serviceConfirm(HashMap<String, Object> mapr);

	public int updateService(HashMap<String, Object> map);
}
