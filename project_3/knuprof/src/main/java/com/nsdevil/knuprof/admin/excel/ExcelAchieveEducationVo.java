package com.nsdevil.knuprof.admin.excel;

public class ExcelAchieveEducationVo {
	private String userId;
	private String userCode;
	private String userName;
	private String unit;
	private String contents;
	private String point;
	public String educationCode;
	public String useFlag;
	public ExcelAchieveEducationVo(String educationCode){
		this.educationCode = educationCode;
	}
	public ExcelAchieveEducationVo(String educationCode,String userCode, String unit, String contents,String point,String useFlag){
		this.educationCode = educationCode;
		this.userCode = userCode;
		this.unit = unit;
		this.contents = contents;
		this.point = point;
		this.useFlag = useFlag;
	}
	
	public String getEducationCode() {
		return educationCode;
	}
	public void setEducationCode(String educationCode) {
		this.educationCode = educationCode;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}
	public String getUseFlag() {
		return useFlag;
	}
	public void setUseFlag(String useFlag) {
		this.useFlag = useFlag;
	}
	
}
