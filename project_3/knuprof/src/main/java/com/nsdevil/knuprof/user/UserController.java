package com.nsdevil.knuprof.user;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.HttpRequestHandlerServlet;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.nsdevil.knuprof.main.MainController;
import com.nsdevil.knuprof.prof.service.ProfessorService;
import com.nsdevil.knuprof.user.service.UserService;
import com.nsdevil.knuprof.user.vo.UserInfo;
import com.nsdevil.knuprof.util.FileUploader;
import com.nsdevil.knuprof.util.JSONUtil;
import com.nsdevil.knuprof.util.PageSplitter;
import com.nsdevil.knuprof.util.Util;

import net.sf.json.JSONObject;

/**
 * 사용자 정보를 위한 컨트롤러
 * @author limchul
 *
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private UserService userService; 
	
	@Inject
	private FileSystemResource fileSystemResource;
	
	
	@RequestMapping(value = "/getMyInfo", method = RequestMethod.POST)
	@ResponseBody
	public UserInfo getMyInfo(HttpServletRequest request,HttpSession session, Locale locale, Model model) {
		logger.debug("session.getId() =>"+ session.getId());
		for(String name : session.getValueNames()){
			logger.debug("saved SessionAttribue Name =>"+ name);
			logger.debug(name + " | " + session.getAttribute(name));
		}
		int userCode = (Integer) session.getAttribute("userCode");
		logger.debug("userCode =>"+ userCode);
		if(!(userCode > 0)){
			return null;
		}
		UserInfo userInfo = userService.getUserInfo(userCode);
		return userInfo;
	}
	
	@RequestMapping(value = "/updateMyInfo", method = RequestMethod.POST)
	@ResponseBody
	public String updateMyInfo(HttpServletRequest request,HttpSession session, Locale locale, Model model,@RequestBody String jsonString) {
		logger.debug("@RequestBody String jsonString =>"+ jsonString);

		UserInfo requestUser = null;
		try {
			requestUser = new ObjectMapper(new JsonFactory()).readValue(jsonString, UserInfo.class);
			System.out.println("userInfo.getUserId() :"+ requestUser.getUserId());
		} catch (JsonParseException e) {
			e.printStackTrace();
		} /*catch (JsonMappingException e) {
			e.printStackTrace();
		}*/ catch (IOException e) {
			e.printStackTrace();
		}
		
		String status = "fail";

		JSONObject json = new JSONObject();
		
		
		if(requestUser == null){
			// 파싱에러
			json.accumulate("status", status);
			json.accumulate("message", "통신상의 오류가 발생하였습니다.");
			
			return json.toString();
		}
		int userCode = (Integer) session.getAttribute("userCode");
		UserInfo user = userService.getUserInfo(userCode);
		
		logger.debug("stored =>"+ user.getUserPass() );
		logger.debug("newwww =>"+ Util.getSHA256(requestUser.getUserPass()));
		logger.debug("sotred same =>"+ user.getUserPass().equals(Util.getSHA256(requestUser.getUserPass())));
		
		if(user != null && user.getUserPass().equals(Util.getSHA256(requestUser.getUserPass()))){
			if(requestUser.getNewPass() != null && requestUser.getNewPass().length() >= 4 ){
				requestUser.setNewPass(Util.getSHA256(requestUser.getNewPass()));
			}else{
				requestUser.setNewPass(null);
			}
			logger.debug("requestUser phone=>"+ requestUser.getUserPhone());
			logger.debug("requestUser email=>"+ requestUser.getUserEmail());
			requestUser.setUserCode(userCode);
			int result = userService.updateUserInfo(requestUser);
			logger.debug("userService.updateUserInfo(userInfo) =>"+ result);

		}else{
			json.accumulate("status", status);
			json.accumulate("message", "입력된 비밀번호가 일치 하지 않습니다.");
			return json.toString();
		}
		json.accumulate("status", "success");
		return json.toString();		
	}
	
	
	@RequestMapping(value = "/getUserList", method = RequestMethod.POST)
	@ResponseBody
	public String getUserList(HttpServletRequest request,HttpSession session, Locale locale, Model model,@RequestBody String jsonString) {
		JSONUtil jsonUtil = new JSONUtil(jsonString);
		
		int pageNo = Integer.parseInt(jsonUtil.getString("pageNo", "1"));
		int rowCount = Integer.parseInt(jsonUtil.getString("rowCount", "0"));
		String searchYear = jsonUtil.getString("searchYear", null);
		String searchUserName = jsonUtil.getString("searchUserName", null);
		String searchUserLevel = jsonUtil.getString("searchUserLevel", null);
		
		logger.debug("pageInfo =>"+  pageNo + " / " + rowCount);
		logger.debug("searchInfo =>" + searchYear + " / " + searchUserName + " / " + searchUserLevel);
		JSONObject json = new JSONObject();
		json.accumulate("status", "success");

		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("userLevel =>"+ userLevel);
		
		if(userLevel != 0){
			json.accumulate("status", "fail");
			json.accumulate("message", "권한 오류가 발생하였습니다.");
			return json.toString();
		}
		
		int pagePerRowCount = rowCount > 0 ? rowCount : PageSplitter.PAGE_PER_ROW_COUNT;
		int startRowCount = (pageNo -1) * pagePerRowCount;
		
		logger.debug("pagePerRowCount =>"+ pagePerRowCount);
		logger.debug("startRowCount =>"+ startRowCount);
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("startRowCount", (startRowCount));
		param.put("pagePerRowCount", (pagePerRowCount));
		param.put("searchYear", searchYear);
		param.put("searchUserLevel",searchUserLevel);
		param.put("searchUserName", searchUserName);
		
		logger.debug("startRowCount =>"+ startRowCount  + "  / pagePerRowCount =>"+ pagePerRowCount);
		
		int getUserListTotalCount = userService.getUserListTotalCount(param);
		logger.debug(" getUserListTotalCount =>"+ getUserListTotalCount);
		
		if(getUserListTotalCount > 0){
			List<Map<String,Object>> userList = userService.getUserList(param);
			
			PageSplitter pageSplitter = new PageSplitter(userList, getUserListTotalCount, pageNo, pagePerRowCount);
			json.accumulate("list", pageSplitter.getNumberingResultList());
			json.accumulate("pagingInfo", pageSplitter.getPageNavigationInfo());
		}
		
		return json.toString();
	}
	
	@RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
	@ResponseBody
	public String deleteUser(HttpServletRequest request,HttpSession session, Locale locale, Model model,@RequestBody String jsonString) {
		logger.debug("session.getId() =>"+ session.getId());
		JSONUtil jsonUtil = new JSONUtil(jsonString);
		int deleteUserCode =  Integer.parseInt(jsonUtil.getString("userCode", "0"));
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("userLevel =>"+ userLevel);
		JSONObject json = new JSONObject();
		if(userLevel != 0){
			json.accumulate("status", "fail");
			json.accumulate("message", "권한 오류가 발생하였습니다.");
			return json.toString();
		}
		if(deleteUserCode > 0){
			int deleteResult = userService.deleteUser(deleteUserCode);
			logger.debug("deleteUserResult =>"+ deleteResult);
			json.accumulate("status", "success");
			return json.toString();
		}
		
		return null;
	}
	
	@RequestMapping(value = "/initUserPassword",  method = RequestMethod.POST)
	@ResponseBody
	public String initUserPassword(HttpServletRequest request,HttpSession session, Locale locale, Model model,@RequestParam HashMap<String, Object> param) {
		logger.debug("session.getId() =>"+ session.getId());
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("userLevel =>"+ userLevel);
		JSONObject json = new JSONObject();
		if(userLevel != 0){
			json.accumulate("status", "fail");
			json.accumulate("message", "권한 오류가 발생하였습니다.");
			return json.toString();
		}
		String userCode = (String) param.get("userCode");
		if(userCode  != null){
			param.put("userPass", Util.getSHA256("000000"));
			int initUserPassword = userService.initUserPassword(param);
			logger.debug("initUserPassword =>"+ initUserPassword);
			json.accumulate("status", "success");
			return json.toString();
		}
		
		return null;
	}
	@RequestMapping(value = "/addStaff", method = RequestMethod.POST)
	@ResponseBody
	public String addStaff(HttpServletRequest request,HttpSession session, Locale locale, Model model,@RequestBody String jsonString) {
		logger.debug("session.getId() =>"+ session.getId());
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("userLevel =>"+ userLevel);
		JSONUtil jsonUtil = new JSONUtil(jsonString);
		
		String userId = jsonUtil.getString("userId", null);
		String userName = jsonUtil.getString("userName", null);
		String userPass = jsonUtil.getString("userPass", null);
		List<String> categoryIds = jsonUtil.getStringList("categoryIds");
		
		
		logger.debug( userId + " / " + userName + " / " + userPass + " / " + categoryIds.size());

		JSONObject json = new JSONObject();
		if(userLevel != 0){
			json.accumulate("status", "fail");
			json.accumulate("message", "권한 오류가 발생하였습니다.");
			return json.toString();
		}
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("userId", userId);
		param.put("userName", userName);
		param.put("userPass", Util.getSHA256(userPass));
		param.put("userLevel", 1);
		param.put("categoryIds", categoryIds);
		
		int result = userService.addStaff(param);
		logger.debug("addStaff result =>"+ result);
		if(result == -1){
			json.accumulate("status", "fail");
			json.accumulate("message", "이미 등록된 아이디가 있습니다");
			return json.toString();
		}
		json.accumulate("status", "success");
		
		return json.toString();
	}
	@RequestMapping(value = "/updateStaff", method = RequestMethod.POST)
	@ResponseBody
	public String updateStaff(HttpServletRequest request,HttpSession session, Locale locale, Model model,@RequestBody String jsonString) {
		logger.debug("session.getId() =>"+ session.getId());
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("userLevel =>"+ userLevel);
		JSONUtil jsonUtil = new JSONUtil(jsonString);
		
		String userCode = jsonUtil.getString("userCode", null);
		String userId = jsonUtil.getString("userId", null);
		String userName = jsonUtil.getString("userName", null);
		String userPass = jsonUtil.getString("userPass", null);
		List<String> categoryIds = jsonUtil.getStringList("categoryIds");
		
		
		logger.debug( userCode + " / " + userId + " / " + userName + " / " + userPass + " / " + categoryIds.size());

		JSONObject json = new JSONObject();
		if(userLevel != 0){
			json.accumulate("status", "fail");
			json.accumulate("message", "권한 오류가 발생하였습니다.");
			return json.toString();
		}
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("userCode", userCode);
		param.put("userId", userId);
		param.put("userName", userName);
		param.put("userPass", Util.getSHA256(userPass));
		param.put("userLevel", 1);
		param.put("categoryIds", categoryIds);
		
		int result = userService.updateStaff(param);
		logger.debug("addStaff result =>"+ result);
		
		json.accumulate("status", "success");
		
		return json.toString();
	}
	

	
	@RequestMapping(value = "/addProfessor", method = RequestMethod.POST)
	@ResponseBody
	public String addProfessor(HttpServletRequest request,HttpSession session, Locale locale, Model model,@RequestBody String jsonString) {
		logger.debug("session.getId() =>"+ session.getId());
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("userLevel =>"+ userLevel);
		JSONObject json = new JSONObject();
		if(userLevel != 0){
			json.accumulate("status", "fail");
			json.accumulate("message", "권한 오류가 발생하였습니다.");
			return json.toString();
		}
		
		JSONUtil jsonUtil = new JSONUtil(jsonString);

		String userName = jsonUtil.getString("userName", null);
		String jobTitle = jsonUtil.getString("jobTitle", null);
		String subject = jsonUtil.getString("subject", null);
		String profNum = jsonUtil.getString("profNum", null);
		String userId = jsonUtil.getString("userId", null);
		String userPass = jsonUtil.getString("userPass", null);
		String telNum = jsonUtil.getString("telNum", null);
		String email = jsonUtil.getString("email", null);
		String addition = jsonUtil.getString("addition", null);
		String stPoint = jsonUtil.getString("stPoint", null);
		
		logger.debug("userName :"+ userName);
		logger.debug("jobTitle :"+ jobTitle);
		logger.debug("subject :"+ subject);
		logger.debug("profNum :"+ profNum);
		logger.debug("userId :"+ userId);
		logger.debug("userPass :"+ userPass);
		logger.debug("telNum :"+ telNum);
		logger.debug("email :"+ email);
		logger.debug("addition :"+ addition);
		logger.debug("stPoint :"+ stPoint);

		
		Map<String,Object> param = new HashMap<String,Object>();
		
		param.put("userName", userName);
		param.put("jobTitle", jobTitle);
		param.put("subject", subject);
		param.put("profNum", profNum);
		param.put("userId", userId);
		param.put("userPass", Util.getSHA256(userPass));
		param.put("telNum", telNum);
		param.put("email", email);
		param.put("addition", Integer.parseInt(addition));
		param.put("stPoint", stPoint);
		
		param.put("userLevel", 2);
		
		
		int result = userService.addProfessor(param);
		logger.debug("addProfessor result =>"+ result);
		if(result == -1){
			json.accumulate("status", "fail");
			json.accumulate("message", "이미 등록된 아이디가 있습니다.");
			return json.toString();
		}
		json.accumulate("status", "success");
		
		return json.toString();
	}
	
	@RequestMapping(value = "/updateProfessor", method = RequestMethod.POST)
	@ResponseBody
	public String updateProfessor(HttpServletRequest request,HttpSession session, Locale locale, Model model,@RequestBody String jsonString) {
		logger.debug("session.getId() =>"+ session.getId());
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("userLevel =>"+ userLevel);
		JSONObject json = new JSONObject();
		if(userLevel != 0){
			json.accumulate("status", "fail");
			json.accumulate("message", "권한 오류가 발생하였습니다.");
			return json.toString();
		}
		JSONUtil jsonUtil = new JSONUtil(jsonString);
		
		String userName = jsonUtil.getString("userName", null);
		String jobTitle = jsonUtil.getString("jobTitle", null);
		String subject = jsonUtil.getString("subject", null);
		String profNum = jsonUtil.getString("profNum", null);
//		String userId = jsonUtil.getString("userId", null);
//		String userPass = jsonUtil.getString("userPass", null);
		String telNum = jsonUtil.getString("telNum", null);
		String email = jsonUtil.getString("email", null);
		String addition = jsonUtil.getString("addition", null);
		String stPoint = jsonUtil.getString("stPoint", null);
		
		String userCode = jsonUtil.getString("userCode", null);
		
		logger.debug("userName :"+ userName);
		logger.debug("jobTitle :"+ jobTitle);
		logger.debug("subject :"+ subject);
		logger.debug("profNum :"+ profNum);
//		logger.debug("userId :"+ userId);
//		logger.debug("userPass :"+ userPass);
		logger.debug("telNum :"+ telNum);
		logger.debug("email :"+ email);
		logger.debug("addition :"+ addition);
		logger.debug("stPoint :"+ stPoint);
		logger.debug("userCode :"+ userCode);

		
		Map<String,Object> param = new HashMap<String,Object>();
		
		param.put("userName", userName);
		param.put("jobTitle", jobTitle);
		param.put("subject", subject);
		param.put("profNum", profNum);
//		param.put("userId", userId);
//		param.put("userPass", Util.getSHA256(userPass));
		param.put("telNum", telNum);
		param.put("email", email);
		param.put("addition", Integer.parseInt(addition));
		param.put("stPoint", stPoint);
		param.put("userCode", userCode);
		
		int result = userService.updateProfessor(param);
		logger.debug("updateProfessor result =>"+ result);
		
		if(result != 1){
			json.accumulate("status", "fail");
		}
		json.accumulate("status", "success");
		
		return json.toString();
	}
	
	
	@RequestMapping(value = "/userDataUpload", method = RequestMethod.POST)
	@ResponseBody
	public String userDataUpload(HttpServletRequest request, HttpSession session, Locale locale, Model model) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("/userDataUpload!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel);
		JSONObject json = new JSONObject();
		
		if(userLevel != 0){
			json.accumulate("status", "fail");
			json.accumulate("message", "권한 오류가 발생하였습니다.");
			return json.toString();
		}
		
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		MultipartFile uploadInputFile = multipartRequest.getFile("uploadInputFile");
		String uploadFileType = multipartRequest.getParameter("uploadFileType");
		logger.debug("uploadFileType =>"+ uploadFileType);
		if(uploadInputFile == null){
			json.accumulate("message", "업로드 중 오류가 발생하였습니다.");
			return json.toString();
		}

		String saveLocation =fileSystemResource.getPath()  + "/excel/" ;
		
		String saveResultFileName = FileUploader.uploadFile(saveLocation, uploadInputFile); 
		logger.debug("result:" + saveResultFileName);
		if(saveResultFileName != null && "1".equals(uploadFileType) ){
			ReadExcelUserAdder excel = new ReadExcelUserAdder(saveLocation + saveResultFileName);
			List<Map<String,Object>> getStaffList = excel.getStaffList();
			logger.debug("getStaffList size =>"+ getStaffList.size());
				
			int addStaffList = userService.addStaffList(getStaffList);
			if(addStaffList == -1){
				json.accumulate("status", "fail");
				json.accumulate("message", "이미 등록된 아이디가 있습니다.");
				return json.toString();
			}
			json.accumulate("count", addStaffList);
		} else if(saveResultFileName != null && "2".equals(uploadFileType) ){  //교수님
			ReadExcelUserAdder excel = new ReadExcelUserAdder(saveLocation + saveResultFileName);
			List<Map<String,Object>> professorList = excel.getProfessorList();
			logger.debug("professorList size =>"+ professorList.size());
			
			int addProfessorList = userService.addProfessorList(professorList);
			if(addProfessorList == -1){
				json.accumulate("status", "fail");
				json.accumulate("message", "이미 등록된 아이디가 있습니다.");
				return json.toString();
			}
			json.accumulate("count", addProfessorList);

		} else{
			json.accumulate("message", "엑셀파일에  오류가 발생하였습니다.\n 엑셀을 확인해 주세요.");
			return json.toString();
		}
		json.accumulate("uploadFileType", uploadFileType);
		json.accumulate("status", "success");
		return json.toString();
	}
	
}
