package com.nsdevil.knuprof.user.service;

import java.util.List;
import java.util.Map;

import com.nsdevil.knuprof.user.vo.UserInfo;

public interface UserService {
	public UserInfo getUserInfo(int userCode);
	public int updateUserInfo(UserInfo userInfo);
	public List<Map<String,Object>> getUserList(Map<String,Object> param);
	public int getUserListTotalCount(Map<String, Object> param);
	public int deleteUser(int userCode);
	public int initUserPassword(Map<String, Object> param);
	
	public int addStaff(Map<String,Object> param);
	public int updateStaff(Map<String,Object> param);
	public int addProfessor(Map<String,Object> param);
	public int updateProfessor(Map<String,Object> param);
	public List<Map<String,String>>  getStaffAuthInfo(int userCode);
	
	public int addStaffList(List<Map<String,Object>> staffList);
	public int addProfessorList(List<Map<String,Object>> professorList);
}
