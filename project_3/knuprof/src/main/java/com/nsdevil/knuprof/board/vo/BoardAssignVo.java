package com.nsdevil.knuprof.board.vo;

public class BoardAssignVo {

	private int boardGroupId;
	private int boardCode;
	private String boardType;
	private String regDate;
	private int prevBoardCode;
	private int nextBoardCode;
	public int getBoardGroupId() {
		return boardGroupId;
	}
	public void setBoardGroupId(int boardGroupId) {
		this.boardGroupId = boardGroupId;
	}
	public int getBoardCode() {
		return boardCode;
	}
	public void setBoardCode(int boardCode) {
		this.boardCode = boardCode;
	}
	public String getBoardType() {
		return boardType;
	}
	public void setBoardType(String boardType) {
		this.boardType = boardType;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public int getPrevBoardCode() {
		return prevBoardCode;
	}
	public void setPrevBoardCode(int prevBoardCode) {
		this.prevBoardCode = prevBoardCode;
	}
	public int getNextBoardCode() {
		return nextBoardCode;
	}
	public void setNextBoardCode(int nextBoardCode) {
		this.nextBoardCode = nextBoardCode;
	}
	
	
}
