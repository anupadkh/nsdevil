package com.nsdevil.knuprof.prof.domain;

import java.util.ArrayList;

public class ServiceVo {
	
	private int serviceCode;
	private String serviceName;
	private String serviceDetail1;
	private String serviceDetail2;
	private double point;
	private String unit;
	private String note;
	private String placeholder;
	private int dateType;
	private String selectOption;
	private String[] optionList;
	private int authLevel;
	private int serviceRowSpan;
	private int detailRowSpan;
	private int noteRowSpan;
	private boolean serviceHide; 
	private boolean detailHide;
	private boolean noteHide;
	private int detailColSpan;
	private int serviceType;
	private String startDate;
	private String endDate;
	private String categoryId;
	
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public int getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(int serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceDetail1() {
		return serviceDetail1;
	}
	
	public void setServiceDetail1(String serviceDetail1) {
		this.serviceDetail1 = serviceDetail1;
	}
	
	public String getServiceDetail2() {
		return serviceDetail2;
	}
	
	public void setServiceDetail2(String serviceDetail2) {
		this.serviceDetail2 = serviceDetail2;
	}
	
	public double getPoint() {
		return point;
	}
	
	public void setPoint(double point) {
		this.point = point;
	}
	
	public String getUnit() {
		return unit;
	}
	
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public String getNote() {
		return note;
	}
	
	public void setNote(String note) {
		this.note = note;
	}
	
	public String getPlaceholder() {
		return placeholder;
	}
	
	public void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
	}
	
	public int getDateType() {
		return dateType;
	}
	
	public void setDateType(int dateType) {
		this.dateType = dateType;
	}
	
	public String getSelectOption() {
		return selectOption;
	}
	
	public void setSelectOption(String selectOption) {
		this.selectOption = selectOption;
	}
	
	public String[] getOptionList() {
		return optionList;
	}
	
	public void setOptionList(String[] optionList) {
		this.optionList = optionList;
	}
	
	public int getAuthLevel() {
		return authLevel;
	}
	
	public void setAuthLevel(int authLevel) {
		this.authLevel = authLevel;
	}
	
	public int getServiceRowSpan() {
		return serviceRowSpan;
	}
	
	public void setServiceRowSpan(int serviceRowSpan) {
		this.serviceRowSpan = serviceRowSpan;
	}
	
	public int getDetailRowSpan() {
		return detailRowSpan;
	}
	
	public void setDetailRowSpan(int detailRowSpan) {
		this.detailRowSpan = detailRowSpan;
	}
	
	public int getNoteRowSpan() {
		return noteRowSpan;
	}
	
	public void setNoteRowSpan(int noteRowSpan) {
		this.noteRowSpan = noteRowSpan;
	}
	
	public boolean isServiceHide() {
		return serviceHide;
	}
	
	public void setServiceHide(boolean serviceHide) {
		this.serviceHide = serviceHide;
	}
	
	public boolean isDetailHide() {
		return detailHide;
	}
	
	public void setDetailHide(boolean detailHide) {
		this.detailHide = detailHide;
	}
	
	public boolean isNoteHide() {
		return noteHide;
	}
	
	public void setNoteHide(boolean noteHide) {
		this.noteHide = noteHide;
	}
	
	public int getDetailColSpan() {
		return detailColSpan;
	}
	
	public void setDetailColSpan(int detailColSpan) {
		this.detailColSpan = detailColSpan;
	}
	
	public int getServiceType() {
		return serviceType;
	}
	
	public void setServiceType(int serviceType) {
		this.serviceType = serviceType;
	}
	
	public String getStartDate() {
		return startDate;
	}
	
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
	public String getEndDate() {
		return endDate;
	}
	
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}