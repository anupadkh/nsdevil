package com.nsdevil.knuprof.board;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nsdevil.knuprof.board.service.BoardService;
import com.nsdevil.knuprof.util.PageSplitter;

import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = "/board")
public class BoardController {
	private final Logger logger = LoggerFactory.getLogger(BoardController.class);
	@Autowired
	private BoardService boardService; 
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String list(HttpServletRequest request, HttpSession session, Locale locale, Model model) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		int userCode = (Integer) session.getAttribute("userCode");
		logger.debug("/board/!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel  + "  "+ userCode);

		Map<String,Object> param = new HashMap<String,Object>();
		if(userLevel > 1){
			param.put("userCode",userCode);	
		}
		
		int getBoardListTotalCount = boardService.getBoardListTotalCount(param);
		logger.debug("getBoardListTotalCount =>"+ getBoardListTotalCount);
		JSONObject json = new JSONObject();
		
		if(getBoardListTotalCount > 0){
			int pageNo = 1;
			int pagePerRowCount = PageSplitter.PAGE_PER_ROW_COUNT;
			int startRowCount = (pageNo -1) * pagePerRowCount;
			
			json = getBoardPageInfo(param, getBoardListTotalCount, pageNo, pagePerRowCount,startRowCount );
			
			model.addAttribute("json", json);
		}
		
		if(userLevel < 2){
			return "board/list_admin";
		}else if(userLevel == 2 ){
			return "board/list_pro";
		}
		return "login";
	}
	
	@RequestMapping(value = "/getBoardList", method = RequestMethod.POST)
	@ResponseBody
	public String getBoardList(HttpServletRequest request, HttpSession session, Locale locale, Model model,@RequestParam HashMap<String, Object> param) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		int userCode = (Integer) session.getAttribute("userCode");
		logger.debug("/getBoardList!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel  + "  "+ userCode);
		JSONObject json = new JSONObject();
		
		logger.debug("optionString =>"+ param.get("optionString"));
		logger.debug("pageInfo =>"+  param.get("pageNo") + " / " + param.get("rowCount"));
		if(userLevel > 1){
			param.put("userCode",userCode);	
		}
		int getBoardListTotalCount = boardService.getBoardListTotalCount(param);
		logger.debug("getBoardListTotalCount =>"+ getBoardListTotalCount);

		if(getBoardListTotalCount > 0){
			int pageNo = Integer.parseInt((String) param.get("pageNo"));
			int rowCount = Integer.parseInt((String) param.get("rowCount"));
			
			int pagePerRowCount = rowCount > 0 ? rowCount : PageSplitter.PAGE_PER_ROW_COUNT;
			int startRowCount = (pageNo -1) * pagePerRowCount;
			
			json.accumulate("status", "success");
			json = getBoardPageInfo(param, getBoardListTotalCount,pageNo, pagePerRowCount,startRowCount);
			
			return json.toString();
		}else{
			json.accumulate("status", "fail");
			json.accumulate("message", "조회결과가 없습니다");
		}
		return json.toString();
	}
	
	private JSONObject getBoardPageInfo(Map<String,Object> param, int totalCount, int pageNo, int pagePerRowCount, int startRowCount){
		JSONObject json = new JSONObject();
		logger.debug("pagePerRowCount =>"+ pagePerRowCount);
		logger.debug("startRowCount =>"+ startRowCount);
		param.put("startRowCount", (startRowCount));
		param.put("pagePerRowCount", (pagePerRowCount));
		List<Map<String,Object>> getBoardList = boardService.getBoardList(param);
		logger.debug("getBoardList =>"+ getBoardList.size());
		
		PageSplitter pageSplitter = new PageSplitter(getBoardList, totalCount, pageNo, pagePerRowCount);
		json.accumulate("boardList", pageSplitter.getNumberingResultList());
		json.accumulate("pagingInfo", pageSplitter.getPageNavigationInfo());
		return json;
	}
	
	@RequestMapping(value = "/detail", method = RequestMethod.POST)
	public String write(HttpServletRequest request, HttpSession session, Locale locale, Model model) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("/board/detail!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel);
		int userCode = (Integer) session.getAttribute("userCode");
		
		String boardType = request.getParameter("boardType");
		String boardCode = request.getParameter("boardCode");
		
		model.addAttribute("boardType", boardType);
		model.addAttribute("boardCode", boardCode);
		
		if(userLevel < 2){
			return "board/detail_pro";
		}else if(userLevel == 2 ){
			return "board/detail_pro";
		}
		model.addAttribute("json",null);
		return "login";
	}
	
	@RequestMapping(value = "/getDetailInfo", method = RequestMethod.POST)
	@ResponseBody
	public String getDetailInfo(HttpServletRequest request, HttpSession session, Locale locale, Model model,@RequestParam HashMap<String,Object> param) {
		JSONObject json = new JSONObject();
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("/board/getDetailInfo!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel);
		int userCode = (Integer) session.getAttribute("userCode");
		logger.debug("boardType =>"+ param.get("boardType") + "  boardCode=>"+ param.get("boardCode") );
		String boardType = (String) param.get("boardType");
		String boardCode = (String) param.get("boardCode") ;
		
		logger.debug(" type=>"+ boardType + "  /  code=>"+ boardCode);
		if(boardType != null && boardCode != null){
			Map<String,Object> getBoardDetailInfo = boardService.getBoardDetailInfo(userCode,boardType,Integer.parseInt(boardCode));
			json.accumulateAll(getBoardDetailInfo);
			json.accumulate("status", "success");
		}else{
			json.accumulate("status", "fail");
		}		
		return json.toString();
	}
	@RequestMapping(value = "/addBoardQna", method = RequestMethod.POST)
	@ResponseBody
	public String addBoardQna(HttpServletRequest request, HttpSession session, Locale locale, Model model,@RequestParam HashMap<String,Object> param) {
		JSONObject json = new JSONObject();
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("/board/addBoardQna!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel);
		int userCode = (Integer) session.getAttribute("userCode");
		logger.debug("title =>"+ param.get("title") + "  contents=>"+ param.get("contents") + " groupId =>"+  param.get("groupId"));
		String contents = (String) param.get("contents");
		logger.debug("param.userCode =>"+ param.get("userCode"));
		//param.put("userCode", userCode);
		if(userCode > 0 && contents != null){
			int addBoardQna = boardService.addBoardQna(param);
			logger.debug("addBoardQna =>"+ addBoardQna);
			json.accumulate("result", addBoardQna);
			json.accumulate("status", "success");
		}else{
			json.accumulate("status", "fail");
		}		
		return json.toString();
	}
	@RequestMapping(value = "/updateBoardQna", method = RequestMethod.POST)
	@ResponseBody
	public String updateBoardQna(HttpServletRequest request, HttpSession session, Locale locale, Model model,@RequestParam HashMap<String,Object> param) {
		JSONObject json = new JSONObject();
		logger.debug("/board/updateBoardQna!!!!!!!!!!!!!!!!!!!!!!!! ");
		int userCode = (Integer) session.getAttribute("userCode");
		String boardCode = (String) param.get("boardCode");
		String groupId  =(String) param.get("groupId");
		param.put("userCode", userCode);
		if(userCode > 0 && (boardCode != null || groupId != null)){
			int updateBoardQna = boardService.updateBoardQna(param);
			logger.debug("updateBoardQna =>"+ updateBoardQna);
			json.accumulate("result", updateBoardQna);
			json.accumulate("status", "success");
		}else{
			json.accumulate("status", "fail");
		}		
		return json.toString();
	}
	
	@RequestMapping(value = "/addNotice", method = RequestMethod.POST)
	@ResponseBody
	public String addNotice(HttpServletRequest request, HttpSession session, Locale locale, Model model,@RequestParam HashMap<String, Object> param) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("/addNotice!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel);
		JSONObject json = new JSONObject();
		if(userLevel > 1){
			json.accumulate("status", "fail");
			json.accumulate("message", "권한 오류가 발생하였습니다.");
			return json.toString();
		}
		logger.debug("userCode =>"+ param.get("userCode"));
		int addNoticeData = boardService.addNoticeData(param);
		logger.debug("addNoticeData =>"+ addNoticeData);
		if(addNoticeData > 0){
			json.accumulate("status", "success");
			json.accumulate("result", addNoticeData);
			return json.toString();
		}
		json.accumulate("message", "오류가 발생하였습니다.");
		return json.toString();
	}
	@RequestMapping(value = "/updateNotice", method = RequestMethod.POST)
	@ResponseBody
	public String updateNotice(HttpServletRequest request, HttpSession session, Locale locale, Model model,@RequestParam HashMap<String,Object> param) {
		JSONObject json = new JSONObject();
		logger.debug("/board/updateNotice!!!!!!!!!!!!!!!!!!!!!!!! ");
		int userCode = (Integer) session.getAttribute("userCode");
		String boardCode = (String) param.get("boardCode");
		logger.debug("boardCode =>"+ boardCode + "  useFlag =>"+ param.get("useFlag"));
		param.put("userCode", userCode);
		if(userCode > 0 && boardCode != null){
			int updateNotice = boardService.updateNotice(param);
			logger.debug("updateNotice =>"+ updateNotice);
			json.accumulate("result", updateNotice);
			json.accumulate("status", "success");
		}else{
			json.accumulate("status", "fail");
		}		
		return json.toString();
	}
	
}
