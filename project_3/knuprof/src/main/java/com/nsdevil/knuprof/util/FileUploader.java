package com.nsdevil.knuprof.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

public class FileUploader {
	
	public static String uploadFile(String uploadDir, MultipartFile file) {
		
		String fileName = null;
		String realName = null;
		
		//파일이 있으면 업로드
		if (file != null && !file.isEmpty()) {
			
			//업로드 폴더 생성
			String[] dirList = uploadDir.split("/");
			
			String filePath = "";
			
			for (String dir : dirList) {
				filePath += dir + "/";
				File fileDir = new File(filePath);
				if (!fileDir.exists()) {
					fileDir.mkdirs();
				}
			}
			
			fileName = file.getOriginalFilename();
			
			String fileExt = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
			
    		realName = Util.getUploadFileName(uploadDir, file.getOriginalFilename());
    		
			//upload 가능한 파일 타입 지정
	        if(fileExt.equalsIgnoreCase("jpg") ||
	        		fileExt.equalsIgnoreCase("jpeg") ||
	        		fileExt.equalsIgnoreCase("gif") ||
	        		fileExt.equalsIgnoreCase("png") ||
	        		fileExt.equalsIgnoreCase("bmp") ||
	        		fileExt.equalsIgnoreCase("mp3") ||
	        		fileExt.equalsIgnoreCase("mp4") ||
	        		fileExt.equalsIgnoreCase("xml") ||
	        		fileExt.equalsIgnoreCase("xls") ||
	        		fileExt.equalsIgnoreCase("xlsx") ||
	        		fileExt.equalsIgnoreCase("txt") ||
	        		fileExt.equalsIgnoreCase("hwp") ||
	        		fileExt.equalsIgnoreCase("zip") ||
	        		fileExt.equalsIgnoreCase("pdf") ||
	        		fileExt.equalsIgnoreCase("doc") ||
	        		fileExt.equalsIgnoreCase("docx") ||
	        		fileExt.equalsIgnoreCase("ppt") ||
	        		fileExt.equalsIgnoreCase("pptx")) {
	        	
	            try {
	            	byte[] bytes = file.getBytes();
	            	
					File outFile = new File(uploadDir + "/" + realName);
					
					FileOutputStream fileOutputStream = new FileOutputStream(outFile);
					fileOutputStream.write(bytes);
					fileOutputStream.close();
	            } catch(IOException e) {
	            	
	            }
	        } else {
	        	realName = "not";
	        	
	        }
		}
		return realName;
	}

	public static String uploadFile2(String uploadDir, MultipartFile file, String jj_code, String km_code) {
		
		String fileName = null;
		String realName = null;
		String bornName = null;
		
		//파일이 있으면 업로드
		if (file != null && !file.isEmpty()) {
			
			//업로드 폴더 생성
			String[] dirList = uploadDir.split("/");
			
			String filePath = "";
			
			for (String dir : dirList) {
				filePath += dir + "/";
				File fileDir = new File(filePath);
				if (!fileDir.exists()) {
					fileDir.mkdirs();
				}
			}
			
			fileName = file.getOriginalFilename();
			
			String fileExt = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
			String targetName = fileName.substring(0, fileName.lastIndexOf("."));
			
    		realName = Util.getUploadFileName(uploadDir, file.getOriginalFilename());
    		bornName = jj_code + "_" + km_code + "_" + realName;
    		
			//upload 가능한 파일 타입 지정
	        if(fileExt.equalsIgnoreCase("jpg") ||
	        		fileExt.equalsIgnoreCase("jpeg") ||
	        		fileExt.equalsIgnoreCase("gif") ||
	        		fileExt.equalsIgnoreCase("png") ||
	        		fileExt.equalsIgnoreCase("bmp") ||
	        		fileExt.equalsIgnoreCase("mp3") ||
	        		fileExt.equalsIgnoreCase("mp4") ||
	        		fileExt.equalsIgnoreCase("xml") ||
	        		fileExt.equalsIgnoreCase("xls") ||
	        		fileExt.equalsIgnoreCase("xlsx") ||
	        		fileExt.equalsIgnoreCase("txt") ||
	        		fileExt.equalsIgnoreCase("hwp") ||
	        		fileExt.equalsIgnoreCase("zip")) {
	        	
	            try {
	            	byte[] bytes = file.getBytes();
	            	
					File outFile = new File(uploadDir + "/" + targetName + "_" + jj_code + "_" + km_code + "_" + realName);
					
					FileOutputStream fileOutputStream = new FileOutputStream(outFile);
					fileOutputStream.write(bytes);
					fileOutputStream.close();
	            } catch(IOException e) {
	            	
	            }
	        } else {
	        	bornName = "not";
	        	
	        }
		}
		return bornName;
	}
}
