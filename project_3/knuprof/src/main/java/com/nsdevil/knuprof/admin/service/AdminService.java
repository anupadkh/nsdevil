package com.nsdevil.knuprof.admin.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nsdevil.knuprof.admin.excel.ExcelAchieveEducationVo;
import com.nsdevil.knuprof.admin.excel.ExcelAchieveServiceVo;
import com.nsdevil.knuprof.admin.excel.ExcelAchieveResearchVo;

public interface AdminService {
	public List<Map<String,Object>> getProfessorList(Map<String,Object> param);
	public List<Map<String,Object>> getCategoryData(Map<String,Object> param);
	public Map<String,Object> getCountAchieveRegistUser(Map<String,Object> param); 
	public List<Map<String,Object>> getAchieveRegistUserList(Map<String,Object> param);
	
	public int addAchieveRegistService(List<Map<String,ExcelAchieveServiceVo>> paramList);
	
	public List<Map<String, Object>> getAchieveProfessorResult(Map<String, Object> param);
	public List<Map<String, Object>> getAchievePointInfo(Map<String, Object> param);
	
	public int upsertAchieveRegistEducation(List<Map<String,ExcelAchieveEducationVo>> paramList);
	public int upsertAchieveRegistEducation(ArrayList<ExcelAchieveEducationVo> paramList);
	public int upsertAchieveRegistEducation(ExcelAchieveEducationVo param);
	
	int upsertAchieveRegistResearch(List<Map<String, ExcelAchieveResearchVo>> paramList);
	public int upsertAchieveRegistResearch(String requestUserCode,ArrayList<ExcelAchieveResearchVo> paramList);
	
	public List<Map<String,Object>> getAchieveResearchFileList(Map<String,Object> map);
	public HashMap<String, Object> achieveResearchUploadFile(HashMap<String, Object> param);
	
	public int deleteFile(Map<String,Object> param);
	public List<Map<String,Object>> getFileCountByResearchCode(Map<String,Object> map);
	
	public int getPeriodListTotalCount(Map<String, Object> param);
	public List<Map<String,Object>> getPeriodList(Map<String, Object> param);
	public Map<String, Object> addPeriod(Map<String, Object> param);
	public int updatePeriod(Map<String, Object> param);
	
	public List<Map<String,Object>> getAchieveGradeScoreInfo(Map<String, Object> param);
	
	public int upsertAchievePeriodCriterion(List<HashMap<String, Object>> param);
	public List<Map<String,Object>> getPeriodCriterion(Map<String, Object> param);
	
	public int upsertAchievePeriodGrade(Map<String,Object> map);
	public Map<String,Object> getPeriodGrade(Map<String,Object> map);
	
	public int upsertAchievePeriodMinPoint(Map<String,Object> map);
	
	public  List<String>getPeriodYearInfo();
	
	public int updateGradeScoreInfo( List<Map<String,Object>> list, String achieveType);
	public int serviceConfirm(HashMap<String, Object> param);
	public int serviceUpdate(HashMap<String, Object> param);
}
