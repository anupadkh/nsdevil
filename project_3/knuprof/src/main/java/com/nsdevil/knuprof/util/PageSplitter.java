package com.nsdevil.knuprof.util;


import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 페이징 클래스
 * @author limchul
 *
 */
public class PageSplitter {
	private final Log log = LogFactory.getLog(PageSplitter.class);
	
	public static final int PAGE_PER_ROW_COUNT = 10;
	public static final int GROUP_PER_PAGE_COUNT = 10;
	
	private List<Map<String,Object>> resultList = null;
	private int totalRowCount = 0;
	private int pageNo = 0;
	private int pagePerRowCount = 0;
	public PageSplitter(List<Map<String,Object>> resultList, int totalRowCount, int pageNo, int pagePerRowCount){
		this.resultList = resultList;
		this.totalRowCount = totalRowCount;
		this.pageNo = pageNo;
		this.pagePerRowCount = pagePerRowCount;
	}
	public PageSplitter(ArrayList<?> resultList, int totalRowCount, int pageNo, int pagePerRowCount){
		this.resultList = objectListToMap(resultList);
		this.totalRowCount = totalRowCount;
		this.pageNo = pageNo;
		this.pagePerRowCount = pagePerRowCount;
	}
	
	public List<Map<String,Object>> getNumberingResultList(){
		
		log.debug("totalRowCount    =>"+ totalRowCount);
		log.debug("resultList size  =>"+ resultList.size());
		List<Map<String,Object>> newMapList = new ArrayList<Map<String,Object>>();
		
		pagePerRowCount = pagePerRowCount == 0 ? PAGE_PER_ROW_COUNT : pagePerRowCount;
		int base = (pageNo-1) * pagePerRowCount;
		int until = base + pagePerRowCount;
		log.debug("base :"+ base + "  until:"+ until);
		log.debug("===============");
		
		int seq=0;
		for(Map<String,Object> resultMap : resultList ){
			int no = totalRowCount - (base+ seq);
			//log.debug("resultMap.getIndex :"+ resultMap.get("user_index")  + "   no:" +(no));
			seq++;
			resultMap.put("no", no);
			newMapList.add(resultMap);
		}
		return newMapList;
		
	}
	public Map<String,String> getPageNavigationInfo(){
		int totalPageCount = totalRowCount / pagePerRowCount + (totalRowCount % pagePerRowCount > 0 ? 1 : 0);
		if(pageNo > totalPageCount){
			pageNo = totalPageCount;
		}
		int group_no = pageNo / GROUP_PER_PAGE_COUNT + ( pageNo % GROUP_PER_PAGE_COUNT>0 ? 1 : 0);
		int endNo = group_no * GROUP_PER_PAGE_COUNT;		
		int startNo = endNo - (GROUP_PER_PAGE_COUNT - 1);
		
		if(endNo > totalPageCount){
			endNo = totalPageCount;
		}
		int prevPageNo = startNo - GROUP_PER_PAGE_COUNT; 
		int nextPageNo = startNo + GROUP_PER_PAGE_COUNT;
		
		if(prevPageNo < 1){
			prevPageNo = 1;
		}
		
		if(nextPageNo > totalPageCount){	
			nextPageNo = totalPageCount / GROUP_PER_PAGE_COUNT * GROUP_PER_PAGE_COUNT + 1;
		}
		
		log.debug("pageNo : "+ pageNo);
		log.debug("totalPageCount :"+ totalPageCount);
		log.debug("startNo : "+ startNo);
		log.debug("endNo   : "+ endNo);
		log.debug("prevPageNo :"+ prevPageNo);
		log.debug("nextPageNo :"+ nextPageNo);
		
		Map<String,String> getPageNavigationInfo = new HashMap<String, String>();
		getPageNavigationInfo.put("currentPageNo", String.valueOf(pageNo));
		getPageNavigationInfo.put("totalPageCount", String.valueOf(totalPageCount));
		getPageNavigationInfo.put("startNo", String.valueOf(startNo));
		getPageNavigationInfo.put("endNo", String.valueOf(endNo));
		getPageNavigationInfo.put("prevPageNo", String.valueOf(prevPageNo));
		getPageNavigationInfo.put("nextPageNo", String.valueOf(nextPageNo));
		getPageNavigationInfo.put("groupPerPageCount", String.valueOf(GROUP_PER_PAGE_COUNT));
		return getPageNavigationInfo;
	}
	
	public List<Map<String,Object>> objectListToMap(List list){
		List<Map<String,Object>> listMap = new ArrayList<Map<String,Object>>();
		for(int i=0; i < list.size(); i++){
			listMap.add(objectToMap(list.get(i)));
		}
		return listMap;
	}
	public Map<String,Object> objectToMap(Object obj){
		try {
			Field[] fields = obj.getClass().getDeclaredFields();
			Map <String,Object>resultMap = new HashMap<String,Object>();
			for(int i=0; i<=fields.length-1;i++){
				fields[i].setAccessible(true);
				resultMap.put((String) fields[i].getName(), (Object) fields[i].get(obj));
			}
			return resultMap;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

}
