package com.nsdevil.knuprof.user.dao;

import java.util.List;
import java.util.Map;

import com.nsdevil.knuprof.user.vo.UserInfo;

public interface UserDao {
	public UserInfo getUserInfo(int userCode);
	public int updateUserInfo(UserInfo userInfo);
	public List<Map<String,Object>> getUserList(Map<String, Object> param);
	public int getUserListTotalCount(Map<String, Object> param);
	public int deleteUser(int userCode);
	public int initUserPassword(Map<String, Object> param);
	
	public Map<String,Object> addUser(Map<String,Object> param);
	public Map<String,Object> addStaffAuth(Map<String,Object> param);
	public int updateStaffUserInfo(Map<String,Object> param);
	public int deleteStaffAuth(int userCode);
	public List<Map<String,String>> getStaffAuthInfo(int userCode);
	
	public UserInfo getUserInfoById(String userId);
	
	public List<Map<String,String>> getCategoryAuthInfo();
	
	public int addProfessor(Map<String,Object> param);
	public int updateProfessor(Map<String,Object> param);
}
