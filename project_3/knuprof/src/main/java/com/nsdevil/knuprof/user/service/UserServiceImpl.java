package com.nsdevil.knuprof.user.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nsdevil.knuprof.user.UserController;
import com.nsdevil.knuprof.user.dao.UserDao;
import com.nsdevil.knuprof.user.vo.UserInfo;

@Service
public class UserServiceImpl implements UserService {
	private final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	@Autowired
	private UserDao dao;
	

	@Override
	public UserInfo getUserInfo(int userCode) {
		return dao.getUserInfo(userCode);
	}


	@Override
	public int updateUserInfo(UserInfo userInfo) {
		return dao.updateUserInfo(userInfo);
	}


	@Override
	public List<Map<String,Object>> getUserList(Map<String, Object> param) {
		return dao.getUserList(param);
	}


	@Override
	public int getUserListTotalCount(Map<String, Object> param) {
		return dao.getUserListTotalCount(param);
	}


	@Override
	public int deleteUser(int userCode) {
		return dao.deleteUser(userCode);
	}
	@Override
	public int initUserPassword(Map<String, Object> param) {
		return dao.initUserPassword(param);
	}



	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public int addStaff(Map<String, Object> param) {
		UserInfo existUser = dao.getUserInfoById((String) param.get("userId"));
		if(existUser != null){
			return -1;
		}
		Map<String,Object> addUser = dao.addUser(param);
		logger.debug(" addUser ==>"+ addUser);
		if(addUser != null){
			logger.debug("userCode =>"+ addUser.get("user_code"));
			if(addUser.get("user_code") != null){
				param.put("userCode", addUser.get("user_code"));
				for(String categoryId : (List<String>) param.get("categoryIds")){
					param.put("categoryId", categoryId);
					Map<String,Object> result = dao.addStaffAuth(param);
					logger.debug("result =>"+ result);
				}
			}
			
		}
		return 0;
	}
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public int updateStaff(Map<String, Object> param) {

		int updateStaffUserInfo = dao.updateStaffUserInfo(param);
		
		int userCode = Integer.parseInt((String) param.get("userCode"));
		int deleteResult = dao.deleteStaffAuth(userCode);
		logger.debug("deleteResult =>"+ deleteResult);
		for(String categoryId : (List<String>) param.get("categoryIds")){
			param.put("categoryId", categoryId);
			Map<String,Object> result = dao.addStaffAuth(param);
			logger.debug("result =>"+ result);
		}
		return 0;			
	}
	
	public List<Map<String,String>>  getStaffAuthInfo(int userCode){
		return dao.getStaffAuthInfo(userCode);
	}


	@Override
	@Transactional
	public int addProfessor(Map<String, Object> param) {
		UserInfo existUser = dao.getUserInfoById((String) param.get("userId"));
		if(existUser != null){
			return -1;
		}
		return dao.addProfessor(param);
	}


	@Override
	public int updateProfessor(Map<String, Object> param) {
		return dao.updateProfessor(param);
	}
	
	@Override
	@Transactional
	public int addStaffList(List<Map<String,Object>> staffList){
		int resultCount = 0;
		for(int i=0; i < staffList.size(); i++){
			Map<String,Object> paramMap = staffList.get(i);
			UserInfo existUser = dao.getUserInfoById((String) paramMap.get("userId"));
			if(existUser != null){
				return -1;
			}
			Map<String,Object> addUser = dao.addUser(paramMap);
			logger.debug(" addUser ==>"+ addUser);
			if(addUser != null){
				logger.debug("userCode =>"+ addUser.get("user_code"));
				if(addUser.get("user_code") != null){
					addUser.put("userCode", addUser.get("user_code"));
					for(String categoryId : (List<String>) paramMap.get("categoryIds")){
						addUser.put("categoryId", categoryId);
						Map<String,Object> result = dao.addStaffAuth(addUser);
						logger.debug("result =>"+ result.get("staff_auth_code"));
					}
				}
			}
			resultCount++;
		} 
		return resultCount;
	}
	
	@Override
	@Transactional
	public int addProfessorList(List<Map<String,Object>> professorList){
		int resultCount = 0;
		for(int i=0; i < professorList.size(); i++){
			Map<String,Object> paramMap = professorList.get(i);
			UserInfo existUser = dao.getUserInfoById((String) paramMap.get("userId"));
			if(existUser != null){
				return -1;
			}
			logger.debug("result =>" + dao.addProfessor(paramMap));
			resultCount++;
		}
		return resultCount;
	}


	
	
}
