package com.nsdevil.knuprof.admin.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nsdevil.knuprof.admin.dao.AdminDao;
import com.nsdevil.knuprof.prof.dao.ProfessorDao;
import com.nsdevil.knuprof.prof.domain.ServiceResultVo;
import com.nsdevil.knuprof.user.dao.UserDao;
import com.nsdevil.knuprof.user.vo.UserInfo;
import com.nsdevil.knuprof.admin.excel.ExcelAchieveEducationVo;
import com.nsdevil.knuprof.admin.excel.ExcelAchieveServiceAdder;
import com.nsdevil.knuprof.admin.excel.ExcelAchieveServiceVo;
import com.nsdevil.knuprof.admin.excel.ExcelAchieveResearchVo;

@Service
public class AdminServiceImpl implements AdminService {
	private final Logger logger = LoggerFactory.getLogger(AdminServiceImpl.class);
	@Autowired
	private AdminDao adminDao;

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private ProfessorDao professorDao;
	
	


	public List<Map<String,Object>> getProfessorList(Map<String,Object> param){
		return adminDao.getProfessorList(param);
	}
	
	
	
	public List<Map<String,Object>> getCategoryData(Map<String,Object> param){
		int userCode = (Integer) param.get("userCode");
		String category = (String) param.get("category");
		logger.debug("userCode =>"+ userCode + " category =>"+category);
		List<Map<String,String>> authInfo = userDao.getStaffAuthInfo(userCode);
		logger.debug("authInfo =>"+ authInfo);
		ArrayList<String> categoryIds = new ArrayList<String>();
		for(int i=0; i<authInfo.size(); i++){
			String categoryCode = authInfo.get(i).get("categorycode");
			logger.debug("categoryCode =>"+ categoryCode);
			if(categoryCode.startsWith(category)){
				categoryIds.add(authInfo.get(i).get("categoryid"));
			}
		}
		logger.debug("categoryIds =>"+ categoryIds);
		if(categoryIds != null){
			List<Map<String,Object>> htmlData = null;
			param.put("categoryIds", categoryIds);
			if(category.startsWith("s")){
				htmlData = adminDao.getServiceHtmlData(param);
			}
			return htmlData;
		}
		return null;
	}
	
	
	private void getServiceCode(String... categoryIds){
		List <Integer> categoryIdList = new ArrayList<Integer>();
		for(String categoryId : categoryIds){
			if(categoryId == "si01"){
				categoryIdList.add(1);
			}
		}
		
	}



	@Override
	public Map<String, Object> getCountAchieveRegistUser(Map<String,Object> param) {
		return adminDao.getCountAchieveRegistUser(param);
	}
	
	@Override
	public List<Map<String,Object>> getAchieveRegistUserList(Map<String,Object> param){
		List<Map<String,Object>> userList = adminDao.getAchieveRegistUserList(param);
		ArrayList<Map<String,Object>> userDataArray = new ArrayList<Map<String,Object>>();
		if(userList != null){
			for(int i=0; i < userList.size() ; i++){
				HashMap<String,Object> tmp = (HashMap<String, Object>) userList.get(i);
				//logger.debug("user.get(userCode) =>"+ tmp.get("usercode") + " / "+ tmp.get("username"));
				tmp.put("userCode", tmp.get("usercode"));
				ArrayList<ServiceResultVo> serviceResult  = getServiceResult(tmp);
				tmp.put("serviceResult", serviceResult);
				userDataArray.add(tmp);
			}
		}
		return userDataArray;
	}
	
	private ArrayList<ServiceResultVo> getServiceResult(HashMap<String, Object> param) {
		HashMap<String, Object> result = null;	
		ArrayList<ServiceResultVo> serviceResult1 = professorDao.getServiceResult(param);
    	if (serviceResult1 != null) {
    		setServiceResultData(serviceResult1);
		}
		return serviceResult1;
	}
	
	private void setServiceResultData(ArrayList<ServiceResultVo> serviceResultList) {
		for (ServiceResultVo serviceResultVo : serviceResultList) {
			
			if (serviceResultVo.getServiceCode() != 0 && serviceResultVo.getContents() != null && serviceResultVo.getContents().trim().length() > 0) {
				
				if (serviceResultVo.getStartDate() == null) {
					serviceResultVo.setStartDate("");
				}
				if (serviceResultVo.getEndDate() == null) {
					serviceResultVo.setEndDate("");
				}
			
				serviceResultVo.setContentsText(serviceResultVo.getServiceCode() + "|" + serviceResultVo.getServiceType() + "|" + serviceResultVo.getStartDate() + "|" + serviceResultVo.getEndDate() + "|" + serviceResultVo.getContents());
				
				String contentsViewText = "";
				
				if (serviceResultVo.getStartDate() != null && serviceResultVo.getStartDate().trim().length() > 0) {
					
					if (serviceResultVo.getDateType() == 4) {
						contentsViewText += serviceResultVo.getStartDate().substring(2).replaceAll("-", ".") + " " + serviceResultVo.getStartTime() + " ~ " + serviceResultVo.getEndTime();
					} else {
						contentsViewText += serviceResultVo.getStartDate().substring(2).replaceAll("-", ".");
						if (serviceResultVo.getEndDate() != null && serviceResultVo.getEndDate().trim().length() > 0) {
							contentsViewText +=  " ~ " + serviceResultVo.getEndDate().substring(2).replaceAll("-", ".");
						}
					}
				}
				
				if (serviceResultVo.getContents() != null && serviceResultVo.getContents().trim().length() > 0) {
					if (contentsViewText.length() > 0) {
						contentsViewText += "<br>";
					}
					contentsViewText += serviceResultVo.getContents();
				}
				
				if (contentsViewText.length() > 0) {
					serviceResultVo.setContentsViewText(contentsViewText);
				}
			}
		}
	}



	@Override
	@Transactional
	public int addAchieveRegistService(List<Map<String, ExcelAchieveServiceVo>> paramList) {
		for(int i=0; i < paramList.size(); i++){
			Map<String,ExcelAchieveServiceVo> paramMap = paramList.get(i);
			Iterator<String> keys = paramMap.keySet().iterator();
			while( keys.hasNext() ){
				String key = keys.next();
				ExcelAchieveServiceVo vo = paramMap.get(key);
				logger.debug(i + "[" + key + "] =>" +  vo.getServiceCode() + " / " +  vo.getUserId() + " / "+ vo.getUserName() + " / " + vo.getStartDate() + " / " + vo.getEndDate() + " / " + vo.getContents() );
				UserInfo userInfo = userDao.getUserInfoById(vo.getUserId());
				if(userInfo != null){
					logger.debug("userInfo.getUserCode(); "+ userInfo.getUserCode());
					vo.setUserCode(String.valueOf(userInfo.getUserCode()));
				}
			}			
		}
		
		int count = 0;
		for(int i=0; i < paramList.size(); i++){
			Map<String,ExcelAchieveServiceVo> paramMap = paramList.get(i);
			Iterator<String> keys = paramMap.keySet().iterator();
			while( keys.hasNext() ){
				String key = keys.next();
				ExcelAchieveServiceVo vo = paramMap.get(key);
				logger.debug(i + "[" + key + "] =>" +  vo.getServiceCode() + " / " + vo.getUserCode() + " / " +  vo.getUserId() + " / "+ vo.getUserName() + " / " + vo.getStartDate() + " / " + vo.getEndDate() + " / " + vo.getContents() );
				Map<String,Object>  result = adminDao.addAchieveRegistService(vo);
				count++;
				if(result != null){				
					logger.debug("resultCode =>"+ result.get("service_result_code"));
				}
			}
		}
		return count;
	}



	@Override
	public List<Map<String, Object>> getAchieveProfessorResult(Map<String, Object> param) {
		return adminDao.getAchieveProfessorResult(param);
	}



	@Override
	public List<Map<String, Object>> getAchievePointInfo(Map<String, Object> param) {
		return adminDao.getAchievePointInfo(param);
	}

	@Override
	@Transactional
	public int upsertAchieveRegistEducation(List<Map<String, ExcelAchieveEducationVo>> paramList) {
		for(int i=0; i < paramList.size(); i++){
			Map<String,ExcelAchieveEducationVo> paramMap = paramList.get(i);
			Iterator<String> keys = paramMap.keySet().iterator();
			while( keys.hasNext() ){
				String key = keys.next();
				ExcelAchieveEducationVo vo = paramMap.get(key);
				//logger.debug(i + " ?? [" + key + "] =>" +  vo.getEducationCode() + " / " +  vo.getUserId() + " / "+ vo.getUserName() + " / " + vo.getUnit() + " / " + vo.getContents() );
				UserInfo userInfo = userDao.getUserInfoById(vo.getUserId());
				if(userInfo != null){
					//logger.debug("userInfo.getUserCode(); "+ userInfo.getUserCode());
					vo.setUserCode(String.valueOf(userInfo.getUserCode()));
				}
			}			
		}
		
		int count = 0;
		for(int i=0; i < paramList.size(); i++){
			Map<String,ExcelAchieveEducationVo> paramMap = paramList.get(i);
			Iterator<String> keys = paramMap.keySet().iterator();
			while( keys.hasNext() ){
				String key = keys.next();
				ExcelAchieveEducationVo vo = paramMap.get(key);
				//logger.debug(i + "[" + key + "] =>" +  vo.getEducationCode() + " / " + vo.getUserCode() + " / " +  vo.getUserId() + " / "+ vo.getUserName() + " / " + vo.getUnit() + " / "  + vo.getContents() + "/ "+ vo.getPoint() );
				int  result = adminDao.upsertAchieveRegistEducation(vo);
				if(result > -1){				
					count++;
				}
			}
		}
		
		return count;
	}
	
	@Override
	@Transactional
	public int upsertAchieveRegistEducation(ArrayList<ExcelAchieveEducationVo> paramList) {
		int cnt = 0;
		for(int i=0; i < paramList.size(); i++){
			int result = adminDao.upsertAchieveRegistEducation(paramList.get(i));
			cnt++;
		}
		return cnt;
	}
	
	@Override
	public int upsertAchieveRegistEducation(ExcelAchieveEducationVo param) {
		int result = adminDao.upsertAchieveRegistEducation(param);
		return result;
	}
	
	
	

	@Override
	@Transactional
	public int upsertAchieveRegistResearch(List<Map<String, ExcelAchieveResearchVo>> paramList) {
		for(int i=0; i < paramList.size(); i++){
			Map<String,ExcelAchieveResearchVo> paramMap = paramList.get(i);
			Iterator<String> keys = paramMap.keySet().iterator();
			while( keys.hasNext() ){
				String key = keys.next();
				ExcelAchieveResearchVo vo = paramMap.get(key);
				logger.debug(i + "[" + key + "] =>" +  vo.getResearchItemNum() + " / " +  vo.getUserId() + " / "+ vo.getUserName() + " / " + vo.getUnit() + " / " + vo.getContents() );
				UserInfo userInfo = userDao.getUserInfoById(vo.getUserId());
				//logger.debug("userInfo =>" + userInfo);
				
				if(userInfo != null){
					//logger.debug("userInfo.getUserCode(); "+ userInfo.getUserCode());
					vo.setUserCode(String.valueOf(userInfo.getUserCode()));
				}
			}			
		}
		
		int count = 0;
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("achieveType", "r");
		List<Map<String,Object>> achievePointList = adminDao.getAchievePointInfo(map);
		for(int i=0; i < paramList.size(); i++){
			Map<String,ExcelAchieveResearchVo> paramMap = paramList.get(i);
			Iterator<String> keys = paramMap.keySet().iterator();
			while( keys.hasNext() ){
				String key = keys.next();
				ExcelAchieveResearchVo vo = paramMap.get(key);
				//logger.debug(i + "[" + key + "] =>" +  vo.getResearchItemNum() + " / " + vo.getUserCode() + " / " +  vo.getUserId() + " / "+ vo.getUserName() + " / " + vo.getUnit() + " / "  + vo.getContents() + "/ "+ vo.getPoint() );
				if(vo.getPoint() == null){
					vo.setPoint(getCalcualtePoint(achievePointList, vo.getResearchItemNum(), vo.getUnit()));
				}
				//logger.debug("vo.getPoint :"+ vo.getPoint());
				logger.debug(i + "[" + key + "] =>" +  vo.getResearchItemNum() + " / " + vo.getUserCode() + " / " +  vo.getUserId() + " / "+ vo.getUserName() + " / " + vo.getUnit() + " / "  + vo.getContents() + "/ "+ vo.getPoint() );
				int  result = adminDao.upsertAchieveRegistResearch(vo);
				if(result > -1){				
					count++;
				}
			}
		}
		
		return count;
	}
	private String getCalcualtePoint(List<Map<String,Object>> pointList,String itemNum,String unit){
		for(int i=0; i < pointList.size(); i++){
			Map<String,Object> pointInfo = pointList.get(i);
			//logger.debug("point.get(\"research_item_num\") :"+ pointInfo.get("research_item_num") + "  "+ itemNum);
			if(( (Integer) pointInfo.get("research_item_num")) ==  Integer.parseInt(itemNum)){
				int point = Integer.parseInt(String.valueOf(pointInfo.get("point")));
				int userUnit = Integer.parseInt(unit);
				int result = point * userUnit;
				//logger.debug("result =>"+ result);
				return String.valueOf(result);
			}
		}
		return null;
	}


	@Override
	@Transactional
	public int upsertAchieveRegistResearch(String requestUserCode, ArrayList<ExcelAchieveResearchVo> paramList) {
//		String researchCodeList = "";
//		for(int i=0; i < paramList.size(); i++){
//			ExcelAchieveResearchVo vo = paramList.get(i);
//			researchCodeList += vo.getResearchCode();
//			if((i+1) < paramList.size()){
//				researchCodeList += ",";
//			}
//		}
//		Map<String,Object> map = new HashMap<String,Object>();
//		map.put("userCode", requestUserCode);
//		map.put("researchCodeList", researchCodeList);
//		int deleteAchieveResearchResult = adminDao.deleteAchieveResearchResult(map);
//		logger.debug("deleteAchieveResearchResult =>"+ deleteAchieveResearchResult);
		
		int cnt = 0;
		for(int i=0; i < paramList.size(); i++){
			int result = adminDao.upsertAchieveRegistResearch(paramList.get(i));
			cnt++;
		}
		return cnt;
	}



	@Override
	public List<Map<String, Object>> getAchieveResearchFileList(Map<String, Object> map) {
		return adminDao.getAchieveResearchFileList(map);
	}
	
	public HashMap<String, Object> achieveResearchUploadFile(HashMap<String, Object> param){
		return adminDao.achieveResearchUploadFile(param);
	}
	
	public int deleteFile(Map<String,Object> param){
		return adminDao.deleteFile(param);
	}
	public List<Map<String,Object>> getFileCountByResearchCode(Map<String,Object> map){
		return adminDao.getFileCountByResearchCode(map);
	}

	@Override
	public int getPeriodListTotalCount(Map<String, Object> param){
		return adminDao.getPeriodListTotalCount(param);
	}
	@Override
	public List<Map<String,Object>> getPeriodList(Map<String, Object> param){
		return adminDao.getPeriodList(param);
	}
	@Override
	@Transactional
	public Map<String, Object> addPeriod(Map<String, Object> param) {
		Map<String, Object> result = adminDao.addPeriod(param);
		int periodCode = (Integer) result.get("period_code");
		if(periodCode > 0){
			param.put("periodCode", periodCode);
			Map<String, Object> addMyNotice = adminDao.addMyNotice(param);
			logger.debug("addMyNotice =>"+ addMyNotice);
		}
		return result;
	}
	@Override
	@Transactional
	public int updatePeriod(Map<String, Object> param){
		int result = adminDao.updatePeriod(param);;
		logger.debug("result =>"+ result);
		if(result > 0){
			String isDelete = param.get("useFlag") != null ? (String) param.get("useFlag") : null;
			if(isDelete != null && "N".equals(isDelete)){
				logger.debug("기간삭제이므로, 공지사항에 업데이트안함");
			}else{
				//int updateMyNotice = adminDao.updateMyNotice(param);
				//logger.debug("updateMyNotice =>"+ updateMyNotice);
				Map<String, Object> addMyNotice = adminDao.addMyNotice(param);
				logger.debug("addMyNotice =>"+ addMyNotice);
			}
		}
		return result;
	}



	@Override
	public List<Map<String, Object>> getAchieveGradeScoreInfo(Map<String, Object> param) {
		return adminDao.getAchieveGradeScoreInfo(param);
	}



	@Override
	@Transactional
	public int upsertAchievePeriodCriterion(List<HashMap<String, Object>> params) {
		int resultCnt = 0;
		for(int i=0; i < params.size(); i++){
			resultCnt = adminDao.upsertAchievePeriodCriterion(params.get(i));
			logger.debug("resultCnt =>"+ resultCnt);
		}
		return resultCnt;
	}



	@Override
	public List<Map<String, Object>> getPeriodCriterion(Map<String, Object> param) {
		return adminDao.getPeriodCriterion(param);
	}



	@Override
	public int upsertAchievePeriodGrade(Map<String, Object> map) {
		return adminDao.upsertAchievePeriodGrade(map);
	}
	@Override
	public Map<String, Object> getPeriodGrade(Map<String, Object> map) {
		return adminDao.getPeriodGrade(map);
	}
	
	public int upsertAchievePeriodMinPoint(Map<String,Object> map){
		return adminDao.upsertAchievePeriodMinPoint(map);
	}
	
	public  List<String>getPeriodYearInfo(){
		return adminDao.getPeriodYearInfo();
	}
	@Transactional
	public int updateGradeScoreInfo( List<Map<String,Object>> list, String achieveType){
		if(achieveType.equals("e")){
			for(int i =0; i < list.size(); i++){
				int result = adminDao.updateGradeScoreInfo_education(list.get(i));
				logger.debug("result =>"+ result);
			}
			return 1;	
		}else if(achieveType.equals("r")){
			for(int i =0; i < list.size(); i++){
				int result = adminDao.updateGradeScoreInfo_research(list.get(i));
				logger.debug("result =>"+ result);
			}
			return 1;	
		}else if(achieveType.equals("si") || achieveType.equals("so")){
			for(int i =0; i < list.size(); i++){
				int result = adminDao.updateGradeScoreInfo_service(list.get(i));
				logger.debug("result =>"+ result);
			}
			return 1;	
		}
		return 0;
	}
	
	public int serviceConfirm(HashMap<String,Object> map){
		return adminDao.serviceConfirm(map);
	}
	
	public int serviceUpdate(HashMap<String,Object> map){
		return adminDao.updateService(map);
	}
}
