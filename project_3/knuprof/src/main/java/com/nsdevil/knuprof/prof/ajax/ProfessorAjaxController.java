package com.nsdevil.knuprof.prof.ajax;

import java.io.File;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.SimpleFormatter;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.nsdevil.knuprof.prof.domain.ServiceFileVo;
import com.nsdevil.knuprof.prof.service.ProfessorService;
import com.nsdevil.knuprof.util.FileUploader;

import net.sf.json.JSONObject;

@Controller
public class ProfessorAjaxController {
	
	@Inject
	private FileSystemResource fileSystemResource;
	
	@Autowired
	private ProfessorService professorService; 
	
	@RequestMapping(value = "/ajax/service/getServiceList", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String getServiceList(HttpServletRequest request, @RequestParam HashMap<String, Object> param) {
		
		JSONObject json = new JSONObject();

		String status = "success";
		
		HttpSession session = request.getSession();
		
		param.put("userCode", session.getAttribute("userCode").toString());
		
		HashMap<String, Object> result = professorService.getServiceList(param);
		
		if (result != null) {
			json.accumulateAll(result);
		} else {
			status = "fail";
		}
		
		json.accumulate("status", status);

		return json.toString();
	}
	
	@RequestMapping(value = "/ajax/service/addService", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String addService(HttpServletRequest request, @RequestParam HashMap<String, Object> param, @RequestParam(value = "contents_text") String[] contentsList) {
		
		JSONObject json = new JSONObject();

		String status = "success";
		
		HttpSession session = request.getSession();
		
		param.put("userCode", session.getAttribute("userCode").toString());
		param.put("adminName", session.getAttribute("adminName").toString());
		
		param.put("contentsList", contentsList);
		
		professorService.addService(param);
		
		json.accumulate("status", status);

		return json.toString();
	}
	
	@RequestMapping(value = "/ajax/service/addSingleService", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String addService(HttpServletRequest request, @RequestParam HashMap<String, Object> param) {
		JSONObject json = new JSONObject();
		try {
						
			String status = "success";
			
			HttpSession session = request.getSession();
			
			param.put("userCode", session.getAttribute("userCode").toString());
			param.put("adminName", session.getAttribute("adminName").toString());
			
			//봉사 정보 가져온다.
			HashMap<String, Object> serviceInfo = professorService.getService(param);
			param.put("period_type","A");
			//평가 기준 기간 가져온다.
			HashMap<String, Object> periodInfo = professorService.getPeriod(param);
			
			System.out.println(serviceInfo);
			System.out.println(periodInfo);
			double point = Integer.parseInt(serviceInfo.get("point").toString());
			double totalPoint = 0;
			long calDay = 0;
			String unit = serviceInfo.get("unit").toString();
			
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			
			Date startDate = format.parse(periodInfo.get("start_date").toString());
			Date endDate = format.parse(periodInfo.get("end_date").toString());
			
			DecimalFormat dformat = new DecimalFormat("####.##");
			
			if(unit.trim().equals("월") || unit.trim().equals("년")){
				Date serviceStartDate = format.parse(param.get("startDate").toString());
				Date serviceEndDate = format.parse(param.get("endDate").toString());
				//보직 종료일이 평가기준일 보다 작을때
				if(serviceStartDate.compareTo(endDate) > 0){
					totalPoint = 0;
				}
				//보직 시작일이 평가 기준 시작일보다 이전, 종료일이 평가 기준 종료일보다 이전
				else if(startDate.compareTo(serviceStartDate) >= 0 && endDate.compareTo(serviceEndDate) >= 0){
					if(unit.trim().equals("년")){
						calDay = serviceEndDate.getTime() - startDate.getTime();
						totalPoint = (calDay / (24*60*60*1000)) / 365.0 * point;
					}else if(unit.trim().equals("월")){
						int sYear= Integer.parseInt(periodInfo.get("start_date").toString().substring(0,4)); 
						int sMonth = Integer.parseInt(periodInfo.get("start_date").toString().substring(5,7)); 
						int eYear = Integer.parseInt(param.get("endDate").toString().substring(0,4)); 
						int eMonth = Integer.parseInt(param.get("endDate").toString().substring(5,7)); 
						int month_diff = (eYear - sYear)* 12 + (eMonth - sMonth)+1; 
						totalPoint = month_diff * point;
						System.out.println(month_diff); 
					}
					
				}
				//보직 시작일이 평가 기준 시작일보다 이전, 종료일이 평가 기준 종료일보다 이후
				else if(startDate.compareTo(serviceStartDate) >= 0 && endDate.compareTo(serviceEndDate) <= 0){
					if(unit.trim().equals("년")){
						totalPoint = point;
					}else if(unit.trim().equals("월")){
						int sYear= Integer.parseInt(periodInfo.get("start_date").toString().substring(0,4)); 
						int sMonth = Integer.parseInt(periodInfo.get("start_date").toString().substring(5,7)); 
						int eYear = Integer.parseInt(periodInfo.get("end_date").toString().substring(0,4)); 
						int eMonth = Integer.parseInt(periodInfo.get("end_date").toString().substring(5,7)); 
						int month_diff = (eYear - sYear)* 12 + (eMonth - sMonth)+1; 
						totalPoint = month_diff * point;
						System.out.println(month_diff); 
					}
					
				}
				//보직 시작일이 평가 기준 시알일보다 이후, 종료일이 평기 기준 종료일보다 이전
				else if(startDate.compareTo(serviceStartDate) <= 0 && endDate.compareTo(serviceEndDate) >= 0){
					if(unit.trim().equals("년")){
						calDay = serviceEndDate.getTime() - serviceStartDate.getTime();	
						totalPoint = (calDay / (24*60*60*1000)) / 365.0 * point;
					}else if(unit.trim().equals("월")){
						int sYear= Integer.parseInt(param.get("startDate").toString().substring(0,4)); 
						int sMonth = Integer.parseInt(param.get("startDate").toString().substring(5,7)); 
						int eYear = Integer.parseInt(param.get("endDate").toString().substring(0,4)); 
						int eMonth = Integer.parseInt(param.get("endDate").toString().substring(5,7)); 
						int month_diff = (eYear - sYear)* 12 + (eMonth - sMonth)+1; 
						totalPoint = month_diff * point;
						System.out.println(month_diff); 
					}
					
				}
				//보직 시작일이 평가 기준 시알일보다 이후, 종료일이 평기 기준 종료일보다 이후
				else if(startDate.compareTo(serviceStartDate) <= 0 && endDate.compareTo(serviceEndDate) <=0 ){
					if(unit.trim().equals("년")){
						calDay = endDate.getTime() - serviceStartDate.getTime();
						totalPoint = (calDay / (24*60*60*1000)) / 365.0 * point;
					}else if(unit.trim().equals("월")){
						int sYear= Integer.parseInt(param.get("startDate").toString().substring(0,4)); 
						int sMonth = Integer.parseInt(param.get("startDate").toString().substring(5,7)); 
						int eYear = Integer.parseInt(periodInfo.get("end_date").toString().substring(0,4)); 
						int eMonth = Integer.parseInt(periodInfo.get("end_date").toString().substring(5,7)); 
						
						int month_diff = (eYear - sYear)* 12 + (eMonth - sMonth)+1; 
						totalPoint = month_diff * point;
						System.out.println(month_diff); 
					}
					
				}else{
					System.out.println(" 타임5   " + serviceStartDate.getTime() + "    " + serviceEndDate.getTime() );
				}
						
				param.put("point", dformat.format(totalPoint));
			}else{
				param.put("point", point);
			}
			
			
			System.out.println("totalPoint = " + dformat.format(totalPoint));
			HashMap<String, Object> result = professorService.addSingleService(param);
			
			if (result != null) {
				json.accumulateAll(result);
			} else {
				status = "fail";
			}
			json.accumulate("status", status);
		} catch (ParseException e) {			
			e.printStackTrace();
		}
		return json.toString();
	}
	
	@RequestMapping(value = "/ajax/service/deleteSingleService", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String deleteSingleService(HttpServletRequest request, @RequestParam HashMap<String, Object> param) {
		
		JSONObject json = new JSONObject();

		String status = "success";
		
		HashMap<String, Object> result = professorService.deleteSingleService(param);
		
		if (result != null) {
			json.accumulateAll(result);
		} else {
			status = "fail";
		}
		
		json.accumulate("status", status);

		return json.toString();
	}
	
	//ajaxForm 이용시 ie8에서 json 파일을 다운로드 받는 문제가 생겨 Content Type을 text/plain 으로 변경
	@RequestMapping(value = "/ajax/service/uploadFile", produces="text/plain; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String uploadFile(HttpServletRequest request,
			@RequestParam HashMap<String, Object> param,
			@RequestParam(value="file", required=false) MultipartFile file) {
		
		JSONObject json = new JSONObject();

		String status = "success";
		
		HttpSession session = request.getSession();
		
		param.put("userCode", session.getAttribute("userCode").toString());
		
		String uploadFileName = FileUploader.uploadFile(fileSystemResource.getPath() + "/upload", file);
		param.put("orignalName", file.getOriginalFilename());
		param.put("fileName", uploadFileName);
		
		if (uploadFileName.equals("not")) {
			status = "fail";
		} else {
			HashMap<String, Object> result = professorService.uploadFile(param);
			json.accumulateAll(result);
		}
		
		json.accumulate("status", status);
		json.accumulate("fileName", file.getOriginalFilename());
		json.accumulate("uploadFileName", uploadFileName);
		
		return json.toString();
	}
	
	@RequestMapping(value = "/ajax/service/deleteFile", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String deleteFile(HttpServletRequest request,
			@RequestParam HashMap<String, Object> param) {
		
		JSONObject json = new JSONObject();

		String status = "success";
		
		File file = new File(fileSystemResource.getPath() + "/upload/" + param.get("uploadFileName"));
		
		professorService.deleteFile(param);
		
		if (file.exists()) {
			file.delete();
		}
		
		json.accumulate("status", status);
		
		return json.toString();
	}
	
	@RequestMapping(value = "/ajax/service/getFileList", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String getFileList(HttpServletRequest request,
			@RequestParam HashMap<String, Object> param) {
		
		JSONObject json = new JSONObject();

		String status = "success";
		
		HttpSession session = request.getSession();
		
		param.put("userCode", session.getAttribute("userCode").toString());
		
		ArrayList<ServiceFileVo> result = professorService.getFileList(param);
		
		json.accumulate("list", result);
		json.accumulate("status", status);
		
		return json.toString();
	}
	
	@RequestMapping(value = "/ajax/service/getServiceResult", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String getServiceResult(HttpServletRequest request, @RequestParam HashMap<String, Object> param) {
		
		JSONObject json = new JSONObject();
		
		String status = "success";
		
		HttpSession session = request.getSession();
		
		param.put("userCode", session.getAttribute("userCode").toString());
		
		HashMap<String, Object> result = professorService.getServiceResult(param);
		
		if (result != null) {
			json.accumulateAll(result);
		} else {
			status = "fail";
		}
		
		json.accumulate("status", status);
		
		return json.toString();
	}
	
	@RequestMapping(value = "/ajax/service/copyAttachedFile", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String copyAttachedFile(HttpServletRequest request, @RequestParam HashMap<String, Object> param) {
		
		JSONObject json = new JSONObject();
		
		String status = "success";
		
		boolean result = professorService.copyAttachedFile();
		
		if (!result) {
			status = "fail";
		}
		
		json.accumulate("status", status);
		
		return json.toString();
	}	
}
