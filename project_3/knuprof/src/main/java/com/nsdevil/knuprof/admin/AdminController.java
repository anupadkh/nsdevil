package com.nsdevil.knuprof.admin;

import java.io.File;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.nsdevil.knuprof.admin.excel.ExcelAchieveEducationAdder;
import com.nsdevil.knuprof.admin.excel.ExcelAchieveEducationVo;
import com.nsdevil.knuprof.admin.excel.ExcelAchieveResearchAdder;
import com.nsdevil.knuprof.admin.excel.ExcelAchieveResearchVo;
import com.nsdevil.knuprof.admin.excel.ExcelAchieveServiceAdder;
import com.nsdevil.knuprof.admin.excel.ExcelAchieveServiceVo;
import com.nsdevil.knuprof.admin.service.AdminService;
import com.nsdevil.knuprof.prof.domain.ServiceFileVo;
import com.nsdevil.knuprof.prof.service.ProfessorService;
import com.nsdevil.knuprof.user.service.UserService;
import com.nsdevil.knuprof.user.vo.UserInfo;
import com.nsdevil.knuprof.util.FileUploader;
import com.nsdevil.knuprof.util.JSONUtil;
import com.nsdevil.knuprof.util.PageSplitter;

import net.sf.json.JSONObject;

@Controller
public class AdminController {
	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);
	@Autowired
	private UserService userService; 
	
	@Autowired
	private AdminService adminService;
	
	@Autowired
	private ProfessorService professorService; 
	
	@Inject
	private FileSystemResource fileSystemResource;
	
	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String admin(HttpServletRequest request, HttpSession session, Locale locale, Model model) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("/admin!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel);
		if(userLevel == 0){
			return "admin/userList";
		}
		return "login";
	}
	@RequestMapping(value = "/admin/achieve", method = RequestMethod.GET)
	public String registCategory(HttpServletRequest request, HttpSession session, Locale locale, Model model) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		int userCode = (Integer) session.getAttribute("userCode");
		logger.debug("/admin/achieve !!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel  + " userCode "+ userCode );
		String category = "s";
		if(request.getParameter("category") != null){
			category = request.getParameter("category");
		}
		logger.debug("category =>"+ category);
		model.addAttribute("category", category);
		List<Map<String,String>> authInfo =  null;
		JSONObject json = new JSONObject();
		if(userLevel == 1){
			authInfo =userService.getStaffAuthInfo(userCode);
			json.accumulate("authInfo", authInfo);
		}else{
			json.accumulate("authInfo", null);
		}
		model.addAttribute("json", json);
		
		if(category.equals("s")){
			return "admin/achieve_service_result";
		}
		if(userLevel == 0 || userLevel == 1){
			return "admin/achieve_service";
		}
		return "login";
	}
	
	@RequestMapping(value = "/admin/addUser", method = RequestMethod.GET)
	public String addUser(HttpServletRequest request, HttpSession session, Locale locale, Model model) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("/admin/addUser!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel);
		if(userLevel == 0){
			return "admin/addUser";
		}else if(userLevel == 1){
			return "/login";
		}
		return "login";
	}
	
	@RequestMapping(value = "/admin/addUserByExcel", method = RequestMethod.GET)
	public String addUserByExcel(HttpServletRequest request, HttpSession session, Locale locale, Model model) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("/admin/addUserByExcel !!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel);
		if(userLevel == 0){
			return "admin/addUserByExcel";
		}else if(userLevel == 1){
			return "/login";
		}
		return "login";
	}
	
	
	@RequestMapping(value = "/admin/editUser", method = RequestMethod.POST)
	public String editUser2(HttpServletRequest request, HttpSession session, Locale locale, Model model) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("/admin/editUser!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel);
		int userCode = 0;
		if(request.getParameter("userCode") != null){
			userCode = Integer.parseInt(request.getParameter("userCode"));
		}
		logger.debug("userCdoe =>"+ userCode);
		if(userLevel == 0){
			UserInfo userInfo = userService.getUserInfo(userCode);
			
			JSONObject json = new JSONObject();
			List<Map<String,String>> authInfo =  null;
			if(userInfo.getUserLevel() == 1){
				authInfo =userService.getStaffAuthInfo(userCode);
			}
			json.accumulate("authInfo", authInfo);
			
			model.addAttribute("json", json);
			model.addAttribute("userInfo", userInfo);
			return "admin/editUser";
		}else if(userLevel == 1){
			return "/login";
		}
		return "login";
	}
	
	@RequestMapping(value = "/admin/getProfessorList",  produces="application/json; charset=utf-8", method = RequestMethod.POST)
	@ResponseBody
	public String getProfessorList(HttpServletRequest request,HttpSession session, Locale locale, Model model, @RequestParam HashMap<String, Object> param ) {
		logger.debug("session.getId() =>"+ session.getId());
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("userLevel =>"+ userLevel);
		logger.debug("param.get('achieveType') ==================>"+ param.get("achieveType"));
		JSONObject json = new JSONObject();
		if(userLevel == 0 || userLevel == 1){
			List<Map<String,Object>> getProfessorList = adminService.getProfessorList(param);
			if(getProfessorList == null){
				json.accumulate("status", "fail");
				json.accumulate("message", "오류가 발생하였습니다.");
				return json.toString();
			}
			logger.debug("getProfessorList size :"+ getProfessorList.size());
			
			Map<String,Object> getCountAchieveRegistUser = adminService.getCountAchieveRegistUser(param);
			
			json.accumulate("status", "success");
			json.accumulate("list", getProfessorList);
			json.accumulate("registInfo", getCountAchieveRegistUser);
			return json.toString();
			
		}
		json.accumulate("status", "fail");
		json.accumulate("message", "권한 오류가 발생하였습니다.");
		return json.toString();
	}
	
//	@RequestMapping(value = "/admin/getCategoryHtmlData", method = RequestMethod.POST)
//	@ResponseBody
//	public String getCategoryHtmlData(HttpServletRequest request,HttpSession session, Locale locale, Model model,@RequestBody String jsonString) {
//		logger.debug("session.getId() =>"+ session.getId());
//		int userLevel = (Integer) session.getAttribute("userlevel");
//		logger.debug("userLevel =>"+ userLevel);
//		JSONUtil jsonUtil = new JSONUtil(jsonString);
//		String category = jsonUtil.getString("category", null);
//		JSONObject json = new JSONObject();
//		
//		if(userLevel == 0 || userLevel == 1){
//			Map<String,Object> param = new HashMap<String,Object>();
//			param.put("userCode", session.getAttribute("userCode"));
//			param.put("category", category);
//			List<Map<String,Object>> getCategoryData = adminService.getCategoryData(param);
//			logger.debug("getCategoryData =>"+ getCategoryData);
//			
//			json.accumulate("status", "success");
//			json.accumulate("list", getCategoryData);
//			
//			return json.toString();
//		}
//		json.accumulate("status", "fail");
//		json.accumulate("message", "권한 오류가 발생하였습니다.");
//		return json.toString();
//	}
	
	@RequestMapping(value = "/admin/getStaffAuthInfo", method = RequestMethod.POST)
	@ResponseBody
	public String getStaffAuchInfo(HttpServletRequest request,HttpSession session, Locale locale, Model model,@RequestBody String jsonString) {
		logger.debug("session.getId() =>"+ session.getId());
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("userLevel =>"+ userLevel);
		JSONObject json = new JSONObject();
		if(userLevel == 0 || userLevel == 1){
			int userCode =  (Integer) session.getAttribute("userCode");
			List<Map<String,String>>getStaffAuthInfo = userService.getStaffAuthInfo(userCode);
			logger.debug("getCategoryData =>"+ getStaffAuthInfo);
			
			json.accumulate("status", "success");
			json.accumulate("list", getStaffAuthInfo);
			
			return json.toString();
		}
		json.accumulate("status", "fail");
		json.accumulate("message", "권한 오류가 발생하였습니다.");
		return json.toString();
	}
	
	
	@RequestMapping(value = "/admin/getServiceResult", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String getServiceResult(HttpServletRequest request,HttpSession session,@RequestParam HashMap<String, Object> param ) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		JSONObject json = new JSONObject();
		logger.debug("/admin/getServiceResult!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel + " param:"+ param.get("userCode"));
		if(userLevel > 1){
			json.accumulate("status", "fail");
			json.accumulate("message", "권한 오류가 발생하였습니다.");
			return json.toString();
		}
		String status = "success";

		HashMap<String, Object> getServiceResult = professorService.getServiceResult(param);
		
		if (getServiceResult != null) {
			json.accumulateAll(getServiceResult);
		} else {
			status = "fail";
		}
		
		json.accumulate("status", status);
		
		return json.toString();
	}
	@RequestMapping(value = "/admin/getFileList", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String getFileList(HttpServletRequest request,HttpSession session,@RequestParam HashMap<String, Object> param ) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("/admin/getFileList!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel);
		JSONObject json = new JSONObject();
//		if(userLevel > 1){
//			json.accumulate("status", "fail");
//			json.accumulate("message", "권한 오류가 발생하였습니다.");
//			return json.toString();
//		}
		String status = "success";

		logger.debug("userCode =>" + param.get("userCode"));
		logger.debug("serviceCode =>" + param.get("serviceCode"));
		logger.debug("researchCode =>" + param.get("researchCode"));
		logger.debug("achieveType =>" + param.get("achieveType"));
		String achieveType = (String) param.get("achieveType");
		
		if("r".equals(achieveType)){
			List<Map<String,Object>> result = adminService.getAchieveResearchFileList(param);
			logger.debug("result =>"+ result.size());
			json.accumulate("list", result);
			json.accumulate("status", status);
		}else{
			ArrayList<ServiceFileVo> result = professorService.getFileList(param);
			logger.debug("result =>"+ result.size());
			json.accumulate("list", result);
			json.accumulate("status", status);
			
		}
		
		return json.toString();
	}
	
	@RequestMapping(value = "/admin/updateAchieve", method = RequestMethod.GET)
	public String registService(HttpServletRequest request, HttpSession session, Locale locale, Model model,@RequestParam HashMap<String, Object> param) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		int userCode = (Integer) session.getAttribute("userCode");
		logger.debug("/admin/updateAchieve !!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel + " / requestUserCode =>"+ request.getParameter("requestUserCode") + " / category =>"+ request.getParameter("category"));
		
		String requestUserCode = null;
		if(request.getParameter("requestUserCode") != null){
			requestUserCode = request.getParameter("requestUserCode");
			logger.debug("requestUserCode =>" + requestUserCode);
			model.addAttribute("requestUserCode", requestUserCode);
			model.addAttribute("userName", request.getParameter("userName"));
			model.addAttribute("subject", request.getParameter("subject"));
		}
		model.addAttribute("category", request.getParameter("category"));
		HashMap<String, Object> result = professorService.getServiceList(param);
		logger.debug("result ==================");
		logger.debug(""+result);
		model.addAttribute("data", result);
		
		param.put("achieveType", request.getParameter("category"));
		List<Map<String, Object>> getAchievePointInfo = adminService.getAchievePointInfo(param);
		List<Map<String,String>> authInfo =  null;
		
		JSONObject json = new JSONObject();
		json.accumulate("achievePointInfo", getAchievePointInfo);
		if(userLevel == 1){
			authInfo =userService.getStaffAuthInfo(userCode);
			json.accumulate("authInfo", authInfo);
		}else{
			json.accumulate("authInfo", null);
		}
		model.addAttribute("json", json);
		
		
		if(userLevel == 0 || userLevel == 1){
			if("s".equals(request.getParameter("category"))){
				return "admin/achieve_service_update";
			}else if("e".equals(request.getParameter("category"))){
				return "admin/achieve_education_update";
			}else if("r".equals(request.getParameter("category"))){
				return "admin/achieve_research_update";
			}
		}
		return "/login";
	}
	
	//ajaxForm 이용시 ie8에서 json 파일을 다운로드 받는 문제가 생겨 Content Type을 text/plain 으로 변경
	@RequestMapping(value = "/admin/uploadFile", produces="text/plain; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String uploadFile(HttpServletRequest request,
			@RequestParam HashMap<String, Object> param,
			@RequestParam(value="file", required=false) MultipartFile file) {
		logger.debug("/admin/uploadFile !!!!!!!!!!!!!!!!!!!!!!!! =>"+ "requestUserCode =>"+ param.get("userCode") + "  / category =>"+ param.get("category"));
		JSONObject json = new JSONObject();

		String status = "success";
		String category = (String) param.get("category");
		String uploadFileName = FileUploader.uploadFile(fileSystemResource.getPath() + "/upload/", file);
		param.put("orignalName", file.getOriginalFilename());
		param.put("fileName", uploadFileName);
		logger.debug("originalName =>"+ file.getOriginalFilename());
		logger.debug("fileName =>" + uploadFileName);
		if (uploadFileName.equals("not")) {
			status = "fail";
		} else {
			if("r".equals(category)){
				HashMap<String, Object> result = adminService.achieveResearchUploadFile(param);
				json.accumulateAll(result);
			}else{
				HashMap<String, Object> result = professorService.uploadFile(param);
				json.accumulateAll(result);
			}
		}
		
		json.accumulate("status", status);
		json.accumulate("fileName", file.getOriginalFilename());
		json.accumulate("uploadFileName", uploadFileName);
		logger.debug("jsonToString=>"+ json.toString());
		return json.toString();
	}
	
	@RequestMapping(value = "/admin/getAchieveRegistUserList", produces="text/plain; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String getAchieveRegistUserList(HttpServletRequest request,@RequestParam HashMap<String, Object> param) {
		logger.debug("/admin/getAchieveRegistUserList !!!!!!!!!!!!!!!!!!!!!!!! =>"+ "requestUserCode =>"+ param.get("userCode") + "  registered :"+ param.get("registered"));
		JSONObject json = new JSONObject();

		String status = "success";
		List<Map<String,Object>> getAchieveRegistUserList = adminService.getAchieveRegistUserList(param);
		logger.debug("getAchieveRegistUserList.size(); " + getAchieveRegistUserList.size());
		
		
		json.accumulate("status", status);
		json.accumulate("achieveList", getAchieveRegistUserList);
		return json.toString();
	}
	
	@RequestMapping(value = "/admin/addSingleService", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String addService(HttpServletRequest request, @RequestParam HashMap<String, Object> param) {
		JSONObject json = new JSONObject();
		try {
	
			String status = "success";
			param.put("adminName", request.getSession().getAttribute("adminName").toString());
			
			//봉사 정보 가져온다.
			HashMap<String, Object> serviceInfo = professorService.getService(param);
			param.put("period_type","A");
			//평가 기준 기간 가져온다.
			HashMap<String, Object> periodInfo = professorService.getPeriod(param);
			
			System.out.println(serviceInfo.toString());
			System.out.println(periodInfo.toString());
			
			double point = Integer.parseInt(serviceInfo.get("point").toString());
			double totalPoint = 0;
			long calDay = 0;
			String unit = serviceInfo.get("unit").toString();
			
			System.out.println("point == " + point);
			
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			
			Date startDate = format.parse(periodInfo.get("start_date").toString());
			Date endDate = format.parse(periodInfo.get("end_date").toString());
			
			DecimalFormat dformat = new DecimalFormat("####.##");
			
			if(unit.trim().equals("월") || unit.trim().equals("년")){
				Date serviceStartDate = format.parse(param.get("startDate").toString());
				Date serviceEndDate = format.parse(param.get("endDate").toString());
				//보직 종료일이 평가기준일 보다 작을때
				if(serviceStartDate.compareTo(endDate) > 0){
					totalPoint = 0;
				}
				//보직 시작일이 평가 기준 시작일보다 이전, 종료일이 평가 기준 종료일보다 이전
				else if(startDate.compareTo(serviceStartDate) >= 0 && endDate.compareTo(serviceEndDate) >= 0){
					if(unit.trim().equals("년")){
						calDay = serviceEndDate.getTime() - startDate.getTime();
						totalPoint = (calDay / (24*60*60*1000)) / 365.0 * point;
					}else if(unit.trim().equals("월")){
						int sYear= Integer.parseInt(periodInfo.get("start_date").toString().substring(0,4)); 
						int sMonth = Integer.parseInt(periodInfo.get("start_date").toString().substring(5,7)); 
						int eYear = Integer.parseInt(param.get("endDate").toString().substring(0,4)); 
						int eMonth = Integer.parseInt(param.get("endDate").toString().substring(5,7)); 
						int month_diff = (eYear - sYear)* 12 + (eMonth - sMonth)+1; 
						totalPoint = month_diff * point;
						System.out.println(month_diff); 
					}
					
				}
				//보직 시작일이 평가 기준 시작일보다 이전, 종료일이 평가 기준 종료일보다 이후
				else if(startDate.compareTo(serviceStartDate) >= 0 && endDate.compareTo(serviceEndDate) <= 0){
					if(unit.trim().equals("년")){
						totalPoint = point;
					}else if(unit.trim().equals("월")){
						int sYear= Integer.parseInt(periodInfo.get("start_date").toString().substring(0,4)); 
						int sMonth = Integer.parseInt(periodInfo.get("start_date").toString().substring(5,7)); 
						int eYear = Integer.parseInt(periodInfo.get("end_date").toString().substring(0,4)); 
						int eMonth = Integer.parseInt(periodInfo.get("end_date").toString().substring(5,7)); 
						int month_diff = (eYear - sYear)* 12 + (eMonth - sMonth)+1; 
						totalPoint = month_diff * point;
						System.out.println(month_diff); 
					}
					
				}
				//보직 시작일이 평가 기준 시알일보다 이후, 종료일이 평기 기준 종료일보다 이전
				else if(startDate.compareTo(serviceStartDate) <= 0 && endDate.compareTo(serviceEndDate) >= 0){
					if(unit.trim().equals("년")){
						calDay = serviceEndDate.getTime() - serviceStartDate.getTime();	
						totalPoint = (calDay / (24*60*60*1000)) / 365.0 * point;
					}else if(unit.trim().equals("월")){
						int sYear= Integer.parseInt(param.get("startDate").toString().substring(0,4)); 
						int sMonth = Integer.parseInt(param.get("startDate").toString().substring(5,7)); 
						int eYear = Integer.parseInt(param.get("endDate").toString().substring(0,4)); 
						int eMonth = Integer.parseInt(param.get("endDate").toString().substring(5,7)); 
						int month_diff = (eYear - sYear)* 12 + (eMonth - sMonth)+1; 
						totalPoint = month_diff * point;
						System.out.println(month_diff); 
					}
					
				}
				//보직 시작일이 평가 기준 시알일보다 이후, 종료일이 평기 기준 종료일보다 이후
				else if(startDate.compareTo(serviceStartDate) <= 0 && endDate.compareTo(serviceEndDate) <=0 ){
					if(unit.trim().equals("년")){
						calDay = endDate.getTime() - serviceStartDate.getTime();
						totalPoint = (calDay / (24*60*60*1000)) / 365.0 * point;
					}else if(unit.trim().equals("월")){
						int sYear= Integer.parseInt(param.get("startDate").toString().substring(0,4)); 
						int sMonth = Integer.parseInt(param.get("startDate").toString().substring(5,7)); 
						int eYear = Integer.parseInt(periodInfo.get("end_date").toString().substring(0,4)); 
						int eMonth = Integer.parseInt(periodInfo.get("end_date").toString().substring(5,7)); 
						
						int month_diff = (eYear - sYear)* 12 + (eMonth - sMonth)+1; 
						totalPoint = month_diff * point;
						System.out.println(month_diff); 
					}
					
				}else{
					System.out.println(" 타임5   " + serviceStartDate.getTime() + "    " + serviceEndDate.getTime() );
				}
						
				param.put("point", dformat.format(totalPoint));
			}else{
				param.put("point", point);
			}
			System.out.println(param.toString());
			HashMap<String, Object> result = professorService.addSingleService(param);
			
			if (result != null) {
				json.accumulateAll(result);
			} else {
				status = "fail";
			}
			json.accumulate("status", status);
		} catch (ParseException e) {			
			e.printStackTrace();
		}
		return json.toString();
	}

	
	
	//ajaxForm 이용시 ie8에서 json 파일을 다운로드 받는 문제가 생겨 Content Type을 text/plain 으로 변경
	@RequestMapping(value = "/admin/achieveExcelFile", produces="text/plain; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String achieveExcelFile(HttpServletRequest request,
			@RequestParam HashMap<String, Object> param,
			@RequestParam(value="uploadInputFile", required=false) MultipartFile file) {
		//logger.debug("/admin/achieveExcelFile !!!!!!!!!!!!!!!!!!!!!!!! file=>"+file);
		JSONObject json = new JSONObject();
		
		String uploadFileName = FileUploader.uploadFile(fileSystemResource.getPath() + "/upload", file);
		//param.put("orignalName", file.getOriginalFilename());
		//param.put("fileName", uploadFileName);
		String achieveType = (String) param.get("achieveType");
		//logger.debug("achieveType =>"+ achieveType);
		String fileLocation = fileSystemResource.getPath() + "/upload/" + uploadFileName;
		//logger.debug("fileLocation =>"+ fileLocation);
		int resultCount = -1;
		if (uploadFileName.equals("not")) {
			json.accumulate("status", "fail");
		} else {
			if("s".equals(achieveType)){
				ExcelAchieveServiceAdder excel = new ExcelAchieveServiceAdder(fileLocation);
				List<Map<String, ExcelAchieveServiceVo>> serviceList = excel.getServiceList();
				
				resultCount = adminService.addAchieveRegistService(serviceList);
				//logger.debug("resultCount =>"+ resultCount);
				json.accumulate("status", "success");
				json.accumulate("resultCount", resultCount);
			}else if("e".equals(achieveType)){
				ExcelAchieveEducationAdder excel = new ExcelAchieveEducationAdder(fileLocation);
				List<Map<String, ExcelAchieveEducationVo>> getExcelAchieveList = excel.getExcelAchieveList();
				
				if(getExcelAchieveList != null){
					resultCount = adminService.upsertAchieveRegistEducation(getExcelAchieveList);
					//logger.debug("resultCount =>"+ resultCount);
					json.accumulate("status", "success");
					json.accumulate("resultCount", resultCount);
				}
			}else if("r".equals(achieveType)){
				ExcelAchieveResearchAdder excel = new ExcelAchieveResearchAdder(fileLocation);
				List<Map<String, ExcelAchieveResearchVo>> getExcelAchieveList = excel.getExcelAchieveList();
				
				//logger.debug("getExcelAchieveList =>"+ getExcelAchieveList.size());
				if(getExcelAchieveList != null){
					resultCount = adminService.upsertAchieveRegistResearch(getExcelAchieveList);
					//logger.debug("resultCount =>"+ resultCount);
					json.accumulate("status", "success");
					json.accumulate("resultCount", resultCount);
				}
			}
			if(resultCount == -1){
				json.accumulate("status", "fail");
			}
		}
		
		return json.toString();
	}
	
	@RequestMapping(value = "/admin/getAchieveProfessorResult", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	@Transactional(readOnly = true)
	public @ResponseBody String getAchieveProfessorResult(HttpServletRequest request, @RequestParam HashMap<String, Object> param) {
		logger.debug("getAchieveProfessorResult!!!!!!!! achieveType :"+ param.get("achieveType"));
		JSONObject json = new JSONObject();
		String achieveType = null;
		if(param.get("achieveType") != null){
			achieveType = (String) param.get("achieveType");
		}
		String status = "success";
		List<Map<String, Object>> result = adminService.getAchieveProfessorResult(param);
		logger.debug(" getAchieveProfessorResult result"+ result);
//		if("r".equals(achieveType)){
//			List<Map<String, Object>> fileCount = adminService.getFileCountByResearchCode(param);
//			json.accumulate("fileCount", fileCount);
//		}
		if (result != null) {
			json.accumulate("result",result);
			//json.accumulateAll(result);
		} else {
			status = "fail";
		}
		json.accumulate("status", status);
		logger.debug(json.toString());
		return json.toString();
	}

	
	
	@RequestMapping(value = "/admin/test", produces="application/json; charset=utf-8", method = RequestMethod.GET)
	public @ResponseBody String test(HttpServletRequest request) {
		
		JSONObject json = new JSONObject();
		ExcelAchieveEducationVo param = new ExcelAchieveEducationVo("6");
		param.setUserCode("302");
		param.setContents("콘텐츠@@@@@@@@@@@@@@@");
		param.setPoint("10");
		param.setUnit("2");
		String status = "success";
		int result = adminService.upsertAchieveRegistEducation(param);
		if (result > 0) {
			json.accumulate("result",result);
			//json.accumulateAll(result);
		} else {
			status = "fail";
		}
		json.accumulate("status", status);

		return json.toString();
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/admin/updateAchieveData", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String updateAchieveData(HttpServletRequest request, @RequestBody String jsonString) {
		JSONObject json = new JSONObject();
		logger.debug(jsonString);
		
		JSONUtil jsonObject = new JSONUtil(jsonString);
		String achieveType = jsonObject.getString("achieveType",null);
		String requestUserCode = jsonObject.getString("requestUserCode",null);
		String paramList = jsonObject.getString("paramList", null);
		logger.debug("achieveType =>"+ achieveType);
		logger.debug("requestUserCode =>"+ requestUserCode);
		logger.debug("paramList =>"+ paramList);
		
		List<JSONObject> jsonObjectList = new JSONUtil(paramList).getJsonObjectList();
		if(jsonObjectList == null){
			json.accumulate("status", "fail");
			return json.toString();
		}
		if(achieveType != null && "e".equals(achieveType)){
			ArrayList<ExcelAchieveEducationVo> list = null;
			for(int i=0; i < jsonObjectList.size(); i++){
				JSONObject jObject = jsonObjectList.get(i);
				if(list == null){
					 list = new ArrayList<ExcelAchieveEducationVo>();
				}
				list.add(new ExcelAchieveEducationVo(jObject.getString("educationCode"), jObject.getString("userCode"), jObject.getString("unit"), jObject.getString("contents"), jObject.getString("point"), jObject.getString("useFlag")));
			}
			for(ExcelAchieveEducationVo vo : list){
				logger.debug("getEducationCode :"+ vo.getEducationCode() + " /usercode :"+ vo.getUserCode() + " /contents:" + vo.getContents() + " /unit:" + vo.getUnit() + " / point :"+ vo.getPoint() + " / useFlag :"+ vo.getUseFlag());
			}
			if(list != null){
				int result = adminService.upsertAchieveRegistEducation(list);
				json.accumulate("status", "success");
				json.accumulate("result", result);
			}else{
				json.accumulate("status", "fail");
			}
		}else if(achieveType != null && "r".equals(achieveType)){
			ArrayList<ExcelAchieveResearchVo> list = null;
			for(int i=0; i < jsonObjectList.size(); i++){
				JSONObject jObject = jsonObjectList.get(i);
				if(list == null){
					 list = new ArrayList<ExcelAchieveResearchVo>();
				}
				list.add(new ExcelAchieveResearchVo(jObject.getString("researchItemNum")
						, jObject.getString("userCode"), jObject.getString("unit"),
						jObject.getString("contents"), jObject.getString("point"), jObject.getString("useFlag")));
			}
			for(ExcelAchieveResearchVo vo : list){
				logger.debug("getResearchCode :"+ vo.getResearchItemNum() + " /usercode :"+ vo.getUserCode() + " /contents:" + vo.getContents() + " /unit:" + vo.getUnit() + " / point :"+ vo.getPoint());
			}
			if(list != null){
				int result = adminService.upsertAchieveRegistResearch(requestUserCode,list);
				json.accumulate("status", "success");
				json.accumulate("result", result);
			}else{
				json.accumulate("status", "fail");
			}
		}
		
		return json.toString();
	}
	
	@RequestMapping(value = "/admin/deleteFile", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String deleteFile(HttpServletRequest request,@RequestParam HashMap<String, Object> param) {
		logger.debug("/admin/deleteFile!!!!!!!!!!!!!!!!!!!!!!!!");
		logger.debug("param.get('achieveType):" + param.get("achieveType") + " researchFileCode =>"+ param.get("researchFileCode"));
		JSONObject json = new JSONObject();
		String status = "success";
		File file = new File(fileSystemResource.getPath() + "/upload/" + param.get("uploadFileName"));
		int result = adminService.deleteFile(param);
		logger.debug("result =>"+ result);
		if (file.exists()) {
			file.delete();
		}
		
		json.accumulate("status", status);
		json.accumulate("result", result);
		
		return json.toString();
	}
	
	@RequestMapping(value = "/admin/getFileCountByResearchCode", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String getFileCountByResearchCode(HttpServletRequest request,@RequestParam HashMap<String, Object> param) {
		logger.debug("param.get('achieveType):" + param.get("achieveType"));
		JSONObject json = new JSONObject();
		String status = "success";
		
		List<Map<String,Object>> fileCount = adminService.getFileCountByResearchCode(param);
		json.accumulate("fileCount", fileCount);
		json.accumulate("status", status);
		
		return json.toString();
	}
	
	@RequestMapping(value = "/admin/period",  method = RequestMethod.GET)
	public String period(HttpServletRequest request,HttpSession session) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("/admin/period!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel);
		if(userLevel == 0){
			return "admin/period";
		}
		return "login";
	}
	
	@RequestMapping(value = "/admin/getPeriodList", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String getPeriodList(HttpServletRequest request,HttpSession session,@RequestParam HashMap<String, Object> param) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		int userCode = (Integer) session.getAttribute("userCode");
		JSONObject json = new JSONObject();
		logger.debug("/admin/getPeriodList!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel + "   "+ userCode);
		if(userLevel != 0){
			json.accumulate("status", "fail");
			json.accumulate("message", "권한 오류가 발생하였습니다");
			return json.toString();
		}
		
		
		List<Map<String,Object>> periodList = adminService.getPeriodList(param);
		List<Map<String,Object>> periodCriterion = adminService.getPeriodCriterion(param);
		Map<String,Object> periodGrade = adminService.getPeriodGrade(param);
		logger.debug("periodList =>"+ periodList);
		if(periodList != null && periodList.size() > 0){
			json.accumulate("status", "success");
			json.accumulate("periodList", periodList);
			json.accumulate("periodCriterion", periodCriterion);
			json.accumulate("periodGrade", periodGrade);
		}else{
			json.accumulate("status", "fail");
		}
		
		return json.toString();
	}
	@RequestMapping(value = "/admin/addPeriod", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String addPeriod(HttpServletRequest request,HttpSession session,@RequestParam HashMap<String, Object> param) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		int userCode = (Integer) session.getAttribute("userCode");
		JSONObject json = new JSONObject();
		logger.debug("/admin/addPeriod!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel + "   "+ userCode);
		logger.debug("" + param.get("year") + " / "+ param.get("startDate") + " / " + param.get("endData"));
		if(userLevel != 0){
			json.accumulate("status", "fail");
			json.accumulate("message", "권한 오류가 발생하였습니다");
			return json.toString();
		}
		
		param.put("userCode", userCode);
		List<Map<String,Object>> periodList=  adminService.getPeriodList(param);
		logger.debug("periodList =>"+ periodList.size());
		if(periodList != null && periodList.size() > 0){
			json.accumulate("status", "fail");
			json.accumulate("message", "duplicated");
		}else{
			Map<String,Object> result = adminService.addPeriod(param);
			json.accumulate("result", result);
			json.accumulate("status", "success");
		}
		return json.toString();
	}
	@RequestMapping(value = "/admin/updatePeriod", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String updatePeriod(HttpServletRequest request,HttpSession session,@RequestParam HashMap<String, Object> param) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		int userCode = (Integer) session.getAttribute("userCode");
		JSONObject json = new JSONObject();
		//logger.debug("/admin/updatePeriod!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel + "   "+ userCode);
		//logger.debug( "periodCode :"+param.get("periodCode") + " startDate:"+ param.get("startDate") + "  endDate :"+ param.get("endDate") + " useFlag:"+ param.get("useFlag"));
		System.out.println(param.toString());
		if(userLevel != 0){
			json.accumulate("status", "fail");
			json.accumulate("message", "권한 오류가 발생하였습니다");
			return json.toString();
		}
		
		param.put("userCode", userCode);
		int result = adminService.updatePeriod(param);
		logger.debug("result =>"+ result);
		json.accumulate("status", "success");
		json.accumulate("result", result);
		return json.toString();
	}
	@RequestMapping(value = "/admin/updateCriterion", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String updateCriterion(HttpServletRequest request,HttpSession session,@RequestBody String jsonString,@RequestParam HashMap<String, Object> param) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		int userCode = (Integer) session.getAttribute("userCode");
		JSONObject json = new JSONObject();
		logger.debug("/admin/updateCriterion!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel + "   "+ userCode);
		logger.debug(jsonString);
		if(userLevel != 0){
			json.accumulate("status", "fail");
			json.accumulate("message", "권한 오류가 발생하였습니다");
			return json.toString();
		}
		

		JSONUtil jsonObject = new JSONUtil(jsonString);
		String paramList = jsonObject.getString("paramList", null);
		logger.debug("paramList =>"+ paramList);
		
		List<JSONObject> jsonObjectList = new JSONUtil(paramList).getJsonObjectList();

		List<HashMap<String,Object>> list = new ArrayList<HashMap<String,Object>>();
		for(int i=0; i < jsonObjectList.size(); i++){
			JSONObject jObject = jsonObjectList.get(i);
			logger.debug("" + jObject.getString("category_code"));
			HashMap<String,Object> map = new HashMap<String,Object>();
			map.put("type_ed", jObject.getString("type_ed"));
			map.put("type_re", jObject.getString("type_re"));
			map.put("type_se", jObject.getString("type_se"));
			map.put("type_st", jObject.getString("type_st"));
			map.put("year", jObject.getString("year"));
			map.put("category_code", jObject.getString("category_code"));
			list.add(map);
			
			Iterator<String> keys = map.keySet().iterator();
	        while( keys.hasNext() ){
	            String key = keys.next();
	            logger.debug( String.format("키 : %s, 값 : %s", key, map.get(key)) );
	        }
		}
		
		int result = adminService.upsertAchievePeriodCriterion(list);
		logger.debug("result =>"+ result);

		json.accumulate("status", "success");
		return json.toString();
	}
	@RequestMapping(value = "/admin/updatePeriodGrade", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String updatePeriodGrade(HttpServletRequest request,HttpSession session,@RequestParam HashMap<String, Object> param) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		int userCode = (Integer) session.getAttribute("userCode");
		JSONObject json = new JSONObject();
		logger.debug("/admin/updatePeriodGrade!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel + "   "+ userCode);
		if(userLevel != 0){
			json.accumulate("status", "fail");
			json.accumulate("message", "권한 오류가 발생하였습니다");
			return json.toString();
		}
		
		int result = adminService.upsertAchievePeriodGrade(param);

		logger.debug("result =>"+ result);

		json.accumulate("status", "success");
		return json.toString();
	}
	@RequestMapping(value = "/admin/updateAchievePeriodMinPoint", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String updateAchievePeriodMinPoint(HttpServletRequest request,HttpSession session,@RequestParam HashMap<String, Object> param) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		int userCode = (Integer) session.getAttribute("userCode");
		JSONObject json = new JSONObject();
		logger.debug("/admin/updateAchievePeriodMinPoint!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel + "   "+ userCode);
		if(userLevel != 0){
			json.accumulate("status", "fail");
			json.accumulate("message", "권한 오류가 발생하였습니다");
			return json.toString();
		}
		
		int result = adminService.upsertAchievePeriodMinPoint(param);

		logger.debug("result =>"+ result);

		json.accumulate("status", "success");
		return json.toString();
	}
	
	
	
	@RequestMapping(value = "/admin/gradeScore",  method = RequestMethod.GET)
	public String gradeScore(HttpServletRequest request,Model model,HttpSession session) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		logger.debug("/admin/gradeScore!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel);
		List<String> yearInfo = adminService.getPeriodYearInfo();
		if(yearInfo != null){
			model.addAttribute("yearInfo", yearInfo);
		}
		if(userLevel == 0){
			return "admin/gradeScore";
		}
		
		return "login";
	}
	
	@RequestMapping(value = "/admin/getGradeScoreInfo", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String getGradeScoreInfo(HttpServletRequest request,HttpSession session,@RequestParam HashMap<String, Object> param) {
		int userLevel = (Integer) session.getAttribute("userlevel");
		int userCode = (Integer) session.getAttribute("userCode");
		JSONObject json = new JSONObject();
		logger.debug("/admin/getGradeScoreInfo!!!!!!!!!!!!!!!!!!!!!!!! =>"+ userLevel + "   "+ userCode);
		if(userLevel != 0){
			json.accumulate("status", "fail");
			json.accumulate("message", "권한 오류가 발생하였습니다");
			return json.toString();
		}
		logger.debug("achieveType =>"+ param.get("achieveType"));
		List<Map<String,Object>> getAchieveGradeScoreInfo = adminService.getAchieveGradeScoreInfo(param);
		if(getAchieveGradeScoreInfo == null || getAchieveGradeScoreInfo.size() == 0){
			json.accumulate("status", "fail");
			json.accumulate("result", "검색결과가 없습니다");
		}else{
			logger.debug("getAchieveGradeScoreInfo=>"+ getAchieveGradeScoreInfo.size());
			
			json.accumulate("status", "success");
			json.accumulate("result", getAchieveGradeScoreInfo);			
		}
		return json.toString();
	}
	
	
	@SuppressWarnings("unused")
	@RequestMapping(value = "/admin/updateGradeScoreInfo", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String updateGradeScoreInfo(HttpServletRequest request, @RequestBody String jsonString) {
		JSONObject json = new JSONObject();
		logger.debug(jsonString);
		
		JSONUtil jsonObject = new JSONUtil(jsonString);
		String achieveType = jsonObject.getString("achieveType",null);
		String searchYear = jsonObject.getString("searchYear",null);
		String paramList = jsonObject.getString("paramList", null);
		logger.debug("achieveType =>"+ achieveType);
		logger.debug("searchYear =>"+ searchYear);
		logger.debug("paramList =>"+ paramList);
		
		List<JSONObject> jsonObjectList = new JSONUtil(paramList).getJsonObjectList();
		if(jsonObjectList == null){
			json.accumulate("status", "fail");
			return json.toString();
		}
		if(achieveType != null && "e".equals(achieveType)){
			List<Map<String,Object>> scoreList=  new ArrayList<Map<String,Object>>();
			for(int i=0; i < jsonObjectList.size(); i++){
				logger.debug("[" + i + " ] " + jsonObjectList.get(i));
				JSONObject jObject = jsonObjectList.get(i);
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("searchYear", searchYear);
				map.put("comment", jObject.getString("text"));
				map.put("point", jObject.getString("point"));
				map.put("unit", jObject.getString("unit"));
				map.put("weight_point", jObject.getString("weight"));
				map.put("limit", jObject.getString("limit"));
				map.put("min_check", jObject.getString("min"));
				map.put("category_id", jObject.getString("category_id"));
				map.put("education_item_num", i+1);
				scoreList.add(map);
			}
			if(scoreList != null){
//				int result = adminService.upsertAchieveRegistEducation(list);
				int result = adminService.updateGradeScoreInfo(scoreList, achieveType);
				json.accumulate("status", "success");
//				json.accumulate("result", result);
			}else{
				json.accumulate("status", "fail");
			}
			return json.toString();
		}else if(achieveType != null && "r".equals(achieveType)){
			List<Map<String,Object>> scoreList=  new ArrayList<Map<String,Object>>();
			for(int i=0; i < jsonObjectList.size(); i++){
				logger.debug("[" + i + " ] " + jsonObjectList.get(i));
				JSONObject jObject = jsonObjectList.get(i);
				Map<String,Object> map = new HashMap<String,Object>();
				map.put("searchYear", searchYear);
				map.put("comment", jObject.getString("text"));
				map.put("point", jObject.getString("point"));
				map.put("unit", jObject.getString("unit"));
				map.put("weight_point", jObject.getString("weight"));
				map.put("limit", jObject.getString("limit"));
				map.put("min_check", jObject.getString("min"));
				map.put("category_id", jObject.getString("category_id"));
				map.put("research_item_num", i+1);
				scoreList.add(map);
			}
			logger.debug("achieveType ======>"+ achieveType);
			logger.debug("scoreList.size =>"+ scoreList.size());
			if(scoreList != null){
				int result = adminService.updateGradeScoreInfo(scoreList, achieveType);
				json.accumulate("status", "success");
				json.accumulate("result", result);
			}else{
				json.accumulate("status", "fail");
			}
		}else if(achieveType != null && ("si".equals(achieveType) || "so".equals(achieveType))){
			List<Map<String,Object>> scoreList=  new ArrayList<Map<String,Object>>();
			for(int i=0; i < jsonObjectList.size(); i++){
				logger.debug("[" + i + " ] " + jsonObjectList.get(i));
				JSONObject jObject = jsonObjectList.get(i);
				Map<String,Object> map = new HashMap<String,Object>();
				logger.debug("jObject.getString(category_id)" + jObject.getString("category_id"));
				map.put("searchYear", searchYear);
				map.put("comment", jObject.getString("text"));
				map.put("point", jObject.getString("point"));
				map.put("unit", jObject.getString("unit"));
				map.put("weight_point", jObject.getString("weight"));
				map.put("limit", jObject.getString("limit"));
				map.put("min_check", jObject.getString("min"));
				map.put("service_type", jObject.getString("service_type"));
				map.put("category_id", jObject.getString("category_id"));
				map.put("service_item_num", i+1);
				scoreList.add(map);
			}
			logger.debug("achieveType ======>"+ achieveType);
			logger.debug("scoreList.size =>"+ scoreList.size());
			if(scoreList != null){
				int result = adminService.updateGradeScoreInfo(scoreList, achieveType);
				json.accumulate("status", "success");
				json.accumulate("result", result);
			}else{
				json.accumulate("status", "fail");
			}
		}
		
		return json.toString();
	}
	
	@RequestMapping(value = "/admin/serviceConfirm", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String serviceConfirm(HttpServletRequest request, @RequestParam HashMap<String, Object> param) {
		
		JSONObject json = new JSONObject();
		String status = "success";
				
		int result = adminService.serviceConfirm(param);
		System.out.println(result);
		if(result != 1){
			status="fail";
		}
		json.put("status", status);
		return json.toString();
	}
	
	@RequestMapping(value = "/admin/serviceUpdate", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String serviceUpdate(HttpServletRequest request, @RequestParam HashMap<String, Object> param) {
		String uptAgent = request.getSession().getAttribute("userCode").toString();
		param.put("upt_agent", uptAgent);
		
		JSONObject json = new JSONObject();
		String status = "success";
		System.out.println(param.toString());
		if(param.get("content").toString().equals("") || param.get("content").toString().equals("직접입력")){
			param.put("contents", param.get("direct_input").toString());
		}else{
			param.put("contents", param.get("content").toString());
		}
		int result = adminService.serviceUpdate(param);
		System.out.println(result);
		if(result != 1){
			status="fail";
		}
		json.put("status", status);
		return json.toString();
	}
}
