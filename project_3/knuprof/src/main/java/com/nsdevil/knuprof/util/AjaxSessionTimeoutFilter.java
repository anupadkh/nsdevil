package com.nsdevil.knuprof.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AjaxSessionTimeoutFilter implements Filter {
	
	/** 커스텀 AJAX 오류코드 */
    private int customSessionExpiredErrorCode = 901;
 
    @Override
    public void init(FilterConfig arg0) throws ServletException {
        // Property check here
    }
 
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filerChain) throws IOException, ServletException {
        HttpSession currentSession = ((HttpServletRequest)request).getSession(false);
        if(currentSession == null) {
            String ajaxHeader = ((HttpServletRequest) request).getHeader("X-Requested-With");
            if("XMLHttpRequest".equals(ajaxHeader)) {
                HttpServletResponse resp = (HttpServletResponse) response;
                resp.sendError(this.customSessionExpiredErrorCode);
            }
        }
    }

	@Override
	public void destroy() {
		// TODO 자동 생성된 메소드 스텁
		
	}
}
