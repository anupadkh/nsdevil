package com.nsdevil.knuprof.main.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nsdevil.knuprof.main.dao.MainDao;
import com.nsdevil.knuprof.main.domain.LoginVo;

@Service
public class MainServiceImpl implements MainService {
	
	@Autowired
	private MainDao mainDao;
	
	@Override
	public  HashMap<String, Object> getLoginState(LoginVo loginVo) {
		return mainDao.getLoginState(loginVo);
	}

	@Override
	public LoginVo getUserInfo(int userCode) {
		return mainDao.getUserInfo(userCode);
	}

	@Override
	public String addLoginLog(LoginVo loginVo) {
		return mainDao.addLoginLog(loginVo);
	}
}
