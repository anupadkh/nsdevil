package com.nsdevil.knuprof.main.service;

import java.util.HashMap;

import com.nsdevil.knuprof.main.domain.LoginVo;

public interface MainService {
	public HashMap<String, Object> getLoginState(LoginVo loginVo);
	public LoginVo getUserInfo(int userCode);
	public String addLoginLog(LoginVo loginVo);
}
