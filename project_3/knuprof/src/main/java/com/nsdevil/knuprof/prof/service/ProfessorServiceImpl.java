package com.nsdevil.knuprof.prof.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;

import com.nsdevil.knuprof.prof.dao.ProfessorDao;
import com.nsdevil.knuprof.prof.domain.ServiceFileVo;
import com.nsdevil.knuprof.prof.domain.ServiceResultVo;
import com.nsdevil.knuprof.prof.domain.ServiceVo;
import com.nsdevil.knuprof.util.Util;

@Service
public class ProfessorServiceImpl implements ProfessorService {

	@Autowired
	private ProfessorDao professorDao;
	
	@Inject
	private FileSystemResource fileSystemResource;
	
	@Override
	public HashMap<String, Object> getServiceList(HashMap<String, Object> param) {
		
		HashMap<String, Object> result = null;
		
		param.put("serviceType", 1);
		ArrayList<ServiceVo> service1List = professorDao.getServiceList(param);
		
		param.put("serviceType", 2);
		ArrayList<ServiceVo> service2List = professorDao.getServiceList(param);
		
		if (service1List != null && service2List != null) {
			
			setServiceData(service1List);
			setServiceData(service2List);
			
			result = new HashMap<String, Object>();
			result.put("service1List", service1List);
			result.put("service2List", service2List);
		}
		
		return result;
	}
	
	/**
	 * 화면 출력시 필요한 변수 계산하여 추가
	 * @param serviceList
	 */
	private void setServiceData(ArrayList<ServiceVo> serviceList) {
		
		int serviceRowSpan = 0;
		int detailRowSpan = 0;
		int noteRowSpan = 0;
		
		for (ServiceVo serviceVo : serviceList) {
			serviceVo.setServiceDetail1(validateStringValue(serviceVo.getServiceDetail1()));
			serviceVo.setServiceDetail2(validateStringValue(serviceVo.getServiceDetail2()));
			serviceVo.setNote(validateStringValue(serviceVo.getNote()));
			
			if (serviceVo.getSelectOption() != null && serviceVo.getSelectOption().trim().length() > 0) {
				serviceVo.setOptionList(serviceVo.getSelectOption().trim().split("\\|", -1));
			}
			
			if (serviceVo.getServiceRowSpan() > 0) {
				serviceRowSpan = serviceVo.getServiceRowSpan();
			}
			if (serviceVo.getDetailRowSpan() > 0) {
				detailRowSpan = serviceVo.getDetailRowSpan();
			}
			if (serviceVo.getNoteRowSpan() > 0) {
				noteRowSpan = serviceVo.getNoteRowSpan();
			}
			
			if (serviceVo.getServiceRowSpan() == 0 && serviceRowSpan > 0) {
				serviceVo.setServiceHide(true);
			}
			
			if (serviceVo.getDetailRowSpan() == 0 && (serviceVo.getServiceDetail1() == null || serviceVo.getServiceDetail1().trim().length() == 0)) {
				serviceVo.setDetailHide(true);
				if (detailRowSpan == 0) {
					serviceVo.setDetailColSpan(2);
				}
			}
			
			if (serviceVo.getNoteRowSpan() == 0 && noteRowSpan > 0) {
				serviceVo.setNoteHide(true);
			}
			
			if (serviceRowSpan > 0) {
				serviceRowSpan--;
			}
			if (detailRowSpan > 0) {
				detailRowSpan--;
			}
			if (noteRowSpan > 0) {
				noteRowSpan--;
			}
		}
	}
	
	private String validateStringValue(String stringValue) {
		if (stringValue != null) {
			stringValue = StringEscapeUtils.escapeHtml(stringValue.replace("\\n", "<br>").replace("\\r", "<br>"));
		}
		return stringValue;
	}

	@Override
	public void addService(HashMap<String, Object> param) {
		
		String serviceCodeList = "";
		
		for (String contentsText : (String[])param.get("contentsList")) {
			
			if (contentsText != null && contentsText.trim().length() > 0) {
				String[] contentsArr = contentsText.split("\\|", -1);
				
				if (contentsArr.length == 5) {
					
					if (serviceCodeList.length() > 0) {
						serviceCodeList += ",";
					}
					serviceCodeList += contentsArr[0];
					
					param.put("serviceCode", contentsArr[0]);
					param.put("serviceType", contentsArr[1]);
					param.put("startDate", contentsArr[2]);
					param.put("endDate", contentsArr[3]);
					param.put("contents", contentsArr[4]);
					param.put("point", 0);
					
					if (contentsArr[4].length() > 0) {
						professorDao.addService(param);
					}
					/*
					int serviceCount = professorDao.getServiceCount(param);
					
					if (contentsArr[4].length() > 0) {
						if (serviceCount > 0) {
							professorDao.updateService(param);
						} else {
							professorDao.addService(param);
						}
					}
					*/
				}
			}
		}
		
		param.put("serviceCodeList", serviceCodeList);
		professorDao.deleteService(param);
	}

	@Override
	public HashMap<String, Object> addSingleService(HashMap<String, Object> param) {
		
		return professorDao.addService(param);
	}

	@Override
	public HashMap<String, Object> deleteSingleService(HashMap<String, Object> param) {
		return professorDao.deleteSingleService(param);
	}

	@Override
	public HashMap<String, Object> uploadFile(HashMap<String, Object> param) {
		return professorDao.uploadFile(param);
	}

	@Override
	public void deleteFile(HashMap<String, Object> param) {
		professorDao.deleteFile(param);
	}

	@Override
	public ArrayList<ServiceFileVo> getFileList(HashMap<String, Object> param) {
		return professorDao.getFileList(param);
	}

	@Override
	public HashMap<String, Object> getServiceResult(HashMap<String, Object> param) {
		
		HashMap<String, Object> result = null;
		
		param.put("serviceType", 1);
		ArrayList<ServiceResultVo> serviceResult1 = professorDao.getServiceResult(param);
		
		param.put("serviceType", 2);
		ArrayList<ServiceResultVo> serviceResult2 = professorDao.getServiceResult(param);
		
		if (serviceResult1 != null && serviceResult2 != null) {
			
			setServiceResultData(serviceResult1);
			setServiceResultData(serviceResult2);
			
			result = new HashMap<String, Object>();
			result.put("serviceResult1", serviceResult1);
			result.put("serviceResult2", serviceResult2);
		}
		return result;
	}
	
	private void setServiceResultData(ArrayList<ServiceResultVo> serviceResultList) {
		for (ServiceResultVo serviceResultVo : serviceResultList) {
			
			if (serviceResultVo.getServiceCode() != 0 && serviceResultVo.getContents() != null && serviceResultVo.getContents().trim().length() > 0) {
				
				if (serviceResultVo.getStartDate() == null) {
					serviceResultVo.setStartDate("");
				}
				if (serviceResultVo.getEndDate() == null) {
					serviceResultVo.setEndDate("");
				}
			
				serviceResultVo.setContentsText(serviceResultVo.getServiceCode() + "|" + serviceResultVo.getServiceType() + "|" 
				+ serviceResultVo.getStartDate() + "|" + serviceResultVo.getEndDate() + "|" + serviceResultVo.getContents() + "|" + serviceResultVo.getPoint() + "|" + serviceResultVo.getDateType());
				
				String contentsViewText = "";
				
				if (serviceResultVo.getStartDate() != null && serviceResultVo.getStartDate().trim().length() > 0) {
					
					if (serviceResultVo.getDateType() == 4) {
						contentsViewText += serviceResultVo.getStartDate().substring(2).replaceAll("-", ".") + " " + serviceResultVo.getStartTime() + " ~ " + serviceResultVo.getEndTime();
					} else {
						contentsViewText += serviceResultVo.getStartDate().substring(2).replaceAll("-", ".");
						if (serviceResultVo.getEndDate() != null && serviceResultVo.getEndDate().trim().length() > 0) {
							contentsViewText +=  " ~ " + serviceResultVo.getEndDate().substring(2).replaceAll("-", ".");
						}
					}
				}
				
				if (serviceResultVo.getContents() != null && serviceResultVo.getContents().trim().length() > 0) {
					if (contentsViewText.length() > 0) {
						contentsViewText += "<br>";
					}
					contentsViewText += serviceResultVo.getContents();
				}
				
				if (contentsViewText.length() > 0) {
					serviceResultVo.setContentsViewText(contentsViewText);
				}
			}
		}
	}

	@Override
	public boolean copyAttachedFile() {
		ArrayList<ServiceFileVo> result = professorDao.getServiceFileList();
		for (ServiceFileVo serviceFileVo : result) {
			
			File inDir = new File(fileSystemResource.getPath() + "/upload");
			
			File outDir = new File(fileSystemResource.getPath() + "/result/" + serviceFileVo.getUserId() + "_" + serviceFileVo.getUserName());
			
			if (!outDir.exists()) {
				outDir.mkdirs();
			}
			
			File inFile = new File(inDir.getAbsolutePath() + "/" + serviceFileVo.getFileName());
			
			if (inFile.exists()) {
				Util.fileCopy(inFile.getAbsolutePath(), outDir.getAbsolutePath() + "/" + serviceFileVo.getOrignalName());
			}
		}
		return true;
	}

	@Override
	public List<Map<String, Object>> getNoticeList(Map<String,Object> param) {
		return professorDao.getNoticeList(param);
	}
	@Override
	public int getNoticeListTotalCount(Map<String,Object> param) {
		return professorDao.getNoticeListTotalCount(param);
	}
	@Override
	public Map<String,Object> getMyResultTotalPoint(Map<String,Object> param){
		return professorDao.getMyResultTotalPoint(param);
	}
	
	@Override
	public Map<String,Object> getMyResultStandareScore(Map<String,Object> param){
		return professorDao.getMyResultStandareScore(param);
	}

	@Override
	public  List<Map<String,Object>> getPeriodInfo(Map<String, Object> param) {
		return professorDao.getPeriodInfo(param);
	}
	
	@Override
	public HashMap<String,Object> getService(HashMap<String,Object> param){
		return professorDao.getService(param);
	}
	@Override
	public HashMap<String,Object> getPeriod(HashMap<String,Object> param){
		return professorDao.getPeriod(param);
	}
	
}
