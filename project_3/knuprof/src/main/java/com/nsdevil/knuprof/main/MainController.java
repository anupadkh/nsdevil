package com.nsdevil.knuprof.main;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nsdevil.knuprof.util.Util;

/**
 * Handles requests for the application home page.
 */
@Controller
public class MainController {
	
	private static final Logger logger = LoggerFactory.getLogger(MainController.class);
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(HttpServletRequest request, Locale locale, Model model) {
		return "redirect:/login";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(HttpServletRequest request, Locale locale, Model model) {
		
//		System.out.println(Util.getSHA256("1234"));
		
		String viewName = "login";
		
		if(request.getSession().getAttribute("userlevel") != null) {

			String userLevel = request.getSession().getAttribute("userlevel").toString();
			
			logger.debug("userLevel : {1}", userLevel);
			
			if (userLevel.equals("1")) {
				viewName = "redirect:/admin/";
			} else if (userLevel.equals("2")) {
				viewName = "redirect:/pro/notice";
			}
		}
		return viewName;
	}
	
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public String main(Locale locale, Model model) {
		return "main";
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request ,HttpServletResponse response) {
		
		HttpSession session = request.getSession();
		
		//세션 삭제
		session.removeAttribute("userCode");
		session.removeAttribute("adminId");
		session.removeAttribute("adminName");
		session.removeAttribute("userlevel");
		
		response.setDateHeader("Expires", 0);
		response.setHeader("Pragma", "no-cache");
		if (request.getProtocol().equals("HTTP/1.1")) {
			response.setHeader("Cache-Control", "no-cache");
		}
		return "redirect:/login";
	}
}
