package com.nsdevil.knuprof.util;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class SessionInterceptor extends HandlerInterceptorAdapter {
	
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
		
		String adminId = (String)request.getSession().getAttribute("adminId");
		
		//세션 변수가 없으면 로그인화면으로 이동
		if(null == adminId) {

			response.sendRedirect(request.getContextPath());

			return false;

		}
		
		return true;	
	}
}
