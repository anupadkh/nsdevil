package com.nsdevil.knuprof.admin.excel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ExcelAchieveEducationAdder {
	private final Logger logger = LoggerFactory.getLogger(ExcelAchieveEducationAdder.class);
	private final int SKIPPED_ROW_INDEX = 5;
	private String excelFilePath = null;
	public ExcelAchieveEducationAdder(String excelFilePath){
		this.excelFilePath = excelFilePath;
	}

	public List<Map<String,ExcelAchieveEducationVo>> getExcelAchieveList() {

		FileInputStream fis = null;
		XSSFWorkbook workbook = null;
		
		List<Map<String,ExcelAchieveEducationVo>> paramMapList = new ArrayList<Map<String,ExcelAchieveEducationVo>>();

		try {
			fis = new FileInputStream(excelFilePath);
			workbook = new XSSFWorkbook(fis);
			
			//logger.debug("workbook.getNumberOfSheets(); =>" + workbook.getNumberOfSheets());
			
			XSSFSheet sheet = workbook.getSheetAt(0);
			//logger.debug("sheet.getSheetName() =>"+ sheet.getSheetName());
			/*if("교육".equals(sheet.getSheetName())){
				logger.debug("sheet name is same!!!");
			}*/

			System.out.println("index =  "  + sheet.getPhysicalNumberOfRows());
			for (int rowIndex = 0; rowIndex < sheet.getPhysicalNumberOfRows(); rowIndex++) {
				if (rowIndex < SKIPPED_ROW_INDEX) {  // title  pass
					continue;
				}
				XSSFRow row = sheet.getRow(rowIndex);
				if (row.getCell(0) == null || "".equals(row.getCell(0).toString())) {
					//logger.debug("first Cell is Empty. skipped this row...");
					continue;
				}

				String userId = null;
				String userName = null;

				System.out.println("index =  "  + rowIndex);
				Map<String,ExcelAchieveEducationVo> paramMap = new HashMap<String,ExcelAchieveEducationVo>();
				
				for (int cellIndex = 0; cellIndex < row.getPhysicalNumberOfCells(); cellIndex++) {
					XSSFCell cell = row.getCell(cellIndex);
					if (cell == null) {
						//break;
					}
					if (cellIndex == 0 && "".equals(row.getCell(cellIndex).toString())) {
						break;
					}

					if (cellIndex == 0 && cell.getCellType() == XSSFCell.CELL_TYPE_BLANK) {
						break;
					}
					String stringValue = null;

					switch (cell.getCellType()) {
					case XSSFCell.CELL_TYPE_BOOLEAN:
						break;
					case XSSFCell.CELL_TYPE_NUMERIC:
						// stringValue = cell.getNumericCellValue()+"";
							stringValue = String.valueOf(new Double(cell.getNumericCellValue()));
						break;
					case XSSFCell.CELL_TYPE_STRING:
						stringValue = cell.getStringCellValue() + "";
						break;
					case XSSFCell.CELL_TYPE_FORMULA:
						stringValue = cell.getCellFormula();
						break;
					case XSSFCell.CELL_TYPE_BLANK:
						stringValue = cell.getBooleanCellValue() + "";
						break;
					case XSSFCell.CELL_TYPE_ERROR:
						stringValue = cell.getErrorCellValue() + "";
						break;
					default:
						stringValue = cell.getStringCellValue() + "";
						break;
					}
					
					switch (cellIndex) {
					case 0:
						//logger.debug("userId=>"+ stringValue);
						userId = stringValue;
						break;
					case 1:
						//logger.debug("userName=>"+ stringValue);
						userName = stringValue;
						break;
					}

					int educationCode = getCellSequence(cellIndex);
					//System.out.println("educationCode = " + educationCode);
					//logger.debug("educationCode :"+ educationCode + " / " + cellIndex);
					switch(cellIndex){
					//unit
					case 2: case 4: case 6: case 8: case 10: case 12: case 14: case 16: case 18: 	//강의시간
					case 20: 	//강의평가
					case 22: 	//시험관
					case 24: case 26: case 28: 	//문항및 교육모듈 개발
						if(stringValue != null && !"".equals(stringValue) && !"false".equals(stringValue)){
							ExcelAchieveEducationVo vo = paramMap.get(educationCode);
							if(vo == null){
								vo = new ExcelAchieveEducationVo(String.valueOf(educationCode));
								vo.setUnit(stringValue);
								vo.setUserId(userId);
								vo.setUserName(userName);
								paramMap.put(String.valueOf(educationCode), vo);
							}
						}
						break;
					//contents
					case 3: case 5: case 7: case 9: case 11: case 13: case 15: case 17: case 19:  //강의시간 
					case 21:  	//강의평가
					case 23:    //시험관
					case 25: case 27: case 29: 	//문항 및 교육모듈 개발
						if(stringValue != null && !"".equals(stringValue) && !"false".equals(stringValue)){
							ExcelAchieveEducationVo vo = paramMap.get(String.valueOf(educationCode));					
							if(vo == null){
								break;
							}
							vo.setContents(stringValue);
							paramMap.put(String.valueOf(educationCode), vo );
						} 
						break;
					default :
						break;
					}
					
				}
//				Iterator<String> keys = paramMap.keySet().iterator();
//				while( keys.hasNext() ){
//					String key = keys.next();
//					ExcelAchieveEducationVo vo = paramMap.get(key);
//					System.out.println(key + " =>" + paramMap.get(key) + "=>  "+ vo.getEducationCode() + " / " +  vo.getUserId() + " / "+ vo.getUserName() + " / " + " / " + vo.getContents() );
//				}
				paramMapList.add(paramMap);
			}
			System.out.println(paramMapList.size());
			for(int i = 0; i < paramMapList.size(); i++){
				Map<String,ExcelAchieveEducationVo> paramMap = paramMapList.get(i);
				Iterator<String> keys = paramMap.keySet().iterator();
				while( keys.hasNext() ){
					String key = keys.next();
					ExcelAchieveEducationVo vo = paramMap.get(key);
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (workbook != null)
					workbook.close();
				if (fis != null)
					fis.close();
			} catch (IOException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		

		return paramMapList;
	}
	
	int [][]cellSequence={
			{}
			,{2,3}
			,{4,5}
			,{6,7}
			,{8,9}
			,{10,11}
			,{12,13}
			,{14,15}
			,{16,17}
			,{18,19}
			,{20,21}
			,{22,23}
			,{24,25}
			,{26,27}
			,{28,29}
	};
	private int getCellSequence(int num){
		for(int i=0; i < cellSequence.length; i++){
			for(int j=0; j <cellSequence[i].length; j++){
				//System.out.print(sequence[i][j] + " " );
				if(num == cellSequence[i][j]){
					return i;
				}
			}
			System.out.println();;
		}
		return -1;
	}
	
		
	public static void main(String[]asrgs){
		String excelFilePath =  "d:/achieve_education.xlsx";
		ExcelAchieveEducationAdder excel =  new ExcelAchieveEducationAdder(excelFilePath);
		excel.getExcelAchieveList();
		String startDate = "20160101";
		//System.out.println(new ExcelAchieveEducationAdder(excelFilePath).getDateFormat(startDate,false));
		
	}
}
