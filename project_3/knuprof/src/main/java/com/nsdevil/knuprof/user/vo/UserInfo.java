package com.nsdevil.knuprof.user.vo;

import java.sql.Timestamp;

public class UserInfo {
	private int userCode;	
	private String userId;	
	private String userName;
	private int userLevel;	//1 : superAdmin 2 : Professor 3 : staff
	private String userPass;
	private String userPhone;
	private String userEmail;
	private String useFlag;		// 사용여부
	private Timestamp regDate;
	private String jobTitle;	//직급
	private String subject;		//과목
	private String profNum;		//교직원번호
	private String newPass;
	private int addition;
	private String stPoint;
	
	public int getAddition() {
		return addition;
	}
	public void setAddition(int addition) {
		this.addition = addition;
	}
	public String getStPoint() {
		return stPoint;
	}
	public void setStPoint(String stPoint) {
		this.stPoint = stPoint;
	}
	public String getNewPass() {
		return newPass;
	}
	public void setNewPass(String newPass) {
		this.newPass = newPass;
	}
	public int getUserCode() {
		return userCode;
	}
	public void setUserCode(int userCode) {
		this.userCode = userCode;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getUserLevel() {
		return userLevel;
	}
	public void setUserLevel(int userLevel) {
		this.userLevel = userLevel;
	}
	public String getUserPass() {
		return userPass;
	}
	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUseFlag() {
		return useFlag;
	}
	public void setUseFlag(String useFlag) {
		this.useFlag = useFlag;
	}
	public Timestamp getRegDate() {
		return regDate;
	}
	public void setRegDate(Timestamp regDate) {
		this.regDate = regDate;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getProfNum() {
		return profNum;
	}
	public void setProfNum(String profNum) {
		this.profNum = profNum;
	}
	
	
}
