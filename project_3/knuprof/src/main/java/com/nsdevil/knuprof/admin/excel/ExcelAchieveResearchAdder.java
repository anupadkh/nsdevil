package com.nsdevil.knuprof.admin.excel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ExcelAchieveResearchAdder {
	private final Logger logger = LoggerFactory.getLogger(ExcelAchieveResearchAdder.class);
	private final int SKIPPED_ROW_INDEX = 6;
	private String excelFilePath = null;
	public ExcelAchieveResearchAdder(String excelFilePath){
		this.excelFilePath = excelFilePath;
	}

	public List<Map<String,ExcelAchieveResearchVo>> getExcelAchieveList() {

		FileInputStream fis = null;
		XSSFWorkbook workbook = null;
		
		List<Map<String,ExcelAchieveResearchVo>> paramMapList = new ArrayList<Map<String,ExcelAchieveResearchVo>>();

		try {
			fis = new FileInputStream(excelFilePath);
			workbook = new XSSFWorkbook(fis);
			
			//logger.debug("workbook.getNumberOfSheets(); =>" + workbook.getNumberOfSheets());
			
			XSSFSheet sheet = workbook.getSheetAt(0);
			//logger.debug("sheet.getSheetName() =>"+ sheet.getSheetName());
			/*if("교육".equals(sheet.getSheetName())){
				logger.debug("sheet name is same!!!");
			}*/
			System.out.println("Row : " + sheet.getPhysicalNumberOfRows());
			for (int rowIndex = 0; rowIndex < sheet.getPhysicalNumberOfRows(); rowIndex++) {
				if (rowIndex < SKIPPED_ROW_INDEX) {  // title  pass
					continue;
				}
				XSSFRow row = sheet.getRow(rowIndex);
				if (row.getCell(0) == null || "".equals(row.getCell(0).toString())) {
					//logger.debug("first Cell is Empty. skipped this row...");
					continue;
				}

				String userId = null;
				String userName = null;

				Map<String,ExcelAchieveResearchVo> paramMap = new HashMap<String,ExcelAchieveResearchVo>();
				
				for (int cellIndex = 0; cellIndex < row.getPhysicalNumberOfCells(); cellIndex++) {
					XSSFCell cell = row.getCell(cellIndex);
					if (cell == null) {
						//break;
					}
					if (cellIndex == 0 && "".equals(row.getCell(cellIndex).toString())) {
						break;
					}

					if (cellIndex == 0 && cell.getCellType() == XSSFCell.CELL_TYPE_BLANK) {
						break;
					}
					String stringValue = "";
					
					switch (cell.getCellType()) {
					case XSSFCell.CELL_TYPE_BOOLEAN:
						break;
					case XSSFCell.CELL_TYPE_NUMERIC:
						// stringValue = cell.getNumericCellValue()+"";
						if(cellIndex == 3 || cellIndex == 5 || cellIndex == 7 || cellIndex == 9 || cellIndex == 11 || cellIndex == 13 || cellIndex == 15 || 
						cellIndex == 17 || cellIndex == 19 || cellIndex == 21 || cellIndex == 23 || cellIndex == 25 || cellIndex == 27){
							stringValue = String.valueOf(new Double(cell.getNumericCellValue()));
							System.out.println(" 내용 = " + stringValue);
						}else{
							stringValue = String.valueOf(new Double(cell.getNumericCellValue()).intValue());
							System.out.println(" 내용 = " + stringValue);							
						}						
						break;
					case XSSFCell.CELL_TYPE_STRING:
						stringValue = cell.getStringCellValue() + "";
						break;
					case XSSFCell.CELL_TYPE_FORMULA:
						stringValue = cell.getCellFormula();
						break;
					case XSSFCell.CELL_TYPE_BLANK:
						stringValue = cell.getBooleanCellValue() + "";
						break;
					case XSSFCell.CELL_TYPE_ERROR:
						stringValue = cell.getErrorCellValue() + "";
						break;
					default:
						stringValue = cell.getStringCellValue() + "";
						break;
					}
					switch (cellIndex) {
					case 0:
						//logger.debug("userId=>"+ stringValue);
						userId = stringValue;
						break;
					case 1:
						//logger.debug("userName=>"+ stringValue);
						userName = stringValue;
						break;
					}

					int researchItemNum = getCellSequence(cellIndex);
					System.out.println("researchItemNum :"+ researchItemNum + " / " + cellIndex + " / " + rowIndex);
					//logger.debug("researchItemNum :"+ researchItemNum + " / " + cellIndex);
					switch(cellIndex){
					//unit
					case 2: case 4: case 6: case 8: case 10: //논문
					case 12: case 14: //저서 교재 
					case 16: case 18: //역서 편저
					case 20: //기타
					case 22: //연구
					case 24: //활동
					case 26: //연구비 
						if(stringValue != null && !"".equals(stringValue) && !"false".equals(stringValue)){
							ExcelAchieveResearchVo vo = paramMap.get(researchItemNum);
							if(vo == null){
								vo = new ExcelAchieveResearchVo(String.valueOf(researchItemNum));
								vo.setUnit(stringValue);
								vo.setUserId(userId);
								vo.setUserName(userName);
								vo.setUseFlag("Y");
								paramMap.put(String.valueOf(researchItemNum), vo);
							}
						}
						break;
					//contents
					case 3: case 5: case 7: case 9: case 11: //논문 
					case 13: case 15: //저서 교재
					case 17: case 19: //역서 편저
					case 21:  	//기타
					case 23:    //연구
					case 25: //활동 
					case 27: //연구비
						if(stringValue != null && !"".equals(stringValue) && !"false".equals(stringValue)){
							ExcelAchieveResearchVo vo = paramMap.get(String.valueOf(researchItemNum));					
							if(vo == null){
								break;
							}
							vo.setContents(stringValue);
							paramMap.put(String.valueOf(researchItemNum), vo );
						} 
						break;
					default :
						break;
					}
					
				}
//				Iterator<String> keys = paramMap.keySet().iterator();
//				while( keys.hasNext() ){
//					String key = keys.next();
//					ExcelAchieveEducationVo vo = paramMap.get(key);
//					System.out.println(key + " =>" + paramMap.get(key) + "=>  "+ vo.getEducationCode() + " / " +  vo.getUserId() + " / "+ vo.getUserName() + " / " + " / " + vo.getContents() );
//				}
				paramMapList.add(paramMap);
			}
			
			System.out.println("paramMapList.add(paramMap); =>"+ paramMapList.size());
			for(int i = 0; i < paramMapList.size(); i++){
				Map<String,ExcelAchieveResearchVo> paramMap = paramMapList.get(i);
				Iterator<String> keys = paramMap.keySet().iterator();
				while( keys.hasNext() ){
					String key = keys.next();
					ExcelAchieveResearchVo vo = paramMap.get(key);
					System.out.println(key + " =>" + paramMap.get(key) + "=>  "+ vo.getResearchItemNum() + " / " +  vo.getUserId() + " / "+ vo.getUserName() + " / " + vo.getUnit() + " / " + vo.getContents() );
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try {
				if (workbook != null)
					workbook.close();
				if (fis != null)
					fis.close();
			} catch (IOException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		

		return paramMapList;
	}
	
	int [][]cellSequence={
			{}
			,{2,3}  //1
			,{4,5}
			,{6,7}
			,{8,9}
			,{10,11}
			,{12,13}
			,{14,15}
			,{16,17}
			,{18,19}
			,{20,21}
			,{22,23}
			,{24,25}
			,{26,27} //13
	};
	private int getCellSequence(int num){
		for(int i=0; i < cellSequence.length; i++){
			for(int j=0; j <cellSequence[i].length; j++){
				//System.out.print(sequence[i][j] + " " );
				if(num == cellSequence[i][j]){
					//System.out.println("sequece의 " + i +" 번째 인덱스");
					return i;
				}
			}
			System.out.println();;
		}
		return -1;
	}
	
		
	public static void main(String[]asrgs){
		String excelFilePath =  "d:/achieve_education.xlsx";
		ExcelAchieveResearchAdder excel =  new ExcelAchieveResearchAdder(excelFilePath);
		excel.getExcelAchieveList();
		String startDate = "20160101";
		//System.out.println(new ExcelAchieveEducationAdder(excelFilePath).getDateFormat(startDate,false));
		
	}
}
