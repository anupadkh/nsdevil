package com.nsdevil.knuprof.board.dao;

import java.util.List;
import java.util.Map;

import com.nsdevil.knuprof.board.vo.BoardAssignVo;

public interface BoardDao {
	public Map<String,Object> getNoticeDetail(int boardCode);
	public int addNoticeData(Map<String,Object> param);
	public int updateNotice(Map<String,Object> param);
	public List<Map<String,Object>> getBoardDetailList(int userCode);
	public List<Map<String,Object>> getAssignBoardDataByUser(int userCode);
	
	public int getBoardListTotalCount(Map<String,Object> param);
	public List<Map<String,Object>> getBoardList(Map<String,Object> param);
	
	public int addBoardQna(Map<String, Object> param);
	public int updateBoardQna(Map<String,Object> param);
	
	public List<Integer> getBoardCodeListByGroupId(int groupId);
	
	//public List<Map<String,Object>> getAssignBoardDataByBoardCode(int boardCode);
	public BoardAssignVo getAssignBoardDataByBoardCode(int boardCode);
	public BoardAssignVo getAssignNoticeDataByBoardCode(int boardCode);
	
	public int getFirstBoardNoticeCode();
	public int getLastBoardQnaCode();
}
