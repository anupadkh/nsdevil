package com.nsdevil.knuprof.prof;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nsdevil.knuprof.prof.service.ProfessorService;

import com.nsdevil.knuprof.util.JSONUtil;
import com.nsdevil.knuprof.util.PageSplitter;

import net.sf.json.JSONObject;

@Controller
public class ProfessorController {
	private static final Logger logger = LoggerFactory.getLogger(ProfessorController.class);
	@Autowired
	private ProfessorService professorService; 
	
	@RequestMapping(value = "/pro/notice", method = RequestMethod.GET)
	public String notice(Locale locale, Model model) {
		return "pro/notice";
	}
	
	@RequestMapping(value = "/pro/info", method = RequestMethod.GET)
	public String info(Locale locale, Model model) {
		return "pro/info";
	}
	
	@RequestMapping(value = "/pro/result", method = RequestMethod.GET)
	public String result(HttpServletRequest request,Locale locale, Model model,HttpSession session) {

		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("userCode", session.getAttribute("userCode").toString());
		
		HashMap<String, Object> result = professorService.getServiceList(param);
		
		model.addAttribute("data", result);
		return "pro/result";
	}
	
	@RequestMapping(value = "/pro/service", method = RequestMethod.GET)
	public String login(HttpServletRequest request, Locale locale, Model model) {
		
		HttpSession session = request.getSession();
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("userCode", session.getAttribute("userCode").toString());
		
		HashMap<String, Object> result = professorService.getServiceList(param);
		
		logger.debug("result ==================");
		logger.debug(""+result);
		model.addAttribute("data", result);
		
		return "pro/service";
	}
	
	@RequestMapping(value = "/pro/board", method = RequestMethod.GET)
	public String board(Locale locale, Model model) {
		return "pro/board";
	}
	
	
	
	// My 메뉴의 알림리스트를 가져오기위한 메소드
	@RequestMapping(value = "/pro/getNoticeList", method = RequestMethod.POST)
	@ResponseBody
	public String getNoticeList(HttpServletRequest request,HttpSession session, @RequestBody String jsonString) {
		int userCode = (Integer) session.getAttribute("userCode");
		JSONUtil jsonUtil = new JSONUtil(jsonString);
		
		int pageNo = Integer.parseInt(jsonUtil.getString("pageNo", "1"));
		int rowCount = Integer.parseInt(jsonUtil.getString("rowCount", "0"));
		
		logger.debug("userCode =>"+ userCode);
		JSONObject json = new JSONObject();
		json.accumulate("status", "success");
		
		if(!(userCode > 0)){
			json.accumulate("status", "fail");
			json.accumulate("message", "통신상의 오류가 발생하였습니다.");
			return json.toString();
		}
		
		int pagePerRowCount = rowCount > 0 ? rowCount : PageSplitter.PAGE_PER_ROW_COUNT;
		int startRowCount = (pageNo -1) * pagePerRowCount;
		
		logger.debug("pagePerRowCount =>"+ pagePerRowCount);
		logger.debug("startRowCount =>"+ startRowCount);
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("startRowCount", (startRowCount));
		map.put("pagePerRowCount", (pagePerRowCount));
		map.put("userCode", userCode);
		
		logger.debug("startRowCount =>"+ startRowCount  + "  / pagePerRowCount =>"+ pagePerRowCount);
		
		int getNoticeListTotalCount = professorService.getNoticeListTotalCount(map);
		logger.debug(" getNoticeListTotalCount =>"+ getNoticeListTotalCount);
		if(getNoticeListTotalCount > 0){
			List<Map<String,Object>> noticeList = professorService.getNoticeList(map);
			PageSplitter pageSplitter = new PageSplitter(noticeList, getNoticeListTotalCount, pageNo, pagePerRowCount); 
			
			json.accumulate("list", pageSplitter.getNumberingResultList());
			json.accumulate("pagingInfo", pageSplitter.getPageNavigationInfo());
		}
		
		return json.toString();
	}
	
	@RequestMapping(value = "/pro/getMyResultTotalPoint", method = RequestMethod.POST)
	@ResponseBody
	public String getMyResultTotalPoint(HttpServletRequest request,HttpSession session, @RequestBody String jsonString) {
		int userCode = (Integer) session.getAttribute("userCode");
		String userId = session.getAttribute("adminId").toString();
		logger.debug("userCode =>"+ userCode);
		JSONObject json = new JSONObject();
		json.accumulate("status", "success");
		
		if(!(userCode > 0)){
			json.accumulate("status", "fail");
			json.accumulate("message", "통신상의 오류가 발생하였습니다.");
			return json.toString();
		}
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("userCode", userCode);
		map.put("user_id", userId);
		
		Map<String,Object> getMyResultTotalPoint = professorService.getMyResultTotalPoint(map);
		Map<String,Object> getMyResultStandareScore = professorService.getMyResultStandareScore(map);
		logger.debug(" getMyResultTotalPoint =>"+ getMyResultTotalPoint);
		if(getMyResultTotalPoint != null ){
			
			json.accumulate("myResultTotalPoint", getMyResultTotalPoint);
			json.accumulate("myResultStandareScore", getMyResultStandareScore);
		}
		
		return json.toString();
	}
	@RequestMapping(value = "/pro/getPeriodInfo", method = RequestMethod.POST)
	@ResponseBody
	public String getPeriodInfo(HttpServletRequest request,HttpSession session, @RequestBody String jsonString) {
		int userCode = (Integer) session.getAttribute("userCode");
		logger.debug("userCode =>"+ userCode);
		JSONObject json = new JSONObject();
		json.accumulate("status", "success");
		if(!(userCode > 0)){
			json.accumulate("status", "fail");
			json.accumulate("message", "통신상의 오류가 발생하였습니다.");
			return json.toString();
		}
		Map<String,Object> map = new HashMap<String,Object>();
		//map.put("userCode", userCode);
		List<Map<String,Object>> getPeriodInfo = professorService.getPeriodInfo(map);
		logger.debug(" getPeriodInfo =>"+ getPeriodInfo);
		if(getPeriodInfo != null ){
			json.accumulate("periodInfo", getPeriodInfo);
		}
		System.out.println(json.toString());
		return json.toString();
	}
}
