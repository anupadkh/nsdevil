package com.nsdevil.knuprof.board.service;

import java.util.List;
import java.util.Map;


public interface BoardService {
	public int addNoticeData(Map<String,Object> param);
	public int updateNotice(Map<String,Object> param);
	public int getBoardListTotalCount(Map<String,Object> param);
	public List<Map<String,Object>> getBoardList(Map<String,Object> param);
	public Map<String,Object> getBoardDetailInfo(int userCode, String boardType,int boardCode);
	public int addBoardQna(Map<String,Object> param);
	int updateBoardQna(Map<String,Object> param);
}
