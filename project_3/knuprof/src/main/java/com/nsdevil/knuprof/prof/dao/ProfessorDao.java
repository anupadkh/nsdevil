package com.nsdevil.knuprof.prof.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nsdevil.knuprof.prof.domain.ServiceFileVo;
import com.nsdevil.knuprof.prof.domain.ServiceResultVo;
import com.nsdevil.knuprof.prof.domain.ServiceVo;

public interface ProfessorDao {
	public ArrayList<ServiceVo> getServiceList(HashMap<String, Object> param);
	public HashMap<String, Object> addService(HashMap<String, Object> param);
	public void deleteService(HashMap<String, Object> param);
	public HashMap<String, Object> deleteSingleService(HashMap<String, Object> param);
	public void updateService(HashMap<String, Object> param);
	public int getServiceCount(HashMap<String, Object> param);
	public HashMap<String, Object> uploadFile(HashMap<String, Object> param);
	public void deleteFile(HashMap<String, Object> param);
	public ArrayList<ServiceFileVo> getFileList(HashMap<String, Object> param);
	public ArrayList<ServiceResultVo> getServiceResult(HashMap<String, Object> param);
	public ArrayList<ServiceFileVo> getServiceFileList();
	
	public List<Map<String,Object>> getNoticeList(Map<String,Object> param);
	public int getNoticeListTotalCount(Map<String,Object> param);
	
	public Map<String,Object> getMyResultTotalPoint(Map<String,Object> param);
	public List<Map<String,Object>> getPeriodInfo(Map<String,Object> param);
	public HashMap<String, Object> getService(HashMap<String, Object> param);
	public HashMap<String, Object> getPeriod(HashMap<String, Object> param);
	public Map<String, Object> getMyResultStandareScore(Map<String, Object> param);
	
}
