package com.nsdevil.knuprof.admin.excel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ExcelAchieveServiceAdder {
	private final Logger logger = LoggerFactory.getLogger(ExcelAchieveServiceAdder.class);
	private String excelFilePath = null;
	private final int SKIPPED_ROW_INDEX = 7;
	public ExcelAchieveServiceAdder(String excelFilePath){
		this.excelFilePath = excelFilePath;
	}

	public List<Map<String,ExcelAchieveServiceVo>> getServiceList() {

		FileInputStream fis = null;
		XSSFWorkbook workbook = null;
		
		List<Map<String,ExcelAchieveServiceVo>> paramMapList = new ArrayList<Map<String,ExcelAchieveServiceVo>>();

		try {
			fis = new FileInputStream(excelFilePath);
			workbook = new XSSFWorkbook(fis);

			logger.debug("workbook.getNumberOfSheets(); =>" + workbook.getNumberOfSheets());
			XSSFSheet sheet = workbook.getSheetAt(0);
			
			for (int rowIndex = 0; rowIndex < sheet.getPhysicalNumberOfRows(); rowIndex++) {
				if (rowIndex < SKIPPED_ROW_INDEX) {  // title  pass
					continue;
				}
				XSSFRow row = sheet.getRow(rowIndex);
				if (row.getCell(0) == null || "".equals(row.getCell(0).toString())) {
					//logger.debug("first Cell is Empty. skipped this row...");
					continue;
				}

				String userId = null;
				String userName = null;

				Map<String,ExcelAchieveServiceVo> paramMap = new HashMap<String,ExcelAchieveServiceVo>();
				
				for (int cellIndex = 0; cellIndex < row.getPhysicalNumberOfCells(); cellIndex++) {
					XSSFCell cell = row.getCell(cellIndex);
					if (cell == null) {
						//break;
					}
					if (cellIndex == 0 && "".equals(row.getCell(cellIndex).toString())) {
						break;
					}

					if (cellIndex == 0 && cell.getCellType() == XSSFCell.CELL_TYPE_BLANK) {
						break;
					}
					String stringValue = null;
					
					switch (cell.getCellType()) {
					case XSSFCell.CELL_TYPE_BOOLEAN:
						break;
					case XSSFCell.CELL_TYPE_NUMERIC:
						if(cellIndex == 2|| cellIndex == 3 || cellIndex == 5 || cellIndex == 6 || cellIndex == 8 || cellIndex == 9 || cellIndex == 11 || cellIndex == 12 ||
							 cellIndex == 14 || cellIndex == 15 || cellIndex == 17 || cellIndex == 18 || cellIndex == 20 || cellIndex == 21 || cellIndex == 23 ||
							 cellIndex == 24 || cellIndex == 26  || cellIndex == 27 || cellIndex == 29 || cellIndex == 30 || cellIndex == 32 || cellIndex == 33 ||
							 cellIndex == 35 || cellIndex == 36 || cellIndex == 38 || cellIndex == 39 || cellIndex == 41 || cellIndex == 42 || cellIndex == 44 ||
							 cellIndex == 45 || cellIndex == 47 || cellIndex == 48 || cellIndex == 50 || cellIndex == 51 || cellIndex == 53 || cellIndex == 54 ||
							 cellIndex == 59 || cellIndex == 60 || cellIndex == 62 || cellIndex == 63 || cellIndex == 65 || cellIndex == 66 || cellIndex == 68 ||
							 cellIndex == 69 || cellIndex == 71 || cellIndex == 72 || cellIndex == 74 || cellIndex == 76 || cellIndex == 77)
							stringValue = String.valueOf(new Double(cell.getNumericCellValue()).intValue());
						else
							stringValue = String.valueOf(new Double(cell.getNumericCellValue()));
						break;
					case XSSFCell.CELL_TYPE_STRING:
						stringValue = cell.getStringCellValue() + "";
						break;
					case XSSFCell.CELL_TYPE_FORMULA:
						stringValue = cell.getCellFormula();
						break;
					case XSSFCell.CELL_TYPE_BLANK:
						stringValue = cell.getBooleanCellValue() + "";
						break;
					case XSSFCell.CELL_TYPE_ERROR:
						stringValue = cell.getErrorCellValue() + "";
						break;
					default:
						stringValue = cell.getStringCellValue() + "";
						break;
					}

					if(cellIndex == 13)
						System.out.println("점수 " + stringValue);
					switch (cellIndex) {
					case 0:
						logger.debug("userId=>"+ stringValue);
						userId = stringValue;
						break;
					case 1:
						logger.debug("userName=>"+ stringValue);
						userName = stringValue;
						break;
					}

					int serviceCode = getCellSequence(cellIndex);
					switch (cellIndex) {
					
					//startDate
					case 2: case 5: case 8: case 11: case 14: case 17: case 20:
					case 23: case 26: case 29: case 32: case 35: case 38:
					case 41: case 44:
					case 47: case 50: case 53:
					case 59: case 62:  //학술봉사
					case 65:   //의료봉사
					case 68: case 71: case 74: case 76: //기타 [전공관련국제기구,정부기관 기관,특별강연, 동창회의사회]

						logger.debug("startDate=>"+ stringValue);
						if(stringValue != null && !"".equals(stringValue) && !"false".equals(stringValue)){
							ExcelAchieveServiceVo vo = paramMap.get(serviceCode);
							if(vo == null){
								vo = new ExcelAchieveServiceVo(String.valueOf(serviceCode));
								vo.setStartDate(getDateFormat(stringValue,true));
								vo.setUserId(userId);
								vo.setUserName(userName);
								paramMap.put(String.valueOf(serviceCode), vo );
							}
						}
						break;
						//endDate
					case 3: case 6: case 9: case 12: case 15: case 18: case 21: 
					case 24: case 27: case 30: case 33: case 36: case 39:
					case 42: case 45:
					case 48: case 51: case 54:
					case 60: case 63:   // 학술봉사
					case 66: //의료봉사
					case 69: case 72: //전공관련 국제기구
					case 77:  // 동창회의
						logger.debug("endDate=>"+ stringValue);
						if(stringValue != null && !"".equals(stringValue)  && !"false".equals(stringValue)){
							ExcelAchieveServiceVo vo =  paramMap.get(String.valueOf(serviceCode));
							if(vo == null){  //시작날짜가 입력안되었슴. 패스함
								break;
							}else{
								vo.setEndDate(getDateFormat(stringValue,false));
								paramMap.put(String.valueOf(serviceCode), vo);
							}
						}
						break;
						//endDate항목
					case 4: case 7: case 10: case 13: case 16: case 19: case 22:
					case 25: case 28: case 31: case 34: case 37: case 40:
					case 43: case 46: case 49: case 52: case 55:
					case 75: //특별강연 -endDate가없슴.
						logger.debug("contents=>"+ stringValue);
						if(stringValue != null && !"".equals(stringValue)  &&  !"false".equals(stringValue)){
							ExcelAchieveServiceVo vo =  paramMap.get(String.valueOf(serviceCode));
							if(vo == null){  //시작날짜가 입력안되었슴. 패스함
								break;
							}else{
								vo.setContents(stringValue);
								paramMap.put(String.valueOf(serviceCode), vo);
							}
						}
						break;

						//날짜가 아예 없는 항목
					case 56: case 57: case 58:  //학교발전기여도
					case 79: case 80: case 81:  //수상실적
						if(stringValue != null && !"".equals(stringValue)  &&  !"false".equals(stringValue)){
							if(stringValue != null && !"".equals(stringValue) && !"false".equals(stringValue)){
								ExcelAchieveServiceVo vo = paramMap.get(serviceCode);
								System.out.println("vo =>"+ vo);
								if(vo == null){
									vo = new ExcelAchieveServiceVo(String.valueOf(serviceCode));
									vo.setUserId(userId);
									vo.setUserName(userName);
									vo.setContents(stringValue);
									paramMap.put(String.valueOf(serviceCode), vo );
								}
							}
						}
					default:
						break;
					}		
				
				}
				//paramMap.keySet();
				Iterator<String> keys = paramMap.keySet().iterator();
				while( keys.hasNext() ){
					String key = keys.next();
					ExcelAchieveServiceVo vo = paramMap.get(key);
					System.out.println(key + " =>" + paramMap.get(key) + "=>  "+ vo.getServiceCode() + " / " +  vo.getUserId() + " / "+ vo.getUserName() + " / " + vo.getStartDate() + " / " + vo.getEndDate() + " / " + vo.getContents() );
				}
				paramMapList.add(paramMap);
			}
			
			System.out.println("paramMapList.add(paramMap); =>"+ paramMapList.size());
			for(int i = 0; i < paramMapList.size(); i++){
				Map<String,ExcelAchieveServiceVo> paramMap = paramMapList.get(i);
				Iterator<String> keys = paramMap.keySet().iterator();
				while( keys.hasNext() ){
					String key = keys.next();
					ExcelAchieveServiceVo vo = paramMap.get(key);
					System.out.println(key + " =>" + paramMap.get(key) + "=>  "+ vo.getServiceCode() + " / " +  vo.getUserId() + " / "+ vo.getUserName() + " / " + vo.getStartDate() + " / " + vo.getEndDate() + " / " + vo.getContents() );
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (workbook != null)
					workbook.close();
				if (fis != null)
					fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		

		return paramMapList;
	}
	
	int [][]cellSequence={
			{2,3,4}
			,{5,6,7}
			,{8,9,10}
			,{11,12,13}
			,{14,15,16}
			,{17,18,19}
			,{20,21,22}
			,{23,24,25}
			,{26,27,28}
			,{29,30,31}
			,{32,33,34}
			,{35,36,37}
			,{38,39,40}
			,{41,42,43}
			,{44,45,46}
			,{47,48,49}
			,{50,51,52}
			,{53,54,55}
			,{56}
			,{57}
			,{58}
			,{59,60,61}
			,{62,63,64}
			,{65,66,67}
			,{68,69,70}
			,{71,72,73}
			,{74,75}
			,{76,77,78}
			,{79}
			,{80}
			,{81}
	};
	private String getDateFormat(String date,boolean isStart){
		if(date != null & date.length() == 8){
			StringBuffer sb = new StringBuffer(date.substring(0,4));
			sb.append("-").append(date.substring(4,6));
			sb.append("-").append(date.substring(6,8));
			sb.append(isStart ? " 00:00:00" : " 23:59:59");
			return sb.toString();
		}
		return null;
	}

	private int getCellSequence(int num){
		for(int i=0; i < cellSequence.length; i++){
			for(int j=0; j <cellSequence[i].length; j++){
				//System.out.print(sequence[i][j] + " " );
				if(num == cellSequence[i][j]){
					System.out.println("sequece의 " + i +" 번째 인덱스");
					return i+1;
				}
			}
			System.out.println();;
		}
		return -1;
	}
	
	public static void main(String[]asrgs){
		String excelFilePath =  "d:/achieve_service.xlsx";
//		ExcelAchieveServiceAdder excel =  new ExcelAchieveServiceAdder(excelFilePath);
//		excel.getServiceList();
		String startDate = "20160101";
		System.out.println(new ExcelAchieveServiceAdder(excelFilePath).getDateFormat(startDate,false));
		
	}
}
