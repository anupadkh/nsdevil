package com.nsdevil.knuprof.prof.domain;

public class ServiceFileVo {
	
	private int serviceFileCode;
	private int serviceCode;
	private int userCode;
	private String userId;
	private String userName;
	private String fileName;
	private String orignalName;
	private String regDate;
	
	public int getServiceFileCode() {
		return serviceFileCode;
	}
	
	public void setServiceFileCode(int serviceFileCode) {
		this.serviceFileCode = serviceFileCode;
	}
	
	public int getServiceCode() {
		return serviceCode;
	}
	
	public void setServiceCode(int serviceCode) {
		this.serviceCode = serviceCode;
	}
	
	public int getUserCode() {
		return userCode;
	}
	
	public void setUserCode(int userCode) {
		this.userCode = userCode;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFileName() {
		return fileName;
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getOrignalName() {
		return orignalName;
	}
	
	public void setOrignalName(String orignalName) {
		this.orignalName = orignalName;
	}
	
	public String getRegDate() {
		return regDate;
	}
	
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
}