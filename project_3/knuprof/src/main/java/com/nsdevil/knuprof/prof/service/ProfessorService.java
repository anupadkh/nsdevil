package com.nsdevil.knuprof.prof.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nsdevil.knuprof.prof.domain.ServiceFileVo;

public interface ProfessorService {
	public HashMap<String, Object> getServiceList(HashMap<String, Object> param);
	public void addService(HashMap<String, Object> param);
	public HashMap<String, Object> addSingleService(HashMap<String, Object> param);
	public HashMap<String, Object> deleteSingleService(HashMap<String, Object> param);
	public HashMap<String, Object> uploadFile(HashMap<String, Object> param);
	public void deleteFile(HashMap<String, Object> param);
	public ArrayList<ServiceFileVo> getFileList(HashMap<String, Object> param);
	public HashMap<String, Object> getServiceResult(HashMap<String, Object> param);
	public boolean copyAttachedFile();
	
	
	public List<Map<String,Object>> getNoticeList(Map<String,Object> param);
	public int getNoticeListTotalCount(Map<String,Object> param);
	
	public Map<String,Object> getMyResultTotalPoint(Map<String,Object> param);
	public  List<Map<String,Object>> getPeriodInfo(Map<String,Object> param);
	public HashMap<String, Object> getService(HashMap<String, Object> param);
	public HashMap<String, Object> getPeriod(HashMap<String, Object> param);
	public Map<String, Object> getMyResultStandareScore(Map<String, Object> map);
	
	
	
}
