package com.nsdevil.knuprof.board.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nsdevil.knuprof.board.dao.BoardDao;
import com.nsdevil.knuprof.board.vo.BoardAssignVo;

@Service
public class BoardServiceImpl implements BoardService {
	private final Logger logger = LoggerFactory.getLogger(BoardServiceImpl.class);

	@Autowired
	BoardDao boardDao;
	
	@Override
	public int addNoticeData(Map<String, Object> param) {
		return boardDao.addNoticeData(param);
	}
	@Override
	public int updateNotice(Map<String, Object> param) {
		return boardDao.updateNotice(param);
	}


	@Override
	public List<Map<String, Object>> getBoardList(Map<String,Object> param) {
		return boardDao.getBoardList(param);
	}

	@Override
	public int getBoardListTotalCount(Map<String, Object> param) {
		return boardDao.getBoardListTotalCount(param);
	}

	@Override
	@Transactional(readOnly = true)
	public Map<String, Object> getBoardDetailInfo(int userCode, String boardType, int boardCode) {
		Map<String,Object> map = new HashMap<String, Object>();
		if("notice".equals(boardType)){
			map.put("notice", boardDao.getNoticeDetail(boardCode));
			
			BoardAssignVo vo = boardDao.getAssignNoticeDataByBoardCode(boardCode);
			logger.debug("notice vo.getNextBoardCode() :"+ vo.getNextBoardCode());
			logger.debug("notice vo.getPrevBoardCode() :"+ vo.getPrevBoardCode());
			Map<String,Object> assign = new HashMap<String, Object>();
			assign.put("nextBoardCode", vo.getNextBoardCode());
			assign.put("nextBoardType", "notice");
			assign.put("prevBoardCode", vo.getPrevBoardCode());
			assign.put("prevBoardType", "notice");
			if(vo.getPrevBoardCode() == 0){
				assign.put("prevBoardCode", boardDao.getLastBoardQnaCode());
				assign.put("prevBoardType", "qna");
			}
			map.put("assignInfo", assign);
			
		}else if("qna".equals(boardType)){
			map.put("qna", boardDao.getBoardDetailList(boardCode));
			
			BoardAssignVo vo = boardDao.getAssignBoardDataByBoardCode(boardCode);
			logger.debug("qna vo.getNextBoardCode() :"+ vo.getNextBoardCode());
			logger.debug("qna vo.getPrevBoardCode() :"+ vo.getPrevBoardCode());
			Map<String,Object> assign = new HashMap<String, Object>();
			assign.put("nextBoardCode", vo.getNextBoardCode());
			assign.put("nextBoardType", "qna");
			assign.put("prevBoardCode", vo.getPrevBoardCode());
			assign.put("prevBoardType", "qna");
			
			if(vo.getNextBoardCode() == 0){
				assign.put("nextBoardCode", boardDao.getFirstBoardNoticeCode());
				assign.put("nextBoardType", "notice");
			}
			
			map.put("assignInfo", assign);
		}
		//map.put("assignInfo", boardDao.getAssignBoardDataByUser(userCode));
		//map.put("assignInfo", boardDao.getAssignBoardDataByBoardCode(boardCode));
		return map;
	}

	@Override
	@Transactional
	public int addBoardQna(Map<String, Object> param) {
		int result = boardDao.addBoardQna(param);
		if(param.get("groupId") != null && param.get("answered") != null){
			logger.debug("!!!!!!!!!!!!!!!!!! =>"+ param.get("groupId"));
			logger.debug("!!!!!!!!!!!!!!!!!! =>"+ param.get("answered"));
			
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("answered", param.get("answered"));
			map.put("groupId", param.get("groupId"));
			int updateBoardQna = boardDao.updateBoardQna(map);
			logger.debug("updateBoardQna :"+ updateBoardQna);
		}
		return result;
	}

	@Transactional
	@Override
	public int updateBoardQna(Map<String, Object> param) {
		int groupId = Integer.parseInt(param.get("groupId") == null ? "-1" : (String) param.get("groupId"));
		int replyOrder = Integer.parseInt(param.get("replyOrder") == null ? "-1" : (String) param.get("replyOrder"));
		String useFlag = (String) param.get("use_flag");
		if(replyOrder == -1 && groupId > 0 && "N".equals(useFlag)){
			List<Integer> list = boardDao.getBoardCodeListByGroupId(groupId);
			logger.debug("list .size =>"+ list.size());
//			for(int i=0; i < list.size(); i++){
//				boardDao.updateBoardQna(param);
//			}
			return boardDao.updateBoardQna(param);
		}else{
			
			return boardDao.updateBoardQna(param);
		}
	}

	
}
