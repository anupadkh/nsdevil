package com.nsdevil.knuprof.user;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nsdevil.knuprof.util.Util;

public class ReadExcelUserAdder {
	private final Logger logger = LoggerFactory.getLogger(ReadExcelUserAdder.class);
	private String excelFilePath = null;
	public ReadExcelUserAdder(String excelFilePath){
		this.excelFilePath = excelFilePath;
	}

	public List<Map<String, Object>> getProfessorList() {
		List<Map<String, Object>> userList = new ArrayList<Map<String, Object>>();

		FileInputStream fis = null;
		XSSFWorkbook workbook = null;

		try {
			fis = new FileInputStream(excelFilePath);
			workbook = new XSSFWorkbook(fis);

			logger.debug("workbook.getNumberOfSheets(); =>" + workbook.getNumberOfSheets());

			// for(int sheetIndex=0; sheetIndex< workbook.getNumberOfSheets();
			// sheetIndex++){
			// XSSFSheet sheet = workbook.getSheetAt(sheetIndex);
			// }
			XSSFSheet sheet = workbook.getSheetAt(0);
			for (int rowIndex = 0; rowIndex < sheet.getPhysicalNumberOfRows(); rowIndex++) {
				if (rowIndex < 3) {  // title 은지나고..
					continue;
				}
				XSSFRow row = sheet.getRow(rowIndex);
				if (row.getCell(0) == null || "".equals(row.getCell(0).toString())) {
					//logger.debug("first Cell is Empty. skipped this row...");
					continue;
				}

				Map<String, Object> userInfo = new HashMap<String, Object>();

				for (int cellIndex = 0; cellIndex < row.getPhysicalNumberOfCells(); cellIndex++) {
					XSSFCell cell = row.getCell(cellIndex);
					if (cell == null) {
						break;
					}
					if (cellIndex == 0 && "".equals(row.getCell(cellIndex).toString())) {
						break;
					}

					if (cellIndex == 0 && cell.getCellType() == XSSFCell.CELL_TYPE_BLANK) {
						break;
					}
					String stringValue = null;
					
					switch (cell.getCellType()) {
					case XSSFCell.CELL_TYPE_BOOLEAN:
						break;
					case XSSFCell.CELL_TYPE_NUMERIC:
						// stringValue = cell.getNumericCellValue()+"";
						stringValue = String.valueOf(new Double(cell.getNumericCellValue()).intValue());
						break;
					case XSSFCell.CELL_TYPE_STRING:
						stringValue = cell.getStringCellValue() + "";
						break;
					case XSSFCell.CELL_TYPE_FORMULA:
						stringValue = cell.getCellFormula();
						break;
					case XSSFCell.CELL_TYPE_BLANK:
						stringValue = cell.getBooleanCellValue() + "";
						break;
					case XSSFCell.CELL_TYPE_ERROR:
						stringValue = cell.getErrorCellValue() + "";
						break;
					default:
						stringValue = new String();
						break;
					}

					switch (cellIndex) {
					case 0:
						userInfo.put("userId", stringValue);
						break;
					case 1:
						userInfo.put("userPass", Util.getSHA256(stringValue));
						break;
					case 2:
						userInfo.put("userName", stringValue);
						break;
					case 3:
						userInfo.put("jobTitle", stringValue);
						break;
					case 4:
						userInfo.put("subject", stringValue);
						break;
					case 5:
						userInfo.put("profNum", stringValue);
						break;
					case 6:
						int addition = -1;
						if(stringValue.startsWith("겸직")){
							addition = Integer.parseInt(stringValue.replace("겸직",""));
							userInfo.put("addition", stringValue);
						}else if(stringValue.startsWith("기금")){
							addition = Integer.parseInt(stringValue.replace("기금","")) + 6;
						}
						userInfo.put("addition", addition);
						break;
					case 7:
						if(stringValue.startsWith("대상자")){
							stringValue = "Y";
						}
						stringValue = "N";
						userInfo.put("stPoint", stringValue);
						break;
					case 8:
						if(stringValue.length() == 10){
							stringValue = new StringBuilder("0")
									.append(stringValue.substring(0, 2))
									.append("-").append(stringValue.substring(2, 6))
									.append("-").append(stringValue.substring(6, 10)).toString();
									
						}else if(stringValue.length() == 9){
							stringValue = new StringBuilder("0")
									.append(stringValue.substring(0, 2))
									.append("-").append(stringValue.substring(2, 5))
									.append("-").append(stringValue.substring(5, 9)).toString();
						}
						userInfo.put("telNum", stringValue);
						break;
					case 9:
						userInfo.put("email", stringValue);
					default:
						break;
					}

					System.out.print(stringValue);
				}
				userInfo.put("userLevel", "2");
				userList.add(userInfo);
				System.out.println();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (workbook != null)
					workbook.close();
				if (fis != null)
					fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		logger.debug("userList.size() =>"+ userList.size());
		return userList;
	}
	
	
	public List<Map<String, Object>> getStaffList() {
		List<Map<String, Object>> userList = new ArrayList<Map<String, Object>>();

		FileInputStream fis = null;
		XSSFWorkbook workbook = null;

		try {
			fis = new FileInputStream(excelFilePath);
			workbook = new XSSFWorkbook(fis);

			String [] categoryIds = {
					"ed01","ed02","ed03","ed04"
					,"re01","re02","re03","re04","re05","re06"
					,"si01","si02","si03","si04"
					,"so01","so02","so03","so04"
					};
			
			XSSFSheet sheet = workbook.getSheetAt(0);
			for (int rowIndex = 0; rowIndex < sheet.getPhysicalNumberOfRows(); rowIndex++) {
				if (rowIndex < 5) {
					continue;
				}
				
				XSSFRow row = sheet.getRow(rowIndex);

				if (row.getCell(0) == null || "".equals(row.getCell(0).toString())) {
					//logger.debug("first Cell is Empty. skipped this row...");
					continue;
				}
				Map<String, Object> userInfo = new HashMap<String, Object>();
				List<String> categoryIdArray = new ArrayList<String>();
				for (int cellIndex = 0; cellIndex < row.getPhysicalNumberOfCells(); cellIndex++) {
					XSSFCell cell = row.getCell(cellIndex);
					if (cell == null) {
						break;
					}
					if (cellIndex == 0 && "".equals(row.getCell(cellIndex).toString())) {
						break;
					}
					if (cellIndex == 0 && cell.getCellType() == XSSFCell.CELL_TYPE_BLANK) {
						break;
					}
					String stringValue = null;
					switch (cell.getCellType()) {
					case XSSFCell.CELL_TYPE_BOOLEAN:
						break;
					case XSSFCell.CELL_TYPE_NUMERIC:
						stringValue = String.valueOf(new Double(cell.getNumericCellValue()).intValue());
						break;
					case XSSFCell.CELL_TYPE_STRING:
						stringValue = cell.getStringCellValue() + "";
						break;
					case XSSFCell.CELL_TYPE_FORMULA:
						stringValue = cell.getCellFormula();
						break;
					case XSSFCell.CELL_TYPE_BLANK:
						stringValue = cell.getBooleanCellValue() + "";
						break;
					case XSSFCell.CELL_TYPE_ERROR:
						stringValue = cell.getErrorCellValue() + "";
						break;
					default:
						stringValue = new String();
						break;
					}
					
					if(cellIndex  == 0){
						userInfo.put("userId", stringValue);
					}else if(cellIndex == 1){
						userInfo.put("userPass", Util.getSHA256(stringValue));
					}else if(cellIndex == 2){
						userInfo.put("userName", stringValue);
					}else if(cellIndex > 2 && cellIndex < 21){
						if("y".equals(stringValue.toLowerCase())){
							System.out.print("  " + categoryIds[cellIndex - 3]  +  "  " );
							categoryIdArray.add(categoryIds[cellIndex - 3] );
							userInfo.put("categoryIds",categoryIdArray);
						}
					}
					System.out.print(stringValue);
				}
				userInfo.put("userLevel",1);
				userList.add(userInfo);
				logger.debug("\ncategoryIds =>"+ userInfo.get("categoryIds").toString());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (workbook != null)
					workbook.close();
				if (fis != null)
					fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		logger.debug("userList.size() = " + userList.size());
		for (int i = 0; i < userList.size(); i++) {
			Map map = userList.get(i);
			   Iterator<Object> iterator = map.keySet().iterator();
			    while (iterator.hasNext()) {
			        String key = (String) iterator.next();
			        System.out.print("\t" + map.get(key));
			    }
			    logger.debug("");
		}
		return userList;
	}
	public static void main(String[]asrgs){
		String stringValue = "106578599";
		
		System.out.println(stringValue);
		
	}
}
