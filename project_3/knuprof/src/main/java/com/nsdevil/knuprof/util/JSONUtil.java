package com.nsdevil.knuprof.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class JSONUtil {
	private final Log log = LogFactory.getLog(JSONUtil.class);
	
	private net.sf.json.JSONObject jsonObject = null;
	
	public JSONUtil(String jsonString) {
		jsonObject = net.sf.json.JSONObject.fromObject(jsonString);
	}

	public String getString(String key, String defaultValue) {
		String value = defaultValue;
		try {
			value = jsonObject.has(key) ? jsonObject.getString(key) : defaultValue;
			if (value != null) {
				value = (value.equals("null") || value.length() == 0) ? defaultValue : value;
			}else{
				log.debug("Can't find key \""+ key + "\"");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return value;
	}
	public List<String> getStringList(String key){
		JSONArray jsonArray = getJSONArray(key);
		if(jsonArray != null && jsonArray.size() > 0){
			List<String> list = new ArrayList<String>();
			for (int i=0; i<jsonArray.size(); i++) {
				list.add( jsonArray.getString(i) );
			}
			return list;
		}
		return null;
	}
	
	public JSONArray getJSONArray(String key){
        JSONArray jsonArray = null;
        try {
            jsonArray = jsonObject.has(key)? jsonObject.getJSONArray(key) : null;
            //jsonArray.toArray();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }
	
	public int getArrayLength(){
		int i =0;
		Iterator<String> keys = jsonObject.keys();
		while( keys.hasNext() ){
			String keyName = keys.next();
			//System.out.println("key =>"+ keyName);
			i++;
		}
		return i;
	}	
	public List<JSONObject> getJsonObjectList(){
		List<JSONObject> list = null;
		Iterator<String> keys = jsonObject.keys();
		while( keys.hasNext() ){
			String keyName = keys.next();
			//System.out.println("keyName =>"+ keyName);
	        try {
	        	JSONObject object = jsonObject.getJSONObject(keyName);
	        	if(list == null){
	        		list = new ArrayList<JSONObject>();
	        	}
	        	list.add(object);
	        } catch (JSONException e) {
	        	e.printStackTrace();
		    }
		}
		return list;
	}
}