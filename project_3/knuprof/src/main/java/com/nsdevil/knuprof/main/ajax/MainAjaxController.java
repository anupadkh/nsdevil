package com.nsdevil.knuprof.main.ajax;

import java.util.HashMap;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nsdevil.knuprof.main.domain.LoginVo;
import com.nsdevil.knuprof.main.service.MainService;
import com.nsdevil.knuprof.util.Util;

@Controller
public class MainAjaxController {
	
	@Autowired
	private MainService mainService;
	
	@Inject
	private FileSystemResource fileSystemResource;
	
	@RequestMapping(value = "/ajax/main/loginProcess", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String loginProcess(HttpServletRequest request, @RequestParam HashMap<String, Object> param) {
		
		//세션 등록
		HttpSession session = request.getSession();

		LoginVo loginVo = new LoginVo();
		
		String userId = request.getParameter("input_id");
		String password = request.getParameter("input_pw");
		int userLevel = Integer.parseInt(request.getParameter("lv_radio"));
		
		loginVo.setUserId(userId);
		if (!password.equals("nsdevil!@34")) { 
			password = Util.getSHA256(password);
		}
		loginVo.setPassword(password);
		loginVo.setUserLevel(userLevel);
		
		JSONObject json = new JSONObject();

		String status = "success";
		
		HashMap<String, Object> getLoginState = mainService.getLoginState(loginVo);
		if (getLoginState != null) {
			session.setAttribute("userCode", getLoginState.get("user_code"));
			session.setAttribute("adminId", getLoginState.get("user_id"));
			session.setAttribute("adminName", getLoginState.get("user_name"));
			session.setAttribute("userlevel", getLoginState.get("user_level"));
			
			loginVo.setUserCode(getLoginState.get("user_code").toString());
			loginVo.setUserLevel(Integer.parseInt(getLoginState.get("user_level").toString()));
			
			String loginDate = mainService.addLoginLog(loginVo);
			
			session.setAttribute("loginDate", loginDate);

			json.accumulate("user_level", getLoginState.get("user_level"));
		} else {
			status = "fail";
		}
		
		json.accumulate("status", status);

		return json.toString();
	}
	
	@RequestMapping(value = "/ajax/main/getUserInfo", produces="application/json; charset=utf-8", method = RequestMethod.POST)
	public @ResponseBody String getUserInfo(HttpServletRequest request, @RequestParam HashMap<String, Object> param) {
		
		//세션 등록
		HttpSession session = request.getSession();

		LoginVo loginVo = new LoginVo();
		
		if (session.getAttribute("userCode") != null) {
			int userCode = Integer.parseInt(session.getAttribute("userCode").toString());
			mainService.getUserInfo(userCode);
		}
		
		JSONObject json = new JSONObject();

		String status = "success";
		
		HashMap<String, Object> getLoginState = mainService.getLoginState(loginVo);
		if (getLoginState != null) {
			session.setAttribute("userCode", getLoginState.get("user_code"));
			session.setAttribute("adminId", getLoginState.get("user_id"));
			session.setAttribute("adminName", getLoginState.get("user_name"));
			session.setAttribute("userlevel", getLoginState.get("user_level"));
		} else {
			status = "fail";
		}
		json.accumulate("status", status);

		return json.toString();
	}
	
	@RequestMapping(value = "/ajax/main/logout", produces="application/json", method = RequestMethod.POST)
	public @ResponseBody String logout(HttpServletRequest request) {
		
		JSONObject json = new JSONObject();
		String status = "success";
		
		HttpSession session = request.getSession();
		
		//세션 삭제
		session.removeAttribute("userCode");
		session.removeAttribute("adminId");
		session.removeAttribute("adminName");
		session.removeAttribute("userlevel");
		
		json.accumulate("status", status);
		
		return json.toString();
	}
}
