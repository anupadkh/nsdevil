package com.nsdevil.knuprof.prof.domain;

public class ServiceResultVo {
	
	private int serviceResultCode;
	private int serviceCode;
	private int userCode;
	private String userName;
	private String startDate;
	private String startTime;
	private String endDate;
	private String endTime;
	private String contents;
	private String userPoint;
	private String contentsText;
	private String contentsViewText;
	private int fileCount;
	private double point;
	private int serviceType;
	private int dateType;
	
	public int getServiceResultCode() {
		return serviceResultCode;
	}
	
	public void setServiceResultCode(int serviceResultCode) {
		this.serviceResultCode = serviceResultCode;
	}
	
	public int getServiceCode() {
		return serviceCode;
	}
	
	public void setServiceCode(int serviceCode) {
		this.serviceCode = serviceCode;
	}
	
	public int getUserCode() {
		return userCode;
	}
	
	public void setUserCode(int userCode) {
		this.userCode = userCode;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getContents() {
		return contents;
	}
	
	public void setContents(String contents) {
		this.contents = contents;
	}
	
	public String getUserPoint() {
		return userPoint;
	}
	
	public void setUserPoint(String userPoint) {
		this.userPoint = userPoint;
	}
	
	public String getContentsText() {
		return contentsText;
	}
	
	public void setContentsText(String contentsText) {
		this.contentsText = contentsText;
	}
	
	public String getContentsViewText() {
		return contentsViewText;
	}
	
	public void setContentsViewText(String contentsViewText) {
		this.contentsViewText = contentsViewText;
	}
	
	public int getFileCount() {
		return fileCount;
	}
	
	public void setFileCount(int fileCount) {
		this.fileCount = fileCount;
	}
	
	public double getPoint() {
		return point;
	}
	
	public void setPoint(double point) {
		this.point = point;
	}
	
	public int getServiceType() {
		return serviceType;
	}
	
	public void setServiceType(int serviceType) {
		this.serviceType = serviceType;
	}

	public int getDateType() {
		return dateType;
	}

	public void setDateType(int dateType) {
		this.dateType = dateType;
	}
}