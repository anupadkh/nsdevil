package com.nsdevil.knuprof.main.dao;

import java.util.HashMap;

import com.nsdevil.knuprof.main.domain.LoginVo;

public interface MainDao {
	public void loginState(HashMap<String, Object> param);
	public HashMap<String, Object> getLoginState(LoginVo loginVo);
	public LoginVo getUserInfo(int userCode);
	public String addLoginLog(LoginVo loginVo);
}
