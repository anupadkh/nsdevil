package com.nsdevil.knuprof.util;

import java.awt.Image;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Util {
	
	/**
	 * QueryString을 Map으로 변환하여 리턴
	 * @param query
	 * @return
	 */
	public static Map<String, String> getQueryMap(String query) {
		String[] params = query.split("&");
		Map<String, String> map = new HashMap<String, String>();
		for (String param : params) {
			String name = param.split("=")[0];
			String value = param.split("=")[1];
			map.put(name, value);
		}
		return map;
	}
	
	/**
	 * Map을 QueryString으로 변환
	 * @param map
	 * @return
	 */
	public static String mapToQueryString(Map<String, String> map) {
		StringBuilder string = new StringBuilder();
		
		for(Entry<String, String> entry : map.entrySet()) {
			if (string.length() > 0) {
				string.append("&");
			}
			string.append(entry.getKey());
			string.append("=");
			string.append(entry.getValue());
		}

		return string.toString();
	}
	
	/**
	 * 문자열이 숫자형인지 체크
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {
		try {
			Double.parseDouble(str);  
		} catch(NumberFormatException e) {  
			return false;
		}
		return true;  
	}
	
	/**
	 * 폴더 삭제
	 * @param targetFolder
	 */
	public static void deleteFolder(File targetFolder) {
		if(targetFolder.isDirectory()){
			File[] targetFolderFiles = targetFolder.listFiles();
			for (File file : targetFolderFiles) {
				if(file.isDirectory()){
					deleteFolder(file);
					
				}else{
					file.delete();
				}
			}
			targetFolder.delete();
		}
	}
	
	/**
	 * 파일 생성
	 * @param inFileName
	 * @param outFileName
	 */
	public static void createFile(String fileName, String contents) {
		try {
            BufferedWriter fw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName, false), "UTF-8"));

            fw.write(contents);
            fw.flush();

            fw.close(); 
        }catch(Exception e){
            e.printStackTrace();
        }
	}
	
	/**
	 * 파일 복사
	 * @param inFileName
	 * @param outFileName
	 */
	public static void fileCopy(String inFileName, String outFileName) {
		try {
			FileInputStream fis = new FileInputStream(inFileName);
			FileOutputStream fos = new FileOutputStream(outFileName);

			byte[] buffer = new byte[1024];

			int length;

			while ((length = fis.read(buffer)) > 0) {
				fos.write(buffer, 0, length);
			}

			fis.close();
			fos.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 파일 삭제
	 * @param targetFile
	 */
	public static void fileDelete(String targetFile) {
		File deleteFile = new File(targetFile);
		deleteFile.delete();
	}
	
	/**
	 * MD5 비밀번호 암호화
	 * @param input
	 * @return
	 */
	public static String md5(String input) {
		String md5 = null;

		if (null == input) return null;

		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(input.getBytes(), 0, input.length());
			md5 = new BigInteger(1, digest.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			md5 = input;
		}
		return md5;
	}
	
	/**
	 * SHA256 비밀번호 암호화
	 * @param input
	 * @return
	 */
	public static String getSHA256(String input) {
		String SHA = "";
		
		if (null == input) return null;
		
		try {
			MessageDigest sh = MessageDigest.getInstance("SHA-256"); 
			sh.update(input.getBytes()); 
			byte byteData[] = sh.digest();
			StringBuffer sb = new StringBuffer(); 
			for(int i = 0 ; i < byteData.length ; i++){
				sb.append(Integer.toString((byteData[i]&0xff) + 0x100, 16).substring(1));
			}
			SHA = sb.toString();
			
		} catch(NoSuchAlgorithmException e) {
			e.printStackTrace(); 
			SHA = null; 
		}
		return SHA;
	}
	
	/**
	 * 현재시간
	 * @param input
	 * @return
	 */
	public static String getCurrentDate() {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
	
		String currentDate = dateFormat.format(calendar.getTime());
		
		return currentDate;
	}
	
	/**
	 * 이미지 크기 변경
	 * @param soruce, width, height
	 * @return
	 */
	 public static Image resizing(Image soruce, int width, int height) throws Exception {
		 
		 int newW = width;
		 int newH = height;

		 return soruce.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
	 }
	 
	/**
	 * 랜덤 파일명 생성
	 * @param uploadDir
	 * @param fileName
	 * @return
	 */
	public static String getUploadFileName(String uploadDir, String fileName) {
		
		String realName = null;
		String milName = "";
		String fileExt = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length());
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		long milliSecond = System.currentTimeMillis();
		milName = String.valueOf(milliSecond).substring( 10 ,13);
		
    	realName = (sdf.format(new java.util.Date()) + milName  ) + "."+fileExt;
    	
		
    	File ckfile = new File(uploadDir + "/" + realName);
    	
		int i = 0;
		while(ckfile.exists()) {
			i++;
			realName = ( (sdf.format(new java.util.Date()) + milName) ) + i  + "."+fileExt;
			ckfile = new File(uploadDir + "/" + realName);
		}
		return realName;
	}
}
