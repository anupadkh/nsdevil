function setPagingData(json){
	console.debug(json);
	
	var contents = "<ul>";
	
	if(typeof json == "undefined" || json == null){
		 
         contents += "<li><a style=\"cursor:default\" href=\"javascript:void(0)\"><img src=\"images/ppre_img_off.png\" alt=\"처음\"/></a></li>";
         contents += "<li><a style=\"cursor:default\" href=\"javascript:void(0)\"><img src=\"images/pre_img_off.png\" alt=\"이전\" /></a></li>";
         contents += "<li><a style=\"cursor:default\" href=\"javascript:void(0)\" class=\"active\">1</a></li>";
         contents += "<li><a style=\"cursor:default\" href=\"javascript:void(0)\"><img src=\"images/next_img_off.png\" alt=\"맨끝\"/></a></li>";
         contents += "<li><a style=\"cursor:default\" href=\"javascript:void(0)\"><img src=\"images/nnext_img_off.png\" alt=\"다음\" /></a></li>";
         contents += "</ul>";
         $("#pagingLayer").html(contents);
         return;
	}
	
    if(json.startNo == json.prevPageNo && json.startNo == json.currentPageNo){
    	contents += "<li><a style=\"cursor:default\" href=\"javascript:void(0);\"><img src=\"images/ppre_img_off.png\" alt=\"처음\" title=\"처음\"/></a></li>";	
    }else{
    	contents += "<li><a href=\"javascript:getPageList("+ 1 + ")\"><img src=\"images/ppre_img.png\" alt=\"처음\"  title=\"처음\"/></a></li>";
    }
    if(json.startNo == json.prevPageNo){
    	contents += "<li><a style=\"cursor:default\" href=\"javascript:void(0);\"><img src=\"images/pre_img_off.png\" alt=\"이전\" /></a></li>";	
    }else{
    	contents += "<li><a href=\"javascript:getPageList("+ json.prevPageNo + ")\"><img src=\"images/pre_img.png\" alt=\"이전\" /></a></li>";
    }
    
	for(var i = json.startNo; i <= json.endNo; i++ ){
		console.debug(i);
		if(json.currentPageNo == i){
			contents += "<li><a style=\"cursor:default\" href=\"javascript:void(0);\" class=\"active\">" + i + "</a></li>";	
		}else{
			contents += "<li><a href=\"javascript:getPageList(" + i + ")\">" + i + "</a></li>";
		}
	}
	if(json.totalPageCount == json.endNo || json.endNo == json.nextPageNo){
    	contents += "<li><a style=\"cursor:default\" href=\"javascript:void(0);\"><img src=\"images/next_img_off.png\" alt=\"다음\" /></a></li>";	
    }else{
    	contents += "<li><a href=\"javascript:getPageList("+ json.nextPageNo + ")\"><img src=\"images/next_img.png\" alt=\"다음\" /></a></li>";
    }
    
	if(json.totalPageCount == json.endNo && json.currentPageNo == json.endNo){
    	contents += "<li><a style=\"cursor:default\" href=\"javascript:void(0);\"><img src=\"images/nnext_img_off.png\" alt=\"맨끝\"  title=\"맨끝\"/></a></li>";
    }else{
    	contents += "<li><a href=\"javascript:getPageList("+ json.totalPageCount + ")\"><img src=\"images/nnext_img.png\" alt=\"맨끝\"  title=\"맨끝\"/></a></li>";
    }
	
	contents += "</ul>";
	
	$("#pagingLayer").html(contents);	
}