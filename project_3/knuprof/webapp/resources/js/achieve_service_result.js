
function updateAchieve(){
	
	if(currentRequestUserCode == undefined || currentRequestUserCode == null || currentRequestUserCode == 0){
		return;
	}
	var form = $("#updateAchieve");
	$("#updateAchieve input[name='requestUserCode']").val(currentRequestUserCode);
	form.submit();
}


function getMyResultTotalPoint() {
	$.ajax({
        type: "POST"
        ,url: "../pro/getMyResultTotalPoint"
        ,contentType : 'application/json; charset=UTF-8'
        ,data: {}
        ,dataType: "json"
        ,global : false
        ,cache: false
        ,async: false
        ,success: function(json){
           console.debug(json);
           if(json != undefined && json != null){
        	   if(json.status == "success"){
        		   /*$("#summary_ranking").html(json.myResultTotalPoint.ranking);*/
        		   $("#summary_sum_total").html(json.myResultTotalPoint.totalpoint);
        		   $("#summary_sum_service").html(json.myResultTotalPoint.totalpoint);
        		   $("#summary_user_name").html(json.myResultTotalPoint.username);
        		   
        	   }else{
        		   alert("오류가 발생했습니다.");
        	   }
           }
           return;
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
            alert("오류가 발생했습니다.");
        }
        ,complete: function() {
        	
        }
    });
}

function setServiceResult(json, userCode){
	$("[id^='serviceCode_count_']").html("");
	$("[id^='serviceCode_content_']").html("");
	$("[id^='serviceCode_file_']").html("");
	$("[id^='serviceCode_point_']").html("");
	
	var j = 0;
	for(var i=0; i < json.length; i++){
		var data = json[i];
//		console.debug(data);
		
		var countId = "#serviceCode_count_" + data.serviceType + "_"+ data.serviceCode;
		var contentId = "#serviceCode_content_" + data.serviceType + "_"+ data.serviceCode;
		var fileId = "#serviceCode_file_" + data.serviceType + "_" + data.serviceCode;
		var pointId = "#serviceCode_point_" + data.serviceType + "_" + data.serviceCode;
		
//		console.debug(countId);
//		console.debug(contentId);
//		console.debug(fileId);
//		console.debug(pointId);
		
		setServiceResult_count(countId, 1);
		setServiceResult_content(contentId, data.contentsViewText);
		setServiceResult_file(fileId, data.fileCount ,data.serviceCode, userCode);
		setServiceResult_point(pointId,data.point);
	}
	$(".service-table-content-div").show();
	
	
	if(userLevel == 1){
		
		$(".total-score-tr").hide();
		$("#totalPointTitleLayer").hide();
		$("#totalPointLayer").hide();
		hideUnPermissionLayer();
	}else{
		var point1 = setServiceCode_sum("serviceCode_point_1");
		var point2 = setServiceCode_sum("serviceCode_point_2");
		$("#serviceCode_point_total").html((point1 + point2) + "점");
	}
	
}
function setServiceCode_sum(id){
	var serviceCode_sum = 0;
	$("[id^='"+ id + "']").each(function(index){
		console.debug($(this).html().trim());
		var html = $(this).html().trim();
		var pointed = 0;
     	if(html == ""){
     		pointed = 0;
		}else{
			pointed = parseInt(html);
		}
     	serviceCode_sum = serviceCode_sum + pointed;
		
	});
	$("#" + id + "_sum").html(serviceCode_sum + "점");
	$("#" + id + "_sum_sum").html(serviceCode_sum + "점");
	return serviceCode_sum;
}

function setServiceResult_count(id, point){
	var html = $(id).html().trim();
	var pointed = 0;
	if(html == ""){
		pointed = 0;
	}else{
		pointed = parseInt(html);
	}
	
	pointed = pointed + point;
	
//	console.debug(pointed);
	$(id).html(pointed);
}
function setServiceResult_content(contentId, contentsViewText){
	var html_content = "";
	html_content += "<div class=\"service-table-content-div\">";
	html_content += contentsViewText;
	html_content += "</div>";

//	console.debug("setServiceResult_content =>" + contentId + "   / "+ contentsViewText);
	$(contentId).append(html_content);
}
function setServiceResult_file(fileId, fileCount, serviceCode,userCode){
	if(fileCount == 0){
		return;
	}

	var html = $(fileId).html().trim();
	var html_content = "";
	if(html == ""){
		html_content += "<p>";
		html_content += "<span class=\"text-bold\"><a>" + fileCount + "건</a></span>";
		html_content += "</p>";
		html_content += "<a class=\"btn-down\" onClick=\"getFileList(" + serviceCode + ","+ userCode +")\">다운로드</a>";
	}else{
		return;
	}
	$(fileId).html(html_content);
}
function setServiceResult_point(pointId, point){
	var pointedHtml = $(pointId).html().trim();
	var pointed = 0;
	if(pointedHtml == ""){
		pointed = 0;
	}else{
		pointed = parseInt(pointedHtml);
	}
	
	pointed = pointed + point;
	
//	console.debug(pointed);
	var pointedHtml = $(pointId).html(pointed);
}



/**
 * 파일목록 출력
 */
function getFileList(serviceCode, userCode) {
	
	currentUsercode = currentRequestUserCode;
	if(userCode != undefined && userCode != null){
		currentUsercode = userCode;
	}
	$.ajax({
        type: "POST",
        url: "./getFileList",
        data: {
        	"serviceCode": serviceCode
        	,"userCode" : currentUsercode
        },
        dataType: "json",
        success: function(data, status) {
        	if (data.status == "success") {
        		console.debug(data);
        		$("#uploadFileList").empty();
        		$.each(data.list, function(index) {
        			
		
        			var fileHtml = "<div class=\"popup-input-del-div\">"
    					+ "<input type=\"hidden\" name=\"serviceFileCode\" value=\"" + this.serviceFileCode + "\" />"
    					+ "<input type=\"hidden\" name=\"uploadFileName\" value=\"" + this.fileName + "\" />"
    					+ "<p>" + this.regDate + "</p>"
    					+ "<p><a href=\"../download?fileName=" + this.fileName + "&orignalName=" + encodeURI(this.orignalName) + "\">" + this.orignalName + "</a></p>"
    	                + "<p><input type=\"text\" value=\"" + this.orignalName + "\" readonly /></p>"
    	            + "</div>";
    				$("#uploadFileList").append(fileHtml);
        		});
        		
        		$("#uploadFileLayer").fadeIn(300);
        	} else {
        		alert("오류가 발생했습니다.");
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        }
    });
}

