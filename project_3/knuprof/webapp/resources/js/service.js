
/**
 * 봉사 항목 취득
 */
function getServiceList() {
	$.ajax({
        type: "POST",
        url: "../ajax/service/getServiceList",
        data: {
        },
        dataType: "json",
        success: function(data, status) {
        	
        	if (data.status == "success") {
        		
        		$.datetimepicker.setLocale('kr');
        	    $('.datepicker-input').datetimepicker({
        	        timepicker:false,
        	        datepicker:true,
        	        //format:'y.m.d H:i',
        	        //formatDate:'y.m.d H:i'
        	        format:'y.m.d',
        	        formatDate:'y.m.d'
        	    });
        	    $('.timepicker-input').datetimepicker({
        	        timepicker:true,
        	        datepicker:false,
        	        //format:'y.m.d H:i',
        	        //formatDate:'y.m.d H:i'
        	        format:'H:i',
        	        formatDate:'H:i',
        	        step:10
        	    });
        	    $('.btn-monthpick').bind('click',function(){
        	        $(this).siblings('input').datetimepicker('show');
        	    });
        	    // 증빙서류 등록 팝업
        	    $(".btn-service-popup").bind("click", function(){
        	        $(".wrap-plan-popup").fadeIn(300);
        	    });
        	    $(".btn-close-giude-popup").bind("click", function(){
        	        $(".wrap-plan-popup").fadeOut(300);
        	    });
        	    // input text 파일명 표시
        	    function fileName(){
        	        $(".input-upload").each(function(){
        	            $(this).change(function(){
        	                if( $(this).val() != "" ){
        	                    var value = $(this).val();
        	                    var chk = $(this).val().split('.').pop().toLowerCase();
        	                    /*
        	                      if($.inArray(chk, ["gif","png","jpg","jpeg","mp4","ppt","pptx","pdf"]) == -1) {
        	                        alert("해당 파일은 첨부할 수 없습니다.");
        	                        $(this).val("");
        	                        return true;
        	                      } else {
        	                          $(this).parents("label").parent("p").siblings().children("input[type='text']").val(value);
        	                      }*/
        	                    $(this).parents("label").parent("p").siblings().children("input[type='text']").val(value);
        	                }
        	            });
        	        });
        	    };fileName();
        	    // 파일 등록폼 추가
        	    $(".wrap-popup").on("click",".btn-add-file", function(){
        	        $(".btn-add-file").before('<div class="popup-input-file-div"><p><input type="text" value="" readonly /></p><p><label class="btn-input-file-label">등록<input type="file" class="input-upload" value="" /></label></p></div>');
        	        fileName();
        	    });
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        }
    });
}

/**
 * 사용자 정보 취득
 */
function getServiceResult(disableMode) {
	$.ajax({
        type: "POST",
        url: "../ajax/service/getServiceResult",
        data: {
        },
        dataType: "json",
        success: function(data, status) {
        	if (data.status == "success") {
        		printServiceResult("#service1List", data.serviceResult1);
        		printServiceResult("#service2List", data.serviceResult2);
        		
        		updateTotalPoint();
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        }
        ,complete : function(){
        	if(disableMode != null ){
        		//if(disableMode == "B"){
        			alert("현재 등록기간이 아닙니다,\n등록기간이외에는 수정이 불가능합니다.");
        			disableElements();
        		//}
        	}
        }
    });
}

function printServiceResult(elementId, resultList) {
	$(elementId + " .contentsVIew").empty();
	$(elementId + " .userPoint").html("0");
	$.each(resultList, function() {
		
		var html = "";
		
		var trObj = $(elementId + " :input[name='serviceCode'][value='" + this.serviceCode + "']").parents("tr");
		if (this.contentsViewText != "") {
			var contentsDiv = trObj.find(".contentsVIew");
			
			html = "<div class=\"service-table-content-div\">"
				+ "<input type=\"hidden\" name=\"service_result_code\" value=\"" + this.serviceResultCode + "\" />"
	        	+ "<input type=\"hidden\" name=\"contents_text\" value=\"" + this.contentsText + "\" />"
	    		+ "<div class=\"contents_text\">" + this.contentsViewText + "</div>"
	            + "<label><button type=\"button\" class=\"btn-del-content\" onClick=\"javascript:deleteContents($(this));\">X</button></label>"
	            + "</div>";
			
			contentsDiv.append(html);
			contentsDiv.find(".service-table-content-div").show();
		}
		if (this.fileCount > 0) {
			html = "<p>등록된 서류 :"
                + "<span class=\"text-bold\">"
                + "<b>" + this.fileCount + "건</b>"
                + "</span>"
                + "</p>";
			
			trObj.find(".fileCountDiv").html(html);
		}
		addPoint(trObj, this.point);
	});
}

function addPoint(trObj, point) {
	var pointDiv = trObj.find(".userPoint");
	
	var prevPoint = pointDiv.html();
	 
	if (prevPoint == "") {
		prevPoint = 0;
	}
	
	point = parseFloat(prevPoint) + parseFloat(point);
	
	pointDiv.html(ToFloat(point));
}

function getTotalPoint(elementId) {
	
	var totalPoint = 0;
	
	$(elementId + " .userPoint").each(function() {
		var point = $(this).html();
		if (point == "") {
			point = 0;
		}
		totalPoint += parseFloat(point);
	});
	return totalPoint;
}

function updateTotalPoint() {
	
	var totalPoint1 = getTotalPoint("#service1List");
	var totalPoint2 = getTotalPoint("#service2List");
	$("#inTotalPoint1").html(ToFloat(totalPoint1));
	$("#inTotalPoint2").html(ToFloat(totalPoint1));
	$("#outTotalPoint1").html(ToFloat(totalPoint2));
	$("#outTotalPoint").html(ToFloat(totalPoint2));
	
	var finalPoint = totalPoint1 + totalPoint2;
	
	$("#finalPoint").html(ToFloat(finalPoint));
}



/**
 * 보직선택
 */
function selectJobTitle(obj) {
	var inputObj = obj.parent().parent().find(".directInput");
	if (obj.val() == "직접입력") {
		inputObj.addClass("show");
	} else {
		inputObj.removeClass("show");
		inputObj.find(":input[name='direct_input']").val(null);
	}
}

/**
 * 봉사내용 입력
 */
function addContents(obj) {
	var trObj = obj.parents("tr");
	
	var dateType = trObj.find(":input[name='date_type']").val();
	var jobTitleSel = trObj.find(":input[name='job_title_sel']").val();
	var directInput = trObj.find(":input[name='direct_input']").val();
	var directInputEnabled = trObj.find(":input[name='direct_input_enabled']").val();
	var startDate = "";
	var endDate = "";
	var startTime = "";
	var endTime = "";
	var jobTitle = "";
	
	var contents = "";
	
	if (dateType != 0) {
		startDate = trObj.find(":input[name='start_date']").val();
		
		if (startDate != "") {
			contents += startDate;
		} else {
			if (dateType == 1) {
				alert("시작일을 등록해주세요.");
			} else {
				alert("날짜를 등록해주세요.");
			}
			return;
		}
		
		if (dateType == 1) {
			endDate = trObj.find(":input[name='end_date']").val();
			
			if (endDate != "") {
				contents += " ~ " + endDate;
			} else {
				alert("종료일을 등록해주세요.");
				return;
			}
			
			if ((new Date("20" + startDate.replace(/\./gi, "-"))).getTime() > (new Date("20" + endDate.replace(/\./gi, "-"))).getTime()) {
				alert("시작일이 종료일보다 큽니다.");
				return;
			}
		}
		if (dateType == 4) {
			startTime = trObj.find(":input[name='start_time']").val();
			endTime = trObj.find(":input[name='end_time']").val();
			
			if (startTime != "") {
				contents += " " + startTime;
			} else {
				alert("시작시간을 등록해주세요.");
				return;
			}
			
			if (endTime != "") {
				contents += " ~ " + endTime;
			} else {
				alert("종료시간을 등록해주세요.");
				return;
			}
			
			if (stringToTime(startTime) > stringToTime(endTime)) {
				alert("시작시간이 종료시간보다 큽니다.");
				return;
			}
		}
	}
	
	if (jobTitleSel == "" || (jobTitleSel == "직접입력" && directInput == "")) {
		alert("보직을 선택해주세요.");
		return;
	}
	
	if (directInputEnabled == "Y" && directInput == "") {
		alert("내용을 입력해주세요.");
		return;
	}
	
	if (contents != "") {
		contents += "<br>";
	}
	if (typeof jobTitleSel != "undefined" && jobTitleSel != "" && jobTitleSel != "직접입력") {
		contents += jobTitleSel;
		jobTitle = jobTitleSel;
	} else if (directInput != "") {
		contents += directInput;
		jobTitle = directInput;
	}
	
	var contentsStartDate = "";
	var contentsEndDate = "";
	
	if (startDate != "") {
		contentsStartDate = "20" + startDate.replace(/\./gi, "-");
		
		if (startTime != "") {
			contentsStartDate += " " + startTime + ":00";
		} else {
			contentsStartDate += " 00:00:00";
		}
		if (endDate != "") {
			contentsEndDate = "20" + endDate.replace(/\./gi, "-");
		} else {
			contentsEndDate = "20" + startDate.replace(/\./gi, "-");
		}
		if (endTime != "") {
			contentsEndDate += " " + endTime + ":00";
		} else {
			contentsEndDate += " 23:59:59";
		}
	}
	
	var serviceCode = trObj.find(":input[name='serviceCode']").val();
	var serviceType = trObj.find(":input[name='serviceType']").val();
	
	var contentsText = serviceCode + "|" + serviceType + "|" + contentsStartDate + "|" + contentsEndDate + "|" + jobTitle;
	
	trObj.find(":input[name='job_title_sel']").val(null);
	trObj.find(":input[name='start_date']").val(null);
	trObj.find(":input[name='end_date']").val(null);
	trObj.find(":input[name='start_time']").val(null);
	trObj.find(":input[name='end_time']").val(null);
	trObj.find(":input[name='direct_input']").val(null);
	
	$.ajax({
        type: "POST",
        url: "../ajax/service/addSingleService",
        data: {
        	"serviceCode":serviceCode,
        	"serviceType":serviceType,
        	"startDate":contentsStartDate,
        	"endDate":contentsEndDate,
        	"contents":jobTitle
        },
        dataType: "json",
        success: function(data, status) {
        	if (data.status == "success") {
        		var contentsDiv = trObj.find(".contentsVIew");
        		
        		var html = "<div class=\"service-table-content-div\">"
        			+ "<input type=\"hidden\" name=\"service_result_code\" value=\"" + data.service_result_code + "\" />"
    		    	+ "<input type=\"hidden\" name=\"contents_text\" value=\"" + contentsText + "\" />"
    				+ "<div class=\"contents_text\">" + contents + "</div>"
    		        + "<label><button type=\"button\" class=\"btn-del-content\" onClick=\"javascript:deleteContents($(this));\">X</button></label>"
    		        + "</div>";
        		
        		contentsDiv.append(html);
        		contentsDiv.find(".service-table-content-div").show();
        		
        		addPoint(trObj, data.point);
        		
        		updateTotalPoint();
        		
        	} else {
        		alert("오류가 발생했습니다.");
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        },
		beforeSend:function() {
			$.blockUI();
		},
		complete:function() {
			$.unblockUI();
		}
    });
}

/**
 * 봉사등록
 */
function addService() {
	
	if ($(":input[name='contents_text']").filter(function() { return $(this).val() != ""; }).length == 0) {
		alert("입력한 항목이 없습니다.");
		return;
	}
	
	$("#serviceForm").ajaxForm({
		type: "POST",
		url: "../ajax/service/addService",
		dataType: "json",
		success: function(data, status){
			if (data.status == "success") {
        		alert("수정이 완료되었습니다.");
        	}
		},
		error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        },
		beforeSend:function() {
			$.blockUI();
		},
		complete:function() {
			$.unblockUI();
		}
	}).submit();
}

/**
 * 00:00 형식의 시간을 초로 변경 
 * @returns {Number}
 */
function stringToTime(timeString) {
	var timeArr = timeString.split(':');
	return parseInt(timeArr[0], 10) * 60 * 60 + parseInt(timeArr[1], 10) * 60;
}

/**
 * 입력한 항목 삭제
 */
function deleteContents(obj) {
	
	if (!confirm("내용을 삭제하시겠습니까?")) {
		return;
	}
	
	var serviceResultCode = $(obj).parent().parent().find(":input[name='service_result_code']").val();
	
	$.ajax({
        type: "POST",
        url: "../ajax/service/deleteSingleService",
        data: {
        	"serviceResultCode":serviceResultCode
        },
        dataType: "json",
        success: function(data, status) {
        	if (data.status == "success") {
        		addPoint(obj.parents("tr"), -parseFloat(data.point));
        		obj.parents(".service-table-content-div").remove();
        		updateTotalPoint();
        	} else {
        		alert("오류가 발생했습니다.");
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        },
		beforeSend:function() {
			$.blockUI();
		},
		complete:function() {
			$.unblockUI();
		}
    });
}

/**
 * 파일 업로드
 * @param serviceCode
 */
function uploadFile(obj) {
	
	var trObj = obj.parents("tr");
	
	var contentsText = trObj.find(":input[name='contents_text']").val();
	
	if (typeof contentsText == "undefined" || contentsText == "") {
		alert("내용을 먼저 입력해 주세요.");
		return;
	}
	
	var serviceCode = trObj.find(":input[name='serviceCode']").val();
	
	$(".wrap-plan-popup").fadeIn(300);
	
	$(":input[name='uploadServiceCode']").val(serviceCode);
	
	while($("form[name='uploadForm']").length < 2) {
		addFileForm();
	}
	
	getFileList();
}

/**
 * 파일폼 추가
 */
function addFileForm() {
	
	var html = $("<form name=\"uploadForm\" enctype=\"multipart/form-data\" onSubmit=\"return false;\">"
		+ "<div class=\"popup-input-file-div\">"
		+ "<p><input type=\"text\" name=\"fileName\" readonly /></p>"
		+ "<p><label class=\"btn-input-file-label\">등록<input type=\"file\" name=\"file\" class=\"input-upload\" /></label></p>"
		+ "</div>"
		+ "</form>");
	
	html.ajaxForm({
		type: "POST",
		url: "../ajax/service/uploadFile",
		data: {
			"serviceCode":$(":input[name='uploadServiceCode']").val()
		},
		dataType: "json",
		success: function(data, status){
			if (data.status == "success") {
				var fileHtml = "<div class=\"popup-input-del-div\">"
					+ "<input type=\"hidden\" name=\"serviceFileCode\" value=\"" + data.service_file_code + "\" />"
					+ "<input type=\"hidden\" name=\"uploadFileName\" value=\"" + data.uploadFileName + "\" />"
					+ "<p>" + data.reg_date + "</p>"
	                + "<p><input type=\"text\" value=\"" + data.fileName + "\" readonly /></p>"
	                + "<p><button type=\"button\" class=\"btn-dle-file\" onClick=\"javascript:deleteFile($(this));\">파일삭제</button></p>"
	            + "</div>";
				$("#uploadFileList").append(fileHtml);
        	} else {
        		html.find(":input[name='fileName']").val(null);
        		if (data.uploadFileName == "not") {
					alert("지원되지 않는 파일형식입니다.\n엽로드 가능한 파일형식:\njpg, jpeg, gif, png, bmp, mp3, mp4, zip,\ntxt, pdf, xls, xlsx, doc, docx, ppt, pptx, hwp");
					return;
				} else {
					alert("오류가 발생했습니다.");
				}
        	}
		},
		error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        },
		beforeSend:function() {
			$.blockUI();
		},
		complete:function() {
			$.unblockUI();
		}
	});
	
	$(".btn-add-file").before(html);
	
    fileName();
}

function fileName(){
    $(".input-upload").each(function(){
    	$(this).unbind("change");
        $(this).change(function(){
            if( $(this).val() != "" ){
                var value = $(this).val();
                var chk = $(this).val().split('.').pop().toLowerCase();
                
                /*
                  if($.inArray(chk, ["gif","png","jpg","jpeg","mp4","ppt","pptx","pdf"]) == -1) {
                    alert("해당 파일은 첨부할 수 없습니다.");
                    $(this).val("");
                    return true;
                  } else {
                      $(this).parents("label").parent("p").siblings().children("input[type='text']").val(value);
                  }*/
                $(this).parents("label").parent("p").siblings().children("input[type='text']").val(value);
                
                $(this).parents("form").submit();
                
                //ie8에서 오류가 발생해 1초 뒤에 폼이 삭제되게 수정
                setTimeout(function() {
                	$(this).remove();
                }, 1000);
            }
        });
    });
}

/**
 * 파일삭제
 * @param obj
 */
function deleteFile(obj) {
	
	if (!confirm("파일을 삭제하시겠습니까?")) {
		return;
	}
	
	var fileDiv = obj.parent().parent();
	var serviceFileCode = fileDiv.find(":input[name='serviceFileCode']").val();
	var uploadFileName = fileDiv.find(":input[name='uploadFileName']").val();
	
	$.ajax({
        type: "POST",
        url: "../ajax/service/deleteFile",
        data: {
        	"serviceFileCode":serviceFileCode,
        	"uploadFileName":uploadFileName
        },
        dataType: "json",
        success: function(data, status) {
        	if (data.status == "success") {
        		$(":input[name='fileName']").filter(function() { return $(this).val() != ""; }).eq(fileDiv.index()).parents("form").remove();
        		fileDiv.remove();
        	} else {
        		alert("오류가 발생했습니다.");
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        },
		beforeSend:function() {
			$.blockUI();
		},
		complete:function() {
			$.unblockUI();
		}
    });
}

/**
 * 파일목록 출력
 */
function getFileList() {
	$.ajax({
        type: "POST",
        url: "../ajax/service/getFileList",
        data: {
        	"serviceCode":$(":input[name='uploadServiceCode']").val()
        },
        dataType: "json",
        success: function(data, status) {
        	if (data.status == "success") {
        		$("#uploadFileList").empty();
        		$.each(data.list, function(index) {
        			
        			if ($("form[name='uploadForm']").length > index) {
        				var formObj = $("form[name='uploadForm']:eq(" + index + ")");
        				formObj.find(":input[name='fileName']").val(this.orignalName);
        				formObj.find(":input[name='file']").remove();
        			} else {
        				var html = $("<form name=\"uploadForm\" enctype=\"multipart/form-data\" onSubmit=\"return false;\">"
    						+ "<div class=\"popup-input-file-div\">"
    						+ "<p><input type=\"text\" name=\"fileName\" value=\"" + this.orignalName + "\" readonly /></p>"
    						+ "<p><label class=\"btn-input-file-label\">등록</label></p>"
    						+ "</div>"
    						+ "</form>");
        				$(".btn-add-file").before(html);
        			}
        			
        			var fileHtml = "<div class=\"popup-input-del-div\">"
    					+ "<input type=\"hidden\" name=\"serviceFileCode\" value=\"" + this.serviceFileCode + "\" />"
    					+ "<input type=\"hidden\" name=\"uploadFileName\" value=\"" + this.fileName + "\" />"
    					+ "<p>" + this.regDate + "</p>"
//    					+ "<p><a href=\"../download?fileName=" + this.fileName + "&orignalName=" + encodeURI(this.orignalName) + "\">" + this.orignalName + "</a></p>"
    	                + "<p><input type=\"text\" value=\"" + this.orignalName + "\" readonly /></p>"
    	                + "<p><button type=\"button\" class=\"btn-dle-file\" onClick=\"javascript:deleteFile($(this));\">파일삭제</button></p>"
    	            + "</div>";
    				$("#uploadFileList").append(fileHtml);
        		});
        		
        		if (data.list.length == 0) {
        			$("form[name='uploadForm']").remove();
        			$("#uploadFileList").empty();
        			
        			while($("form[name='uploadForm']").length < 2) {
        				addFileForm();
        			}
        		}
        	} else {
        		alert("오류가 발생했습니다.");
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        }
    });
}

/**
 * 첨부파일 복사
 * @param obj
 * @returns
 */
function copyAttachedFile() {
	
	if (!confirm("첨부파일을 복사하시겠습니까?")) {
		return;
	}
	
	$.ajax({
        type: "POST",
        url: "../ajax/service/copyAttachedFile",
        data: {
        },
        dataType: "json",
        success: function(data, status) {
        	if (data.status != "success") {
        		alert("오류가 발생했습니다.");
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        },
		beforeSend:function() {
			$.blockUI();
		},
		complete:function() {
			$.unblockUI();
		}
    });
}

function ToFloat(number){
	var tmp = number + "";
	if(tmp.indexOf(".") != -1){
		number = number.toFixed(2);
		number = number.replace(/(0+$)/, "");
	}

	return number;
}