
/**
 * 사용자 정보 취득
 */
function getServiceResult() {
	$.ajax({
        type: "POST",
        url: "../ajax/service/getServiceResult",
        data: {
        },
        dataType: "json",
        success: function(data, status) {
        	if (data.status == "success") {        		
        		setServiceResult(data.serviceResult1);
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        }
    });
}

function ToFloat(number){
    var tmp = number + "";
    if(tmp.indexOf(".") != -1){
        number = Math.floor(number*100)/100;
//        number = number.replace(/(0+$)/, "");
    }

    return number;
}

function getMyResultTotalPoint() {
	$.ajax({
        type: "POST"
        ,url: "../pro/getMyResultTotalPoint"
        ,contentType : 'application/json; charset=UTF-8'
        ,data: {}
        ,dataType: "json"
        ,global : false
        ,cache: false
        ,async: false
        ,success: function(json){
           console.debug(json);
           if(json != undefined && json != null){
        	   if(json.status == "success"){
        		   console.log(json);
        		   $("#summary_class").html(json.myResultStandareScore.pro_class);
        		   $("#summary_ranking").html(json.myResultStandareScore.pro_ranks != '0' ? json.myResultStandareScore.pro_ranks : '');
        		   
        		   $("#change_standard_score").html(ToFloat(json.myResultStandareScore.change_standard_score));
        		   $("#change_education_score").html(ToFloat(json.myResultStandareScore.change_education_score));
        		   $("#change_research_score").html(ToFloat(json.myResultStandareScore.change_research_score));
        		   $("#change_service_score").html(ToFloat(json.myResultStandareScore.change_service_score));
        		   $("#total_class").html(ToFloat(json.myResultStandareScore.pro_class));
        		   $("#best_type").html(ToFloat(json.myResultStandareScore.best_type));
        		   $("#best_score").html(ToFloat(json.myResultStandareScore.best_score));
        		   
        		   $("#summary_sum_total").html(ToFloat(json.myResultTotalPoint.totalpoint));        		   
        		   $("#summary_sum_research").html(ToFloat(json.myResultTotalPoint.researchpoint) + " / " + ToFloat(json.myResultStandareScore.research_score));
        		   $("#summary_sum_education").html(ToFloat(json.myResultTotalPoint.educationpoint) + " / " + ToFloat(json.myResultStandareScore.education_score));
        		   $("#summary_sum_service").html(ToFloat(json.myResultTotalPoint.servicepoint) + " / " + ToFloat(json.myResultStandareScore.service_score));
        		   /*$("#summary_sum_research").html(ToFloat(json.myResultTotalPoint.researchpoint));
        		   $("#summary_sum_education").html(ToFloat(json.myResultTotalPoint.educationpoint));
        		   $("#summary_sum_service").html(ToFloat(json.myResultTotalPoint.servicepoint));*/ 
        		   $("#summary_user_name").html(json.myResultTotalPoint.user_name);
        		   
        	   }else{
        		   alert("오류가 발생했습니다.");
        	   }
           }
           return;
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
            alert("오류가 발생했습니다.");
        }
        ,complete: function() {
        	
        }
    });
}

function setServiceResult(json){
	$("td[id^='serviceCode_count_']").empty();
	$("td[id^='serviceCode_content_']").empty();
	$("td[id^='serviceCode_file_']").empty();
	$("td[id^='serviceCode_point_']").empty();
	var j = 0;
	for(var i=0; i < json.length; i++){
		var data = json[i];
		console.debug(data);
		
		var countId = "#serviceCode_count_" + data.serviceType + "_"+ data.serviceCode;
		var contentId = "#serviceCode_content_" + data.serviceType + "_"+ data.serviceCode;
		var fileId = "#serviceCode_file_" + data.serviceType + "_" + data.serviceCode;
		var pointId = "#serviceCode_point_" + data.serviceType + "_" + data.serviceCode;

				
		console.debug(contentId);
		console.debug(fileId);
		console.debug(pointId);
		
		setServiceResult_count(countId, 1);
		setServiceResult_content(contentId, data.contentsViewText);
		setServiceResult_file(fileId, data.fileCount ,data.serviceCode);
		setServiceResult_point(pointId,data.point);
	}
	$(".service-table-content-div").show();
	
	
	var point1 = setServiceCode_sum("serviceCode_point_1");
	var point2 = setServiceCode_sum("serviceCode_point_2");
	$("#serviceCode_point_total").html(ToFloat((point1 + point2)) + "점");
}
function setServiceCode_sum(id){
	var serviceCode_sum = 0;
	$("[id^='"+ id + "']").each(function(index){
		console.debug($(this).html().trim());
		var html = $(this).html().trim();
		var pointed = 0;
     	if(html == ""){
     		pointed = 0;
		}else{
			pointed = ToFloat(html)*1;
		}
     	
     	serviceCode_sum = serviceCode_sum + pointed;
		
	});
	
	$("#" + id + "_sum").html(serviceCode_sum + "점");
	$("#" + id + "_sum_sum").html(serviceCode_sum + "점");
	return serviceCode_sum;
}

function setServiceResult_count(id, point){
	var html = $(id).html().trim();
	var pointed = 0;
	if(html == ""){
		pointed = 0;
	}else{
		pointed = parseInt(html);
	}
	
	pointed = pointed + point;
	
	console.debug(pointed);
	$(id).html(pointed);
}
function setServiceResult_content(contentId, contentsViewText){
	var html_content = "";
	html_content += "<div class=\"service-table-content-div\">";
	html_content += contentsViewText;
	html_content += "</div>";

	$(contentId).append(html_content);
}
function setServiceResult_file(fileId, fileCount, serviceCode){
	if(fileCount == 0){
		return;
	}

	var html = $(fileId).html().trim();
	var html_content = "";
	if(html == ""){
		html_content += "<p>";
		html_content += "<span class=\"text-bold\"><a>" + fileCount + "건</a></span>";
		html_content += "</p>";
		html_content += "<a class=\"btn-down\" onClick=\"getFileList(" + serviceCode + ")\">다운로드</a>";
	}else{
		return;
	}
	$(fileId).html(html_content);
}
function setServiceResult_point(pointId, point){
	var pointedHtml = $(pointId).html().trim();
	var pointed = 0;
	if(pointedHtml == ""){
		pointed = 0;
	}else{
		pointed = parseInt(pointedHtml);
	}
	
	pointed = pointed + point;
	
	console.debug(pointed);
	var pointedHtml = $(pointId).html(pointed);
}



/**
 * 파일목록 출력
 */
function getFileList(serviceCode) {
	
	$.ajax({
        type: "POST",
        url: "../ajax/service/getFileList",
        data: {
        	"serviceCode": serviceCode
        },
        dataType: "json",
        success: function(data, status) {
        	if (data.status == "success") {
        		console.debug(data);
        		$("#uploadFileList").empty();
        		$.each(data.list, function(index) {
        			
		
        			var fileHtml = "<div class=\"popup-input-del-div\">"
    					+ "<input type=\"hidden\" name=\"serviceFileCode\" value=\"" + this.serviceFileCode + "\" />"
    					+ "<input type=\"hidden\" name=\"uploadFileName\" value=\"" + this.fileName + "\" />"
    					+ "<p>" + this.regDate + "</p>"
    					+ "<p><a href=\"../download?fileName=" + this.fileName + "&orignalName=" + encodeURI(this.orignalName) + "\">" + this.orignalName + "</a></p>"
    	                + "<p><input type=\"text\" value=\"" + this.orignalName + "\" readonly /></p>"
    	            + "</div>";
    				$("#uploadFileList").append(fileHtml);
        		});
        		
        		$("#uploadFileLayer").show();
        	} else {
        		alert("오류가 발생했습니다.");
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        }
    });
}
