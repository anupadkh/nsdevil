
/**
 * 쿠키 취득
 * @param Name
 * @returns
 */
function getCookie(Name) {
	var search = Name + "=";
	if (document.cookie.length > 0) {
		offset = document.cookie.indexOf(search);
		if (offset != -1) {
			offset += search.length;
			end = document.cookie.indexOf(";", offset);
			if (end == -1)
				end = document.cookie.length;
			return unescape(document.cookie.substring(offset, end));
		}
	}
}

/**
 * 쿠키 저장
 * @param name
 * @param value
 * @param expiredays
 */
function setCookie(name,value,expiredays) {
	var todayDate = new Date();
	todayDate.setDate(todayDate.getDate() + expiredays);
	document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";";
}

/**
 * 쿠키 삭제
 * @param cookieName 삭제할 쿠키명
 */
function deleteCookie(cookieName) {
	 var expireDate = new Date();
	 expireDate.setDate(expireDate.getDate() - 1);
	 document.cookie = cookieName + "= " + "; expires=" + expireDate.toGMTString() + "; path=/";
}

/**
 * 숫자만 입력
 * @param strValue
 */
function numberFilter(strValue) {
	return strValue.replace(/[^0-9]/gi, ""); 
}

/**
 * 사용자 정보 취득
 */
function getUserInfo() {
	$.ajax({
        type: "POST",
        url: "ajax/main/getUserInfo",
        data: {
        },
        dataType: "json",
        success: function(data, status) {
        	if (data.status == "success") {
        		
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        }
    });
}