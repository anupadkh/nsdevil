/**
 * 00:00 형식의 시간을 초로 변경 
 * @returns {Number}
 */
function stringToTime(timeString) {
	var timeArr = timeString.split(':');
	return parseInt(timeArr[0], 10) * 60 * 60 + parseInt(timeArr[1], 10) * 60;
}


/**
 * 파일 업로드
 * @param serviceCode
 */
function uploadFile(obj, seq) {
	console.debug("uploadFile =>"+ obj + " / " + seq);
	var trObj = obj.parents("tr");
	
	var contentsText = trObj.find(":input[id^='achieve_re_contents_']").val();
	var unit = trObj.find(":input[id^='achieve_re_unit_']").val();
	
//	console.debug("unit =>"+ unit + "contetnsText =>"+ contentsText + "   / seq =>"+ seq);
//	if(typeof unit != "undefined"){
//		if(unit == ""){
//			alert("건수를 먼저 입력해 주세요.");
//			return;	
//		}
//	}
	
	var serviceCode = seq;//trObj.find(":input[name='serviceCode']").val();
	$(".wrap-plan-popup").fadeIn(300);
	
	$(":input[name='uploadServiceCode']").val(serviceCode);
	
	console.debug('uploadForm length =>' + $("form[name='uploadForm']").length);
	//while($("form[name='uploadForm']").length < 2) {
		//addFileForm();
	//}
	
	getFileList(seq);
}

/**
 * 파일폼 추가
 */
function addFileForm() {
	
	var html = $("<form name=\"uploadForm\" enctype=\"multipart/form-data\" onSubmit=\"return false;\">"
		+ "<div class=\"popup-input-file-div\">"
		+ "<p><input type=\"text\" name=\"fileName\" readonly /></p>"
		+ "<p><label class=\"btn-input-file-label\">등록<input type=\"file\" name=\"file\" class=\"input-upload\" /></label></p>"
		+ "</div>"
		+ "</form>");
	
	var serviceCode = $(":input[name='uploadServiceCode']").val();
	html.ajaxForm({
		type: "POST",
		url: "./uploadFile",
		data: {
			"serviceCode": serviceCode
			,"userCode" : requestUserCode
			,"category" : "r"
		},
		
		dataType: "json",
		success: function(data, status){
			console.debug("=============addFileForm");
			console.debug(data);
			if (data.status == "success") {
				var fileHtml = "<div class=\"popup-input-del-div\">"
					+ "<input type=\"hidden\" name=\"serviceFileCode\" value=\"" + data.research_file_code + "\" />"
					+ "<input type=\"hidden\" name=\"uploadFileName\" value=\"" + data.uploadFileName + "\" />"
					+ "<p  style=\"width:100px\">" + data.reg_date + "</p>"
	                + "<p class=\"popup-fd-title\" style=\"width:260px;;height: auto;vertical-align: middle;\">" +
	                		"<a href=\"../download?fileName=" + data.uploadFileName + "&orignalName=" + encodeURI(data.fileName) + "\">" + data.fileName +"</a></p>"
	                + "<p><button type=\"button\" class=\"btn-dle-file\" onClick=\"javascript:deleteFile($(this) ,"+ data.research_file_code +");\">파일삭제</button></p>"
	            + "</div>";
				$("#uploadFileList").append(fileHtml);				
        	} else {
        		html.find(":input[name='fileName']").val(null);
        		if (data.uploadFileName == "not") {
					alert("지원되지 않는 파일형식입니다.\n엽로드 가능한 파일형식:\njpg, jpeg, gif, png, bmp, mp3, mp4, zip,\ntxt, pdf, xls, xlsx, doc, docx, ppt, pptx, hwp");
					return;
				} else {
					alert("오류가 발생했습니다.");
				}
        	}
		},
		error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        },
		beforeSend:function() {
			//$.blockUI();
		},
		complete:function() {
			//$.unblockUI();
		}
	});
	
	$(".btn-add-file").before(html);
	
    fileName();
}

function fileName(){
    $(".input-upload").each(function(){
    	$(this).unbind("change");
        $(this).change(function(){
            if( $(this).val() != "" ){
                var value = $(this).val();
                var chk = $(this).val().split('.').pop().toLowerCase();
                
                /*
                  if($.inArray(chk, ["gif","png","jpg","jpeg","mp4","ppt","pptx","pdf"]) == -1) {
                    alert("해당 파일은 첨부할 수 없습니다.");
                    $(this).val("");
                    return true;
                  } else {
                      $(this).parents("label").parent("p").siblings().children("input[type='text']").val(value);
                  }*/
                $(this).parents("label").parent("p").siblings().children("input[type='text']").val(value);
                
                $(this).parents("form").submit();
                
                //ie8에서 오류가 발생해 1초 뒤에 폼이 삭제되게 수정
                setTimeout(function() {
                	$(this).remove();
                }, 1000);
            }
        });
    });
}

/**
 * 파일삭제
 * @param obj
 */
function deleteFile(obj,researchFileCode) {
	
	if (!confirm("파일을 삭제하시겠습니까?")) {
		return;
	}
	
	var fileDiv = obj.parent().parent();
	//var serviceFileCode = fileDiv.find(":input[name='serviceFileCode']").val();
	var uploadFileName = fileDiv.find(":input[name='uploadFileName']").val();
	console.debug("============deleteFile =>"+ researchFileCode);
	$.ajax({
        type: "POST",
        url: "./deleteFile",
        data: {
        	"researchFileCode":researchFileCode,
        	"uploadFileName":uploadFileName,
        	"achieveType": "r"
        },
        dataType: "json",
        success: function(data, status) {
        	console.debug(data);
        	if (data.status == "success") {
        		$(":input[name='fileName']").filter(function() { return $(this).val() != ""; }).eq(fileDiv.index()).parents("form").remove();
        		fileDiv.remove();
        	} else {
        		alert("오류가 발생했습니다.");
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        },
		beforeSend:function() {
			//$.blockUI();
		},
		complete:function() {
			//$.unblockUI();
		}
    });
}

/**
 * 파일목록 출력
 */
function getFileList(seq) {
	console.debug("getFileList =>"+ seq);
	$.ajax({
        type: "POST",
        url: "./getFileList",
        data: {
        	"researchCode":seq //$(":input[name='uploadServiceCode']").val()
        	,"userCode" : requestUserCode
        	,"achieveType" : "r"
        },
        dataType: "json",
        success: function(data, status) {
        	console.debug("getFileList ============");
        	console.debug(data);
        	if (data.status == "success") {
        		$("#uploadFileList").empty();
        		$("form[name='uploadForm']").remove();
        		$.each(data.list, function(index) {
        			
        			if ($("form[name='uploadForm']").length > index) {
        				var formObj = $("form[name='uploadForm']:eq(" + index + ")");
        				formObj.find(":input[name='fileName']").val(this.original_name);
        				formObj.find(":input[name='file']").remove();
        			} else {
        				var html = $("<form name=\"uploadForm\" enctype=\"multipart/form-data\" onSubmit=\"return false;\">"
    						+ "<div class=\"popup-input-file-div\">"
    						+ "<p><input type=\"text\" name=\"fileName\" value=\"" + this.original_name + "\" readonly /></p>"
    						+ "<p><label class=\"btn-input-file-label\">등록</label></p>"
    						+ "</div>"
    						+ "</form>");
        				$(".btn-add-file").before(html);
        			}
        			
        			var fileHtml = "<div class=\"popup-input-del-div\">"
    					+ "<input type=\"hidden\" name=\"serviceFileCode\" value=\"" + this.research_file_code + "\" />"
    					+ "<input type=\"hidden\" name=\"uploadFileName\" value=\"" + this.file_name + "\" />"
    					+ "<p style=\"width:100px\">" + this.reg_date + "</p>"
    					+ "<p class=\"popup-fd-title\" style=\"width:260px;;height: auto;vertical-align: middle;\"><a href=\"../download?fileName=" + this.file_name + "&orignalName=" + encodeURI(this.original_name) + "\">" +  this.original_name + "</a></p>"
    	                + "<p style=\"width:100px\"><button type=\"button\" class=\"btn-dle-file\" onClick=\"javascript:deleteFile($(this), "+this.research_file_code +");\">파일삭제</button></p>"
    	            + "</div>";
    				$("#uploadFileList").append(fileHtml);
        		});
        		
        		if (data.list.length == 0) {
        			$("form[name='uploadForm']").remove();
        			$("#uploadFileList").empty();
        			addFileForm();
        			//while($("form[name='uploadForm']").length < 2) {
        				//addFileForm();
        			//}
        		}
        	} else {
        		alert("오류가 발생했습니다.");
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        }
    });
}