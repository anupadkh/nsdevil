<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../include/pro_header.jsp" %>

<script src="js/datetimep.js"></script>

<div id="wrap">
    
    <%@ include file="../include/top.jsp" %>
    
    <div class="container">

        <!-- 탭 메뉴 영역 -->
        <!--<div class="tabmenu-div">
            <ul class="tabmenu-ul tabs">
                <li data-to="tab1" class="active">
                    <a>교내 봉사</a>
                </li>
                <li data-to="tab2">
                    <a>교외 봉사</a>
                </li>
            </ul>
        </div>-->
        <!-- //.tabmenu-div -->
        
        <form id="serviceForm" onSubmit="return false;">
		
        <!-- 테이블 영역 -->
        <div class="list-table-contents hidden" id="contents">
            <p class="tab-content-table-title">
                [교내 봉사]
            </p>
            <table class="service-table">
                <colgroup>
                    <col width="90" />
                    <col width="" />
                    <col width="" />
                    <col width="80" />
                    <col width="160" />
                    <col width="250" />
                    <col width="220" />
                    <col width="100" />
                    <col width="50" />
                </colgroup>
                <tr>
					<th scope="col">업적내용</th>
					<th colspan="2" scope="col">평가항목(평가기간이내)</th>
					<th scope="col">기준<br />점수</th>
					<th scope="col">비고</th>
					<th scope="col">보직 선택 기간입력</th>
					<th scope="col">내용</th>
					<th scope="col">증빙서류</th>
					<th scope="col">점수</th>
				</tr>
				<tbody id="service1List">
				<c:forEach var="item" items="${data.service1List}">
				<%@ include file="serviceList.jsp" %>
				</c:forEach>
				</tbody>
				<tr>
					<td colspan="4" class="total_text">
						소계
					</td>
					<td colspan="5" class="total_text">
						<span id="inTotalPoint1"></span>점
					</td>
                </tr>
            </table>
            <!-- //.service-table -->
            
            <p class="tab-content-table-title">
                [교외 봉사]
            </p>
            <table class="service-table">
                <colgroup>
                    <col width="90" />
                    <col width="" />
                    <col width="" />
                    <col width="70" />
                    <col width="190" />
                    <col width="250" />
                    <col width="220" />
                    <col width="100" />
                    <col width="50" />
                </colgroup>
                <tr>
                    <th scope="col">업적내용</th>
                    <th colspan="2" scope="col">평가항목(평가기간이내)</th>
                    <th scope="col">기준<br />점수</th>
                    <th scope="col">비고</th>
                    <th scope="col">보직 선택 기간입력</th>
                    <th scope="col">내용</th>
                    <th scope="col">증빙서류</th>
                    <th scope="col">점수</th>
                </tr>
                <tbody id="service2List">
                <c:forEach var="item" items="${data.service2List}">
                <%@ include file="serviceList.jsp" %>
                </c:forEach>
                </tbody>
                <tr>
                    <td colspan="4" class="total_text">
                        소계
                    </td>
                    <td colspan="5" class="total_text">
                        <span id="outTotalPoint1"></span>점
                    </td>
                </tr>
            </table>
            <!-- //.service-table -->
            
            <p class="tab-content-table-title">
                [총점]
            </p>
            <table class="service-table co-tabs-table">
                <colgroup>
                    <col width="" />
                    <col width="" />
                    <col width="" />
                    <col width="" />
                    <col width="" />
                </colgroup>
                <tbody>
                    <tr>
                        <th>
                            구분
                        </th>
                        <th style="display:none;">
                            교내봉사
                        </th>
                        <th>
                            교외봉사
                        </th>
                        <th>
                            합계
                        </th>
                        <th>
                            비고
                        </th>
                    </tr>
                    <tr class="total-score-tr">
                        <td>
                            점수
                        </td>
                        <td style="display:none;">
                            <span id="inTotalPoint2"></span>점
                        </td>
                        <td>
                            <span id="outTotalPoint"></span>점
                        </td>
                        <td>
                            <span id="finalPoint"></span>점
                        </td>
                        <td>

                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- //.list-table-contents-->
        
        <div class="guide-txt-table-div hidden">
            <p>
                &lt;주&gt;
            </p>
            <p>
                1. [봉사영역 항목별 평가표]의 교내봉사 부분 중 교내 공문으로 발령 받은 경우는 의전원 행정실에서 작성한다. 단 교내 공문으로 발령 받았더라도 비고란에 조건이 있는 사항과 공문으로 발령 받지 않은 봉사내역은 내용과 함께 근거 자료를 제출하여야 한다. 교외봉사업적은 봉사영역 평가 항목별 평점표를 작성하고 근거자료를 제출 한다.
            </p>
            <p class="color-red">
                2. 동일기간 보직을 겸직하는 경우 점수가 상위인 보직의 점수를 부여한다. 교실주임의 경우 주임교수회의를 1/3 참석하지 않은 경우 가점의 1/2만 인정한다.
            </p>

            <p>
                3. 상임위원회, 운영위원회, 상임소위원회 위원, 기타 지원시설(공동기기실, 실험동물실, 의대도서관분관 등)의 장을 포함한다. 위원회 위원은 1/2 이상 회의 참석한 경우에만 가점을 부여하고, 참석여부는 회의록으로 확인한다.
            </p>
            <p>
                4. 기간이 년 단위인 경우 1년 미만인 경우에는 기간을 반올림한 개월 수에 비례하여 환산한 점수를 부여한다.
            </p>
        </div>
        <!-- //.guide-txt-table-div -->
        
        <!-- 등록 버튼 영역 -->
        <!--
        <div class="signup-btn-div text-right">
            <a href="javascript:addService();" class="btn-signup">수정</a>
        </div>
        -->
        <!-- //.signup-btn-div -->
        
        </form>
        
    </div>
    <!-- //.container -->

    <%@ include file="../include/bottom.jsp" %>
    
</div>
<!-- //#wrap -->

    <!-- 증빙서류 등록 팝업 영역 -->
    <div class="wrap-popup wrap-plan-popup hidden popup-thesis">
    	<input type="hidden" name="uploadServiceCode" />
        <div class="popup-guide">
            <div class="popup-guide-header">
                <h2>
                    증빙서류
                </h2>
                <a class="btn-close-giude-popup" title="close">
                    X
                </a>
            </div>
            <div class="popup-guide-content">
                <fieldset>
                    <div class="popup-input-file-up-div">
                        <p class="popup-fd-title">
                            파일 추가를 누르시면 파일을 여러 개 등록할 수 있습니다.
                        </p>
                        <button type="button" class="btn-add-file">
                            + 파일 추가
                        </button>
                    </div>
                    <!-- //.popup-input-file-up-div -->

                    <div class="popup-input-file-up-div">
                        <!--<p class="popup-fd-title">
                            <span class="color-blue">
                                (<span class="word-cut">
                                SCI급 논문
                                </span>)
                            </span> 증빙서류 첨부내역
                        </p>-->
                        <div id="uploadFileList"></div>
                        <div class="btn-submit-div">
                            <a>완료</a>
                        </div>
                    </div>
                    <!-- //.popup-input-file-up-div -->
                </fieldset>
            </div>
        </div>
    </div>
    <!-- //.wrap-popup -->
<script type="text/javascript" src="<c:url value="/js/service.js"/>"></script>
<script>
$(".service-nav a").removeClass("active");
$(".service-nav a:eq(1)").addClass("active");
$(document).ready(function(){
	

	
	//getServiceResult();
	
    //탭메뉴
	/*$(".tab-content").hide();
    $(".tab-content:first").show();
    $(".tabs li").bind("click", function(){
        $(".tabs li").removeClass("active");
        $(this).addClass("active");
        $(".tab-content").hide();
        var activeTab = $(this).attr("data-to");
        $("." + activeTab).show();
    });*/
    //datepicker
    
	$.datetimepicker.setLocale('kr');
    $('.datepicker-input').datetimepicker({
        timepicker:false,
        datepicker:true,
        //format:'y.m.d H:i',
        //formatDate:'y.m.d H:i'
        format:'y.m.d',
        formatDate:'y.m.d'
    });
    $('.timepicker-input').datetimepicker({
        timepicker:true,
        datepicker:false,
        //format:'y.m.d H:i',
        //formatDate:'y.m.d H:i'
        format:'H:i',
        formatDate:'H:i',
        step:10
    });
    $('.btn-monthpick').bind('click',function(){
        $(this).siblings('input').datetimepicker('show');
    });
    $(".btn-close-giude-popup").bind("click", function(){
        $(".wrap-plan-popup").fadeOut(300);
        getServiceResult();
    });
    
    $(".btn-submit-div a").bind("click", function(){
        $(".wrap-plan-popup").fadeOut(300);
        getServiceResult();
    });
    
    
    // 파일 등록폼 추가
    $(".wrap-popup").on("click",".btn-add-file", function(){
        addFileForm();
    });
    
    getPeriodInfo();
});
function getPeriodInfo() {
	 function getToday(){
        var date = new Date();
        var year  = date.getFullYear();
        var month = date.getMonth() + 1; // 0부터 시작하므로 1더함 더함
        var day   = date.getDate();
    
        if (("" + month).length == 1) { month = "0" + month; }
        if (("" + day).length   == 1) { day   = "0" + day;   }
		return ("" + year + month + day)	       
    }
	$.ajax({
        type: "POST",
        url: '<c:url value="/pro/getPeriodInfo"/>',
        data: {},
        dataType: "json",
        success: function(data, status) {
        	console.debug("getPeriodInfo ============");
        	console.debug(data);
        	if (data.status == "success") {
        		if(data.periodInfo == undefined || data.periodInfo == null){
        			//alert("조회기간 정보를 찾을 수 없습니다.\n 관리자에게 문의하세요");
        			blockResultData();
        			return;
        		}
        		var startDate_a = 0;
        		var endDate_a = 0;
        		var startDate_b = 0;
        		var endDate_b = 0;
        		var startDate_c = 0;
        		var endDate_c = 0;
        		for(var i=0; i < data.periodInfo.length; i++){
        			var period = data.periodInfo[i];
        			if(period.period_type == "B"){
        				startDate_a = parseInt(period.start_date,10);
        				endDate_a = parseInt(period.end_date,10);
        			}
        			if(period.period_type == "A"){
        				startDate_b = parseInt(period.start_date,10);
        				endDate_b = parseInt(period.end_date,10);
        			}
        			if(period.period_type == "C"){
        				startDate_c = parseInt(period.start_date,10);
        				endDate_c = parseInt(period.end_date,10);
        			}
        		}
        		if(startDate_a == 0 || startDate_a ==0){
        			//alert("조회기간 정보를 찾을 수 없습니다.\n 관리자에게 문의하세요");
        			blockResultData();
        			return;
        		}
        		var disableMode=null;
        		var rightNow = new Date();
        		var today = getToday();// rightNow.toLocaleString().slice(0,10).replace(/-/g,"");
        		today = parseInt(today,10);
        		console.log(today + " / " + startDate_a + " / " + endDate_a);
        		if( (today >= startDate_a && today <= endDate_a)){
            	 	$("#contents").show();
            	 	$(".guide-txt-table-div").show();
            	 	getServiceResult(disableMode);
            	 	return;
        		}
        		if(today >= startDate_b){
        			console.debug("조회공시 기간에 걸림");
        			//blockResultData();
        			//return;
        			//disableButtons();
        			disableMode="B";
        		}
        		if(today >= startDate_c){
        			console.debug("확정조회공시 기간에 걸림");
        			//blockResultData();
        			disableMode="C";
        		}
        		blockResultData();
        	} else {
        		alert("오류가 발생했습니다.");
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        }
    });
}
function disableElements(){
	$("button").each(function(index){
		if($(this).attr("class") == "btn-service-popup"){
			//$(this).attr("onclick","javascript:disableButtons(true)");
		}else{
			$("button").attr("disabled",true);		
		}
	});
	$("input").attr("disabled",true);
	$("select").attr("disabled",true);
}

function blockResultData(){
	$(".guide-txt-table-div").hide();
	var contents = "";
	contents += "<div class=\"date_not\">";
	contents += "<p><img src=\"images/not.png\"></p>";
	contents += "<p class=\"date_memo_title\">봉사 등록 기간이 아닙니다.<p>";
	contents += "<p class=\"date_memo_text\">(봉사등록기간에만 등록하실 수 있습니다.)</p>";
	contents += "<p class=\"red_bt\"><a href=\"javascript:history.back(-1)\">확인</a></p>";
	contents += "</div>";
	$("#contents").html(contents);
	$("#contents").show();
}
</script>