<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../include/pro_header.jsp" %>

<div id="wrap">
    
    <%@ include file="../include/top.jsp" %>

    <div class="container">
        <div class="left_menu">
            <ul class="left_ul">
                <li><a href="./notice">알림</a></li>
                <li><a href="./info">내정보 수정</a></li>
                <li class="menuon"><a href="./result">평가결과 조회</a></li>
            </ul>    
        </div>
        <!-- //.leftmenu -->

        <div class="content hidden" id="contents">
            <!-- 테이블 내용 영역 -->
            <div class="list-table-contents scroll-section">
                <!-- 페이지당 리스트 최대 10건 노출 -->
                <div class="score-div" id="" style="padding-left:5px;height:35px;font-size:20px;">
                	▣ 영역별 원점수 / 표준 점수
                </div>
                <table class="service-table">
                    <colgroup>
                        <col width="140" />
                        <col width="250" />
                        <col width="250" />
                        <col width="250" />
                    </colgroup>
                    <tbody>
                        <tr>

                            <th colspan="">
                                성명
                            </th>
                            <th colspan="">
                                교육<br />
                                (원점수 / 표준점수)
                            </th>
                            <th colspan="">
                                연구<br />
                                (원점수 / 표준점수)
                            </th>
                            <th colspan="">
                                봉사<br />
                                (원점수 / 표준점수)
                            </th>
                           
                        </tr>
                        <tr>

                            <td id="summary_user_name">
                               <!--   조희중 -->
                            </td>
                            <td >
                                <div class="score-div" id="summary_sum_education">
                                    <!-- 54.68 / 54.72  -->
                                </div>
                            </td>
                            <!-- 해당영역 미승인 표시 클래스 "disappr"-->
                            <td  class="">
                                <div class="score-div" id="summary_sum_research">
                                    <!-- 34.026 / 34.37 -->
                                </div>
                            </td>
                            <td >
                                <div class="score-div" id="summary_sum_service" >
                                    <!-- 36.32 / 34.86 -->
                                </div>
                            </td>                           
                        </tr>
                    </tbody>
                </table>
                <br>
                <div class="score-div" id="" style="padding-left:5px;height:35px;font-size:20px;">
                	▣ 유형별 환산 점수
                </div>
                <table class="service-table">
                    <colgroup>
                        <col width="140" />
                        <col width="125" />
                        <col width="125" />
                        <col width="125" />
                        <col width="125" />
                        <col width="125" />
                        <col width="125" />
                    </colgroup>
                    <tbody>
                        <tr>

                            <th>
                                표준형
                            </th>
                            <th>
                                교육
                
                            </th>
                            <th>
                                연구
                            </th>
                            <th>
                                봉사
                            </th>
                            <th>
                                유형
                            </th>
                            <th>
                                최종점수
                            </th>
                            <th>
                                등급
                            </th>
                            
                           
                        </tr>
                        <tr>
                            <td id="change_standard_score">
                               <!--   조희중 -->
                            </td>
                            <td >
                                <div class="score-div" id="change_education_score">
                                    <!-- 54.68 / 54.72  -->
                                </div>
                            </td>
                            <!-- 해당영역 미승인 표시 클래스 "disappr"-->
                            <td  class="">
                                <div class="score-div" id="change_research_score">
                                    <!-- 34.026 / 34.37 -->
                                </div>
                            </td>
                            <td >
                                <div class="score-div" id="change_service_score" >
                                    <!-- 36.32 / 34.86 -->
                                </div>
                            </td>
                            <td >
                                <div class="score-div" id="best_type">
                                    <!-- 54.68 / 54.72  -->
                                </div>
                            </td>
                            <!-- 해당영역 미승인 표시 클래스 "disappr"-->
                            <td  class="">
                                <div class="score-div" id="best_score">
                                    <!-- 34.026 / 34.37 -->
                                </div>
                            </td>
                            <td >
                                <div class="score-div" id="total_class" >
                                    <!-- 36.32 / 34.86 -->
                                </div>
                            </td>                           
                        </tr>
                    </tbody>
                </table>
                <!-- //.service-table -->
            </div>
            <!-- //.list-table-contents -->
            
            <!-- 탭 메뉴 영역 -->
            <div class="tabmenu-div mg-t30">
                <ul class="tabmenu-ul tabs">
                    <li id="menu_tab1" data-to="tab1" class="active">
                        <a>교육</a>
                    </li>
                    <li  id="menu_tab2" data-to="tab2">
                        <a>연구</a>
                    </li>
                    <li  id="menu_tab3" data-to="tab3">
                        <a>봉사</a>
                    </li>
                </ul>
            </div>
            <!-- //.tabmenu-div -->
            
            <!-- 교육 -->
            <div class="tab-content tab1 mg-b40">
                <div class="list-table-contents">
            	<table class="service-table co-tabs-table autoheight">
                    <colgroup>
                        <col width="80" />
                        <col width="220" />
                        <col width="180" />
                        <col width="" />
                        <col width="90" />
                        <col width="" />
                        <col width="80" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <th colspan="2">
                                평가 항목(평가기간 이내)
                            </th>
                            <th>
                                기준점수
                            </th>
                            <th>
                                비고
                            </th>
                            <th>
                                건수(횟수)
                            </th>
                            <th>
                                내용
                            </th>
                            <th>
                                점수 확인
                            </th>
                        </tr>
                        <tr class="auth_category_ed01">
                            <td rowspan="9">
                                강의시간
                            </td>
                            <td>
                                강의 시간 수<br />
                                (해당학년도 1년간)
                            </td>
                            <td>
                                Sqrt<br />
                                (강의 시간 수)
                            </td>
                            <td class="text-left">
                                * 의학전문대학원의 실지로 시행된 강의시간 수<br />
                                * 인정(교과목강의, 국시특강, 대학원 공통과목, 기초의학리뷰)<br />
                                * 제외(실습, 대학원) 의전원 가중치 1, 타단대 가중치 0.7<br />
                                * 타 단대강의 및 대학원공통과목은 증빙자료 개별 제출
                            </td>
                            <td class="score-input-td" >
                                <input type="text" value=""  id="achieve_edu_unit_1"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_1" ></textarea>
                            </td>
                            <td id="achieve_edu_point_1">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_ed01">
                            <td class="border-l">
                                기초세미나
                            </td>
                            <td>
                                과목 수 * 1.0
                            </td>
                            <td class="text-left">
                                해당년도 강의계획서 기준
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_2" />
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_2" ></textarea>
                            </td>
                            <td id="achieve_edu_point_2">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_ed01">
                            <td class="border-l">
                                PBL 튜터
                            </td>
                            <td>
                                회수(모듈 2회 기준) * 1.0
                            </td>
                            <td class="text-left">

                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_3"  />
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_3" ></textarea>
                            </td>
                            <td id="achieve_edu_point_3">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_ed01">
                            <td class="border-l">
                                주소바탕학습
                            </td>
                            <td>
                                회수 * 1.0
                            </td>
                            <td class="text-left">

                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_4" />
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_4" ></textarea>
                            </td>
                            <td id="achieve_edu_point_4">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_ed01">
                            <td class="border-l">
                                4학년자유실습,<br />
                                2학년 임상술기학
                            </td>
                            <td>
                                회수 * 0.5
                            </td>
                            <td class="text-left">
                                4학년자율학습(지도 혹은 강의) 기본단위 2시간을 1횟수로 함.
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_5" />
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_5" ></textarea>
                            </td>
                            <td id="achieve_edu_point_5">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_ed01">
                            <td class="border-l">
                                임상실습 중<br />
                                OSCE/CPX 전담교수
                            </td>
                            <td>
                                4주(주당 30분 기준) * 0.5
                            </td>
                            <td class="text-left">
                                CPX는 가중치 2, <span class="color-red">위탁형태인 경우 20% 인정함.</span>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_6"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_6" ></textarea>
                            </td>
                            <td id="achieve_edu_point_6">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_ed01">
                            <td class="border-l">
                                강좌책임/간사교수<br />
                                (해당학년도 1년간)
                            </td>
                            <td>
                                (책임+간사과목수) * 2
                            </td>
                            <td class="text-left">
                                다수인 경우(1/인원수)로 환산함.<br />
                                상한선 3과목, <span class="color-red">해당년도 강의계획서 기준</span>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_7"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_7" ></textarea>
                            </td>
                            <td id="achieve_edu_point_7">

                            </td>
                        </tr>
                        <tr class="auth_category_ed01">
                            <td class="border-l">
                                임상실습 교육책임<br />
                                (해당학년도 1년간)
                            </td>
                            <td>
                                1년 2점, 6개월 1.5점,<br />
                                개월 미만 0.5점,<br />
                                1개월 미만은 주당 0.1점
                            </td>
                            <td class="text-left color-red">
                                3,4학년 임상실습지침서 기준
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_8"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_8" ></textarea>
                            </td>
                            <td id="achieve_edu_point_8">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_ed01">
                            <td class="border-l color-red">
                                서브인턴 교육책임<br />
                                (해당학년도 1년간)
                            </td>
                            <td class="color-red">
                                0.5점/1주
                            </td>
                            <td class="text-left color-red">
                                3,4학년 임상실습지침서 기준
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_9"/>
                            </td>
                            <td class="vt-top">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_9" ></textarea>
                            </td>
                            <td id="achieve_edu_point_9">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_ed02">
                            <td>
                                강의 평가
                            </td>
                            <td>
                                평가점수
                            </td>
                            <td>
                                개별교수의 평균값
                            </td>
                            <td class="text-left">
                                강의평가가 없는 경우 0점
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_10"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_10" ></textarea>
                            </td>
                            <td id="achieve_edu_point_10">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_ed03">
                            <td>
                                시험관
                            </td>
                            <td>
                                * 교내CPX/OSCE<br>
                                * 대경모의시험CPX<br>
                                * 국시 실기시험<br>
                                * 임상술기학<br>
                                * 골학구두시험
                            </td>
                            <td>
                                회수<span class="color-red">(8시간 참여/1일)</span> * 2.0
                            </td>
                            <td class="text-left">
                                골학구두시험은 가중치 0.5<br />
                                임상술기학은 가중치 0.5
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_11"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_11" ></textarea>
                            </td>
                            <td id="achieve_edu_point_11">

                            </td>
                        </tr>
                        <tr class="auth_category_ed04">
                            <td rowspan="3">
                                문항 및 교육모듈 개발
                            </td>
                            <td>
                                모의고사 학력평가 문제 출제 문항수<br>
                               * 공동시험감독<br>
* PBL 신규 사례 개발<br>
* OSCE/CPX 문항 검토 <br>
* PBL 사례 개발자 모듈 설명회 
                                
                            </td>
                            <td>
                                문항당 0.05점<br>
                                X 0.5<br>
X2<br>
X0.5<br>
X0.25
                                
                            </td>
                            <td class="text-left">
                                상한선 2.5점
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_12"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_12" ></textarea>
                            </td>
                            <td id="achieve_edu_point_12">

                            </td>
                        </tr>
                        <tr class="auth_category_ed04">
                            <td class="border-l">
                                PBL 등
                            </td>
                            <td>
                                회수 * 2.0
                            </td>
                            <td class="text-left">
교수학습센터 프로그램 참가<br>
교수법개선, 수업개발 <br>
가점의 총합 X 0.1 <br>
상한선 2점 

                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_13"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_13" ></textarea>
                            </td>
                            <td id="achieve_edu_point_13">

                            </td>
                        </tr>
                        <tr class="auth_category_ed04">
                            <td class="border-l">
                                의학교육학회 세미나/심포지움 참석,<br />
                                자체의학교육세미나(동.하계)
                            </td>
                            <td>
                                회수 * 1.0
                            </td>
                            <td class="text-left">
                                교육학회 세미나/심포지움 참석은 상한선을 두어 3.0점을 최대로 함.<br />
                                2시간 미만은 가중치 0.5
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_unit_14"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_14" ></textarea>
                            </td>
                            <td id="achieve_edu_point_14">

                            </td>
                        </tr>
                        <tr class="total-score-tr">
                            <td colspan="2">
                                총점
                            </td>
                            <td colspan="5" id="totalResultPoint_edu">
                                00.00점
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            </div>
            <!-- //.tab1 -->
            
            <div class="tab-content tab2 mg-b40">
				<div class="list-table-contents">
           		<table class="service-table co-tabs-table autoheight">
                    <colgroup>
                        <col width="80" />
                        <col width="100" />
                        <col width="180" />
                        <col width="80" />
                        <col width="150" />
                        <col width="100" />
                        <col width="" />
                        <col width="" />
                        <col width="50" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>
                                업적내용
                            </th>
                            <th colspan="2">
                                연구 평가 항목
                            </th>
                            <th>
                                기준점수
                            </th>
                            <th>
                                비고
                            </th>
                            <th>
                                건수
                            </th>
                            <th>
                                내용
                            </th>
                            <th>
                                증빙서류
                            </th>
                            <th>
                                점수
                            </th>
                        </tr>
                        
                        <tr class="auth_category_re01">
                            <td rowspan="5">
                                논문
                            </td>
                            <td rowspan="2">
                                SCI급 논문(SCI, SCIE, SCOPUS, SSCI, AHCI 등)
                            </td>
                            <td class="text-left">
                                Impact factor(IF) ≥ 1.0, X<br />[(IF/2)+1]
                            </td>
                            <td>
                                30점
                            </td>
                            <td class="text-left">
                                &lt;주 3> 참조
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_re_unit_1"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="논문 명"  id="achieve_re_contents_1"  ></textarea>
                            </td>
                            <td class="service-table-up-td" id="achieve_file_1">
                            	<!--
                                <p>
                                    <span class="text-bold"><a>3건</a></span>
                                </p>
                                 <a class="btn-down">다운로드</a>
                                <button type="button" class="btn-service-popup">등록</button>
                                -->
								<button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 1);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td id="achieve_re_point_1">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_re01">
                            <td class="border-l text-left">
                                IF＜1.0
                            </td>
                            <td>
                                30점
                            </td>
                            <td class="text-left">
                                &lt;주 4> 참조
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_2"/>
                            </td>
                            <td class="textarea-td autoheight">
                                <textarea cols="20" rows="1"  id="achieve_re_contents_2"></textarea>
                            </td>
                            <td class="service-table-up-td"  id="achieve_file_2">
                            	<button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 2);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td  id="achieve_re_point_2">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_re01">
                            <td colspan="2" class="border-l text-left">
                                한국연구재단 등재 및 등재후보 학술지 자연 및 인문사회계열
                            </td>
                            <td>
                                15점
                            </td>
                            <td>
                                
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_3"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1"  id="achieve_re_contents_3"></textarea>
                            </td>
                            <td class="service-table-up-td"  id="achieve_file_3">
                            	<button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 3);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td id="achieve_re_point_3">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_re01">
                            <td colspan="2" class="border-l text-left">
                                기타 국제 학술지 자연 및 인문사회계열
                            </td>
                            <td>
                                10점
                            </td>
                            <td>

                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_4"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1"  id="achieve_re_contents_4"></textarea>
                            </td>
                            <td class="service-table-up-td"  id="achieve_file_4">
                            	<button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 4);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td id="achieve_re_point_4">

                            </td>
                        </tr>
                        <tr class="auth_category_re01">
                            <td colspan="2" class="border-l text-left">
                                국내 전국규모 학술지 자연 및 인문사회계열
                            </td>
                            <td>
                                5점
                            </td>
                            <td>

                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_5"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1"  id="achieve_re_contents_5"></textarea>
                            </td>
                            <td class="service-table-up-td"  id="achieve_file_5">
                            	<button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 5);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td id="achieve_re_point_5">

                            </td>
                        </tr>
                        <tr class="auth_category_re02">
                            <td rowspan="4">
                                저술
                            </td>
                            <td rowspan="2">
                                저서, 교재
                            </td>
                            <td class="text-left">
                                초판
                            </td>
                            <td>
                                30점
                            </td>
                            <td rowspan="2" class="text-left">
                                국내는 가중치 0.5
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_6"/>
                            </td>
                            <td rowspan="2" class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_re_contents_6"></textarea>
                            </td>
                            <td rowspan="2" class="service-table-up-td" id="achieve_file_6">
                            	<button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 6);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td id="achieve_re_point_6">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_re02">
                            <td class="text-left border-l">
                                개정판
                            </td>
                            <td>
                                10점
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_7"/>
                                 <textarea cols="1" rows="1" placeholder="unit7" id="achieve_re_contents_7" style="display:none" readonly></textarea>
                            </td>
                            <td id="achieve_re_point_7">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_re02">
                            <td rowspan="2" class="border-l">
                                역서, 편저 등
                            </td>
                            <td class="text-left">
                                초판
                            </td>
                            <td>
                                10점
                            </td>
                            <td rowspan="2">

                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_8"/>
                            </td>
                            <td rowspan="2" class="textarea-td">
                                <textarea cols="20" rows="1"  id="achieve_re_contents_8"></textarea>
                            </td>
                            <td rowspan="2" class="service-table-up-td" id="achieve_file_8">
                            	<button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 8);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td id="achieve_re_point_8">

                            </td>
                        </tr>
                        <tr class="auth_category_re02">
                            <td class="text-left border-l">
                                개정판
                            </td>
                            <td>
                                5점
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_9"/>
                                <textarea cols="1" rows="1" placeholder="unit9" id="achieve_re_contents_9" style="display:none" readonly></textarea>
                            </td>
                            <td id="achieve_re_point_9">

                            </td>
                        </tr>
                        <tr class="auth_category_re03">
                            <td>
                                기타
                            </td>
                            <td colspan="2" class="text-left">
                                등록된 국외특허
                            </td>
                            <td>
                                10점
                            </td>
                            <td rowspan="3" class="text-left">
                                기타 연구활동 총합이 20점을 초과하지 못함.<br />
                                <span class="color-red">등록일 기준, 등록증 제출함.</span>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_10"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_re_contents_10"></textarea>
                            </td>
                            <td class="service-table-up-td"  id="achieve_file_10">
                                <button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 10);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td  id="achieve_re_point_10">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_re04">
                            <td>
                                연구
                            </td>
                            <td colspan="2" class="text-left">
                                등록된 국내특허
                            </td>
                            <td>
                                5점
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_11"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1"  id="achieve_re_contents_11"></textarea>
                            </td>
                            <td class="service-table-up-td"  id="achieve_file_11">
                                <button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 11);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td  id="achieve_re_point_11">

                            </td>
                        </tr>
                        <tr class="auth_category_re05">
                            <td>
                                활동
                            </td>
                            <td colspan="2" class="text-left">
                                등록된 소프트웨어
                            </td>
                            <td>
                                5점
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_12"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1"  id="achieve_re_contents_12"></textarea>
                            </td>
                            <td class="service-table-up-td"  id="achieve_file_12">
                                <button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 12);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td id="achieve_re_point_12">
								
                            </td>
                        </tr>
                        <tr class="auth_category_re06">
                            <td>
                                연구비
                            </td>
                            <td colspan="2" class="text-left">
                                간접연구비 기준 100만원 미만은 1점, 그 이후 100만원당 1점씩 가산
                            </td>
                            <td>
                                -
                            </td>
                            <td class="text-left">
                                간접연구비가 의학전문대학원으로 배정된 것
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_13"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_re_contents_13"></textarea>
                            </td>
                            <td class="service-table-up-td"  id="achieve_file_13">
                                <button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 13);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td  id="achieve_re_point_13">
                                
                            </td>
                        </tr>
                        <tr class="total-score-tr">
                            <td colspan="4">
                                총점
                            </td>
                            <td colspan="5" id="totalResultPoint_research">
                                
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                
            	<div class="guide-txt-table-div">
                    <p>
                        &lt;주&gt;
                    </p>
                    <p>
                        1. 통합정보시스템(yes.knu.ac.kr)에 등록/승인된 연구업적에 한하여 인정한다. 제 1저자, 교신저자, SCI급 여부 및 발간일의 연월일 까지 정확하게 입력하여야 한다. 미승인으로 인한 불이익을 방지하기 위하여 수시입력/승인요청을 권장한다. 중복 입력된 업적이 없도록 확인한다.
                    </p>
                    <p>
                        2. 공동저자 인정기준은 다음과 같다.
                    </p>
                    <table class="service-table">
                        <caption style="display:table-caption;">
                            &lt;발표2&gt; 연구실적을 공(동)저자 인정 기준표
                        </caption>
                        <colgroup>
                            <col />
                            <col />
                            <col />
                            <col />
                            <col width="70" />
                        </colgroup>
                        <tbody>
                            <tr>
                                <th rowspan="2">
                                    총저자수<br />
                                    (n)
                                </th>
                                <th colspan="3">
                                    인정비율(%)
                                </th>
                                <th rowspan="2">
                                    비고
                                </th>
                            </tr>
                            <tr>
                                <th class="border-l">
                                    주저자 (제1저자, 교신저자)
                                </th>
                                <th>
                                    공동저자
                                </th>
                                <th>
                                    공저자
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    1인
                                </td>
                                <td>
                                    100%
                                </td>
                                <td>
                                    -
                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    2인 이상
                                </td>
                                <td>
                                    {200/(n+1)}%
                                </td>
                                <td>
                                    {100/(n+1)}%
                                </td>
                                <td>
                                    {100/(n+10)}%
                                </td>
                                <td>

                                </td>
                            </tr>
                        </tbody>
                    </table>

                    ※ 논문과 저술영역에서 주저자인 경우에는 총저자수가 4인 이상이라도 n을 4로 계산함.<br />

                    ※ 공동저자는 주저자의 표기가 있는 경우에 나머지 저자를 일컫는다.<br />

                    ※ 주저자의 표기가 없는 연구실적물의 저자는 공저자로 간주한다.

                    <p>
                        3. Impact factor(IF) 기준은 업적자료제출마감일 기준으로 산출 한다. SCI급 IF가 1.0 이상인 잡지에 출판된 논문의 경우 다음과 같이 계산한다. 
                        점수 = 기준점수 x (&lt;별표2&gt;의 공동저자 인정 비율) x [(IF/2)+1]
                    </p>
                    <p>
                        4. 이외의 SCI급(IF가 1.0 미만인 잡지에 출판된) 논문, 저술, 학술발표, 기타 연구 활동의 경우 다음과 같이 계산한다.점수 = 기준점수 * (&lt;별표2&gt;의 공동저자 인정 비율)로 계산한다.
                    </p>
                    <p>
                        5. 연구비 평가항목에서 간접연구비 계산식은 1+(A/100만원)=B이며, 이 경우 A는 개인 간접연구비, B의 소수점 이하는 0으로 처리한다.
                    </p>
                    <p>
                        6. 연구비 평가항목에서 기간 중에 의학전문대학원으로 들어온 간접연구비 총액을 기준으로 평가하고, 공동연구과제는 분담액으로 하되, 분담액을 정하지 않은 경우는 연구 보고서를 기준으로 &lt;위 별표의 공동저자 인정비율&gt;에 따라 산정한 금액으로 한다.
                    </p>
                    <p class="color-red">
                        7. 특허는 등록일 기준으로 하며, 산·단 승인한 자료만 평가대상으로 인정한다. 참고로 산·단 출원 특허는 자동으로 승인절차가 진행되지만 타 기관을 통한 특허는 반드시 산단에 등록하고 승인절차를 받아야 한다. 등록증을 증빙자료로 제출함.
                    </p>
                    <p class="color-red">
                        8. 특허의 인정비율은 YES 시스템상으로 확인할 수 없어 등록인원으로 인정비율을 정하되 연구실적물 주저자 기준으로 산출한다.
                    </p>
                    <p class="color-red">
                        9. 저술활동의 업적의 인정비율은 연구실적물 주저자 기준으로 산출한다.
                    </p>
                </div>
                <!-- //.guide-txt-table-div -->
            </div>
            </div>
            <!-- //.tab2 -->
            
            <div class="tab-content tab3 mg-b40">
                <div class="list-table-contents">
                    <p class="tab-content-table-title">
                        [교내 봉사]
                    </p>
                    <table class="service-table co-tabs-table">
                        <colgroup>
                            <col width="80" />
                            <col width="110" />
                            <col width="250" />
                            <col width="80" />
                            <col width="156" />
                            <col width="90" />
                            <col width="200" />
                            <col width="80" />
                            <col width="60" />
                        </colgroup>
                        <tbody id="tbody_1">
                            <tr>
                                <th>
                                    업적내용
                                </th>
                                <th colspan="2">
                                    평가항목(평가기간이내)
                                </th>
                                <th>
                                    기준점수
                                </th>
                                <th>
                                    비고
                                </th>
                                <th>
                                    건수(회수)
                                </th>
                                <th>
                                    내용
                                </th>
                                <th>
                                    증빙서류
                                </th>
                                <th>
                                    점수
                                </th>
                            </tr>
                            <tr>
                                <td rowspan="7">
                                    본부보직
                                </td>
                                <td colspan="2" class="text-left">
                                    부총장, 처장, 부처장 등<br />
                                    책임시수 3시간에 준하는 보직
                                </td>
                                <td>
                                    10
                                </td>
                                <td rowspan="2" class="text-left">
                                    경북대학교 수업 관리 지침&lt;보직교원 주당 교수시간 기준표&gt;에 준함
                                </td>
                                <td id="serviceCode_count_1_1">
                                    
                                </td>
                                <td class="text-left" id="serviceCode_content_1_1">
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_1">
                                </td>
                                <td id="serviceCode_point_1_1">
                                    
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-left border-l">
                                    입학본부장, 국제교류원장,<br /> 입학본부부본부장, 국제교류원부원장,<br />
                                    인재개발원장, 인재개발부원장 등<br /> 책임시수 3시간에 준하는 보직
                                </td>
                                <td>
                                    10
                                </td>
                                <td id="serviceCode_count_1_2">
                                    
                                </td>
                                <td class="text-left" id="serviceCode_content_1_2">
                                   
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_2">
                                   
                                </td>
                                <td id="serviceCode_point_1_2">
                                    
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-left border-l">
                                    신문방송사주간, 정보전산원장,생활관장,<br /> 공동실험실습관장,보건진료소장
                                </td>
                                <td>
                                    2
                                </td>
                                <td>

                                </td>
                                <td id="serviceCode_count_1_3">
                                
                                </td>
                                <td class="text-left" id="serviceCode_content_1_3">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_3">

                                </td>
                                <td id="serviceCode_point_1_3">

                                </td>
                                
                            </tr>
                            <tr>
                                <td colspan="2" class="text-left border-l">
                                    교수회 의장, 부의장, 처장, 부처장
                                </td>
                                <td>
                                    7
                                </td>
                                <td>

                                </td>
                                <td id="serviceCode_count_1_4">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_4">
                                   
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_4">

                                </td>
                                <td id="serviceCode_point_1_4">

                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-left border-l">
                                    기숙사 담당(명의관 포함), 부설연구소장
                                </td>
                                <td>
                                    2
                                </td>
                                <td>

                                </td>
                                <td id="serviceCode_count_1_5">

                                </td>
								<td class="text-left" id="serviceCode_content_1_5">
                                   
                                </td>
                                
                                <td class="service-table-up-td" id="serviceCode_file_1_5">

                                </td>
                                <td id="serviceCode_point_1_5">

                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-left border-l">
                                    특별학부장 (자율전공,글로벌인재)
                                </td>
                                <td>
                                    2
                                </td>
                                <td>

                                </td>
                                <td id="serviceCode_count_1_6">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_6">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_6">

                                </td>
                                <td id="serviceCode_point_1_6">

                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-left border-l">
                                    발전기금 담당 책임자
                                </td>
                                <td>
                                    3
                                </td>
                                <td>

                                </td>
                                <td id="serviceCode_count_1_7">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_7">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_7">

                                </td>
                                <td id="serviceCode_point_1_7">

                                </td>
                            </tr>
                            <tr>
                                <td rowspan="6">
                                    교내 비보직 봉사-의전원외
                                </td>
                                <td rowspan="3">
                                    학생지도
                                </td>
                                <td class="text-left">
                                    학생단체(동아리)지도교수
                                </td>
                                <td>
                                    1
                                </td>
                                <td>

                                </td>
                                <td id="serviceCode_count_1_8">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_8">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_8">

                                </td>
                                <td id="serviceCode_point_1_8">

                                </td>
                            </tr>
                            <tr>
                                <td class="text-left border-l">
                                    학생상담자원봉사교수
                                </td>
                                <td>
                                    1
                                </td>
                                <td>

                                </td>
                                <td id="serviceCode_count_1_9">

                                </td>
								<td class="text-left" id="serviceCode_content_1_9">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_9">

                                </td>
                                <td id="serviceCode_point_1_9">

                                </td>
                            </tr>
                            <tr>
                                <td class="text-left border-l">
                                    학생관련 특수 프로그램 지도 교수
                                </td>
                                <td>
                                    1
                                </td>
                                <td>

                                </td>
                                <td id="serviceCode_count_1_10">

                                </td>
                               <td class="text-left" id="serviceCode_content_1_10">
                                   
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_10">

                                </td>
                                <td id="serviceCode_point_1_10">

                                </td>
                            </tr>
                            <tr>
                                <td rowspan="3" class="border-l">
                                    국제협력봉사
                                </td>
                                <td class="text-left">
                                    해외대학/연구소 등과의 학술협력 주도
                                </td>
                                <td>
                                    0.5
                                </td>
                                <td rowspan="3" class="text-left">
                                    1. 국제교류원장이 정하는 활동만을 대상으로 한다.<br />
                                    2. 연간 최대 2점을 초과하지 않는다.
                                </td>
                                <td id="serviceCode_count_1_11">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_11">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_11">

                                </td>
                                <td id="serviceCode_point_1_11">

                                </td>
                            </tr>
                            <tr>
                                <td class="text-left border-l">
                                    학생인솔 해외 프로그램 운영
                                </td>
                                <td>
                                    1
                                </td>
                                <td id="serviceCode_count_1_12">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_12">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_12">

                                </td>
                                <td id="serviceCode_point_1_12">

                                </td>
                            </tr>
                            <tr>
                                <td class="text-left border-l">
                                    기타 국제협력활동
                                </td>
                                <td>
                                    0.2
                                </td>
                                <td id="serviceCode_count_1_13">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_13">
                                   
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_13">

                                </td>
                                <td id="serviceCode_point_1_13">

                                </td>
                            </tr>
                            <tr>
                                <td rowspan="3">
                                    의전원<br />
                                    보직자
                                </td>
                                <td colspan="2" class="text-left">
                                    원장, 병원장 등 책임시수 3시간에 준하는 보직.
                                </td>
                                <td>
                                    20점 / 년
                                </td>
                                <td class="text-left">
                                    경북대학교 수업 관리 지침&lt;보직교원 주당 교수시간 기준표&gt;에 준함
                                </td>
                                <td id="serviceCode_count_1_14">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_14">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_14">

                                </td>
                                <td id="serviceCode_point_1_14">

                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-left border-l">
                                    부원장(교무,학생,연구,교육) 및 부원장보실장<br />(기획, 입학, 임상수기, 대외협력),<br />
                                    기타(BK사업단장 등), 교수회의장
                                </td>
                                <td>
                                    12점 / 년
                                </td>
                                <td class="text-left">
                                    부원장보는 가중치 0.5
                                </td>
                                <td id="serviceCode_count_1_15">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_15">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_15">

                                </td>
                                <td id="serviceCode_point_1_15">

                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-left border-l">
                                    대학원 학과장 (역학 및 건강증진학과장, 보건관리학과장,<br /> 보건학과장,
                                    법정의학과장, 과학수사학과장, 법의간호학과장,<br /> 의학과장, 의과학과장, 의용생체공학과장, 의료정보학과장),<br /> 국가 지정연구센터장, 교수회 부의장, 간사교실주임,<br /> BK사업단부단장/팀장, 학년담임 등
                                </td>
                                <td>
                                    6점 / 년
                                </td>
                                <td class="text-left">
                                    다수인 경우 인원수로 나눔
                                </td>
                                <td id="serviceCode_count_1_16">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_16">

                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_16">

                                </td>
                                <td id="serviceCode_point_1_16">

                                </td>
                            </tr>
                            <tr>
                                <td rowspan="5">
                                    의전원<br />
                                    비보직자
                                </td>
                                <td colspan="2" class="text-left">
                                    의전원장이 인정한 교내 봉사<br />
                                    -회의개최 의전원 위원회 위원에 대한 평점
                                </td>
                                <td>
                                    2점 / 년
                                </td>
                                <td class="text-left">
                                    위원장 / 간사는 가중치 2<br />
                                    &lt;주 3,4&gt; 참조
                                </td>
                                <td id="serviceCode_count_1_17">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_17">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_17">

                                </td>
                                <td id="serviceCode_point_1_17">

                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-left border-l">
                                    의학과장이 인정한 교내 봉사<br />
                                    -교내 활동 참여(입학시험면접, 출제 등), 학생지도 활동<br />(학생체육행사, 졸업여행 인솔, MMPI검사, 신입생OT/새터 등),<br /> 학교 행사 참여(신입생 환영회, 국외 대학 교류활동 등)
                                </td>
                                <td>
                                    1점 / 회
                                </td>
                                <td class="text-left">
                                    비 보직자만 해당함, 2시간미만은 가중치 0.5
                                </td>
                                <td id="serviceCode_count_1_18">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_18">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_18">

                                </td>
                                <td id="serviceCode_point_1_18">

                                </td>
                            </tr>
                            <tr>
                                <td rowspan="3" class="border-l">
                                    학교발전기여도
                                </td>
                                <td class="text-left">
                                    대학발전기금 및 장학기금(현금 또는) 물품
                                </td>
                                <td>

                                </td>
                                <td class="text-left">
                                    3천만 원 이상 (9점)<br />
                                    2천만 원 이상 (7점)<br />
                                    1천만 원 이상 (5점)<br />
                                    5백만 원 이상 (3점)<br />
                                    100만 원 이상 (1점)
                                </td>
                                <td id="serviceCode_count_1_19">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_19">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_19">

                                </td>
                                <td id="serviceCode_point_1_19">

                                </td>
                            </tr>
                            <tr>
                                <td class="text-left border-l">
                                    연구프로젝트 신청건수
                                </td>
                                <td>
                                    1점 / 건
                                </td>
                                <td class="text-left">
                                    5천만 원 이상 가중치 2점
                                </td>
                                <td id="serviceCode_count_1_20">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_20">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_20">

                                </td>
                                <td id="serviceCode_point_1_20">

                                </td>
                            </tr>
                            <tr>
                                <td class="text-left border-l">
                                    대학의 신규 주요사업(국책사업 및 대학종합평가 포함) 계획서 및 보고서 작성)
                                </td>
                                <td>
                                    2점 / 건
                                </td>
                                <td>

                                </td>
                                <td id="serviceCode_count_1_21">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_21">
                                   
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_21">

                                </td>
                                <td  id="serviceCode_point_1_21">

                                </td>
                            </tr>
                            <tr class="total-score-tr">
                                <td colspan="4">
                                    소계
                                </td>
                                <td colspan="6" id="serviceCode_point_1_sum">
                                    0 점
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- //.service-table -->

                    <p class="tab-content-table-title">
                        [교외 봉사]
                    </p>
                    <table class="service-table co-tabs-table">
                        <colgroup>
                            <col width="80" />
                            <col width="" />
                            <col width="80" />
                            <col width="156" />
                            <col width="90" />
                            <col width="200" />
                            <col width="80" />
                            <col width="50" />
                        </colgroup>
                        <tbody>
                            <tr>
                                <th>
                                    업적내용
                                </th>
                                <th>
                                    평가항목(평가기간이내)
                                </th>
                                <th>
                                    기준점수
                                </th>
                                <th>
                                    비고
                                </th>
                                <th>
                                    건수(회수)
                                </th>
                                <th>
                                    내용
                                </th>
                                <th>
                                    증빙서류
                                </th>
                                <th>
                                    점수
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    학술봉사
                                </td>
                                <td class="text-left">
                                    국내외 학회 이사장, 회장, 국제학술대회 조직위원장/이사
                                    국외 SCI, SCIE, SSCI, AHCI 등재 학술지의 편집(부)위원장/위원
                                    한국연구재단 등재 학술지 편집위원(0.5점)
                                </td>
                                <td>
                                    6점 / 년
                                </td>
                                <td rowspan="2" class="text-left">
                                    3건만 인정함.
                                    초 세부학회 (대한 의학회 명시된 학회만 인정)는 가중치(0.5) 부여함.
                                    연구재단 등재지이상 발간 학회는 초세부학회에 준함.
                                    연구평가위원은 출장서류로 증빙함. 고시위원은 업적평가위원회에서 인정한 경우에 한함. 편집위원은 가중치 0.5
                                </td>
                                <td id="serviceCode_count_2_22">

                                </td>
                                <td  id="serviceCode_content_2_22">
                                	<!-- 
                                    <div class="service-table-content-div"
                                    	<!-- 
                                        <p>
                                            07.01 ~ 12.31
                                        </p>
                                        <p>
                                            입학본부장
                                        </p>
                                    </div>
                                    -->
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_2_22">
                                	<!-- 
                                    <p>
                                        <span class="text-bold"><a>3건</a></span>
                                    </p>
                                    <a class="btn-down">다운로드</a>
                                     -->
                                </td>
                                <td  id="serviceCode_point_2_22">
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>

                                </td>
                                <td class="text-left">
                                    연구평가위원(한국연구재단, 보건산업진흥원, 학력평가, MEET, 의학관련 고시위원
                                </td>
                                <td>
                                    1점 / 건
                                </td>
                               <td id="serviceCode_count_2_23">

                                </td>
                                <td  id="serviceCode_content_2_23">

                                </td>
                                <td class="service-table-up-td"  id="serviceCode_file_2_23">
  
                                </td>
                                <td id="serviceCode_point_2_23">

                                </td>
            	            </tr>
                            <tr>
                                <td>
                                    의료봉사
                                </td>
                                <td class="text-left">
                                    국내외 의료봉사
                                </td>
                                <td>
                                    1점 / 건
                                </td>
                                <td class="text-left">
                                    1일 이상인 경우 인정.
                                </td>
                                <td  id="serviceCode_count_2_24">

                                </td>
                                <td  id="serviceCode_content_2_24">

                                </td>
                                <td class="service-table-up-td"  id="serviceCode_file_2_24">

                                </td>
                                <td id="serviceCode_point_2_24">

                                </td>
                            </tr>
                            <tr>
                                <td rowspan="4">
                                    기타
                                </td>
                                <td class="text-left">
                                    전공 관련 국제기구 위원
                                </td>
                                <td>

                                </td>
                                <td class="text-left">
                                    UN, UNICEF, WHO급
                                </td>
                                <td id="serviceCode_count_2_25">

                                </td>
                                <td  id="serviceCode_content_2_25">

                                </td>
                                <td class="service-table-up-td"  id="serviceCode_file_2_25">

                                </td>
                                <td id="serviceCode_point_2_25">

                                </td>
                            </tr>
                            <tr>
                                <td class="border-l text-left">
                                    정부기간 (가중치=1), 지자체(가중치=0.4), 국영기업체 및 이에 준하는 기관(가중치=0.2)의 각종 위원
                                </td>
                                <td>
                                    1점 / 년
                                </td>
                                <td class="text-left">
                                    &lt;주 4&gt; 참조, 3건만 인정함.
                                </td>
                                <td  id="serviceCode_count_2_26">

                                </td>
                                <td  id="serviceCode_content_2_26">

                                </td>
                                <td class="service-table-up-td"  id="serviceCode_file_2_26">

                                </td>
                                <td id="serviceCode_point_2_26">

                                </td>
                            </tr>
                            <tr>
                                <td class="border-l text-left">
                                    특별강연 (정부기관, 지자체, 국영기업체 및 이에 준하는 기관이 주관)
                                </td>
                                <td>
                                    1점 / 건
                                </td>
                                <td class="text-left">
                                    3건만 인정함.
                                </td>
                                <td  id="serviceCode_count_2_27">

                                </td>
                                <td  id="serviceCode_content_2_27">

                                </td>
                                <td class="service-table-up-td"  id="serviceCode_file_2_27">

                                </td>
                                <td id="serviceCode_point_2_27">

                                </td>
                            </tr>
                            <tr>
                                <td class="border-l text-left">
                                    동창회 및 의사회 (부)회장, 상임이사
                                </td>
                                <td>
                                    1점 / 년
                                </td>
                                <td  id="serviceCode_count_2_28">

                                </td>
                                <td>

                                </td>
                                <td  id="serviceCode_content_2_28">

                                </td>
                                <td class="service-table-up-td"  id="serviceCode_file_2_28">

                                </td>
                                <td id="serviceCode_point_2_28">

                                </td>
                            </tr>
                            <tr>
                                <td rowspan="3">
                                    수상실적
                                </td>
                                <td class="text-left">
                                    전공 관련 국제기구 위원
                                </td>
                                <td>
                                    4점 / 건
                                </td>
                                <td rowspan="3" class="text-left">
                                    UN, UNICEF, WHO급
                                </td>
                                <td id="serviceCode_count_2_29">

                                </td>
                                <td id="serviceCode_content_2_29">

                                </td>
                                <td class="service-table-up-td"  id="serviceCode_file_2_29">

                                </td>
                                <td id="serviceCode_point_2_29">

                                </td>
                            </tr>
                            <tr>
                                <td class="border-l text-left">
                                    특별강연 (정부기관, 지자체, 국영기업체 및 이에 준하는 기관이 주관)
                                </td>
                                <td>
                                    4점 / 건
                                </td>
                                <td  id="serviceCode_count_2_30">

                                </td>
                                <td  id="serviceCode_content_2_30">

                                </td>
                                <td class="service-table-up-td"  id="serviceCode_file_2_30">

                                </td>
                                <td  id="serviceCode_point_2_30">

                                </td>
                            </tr>
                            <tr>
                                <td class="border-l text-left">
                                    동창회 및 의사회 (부)회장, 상임이사
                                </td>
                                <td>
                                    2점 / 년
                                </td>
                                <td  id="serviceCode_count_2_31">

                                </td>
                                <td  id="serviceCode_content_2_31">

                                </td>
                                <td class="service-table-up-td"  id="serviceCode_file_2_31">

                                </td>
                                <td id="serviceCode_point_2_31">

                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- //.co-tabs-table -->

                    <p class="tab-content-table-title">
                        [총점]
                    </p>
                    <table class="service-table co-tabs-table">
                        <colgroup>
                            <col width="" />
                            <col width="" />
                            <col width="" />
                            <col width="" />
                            <col width="" />
                        </colgroup>
                        <tbody>
                            <tr>
                                <th>
                                    구분
                                </th>
                                <th>
                                    교내봉사
                                </th>
                                <th>
                                    교외봉사
                                </th>
                                <th>
                                    합계
                                </th>
                                <th>
                                    비고
                                </th>
                            </tr>
                            <tr class="total-score-tr">
                                <td>
                                    점수
                                </td>
                                <td id="serviceCode_point_1_sum_sum">
                                   0
                                </td>
                                <td id="serviceCode_point_2_sum_sum">
                                   0
                                </td>
                                <td id="serviceCode_point_total">
                                   0 점
                                </td>
                                <td>

                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="guide-txt-table-div">
                        <p>
                            &lt;주&gt;
                        </p>
                        <p>
                            1. [봉사영역 항목별 평가표]의 교내봉사 부분 중 교내 공문으로 발령 받은 경우는 의전원 행정실에서 작성한다. 단 교내 공문으로 발령 받았더라도 비고란에 조건이 있는 사항과 공문으로 발령 받지 않은 봉사내역은 내용과 함께 근거 자료를 제출하여야 한다. 교외봉사업적은 봉사영역 평가 항목별 평점표를 작성하고 근거자료를 제출 한다.
                        </p>
                        <p class="color-red">
                            2. 동일기간 보직을 겸직하는 경우 점수가 상위인 보직의 점수를 부여한다. 교실주임의 경우 주임교수회의를 1/3 참석하지 않은 경우 가점의 1/2만 인정한다.
                        </p>

                        <p>
                            3. 상임위원회, 운영위원회, 상임소위원회 위원, 기타 지원시설(공동기기실, 실험동물실, 의대도서관분관 등)의 장을 포함한다. 위원회 위원은 1/2 이상 회의 참석한 경우에만 가점을 부여하고, 참석여부는 회의록으로 확인한다.
                        </p>
                        <p>
                            4. 기간이 년 단위인 경우 1년 미만인 경우에는 기간을 반올림한 개월 수에 비례하여 환산한 점수를 부여한다.
                        </p>
                    </div>
                    <!-- //.guide-txt-table-div -->
                </div>
            </div>
            <!-- //.tab3 -->
        </div>
        <!-- //.content -->
    </div>
    <!-- //.container -->

    <%@ include file="../include/bottom.jsp" %>
    
</div>
<!-- //#wrap -->
<!-- 증빙서류 등록 팝업 영역 -->
    <div class="wrap-popup wrap-plan-popup hidden popup-thesis" id="uploadFileLayer">
    	<input type="hidden" name="uploadServiceCode" />
        <div class="popup-guide">
            <div class="popup-guide-header">
                <h2>
                    증빙서류
                </h2>
                <a class="btn-close-giude-popup" title="close" onclick="javascript:$('#uploadFileLayer').fadeOut(300);">
                    X
                </a>
            </div>
            <div class="popup-guide-content">
                <fieldset>
                	<!-- 
                    <div class="popup-input-file-up-div">
                        <p class="popup-fd-title">
                            파일 추가를 누르시면 파일을 여러 개 등록할 수 있습니다.
                        </p>
                        <button type="button" class="btn-add-file">
                            + 파일 추가
                        </button>
                    </div>
                     -->
                    <!-- //.popup-input-file-up-div -->

                    <div class="popup-input-file-up-div">
                        <!--<p class="popup-fd-title">
                            <span class="color-blue">
                                (<span class="word-cut">
                                SCI급 논문
                                </span>)
                            </span> 증빙서류 첨부내역
                        </p>-->
                        <div id="uploadFileList"></div>
                        <div class="btn-submit-div" onclick="javascript:$('#uploadFileLayer').fadeOut(300);">
                            <a>완료</a>
                        </div>
                    </div>
                    <!-- //.popup-input-file-up-div -->
                </fieldset>
            </div>
        </div>
    </div>
    <!-- //.wrap-popup -->
    
<script type="text/javascript" src="<c:url value="/js/serviceResult.js"/>"></script>
<script>
$(".service-nav a").removeClass("active");
$(".service-nav a").eq(0).addClass("active");
var userCode = "${sessionScope.userCode}";
 $(document).ready(function(){
    // 테이블 라인 배경
    $(".service-table tr:even").css("background","#fdfdfd");
     // textarea 자동 높이 맞춤
     function autoHeight(){
         $(".autoheight").on("keyup", "textarea", function (e){
             $(this).css("height", "auto");
             $(this).height( this.scrollHeight );
         });
         $(".autoheight").find("textarea").keyup();
     }autoHeight();
     // textarea 자동 포커스
     $(".autoheight td").click(function (){
         $(this).children("textarea").focus();
     });
     //탭메뉴
     $(".tab-content").hide();
     $(".tab-content:first").show();
     $(".tabs li").bind("click", function(){
         $(".tabs li").removeClass("active");
         $(this).addClass("active");
         $(".tab-content").hide();
         var activeTab = $(this).attr("data-to");
         $("." + activeTab).show();
         autoHeight();
     });
     
     
     $(".service-table-content-div").show();
    
     $("#menu_tab1").bind("click",function(){
    	 getAchieveProfessorResult_edu(userCode);
     });
 	 $("#menu_tab2").bind("click",function(){
 		getAchieveProfessorResult_research(userCode);
     });
 	 $("#menu_tab3").bind("click",function(){
 	     getServiceResult();
	 });
 	$("input").attr("disabled",true);
 	$("input").css("border","solid 0px");
 	$("textarea").attr("disabled",true);
 	$("textarea").css("border","solid 0px");

 	getPeriodInfo();
 	
});
function getAchieveProfessorResult_edu(userCode) {
	$.ajax({
		type: "POST",
	    url: '<c:url value="/admin/getAchieveProfessorResult"/>',
	    data: {
	     	"userCode": userCode
	      	,"achieveType": "e"
	    },
	    dataType: "json",
	    success: function(data, status) {
	     	console.debug(data);
	      	if (data.status == "success") {
				for(var i=0; i< data.result.length; i++){
					var row = data.result[i];
					var itemNum = row.education_item_num;
					//achieve_edu_unit_14  achieve_edu_point_14  achieve_edu_contents_14
					$("#achieve_edu_unit_" + itemNum).val(row.unit == "0" ? "" : row.unit);
					$("#achieve_edu_contents_" + itemNum).val(row.contents == "null"? "" : row.contents );
					$("#achieve_edu_point_" + itemNum).html(row.point == "0" ? "" : ToFloat(row.point));
					
					$(".achieveDataSubmit").html("수정");
				}
				$("textarea").each(function(){
				    $(this).css('height', 'auto' );
				    $(this).height( this.scrollHeight );
				});
				$("textarea").css("border","solid 1px #ccc");
				$(".score-input-td input").css("border","solid 1px #ccc");
				
				var totalPoint = 0.0;
				$("td[id^=achieve_edu_point_]").each(function(e){
					var point = $.trim($(this).html());
					if(point.length > 0){
			    		if(!isNaN(point)){
							totalPoint = parseFloat(totalPoint) + parseFloat(point);
							console.debug("totalPoint =>"+ totalPoint );
			    		}			
					}
				});
				console.debug("totalPoint =>"+ ToFloat(totalPoint));
				//totalPoint = parseFloat(totalPoint).toFixed(2);
				$("#totalResultPoint_edu").html(ToFloat(totalPoint) + " 점");
				
				
				$("input").attr("disabled",true);
			 	$("input").css("border","solid 0px");
			 	$("textarea").attr("disabled",true);
			 	$("textarea").css("border","solid 0px");
	        }
	    },
	    error: function(xhr, textStatus) {
			//alert("오류가 발생했습니다.");
			console.error(error);
	    }
	});
}
function getAchieveProfessorResult_research(userCode) {
	$.ajax({
        type: "POST",
        url: '<c:url value="/admin/getAchieveProfessorResult"/>',
        data: {
        	"userCode": userCode
        	,"achieveType": "r"
        },
        dataType: "json",
        success: function(data, status) {
        	console.debug("================================getAchieveProfessorResult");
        	console.debug(data);
        	if (data.status == "success") {
				for(var i=0; i< data.result.length; i++){
					var row = data.result[i];
					var itemNum = row.research_item_num;
					//achieve_edu_unit_14  achieve_edu_point_14  achieve_edu_contents_14
					$("#achieve_re_unit_" + itemNum).val(row.result_unit == -1 ? "" : row.result_unit);
					$("#achieve_re_contents_" + itemNum).val(row.result_contents);
					$("#achieve_re_point_" + itemNum).html(row.result_point == -1 ? "" : ToFloat(row.result_point));
					if(row.filecount > -1){
						var html = ""; //"<a href=\"javascript:uploadFile($(this), "+ row.research_item_num +");\">"+ row.filecount + "건</a>";
						html += "<p>";
						html += "<span class=\"text-bold\"><a>" + row.filecount + "건</a></span>";
						html += "</p>"
						html += "<a class=\"btn-down\" onClick=\"javascript:getResearchFileList("+ row.research_item_num +");\">다운로드</a>";
						$("#achieve_file_"+ itemNum).find(".fileCountDiv").html(html);
						
					}
				}
				/*
				$("[id^='achieve_re_contents_']").each(function(){
				    while($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
				        $(this).height($(this).height()+1);
				    };
				});
				*/
				$("textarea").each(function(){
				    $(this).css('height', 'auto' );
				    $(this).height( this.scrollHeight );
				});
				$("textarea").css("border","solid 1px #ccc");
				$(".score-input-td input").css("border","solid 1px #ccc");
				
				$("[id^='achieve_re_unit_']").attr("readonly",false);
				$("[id^='achieve_re_contents_']").attr("readonly",false);
				//calculateTotalPoint();
				
				var totalPoint = 0.0;
				$("td[id^=achieve_re_point_]").each(function(e){
					var point = $.trim($(this).html());
					if(point.length > 0){
			    		if(!isNaN(point)){
							totalPoint = parseFloat(totalPoint) + parseFloat(point);
							console.debug("totalPoint =>"+ totalPoint );
			    		}			
					}
				});
				console.debug("totalPoint =>"+ totalPoint);
				//totalPoint = parseFloat(totalPoint).toFixed(2);

				$("#totalResultPoint_research").html(ToFloat(totalPoint.toFixed(4)) + " 점");
				
				$("input").attr("disabled",true);
			 	$("input").css("border","solid 0px");
			 	$("textarea").attr("disabled",true);
			 	$("textarea").css("border","solid 0px");
        	}
        },
        error: function(xhr, textStatus) {
			//alert("오류가 발생했습니다.");
			console.error(error);
        }
    });
}
function getResearchFileList(seq) {
	console.debug("getFileList =>"+ seq);
	$.ajax({
        type: "POST",
        url: '<c:url value="/admin/getFileList"/>',
        data: {
        	"researchCode":seq //$(":input[name='uploadServiceCode']").val()
        	,"userCode" : userCode
        	,"achieveType" : "r"
        },
        dataType: "json",
        success: function(data, status) {
        	console.debug("getFileList ============");
        	console.debug(data);
        	if (data.status == "success") {
        		$("#uploadFileList").empty();
        		$.each(data.list, function(index) {
        			var fileHtml = "<div class=\"popup-input-del-div\">"
    					+ "<p>" + this.reg_date + "</p>"
    					+ "<p><a href=\"../download?fileName=" + this.file_name + "&orignalName=" + encodeURI(this.original_name) + "\">" + this.original_name + "</a></p>"
	    	            + "</div>";
    				$("#uploadFileList").append(fileHtml);
        		});
        		
        		$("#uploadFileLayer").show();
        	} else {
        		alert("오류가 발생했습니다.");
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        }
    });
}
function getPeriodInfo() {
	function getToday(){
        var date = new Date();
        var year  = date.getFullYear();
        var month = date.getMonth() + 1; // 0부터 시작하므로 1더함 더함
        var day   = date.getDate();
    
        if (("" + month).length == 1) { month = "0" + month; }
        if (("" + day).length   == 1) { day   = "0" + day;   }
		return ("" + year + month + day)	       
    }
	
	$.ajax({
        type: "POST",
        url: '<c:url value="/pro/getPeriodInfo"/>',
        data: {},
        dataType: "json",
        success: function(data, status) {
        	console.debug("getPeriodInfo ============");
        	console.debug(data);
        	if (data.status == "success") {
        		if(data.periodInfo == undefined || data.periodInfo == null){
        			alert("조회기간 정보를 찾을 수 없습니다.\n 관리자에게 문의하세요");
        			return;
        		}
        		var startDate = 0;
        		var endDate = 0;
        		for(var i=0; i < data.periodInfo.length; i++){
        			var period = data.periodInfo[i];
        			if(period.period_type == "C"){
        				startDate = parseInt(period.start_date,10);
        				endDate = parseInt(period.end_date,10);
        			}
        		}
        		if(startDate == 0 || endDate ==0){
        			alert("조회기간 정보를 찾을 수 없습니다.\n 관리자에게 문의하세요");
        			return;
        		}

                var rightNow = new Date();
                var today = getToday(); //rightNow.toISOString().slice(0,10).replace(/-/g,"");
                getMyResultTotalPoint();
                $("#menu_tab1").click();
                $("#contents").show();
                /* //평가결과조회 날짜 체크                  
        		 if(today >= startDate && today <= endDate){
	                today = parseInt(today,10);
	                console.log(today + " / " + startDate + " / " + endDate);
	                  getMyResultTotalPoint();
	                  $("#menu_tab1").click();
	                  $("#contents").show();
        		}else{
        			console.debug("가려!");
        			blockResultData();
        		}  */
        	} else {
        		alert("오류가 발생했습니다.");
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        }
    });
}
function blockResultData(){
	var contents = "";
	contents += "<div class=\"date_not\">";
	contents += "<p><img src=\"images/not.png\"></p>";
	contents += "<p class=\"date_memo_title\">평가 결과 조회 기간이 아닙니다.<p>";
	contents += "<p class=\"date_memo_text\">(확정조회공시기간에만 조회하실 수 있습니다.)</p>";
	contents += "<p class=\"red_bt\"><a href=\"javascript:history.back(-1)\">확인</a></p>";
	contents += "</div>";
	$("#contents").html(contents);
	$("#contents").show();
}


function ToFloat(number){
    var tmp = number + "";
    if(tmp.indexOf(".") != -1){
        number = Math.floor(number*100)/100;
//        number = number.replace(/(0+$)/, "");
    }

    return number;
}
</script>
<%@ include file="../include/footer.jsp" %>