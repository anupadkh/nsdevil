<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../include/pro_header.jsp" %>

<div id="wrap">
    
	<%@ include file="../include/top.jsp" %>

    <div class="container">
        <div class="left_bomenu">
            <ul class="board_ul">
                <li class=""><a href="">문의하기</a></li>
           <!-- <li class=""><a href="">게시판 리스트</a></li> -->
            </ul>    
        </div>
        <!-- //.leftmenu -->

        <div class="content">
            <div class="board_b">
                <table class="Notice">
                    <colgroup>
                        <col width="25%" />
                        <col width="75%" />
                    </colgroup>
                    <tbody>
                        <tr class="title_bg">
                            <td>
                                등록일
                            </td>
                            <td>
                                알림내용
                            </td>
                        </tr>
                        <tr>
                            <td>
                                2016.02.01
                            </td>
                            <td class="t_left">
                            	<span class="list_blue">공지</span><a href="">봉사업적 등록을 시작하실 수 있습니다.</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                2015.12.03
                            </td>
                            <td class="t_left">
								<a href="">평가결과 확인은 언제 할 수 있는 건가요?</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                2015.12.03
                            </td>
                            <td class="t_left">
                            	└&nbsp; <span class="list_orange">행정</span><a href="">아직 결정되지 않았습니다..</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                2015.12.03
                            </td>
                            <td class="t_left">
                            	 └&nbsp; <span class="list_gray">나</span><a href="">추가로 봉사 등록을 하고 싶은데 가능한지 여부를....</a>
                            </td>
                        </tr>
                        <!--<tr>
                            <td>
                                2015.12.03
                            </td>
                            <td class="t_left">
                                <span class="notice_ok">승인</span>봉사 등록내역이 최종 승인되었습니다.
                            </td>
                        </tr>-->
                    </tbody>
                </table>             
            </div>
            <!-- //.board_a -->

            <div class="table-list-num">
                <ul>
                    <!-- 첫페이지일 경우 이미지경로에 _off추가 -->
                    <li><a href="#"><img src="images/ppre_img_off.png" alt="처음"/></a></li>
                    <li><a href="#"><img src="images/pre_img_off.png" alt="이전" /></a></li>
                    <!-- 현재 노출된 페이지 번호에 색깔 표시 a태그에 클래스명 "active" 추가 -->
                    <li><a href="#" class="active">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li><a href="#">8</a></li>
                    <li><a href="#">9</a></li>
                    <li><a href="#">10</a></li>
                    <li><a href="#"><img src="images/nnext_img.png" alt="다음" /></a></li>
                    <li><a href="#"><img src="images/next_img.png" alt="맨끝"/></a></li>
                </ul>
            </div>
            <!-- //.table-list-num -->
        </div>
        <!-- //.content -->
    </div>
    <!-- //.container -->

    <%@ include file="../include/bottom.jsp" %>
    
</div>
<!-- //#wrap -->

<script>
$(document).ready(function() {
	$(".service-nav a").removeClass("active");
	$(".service-nav a:eq(2)").addClass("active");
});
</script>


<%@ include file="../include/footer.jsp" %>