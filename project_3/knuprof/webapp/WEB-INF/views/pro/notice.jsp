<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../include/pro_header.jsp" %>

<div id="wrap">
    
	<%@ include file="../include/top.jsp" %>

    <div class="container">
        <div class="left_menu">
            <ul class="left_ul">
                <li class="menuon"><a href="./notice">알림</a></li>
                <li class=""><a href="./info">내정보 수정</a></li>
                <li class=""><a href="./result">평가결과 조회</a></li>
            </ul>    
        </div>
        <!-- //.leftmenu -->

        <div class="content">
            <div class="board_a">
                <table class="Notice">
                    <colgroup>
                        <col width="25%" />
                        <col width="75%" />
                    </colgroup>
                    <tbody id="noticeListLayer">
                    	<!-- 
                        <tr class="title_bg">
                            <td>
                                등록일
                            </td>
                            <td>
                                알림내용
                            </td>
                        </tr>
                        <tr>
                            <td>
                                2016.02.01
                            </td>
                            <td class="t_left">
                                2016. 02.01~ 2016.02.25까지 평가 확인 기간입니다.
                            </td>
                        </tr>
                        <tr>
                            <td>
                                2015.12.03
                            </td>
                            <td class="t_left">
                                <span class="notice_return">행정팀 OOO 수정</span>봉사 – 교내봉사 내역이 수정되었습니다.
                            </td>
                        </tr>
                        <tr>
                            <td>
                                2015.12.03
                            </td>
                            <td class="t_left">
                                <span class="notice_return">행정팀 OOO 수정</span>봉사 – 교외봉사 내역이 수정되었습니다.
                            </td>
                        </tr>
                        <tr>
                            <td>
                                2015.12.03
                            </td>
                            <td class="t_left">
                                <span class="notice_ok">승인</span>봉사 등록내역이 최종 승인되었습니다.
                            </td>
                        </tr>
                        -->
                    </tbody>
                </table>             
            </div>
            <!-- //.board_a -->

            <div class="table-list-num" id="pagingLayer">
            	<!-- 
                <ul>
                    <!-- 첫페이지일 경우 이미지경로에 _off추가 -
                    <li><a href="#"><img src="images/ppre_img_off.png" alt="처음"/></a></li>
                    <li><a href="#"><img src="images/pre_img_off.png" alt="이전" /></a></li>
                    <!-- 현재 노출된 페이지 번호에 색깔 표시 a태그에 클래스명 "active" 추가 -
                    <li><a href="#" class="active">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li><a href="#">8</a></li>
                    <li><a href="#">9</a></li>
                    <li><a href="#">10</a></li>
                    <li><a href="#"><img src="images/nnext_img.png" alt="다음" /></a></li>
                    <li><a href="#"><img src="images/next_img.png" alt="맨끝"/></a></li>
                </ul>
                -->
            </div>
            <!-- //.table-list-num -->
        </div>
        <!-- //.content -->
    </div>
    <!-- //.container -->

    <%@ include file="../include/bottom.jsp" %>
    
</div>
<!-- //#wrap -->

<%@ include file="../include/footer.jsp" %>
<script type="text/javascript" src=<c:url value="/js/paging.js"/>></script>
<script>
$(".service-nav a").removeClass("active");
$(".service-nav a").eq(0).addClass("active");
$(document).ready(function(){
	getNoticeList(1);
	
});

function getNoticeList(pageNo){
	var param = {};
	param.pageNo = pageNo;
	param.rowCount = 10;
	
	$.ajax({
        type: "POST"
        ,url: "../pro/getNoticeList"
        ,contentType : 'application/json; charset=UTF-8'
        ,data: JSON.stringify(param)
        ,dataType: "json"
        ,global : false
        ,cache: false
        ,async: false
        ,success: function(json){
           console.debug(json);
           if(json == undefined && json == null){
        	   return;
           }
           if(json.status == "fail"){
        	   alert("오류가 발생 하였습니다.");
        	   return;
           }
           setNoticeListData(json.list);
           setPagingData(json.pagingInfo);
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
            alert("오류가 발생하였습니다");
        }
        ,complete: function() {
        	
        }
    });
}
function setNoticeListData(json){
	console.debug(json);
	var contents ="";
	contents += "<tr class=\"title_bg\">";
	contents += 	"<td>";
	contents += 		"등록일";
	contents += 	"</td>";
	contents += 	"<td>";
	contents += 		"알림내용";
	contents += 	"</td>";
	contents += "</tr>";

	if(typeof json == "undefined" || json == null || json.length == 0 ){
		contents += "<tr>";	
		contents += 	"<td colspan=2>";
		contents += 		"알림 내용이 없습니다"
		contents += 	"</td>";
		contents += "</tr>";
		$("#noticeListLayer").html(contents);
		return;
	}
	
	for(var i=0; i < json.length; i++){
		contents += "<tr>";	
		contents += 	"<td>";
		contents += 		json[i].regdate;
		contents += 	"</td>";
	    contents += 	"<td class=\"t_left\">";

	    if(json[i].userlevel == 3){
	    	contents += "<span class=\"notice_return\">행정팀 " + json[i].writerusername + " - 수정</span>";
	    }
	    contents +=     	json[i].contents;
	    contents +=     "</td>";
	    contents += "</tr>";
	}
	$("#noticeListLayer").html(contents);
}
function getPageList(pageNo){
	getNoticeList(pageNo);	
}
</script>