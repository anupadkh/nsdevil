<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
					<tr>
                        <c:if test="${!item.serviceHide}">
                        	<c:choose>
                        		<c:when test="${item.serviceRowSpan != 0}">
                        			<td rowspan="${item.serviceRowSpan}" class="text_big" >${item.serviceName}</td>
                        		</c:when>
                        		<c:otherwise>
                        			<td class="text_big" >${item.serviceName}</td>
                        		</c:otherwise>
                        	</c:choose>                        	
                        </c:if>
                        <c:if test="${!item.detailHide}">
                        	<c:choose>
                        		<c:when test="${item.detailRowSpan != 0}">
                        			<td rowspan="${item.detailRowSpan}" class="border-l">${item.serviceDetail1}</td>
                        		</c:when>
                        		<c:otherwise>
                        			<td class="border-l">${item.serviceDetail1}</td>
                        		</c:otherwise>
                        	</c:choose>                        	
                        </c:if>
                        
                        <td colspan="${item.detailColSpan}" class="text-left border-l">${fn:replace(item.serviceDetail2, "\\r", "<br>")}</td>
                        <td><c:if test="${item.point > 0}">${item.point}</c:if><c:if test="${item.unit != null and item.unit.length() > 0}"> / ${item.unit}</c:if></td>
                        
                        <c:if test="${!item.noteHide}">
                        	<c:choose>
                        		<c:when test="${item.noteRowSpan != 0}">
                        			<td rowspan="${item.noteRowSpan}" class="text-left">${item.note}</td>
                        		</c:when>
                        		<c:otherwise>
                        			<td class="text-left">${item.note}</td>
                        		</c:otherwise>
                        	</c:choose>                        	
                        </c:if>
                        
                        <td class="datepick-td">
                        	<input type="hidden" name="date_type" value="${item.dateType}" />
                        	<input type="hidden" name="direct_input_enabled" value="<c:if test="${item.placeholder != null and item.placeholder.length() > 0}">Y</c:if>" />
                        	<c:choose>
                            <c:when test="${item.dateType == 1}">
                            <div class="datepick-div">
	                            <div>
	                                <input type="text" name="start_date" class="datepicker-input" placeholder="시작일" />
	                                <button type="button" value="" class="btn-monthpick">
	                                    <img src="images/calendar.png" alt="달력보기" /> 
	                                </button>
	                                ~&nbsp;
	                            </div>
	                            <div>
	                                <input type="text" name="end_date" class="datepicker-input" placeholder="종료일" />
	                                <button type="button" value="" class="btn-monthpick">
	                                    <img src="images/calendar.png" alt="달력보기" /> 
	                                </button>
	                            </div>
                            </div>
                            </c:when>
                            <c:when test="${item.dateType == 2 || item.dateType == 3 || item.dateType == 4}">
                            <div class="datepick-div">
	                            <div class="long-input-div">
	                            	<c:choose>
	                            	<c:when test="${item.dateType == 3}">
	                                <input type="text" name="start_date" class="datepicker-input" placeholder="강연일" />
	                                </c:when>
	                                <c:otherwise>
	                                <input type="text" name="start_date" class="datepicker-input" placeholder="날짜등록" />
	                                </c:otherwise>
	                                </c:choose>
	                                <button type="button" value="" class="btn-monthpick">
	                                    <img src="images/calendar.png" alt="달력보기" /> 
	                                </button>
	                            </div>
                            </div>
                            </c:when>
                            </c:choose>
                            <c:if test="${item.dateType == 4}">
                            <div class="datepick-div">
                                <div>
                                    <input type="text" name="start_time" class="timepicker-input" placeholder="시작시간" />
                                    <button type="button" value="" class="btn-monthpick">
                                        <img src="images/calendar.png" alt="달력보기" /> 
                                    </button>
                                    ~&nbsp;
                                </div>
                                <div>
                                    <input type="text" name="end_time" class="timepicker-input" placeholder="종료시간" />
                                    <button type="button" value="" class="btn-monthpick">
                                        <img src="images/calendar.png" alt="달력보기" /> 
                                    </button>
                                </div>
                            </div>
                            </c:if>
                            <c:choose>
							<c:when test="${item.placeholder != null and item.placeholder.length() > 0}">
							<div class="service-table-select-div">
							<p>
								<input type="text" name="direct_input" value="" placeholder="${item.placeholder}" />
							</p>
							<p>
								<button type="button" class="btn-view-textinput" onClick="javascript:addContents($(this));">입력</button>
							</p>
							</div>
							</c:when>
							<c:otherwise>
							<div class="service-table-select-div">
							<p>
								<select name="job_title_sel" onChange="javascript:selectJobTitle($(this));">
									<option value="">보직선택</option>
									<c:choose>
									<c:when test="${item.optionList != null}">
									<c:forEach var="value" items="${item.optionList}">
									<option value="${value}">${value}</option>
									</c:forEach>
									</c:when>
									<c:otherwise>
									<option value="총장">총장</option>
									<option value="부총장">부총장</option>
									<option value="교수회의장">교수회의장</option>
									<option value="학장">학장</option>
									<option value="교수">교수</option>
									<option value="부교수">부교수</option>
									<option value="조교수">조교수</option>
									</c:otherwise>
									</c:choose>
									<option value="직접입력">직접입력</option>
								</select>
							</p>
							<p>
								<button type="button" class="btn-view-textinput" onClick="javascript:addContents($(this));">입력</button>
							</p>
							<p class="directInput">
								<input type="text" name="direct_input" value="" placeholder="직접입력" />
							</p>
							</div>
							</c:otherwise>
							</c:choose>
                        </td>
                        <td class="service-td">
                        	<div class="contentsVIew"></div>
                        </td>
                        <td class="service-table-up-td">
                        	<input type="hidden" name="serviceCode" value="${item.serviceCode}" />
                        	<input type="hidden" name="serviceType" value="${item.serviceType}" />
                            <button type="button" class="btn-service-popup" onClick="javascript:uploadFile($(this));">등록</button>
                            <div class="fileCountDiv"></div>
                        </td>
                        <td><div class="userPoint"></div></td>
                    </tr>