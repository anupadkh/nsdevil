<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../include/pro_header.jsp" %>

<div id="wrap">
    
	<%@ include file="../include/top.jsp" %>
    
    <div class="container">
		<div class="left_menu">
            <ul class="left_ul">
            	<c:choose>
            	<c:when test="${sessionScope.userlevel == 2}">
				<li><a href="./notice">알림</a></li>
                <li class="menuon"><a href="./info">내정보 수정</a></li>
                <li><a href="./result">평가결과 조회</a></li>
                </c:when>
                <c:otherwise>
                <li class="menuon"><a href="./info">내정보 수정</a></li>
                </c:otherwise>
                </c:choose>
                
            </ul>
        </div>
        <!-- //.leftmenu -->
        
		<div class="content">
            <div class="info_editer">
                <table>
                    <colgroup>
                        <col width="30%" />
                        <col width="70%" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <td class="text_left">
                                성명
                            </td>
                            <td class="text_right" id="userName">
                                
                            </td>
                        </tr> 
                        <tr>
                            <td class="text_left">
                                직급
                            </td>
                            <td class="text_right" id="jobTitle">
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="text_left">
                                교직원 번호
                            </td>
                            <td class="text_right" id="profNum">
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="text_left">
                                아이디
                            </td>
                            <td class="text_right" id="userId">
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="text_left">
                                현재 비밀번호
                            </td>
                            <td class="text_right">
                                <input type="password" class="a_input" value="" id="userPass"/>
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="text_left">
                                변경할 비밀번호
                            </td>
                            <td class="text_right">
                                <input type="password" class="a_input" value="" id="newPass"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text_left">
                                변경할 비밀번호 확인
                            </td>
                            <td class="text_right">
                                <input type="password" class="a_input" value="" id="confirmPass"/>
                            </td>
                        </tr>
                        <c:if test="${sessionScope.userlevel > 1}">                   
                        <tr>
                            <td class="text_left">
                                연락처
                            </td>
                            <td class="text_right">
                                <input type="text" class="b_input" value="" id="phone0"/>
                                <input type="text" class="b_input" value="" id="phone1"/>
                                <input type="text" class="b_input" value="" id="phone2"/>
                            </td>
                        </tr>                    
                        <tr class="bottom_line">
                            <td class="text_left">
                                이메일
                            </td>
                            <td class="text_right">
                                <input type="text" class="a_input" value="" id="userEmail"/>
                            </td>
                        </tr>
                        </c:if>                                   
                    </tbody>
                </table>
                
                <div class="bt_bottom_set">
                    <span class="red_bt"><a href="javascript:updateMyInfo()">수정</a></span>
                    <span class="gray_bt"><a href="javascript:history.back(-1)">취소</a></span>
                </div>
                <!-- //.bt_bottom_set -->
            </div>
            <!-- //.info_editer -->
		</div>
       <!-- //.content-->
    </div>
    <!-- //.container -->
    
    <%@ include file="../include/bottom.jsp" %>
    
</div>
<!-- //#wrap -->

<%@ include file="../include/footer.jsp" %>
<script>
$(".service-nav a").removeClass("active");
$(".service-nav a").eq(0).addClass("active");
var userLevel = "${sessionScope.userlevel}";

$(document).ready(function(){
	getMyInfo();
	
	
	$("[id^='phone']").keypress(function(event){
  		if (event.which && (event.which  > 47 && event.which  < 58 || event.which == 8)) {
		} else {
			alert("숫자만 입력하세요");
		    event.preventDefault();
		}
	});
	
});
function getMyInfo(){
	$.ajax({
        type: "POST"
        ,url: "../user/getMyInfo"
        ,contentType : 'application/json; charset=UTF-8'
        ,data: {}
        ,dataType: "json"
        ,global : false
        ,cache: false
        ,async: false
        ,success: function(json){
           console.debug(json);
           if(json == undefined && json == null){
        	   return;
           }
           $("#userName").html(json.userName);
           $("#jobTitle").html(json.jobTitle);
           $("#profNum").html(json.profNum);
           $("#userId").html(json.userId);
           var userPhoneEach = json.userPhone.split("-");
           $("#phone0").val(userPhoneEach[0]);
           $("#phone1").val(userPhoneEach[1]);
           $("#phone2").val(userPhoneEach[2]);
           $("#userEmail").val(json.userEmail);
           
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
            alert("오류가 발생하였습니다");
        }
        ,complete: function() {
        	
        }
    });
}

function updateMyInfo(){
	if(!checkValidation()){
		return;
	}
	
	var paramData = {};
	paramData.userId = $("#userId").html();
	paramData.newPass = $("#newPass").val();
	paramData.userPass = $("#userPass").val();
	if(userLevel > 1){
		paramData.userPhone = $("#phone0").val() + "-"+$("#phone1").val() + "-"+$("#phone2").val();
		paramData.userEmail = $("#userEmail").val();
	}
	$.ajax({
        type: "POST"
        ,url: "../user/updateMyInfo"
        ,contentType : 'application/json; charset=UTF-8'
        ,data: JSON.stringify(paramData)
        ,dataType: "json"
        ,global : false
        ,cache: false
        ,async: false
        ,success: function(json){
           console.debug(json);
           if(json != undefined && json != null){
        	   if(json.status == "success"){
        		   $("[id$=Pass]").val("");
        		   alert("성공적으로 수정되었습니다");
        		   history.back(-1);
        	   }else{
        		   alert(json.message);
        	   }
           }
           return;
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
        	alert("내정보 수정에 실패하였습니다");
        }
        ,complete: function() {
        	
        }
    });
}
function checkValidation(){
	if($("#userPass").val().length < 4){
		alert("비밀번호를 입력하세요");
		$("#userPass").focus();
		return false;
	}
	if($("#userPass").val() == $("#newPass").val()){
		alert("변경할 비밀번호가 현재 비밀번호와 동일합니다.");
		$("#newPass").val("").focus();
		return false;
	}
	
	
	if($("#newPass").val().length > 0){
		if($("#newPass").val().length < 10){
			alert("변경할 비밀번호의 길이가 너무 짧습니다");
			$("#newPass").focus();
			return false;
		}
		if($("#newPass").val() != $("#confirmPass").val()){
			alert("변경할 비밀번호가 일치하지 않습니다");
			$("#newPass").focus();
			return false;
		}
		var isValid = checkValidPassword($("#newPass").val(),$("#userId").html(), $("#profNum").html() );
		if(!isValid){
			return false;
		}
	}
	if(userLevel == "2"){
		if(!/^[0]{1}[1]{1}[0|1|6|7|8|9]{1}$/.test($("#phone0").val())){
			alert("연락처를 확인하세요");
			$("#phone0").focus();
			return false;
		}
		if(!/^[0-9]{3,4}$/.test($("#phone1").val())){
			alert("연락처를 확인하세요");
			$("#phone1").focus();
			return false;
		}
		if(!/^[0-9]{4}$/.test($("#phone2").val())){
			alert("연락처를 확인하세요");
			$("#phone2").focus();
			return false;
		}
		var emailRegExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i; 
		if(!emailRegExp.test($("#userEmail").val())){
			alert("이메일을 확인하세요");
			$("#userEmail").focus();
			return false;
		}
	}
	return true;
}
function checkValidPassword(pw,userId,profNum){
	if (pw.indexOf(userId) != -1) { 
		alert("비밀번호에 아이디를 포함 할 수 없습니다.");
		return false;
	}
	if(profNum != null && profNum.length > 0 && pw.indexOf(profNum) != -1){
		alert("비밀번호에 교직원번호를 포함 할 수 없습니다.");
		return false;
	}
	var num = pw.search(/[0-9]/g);
	var eng = pw.search(/[a-z]/ig);
	var spe = pw.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);
	if( !( pw.length > 0 && pw.length < 20)){
		alert("8자리 ~ 20자리 이내로 입력해주세요.");
		return false;
	}
	if(pw.search(/₩s/) != -1){
		alert("비밀번호는 공백업이 입력해주세요.");
		return false;
	}
	if(num < 0 || eng < 0 || spe < 0 ){
		alert("영문,숫자, 특수문자를 혼합하여 입력해주세요.");
		return false;
	}
	
	var samePass = 0;
	var samePass1 = 0; //연속성(+) 카운드
    var samePass2 = 0; //연속성(-) 카운드
    var chrPass0;
    var chrPass1;
    var chrPass2;
    for(var i=0; i < pw.length; i++){
        chrPass0 = pw.charAt(i);
        chrPass1 = pw.charAt(i+1);
        //동일문자 카운트
        if(chrPass0 == chrPass1){
            samePass = samePass + 1
        }
        
        chrPass2 = pw.charAt(i+2);
        //연속성(+) 카운드
        if(chrPass0.charCodeAt(0) - chrPass1.charCodeAt(0) == 1 && chrPass1.charCodeAt(0) - chrPass2.charCodeAt(0) == 1){
            samePass1 = samePass1 + 1
        }
        //연속성(-) 카운드
        if(chrPass0.charCodeAt(0) - chrPass1.charCodeAt(0) == -1 && chrPass1.charCodeAt(0) - chrPass2.charCodeAt(0) == -1){
            samePass2 = samePass2 + 1
        }
    }
    if(samePass > 1){
        alert("동일문자를 3번 이상 사용할 수 없습니다.");
        return false;
    }
    if(samePass1 > 1 || samePass2 > 1 ){
        alert("연속된 문자열(123 또는 321, abc, cba 등)을\n 3자 이상 사용 할 수 없습니다.");
        return false;
    
	}
	return true;
}

</script>