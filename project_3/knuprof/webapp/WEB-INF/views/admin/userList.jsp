<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../include/admin_header.jsp" %>

<div id="wrap">

    <%@ include file="../include/top.jsp" %>

    <div class="container">

        <!-- 탭 메뉴 영역 -->
        <div class="tabmenu-div">
            <ul class="tabmenu-ul">
                <!-- 선택된 탭메뉴 표시 클래스 "active" -->
                <li class="active">
                    <a href="<c:url value="/admin/"/>">이용자 관리</a>
                </li>
                <li>
                    <a href="<c:url value="/admin/gradeScore"/>">기준 점수</a>
                </li>
                <li>
                    <a href="<c:url value="/admin/period"/>">기간 설정</a>
                </li>
            </ul>
        </div>
        <!-- //.tabmenu-div -->
        
        <!-- 이용자 구분 검색영역 -->
        <div class="contents-serach-btns-div">
            <div class="contents-search-div">
                <p style="display:none;">
                    <label for="search_year">년도 : </label>
                    <select id="searchYear">
                    	<option value="2015" selected>2015</option>
                        <option value="2016" selected>2016</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                    </select>
                </p>
                <p>
                    <label>구분 : </label>
                    <select id="searchUserLevel">
                        <option value="">전체</option>
                        <option value="1">행정</option>
                        <option value="2">교수</option>
                    </select>
                </p>
                <p>
                    <label for="search_name">성명 : </label>
                    <input id="searchUserName" type="text" value="" placeholder="성명 입력" />
                    <button type="button" class="btn-search-name"><img src="images/btn-search.png" alt="검색" onclick="javascript:getUserSearch();"/></button>
                </p>
            </div>
            <!-- //.contents-search-div -->
            
            <div class="contents-btns-div">
                <a href="addUser">신규 등록</a>
                <a href="addUserByExcel">단체 등록(엑셀 일괄 등록)</a>
            </div>
        </div>
        <!-- //.contents-serach-btns-div -->

        <!-- 테이블 영역 -->
        <div class="list-table-contents">
            <!-- 페이지당 리스트 최대 10건 노출 -->
            <table class="service-table">
                <colgroup>
                    <col width="105" />
                    <col width="" />
                    <col width="" />
                    <col width="100" />
                    <col width="" />
                    <col width="" />
                    <col width="" />
                    <col width="150" />
                    <col width="180" />
                </colgroup>
                <tbody id="userListLayer">
                <!-- 
                    <tr>
                        <th>
                            등록일
                        </th>
                        <th>
                            아이디
                        </th>
                        <th>
                            교실명
                        </th>
                        <th>
                            직급
                        </th>
                        <th>
                            구분
                        </th>
                        <th>
                            교직원 번호
                        </th>
                        <th>
                            성명
                        </th>
                        <th>
                            연락처
                        </th>
                        <th>
                            수정 / 삭제
                        </th>
                    </tr>
                    <tr>
                        <td>
                            2016.03.17
                        </td>
                        <td>
                            co121333acb
                        </td>
                        <td>
                            생화학 - 세포생물학
                        </td>
                        <td>
                            <span class="tableposition">부교수</span>
                        </td>
                        <td>
                            겸직1
                        </td>
                        <td>
                            19840488
                        </td>
                        <td>
                            <span class="tablename">정기준</span>
                        </td>
                        <td>
                            010-8446-3243
                        </td>
                        <td class="btn-re-td">
                            <a href="sub_1_1_1.html" class="btn-re">수정</a>
                            <a class="btn-del">삭제</a>
                        </td>
                    </tr>
                     -->
                    
                </tbody>
            </table>
            <!-- //.service-table -->
            
            <!-- 페이징 숫자 표시 영역 -->
            <div class="table-list-num"  id="pagingLayer">
            </div>
            <!-- //.table-list-num -->
        </div>
        <!-- //.list-table-contents -->
    </div>
    <!-- //.container -->
	
	<%@ include file="../include/bottom.jsp" %>
	
</div>
<!-- //#wrap -->
<!-- 이용자 삭제 팝업 -->
<div id="deleteLayer" class="wrap-popup-nonebg wrap-del-popup hidden">
    <div class="popup-guide">
        <div class="popup-guide-header">
            <h2>
                이용자 삭제
            </h2>
            <a class="btn-close-del-popup" title="close">
                X
            </a>
        </div>
        <div class="popup-guide-content">
            <span class="popname"></span>
            <span class="popposition"></span> <span id="askContentsLayer">님을 삭제 하시겠습니까?</span>
            <div class="popup-btns-div">
                
                <!-- ok -->
                <button type="button">확인</button>
                
                <!-- cancel button -->
                <button type="button" class="btn-close-del-popup">취소</button>
            </div>
        </div>
    </div>
</div>
<!-- //.wrap-popup -->
<script type="text/javascript" src=<c:url value="/js/paging.js"/>></script>
<script>
$(document).ready(function(){
    // 이름 검색창 체크
    /*
    $(".btn-search-name").bind("click", function(){
        var input = $(this).siblings("input[type='text']");
        if ( input.val() == 0 ){
            alert("검색할 이름을 입력해주세요.");
            input.focus();
        }
    });
    */
    $("#searchUserName").keypress(function(e) { 
        if (e.keyCode == 13){
        	getUserSearch();
    	}
    });
    
    // 테이블 라인 배경
    $(".service-table tr:even").css("background","#fdfdfd");
    
    // 이용자 삭제 팝업
    $(".btn-del").bind("click", function(){
        var tableName = $(this).parent().siblings().children(".tablename").text();
        var tablePosition = $(this).parent().siblings().children(".tableposition").text();
        $(".wrap-del-popup").fadeIn(200);
        $(".popup-guide-content span.popname").text("'" + tableName + "'");
        $(".popup-guide-content span.popposition").text(tablePosition);
    });
    $(".btn-close-del-popup").bind("click", function(){
        $(".wrap-del-popup").fadeOut(200);
    });
});
</script>


<script>
$(document).ready(function(){
	getUserList(1);
});

var searchOption = null;
function getUserList(pageNo){
	var param = {};
	param.pageNo = pageNo;
	param.rowCount = 10;
	
	if(searchOption != null){
		if(typeof searchOption.searchUserName != "undefined"){
			param.searchUserName = searchOption.searchUserName;
		}
		if(typeof searchOption.searchYear != "undefined"){
			param.searchYear = "";//searchOption.searchYear;
		}
		if(typeof searchOption.searchUserLevel != "undefined"){
			param.searchUserLevel = searchOption.searchUserLevel;
		}
	}
	
	$.ajax({
        type: "POST"
        ,url: "../user/getUserList"
        ,contentType : 'application/json; charset=UTF-8'
        ,data: JSON.stringify(param)
        ,dataType: "json"
        ,global : false
        ,cache: false
        ,async: false
        ,success: function(json){
           console.debug(json);
           if(json == undefined && json == null){
        	   return;
           }
           if(json.status == "fail"){
        	   alert("오류가 발생 하였습니다.");
        	   return;
           }
           setUserListLayer(json.list);
           setPagingData(json.pagingInfo);
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
            alert("오류가 발생하였습니다");
        }
        ,complete: function() {
        	
        }
    });
	$(".service-nav a").eq(0).addClass("active");
}

function setUserListLayer(json){
	console.debug(json);

	var html = "<tr><th>등록일</th><th>아이디</th><th>교실명</th><th>직급</th><th>구분</th><th>교직원 번호</th><th>성명</th><th>연락처</th><th>수정 / 삭제</th></tr>";
	if(typeof json == "undefined" || json == null){
		$("#userListLayer").html(html);
		return;
	}

	for(var i=0; i < json.length; i++){
		html += "<tr>";
		html +=  	"<td>" + json[i].regdate + "</td>";
		html += 	"<td>" + json[i].userid + "</td>";
		html += 	"<td>" + json[i].subject + "</td>";
		html += 	"<td>";
		html +=			"<span class=\"tableposition\">" + json[i].jobtitle + "</span>";
		html += 	"</td>";
		html += 	"<td>"+ getAddition(json[i].addition) + "</td>";
		html += 	"<td>" + json[i].profnum + "</td>";
		html += 	"<td>";
		html +=     	"<span class=\"tablename\">" + json[i].username + "</span>";
		html += 	"</td>";
		html += 	"<td>" + json[i].userphone + "</td>";
		html += 	"<td class=\"btn-re-td\">";
		html +=     	"<a style=\"display:inline\" href=\"javascript:editUser('" + json[i].usercode+"')\" class=\"btn-re\">수정</a>";
		html +=         "<a style=\"display:inline\" class=\"btn-del\" onclick=\"deletePopup('" + json[i].usercode + "','"+ json[i].username + "','"+ json[i].userlevel + "')\">삭제</a>";
		html +=         "<a style=\"display:inline\" class=\"btn-del\" onclick=\"initUserPasswordPopup('" + json[i].usercode + "','"+ json[i].username + "','"+ json[i].userlevel + "')\">비밀번호초기화</a>";
		html += 	"</td>";
		html += "</tr>";
	}
	$("#userListLayer").html(html);
}
function getAddition(addition){
	switch(addition){
	case 1:
	case 2:
	case 3:
	case 4:
	case 5:
	case 6:
		return "겸직" + addition;
	case 7: 
		return "기금1";
	case 8: 
		return "기금2";
	default :
	}
	return "";
}
function getPageList(pageNo){
	getUserList(pageNo);
	if(searchOption == null){
		searchOption = new Object();
	}
	searchOption.pageNo = pageNo;
}
function getUserSearch(){
	var searchUserName = $("#searchUserName").val().trim();
	var searchYear = $("#searchYear").val().trim();
	var searchUserLevel = $("#searchUserLevel").val().trim();
	
	if(searchOption == null){
		searchOption = new Object();
	}
	searchOption.searchUserName = searchUserName;
	searchOption.searchYear = searchYear;
	searchOption.searchUserLevel = searchUserLevel;
	getPageList();
}
function deletePopup(userCode,userName, userLevel){
	var question = "교수님을 삭제하시겠습니까?";
	if(userLevel == 1){
		question = "행정팀원을 삭제하시겠습니까?";
	}
	
	var html = "";
	html += "<div class=\"popup-guide\">";
	html +=		"<div class=\"popup-guide-header\">";
	html +=			"<h2>이용자 삭제 </h2>";
	html += 		"<a class=\"btn-close-del-popup\" onclick=\"javascript:$('#deleteLayer').fadeOut(200);\" title=\"close\">X</a>";
	html += 	"</div>";
	html += 	"<div class=\"popup-guide-content\">";
	html += 		"<span class=\"popname\">" + userName + "</span>";
	html += 		"<span class=\"popposition\"></span>" + question; //님을 삭제 하시겠습니까?";
	html += 		"<div class=\"popup-btns-div\">";
	html += 			"<button type=\"button\" id=\"deleteButton\" onclick=\"deleteUser('" + userCode + "')\">확인</button>";
	html += 			"<button type=\"button\" onclick=\"javascript:$('#deleteLayer').fadeOut(200);\" class=\"btn-close-del-popup\">취소</button>";
	html += 		"</div>";
	html += 	"</div>";
	html += "</div>";
	
	$("#deleteLayer").html(html);
	$("#deleteLayer").fadeIn(200);
	$("#deleteButton").focus();
}
function initUserPasswordPopup(userCode,userName, userLevel){
	var question = "교수님의 비밀번호를 \"000000\" 으로 초기화 하시겠습니까?";
	if(userLevel == 1){
		question = "행정팀원의 비밀번호를 \"000000\" 으로 초기화 하시겠습니까?";
	}
	
	var html = "";
	html += "<div class=\"popup-guide\">";
	html +=		"<div class=\"popup-guide-header\">";
	html +=			"<h2>이용자 삭제 </h2>";
	html += 		"<a class=\"btn-close-del-popup\" onclick=\"javascript:$('#deleteLayer').fadeOut(200);\" title=\"close\">X</a>";
	html += 	"</div>";
	html += 	"<div class=\"popup-guide-content\">";
	html += 		"<span class=\"popname\">" + userName + "</span>";
	html += 		"<span class=\"popposition\"></span>" + question; //님을 삭제 하시겠습니까?";
	html += 		"<div class=\"popup-btns-div\">";
	html += 			"<button type=\"button\" id=\"deleteButton\" onclick=\"initUserPassword('" + userCode + "')\">확인</button>";
	html += 			"<button type=\"button\" onclick=\"javascript:$('#deleteLayer').fadeOut(200);\" class=\"btn-close-del-popup\">취소</button>";
	html += 		"</div>";
	html += 	"</div>";
	html += "</div>";
	
	$("#deleteLayer").html(html);
	$("#deleteLayer").fadeIn(200);
	$("#deleteButton").focus();
}
function initUserPassword(userCode){
	$.ajax({
        type: "POST"
        ,url: "../user/initUserPassword"
        ,data: { "userCode" : userCode}
        ,dataType: "json"
        ,global : false
        ,cache: false
        ,async: false
        ,success: function(json){
           console.debug(json);
           if(json == undefined && json == null){
        	   alert("오류가 발생 하였습니다.");
        	   return;
           }
           if(json.status == "fail"){
        	   alert("비밀번호 초기화 중 오류가 발생 하였습니다.");
        	   return;
           }
           alert("성공적으로 비밀번호가 초기화 되었습니다");
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
            alert("오류가 발생하였습니다");
        }
        ,beforeSend:function() {
        	$("#deleteLayer").hide();
        }
        ,complete: function() {
        	
        }
    });
	
}
function deleteUser(userCode){
	
	$.ajax({
        type: "POST"
        ,url: "../user/deleteUser"
        ,contentType : 'application/json; charset=UTF-8'
        ,data: JSON.stringify({"userCode" : userCode})
        ,dataType: "json"
        ,global : false
        ,cache: false
        ,async: false
        ,success: function(json){
           console.debug(json);
           if(json == undefined && json == null){
        	   alert("오류가 발생 하였습니다.");
        	   return;
           }
           if(json.status == "fail"){
        	   alert("삭제 중 오류가 발생 하였습니다.");
        	   return;
           }
           alert("성공적으로 삭제되었습니다");
           $("#deleteLayer").hide();
           getPageList();
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
            alert("오류가 발생하였습니다");
        }
        ,complete: function() {
        	
        }
    });
}
function editUser(userCode){
	var form = document.createElement("form");
	form.setAttribute("method", "POST");
	form.setAttribute("action", "./editUser");
	var hiddenField = document.createElement("input");
	hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", "userCode");
    hiddenField.setAttribute("value", userCode);
	form.appendChild(hiddenField);
	document.body.appendChild(form);
	form.submit();
}
</script>
<%@ include file="../include/footer.jsp" %>