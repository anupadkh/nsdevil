<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../include/admin_header.jsp" %>
<div id="wrap">
    <%@ include file="../include/top.jsp" %>
    <!-- //.header -->

    <div class="container-table">
         <div class="aside">
            <div class="contents-serach-btns-div">
                <input type="text" value="" placeholder="이름" />
                <button type="button" class="btn-search-name"><img src="images/btn-search.png" alt="검색" /></button>
            </div>
            <!-- //.contents-serach-btns-div -->
            
            <div class="count-div">
            	<p class="stay">
                    <span>전체</span>
                    <span>
                        <a href="javascript:getAchieveRegistUserList('');" id="registTotalProfessor" class="btn-done-cnt active"></a>
                    </span>
                </p>
                <p class="">
                    <span>미등록</span>
                    <span>
                        <a href="javascript:getAchieveRegistUserList('N');" id="unregistProfessor" class="btn-done-cnt"></a>
                    </span>
                </p>
                <p>
                    <span>등록완료</span>
                    <span>
                        <a href="javascript:getAchieveRegistUserList('Y');" id="registProfessor"  class="btn-done-cnt"></a>
                    </span>
                </p>
            </div>
            <!-- //.count-div -->
            
            <!-- 교수 리스트 -->
            <ul class="list-pro">
            	<!-- 
                <li class="view-all-pro">
                    <a href="javascript:void(0)">전체 확인</a>
                </li>
                 -->
                <li id="professorListLayer"></li>
            </ul>
        </div>
        <!-- //.aside -->
        

        <div class="contents">
            <a class="btn-coll"><span>좌측메뉴 영역 접기/펴기 토글</span></a>

            <div class="contents-serach-btns-div">
                <p class="contents-title-name" id="contents-title-name">
                    <!-- <span class="name-pro">정동일</span><span>기생충학</span> -->
                </p>
                <div class="contents-btns-div">
                    <a href="javascript:toggleExcelRegistLayer(true)">엑셀파일 일괄 등록하기 (전체 교수님)</a>
                </div>
            </div>
            <!-- //.contents-serach-btns-div -->

            <!-- 탭 메뉴 영역 -->
            <!--<div class="tabmenu-div">
                <ul class="tabmenu-ul tabs">
                    <li data-to="tab1" class="active">
                        <a>교내 봉사</a>
                    </li>
                    <li data-to="tab2">
                        <a>교외 봉사</a>
                    </li>
                </ul>
            </div>-->
            <!-- //.tabmenu-div -->

            <!-- 테이블 영역 -->
            <div class="list-table-contents">
                 <p class="tab-content-table-title" >
                [교내 봉사]
            </p>
            <table class="service-table" >
                <colgroup>
                    <col width="80" />
                    <col width="" />
                    <col width="" />
                    <col width="60" />
                    <col width="150" />
                    <col width="240" />
                    <col width="200" />
                    <col width="110" />
                    <col width="42" />
                </colgroup>
                <tr>
					<th scope="col">업적내용</th>
					<th colspan="2" scope="col">평가항목(평가기간이내)</th>
					<th scope="col">기준<br />점수</th>
					<th scope="col">비고</th>
					<th scope="col">보직 선택 기간입력</th>
					<th scope="col">내용</th>
					<th scope="col">증빙서류</th>
					<th scope="col">점수</th>
				</tr>
				<tbody id="service1List">
				<c:forEach var="item" items="${data.service1List}">
				<%@ include file="achieve_serviceList.jsp" %>
				</c:forEach>
				</tbody>
				<tr id="service1List_totalPoint">
					<td colspan="4" class="total_text">
						소계
					</td>
					<td colspan="5" class="total_text">
						<span id="inTotalPoint1"></span>점
					</td>
                </tr>
            </table>
            <!-- //.service-table -->
            
            <p class="tab-content-table-title">
                [교외 봉사] &nbsp;&nbsp;<input type="button" onClick="ServiceConfirm();" value="확인" style="width:80px;background:#ee5656;border:solid 1px #da2228;height:25px;vertical-align:middle;border-radius:5px;cursor:pointer;font-size:12px;font-weight:500;color:#fff;">
            </p>
            <table class="service-table">
                <colgroup>
                    <col width="80" />
                    <col width="" />
                    <col width="" />
                    <col width="50" />
                    <col width="120" />
                    <col width="240" />
                    <col width="210" />
                    <col width="100" />
                    <col width="50" />
                </colgroup>
                <tr>
                    <th scope="col">업적내용</th>
                    <th colspan="2" scope="col">평가항목(평가기간이내)</th>
                    <th scope="col">기준<br />점수</th>
                    <th scope="col">비고</th>
                    <th scope="col">보직 선택 기간입력</th>
                    <th scope="col">내용</th>
                    <th scope="col">증빙서류</th>
                    <th scope="col">점수</th>
                </tr>
                <tbody id="service2List">
                <c:forEach var="item" items="${data.service2List}">
                <%@ include file="achieve_serviceList.jsp" %>
                </c:forEach>
                </tbody>
            </table>
            <!-- //.service-table -->
            
            <p class="tab-content-table-title" id="totalPointTitleLayer">
                        [총점]
            </p>
            <table class="service-table co-tabs-table" id="totalPointLayer">
                <colgroup>
                    <col width="" />
                    <col width="" />
                    <col width="" />
                    <col width="" />
                    <col width="" />
                </colgroup>
                <tbody>
                    <tr>
                        <th>
                            구분
                        </th>
                        <th>
                            교내봉사
                        </th>
                        <th>
                            교외봉사
                        </th>
                        <th>
                            합계
                        </th>
                        <th>
                            비고
                        </th>
                    </tr>
                    <tr class="total-score-tr">
                        <td>
                            점수
                        </td>
                        <td>
                            <span id="inTotalPoint2"></span>점
                        </td>
                        <td>
                            <span id="outTotalPoint"></span>점
                        </td>
                        <td>
                            <span id="finalPoint"></span>점
                        </td>
                        <td>

                        </td>
                    </tr>
                </tbody>
            </table>
            </div>
            <!-- //.list-table-contents-->

            <div class="guide-txt-table-div">
                <p>
                    &lt;주&gt;
                </p>
                <p>
                    1. [봉사영역 항목별 평가표]의 교내봉사 부분 중 교내 공문으로 발령 받은 경우는 의전원 행정실에서 작성한다. 단 교내 공문으로 발령 받았더라도 비고란에 조건이 있는 사항과 공문으로 발령 받지 않은 봉사내역은 내용과 함께 근거 자료를 제출하여야 한다. 교외봉사업적은 봉사영역 평가 항목별 평점표를 작성하고 근거자료를 제출 한다.
                </p>
                <p class="color-red">
                    2. 동일기간 보직을 겸직하는 경우 점수가 상위인 보직의 점수를 부여한다. 교실주임의 경우 주임교수회의를 1/3 참석하지 않은 경우 가점의 1/2만 인정한다.
                </p>

                <p>
                    3. 상임위원회, 운영위원회, 상임소위원회 위원, 기타 지원시설(공동기기실, 실험동물실, 의대도서관분관 등)의 장을 포함한다. 위원회 위원은 1/2 이상 회의 참석한 경우에만 가점을 부여하고, 참석여부는 회의록으로 확인한다.
                </p>
                <p>
                    4. 기간이 년 단위인 경우 1년 미만인 경우에는 기간을 반올림한 개월 수에 비례하여 환산한 점수를 부여한다.
                </p>
            </div>
            <!-- //.guide-txt-table-div -->

            <!-- 등록 버튼 영역-->
            <div class="signup-btn-div text-right">
                <!-- <a class="btn-signup">등록</a>
                <a href="./achieve?category=s" class="btn-cancel">취소</a>
                -->
            </div>
            
            <!-- //.signup-btn-div -->
        </div>
        <!-- //.contents -->
    </div>
    <!-- //.container-table -->

    <footer id="footer">
        <div class="footer_con">
            <div class="footer_address">
                <p>
                    경북대학교 의과대학 · 의과전문대학원 ADD. (41944)대구광역시 중구 국채보상로 680
                </p>
                <p>
                    TEL. 053-420-4910(교무), 4906(서무) FAX. 053-422-9195(교무), 420-4925(서무) E-mail. meddean@knu.ac.kr
                </p>
                <p>
                    Copyright(c)2014 경북대학교 의과대학·의과전문대학원 All rights reserved.
                </p>
            </div>
        </div>
    </footer>
    <!-- //#footer -->
</div>
<!-- //#wrap -->

    <!-- 증빙서류 등록 팝업 영역 -->
    <div class="wrap-popup wrap-plan-popup hidden popup-thesis">
    	<input type="hidden" name="uploadServiceCode" />
        <div class="popup-guide">
            <div class="popup-guide-header">
                <h2>
                    증빙서류
                </h2>
                <a class="btn-close-giude-popup" title="close">
                    X
                </a>
            </div>
            <div class="popup-guide-content">
                <fieldset>
                    <div class="popup-input-file-up-div">
                        <p class="popup-fd-title">
                            파일 추가를 누르시면 파일을 여러 개 등록할 수 있습니다.
                        </p>
                        <button type="button" class="btn-add-file">
                            + 파일 추가
                        </button>
                    </div>
                    <!-- //.popup-input-file-up-div -->

                    <div class="popup-input-file-up-div">
                        <!-- <p class="popup-fd-title">
                            <span class="color-blue">
                                (<span class="word-cut">
                                SCI급 논문
                                </span>)
                            </span> 증빙서류 첨부내역
                        </p>
                         -->
                        <div id="uploadFileList"></div>
                        <div class="btn-submit-div">
                            <a>완료</a>
                        </div>
                    </div>
                    <!-- //.popup-input-file-up-div -->
                </fieldset>
            </div>
        </div>
    </div>
<!-- //.wrap-popup -->
<div id="toggleExcelRegistLayer" style="display:none;">
			<div class="btn-down-div">
                <h2>
                    해당 되는 <ins>단체등록 양식을 다운로드</ins> 받으신 뒤 내용을 기입하시고,<br />
                    <ins>엑셀 파일을 아래 등록</ins>하시면 모든 교수님의 봉사등록이 일괄 완료됩니다.
                </h2>
                <div class="btn-execl-down-div">
                    <p>
                        봉사 일괄 등록양식<br />
						<a href="<c:url value="/download?path=file&fileName=achieve_service.xlsx"/>" target="_blank">
                            <img src="images/btn-exel-down.png" alt="엑셀다운로드" />
                        </a>
                    </p>
                </div>
            </div>
            <!-- //.btn-down-exel-div -->

            <!-- 이용자 등록 / 수정 영역 -->
            <form id='uploadForm' action='' onSubmit="return false;" enctype='multipart/form-data' method='post'>
            <div class="signup-div">
                <fieldset>
                    <legend>연구 일괄 등록 필드</legend>
                    <div>
                        <label>파일</label>
                        <p>
                            <input type="text" class="input-upload-txt" value="" readonly />
                            <label class="btn-input-file">
                                등록
                                <input type="file" class="input-upload" id="uploadInputFile" name="uploadInputFile" value="" />
                            </label>
                        </p>
                    </div>
                </fieldset>
            </div>
            </form>
            <!-- //.signup-div -->

            <div class="signup-btn-div" style="display:block">
                <a class="btn-signup check-addfile" href="javascript:uploadExcelFile();">봉사 일괄 등록</a>
                <a href="javascript:toggleExcelRegistLayer(false)" class="btn-cancel">취소</a>
            </div>
            <!-- //.signup-btn-div -->
	</div>
<form id="updateAchieve" method="post" action="<c:url value="/admin/updateAchieve"/>">
	<input type="hidden" name="requestUserCode" value=""/>
	<input type="hidden" name="category" value="s"/>
</form>

<!-- s_업적 수정 팝업 -->
    <div class="achievemodify wrap-plan-popup hidden popup-thesis" id="servicePopup" style="display: none;">
        <input type="hidden" name="uploadServiceCode" />
        <div class="popup-guide">
            <div class="popup-guide-header">
                <h2>업적 수정</h2>
                <a class="btn-close-giude-popup" name="servicePopupClose" title="close">X</a>
            </div>
            
            <div class="popup_content">
                <div class="formwrap">
                            
                            <input type="hidden" id="data_type" name="data_type" value="">
                            <input type="hidden" id="service_result_code" name="" value="">
                            <input type="hidden" name="direct_input_enabled" value="">                          
                            
                            <div id="dateDiv" class="datepick-div">
                                <div class="tt">기간</div>
                                <div>
                                    <input type="text" name="start_date" id="start_date" class="datepicker-input" placeholder="시작일">
                                    <button type="button" value="" class="btn-monthpick">
                                        <img src="images/calendar.png" alt="달력보기"> 
                                    </button>
                                    ~&nbsp;
                                </div>
                                <div>
                                    <input type="text" name="end_date" id="end_date" class="datepicker-input" placeholder="종료일">
                                    <button type="button" value="" class="btn-monthpick">
                                        <img src="images/calendar.png" alt="달력보기"> 
                                    </button>
                                </div>
                            </div>                          
                            
                            <div class="service-table-select-div">
                            <div class="tt">내용</div>
                            <p>
                                <select name="job_title_sel" id="contentText" onchange="javascript:selectJobTitle($(this));">
                                    
                                </select>
                            </p>
                            <p id="directInputP" class="">
                                <input type="text" name="direct_input" value="" placeholder="직접입력">
                            </p>
                            </div>
                            
                            <div class="service-table-select-div">
                            <div class="tt">점수</div>
                            <p>
                                <input type="text" class="ip01" id="point" placeholder="점수">
                            </p>
                            </div>
    
                </div>
                
                <div class="btn_wrap">
                    <input type="button" class="btn01" id="updateServiceBtn" value="수정">                    
                    <input type="button" class="btn02" onClick="$('a[name=servicePopupClose]').click();" value="취소">
                </div>
            </div>
        </div>
    </div>
<!-- e_업적 수정 팝업 -->
<script type="text/javascript" src="<c:url value="/js/jquery/jquery.form.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/achieve_service_update.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery/jquery.blockUI.js"/>"></script>
<script src="js/datetimep.js"></script>
<script>
var contentsHtml = $(".contents").html();
var userLevel = "${sessionScope.userlevel}";
var requestUserCode = "${requestUserCode}";
var userName = "${userName}";
var subject = "${subject}";
var category = "${category}";
var authInfo = null;
var isReady = false;
var periodDisableMode=null;
$(document).ready(initalize);
$(document).ready(function(){
	   // 이용자 이름 검색값 검사
 $(".btn-search-name").bind("click", function(){
     var input = $(this).siblings("input[type='text']");
     if ( input.val() == 0 ){
         alert("검색할 이름을 입력해주세요.");
         input.focus().val("");
     }
     findProfessor(input.val().trim());
 });
 $(".contents-serach-btns-div input").keydown(function (key) {
     if(key.keyCode == 13){//키가 13이면 실행 (엔터는 13)
     	 $(".btn-search-name").click();
     }
 });

});
function initalize(){
	// 좌측메뉴 펼침 / 접기 토글 스크림트
    $(".btn-coll").bind("click", function(){
        $(".btn-coll").toggleClass("move-trigger");
        $(".aside").toggleClass("hidden-important");
        $(".contents").toggleClass("width-100");
    });
    //탭메뉴
    $(".tab-content").hide();
    $(".tab-content:first").show();
    $(".tabs li").bind("click", function(){
        $(".tabs li").removeClass("active");
        $(this).addClass("active");
        $(".tab-content").hide();
        var activeTab = $(this).attr("data-to");
        $("." + activeTab).show();
    });
   
    // 미등록, 등록완료 숫자 버튼
    $(".aside .count-div p span a").each(function(){
        $(".aside .count-div p span a").bind("click",function(){
            var btnCount = $(".aside .count-div p span a");
            $(btnCount).removeClass("active");
            $(this).addClass("active");
        });
    });
    //datepicker
    $.datetimepicker.setLocale('kr');
    $('.datepicker-input').datetimepicker({
        timepicker:false,
        datepicker:true,
        //format:'y.m.d H:i',
        //formatDate:'y.m.d H:i'
        format:'y.m.d',
        formatDate:'y.m.d'
    });
    $('.timepicker-input').datetimepicker({
        timepicker:true,
        datepicker:false,
        //format:'y.m.d H:i',
        //formatDate:'y.m.d H:i'
        format:'H:i',
        formatDate:'H:i',
        step:10
    });
    $('.btn-monthpick').bind('click',function(){
        $(this).siblings('input').datetimepicker('show');
    });

        
    $(".service-nav a").removeClass("active");
    switch(category){
    case "e" :
    	$(".service-nav a").eq(2).addClass("active");
    	$("#categoryName").html("교육");
    	break;
    case "r" :
    	$(".service-nav a").eq(3).addClass("active");
    	$("#categoryName").html("연구");
    	break;
    case "s" : 
    	$(".service-nav a").eq(4).addClass("active");
    	$("#categoryName").html("봉사");
    	break;
	}
    
    $(".btn-close-giude-popup").bind("click", function(){
        $(".wrap-plan-popup").fadeOut(300);
        $("#servicePopup").hide();
        getServiceResult();
    });
    
    $(".btn-submit-div a").bind("click", function(){
        $(".wrap-plan-popup").fadeOut(300);
        $("#servicePopup").hide();
        getServiceResult();
    });
    
    
    // 파일 등록폼 추가
    $(".wrap-popup").on("click",".btn-add-file", function(){
        addFileForm();
    });
    
    $("#contents-title-name").html("<span class=\"name-pro\">" + userName + "</span><span>" + subject +"</span>");
    
	//getServiceResult();
    

    if(userLevel == 1){
		//$("#contents").html($("#permissionhiddenLayer").html());
    	
    	$(".total-score-tr").hide();
		$("#totalPointTitleLayer").hide();
    	$("#totalPointLayer").hide();
    	
		$("#service1List_totalPoint").hide();
		
    	
    	$("[class^='auth_category_']").hide();
    	
    	try{
    		authInfo = ${json.authInfo};	
    	}catch(e){
    		console.error("authInfo is null or empty");
    	}
    	
    	if(authInfo != null){
    		for(var i=0; i < authInfo.length; i++){
    			console.debug(authInfo[i]);
    			var categoryId = authInfo[i].categoryid;
    			$("[class^='auth_category_"+ categoryId +"']").show();
    		}
    	}else{
    		$("#contents").html($("#permissionhiddenLayer").html());
    	}
    	
    }
    
    hideUnPermissionLayer();
    if(!isReady){
    	getProfessorList();
    	isReady = true;
    }
    
	if(periodDisableMode != null && periodDisableMode == "C"){
	  	$(".contents-btns-div").remove();
	   	$(".signup-btn-div").remove();
	}
}
function hideUnPermissionLayer(){
	if(userLevel == 0){
		return;
	}
	if(authInfo != null){
		for(var i=0; i < authInfo.length; i++){
			console.debug(authInfo[i]);
			var categoryId = authInfo[i].categoryid;
			$("[class^='auth_category_"+ categoryId +"']").show();
		}
	}else{
		$("#contents").html($("#permissionhiddenLayer").html());
	}
}
function getPeriodInfo() {
	function getToday(){
        var date = new Date();
        var year  = date.getFullYear();
        var month = date.getMonth() + 1; // 0부터 시작하므로 1더함 더함
        var day   = date.getDate();
    
        if (("" + month).length == 1) { month = "0" + month; }
        if (("" + day).length   == 1) { day   = "0" + day;   }
		return ("" + year + month + day)	       
    }
	function getDate(str){
		str = str.toString();
		return str.substring(0,4) + "-"+ str.substring(4,6) + "-"+str.substring(6,9);
	}
	if(userLevel == 0){
		console.debug("관리자 ");
		return;
	}else{
		
	$.ajax({
        type: "POST",
        url: '<c:url value="/pro/getPeriodInfo"/>',
        data: {},
        dataType: "json",
        success: function(data, status) {
        	console.debug("getPeriodInfo ============");
        	console.debug(data);
        	if (data.status == "success") {
        		if(data.periodInfo == undefined || data.periodInfo == null){
        			//alert("조회기간 정보를 찾을 수 없습니다.\n 관리자에게 문의하세요");
        			blockResultData();
        			return;
        		}
        		var startDate_a = 0;
        		var endDate_a = 0;
        		var startDate_b = 0;
        		var endDate_b = 0;
        		var startDate_c = 0;
        		var endDate_c = 0;
        		for(var i=0; i < data.periodInfo.length; i++){
        			var period = data.periodInfo[i];
        			if(period.period_type == "A"){
        				startDate_a = parseInt(period.start_date,10);
        				endDate_a = parseInt(period.end_date,10);
        			}
        			if(period.period_type == "B"){
        				startDate_b = parseInt(period.start_date,10);
        				endDate_b = parseInt(period.end_date,10);
        			}
        			if(period.period_type == "C"){
        				startDate_c = parseInt(period.start_date,10);
        				endDate_c = parseInt(period.end_date,10);
        			}
        		}
        		if(startDate_a == 0 || startDate_a ==0){
        			//alert("조회기간 정보를 찾을 수 없습니다.\n 관리자에게 문의하세요");
        			blockResultData();
        			return;
        		}
        		
        		var rightNow = new Date();
        		var today = getToday() //rightNow.toLocaleString().slice(0,10).replace(/-/g,"");
        		today = parseInt(today,10);
        		console.log(today + " / " + startDate_a + " / " + endDate_a);
        		if(endDate_a >= today){
        			/*
        			console.debug("등록기간에 걸림");
        			periodDisableMode="A";
        			disableElements();
        			var msg = getDate(startDate_a)+ " 부터" + getDate(endDate_a) + "까지는 교수님 봉사 등록 기간입니다\n";
        			msg += "행정팀은 조회공시기간 " + getDate(startDate_b) + " 부터 "+ getDate(endDate_b) + " 까지 수정가능합니다";
        			alert(msg);
        			return;
        			*/
        		}
        		if(today >= startDate_c){
        			console.debug("확정조회공시 기간에 걸림");
        			disableElements();
        			periodDisableMode="C";
        			var msg = getDate(startDate_c)+ " 부터" + getDate(endDate_c) + "까지는 교수님 최종 조회 기간입니다\n";
        			msg += "조회만 가능합니다";
        			alert(msg);
        		}
        		if( (today >= startDate_a && today <= endDate_a)){
            	 	$("#contents").show();
            	 	$(".guide-txt-table-div").show();
            	 	//getServiceResult(disableMode);
            	 	return;
        		}
        		blockResultData();
        	} else {
        		alert("오류가 발생했습니다.");
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        }
    });
	}
}
function disableElements(){
	/*
	$("button").each(function(index){
		if($(this).attr("class") == "btn-service-popup"){
			//$(this).attr("onclick","javascript:disableButtons(true)");
		}else{
			$("button").attr("disabled",true);		
		}
	});
	*/
	$("button").attr("disabled",true);	
	$("#contents input").attr("disabled",true);
	$("select").attr("disabled",true);
	$("#contents input").attr("disabled",true);
	$(".contents-btns-div").hide();
	$("#contents input").css("border","0px");
}

function getProfessorList(){
	$.ajax({
        type: "POST"
        ,url: "./getProfessorList"
        ,data: { "achieveType": category}
        ,dataType: "json"
        ,async: true
        ,success: function(json){
           console.debug(json);
           if(json != undefined && json != null){
        	   if(json.status == "success"){
        		   if(json.list != undefined && json.list != null){
        			   $("#professorListLayer").empty();
        			   var confirm_status = "";
        			   for(var i=0; i <json.list.length; i++){
        				   var addClass = "";
        				   var addClass = json.list[i].registered == "Y" ? "regist" : "unregist";
        				   //console.debug("[" + requestUserCode + "]:"+ json.list[i].usercode + "|"+ json.list[i].username + "|" + json.list[i].registered);
        				   if(json.list[i].usercode == requestUserCode){
        					   addClass = addClass + " on";
        				   }        				   
        				   if(json.list[i].confirm_flag == "Y") confirm_status = "확인";
        				   else confirm_status = "<font color='red'>미확인</font>";
        						    
        				   var html = "";
        				   html += "<li id=\"professor_"+ json.list[i].usercode +"\" class=\""+ addClass + "\">";
        				   html +=     "<a href=\"javascript:getProfessorDetailData('"+ json.list[i].usercode + "')\">";
        				   html +=         "<span>"+ json.list[i].username + "</span><span>"+ json.list[i].subject + "["+ confirm_status +"]</span>";
        				   html +=     "</a>";
        				   html +=     "<input type='hidden' name='userName' value='" + json.list[i].username + "'/>";
        				   html +=     "<input type='hidden' name='subject' value='" + json.list[i].subject + "'/>";
        				   html += "</li>";
        				   $("#professorListLayer").append(html);
        			   }
        			   
        			   if(json.registInfo != undefined && json.registInfo != null){
	           			  	console.debug(json.registInfo);
	           			  	$("#registTotalProfessor").html(json.registInfo.totalcount);
	           			  	$("#unregistProfessor").html(json.registInfo.unregistcount);
	           			  	$("#registProfessor").html(json.registInfo.registcount);
           		   		}
        		   }
        	   }else{
        		   alert(json.message);
        	   }
           }
           return;
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
        	alert("오류가 발생하였습니다");
        }
        ,beforeSend:function() {
        	$.blockUI();
		}
		,complete:function() {
			$.unblockUI();
			getPeriodInfo();
		}
    });
	
}
function getAchieveRegistUserList(registered){
	$("[id^='professor_']").removeClass("on");
	$("#professorListLayer li").show();
	
	$(".count-div p").removeClass("stay");
	$(".contents-serach-btns-div input").val("");
	if(registered == "N"){
		$(".unregist").show();
		$(".regist").hide();
		$("#unregistProfessor").parent().parent().addClass("stay");;
	}else if(registered == "Y"){
		$(".unregist").hide();
		$(".regist").show();
		$("#registProfessor").parent().parent().addClass("stay");;
	}else{
		$(".unregist").show();
		$(".regist").show();
		$("#registTotalProfessor").parent().parent().addClass("stay");;
	}
	
	requestUserCode = null;
	$(".contents").html(contentsHtml);
	initalize();
}
function getProfessorDetailData(userCode){
	requestUserCode = userCode;
	$(".contents").html(contentsHtml);
	initalize();
	$(".view-all-pro a").css("border-bottom", "solid 0px #da2127");
	$("[id^='professor_']").removeClass("on");
	$("#professor_" + userCode).addClass("on");
	
	var userName = $("#professor_" + userCode).children("input[name='userName']").val();
	var subject = $("#professor_" + userCode).children("input[name='subject']").val();
	
	$("#contents-title-name").html("<span class=\"name-pro\">" + userName + "</span><span>" + subject +"</span>");
	$("#updateAchieve input[name='userName']").val(userName);
	$("#updateAchieve input[name='subject']").val(subject);
	
	if(periodDisableMode != null){
		if(periodDisableMode == "A" || periodDisableMode == "C"){
			disableElements();
		}
	}
	
	getServiceResult();
}


function findProfessor(userName){
	var array = new Array();
	$("#professorListLayer input[name^='userName']").each(function(){
		if($(this).val().includes(userName)){
			array.push($(this).parent().attr("id"));
	    }
	});
	console.debug("array =>"+ array);
	var showRegist = "total";
	if($(".count-div p").eq(1).attr("class") == "stay"){
		showRegist = "unregist";
	}else if($(".count-div p").eq(2).attr("class") == "stay"){
		showRegist = "regist";
	}else{
	}
	if(array != null && array.length > 0){
		$("#professorListLayer li").hide();
		for(var i=0; i< array.length; i++){
			
			var professor = $("#" + array[i]); 
			if(showRegist == "unregist"){
				if(professor.attr("class") == "unregist"){
					professor.show();	
				}
			}else if(showRegist == "regist"){
				if(professor.attr("class") == "regist"){
					professor.show();	
				}
			}else{
				$("#" + array[i]).show();	
			}
			
		}
	}else{
		alert("검색결과가 없습니다");		
	}
}

function toggleExcelRegistLayer(isShow){
	console.debug("showExcelRegistLayer()");
	if(isShow){
		$(".contents-btns-div").hide();
		$(".signup-btn-div").hide();
		$(".list-table-contents").hide();
		$(".guide-txt-table-div").hide();
		$(".contents").append($("#toggleExcelRegistLayer")[0].outerHTML);
		
		$("#toggleExcelRegistLayer").show();
		$("#toggleExcelRegistLayer .signup-btn-div").show();
		$(".input-upload").each(function(){
	        $(this).change(function(){
	            if( $(this).val() != "" ){
	                var value = $(this).val();
	                var chk = $(this).val().split('.').pop().toLowerCase();
	                if($.inArray(chk, ["xlsx","xls"]) == -1){
	                    alert("엑셀 파일만 등록해주세요.");
	                    $(this).val("");
	                    return true;
	                } else {
	                    $(this).parent().siblings("input[type='text']").val(value);
	                }
	            }
	        });
	    });
	}else{
		$(".contents-btns-div").show();
		$(".signup-btn-div").show();
		$(".list-table-contents").show();
		$(".guide-txt-table-div").show();
		$("#toggleExcelRegistLayer").remove();
		
	}
	
}

function uploadExcelFile(){
	var addFile = $(".input-upload").val();
    if ( addFile == 0 ){
        alert("엑셀 파일을 등록해주세요.");
        return true;
    }
    
    $("#uploadForm").ajaxForm({
		type: "POST",
		url:  '<c:url value="/admin/achieveExcelFile"/>',
		data: {
			"achieveType":"s"
		},
		dataType: "json",
		success: function(data, status){
			if (data.status == "success") {
				console.debug("data.resultCount =>"+ data.resultCount);
				alert("총 "+ data.resultCount + "건의 봉사 등록을 완료하였습니다.");
        	} else {
        		alert("오류가 발생했습니다");
        	}
		},
		error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        },
		beforeSend:function() {
			$.blockUI();
		},
		complete:function() {
			$.unblockUI();
		}
	}).submit();
}

function ServiceConfirm(){
	if(requestUserCode == ""){
		alert("좌측 리스트에서 교수를 선택해주세요");
		return;
	}
	
	if(!confirm("확인하시겠습니까?"))
		return;
	
	$.ajax({
        type: "POST"
        ,url: "./serviceConfirm"
        ,data: { "user_code": requestUserCode}
        ,dataType: "json"
        ,async: true
        ,success: function(json){
           if(json.status == "success"){
        	   alert("확인 되었습니다.");
        	   getProfessorList();
           }else{
               alert("실패");
           }
           return;
        }
        ,error:function (xhr, ajaxOptions, thrownError){
            console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
            alert("오류가 발생하였습니다");
        }
        ,beforeSend:function() {
            $.blockUI();
        }
        ,complete:function() {
            $.unblockUI();
            getPeriodInfo();
        }
    });	
}

</script>
</body>
</html>