<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../include/admin_header.jsp" %>
<div id="wrap">
    <%@ include file="../include/top.jsp" %>
    <!-- //.header -->

    <div class="container">

        <!-- 탭 메뉴 영역 -->
        <div class="tabmenu-div">
            <ul class="tabmenu-ul">
                <!-- 선택된 탭메뉴 표시 클래스 "active" -->
                <li >
                    <a href="<c:url value="/admin/"/>">이용자 관리</a>
                </li>
                <li>
                    <a href="<c:url value="/admin/gradeScore"/>">기준 점수</a>
                </li>
                <li class="active">
                    <a href="javascript:void(0)">기간 설정</a>
                </li>
            </ul>
        </div>
        <!-- //.tabmenu-div -->
        
        <!-- 이용자 구분 검색영역 -->
        <div class="contents-serach-btns-div" style="margin-bottom:0;">
            <div class="contents-search-div">
                <p>
                    <label for="search_year">년도 : </label>
                    <select id="searchYear">
                    	<option value="" selected>전체</option>
                        <option value="2016">2016</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                    </select>
                </p>
                <p>
                    <button type="button" class="btn-search-name" onclick="searchPeriodList()"><img src="images/btn-search.png" alt="검색" /></button>
                </p>
            </div>
            <!-- //.contents-search-div -->
        </div>
        <!-- //.contents-serach-btns-div -->
        
        <!--<div class="guide-top-txt-div">
            ※ 평가년도 : 평가대상 년도를 4자리로 등록합니다 (예: 2016)
        </div>-->
        <!-- //.guide-top-txt-div -->
        
        <!-- 이용자 구분 검색영역 -->
        <div class="contents-serach-btns-div range-up-div">
            <div class="contents-search-div">
                <p>
                    <label>평상시 등록 : </label>
                    <select id="period_a_year">
                        <option value="2016" selected>2016</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                    </select>
                </p>
                <p>
                    <label>기간설정 : </label>
                    <span>
                        <input  id="period_a_start" type="text" class="month-datepicker" value="" />
                        <button type="button" class="btn-monthpick"><img src="images/calendar.png" alt="달력보기" /></button>
                    </span>
                    ~
                    <span>
                        <input  id="period_a_end"type="text" class="month-datepicker" value="" />
                        <button type="button" class="btn-monthpick"><img src="images/calendar.png" alt="달력보기" /></button>
                    </span>
                </p>
                <p class="contents-btns-div">
                    <a class="btn-submit-cou" href="javascript:addPeriod('A')">등록</a>
                </p>
                <!--<div class="guide-txt-div">
                    (안내) 검수기간이 등록되면 해당 기간에는 교수님들의 연구/봉사/산학협력 등록이 제한됩니다. (조회만 가능) 
                </div>-->
            </div>
        </div>
        <div class="contents-serach-btns-div range-up-div">
            <div class="contents-search-div">
                <p>
                    <label>조회공시 설정 : </label>
                    <select id="period_b_year">
                        <option value="2016" selected>2016</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                    </select>
                </p>
                <p>
                    <label>기간설정 : </label>
                    <span>
                        <input id="period_b_start" type="text" class="month-datepicker" value="" />
                        <button type="button" class="btn-monthpick"><img src="images/calendar.png" alt="달력보기" /></button>
                    </span>
                    ~
                    <span>
                        <input id="period_b_end" type="text" class="month-datepicker" value="" />
                        <button type="button" class="btn-monthpick"><img src="images/calendar.png" alt="달력보기" /></button>
                    </span>
                </p>
                <p class="contents-btns-div">
                    <a class="btn-submit-cou" href="javascript:addPeriod('B')">등록</a>
                </p>
                <!--<div class="guide-txt-div">
                    (안내) 검수기간이 등록되면 해당 기간에는 교수님들의 연구/봉사/산학협력 등록이 제한됩니다. (조회만 가능) 
                </div>-->
            </div>
        </div>
        
        <div class="contents-serach-btns-div range-up-div">
            <div class="contents-search-div">
                <p>
                    <label>확정공시 설정 : </label>
                    <select id="period_c_year">
                        <option value="2016" selected>2016</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                    </select>
                </p>
                <p>
                    <label>기간설정 : </label>
                    <span>
                        <input id="period_c_start" type="text" class="month-datepicker" value="" />
                        <button type="button" class="btn-monthpick"><img src="images/calendar.png" alt="달력보기" /></button>
                    </span>
                    ~
                    <span>
                        <input id="period_c_end" type="text" class="month-datepicker" value="" />
                        <button type="button" class="btn-monthpick"><img src="images/calendar.png" alt="달력보기" /></button>
                    </span>
                </p>
                <p class="contents-btns-div">
                    <a class="btn-submit-par" href="javascript:addPeriod('C')">등록</a>
                </p>
                <!--<div class="guide-txt-div">
                    (안내) 평가확인 기간에만 교수님들께 My 메뉴를 통해 평가결과 확인 페이지가 제공됩니다. (기간을 등록하지 않으면 평가결과를 확인할 수 없습니다.)
                </div>-->
            </div>
            <!--<div class="contents-btns-div">
                <a>등록</a>
            </div>-->
        </div>
        <!-- //.contents-serach-btns-div -->
        

        <!-- 테이블 영역 -->
        <div class="list-table-contents">
            <!-- 페이지당 리스트 최대 6건 노출, 등록일 내림차순 -->
            <table class="service-table">
                <colgroup>
                    <col width="105" />
                    <col width="" />
                    <col width="" />
                    <col width="" />
                    <col width="105" />
                    <col width="105" />
                    <col width="180" />
                </colgroup>
                <tbody id="periodListLayer">
                <!-- 
                    <tr>
                        <th>
                            등록일
                        </th>
                        <th>
                            등록자 아이디
                        </th>
                        <th>
                            등록자 성명
                        </th>
                        <th>
                            구분
                        </th>
                        <th>
                            시작일
                        </th>
                        <th>
                            종료일
                        </th>
                        <th>
                            수정 / 삭제
                        </th>
                    </tr>
                    <tr>
                        <td>
                            2016.03.17
                        </td>
                        <td>
                            co121333acb
                        </td>
                        <td>
                            행정2
                        </td>
                        <td>
                            2016 평가확인기간
                        </td>
                        <td>
                            2016.04.17
                        </td>
                        <td>
                            2016.05.02
                        </td>
                        <td class="btn-re-td">
                            <a class="btn-range-re">수정</a>
                            <a class="btn-range-del">삭제</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            2016.03.15
                        </td>
                        <td>
                            dcgs22348sjs
                        </td>
                        <td>
                            행정1
                        </td>
                        <td>
                            2016 검수기간
                        </td>
                        <td>
                            2016.04.23
                        </td>
                        <td>
                            2016.05.02
                        </td>
                        <td class="btn-re-td">
                            <a class="btn-range-re">수정</a>
                            <a class="btn-range-del">삭제</a>
                        </td>
                    </tr>
                     -->
                </tbody>
            </table>
            <!-- //.service-table -->
            
            <!-- 페이징 숫자 표시 영역 -->
            <div class="table-list-num"  id="pagingLayer">
            </div>
            <!-- //.table-list-num -->
        </div>
        <!-- //.list-table-contents -->
    </div>
    <!-- //.container -->

   <%@ include file="../include/bottom.jsp" %>
</div>
<!-- //#wrap -->

<!-- 기간 수정 / 삭제 팝업 -->
<div class="wrap-popup wrap-rage-del-popup hidden">
	<input type="hidden" name="hiddenYear"/>
	<input type="hidden" name="hiddenStartDate"/>
	<input type="hidden" name="hiddenEndDate"/>
    <div class="popup-guide">
        <div class="popup-guide-header">
            <h2>
                <span name="title">2016 조회공시 기간 </span>수정
            </h2>
            <a class="btn-close-rage-popup" title="close">
                X
            </a>
        </div>
        <div class="popup-guide-content">
            <h3>
                 <span name="title">2016 조회공시 기간</span>
            </h3>
            <p class="popup-input-date-p">
                <label>기간설정 : </label>
                <span>
                    <input type="text" name="startDate" id="startDateId" class="month-datepicker" value="" />
                    <button type="button" class="btn-monthpick"><img src="images/calendar.png" alt="달력보기" /></button>
                </span>
                ~
                <span>
                    <input type="text" name="endDate" class="month-datepicker" value="" />
                    <button type="button" class="btn-monthpick"><img src="images/calendar.png" alt="달력보기" /></button>
                </span>
            </p>
            <!--<div class="textarea-div">
                <textarea cols="30" rows="4" placeholder="(필수등록) 검수기간 삭제사유를 남겨주세요."></textarea>
            </div>-->
            <div class="popup-btns-div">
                <!-- <button type="button">수정</button>-->
                <!--<button type="button">삭제</button>-->
                <!-- <button type="button" class="btn-close-rage-popup">취소</button>-->
            </div>
        </div>
    </div>
</div>
<!-- //.wrap-popup -->

<script src="js/datetimep.js"></script>
<script src="js/monthdatepicker_ui.js"></script>
<script type="text/javascript" src=<c:url value="/js/paging.js"/>></script>
<script>
$(document).ready(function(){
    // 월,일 달력 선택
    $(".month-datepicker").datepicker({
        changeYear:false,
        changeMonth: true,
        dateFormat:'yy-mm-dd',
        showOtherMonths: true,
        selectOtherMonths: true
    });
    $('.btn-monthpick').bind('click',function(){
        $(this).siblings('input').datepicker('show');
    });
    // 년,월,일 달력 선택
    $.datetimepicker.setLocale('kr');
    $('.dateyearpicker-input').datetimepicker({
        timepicker:false,
        datepicker:true,
        format:'y.m.d',
        formatDate:'y.m.d'
    });
    $('.btn-yearmonthpick').bind('click',function(){
        $(this).siblings('input').datetimepicker('show');
    });
    // 테이블 라인 배경
    $(".service-table tr:even").css("background","#fdfdfd");
    // 기간 수정, 삭제 팝업
    /*
    $(".btn-range-re,.btn-range-del").bind("click", function(){
        $("html").css("overflow-y","hidden");
        $(".wrap-rage-del-popup").fadeIn(200);
    });
    */
    $(".btn-close-rage-popup").bind("click", function(){
        $("html").css("overflow-y","auto");
        $(".wrap-rage-del-popup").fadeOut(200);
    });
    // 검수기간등록
    $(".btn-submit-cou,.btn-submit-par").bind("click", function(){
        var year = $(this).parent().siblings().children("input[type='text']");
        var month = $(this).parent().siblings().children().children("input.month-datepicker");
        var num = /[0-9]/g;
        if ( year.val() == 0 ){
            alert("평가년도를 입력해주세요");
            year.focus().val("");
            return false;
        } else if ( year.val().length != 4 || !num.test($.trim($(year).val())) ){
            alert("평가년도는 4자리 숫자로 입력해주세요.");
            year.focus().val("");
            return false;
        } 
        if ( month.val() == 0 ){
            alert("기간을 설정해주세요");
            $(month).first().focus();
            return ;
        }
    });
    
    $(".service-nav a").removeClass("active");
    $(".service-nav a").eq(0).addClass("active");
    
    getPeriodList(1);
});
</script>
<script>
var userLevel = "${sessionScope.userlevel}";
var searchOption = null;
function searchPeriodList(){
	if(searchOption == null){
		searchOption = new Object();
	}
	searchOption.searchYear = $("#searchYear").val();
	console.debug("searchYear =>"+ searchOption.searchYear);
	getPeriodList(1);
}
function getPageList(pageNo){
	getPeriodList(pageNo);
	if(searchOption == null){
		searchOption = new Object();
	}
	searchOption.pageNo = pageNo;
}
function getPeriodList(pageNo){
	var param = {};
	param.pageNo = pageNo;
	param.rowCount = 6;
	
	if(searchOption != null){
		if(searchOption.searchYear != "undefined"){
			param.searchYear = searchOption.searchYear;
		} 	
	}
	
	$.ajax({
        type: "POST",
        url: "./getPeriodList",
        data: param,
        dataType: "json",
        success: function(data, status) {
        	console.debug(data);
        	if (data.status == "success") {
        		setPeriodList(data.list);
                setPagingData(data.pagingInfo);
        	}else{
        		setPeriodList(null);	
        	}
        	
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
			console.error(error);
        }
    });
}
function setPeriodList(list){
	function getDate(str){
		str = str.toString();
		return str.substring(5,7) + "."+ str.substring(8,10);
	}
	console.debug(list);
    var html ="<tr><th>등록일 </th><th>등록자 아이디 </th><th>등록자 성명 </th><th>구분 </th><th>시작일 </th><th>종료일</th><th>수정 / 삭제 </th></tr>";
    if(list == null){
    	$("#periodListLayer").html(html);
    	return;
    }
    for(var i=0; i < list.length; i++){
    	var json = list[i];
    	html += "<tr>"
    	html += 	"<td>" + json.reg_date + "</td>";
    	html +=		"<td>" + json.user_id + "</td>";
    	html +=		"<td>" + json.user_name + "</td>";
    	html +=		"<td>" + "" + json.title + "</td>";
    	html +=		"<td>" + json.start_date + "</td>";
    	html +=		"<td>" + json.end_date +"</td>";
    	html += 	"<td class=\"btn-re-td\">";
    	html +=			"<a class=\btn-range-re\" href=\"javascript:updatePeriod("+ json.period_code + ", 'modify','"+ json.title +"','" +json.year + "','" + json.start_date + "','" + json.end_date + "')\">수정</a>";
    	html += 		"<a class=\btn-range-re\"  href=\"javascript:updatePeriod("+ json.period_code + ", 'delete','"+ json.title + "')\">삭제</a>";
    	html +=		"</td>";
    	html += "</tr>";
    }
    $("#periodListLayer").html(html);
}
function updatePeriod(periodCode,mode,title,year,startDate,endDate){
	switch(mode){
	case "delete":
		if(confirm(title + "를 삭제하시겠습니까?")){
			var contents = title + "이 삭제 되었습니다";
			$.ajax({
		        type: "POST",
		        url: "./updatePeriod",
		        data: {
		        	"useFlag" : "N"
		        	,"periodCode" : periodCode  
		        	,"contents" : contents
		        },
		        dataType: "json",
		        success: function(data, status) {
		        	console.debug(data);
		        	if (data.status == "success") {
		        		getPeriodList(1);
		        	}
		        },
		        error: function(xhr, textStatus) {
					alert("오류가 발생했습니다.");
					console.error(error);
		        }
		    });	
		}
		break;
	case "modify" :
	    $("html").css("overflow-y","hidden");
	    $(".wrap-rage-del-popup span[name='title']").html(title);
	    $(".wrap-rage-del-popup input[name='hiddenYear']").val(year);
	    
		$(".popup-guide input[name='startDate']").val(startDate);
	    $(".popup-guide input[name='endDate']").val(endDate);
	    
	    var html ="<button type=\"button\" onclick=\"updatePeriod("+ periodCode + ",'update','')\">수정</button>";
	    html += "<button type=\"button\" class=\"btn-close-rage-popup\">취소</button>";
	    $(".wrap-rage-del-popup .popup-btns-div").html(html);
        $(".wrap-rage-del-popup").fadeIn(200);
        $(".btn-close-rage-popup").bind("click", function(){
            $("html").css("overflow-y","auto");
            $(".wrap-rage-del-popup").fadeOut(200);
        });
		break;
	case "update":
		var start = $(".wrap-rage-del-popup input[name='startDate']");
		var end = $(".wrap-rage-del-popup input[name='endDate']");
		console.debug(start.val() + " ~ "+ end.val());
		if(start.val() == null || start.val() ==""){
			alert("시작일을 확인하세요");
			start.focus();
			return;
		}
		if(end.val() == null || end.val() ==""){
			alert("종료일을 확인하세요");
			end.focus();
			return;
		}
		var year = $(".wrap-rage-del-popup input[name='hiddenYear']").val();
		var title =  $(".wrap-rage-del-popup span[name='title']").html();
		
	    if(parseInt(year + start.val().replace(".",""),10) > parseInt(year + end.val().replace(".",""),10)){
	    	alert("시작일은 종료일보다 이전 이어야 합니다.")
	    	return;
	    }
	    //var startDate = year + "-" + start.val().replace(".","-") + " 00:00:00";
		//var endDate = year + "-" + end.val().replace(".","-") + " 23:59:59";
		//var contents = title + " 이 " +  year + "-" + start.val().replace(".","-") + "  ~ " + year + "-" + end.val().replace(".","-") + " 로 수정되었습니다";
		var startDate = start.val().replace(".","-") + " 00:00:00";
		var endDate = end.val().replace(".","-") + " 23:59:59";
		var contents = title + " 이 " + start.val().replace(".","-") + "  ~ " + end.val().replace(".","-") + " 로 수정되었습니다";
		$.ajax({
	        type: "POST",
	        url: "./updatePeriod",
	        data: {
	        	"useFlag" : "Y"
	        	,"periodCode" : periodCode  
	        	,"startDate" : startDate
	        	,"endDate" : endDate
	        	,"contents" : contents
	        },
	        dataType: "json",
	        success: function(data, status) {
	        	console.debug(data);
	        	if (data.status == "success") {
	        		getPeriodList(1);
	        	}
	        	 $("html").css("overflow-y","auto");
	             $(".wrap-rage-del-popup").fadeOut(200);
	        },
	        error: function(xhr, textStatus) {
				alert("오류가 발생했습니다.");
				console.error(error);
	        }
	    });	
		break;
	}
}
function addPeriod(type){
	var lType = type.toLowerCase();
	var year = $("#period_" + lType + "_year");
	var start = $("#period_" + lType + "_start");
	var end = $("#period_" + lType + "_end");
	if(year.val() == null || year.val() ==""){
		alert("연도를 확인하세요");
		year.focus();
		return;
	}
	if(start.val() == null || start.val() ==""){
		alert("시작일을 확인하세요");
		start.focus();
		return;
	}
	if(end.val() == null || end.val() ==""){
		alert("종료일을 확인하세요");
		end.focus();
		return;
	}
	if(parseInt(year.val() + start.val().replace(".",""),10) > parseInt(year.val() + end.val().replace(".",""),10)){
    	alert("시작일은 종료일보다 이전 이어야 합니다.")
    	return;
    }
	//var startDate = year.val() + "-" + start.val().replace(".","-") + " 00:00:00";
	//var endDate = year.val() + "-" + end.val().replace(".","-") + " 23:59:59";
	var startDate = start.val().replace(".","-") + " 00:00:00";
	var endDate = end.val().replace(".","-") + " 23:59:59";
	console.debug(startDate + " ~ " + endDate);
	var title = "";
	switch(type){
	case "A":
		title = year.val() + "년 등록 기간";
		break;
	case "B":
		title = year.val() + "년 확인 기간";
		break;
	case "C":
		title = year.val() + "년 평가 확인 기간";
		break;
	}
	//var contents =  year.val() + "-" + start.val().replace(".","-") + " 부터 " +  year.val() + "-" + end.val().replace(".","-") + " 까지 "  + title + " 입니다";
	var contents =  start.val().replace(".","-") + " 부터 " +  end.val().replace(".","-") + " 까지 "  + title + " 입니다";
	console.debug("title =>"+ title);
	$.ajax({
        type: "POST",
        url: "./addPeriod",
        data: {
        	"type": type
        	,"year" : year.val()
        	,"searchYear" : year.val()
        	,"startDate" : startDate
        	,"endDate" : endDate
        	,"title" : title
        	,"contents" : contents
        },
        dataType: "json",
        success: function(data, status) {
        	console.debug(data);
        	if (data.status == "success") {
        		alert(title + "이 등록되었습니다");
        		getPeriodList(1);
        	}else if(data.status =="fail"){
        		if(data.message == "duplicated"){
        			alert("기존에 등록된 ["+ title +"] 이 있습니다.\n기존정보를 먼저 확인바랍니다.");
        		}
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
			console.error(error);
        }
    });
}
</script>
<%@ include file="../include/footer.jsp" %>