<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../include/admin_header.jsp" %>
<div id="wrap">
    <%@ include file="../include/top.jsp" %>
    <!-- //.header -->

    <div class="container-table">
         <div class="aside">
            <div class="contents-serach-btns-div">
                <input type="text" value="" placeholder="이름" />
                <button type="button" class="btn-search-name"><img src="images/btn-search.png" alt="검색" /></button>
            </div>
            <!-- //.contents-serach-btns-div -->
            
            <div class="count-div">
            	<p class="stay">
                    <span>전체</span>
                    <span>
                        <a href="javascript:getAchieveRegistUserList('');" id="registTotalProfessor" class="btn-done-cnt active"></a>
                    </span>
                </p>
                <p class="">
                    <span>미등록</span>
                    <span>
                        <a href="javascript:getAchieveRegistUserList('N');" id="unregistProfessor" class="btn-done-cnt"></a>
                    </span>
                </p>
                <p>
                    <span>등록완료</span>
                    <span>
                        <a href="javascript:getAchieveRegistUserList('Y');" id="registProfessor"  class="btn-done-cnt"></a>
                    </span>
                </p>
            </div>
            <!-- //.count-div -->
            
            <!-- 교수 리스트 -->
            <ul class="list-pro">
                <li id="professorListLayer"></li>
            </ul>
        </div>
        <!-- //.aside -->
        

        <div class="contents">
            <a class="btn-coll"><span>좌측메뉴 영역 접기/펴기 토글</span></a>

            <div class="contents-serach-btns-div">
                <p class="contents-title-name" id="contents-title-name">
                </p>
                 
                <div class="contents-btns-div">
                      <a href="javascript:toggleExcelRegistLayer(true)">엑셀파일 일괄 등록하기 (전체 교수님)</a>
                </div>
                
            </div>
            <!-- //.contents-serach-btns-div -->

            <!-- 테이블 영역 -->
            <div class="list-table-contents">
            	<table class="service-table co-tabs-table autoheight">
                    <colgroup>
                        <col width="80" />
                        <col width="220" />
                        <col width="180" />
                        <col width="" />
                        <col width="90" />
                        <col width="" />
                        <col width="80" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <th colspan="2">
                                평가 항목(평가기간 이내)
                            </th>
                            <th>
                                기준점수
                            </th>
                            <th>
                                비고
                            </th>
                            <th>
                                건수(횟수)
                            </th>
                            <th>
                                내용
                            </th>
                            <th>
                                점수 확인
                            </th>
                        </tr>
                        <tr class="auth_category_ed01">
                            <td rowspan="9">
                                강의시간
                            </td>
                            <td>
                                강의 시간 수<br />
                                (해당학년도 1년간)
                            </td>
                            <td>
                                Sqrt<br />
                                (강의 시간 수)
                            </td>
                            <td class="text-left">
                                * 의학전문대학원의 실지로 시행된 강의시간 수<br />
                                * 인정(교과목강의, 국시특강, 대학원 공통과목, 기초의학리뷰)<br />
                                * 제외(실습, 대학원) 의전원 가중치 1, 타단대 가중치 0.7<br />
                                * 타단대강의 및 대학원공통과목은 증빙자료 개별 제출
                            </td>
                            <td class="score-input-td" >
                                <input type="text" value=""  id="achieve_edu_unit_1"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_1" ></textarea>
                            </td>
                            <td id="achieve_edu_point_1">
                                
                            </td>
                        </tr>
                        <!-- <tr class="auth_category_ed01">
                            <td class="border-l">
                                강의 시간 수(해당학년도 1년간)
								<br/>- 타단대 및 대학원 공통 과목
                            </td>
                            <td>
                                Sqrt<br />
                                (강의 시간 수)
                            </td>
                            <td class="text-left">
                               	타단대 및 대학공통과목은 증빙자료 개인별 제출
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_2" />
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_2" ></textarea>
                            </td>
                            <td id="achieve_edu_point_2">
                                
                            </td>
                        </tr> -->
                        <tr class="auth_category_ed01">
                            <td class="border-l">
                                기초세미나
                            </td>
                            <td>
                                과목 수 * 1.0
                            </td>
                            <td class="text-left">
                                해당년도 강의계획서 기준
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_2" />
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_2" ></textarea>
                            </td>
                            <td id="achieve_edu_point_2">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_ed01">
                            <td class="border-l">
                                PBL 튜터
                            </td>
                            <td>
                                회수(모듈 2회 기준) * 1.0
                            </td>
                            <td class="text-left">

                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_3"  />
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_3" ></textarea>
                            </td>
                            <td id="achieve_edu_point_3">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_ed01">
                            <td class="border-l">
                                주소바탕학습
                            </td>
                            <td>
                                회수 * 1.0
                            </td>
                            <td class="text-left">

                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_4" />
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_4" ></textarea>
                            </td>
                            <td id="achieve_edu_point_4">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_ed01">
                            <td class="border-l">
                                4학년자유실습,<br />
                                2학년 임상술기학
                            </td>
                            <td>
                                회수 * 0.5
                            </td>
                            <td class="text-left">
                                4학년자율학습(지도 혹은 강의) 기본단위 2시간을 1횟수로 함.
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_5" />
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_5" ></textarea>
                            </td>
                            <td id="achieve_edu_point_5">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_ed01">
                            <td class="border-l">
                                임상실습 중<br />
                                OSCE/CPX 전담교수
                            </td>
                            <td>
                                4주(주당 30분 기준) * 0.5
                            </td>
                            <td class="text-left">
                                CPX는 가중치 2, <span class="color-red">위탁형태인 경우 20% 인정함.</span>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_6"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_6" ></textarea>
                            </td>
                            <td id="achieve_edu_point_6">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_ed01">
                            <td class="border-l">
                                강좌책임/간사교수<br />
                                (해당학년도 1년간)
                            </td>
                            <td>
                                (책임+간사과목수) * 2
                            </td>
                            <td class="text-left">
                                다수인 경우(1/인원수)로 환산함.<br />
                                상한선 3과목, <span class="color-red">해당년도 강의계획서 기준</span>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_7"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_7" ></textarea>
                            </td>
                            <td id="achieve_edu_point_7">

                            </td>
                        </tr>
                        <tr class="auth_category_ed01">
                            <td class="border-l">
                                임상실습 교육책임<br />
                                (해당학년도 1년간)
                            </td>
                            <td>
                                1년 2점, 6개월 1.5점,<br />
                                개월 미만 0.5점,<br />
                                1개월 미만은 주당 0.1점
                            </td>
                            <td class="text-left color-red">
                                3,4학년 임상실습지침서 기준
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_8"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_8" ></textarea>
                            </td>
                            <td id="achieve_edu_point_8">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_ed01">
                            <td class="border-l color-red">
                                서브인턴 교육책임<br />
                                (해당학년도 1년간)
                            </td>
                            <td class="color-red">
                                0.5점/1주
                            </td>
                            <td class="text-left color-red">
                                3,4학년 임상실습지침서 기준
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_9"/>
                            </td>
                            <td class="vt-top">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_9" ></textarea>
                            </td>
                            <td id="achieve_edu_point_9">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_ed02">
                            <td>
                                강의 평가
                            </td>
                            <td>
                                평가점수
                            </td>
                            <td>
                                개별교수의 평균값
                            </td>
                            <td class="text-left">
                                강의평가가 없는 경우 0점
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_10"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_10" ></textarea>
                            </td>
                            <td id="achieve_edu_point_10">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_ed03">
                            <td rowspan="1">
                                시험관
                            </td>
                            <td>
                               * 교내CPX/OSCE<br/>
								* 대경모의시험CPX<br/>
								* 국시 실기시험<br/>
								* 임상술기학<br/>
								* 골학구두시험
                            </td>
                            <td>
                                회수<span class="color-red">(8시간 참여/1일)</span> * 2.0
                            </td>
                            <td class="text-left">
                                골학구두시험은 가중치 0.5<br />
                                임상술기학은 가중치 0.5
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_11"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_11" ></textarea>
                            </td>
                            <td id="achieve_edu_point_11">

                            </td>
                        </tr>
                         <!-- <tr class="auth_category_ed03">
                           
                            <td>
                               교내 OSCE
                            </td>
                            <td>
                                회수<span class="color-red">(8시간 참여/1일)</span> * 2.0
                            </td>
                            <td class="text-left">
                                골학구두시험은 가중치 0.5<br />
                                임상술기학은 가중치 0.5
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_11"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_11" ></textarea>
                            </td>
                            <td id="achieve_edu_point_11">

                            </td>
                        </tr>
                         <tr class="auth_category_ed03">
                           
                            <td>
                               대경모의시험 CPX
                            </td>
                            <td>
                                회수<span class="color-red">(8시간 참여/1일)</span> * 2.0
                            </td>
                            <td class="text-left">
                                골학구두시험은 가중치 0.5<br />
                                임상술기학은 가중치 0.5
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_11"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_11" ></textarea>
                            </td>
                            <td id="achieve_edu_point_11">

                            </td>
                        </tr>
                         <tr class="auth_category_ed03">
                            <td>
                               국시실기시험
                            </td>
                            <td>
                                회수<span class="color-red">(8시간 참여/1일)</span> * 2.0
                            </td>
                            <td class="text-left">
                                골학구두시험은 가중치 0.5<br />
                                임상술기학은 가중치 0.5
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_11"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_11" ></textarea>
                            </td>
                            <td id="achieve_edu_point_11">

                            </td>
                        </tr>
                         <tr class="auth_category_ed03">
                            <td>
                               골학구두시험
                            </td>
                            <td>
                                회수<span class="color-red">(8시간 참여/1일)</span> * 2.0
                            </td>
                            <td class="text-left">
                                골학구두시험은 가중치 0.5<br />
                                임상술기학은 가중치 0.5
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_11"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_11" ></textarea>
                            </td>
                            <td id="achieve_edu_point_11">

                            </td>
                        </tr>
                         <tr class="auth_category_ed03">
                            <td>
                               임상술기학
                            </td>
                            <td>
                                회수<span class="color-red">(8시간 참여/1일)</span> * 2.0
                            </td>
                            <td class="text-left">
                                골학구두시험은 가중치 0.5<br />
                                임상술기학은 가중치 0.5
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_11"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_11" ></textarea>
                            </td>
                            <td id="achieve_edu_point_11">

                            </td>
                        </tr> -->
                        <tr class="auth_category_ed04">
                            <td rowspan="3">
                                문항 및 교육모듈 개발
                            </td>
                            <td>
                                모의고사 학력평가 문제 출제 문항수
                            </td>
                            <td>
                                문항당 0.05점
                            </td>
                            <td class="text-left">
                                상한선 2.5점
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_12"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_12" ></textarea>
                            </td>
                            <td id="achieve_edu_point_12">

                            </td>
                        </tr>
                        <tr class="auth_category_ed04">
                            <td class="border-l">
                                PBL 등
                            </td>
                            <td>
                                                    회수 * 2.0
                            </td>
                            <td class="text-left">
								교수학습센터 프로그램 참가<br/>
								교수법개선, 수업개발 <br/>
								가점의 총합 X 0.1 <br/>
								상한선 2점
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_edu_unit_13"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_13" ></textarea>
                            </td>
                            <td id="achieve_edu_point_13">

                            </td>
                        </tr>
                        <tr class="auth_category_ed04">
                            <td class="border-l">
                                의학교육학회 세미나/심포지움 참석,<br />
                                자체의학교육세미나(동.하계)
                            </td>
                            <td>
                                회수 * 1.0
                            </td>
                            <td class="text-left">
                                교육학회 세미나/심포지움 참석은 상한선을 두어 3.0점을 최대로 함.<br />
                                2시간 미만은 가중치 0.5
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_unit_14"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="" id="achieve_edu_contents_14" ></textarea>
                            </td>
                            <td id="achieve_edu_point_14">

                            </td>
                        </tr>
                        <tr class="total-score-tr">
                            <td colspan="2">
                                총점
                            </td>
                            <td colspan="5" id="totalResultPoint">
                                00.00점
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- //.list-table-contents-->

            
            <!-- 등록 버튼 영역-->
            <div class="signup-btn-div text-right">
                <a class="btn-signup achieveDataSubmit" href="javascript:updateAchieveData();">등록</a>
                <a href="javascript:emptyAchieveData();" class="btn-cancel">취소</a>
                <!-- 
                <a href="co_3_1_2.html" class="btn-signup">수정</a>
                 -->
            </div>
            
            <!-- //.signup-btn-div -->
        </div>
        <!-- //.contents -->
    </div>
    <!-- //.container-table -->

    <footer id="footer">
        <div class="footer_con">
            <div class="footer_address">
                <p>
                    경북대학교 의과대학 · 의과전문대학원 ADD. (41944)대구광역시 중구 국채보상로 680
                </p>
                <p>
                    TEL. 053-420-4910(교무), 4906(서무) FAX. 053-422-9195(교무), 420-4925(서무) E-mail. meddean@knu.ac.kr
                </p>
                <p>
                    Copyright(c)2014 경북대학교 의과대학·의과전문대학원 All rights reserved.
                </p>
            </div>
        </div>
    </footer>
    <!-- //#footer -->
</div>
<!-- //#wrap -->

<div id="permissionhiddenLayer" class="hidden">

        <div class="date_not">
            <p><img src="images/not.png"></p>
            <p class="date_memo_title">
                <span id="categoryName">교육</span> 등록 권한이 없습니다.<br />
                관리자에게 문의해 주세요.
            <p>
            <p class="date_memo_text"></p>
            <p class="red_bt"><a href="javascript:void(0)">확인</a></p>
        </div>
</div>
	<div id="toggleExcelRegistLayer" style="display:none;">
			<div class="btn-down-div">
                <h2>
                    해당 되는 <ins>단체등록 양식을 다운로드</ins> 받으신 뒤 내용을 기입하시고,<br />
                    <ins>엑셀 파일을 아래 등록</ins>하시면 모든 교수님의 봉사등록이 일괄 완료됩니다.
                </h2>
                <div class="btn-execl-down-div">
                    <p>
                        교육 일괄 등록양식<br />
						<a href="<c:url value="/download?path=file&fileName=achieve_education.xlsx"/>" target="_blank">
                            <img src="images/btn-exel-down.png" alt="엑셀다운로드" />
                        </a>
                    </p>
                </div>
            </div>
            <!-- //.btn-down-exel-div -->

            <!-- 이용자 등록 / 수정 영역 -->
            <form id='uploadForm' action='' onSubmit="return false;" enctype='multipart/form-data' method='post'>
            <div class="signup-div">
                <fieldset>
                    <legend>연구 일괄 등록 필드</legend>
                    <div>
                        <label>파일</label>
                        <p>
                            <input type="text" class="input-upload-txt" value="" readonly />
                            <label class="btn-input-file">
                                	등록
                                <input type="file" class="input-upload" id="uploadInputFile" name="uploadInputFile" value="" />
                            </label>
                        </p>
                    </div>
                </fieldset>
            </div>
            </form>
            <!-- //.signup-div -->

            <div class="signup-btn-div" style="display:block">
                <a class="btn-signup check-addfile" href="javascript:uploadExcelFile();">교육 일괄 등록</a>
                <a href="javascript:toggleExcelRegistLayer(false)" class="btn-cancel">취소</a>
            </div>
            <!-- //.signup-btn-div -->
	</div>


<form id="updateAchieve" method="post" action="<c:url value="/admin/updateAchieve"/>">
	<input type="hidden" name="requestUserCode" value=""/>
	<input type="hidden" name="category" value="e"/>
	<input type="hidden" name="userName" />
	<input type="hidden" name="subject" />
</form>
<script type="text/javascript" src="<c:url value="/js/jquery/jquery.form.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/achieve_service_update.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery/jquery.blockUI.js"/>"></script>
<script src="js/datetimep.js"></script>
<script>
var contentsHtml = $(".contents").html();
var userLevel = "${sessionScope.userlevel}";
var requestUserCode = "${requestUserCode}";
var userName = "${userName}";
var subject = "${subject}";
var category = "${category}";
var authInfo = null;
var achievePointInfo = null;
var isReady = false;
var periodDisableMode = null;
var resultPointMap = new Map(); 
//$(document).ready(initalize);
$(document).ready(function(){
	   // 이용자 이름 검색값 검사
    $(".btn-search-name").bind("click", function(){
        var input = $(this).siblings("input[type='text']");
        if ( input.val() == 0 ){
            alert("검색할 이름을 입력해주세요.");
            input.focus().val("");
            return;
        }
        findProfessor(input.val().trim());
    });
    $(".contents-serach-btns-div input").keydown(function (key) {
        if(key.keyCode == 13){//키가 13이면 실행 (엔터는 13)
        	 $(".btn-search-name").click();
        }
    });
	
	initalize();
});
function initalize(){
	// 좌측메뉴 펼침 / 접기 토글 스크림트
    $(".btn-coll").bind("click", function(){
        $(".btn-coll").toggleClass("move-trigger");
        $(".aside").toggleClass("hidden-important");
        $(".contents").toggleClass("width-100");
    });
    //탭메뉴
    $(".tab-content").hide();
    $(".tab-content:first").show();
    $(".tabs li").bind("click", function(){
        $(".tabs li").removeClass("active");
        $(this).addClass("active");
        $(".tab-content").hide();
        var activeTab = $(this).attr("data-to");
        $("." + activeTab).show();
    });
 
    // 재입력
    $(".btn-reset").bind("click", function(){
        $(".score-input-td input").val("");
        $(".score-input-td input:first").focus();
    });
    // textarea 자동 높이 맞춤
    $(".autoheight").on("keyup", "textarea", function (e){
        $(this).css("height", "auto");
        $(this).height( this.scrollHeight );
    });
    $(".autoheight").find("textarea").keyup();
    // textarea 자동 포커스
    $(".autoheight td").click(function (){
        $(this).children("textarea").focus();
    });
    // 미등록, 등록완료 숫자 버튼
    $(".aside .count-div p span a").each(function(){
        $(".aside .count-div p span a").bind("click",function(){
            var btnCount = $(".aside .count-div p span a");
            $(btnCount).removeClass("active");
            $(this).addClass("active");
        });
    });

    $('.btn-monthpick').bind('click',function(){
        $(this).siblings('input').datetimepicker('show');
    });

    /*
	$("input[id^='achieve_edu_unit_']").keyup(function(event){
 		console.debug("point");
 		event = event || window.event;
		var keyCode = (event.which) ? event.which : event.keyCode;
 		if( ( keyCode >=48 && keyCode <= 57 ) || ( keyCode >=96 && keyCode <= 105 ) ){
 			if(isNaN($(this).val())){
 	 			alert("잘못 입력하였습니다.")
 	 			$(this).focus();
 	 			return;
 	 		}
 	 		var itemNum = $(this).attr("id").replace("achieve_edu_unit_","");
 	 		console.debug("itemNum =>"+ itemNum);
 	 		var calculatedPoint = calculatePoint(itemNum,$(this).val());
 	 		$("td[id='achieve_edu_point_" + itemNum + "']").html(calculatedPoint);
 	 		calculateTotalPoint();
 		}
 	});
    */
	$("input[id^='achieve_edu_unit_']").blur(function(event){
 		var point = $(this).val();
 		console.debug("unit blur~ =>"+ point  + " / " + isNaN(point));;
 		if(isNaN(point) || point.length == 0){
 			$(this).val("");
 		}
 		var itemNum = $(this).attr("id").replace("achieve_edu_unit_","");
		var calculatedPoint = calculatePoint(itemNum,$(this).val());
		$("td[id='achieve_edu_point_" + itemNum + "']").html(ToFloat(calculatedPoint));
 		calculateTotalPoint();
 	});
    
    $(".service-nav a").removeClass("active");
    switch(category){
    case "e" :
    	$(".service-nav a").eq(2).addClass("active");
    	$("#categoryName").html("교육");
    	break;
    case "r" :
    	$(".service-nav a").eq(3).addClass("active");
    	$("#categoryName").html("연구");
    	break;
    case "s" : 
    	$(".service-nav a").eq(4).addClass("active");
    	$("#categoryName").html("봉사");
    	break;
	}
    
    $(".btn-close-giude-popup").bind("click", function(){
        $(".wrap-plan-popup").fadeOut(300);
        getAchieveProfessorResult();
    });
    
    $(".btn-submit-div a").bind("click", function(){
        $(".wrap-plan-popup").fadeOut(300);
        getAchieveProfessorResult();
    });

    $("#contents-title-name").html("<span class=\"name-pro\">" + userName + "</span><span>" + subject +"</span>");
    
	//getAchieveProfessorResult();
    

    if(userLevel == 1){
		$("#contents").html($("#permissionhiddenLayer").html());
    	
    	$(".total-score-tr").hide();
		$("#totalPointTitleLayer").hide();
    	$("#totalPointLayer").hide();
    	
		$("#service1List_totalPoint").hide();
		
    	
    	$("[class^='auth_category_']").hide();
    	
    	try{
    		authInfo = ${json.authInfo};	
    	}catch(e){
    		console.error("authInfo is null or empty");
    	}
    	if(authInfo != null){
    		for(var i=0; i < authInfo.length; i++){
    			console.debug(authInfo[i]);
    			var categoryId = authInfo[i].categoryid;
    			$("[class^='auth_category_"+ categoryId +"']").show();
    		}
    	}
    }
    
    try{
    	achievePointInfo = ${json.achievePointInfo};
    	console.debug("====================achievePointInfo");
    	console.debug(achievePointInfo);
    }catch(e){
    	console.error("achievePointInfo is null or empty");
    }
    
    
    
    if(!isReady && toggleUnPermissionLayer()){
    	getProfessorList();
    	isReady = true;
    }
    
    $(".score-input-td input").css("border","solid 0px #ccc");
    
    if(periodDisableMode != null && periodDisableMode == "C"){
    	$(".contents-btns-div").remove();
    	$(".signup-btn-div").remove();
    }
}
function toggleUnPermissionLayer(){
	if(userLevel == 0){
		return true;
	}
	console.debug("toggleUnPermissionLayer=============");
	console.debug(authInfo);
	
	var edCount = 0;
	if(authInfo != null){
		for(var i=0; i < authInfo.length; i++){
			console.debug(authInfo[i]);
			var categoryId = authInfo[i].categoryid;
			$("[class^='auth_category_"+ categoryId +"']").show();
			if(authInfo[i].categorycode == "ed"){
				edCount++;
			}
		}
		if(edCount == 0){
			$(".contents").html($("#permissionhiddenLayer").html());
			return false;
		}
		return true;
	}else{
		$(".contents").html($("#permissionhiddenLayer").html());
		return false;
	}
}
function getPeriodInfo() {
	function getToday(){
        var date = new Date();
        var year  = date.getFullYear();
        var month = date.getMonth() + 1; // 0부터 시작하므로 1더함 더함
        var day   = date.getDate();
    
        if (("" + month).length == 1) { month = "0" + month; }
        if (("" + day).length   == 1) { day   = "0" + day;   }
		return ("" + year + month + day)	       
    }
	function getDate(str){
		str = str.toString();
		return str.substring(0,4) + "-"+ str.substring(4,6) + "-"+str.substring(6,9);
	}
	if(userLevel == 0){
		console.debug("관리자 ");
		return;
	}else{
		$.ajax({
	        type: "POST",
	        url: '<c:url value="/pro/getPeriodInfo"/>',
	        data: {},
	        dataType: "json",
	        success: function(data, status) {
	        	console.debug("getPeriodInfo ============");
	        	console.debug(data);
	        	if (data.status == "success") {
	        		if(data.periodInfo == undefined || data.periodInfo == null){
	        			//alert("조회기간 정보를 찾을 수 없습니다.\n 관리자에게 문의하세요");
	        			blockResultData();
	        			return;
	        		}
	        		var startDate_a = 0;
	        		var endDate_a = 0;
	        		var startDate_b = 0;
	        		var endDate_b = 0;
	        		var startDate_c = 0;
	        		var endDate_c = 0;
	        		for(var i=0; i < data.periodInfo.length; i++){
	        			var period = data.periodInfo[i];
	        			if(period.period_type == "A"){
	        				startDate_a = parseInt(period.start_date,10);
	        				endDate_a = parseInt(period.end_date,10);
	        			}
	        			if(period.period_type == "B"){
	        				startDate_b = parseInt(period.start_date,10);
	        				endDate_b = parseInt(period.end_date,10);
	        			}
	        			if(period.period_type == "C"){
	        				startDate_c = parseInt(period.start_date,10);
	        				endDate_c = parseInt(period.end_date,10);
	        			}
	        		}
	        		if(startDate_a == 0 || startDate_a ==0){
	        			//alert("조회기간 정보를 찾을 수 없습니다.\n 관리자에게 문의하세요");
	        			blockResultData();
	        			return;
	        		}
	        		
	        		var rightNow = new Date();
	        		var today = getToday(); //rightNow.toLocaleString().slice(0,10).replace(/-/g,"");
	        		today = parseInt(today,10);
	        		console.log(today + " / " + startDate_a + " / " + endDate_a);
	        		if(endDate_a >= today){
	        			console.debug("등록기간에 걸림");
	        			//periodDisableMode="A";
	        			//var msg = getDate(startDate_a)+ " 부터" + getDate(endDate_a) + "까지는 교수님 봉사 등록 기간입니다\n";
	        			//msg += "행정팀은 조회공시기간 " + getDate(startDate_b) + " 부터 "+ getDate(endDate_b) + " 까지 수정가능합니다";
	        			//alert(msg);
	        			//return;
	        		}
	        		if(today >= startDate_c && today <= endDate_c){
	        			console.debug("확정조회공시 기간에 걸림");
	        			disableElements();
	        			periodDisableMode="C";
	        			var msg = getDate(startDate_c)+ " 부터" + getDate(endDate_c) + "까지는 교수님 최종 조회 기간입니다\n";
	        			msg += "조회만 가능합니다";
	        			alert(msg);
	        			return;
	        		}
	        	} else {
	        		alert("오류가 발생했습니다.");
	        	}
	        },
	        error: function(xhr, textStatus) {
				alert("오류가 발생했습니다.");
	        }
	    });
	}
}
function disableElements(){
	/*
	$("button").each(function(index){
		if($(this).attr("class") == "btn-service-popup"){
			//$(this).attr("onclick","javascript:disableButtons(true)");
		}else{
			$("button").attr("disabled",true);		
		}
	});
	*/
	$(".contents button").attr("disabled",true);
	$(".contents input").attr("disabled",true);
	$("select").attr("disabled",true);
	$(".contents textarea").attr("disabled",true);
	
	$(".signup-btn-div").hide();
	$(".contents-btns-div").hide();
	
	$(".contents input").css("border","0px");
	$(".contents textarea").css("border","0px");
}
function calculateTotalPoint(){
	var totalPoint = 0.0;
	$("td[id^=achieve_edu_point_]").each(function(e){
		var point = $.trim($(this).html());
		if(point.length > 0){
    		if(!isNaN(point)){
				totalPoint = parseFloat(totalPoint) + parseFloat(point);
				console.debug("totalPoint =>"+ totalPoint );
    		}			
		}
	});
	console.debug("totalPoint =>"+ totalPoint);
	//totalPoint = parseFloat(totalPoint).toFixed(2);
	$("#totalResultPoint").html(ToFloat(totalPoint.toFixed(4)) + " 점");
}
function calculatePoint(itemNum, point){
	console.debug(point);
	var resultPoint = 0;
	for(var i=0; i < achievePointInfo.length;i++){
		var info = achievePointInfo[i];
		if(itemNum == info.education_item_num){
			resultPoint = point * info.point;
		}
	}
	return ToFloat(resultPoint);
}
function getProfessorList(){
	$.ajax({
        type: "POST"
        ,url: "./getProfessorList"
        ,data: {"achieveType" : category}
        ,dataType: "json"
        ,global : false
        ,cache: false
        ,async: true
        ,success: function(json){
           console.debug(json);
           if(json != undefined && json != null){
        	   if(json.status == "success"){
        		   if(json.list != undefined && json.list != null){
        			   $("#professorListLayer").empty();
        			   for(var i=0; i <json.list.length; i++){
        				   var addClass = "";
        				   var addClass = json.list[i].registered == "Y" ? "regist" : "unregist";
        				   //console.debug("[" + requestUserCode + "]:"+ json.list[i].usercode + "|"+ json.list[i].username + "|" + json.list[i].registered);
        				   if(json.list[i].usercode == requestUserCode){
        					   addClass = addClass + " on";
        				   }        				           				   
        				   var html = "";
        				   html += "<li id=\"professor_"+ json.list[i].usercode +"\" class=\""+ addClass + "\">";
        				   html +=     "<a href=\"javascript:getProfessorDetailData('"+ json.list[i].usercode + "')\">";
        				   html +=         "<span>"+ json.list[i].username + "</span><span>"+ json.list[i].subject +"</span>";
        				   html +=     "</a>";
        				   html +=     "<input type='hidden' name='userName' value='" + json.list[i].username + "'/>";
        				   html +=     "<input type='hidden' name='subject' value='" + json.list[i].subject + "'/>";
        				   html += "</li>";
        				   $("#professorListLayer").append(html);
        			   }
        			   
        			   if(json.registInfo != undefined && json.registInfo != null){
	           			  	console.debug(json.registInfo);
	           			  	$("#registTotalProfessor").html(json.registInfo.totalcount);
	           			  	$("#unregistProfessor").html(json.registInfo.unregistcount);
	           			  	$("#registProfessor").html(json.registInfo.registcount);
           		   		}
        		   }
        	   }else{
        		   alert(json.message);
        	   }
           }
           return;
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
        	alert("오류가 발생하였습니다");
        }
        ,beforeSend:function() {
        	$.blockUI();
		}
		,complete:function() {
			$.unblockUI();
			getPeriodInfo();
		}
    });
	
}
function getAchieveRegistUserList(registered){
	$("[id^='professor_']").removeClass("on");
	$("#professorListLayer li").show();
	
	$(".count-div p").removeClass("stay");
	$(".contents-serach-btns-div input").val("");
	if(registered == "N"){
		$(".unregist").show();
		$(".regist").hide();
		$("#unregistProfessor").parent().parent().addClass("stay");;
	}else if(registered == "Y"){
		$(".unregist").hide();
		$(".regist").show();
		$("#registProfessor").parent().parent().addClass("stay");;
	}else{
		$(".unregist").show();
		$(".regist").show();
		$("#registTotalProfessor").parent().parent().addClass("stay");;
	}
	
	requestUserCode = null;
	$(".contents").html(contentsHtml);
	initalize();
}
function getProfessorDetailData(userCode){
	requestUserCode = userCode;
	$(".contents").html(contentsHtml);
	initalize();
	$(".view-all-pro a").css("border-bottom", "solid 0px #da2127");
	$("[id^='professor_']").removeClass("on");
	$("#professor_" + userCode).addClass("on");
	
	var userName = $("#professor_" + userCode).children("input[name='userName']").val();
	var subject = $("#professor_" + userCode).children("input[name='subject']").val();
	
	$("#contents-title-name").html("<span class=\"name-pro\">" + userName + "</span><span>" + subject +"</span>");
	$("#updateAchieve input[name='userName']").val(userName);
	$("#updateAchieve input[name='subject']").val(subject);
	
	if(periodDisableMode != null){
		if(periodDisableMode == "A" || periodDisableMode == "C"){
			disableElements();
		}
	}
	getAchieveProfessorResult(userCode);
}

function getAchieveProfessorResult(userCode) {
	$.ajax({
        type: "POST",
        url: "./getAchieveProfessorResult",
        data: {
        	"userCode": userCode
        	,"achieveType": "e"
        },
        dataType: "json",
        success: function(data, status) {
        	console.debug("getAchieveProfessorResult ==================================================");
        	console.debug(data);
        	if (data.status == "success") {
				for(var i=0; i< data.result.length; i++){
					var row = data.result[i];
					var itemNum = row.education_item_num;
					//achieve_edu_unit_14  achieve_edu_point_14  achieve_edu_contents_14
					$("#achieve_edu_unit_" + itemNum).val(row.unit == "0" ? "" : row.unit);
					$("#achieve_edu_contents_" + itemNum).val(row.contents == "null"? "" : row.contents );
					$("#achieve_edu_point_" + itemNum).html(row.point == "0" ? "" : ToFloat(row.point));
					
					$(".achieveDataSubmit").html("수정");
				}
				/*
				$("[id^='achieve_edu_contents_']").each(function(){
				    while($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
				        $(this).height($(this).height()+1);
				    };
				});
				*/
				$("textarea").each(function(){
				    $(this).css('height', 'auto' );
				    $(this).height( this.scrollHeight );
				});
				$("textarea").css("border","solid 1px #ccc");
				$(".score-input-td input").css("border","solid 1px #ccc");
				
				calculateTotalPoint();
        	}
        },
        error: function(xhr, textStatus) {
			//alert("오류가 발생했습니다.");
			console.error(error);
        }
        ,complete:function() {
			if(periodDisableMode != null){
				if(periodDisableMode == "A" || periodDisableMode == "C"){
					disableElements();
				}
			}
		}
        
    });
}
function updateAchieveData(){
	console.debug("updateAchieveData()");
	var paramObject = {};
	var paramList ={};
	var size = $("[id^='achieve_edu_unit_']").length;
	var cnt = 0;
	for(var num = 1; num <= size; num++){
		var unit = $("#achieve_edu_unit_" + num).val();
		var contents = $("#achieve_edu_contents_" + num).val();
		var point = $("#achieve_edu_point_" + num).html();
		
		if(unit.length == 0 && contents.length != 0){
			alert("건수를 입력하세요");
			$("#achieve_edu_unit_" + num).focus();
			console.debug("unit이 비어잇슴");
			return;
		}
		if(unit.length != 0 && contents.length == 0){
			alert("내용을 입력하세요");
			$("#achieve_edu_contents_" + num).focus();
			console.debug("컨텐츠가비어잇슴");
			return;
		}
		
		var useFlag = "Y";
		/*
		if(unit.length == 0 || unit == 0 || unit == ""){
			useFlag = "N"
			contents = null;
			point = "0";
			unit = "0";
		}
		*/
		var param = {
			"userCode" : requestUserCode
			,"educationCode": num
			,"unit": unit
			,"contents" : contents
			,"point" : point
			,"useFlag" : useFlag
		}
		//paramList[num] = param;	
		//cnt++;
		console.debug("contents =>"+ contents);
		console.debug(contents);
		if(unit != null && contents != null && unit.length > 0 && contents.length > 0){
			paramList[num] = param;	
			cnt++;
		}
		
	}
	paramObject.achieveType = "e";
	paramObject.requestUserCode = requestUserCode;
	paramObject.paramList = paramList;
	console.debug(paramObject);
	console.debug(JSON.stringify(paramObject));
	console.debug("cnt =>"+ cnt);
	if(cnt == 0 || requestUserCode == "undefined" || requestUserCode == null || requestUserCode ==""){
		return;
	}
	
	$.ajax({
        type: "POST",
        url: "./updateAchieveData",
        contentType : 'application/json; charset=UTF-8',
        data: JSON.stringify(paramObject),
        dataType: "json",
        success: function(data, status) {
        	console.debug(data);
        	if(data.status == "success"){
        		alert("등록되었습니다");
        		getProfessorList();
        	}else{
        		alert("오류가 발생했습니다");
        	}
        },
        error: function(xhr, textStatus) {
			//alert("오류가 발생했습니다.");
			alert(error);
        }
    });
	
}
function emptyAchieveData(){
	if(confirm("취소하시겠습니까?")){
		$("[id^='achieve_edu_unit_']").val("");
		$("[id^='achieve_edu_contents_']").val("");
		$("[id^='achieve_edu_point_']").html("");
		
		$("[id^='professor_']").removeClass("on");
		
		$("#contents-title-name").html("<span class=\"name-pro\">" +  "</span><span>" + "</span>");
		$("#updateAchieve input[name='userName']").val("");
		$("#updateAchieve input[name='subject']").val("");
		
		$("textarea").css("border","solid 0px #ccc");
		$(".score-input-td input").css("border","solid 0px #ccc");
		$(".achieveDataSubmit").html("등록");		
	}
}


function findProfessor(userName){
	var array = new Array();
	$("#professorListLayer input[name^='userName']").each(function(){
		if($(this).val().includes(userName)){
			array.push($(this).parent().attr("id"));
	    }
	});
	console.debug("array =>"+ array);
	var showRegist = "total";
	if($(".count-div p").eq(1).attr("class") == "stay"){
		showRegist = "unregist";
	}else if($(".count-div p").eq(2).attr("class") == "stay"){
		showRegist = "regist";
	}else{
	}
	if(array != null && array.length > 0){
		$("#professorListLayer li").hide();
		for(var i=0; i< array.length; i++){
			
			var professor = $("#" + array[i]); 
			if(showRegist == "unregist"){
				if(professor.attr("class") == "unregist"){
					professor.show();	
				}
			}else if(showRegist == "regist"){
				if(professor.attr("class") == "regist"){
					professor.show();	
				}
			}else{
				$("#" + array[i]).show();	
			}
			
		}
	}else{
		alert("검색결과가 없습니다");		
	}
}


function toggleExcelRegistLayer(isShow){
	console.debug("showExcelRegistLayer()");
	if(isShow){
		$(".contents-title-name").hide();
		$(".contents-btns-div").hide();
		$(".signup-btn-div").hide();
		$(".list-table-contents").hide();
		$(".contents").append($("#toggleExcelRegistLayer")[0].outerHTML);
		
		$("#toggleExcelRegistLayer").show();
		$("#toggleExcelRegistLayer .signup-btn-div").show();
		$(".input-upload").each(function(){
	        $(this).change(function(){
	            if( $(this).val() != "" ){
	                var value = $(this).val();
	                var chk = $(this).val().split('.').pop().toLowerCase();
	                if($.inArray(chk, ["xlsx","xls"]) == -1){
	                    alert("엑셀 파일만 등록해주세요.");
	                    $(this).val("");
	                    return true;
	                } else {
	                    $(this).parent().siblings("input[type='text']").val(value);
	                }
	            }
	        });
	    });
	}else{
		$(".contents-title-name").show();
		$(".contents-btns-div").show();
		$(".signup-btn-div").show();
		$(".list-table-contents").show();
		$("#toggleExcelRegistLayer").remove();
		
	}
	
}
function uploadExcelFile(){
	var addFile = $(".input-upload").val();
    if ( addFile == 0 ){
        alert("엑셀 파일을 등록해주세요.");
        return true;
    }
    
    $("#uploadForm").ajaxForm({
		type: "POST",
		url:  '<c:url value="/admin/achieveExcelFile"/>',
		data: {
			"achieveType":"e"
		},
		dataType: "json",
		success: function(data, status){
			if (data.status == "success") {
				console.debug("data.resultCount =>"+ data.resultCount);
				alert("총 "+ data.resultCount + "건의 교육 등록을 완료하였습니다.");
        	} else {
        		alert("오류가 발생했습니다");
        	}
		},
		error: function(xhr, textStatus) {
			alert("오류가 발생했습니다." + textStatus);
        },
		beforeSend:function() {
			$.blockUI();
		},
		complete:function() {
			$.unblockUI();
		}
	}).submit();
}


function ToFloat(number){
    var tmp = number + "";
    if(tmp.indexOf(".") != -1){
        number = Math.floor(number*100)/100;
//        number = number.replace(/(0+$)/, "");
    }

    return number;
}
</script>
</body>
</html>