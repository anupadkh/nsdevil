<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../include/admin_header.jsp" %>
<div id="wrap">
    <%@ include file="../include/top.jsp" %>
    <!-- //.header -->

	<style>
.contentsTextArea{
	border:solid 1px red;
	resize:none;
	wrap:hard;
	text-align:left
	
}
</style>
	
    <div class="container">

        <!-- 탭 메뉴 영역 -->
        <div class="tabmenu-div">
            <ul class="tabmenu-ul">
                <!-- 선택된 탭메뉴 표시 클래스 "active" -->
                <li >
                    <a href="<c:url value="/admin/"/>">이용자 관리</a>
                </li>
                <li class="active">
                    <a href="javascript:void(0)">기준 점수</a>
                </li>
                <li >
                    <a href="<c:url value="/admin/period"/>">기간 설정</a>
                </li>
            </ul>
        </div>
        <!-- //.tabmenu-div -->

        <!-- 검색영역 -->
        <div class="contents-serach-btns-div">
            <div class="contents-search-div">
            	<p>
                    <label for="search_year">년도 : </label>
                    <select id="searchYear">
                    	<option value=""></option>
                        <option value="2017" selected>2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                    </select>
                </p>
                <p>
                    <select class="search-div-select" id="achieveType">
                        <option value="e">교육</option>
                        <option value="r">연구</option>
                        <option value="si">봉사(교내)</option>
                        <option value="so">봉사(교외)</option>
                    </select>
                    <button type="button" class="btn-search-name" onclick="getGradeScoreInfo();"><img src="images/btn-search.png" alt="검색" /></button>
                </p>
            </div>
            <!-- //.contents-search-div -->
        </div>
        <!-- //.contents-serach-btns-div -->

        <!-- 테이블 영역 -->
        <div class="list-table-contents">
            <!-- 교육 -->
            <div class="search-select-div search-div-edu">
                <table class="service-table">
                    <colgroup>
                        <col width="10%" />
                        <col width="20%" />
                        <col width="25%" />
                        <col width="20%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="5%" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <th colspan="2">
                                평가 항목(평가기간 이내)
                            </th>
                            <th>
                                평가기준
                            </th>
                            <th>
                                기준점수
                            </th>
                            <th>
                                가중치
                            </th>
                            <th>
                                상한선
                            </th>
                            <th>
                                최소기준
                            </th>
                        </tr>
                        <tr>
                            <td rowspan="10">
                                강의시간
                            </td>
                            <td class="border-l-text-left">
                                강의 시간 수(해당학년도 1년간)<br />
                                - 의예과<br />
                                - 의과학기초연구의전원<br />
                                - 의과대학
                            </td>
                            <td class="text-left" >
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_1">의학전문대학원 및 의과대학의 실지로 시행된 강의시간 수 인정(교과목강의, 국시특강, 대학원 공통과목)
                            </textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_1" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select id="achieve_edu_unit_1">
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="9" id="achieve_edu_weight_point_1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text" id="achieve_edu_limit_point_1" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_edu_min_check_1">
  								<label for="achieve_edu_min_check_1"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	강의 시간 수(해당학년도 1년간)<br>
								- 타단대 및 대학원 공통 과목
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_2">타단대 및 대학공통과목은 증빙자료 개인별 제출</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_2" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_edu_unit_2">
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_weight_point_2"  onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)" />
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_limit_point_2" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_edu_min_check_2">
  								<label for="achieve_edu_min_check_2"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	기초 세미나
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_3">해당년도 강의 계획서 기준</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_3" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_edu_unit_3" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_weight_point_3"  value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_limit_point_3" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_edu_min_check_3">
  								<label for="achieve_edu_min_check_3"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	PBL 튜터
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_4">회수(모듈 2회 기준)</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_4" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_edu_unit_4" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_weight_point_4" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_limit_point_4" value="없음" />
                            </td>
                           <td class="checks">
                               	<input type="checkbox"  id="achieve_edu_min_check_4">
  								<label for="achieve_edu_min_check_4"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	주소바탕학습
                            </td>
                            <td>
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_5"></textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_5" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_edu_unit_5" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_weight_point_5"  value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_limit_point_5"  value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_edu_min_check_5">
  								<label for="achieve_edu_min_check_5"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	4학년 자율실습, 2학년 임상술기학
                            </td>
                            <td>
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_6"></textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_6" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_edu_unit_6" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_weight_point_6" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_limit_point_6" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_edu_min_check_6">
  								<label for="achieve_edu_min_check_6"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	임상실습 중 OSCE/CPX 전담 교수
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_7">4주(주당 30분)을 1회로 인정하여 입력	
- CPX는 가중치 2, 위탁형태의 경우는 20% 인정 </textarea></td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_7" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_edu_unit_7" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_weight_point_7"  value="0.5" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_limit_point_7" value="없음" />
                            </td>
                           	<td class="checks">
                               	<input type="checkbox" id="achieve_edu_min_check_7">
  								<label for="achieve_edu_min_check_7"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	강좌책임/간사교수<br>
								(해당학년도 1년간) 
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_8">다수인 경우(1/인원수)로 환산하여 입력
- 상한선 3과목. 해당학년도 강의 계획서 기준
                            </textarea> 
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_8" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select id="achieve_edu_unit_8">
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_weight_point_8"  value="2" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"   id="achieve_edu_limit_point_8"  value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_edu_min_check_8">
  								<label for="achieve_edu_min_check_8"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	임상실습 교육책임<br>
								(해당학년도 1년간)
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_9">1년 2점, 6개월 1.5점, 6개월 미만 0.5점, 1개월 미만은 주당 1점으로 계산하여 입력</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_9"  value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_edu_unit_9" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_weight_point_9" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_limit_point_9" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_edu_min_check_9">
  								<label for="achieve_edu_min_check_9"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	서브인턴 교육 책임<br>
								(해당학년도 1년간)
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_10">1주 당 0.5점으로 계산하여 입력</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_10"  value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_edu_unit_10" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_weight_point_10" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_limit_point_10" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_edu_min_check_10">
  								<label for="achieve_edu_min_check_10"></label>
                            </td>
                        </tr>
                        <tr>
                        	<td>
                            	강의평가
                            </td>
                            <td class="border-l-text-left">
                            	평가점수
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_11">강의평가가 없을 경우 0점, 개별 교수의 평균값</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_11" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_edu_unit_11" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_weight_point_11" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_limit_point_11" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_edu_min_check_11">
  								<label for="achieve_edu_min_check_11"></label>
                            </td>
                        </tr>
                        <tr>
                        	<td rowspan="6">
                            	시험관
                            </td>
                            <td class="border-l-text-left">
                            	교내 CPX
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_12">회수(8시간 참여/1일)로 환산하여 입력</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_12" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select id="achieve_edu_unit_12" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_weight_point_12" value="2" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_limit_point_12" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_edu_min_check_12">
  								<label for="achieve_edu_min_check_12"></label>
                            </td>
                        </tr>
                       <tr>
                            <td class="border-l-text-left">
                            	교내 OSCE
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_13">회수(8시간 참여/1일)로 환산하여 입력</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_13" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select id="achieve_edu_unit_13" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_weight_point_13" value="2" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_limit_point_13" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox" id="achieve_edu_min_check_13">
  								<label for="achieve_edu_min_check_13"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	대경모의시험 CPX
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_14">회수(8시간 참여/1일)로 환산하여 입력</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_14" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_edu_unit_14" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_weight_point_14" value="2" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_limit_point_14" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_edu_min_check_14">
  								<label for="achieve_edu_min_check_14"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	국시실기시험
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_15">회수(8시간 참여/1일)로 환산하여 입력</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_15" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_edu_unit_15" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_weight_point_15" value="2" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_limit_point_15" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_edu_min_check_15">
  								<label for="achieve_edu_min_check_15"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	골학구두시험
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_16">회수(8시간 참여/1일)로 환산하여 입력</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_16" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_edu_unit_16" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_weight_point_16"  value="0.5" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_limit_point_16"  value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_edu_min_check_16">
  								<label for="achieve_edu_min_check_16"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	임상술기학
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_17">회수(8시간 참여/1일)로 환산하여 입력</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_17" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_edu_unit_17" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_weight_point_17" value="0.5" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_limit_point_17" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_edu_min_check_17">
  								<label for="achieve_edu_min_check_17"></label>
                            </td>
                        </tr>
                        <tr>
                       	 	<td rowspan="4">
                            	문항 및 교육모듈개발
                            </td>
                            <td class="border-l-text-left">
                            	모의고사, 학력평가, 문제 출제 문항 수
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_18">문항 수 입력</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_18" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_edu_unit_18" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_weight_point_18" value="0.05" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text" id="achieve_edu_limit_point_18" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox" id="achieve_edu_min_check_18">
  								<label for="achieve_edu_min_check_18"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	PBL 사례 신규 개발 
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_19"></textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_19" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_edu_unit_19" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" id="achieve_edu_weight_point_19" value="2" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text" id="achieve_edu_limit_point_19" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox" id="achieve_edu_min_check_19">
  								<label for="achieve_edu_min_check_19"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	의학교육학회 세미나/심포지움 참석 
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_20">2시간 미만은 0.5로 환산하여 입력</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_20" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_edu_unit_20" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_weight_point_20" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_limit_point_20" value="3.0" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_edu_min_check_20">
  								<label for="achieve_edu_min_check_20"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	자체의학교육세미나(동/하계)
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_edu_text_21">2시간 미만은 0.5로 환산하여 입력</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_point_21"  value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_edu_unit_21" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_weight_point_21" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_edu_limit_point_21" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_edu_min_check_21">
  								<label for="achieve_edu_min_check_21"></label>
                            </td>
                        </tr>
                    </tbody>
                </table>
               <!-- //.service-table -->
                <!-- //.service-table -->
            </div>
            <!-- //.search-div-edu -->
            
            <div class="search-select-div search-div-research">
                <!-- 연구 -->
                <table class="service-table">
                    <colgroup>
                        <col width="10%" />
                        <col width="20%" />
                        <col width="25%" />
                        <col width="20%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="5%" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <th colspan="2">
                                평가 항목(평가기간 이내)
                            </th>
                            <th>
                                평가기준
                            </th>
                            <th>
                                기준점수
                            </th>
                            <th>
                                가중치
                            </th>
                            <th>
                                상한선
                            </th>
                            <th>
                                최소기준
                            </th>
                        </tr>
                        <tr>
                            <td rowspan="5">
                                논문
                            </td>
                            <td class="border-l-text-left">
                                SCI급 논문<br>
								Impact factor(IF) >=1.0, x [(IF/2)+1] 
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_research_text_1">기준점수 30으로 계산하여 등록</textarea> 
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_point_1" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select id="achieve_research_unit_1" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_weight_point_1" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_limit_point_1" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_research_min_check_1">
  								<label for="achieve_research_min_check_1"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	SCI급 논문<br>
                                IF<1.0
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_research_text_2">기준점수 30으로 계산하여 등록</textarea> 
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_point_2" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select id="achieve_research_unit_2" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_weight_point_2" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_limit_point_2" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox" id="achieve_research_min_check_2">
  								<label for="achieve_research_min_check_2"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	한국연구재단 등재 및 등재 후보 학술지 자연 및 인문사회 계열
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_research_text_3">기준점수 15점으로 계산하여 등록</textarea> 
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_point_3" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_research_unit_3" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_weight_point_3" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_limit_point_3" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_research_min_check_3">
  								<label for="achieve_research_min_check_3"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	기타 국제 학술지 자연 및 인문 사회계열
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_research_text_4">기준점수 10점으로 계산하여 등록</textarea> 
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_point_4" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_research_unit_4" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_weight_point_4" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_limit_point_4" value="없음" />
                            </td>
                           <td class="checks">
                               	<input type="checkbox" id="achieve_research_min_check_4">
  								<label for="achieve_research_min_check_4"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	국내 전국 규모 학술지 자연 및 인문사회계열
                            </td>
                            <td>
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_research_text_5">기준점수 5점으로 계싼하여 등록</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_point_5" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select id="achieve_research_unit_5" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_weight_point_5" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_limit_point_5" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox" id="achieve_research_min_check_5">
  								<label for="achieve_research_min_check_5"></label>
                            </td>
                        </tr> 
                        <tr>
                        	<td rowspan="4">
                            	저술
                            </td>
                            <td class="border-l-text-left">
                            	저서,교재(초판)
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_research_text_6">기준점수 30점으로 계산하여 등록 
- 국내는 가중치 0.5, ISBN(+)만 인정</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_point_6" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_research_unit_6" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_weight_point_6" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_limit_point_6" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_research_min_check_6">
  								<label for="achieve_research_min_check_6"></label>
                            </td>
                        </tr>
                        <tr>
                        	<td class="border-l-text-left">
                            	저서,교재(개정판)
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_research_text_7">기준점수 10점으로 계산하여 등록
- 국내는 가중치 0.5, ISBN(+)만 인정
								</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_point_7" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select id="achieve_research_unit_7" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" id="achieve_research_weight_point_7" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text" id="achieve_research_limit_point_7" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox" id="achieve_research_min_check_7">
  								<label for="achieve_research_min_check_7"></label>
                            </td>
                        </tr>
                       <tr>
                            <td class="border-l-text-left">
                            	역서,편저 등(초판)
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_research_text_8">기준점수 10점으로 계산하여 등록</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_point_8" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select id="achieve_research_unit_8" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_weight_point_8" value="2" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_limit_point_8" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_research_min_check_8">
  								<label for="achieve_research_min_check_8"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	역서,편저 등(개정판)
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_research_text_9">기준점수 5점으로 계산하여 등록</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_point_9" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select id="achieve_research_unit_9" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"   id="achieve_research_weight_point_9" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text" id="achieve_research_limit_point_9"  value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_research_min_check_9" >
  								<label for="achieve_research_min_check_9"></label>
                            </td>
                        </tr>
                        <tr>
                        	<td rowspan="3">
                            	기타
                            </td>
                            <td class="border-l-text-left">
                            	등록된 국외특허 
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_research_text_10">기준점수 10으로 계산하여 등록</textarea> 
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_point_10" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select id="achieve_research_unit_10" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_weight_point_10" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_limit_point_10" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_research_min_check_10" >
  								<label for="achieve_research_min_check_10"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	등록된 국내특허
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_research_text_11">기준점수 5으로 계산하여 등록</textarea> 
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_point_11" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_research_unit_11" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_weight_point_11" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_limit_point_11" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_research_min_check_11">
  								<label for="achieve_research_min_check_11"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	등록된 소프트웨어
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_research_text_12">기준점수 5점으로 계산하여 등록</textarea> 
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_point_12" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_research_unit_12" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_weight_point_12" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_limit_point_12" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_research_min_check_12">
  								<label for="achieve_research_min_check_12"></label>
                            </td>
                        </tr>
                        <tr>
                       	 	<td>
                            	연구비
                            </td>
                            <td class="border-l-text-left">
                            	간접연구비
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_research_text_13">간접연구비 기준 100만원 미만은 1점, 그 이후 100만원 당 1점씩 가산
- 간접연구비가 의학전문 대학원 및 의과대학으로 배정된 것</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_point_13" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_research_unit_13" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_weight_point_13" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_research_limit_point_13" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_research_min_check_13">
  								<label for="achieve_research_min_check_13"></label>
                            </td>
                        </tr>
                    </tbody>
                 </table>
                <!-- //.service-table -->
            </div>
            <!-- //.search-div-research -->
            
            <!-- 봉사 -->
            <div class="search-select-div search-div-service">
                <table class="service-table">
                    <colgroup>
                        <col width="10%" />
                        <col width="10%" />
                        <col width="15%" />
                        <col width="25%" />
                        <col width="15%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="5%" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <th colspan="3">
                                평가 항목(평가기간 이내)
                            </th>
                            <th>
                                평가기준
                            </th>
                            <th>
                                기준점수
                            </th>
                            <th>
                                가중치
                            </th>
                            <th>
                                상한선
                            </th>
                            <th>
                                최소기준
                            </th>
                        </tr>
                        <tr>
                            <td rowspan="7">
                            	본부보직
                            </td>
                            <td class="border-l-text-left" colspan="2">
                                부총장, 처장, 부처장 등 책임시수 3시간에 준하는 보직
                            </td>
                            <td class="text-left">
                              	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_1">[보직교원 주당 교수시간 기준표]에 준함</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_point_1" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_servicein_unit_1" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_weight_point_1" value="10" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_limit_point_1" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_servicein_min_check_1">
  								<label for="achieve_servicein_min_check_1"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left" colspan="2">
                            	입학본부장, 국제교류원장, 입학본부부본부장, 국제교류원부원장, 인재개발원장, 인재개발부원장 등 책임시수 3시간에 준다흔 보직
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_2">[보직교원 주당 교수시간 기준표]에 준함</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_point_2" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_servicein_unit_2" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_weight_point_2" value="10" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_limit_point_2" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_servicein_min_check_2">
  								<label for="achieve_servicein_min_check_2"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left" colspan="2">
                            	신문방송사주간, 정보전산원장, 생활관장, 공동실기시험관장, 보건진료소장
                            </td>
                            <td class="text-left"><textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_3"></textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_point_3" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_servicein_unit_3" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_weight_point_3" value="2" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_limit_point_3" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_servicein_min_check_3">
  								<label for="achieve_servicein_min_check_3"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left" colspan="2">
                            	교수회 의장, 부의장, 처장, 부처장
                            </td>
                            <td class="text-left">
                            <textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_4"></textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_point_4" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)" />
                                <select  id="achieve_servicein_unit_4" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_weight_point_4" value="7" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_limit_point_4" value="없음" />
                            </td>
                           <td class="checks">
                               	<input type="checkbox"  id="achieve_servicein_min_check_4">
  								<label for="achieve_servicein_min_check_4"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left" colspan="2">
                            	기술사 담당(명의관 포함), 부설연구소장
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_5">기준점수 5점으로 계산하여 등록</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_point_5" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_servicein_unit_5" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_weight_point_5" value="2" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_limit_point_5" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_servicein_min_check_5" >
  								<label for="achieve_servicein_min_check_5"></label>
                            </td>
                        </tr> 
                        <tr>
                            <td class="border-l-text-left" colspan="2">
                            	특별학부장(자율전공, 글로절 인재) 
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_6"></textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_point_6"  value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_servicein_unit_6" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_weight_point_6" value="2" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_limit_point_6" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_servicein_min_check_6">
  								<label for="achieve_servicein_min_check_6"></label>
                            </td>
                        </tr> 
                        <tr>
                            <td class="border-l-text-left" colspan="2">
                            	발전기금담당 책임자
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_7"></textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text" id="achieve_servicein_point_7"  value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_servicein_unit_7" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_weight_point_7" value="3" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text" id="achieve_servicein_limit_point_7" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_servicein_min_check_7">
  								<label for="achieve_servicein_min_check_7"></label>
                            </td>
                        </tr> 
                        <tr>
                        	<td rowspan="6">
                            	교내비보직봉사<br>의전원, 의과대학외
                            </td>
                            <td class="border-l-text-left" rowspan="3">
                            	학생 지도
                            </td>
                            <td class="border-l-text-left">
                            	학생단체(동아리)지도교수
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_8"></textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_point_8" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_servicein_unit_8" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_weight_point_8" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_limit_point_8" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_servicein_min_check_8" >
  								<label for="achieve_servicein_min_check_8"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	학생상담자원봉사교수
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_9"></textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_point_9" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_servicein_unit_9" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_weight_point_9" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_limit_point_9" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox" id="achieve_servicein_min_check_9" >
  								<label for="achieve_servicein_min_check_9"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	학생관련 특수 프로그램 지도 교수
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_10"></textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_point_10" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_servicein_unit_10" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_weight_point_10" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_limit_point_10" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_servicein_min_check_10">
  								<label for="achieve_servicein_min_check_10"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left" rowspan="3">
                            	국제 협력 봉사
                            </td>
                           <td class="border-l-text-left">
                            	해외대학/연구소 등과의 학술협력 주도
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_11">국제 교류원장이 정하는 활동만을 대상으로 한다. 연간 최대 2점을 초과하지 않는다.</textarea> 
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_point_11" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_servicein_unit_11" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_weight_point_11" value="0.5" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_limit_point_11" value="2" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_servicein_min_check_11">
  								<label for="achieve_servicein_min_check_11"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	학생인솔 해외 프로그램 운영
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_12">국제 교류원장이 정하는 활동만을 대상으로 한다. 연간 최대 2점을 초과하지 않는다.</textarea> 
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_point_12" value="1" />
                                <select  id="achieve_servicein_unit_12" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_weight_point_12" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)" />
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_limit_point_12" value="2" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_servicein_min_check_12">
  								<label for="achieve_servicein_min_check_12"></label>
                            </td>
                        </tr>
                       	<tr>
                            <td class="border-l-text-left">
                            	기타 국제협력 활동
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_13">국제 교류원장이 정하는 활동만을 대상으로 한다. 연간 최대 2점을 초과하지 않는다.</textarea> 
                            </td>
                            <td class="score-input-td">
                                <input type="text" id="achieve_servicein_point_13"  value="1" />
                                <select  id="achieve_servicein_unit_13" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_weight_point_13" value="0.2" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"   id="achieve_servicein_limit_point_13" value="2" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_servicein_min_check_13">
  								<label for="achieve_servicein_min_check_13"></label>
                            </td>
                       	</tr>
                       	<tr>
                            <td rowspan="4">
                            	의전원 및 의과대학보직자
                            </td>
                            <td class="border-l-text-left" colspan="2">
                                원장, 병원장 등 책임시수 3시간에 준하는 보직 
                            </td>
                            <td class="text-left">
                              	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_14">[보직교원 주당 교수시간 기준표]에 준함</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text" id="achieve_servicein_point_14" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select id="achieve_servicein_unit_14" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" id="achieve_servicein_weight_point_14" value="20" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text" id="achieve_servicein_limit_point_14" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox" id="achieve_servicein_min_check_14">
  								<label for="achieve_servicein_min_check_14"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left" colspan="2">
                            	부원장 (교무, 학생, 연구, 교육) 및 부원장보
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_15">[보직교원 주당 교수시간 기준표]에 준함
- 부원장보는 가중치 0.5</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_point_15" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select id="achieve_servicein_unit_15" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_weight_point_15" value="6" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text" id="achieve_servicein_limit_point_15" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_servicein_min_check_15">
  								<label for="achieve_servicein_min_check_15"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left" colspan="2">
                            	실장(기획, 입학, 임상수기, 대외협력), 기타 (BK 사업단장 등), 교수회 의장
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_16">[보직교원 주당 교수시간 기준표]에 준함</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_point_16" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_servicein_unit_16" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_weight_point_16" value="12" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_limit_point_16" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_servicein_min_check_16">
  								<label for="achieve_servicein_min_check_16"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left" colspan="2">
                            	대학원 학과장(역학 및 건강증진학과장, 보건관리학과장, 보건학과장, 법정의학과장, 과학수사학과장, 법의학간호학과장, 의학과장, 의과학과장, 의용생체공학과장, 의료정보학과장), 교수회 부의장과 간사, 교실주임, BK 사업단부단장/팀장, 학년 담임 등
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_17">다수인 경우에는 인원수로 나눔</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_point_17" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_servicein_unit_17" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_weight_point_17" value="6" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_limit_point_17" value="없음" />
                            </td>
                           <td class="checks">
                               	<input type="checkbox"  id="achieve_servicein_min_check_17">
  								<label for="achieve_servicein_min_check_17"></label>
                            </td>
                        </tr>
                       <tr>
                            <td rowspan="4">
                            	의전원 및 의과대학비보직자
                            </td>
                            <td class="border-l-text-left" colspan="2">
                                의전원장이 인정한 교내 봉사(위원장/간사) 
                            </td>
                            <td class="text-left">
                              	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_18">위원장/간사는 가중치 2</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_point_18" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select id="achieve_servicein_unit_18" >
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_weight_point_18" value="4" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_limit_point_18" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox" id="achieve_servicein_min_check_18">
  								<label for="achieve_servicein_min_check_18"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left" colspan="2">
                            	의전원장이 인정한 교내 봉사(위원장/간사 외)<br>
								- 회의개최 의전원 위원회 위원에 대한 평점 
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_19"></textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_point_19" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_servicein_unit_19" >
  
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_weight_point_19" value="4" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_limit_point_19" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_servicein_min_check_19">
  								<label for="achieve_servicein_min_check_19"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left" colspan="2">
                            	의학과장 및 의예과장이 인정한 교내 봉사<br>
								- 교내 활동 참여(입학시험 면접, 출제 등), 학생 지도 활동(학생체육행사, 졸업여행 인솔, MMPI 검사, 신입생 OT/새터 등), 학교 행사 참여
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_servicein_text_20">비보직자만 해당, 2시간 미만은 가중치 0.5</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_point_20" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_servicein_unit_20" >
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_weight_point_20" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_servicein_limit_point_20" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_servicein_min_check_20">
  								<label for="achieve_servicein_min_check_20"></label>
                            </td>

                        </tr>
                    </tbody>
                </table>
                <!-- //.co-tabs-table -->
            </div>
            <!-- //.search-div-service -->
            
            <!-- 봉사(교외) -->
            <div class="search-select-div search-div-servicest">
                <table class="service-table">
                    <colgroup>
                        <col width="10%" />
                        <col width="20%" />
                        <col width="25%" />
                        <col width="20%" />
                        <col width="10%" />
                        <col width="10%" />
                        <col width="5%" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <th colspan="2">
                                평가 항목(평가기간 이내)
                            </th>
                            <th>
                                평가기준
                            </th>
                            <th>
                                기준점수
                            </th>
                            <th>
                                가중치
                            </th>
                            <th>
                                상한선
                            </th>
                            <th>
                                최소기준
                            </th>
                        </tr>
                        <tr>
                            <td rowspan="7">
                            	학술봉사
                            </td>
                            <td class="border-l-text-left">
                                대한의학회에 등록된 학회 이사장/학회장<br>
                                국내외 SCI, SCIE, SSCI, AHCI 학술지의 이사장
                            </td>
                            <td class="text-left">
                              	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_serviceout_text_1">6점/1년</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_point_1"  value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_serviceout_unit_1" >
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_weight_point_1" value="6" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_limit_point_1" value="18" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_serviceout_min_check_1">
  								<label for="achieve_serviceout_min_check_1"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                                초세부 학회(대한의학회 명시된 학회), 기타학회장
                            </td>
                            <td class="text-left">
                              	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_serviceout_text_2">6점/1년, 가중치 0.5</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_point_2" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_serviceout_unit_2" >
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_weight_point_2" value="3" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_limit_point_2" value="9" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox" id="achieve_serviceout_min_check_2">
  								<label for="achieve_serviceout_min_check_2"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                                국내외 SCI, SCIE, SSCI, AHCI 학술지의 편집 (부)위원장/위원
                            </td>
                            <td class="text-left">
                              	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_serviceout_text_3">6점/1년</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_point_3" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_serviceout_unit_3" >
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_weight_point_3" value="6" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_limit_point_3" value="18" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_serviceout_min_check_3">
  								<label for="achieve_serviceout_min_check_3"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                                한국연구재단 등재학술지의 학술지 편집위원장/위원
                            </td>
                            <td class="text-left">
                              	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_serviceout_text_4">3점/1년</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_point_4" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_serviceout_unit_4" >
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_weight_point_4" value="3" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_limit_point_4" value="9" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox" id="achieve_serviceout_min_check_4" >
  								<label for="achieve_serviceout_min_check_4"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                                국제학술대회 조직위원장/이사(국내 개최)
                            </td>
                            <td class="text-left">
                              	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_serviceout_text_5">6점/1년</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_point_5" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_serviceout_unit_5" >
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_weight_point_5" value="6" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_limit_point_5" value="18" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_serviceout_min_check_5">
  								<label for="achieve_serviceout_min_check_5"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                                국제학술대회조직위원장/이사(국외개최)
                            </td>
                            <td class="text-left">
                              	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_serviceout_text_6">6점/1년, 가중치 0.5</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text" id="achieve_serviceout_point_6"  value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_serviceout_unit_6" >
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_weight_point_6" value="3" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_limit_point_6" value="9" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox" id="achieve_serviceout_min_check_6" >
  								<label for="achieve_serviceout_min_check_6"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                                연구평가위원(한국연구재안, 보건산업진흥원, 학력평가, MEET, 의학관련, 고시위원)
                            </td>
                            <td class="text-left">
                              	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_serviceout_text_7">1점/1건</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_point_7" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_serviceout_unit_7" >
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_weight_point_7" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_limit_point_7" value="-" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_serviceout_min_check_7">
  								<label for="achieve_serviceout_min_check_7"></label>
                            </td>
                        </tr>
                        <tr>
                        	<td>
                            	의료봉사
                            </td>
                            <td class="border-l-text-left">
                            	국내외 의료봉사
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_serviceout_text_81">1일 이상일 경우 인정</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_point_8" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_serviceout_unit_8" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_weight_point_8"  value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_limit_point_8" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_serviceout_min_check_8">
  								<label for="achieve_serviceout_min_check_8"></label>
                            </td>
                        </tr>
                        <tr>
                        	<td rowspan="6">
                            	기타
                            </td>
                            <td class="border-l-text-left">
                            	전공 관련 국제 기구 위원
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_serviceout_text_9">1점/월</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_point_9" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_serviceout_unit_9" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_weight_point_9" value="6" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text" id="achieve_serviceout_limit_point_9" value="18" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_serviceout_min_check_9">
  								<label for="achieve_serviceout_min_check_9"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	정부기관 및 이에 준하는 각종 위원 
                            </td>
                            <td class="text-left">
                            	<textarea style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_serviceout_text_10">3점/1년</textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_point_10" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select id="achieve_serviceout_unit_10" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_weight_point_10" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_limit_point_10" value="3" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_serviceout_min_check_10">
  								<label for="achieve_serviceout_min_check_10"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	지자체 및 이에 준하는 각종 위원 
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_serviceout_text_11"></textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text" id="achieve_serviceout_point_11"  value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_serviceout_unit_11" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_weight_point_11" value="0.4" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_limit_point_11" value="1.2" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_serviceout_min_check_11">
  								<label for="achieve_serviceout_min_check_11"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	국영기업체 및 이에 준하는 각종 위원 
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_serviceout_text_12"></textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_point_12" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_serviceout_unit_12" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_weight_point_12" value="0.2" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_limit_point_12" value="0.6" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_serviceout_min_check_12">
  								<label for="achieve_serviceout_min_check_12"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	특별강연 (정부기관, 지자체, 국영기업체 및 이에 준하는 기관이 주관) 
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_serviceout_text_13"></textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_point_13" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_serviceout_unit_13" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_weight_point_13" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_limit_point_13" value="3" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_serviceout_min_check_13" >
  								<label for="achieve_serviceout_min_check_13"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	동창회 및 이사회 (부)회장, 상임이사
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_serviceout_text_14"></textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_point_14" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_serviceout_unit_14" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_weight_point_14" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_limit_point_14"  value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_serviceout_min_check_14">
  								<label for="achieve_serviceout_min_check_14"></label>
                            </td>
                        </tr>
                        <tr>
                        	<td rowspan="4">
                            	수상실적
                            </td>
                            <td class="border-l-text-left">
                            	대통령상
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_serviceout_text_15"></textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_point_15" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_serviceout_unit_15" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_weight_point_15"  value="8" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_limit_point_15" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_serviceout_min_check_15">
  								<label for="achieve_serviceout_min_check_15"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	정부 수훈
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_serviceout_text_16"></textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_point_16" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_serviceout_unit_16" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_weight_point_16" value="4" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_limit_point_16" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_serviceout_min_check_16">
  								<label for="achieve_serviceout_min_check_16"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	국제단체, 국제학회, 정부 수상
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_serviceout_text_17"></textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_point_17" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select  id="achieve_serviceout_unit_17" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_weight_point_17" value="4" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_limit_point_17" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox"  id="achieve_serviceout_min_check_17">
  								<label for="achieve_serviceout_min_check_17"></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l-text-left">
                            	국내학회, 자치단체, 국영기업체 및 이에 준하는 기관이 수요한 상 
                            </td>
                            <td class="text-left">
                            <textarea onblur="adjustTextareaSize()" style="resize:none;wrap:hard;text-align:left;overflow:hidden;" id="achieve_serviceout_text_18"></textarea>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_point_18" value="1" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,0)"/>
                                <select id="achieve_serviceout_unit_18" >
                                    <option value="건">1건(회)</option>
                                    <option value="년">년</option>
                                    <option value="월">월</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_weight_point_18" value="2" onkeypress="numberKey(this)" onkeyup="hangulKey(this)" onblur="chkFixed(this,2)"/>
                            </td>
                            <td class="score-input-td">
                                <input type="text"  id="achieve_serviceout_limit_point_18" value="없음" />
                            </td>
                            <td class="checks">
                               	<input type="checkbox" id="achieve_serviceout_min_check_18" >
  								<label for="achieve_serviceout_min_check_18"></label>
                            </td>
                        </tr>
                    </tbody>
                </table>
               <!-- //.service-table -->
            
        </div>
        <!-- //.list-table-contents -->
        <!-- 버튼 영역 -->
        <div class="table-bottom-btns-div" style="margin-bottom: 20px;">
            <a class="btn-cons" onclick="addGradeScoreInfo()">저장</a>
            <a class="btn-cancel-cons" onclick="getGradeScoreInfo()">취소</a>
        </div>
    </div>
    <!-- //.container -->

    <footer id="footer">
        <div class="footer_con">
            <div class="footer_address">
                <p>
                    경북대학교 의과대학 · 의과전문대학원 ADD. (41944)대구광역시 중구 국채보상로 680
                </p>
                <p>
                    TEL. 053-420-4910(교무), 4906(서무) FAX. 053-422-9195(교무), 420-4925(서무) E-mail. meddean@knu.ac.kr
                </p>
                <p>
                    Copyright(c)2014 경북대학교 의과대학·의과전문대학원 All rights reserved.
                </p>
            </div>
        </div>
    </footer>
    <!-- //.footer -->
</div>
<!-- //#wrap -->

<!-- 이용자 삭제 팝업 -->
<div class="wrap-popup-nonebg wrap-del-popup hidden">
    <div class="popup-guide">
        <div class="popup-guide-header">
            <h2>
                이용자 삭제
            </h2>
            <a class="btn-close-del-popup" title="close">
                X
            </a>
        </div>
        <div class="popup-guide-content">
            <span class="popname"></span>
            <span class="popposition"></span>님을 삭제 하시겠습니까?
            <div class="popup-btns-div">
                <button type="button">확인</button>
                <button type="button" class="btn-close-del-popup">취소</button>
            </div>
        </div>
    </div>
</div>
<!-- //.wrap-popup -->

<script>
$(document).ready(function(){
    // 이름 검색창 체크
    $(".btn-search-name").bind("click", function(){
        var input = $(this).siblings("input[type='text']");
        if ( input.val() == 0 ){
            alert("검색할 이름을 입력해주세요.");
            input.focus();
        }
    });

    // 테이블 라인 배경
    $(".service-table tr:even").css("background","#fdfdfd");

    // 이용자 삭제 팝업
    $(".btn-del").bind("click", function(){
        var tableName = $(this).parent().siblings().children(".tablename").text();
        var tablePosition = $(this).parent().siblings().children(".tableposition").text();
        $(".wrap-del-popup").fadeIn(200);
        $(".popup-guide-content span.popname").text("'" + tableName + "'");
        $(".popup-guide-content span.popposition").text(tablePosition);
    });
    $(".btn-close-del-popup").bind("click", function(){
        $(".wrap-del-popup").fadeOut(200);
    });
    // 교육 , 연구 , 봉사 기준점수 입력표 노출
    $(".search-select-div:nth-of-type(2),.search-select-div:nth-of-type(3)").hide();
    /*
    $(".contents-search-div").on("change","select.search-div-select",function(){
        if ( $(this).children("option:selected").val() == 0 ){
            $(".btn-search-name").bind("click",function(){
                $(".search-select-div").hide();
                $(".search-div-edu").show();
            });
        }
        if ( $(this).children("option:selected").val() == 1 ){
            $(".btn-search-name").bind("click",function(){
                $(".search-select-div").hide();
                $(".search-div-research").show();
            });
        }
        if ( $(this).children("option:selected").val() == 2 ){
            $(".btn-search-name").bind("click",function(){
                $(".search-select-div").hide();
                $(".search-div-service").show();
            });
        }
    });
    */
    $(".service-nav a").removeClass("active");
    $(".service-nav a").eq(0).addClass("active");
    
    
 // 교육 , 연구 , 봉사 기준점수 입력표 노출
    $(".search-select-div:nth-of-type(2),.search-select-div:nth-of-type(3),.search-select-div:nth-of-type(4)").hide();
    $(".contents-search-div").on("change","select.search-div-select",function(){
        if ( $(this).children("option:selected").val() == 'e' ){
            $(".btn-search-name").bind("click",function(){
                $(".search-select-div").hide();
                $(".search-div-edu").show();
                adjustTextareaSize();
            });
        }
        if ( $(this).children("option:selected").val() == 'r' ){
            $(".btn-search-name").bind("click",function(){
                $(".search-select-div").hide();
                $(".search-div-research").show();
                adjustTextareaSize();
            });
        }
        if ( $(this).children("option:selected").val() == 'si' ){
            $(".btn-search-name").bind("click",function(){
                $(".search-select-div").hide();
                $(".search-div-service").show();
                adjustTextareaSize();
            });
        }
		if ( $(this).children("option:selected").val() == 'so' ){
            $(".btn-search-name").bind("click",function(){
                $(".search-select-div").hide();
                $(".search-div-servicest").show();
                adjustTextareaSize();
            });
        }
    });
    getGradeScoreInfo("e");
    
    $("input[id^=achieve_]").each(function(e){
    	var id =$(this).attr("id") 
    	if(id.indexOf("_limit_point") > -1){
    		//console.debug($(this).attr("id"));
    		var id = $(this).attr("id");
    		$(this).bind("click", function(){
    			$(this).val("");
    	    });
    		$(this).bind("blur", function(){
    			if($(this).val() == ""){
    				$(this).val("없음");	
    			}
    	    });	
    	}
    });
    
    adjustTextareaSize(); 
});
</script>
<script>
function check_key() {
	 var char_ASCII = event.keyCode;
	 //console.debug(char_ASCII);          	     
	  //숫자
	 if ((char_ASCII >= 48 && char_ASCII <= 57 ) || char_ASCII == 46)
	   return 1;
	 //영어
	 else if ((char_ASCII>=65 && char_ASCII<=90) || (char_ASCII>=97 && char_ASCII<=122))
	    return 2;
	 //특수기호
	 else if ((char_ASCII>=33 && char_ASCII<=47) || (char_ASCII>=58 && char_ASCII<=64) 
	   || (char_ASCII>=91 && char_ASCII<=96) || (char_ASCII>=123 && char_ASCII<=126))
	    return 4;
	 //한글
	 else if ((char_ASCII >= 12592) || (char_ASCII <= 12687))
	    return 3;
	 else 
	    return 0;
}
function numberKey(elem,fixed) {
	 var value = $(elem).val();
	 console.debug(value);
	 
	 if($.trim(value)  == ""){
		 //return;
	 }
	
     console.debug("check_key()  =>"+ check_key() );
	 if(check_key() == 1) {
	 }else{
		 event.returnValue = false;   
	  	 alert("숫자만 입력할 수 있습니다.");
	  	 $(elem).focus();
	  	 return;
	 }
}
function hangulKey(elem,fixed){
	console.debug("check_key()  =>"+ check_key() );
	var value = $(elem).val();
	console.debug(value);
	var check = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/;
	if(check.test(value)){
		alert("숫자만 입력할 수 있습니다.");
		$(elem).focus();
		return;
	}
}
function chkFixed(elem,fixed){
	var value = $.trim($(elem).val());
	if(value == ""){
		$(elem).val("0");
	}
	if($(elem).val().indexOf(".") > -1){
		var num = parseFloat($(elem).val());		
		$(elem).val(num.toFixed(2));	
	}
}
function adjustTextareaSize(){
	$("textarea").each(function(){
	    $(this).css('height', 'auto' );
	    $(this).height( this.scrollHeight );
	});
	$("textarea").css("border","solid 1px #ccc");
	$(".score-input-td input").css("border","solid 1px #ccc");
}
var yearInfo = "${yearInfo}";
var userLevel = "${sessionScope.userlevel}";
var requestUserCode = "${requestUserCode}";
var userName = "${userName}";

function getGradeScoreInfo(type){
	var achieveType = $(".search-div-select").val();
	if(type != undefined || type != null){
		achieveType = type;	
	}
	
	$(".search-select-div").hide();
	if(achieveType == "e"){
		$(".search-div-edu").show();
	} else if(achieveType == "r"){
		$(".search-div-research").show();
	} else if(achieveType == "s"){
		$(".search-div-service").show();
	}
	var searchYear = $("#searchYear").val();
	var param = {};
	param.achieveType = achieveType;
	param.searchYear = searchYear;
	$.ajax({
        type: "POST",
        url: "./getGradeScoreInfo",
        data: param,
        dataType: "json",
        success: function(data, status) {
        	console.debug(data);
        	if (data.status == "success") {
		        setGradeSscoreInfo(data.result, achieveType);
        	}else{
        		
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
			console.error(error);
        }
    });
	
}
function setGradeSscoreInfo(result, achieveType){
	if(achieveType == "e"){
		for(var i=0; i < result.length; i++){
			var data = result[i];
					
			var text = $("#achieve_edu_text_"+ data.education_item_num).val(data.comment);
			var point = $("#achieve_edu_point_"+ data.education_item_num).val(data.point);
			var unit = $("#achieve_edu_unit_"+data.education_item_num).val(data.unit);
			var weight = $("#achieve_edu_weight_point_"+data.education_item_num).val(data.weight_point);
			var limit = $("#achieve_edu_limit_point_"+data.education_item_num).val(data.limit_point);
			if(data.min_check == "1"){
				$("#achieve_edu_min_check_"+data.education_item_num).attr("checked", true);
			}else{
				$("#achieve_edu_min_check_"+data.education_item_num).attr("checked", false);
			}
		}		
	}else if(achieveType == "r"){
		for(var i=0; i < result.length; i++){
			var data = result[i];
			var text = $("#achieve_research_text_"+ data.research_item_num).val(data.comment);
			var point = $("#achieve_research_point_"+ data.research_item_num).val(data.point);
			var unit = $("#achieve_research_unit_"+data.research_item_num).val(data.unit);
			var weight = $("#achieve_research_weight_point_"+data.research_item_num).val(data.weight_point);
			var limit = $("#achieve_research_limit_point_"+data.research_item_num).val(data.limit_point);
			if(data.min_check == "1"){
				$("#achieve_research_min_check_"+data.research_item_num).attr("checked", true);
			}else{
				$("#achieve_research_min_check_"+data.research_item_num).attr("checked", false);
			}
		}		
	}else if(achieveType == "si"){
		for(var i=0; i < result.length; i++){
			var data = result[i];
			console.debug(data);
			var text = $("#achieve_servicein_text_"+ data.service_item_num).val(data.comment);
			var point = $("#achieve_servicein_point_"+ data.service_item_num).val(data.point);
			var unit = $("#achieve_servicein_unit_"+data.service_item_num).val(data.unit);
			var weight = $("#achieve_servicein_weight_point_"+data.service_item_num).val(data.weight_point);
			var limit = $("#achieve_servicein_limit_point_"+data.service_item_num).val(data.limit_point);
			if(data.min_check == "1"){
				$("#achieve_servicein_min_check_"+data.service_item_num).attr("checked", true);
			}else{
				$("#achieve_servicein_min_check_"+data.service_item_num).attr("checked", false);
			}
		}		
	}else if(achieveType == "so"){
		for(var i=0; i < result.length; i++){
			var data = result[i];
			var text = $("#achieve_serviceout_text_"+ data.service_item_num).val(data.comment);
			var point = $("#achieve_serviceout_point_"+ data.service_item_num).val(data.point);
			var unit = $("#achieve_serviceout_unit_"+data.service_item_num).val(data.unit);
			var weight = $("#achieve_serviceout_weight_point_"+data.service_item_num).val(data.weight_point);
			var limit = $("#achieve_serviceout_limit_point_"+data.service_item_num).val(data.limit_point);
			if(data.min_check == "1"){
				$("#achieve_serviceout_min_check_"+data.service_item_num).attr("checked", true);
			}else{
				$("#achieve_serviceout_min_check_"+data.service_item_num).attr("checked", false);
			}
		}		
	}
}
function addGradeScoreInfo(){
	console.debug("addGradeScoreInfo ");
	var achieveType = $("#achieveType").val();
	var searchYear = $("#searchYear").val();
	console.debug(achieveType + " / " + searchYear);
	var paramList = {};
	if(achieveType =="e"){
		for(var i=1; i <= 21; i++){
			var text = $.trim($("#achieve_edu_text_"+ i).val());
			var point = $("#achieve_edu_point_"+ i).val();
			var unit = $("#achieve_edu_unit_"+i).val();
			var weight = $("#achieve_edu_weight_point_"+i).val();
			var limit = $("#achieve_edu_limit_point_"+i).val();
			var min = $("#achieve_edu_min_check_"+i).is(":checked") == true ? 1 : 0;
			
			if($.trim(point) == "" || point == null){
				alert("기준점수를 확인하세요");
				$("#achieve_edu_point_"+ i).focus();
			}
			if($.trim(unit) == "" || unit == null){
				alert("기준점수를 확인하세요");
				$("#achieve_edu_unit_"+i).focus();
				return;
			}
			if($.trim(weight) == "" || weight == null){
				alert("가중치를 확인하세요");
				$("#achieve_edu_weight_point_"+i).focus();
				return;
			}
			if($.trim(limit) == "" || limit == null){
				alert("상한선을 확인하세요");
				$("#achieve_edu_limit_point_"+i).focus();
				return;
			}
			
			
			var category_id = "";
			if(i >=1 && i <= 10){
				category_id = "ed01";
			}else if( i == 11){
				category_id = "ed02";
			}else if( i >= 12 && i <= 17){
				category_id = "ed03";
			}else if(i >= 18 && i <= 21){
				category_id = "ed04";
			}
			console.debug("["+i+"] =>"+ text + " / "+ point + " / "+  unit + " / "+  weight + " / "+  limit + " / "+  min + " / " + category_id);
			
			var param = {
					"text" : text
					,"point": point
					,"unit": unit
					,"weight" : weight
					,"limit" : limit
					,"min" : min
					,"category_id" : category_id
			}
			paramList[i] = param;
		}
	}else if(achieveType =="r"){
		for(var i=1; i <= 13; i++){
			var text = $.trim($("#achieve_research_text_"+ i).val());
			var point = $("#achieve_research_point_"+ i).val();
			var unit = $("#achieve_research_unit_"+i).val();
			var weight = $("#achieve_research_weight_point_"+i).val();
			var limit = $("#achieve_research_limit_point_"+i).val();
			var min = $("#achieve_research_min_check_"+i).is(":checked") == true ? 1 : 0;
			
			if($.trim(point) == "" || point == null){
				alert("기준점수를 확인하세요");
				$("#achieve_research_point_"+ i).focus();
			}
			if($.trim(unit) == "" || unit == null){
				alert("기준점수를 확인하세요");
				$("#achieve_research_unit_"+i).focus();
				return;
			}
			if($.trim(weight) == "" || weight == null){
				alert("가중치를 확인하세요");
				$("#achieve_research_weight_point_"+i).focus();
				return;
			}
			if($.trim(limit) == "" || limit == null){
				alert("상한선을 확인하세요");
				$("#achieve_research_limit_point_"+i).focus();
				return;
			}
			var category_id = "";
			if(i >=1 && i <= 5){
				category_id = "re01";
			}else if( i >=6 && i <= 9){
				category_id = "re02";
			}else if( i >= 10 && i <= 12){
				category_id = "re03";
			}else if(i == 13){
				category_id = "re04";
			}
			console.debug("["+i+"] =>"+ text + " / "+ point + " / "+  unit + " / "+  weight + " / "+  limit + " / "+  min + " / " + category_id);

			var param = {
					"text" : text
					,"point": point
					,"unit": unit
					,"weight" : weight
					,"limit" : limit
					,"min" : min
					,"category_id":category_id
			}
			paramList[i] = param;
		}
	}else if(achieveType =="si"){
		for(var i=1; i <= 20; i++){
			var text = $.trim($("#achieve_servicein_text_"+ i).val());
			var point = $("#achieve_servicein_point_"+ i).val();
			var unit = $("#achieve_servicein_unit_"+i).val();
			var weight = $("#achieve_servicein_weight_point_"+i).val();
			var limit = $("#achieve_servicein_limit_point_"+i).val();
			var min = $("#achieve_servicein_min_check_"+i).is(":checked") == true ? 1 : 0;
			if($.trim(point) == "" || point == null){
				alert("기준점수를 확인하세요");
				$("#achieve_servicein_point_"+ i).focus();
			}
			if($.trim(unit) == "" || unit == null){
				alert("기준점수를 확인하세요");
				$("#achieve_servicein_unit_"+i).focus();
				return;
			}
			if($.trim(weight) == "" || weight == null){
				alert("가중치를 확인하세요");
				$("#achieve_servicein_weight_point_"+i).focus();
				return;
			}
			if($.trim(limit) == "" || limit == null){
				alert("상한선을 확인하세요");
				$("#achieve_servicein_limit_point_"+i).focus();
				return;
			}
			
			var category_id = "";
			if(i >=1 && i <= 7){
				category_id = "si01";
			}else if( i >=8 && i <= 13){
				category_id = "si02";
			}else if( i >= 14 && i <= 17){
				category_id = "si03";
			}else if(i >= 18 && i <= 20){
				category_id = "si04";
			}
			console.debug("["+i+"] =>"+ text + " / "+ point + " / "+  unit + " / "+  weight + " / "+  limit + " / "+  min + " / " + category_id);

			var param = {
					"text" : text
					,"point": point
					,"unit": unit
					,"weight" : weight
					,"limit" : limit
					,"min" : min
					,"service_type" : "in"
					,"category_id":category_id
			}
			paramList[i] = param;
		}
	}else if(achieveType =="so"){
		for(var i=1; i <= 18; i++){
			var text = $.trim($("#achieve_serviceout_text_"+ i).val());
			var point = $("#achieve_serviceout_point_"+ i).val();
			var unit = $("#achieve_serviceout_unit_"+i).val();
			var weight = $("#achieve_serviceout_weight_point_"+i).val();
			var limit = $("#achieve_serviceout_limit_point_"+i).val();
			var min = $("#achieve_serviceout_min_check_"+i).is(":checked") == true ? "1" : "0";
			
			if($.trim(point) == "" || point == null){
				alert("기준점수를 확인하세요");
				$("#achieve_serviceout_point_"+ i).focus();
			}
			if($.trim(unit) == "" || unit == null){
				alert("기준점수를 확인하세요");
				$("#achieve_serviceout_unit_"+i).focus();
				return;
			}
			if($.trim(weight) == "" || weight == null){
				alert("가중치를 확인하세요");
				$("#achieve_serviceout_weight_point_"+i).focus();
				return;
			}
			if($.trim(limit) == "" || limit == null){
				alert("상한선을 확인하세요");
				$("#achieve_serviceout_limit_point_"+i).focus();
				return;
			}
			
			var category_id = "";
			if(i >=1 && i <= 7){
				category_id = "so01";
			}else if( i ==8){
				category_id = "so02";
			}else if( i >= 9 && i <= 14){
				category_id = "so03";
			}else if(i >= 15 && i <= 18){
				category_id = "so04";
			}
			console.debug("["+i+"] =>"+ text + " / "+ point + " / "+  unit + " / "+  weight + " / "+  limit + " / "+  min + " / " + category_id);

			var param = {
					"text" : text
					,"point": point
					,"unit": unit
					,"weight" : weight
					,"limit" : limit
					,"min" : min
					,"service_type" : "out"
					,"category_id":category_id
			}
			paramList[i] = param;
		}
	}
	
	console.debug(paramList);
	var paramObject ={};
	paramObject.achieveType = achieveType;
	paramObject.searchYear = searchYear;
	paramObject.paramList = paramList;
	
	console.debug("============================JSON.stringify(paramObject) ==============================");
	console.debug(JSON.stringify(paramObject));
	$.ajax({
        type: "POST",
        url: "./updateGradeScoreInfo",
        contentType : 'application/json; charset=UTF-8',
        data: JSON.stringify(paramObject),
        dataType: "json",
        success: function(data, status) {
        	console.debug(data);
        	if(data.status == "success"){
        		alert("저장되었습니다");
        	}else{
        		alert("오류가 발생했습니다");
        	}
        },
        error: function(xhr, textStatus) {
			//alert("오류가 발생했습니다.");
			alert(error);
        }
    });
}
</script>
</body>
</html>