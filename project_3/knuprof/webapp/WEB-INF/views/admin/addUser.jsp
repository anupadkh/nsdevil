<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../include/admin_header.jsp" %>

<div id="wrap">
     <%@ include file="../include/top.jsp" %>

    <div class="container">
		<hr/>
        <!-- 탭 메뉴 영역 -->
        <div class="tabmenu-div">
            <ul class="tabmenu-ul">
                <!-- 선택된 탭메뉴 표시 클래스 "active" -->
                <li class="active">
                    <a href="<c:url value="/admin/"/>">이용자 관리</a>
                </li>
                <li>
                    <a href="javascript:void(0)">기준 점수</a>
                </li>
                <li>
                    <a href="javascript:void(0)">기간 설정</a>
                </li>
            </ul>
        </div>
        <!-- //.tabmenu-div -->

        <!-- 이용자 등록 / 수정 영역 -->
        <div class="signup-div">
            <fieldset>
                <legend>이용자 등록을 위한 입력필드</legend>
                <span class="color-red">* 표시는 필수 입력사항 입니다.</span>
                <div>
                    <label class="required">구분</label>
                    <p>
                        <button type="button" class="btn-signup-class btn-pro-reg on">교수님</button>
                        <button type="button" class="btn-signup-class btn-co-reg">행정팀</button>
                    </p>
                </div>
                <div>
                    <label class="required">성명</label>
                    <p>
                        <input type="text" class="name" id="user_name" value="" />
                    </p>
                </div>
                <div class="co-hidden">
                    <label class="required">직급</label>
                    <p>
                        <input type="text" class="level" id="job_title" value="" />
                    </p>
                </div>
                <div class="co-hidden">
                    <label class="required">구분</label>
                    <p class="signup-pro-select">
                        <select id="addition">
                            <option value="1">겸직1</option>
                            <option value="2">겸직2</option>
                            <option value="3">겸직3</option>
                            <option value="4">겸직4</option>
                            <option value="5">겸직5</option>
                            <option value="6">겸직6</option>
                            <option value="7">기금1</option>
                            <option value="8">기금2</option>
                        </select>
                    </p>
                    <p class="">
                        <label class="">
                            표준점수 평가 대상 여부 :
                            <!-- <input type="checkbox" id="st_point"/> -->
                            <select id="position">
                            <option value="1">대상(정년 교수)</option>
                            <option value="2">대상(비정년 교수)</option>
                            <option value="3">대상(신임 교수)</option>
                            <option value="4" selected="selected">비대상</option>
                        </select>
                        </label>
                    </p>
                </div>
                <div class="co-hidden">
                    <label class="required">교실명</label>
                    <p>
                        <input type="text" class="classm" id="subject" value="" />
                    </p>
                </div>
                <div class="co-hidden">
                    <label>교직원 번호</label>
                    <p>
                        <input type="text" id="prof_num" value="" />
                        &nbsp;
                        * 교직원 번호가 없는 분은 연봉제 입니다.
                    </p>
                </div>
                <div>
                    <label class="required">아이디</label>
                    <p>
                        <input type="text" class="id" id="user_id" value="" />
                    </p>
                </div>
                <div class="co-view">
                    <label class="required">담당영역</label>
                    <div class="co-view-div">
                        <select class="co-view-select1" onchange='changeListener(this)'>
                            <option value="ed">교육</option>
                            <option value="re">연구</option>
                            <option value="si">교내봉사</option>
                            <option value="so">교외봉사</option>
                        </select>
                        <select class="co-view-select2">
                            <option value="ed01">강의시간</option>
                            <option value="ed02">강의평가</option>
                            <option value="ed03">시험관</option>
                            <option value="ed04">문항 및 교육 모듈 개발</option>
                        </select>
                        <button type="button" class="btn-add-del btn-add-co-select" title="추가">+</button>
                        <div class="append-div">

                        </div>
                    </div>
                </div>
                <div>
                    <label class="required">비밀번호</label>
                    <p>
                        <input type="password" class="pw" id="user_pass" value="" />
                    </p>
                </div>
                <div>
                    <label class="required">비밀번호 확인</label>
                    <p>
                        <input type="password" class="rpw" id="confirm_pass" value="" />
                    </p>
                </div>
                <div class="co-hidden">
                    <label class="required">연락처</label>
                    <p class="tel-p">
                        <input type="text" value="" maxlength="3" />
                        <input type="text" value="" maxlength="4" />
                        <input type="text" value="" maxlength="4" />
                    </p>
                </div>
                <div class="co-hidden">
                    <label>E-mail</label>
                    <p>
                        <input type="text" id="user_email" value="" />
                    </p>
                </div>
            </fieldset>
        </div>
        <!-- //.signup-div -->

        <div class="signup-btn-div">
            <a class="btn-signup btn-check-pro">등록</a>
            <!--<a class="btn-signup btn-check-pro">수정</a>-->
            <a href="javascript:history.back(-1)" class="btn-cancel">취소</a>
        </div>
        <!-- //.signup-btn-div -->
    </div>
    <!-- //.container -->

   <%@ include file="../include/bottom.jsp" %>
</div>
<!-- //#wrap -->

<script>
$(document).ready(function(){
    // 구분 탭 메뉴 스타일
    $(".signup-div .btn-signup-class").bind("click", function(){
        $(".signup-div button").removeClass("on");
        $(this).addClass("on");
    });
    // 교수님 입력폼 보기
    $(".signup-div").on("click",".btn-pro-reg", function(){
        $(".btn-signup").removeClass("btn-check-co").addClass("btn-check-pro");
        $(".co-hidden").show();
        $(".co-view").hide();
        $("input").val("");
        $('select').val("1");
        if($("input[type='checkbox']").is(":checked")){
        	$("input[type='checkbox']").click();
        } 
    });
    // 행정팀 입력폼 보기
    $(".signup-div").on("click",".btn-co-reg", function(){
        $(".btn-signup").removeClass("btn-check-pro").addClass("btn-check-co");
        $(".co-hidden").hide();
        $(".co-view").css("display","table").show();
        $("input").val("");
        $(".append-div").html("");
        $(".co-view-select1").val("ed");
        $(".co-view-select2").val("ed01");
    });

    var name = $("input.name");
    var level = $("input.level");
    var classm = $("input.classm");
    var id = $("input.id");
    var pw = $("input.pw");
    var npw = $("input.npw");
    var rpw = $("input.rpw");
    var telNum1 = $(".tel-p input[type='text']:first");
    var telNum2 = $(".tel-p input[type='text']:nth-of-type(2)");
    var telNum3 = $(".tel-p input[type='text']:last");
    var num = /[0-9]/g;
    var coSelect1 = $(".co-view-select1");
    var coSelect2 = $(".co-view-select2 option");
    // 행정팀 입력값 검사
    function chkCo(){
        if( name.val() == 0 ){
            alert("이름을 입력해주세요");
            name.focus();
            return ;
        }
        if( id.val() == 0 ){
            alert("아이디를 입력해주세요");
            id.focus();
            return ;
        }
        if( pw.val() == 0 ){
            alert("비밀번호를 입력해주세요.");
            pw.focus();
            return ;
        }
        if( pw.val().search(/\s/)!= -1 ){
            alert("비밀번호는 공백없이 입력해주세요.");
            npw.val("").focus();
            return ;
        }
        if ( pw.val()!= rpw.val() ){
            alert("비밀번호가 다릅니다. 다시 입력해주세요.");
            rpw.val("").focus();
            return ;
        }
        /*
        if ( npw.val().length < 6 || npw.val().length >12 ){
            alert("비밀번호를 6 ~ 12자리 이내로 입력해주세요.");
            npw.focus();
            return ;
        }
        
        if(self.location.href.indexOf("addUser") > -1 ){
        	
        }
        
        if ( pw.val() == npw.val() ){
            alert("새 비밀번호는 기존 비밀번호와 다르게 입력해주세요.");
            npw.val("").focus();
            return ;
        }
        
        if ( rpw.val() == 0 ){
            alert("비밀번호를 재입력해주세요.");
            rpw.focus();
            return ;
        }
        */
        if ( coSelect1.attr("value") == 1 ){
            alert("dddd");
        }
       
        registStaff(name.val(),id.val(),pw.val());
    }
    // 교수님 입력값 검사
    function chkPro(){
        if( name.val() == 0 ){
            alert("이름을 입력해주세요");
            name.focus();
            return ;
        }
        if( level.val() == 0 ){
            alert("직급을 입력해주세요");
            level.focus();
            return ;
        }
        if( classm.val() == 0 ){
            alert("교실명을 입력해주세요");
            classm.focus();
            return ;
        }
        if( id.val() == 0 ){
            alert("아이디를 입력해주세요");
            id.focus();
            return ;
        }
        if( pw.val() == 0 ){
            alert("비밀번호를 입력해주세요.");
            pw.focus();
            return ;
        }
        /*
        if ( pw.val() == npw.val() ){
            alert("새 비밀번호는 기존 비밀번호와 다르게 입력해주세요.");
            npw.val("").focus();
            return ;
        }
        */
        if( pw.val().search(/\s/)!= -1 ){
            alert("비밀번호는 공백없이 입력해주세요.");
            npw.val("").focus();
            return ;
        }
        if ( pw.val().length < 6 || pw.val().length >12 ){
            alert("비밀번호를 6 ~ 12자리 이내로 입력해주세요.");
            pw.focus();
            return ;
        }
        if ( rpw.val() == 0 ){
            alert("비밀번호를 재입력해주세요.");
            rpw.focus();
            return ;
        }
        if ( pw.val()!=rpw.val() ){
            alert("비밀번호가 다릅니다. 다시 입력해주세요.");
            rpw.val("").focus();
            return ;
        }
        if ( telNum1.val().length != 3 ){
            alert("연락처 앞 3자리를 입력해주세요");
            telNum1.val("").focus();
            return ;
        }
        if ( telNum2.val().length < 3 ){
            alert("연락처 입력해주세요");
            telNum2.val("").focus();
            return ;
        }
        if ( telNum3.val().length < 3 ){
            alert("연락처 입력해주세요");
            telNum3.val("").focus();
            return ;
        }
        if ( coSelect1.children("option:selected").val() === "1" ){
            alert("ddddss");
        }
        var profNum = $("#prof_num").val();
        var telNum = telNum1.val() + "-"+ telNum2.val() + "-"+ telNum3.val();
        var email = $("#user_email").val();
        var addition = $("#addition").val();
        var stPoint = $('input:checkbox[id="st_point"]').is(":checked") == true ? 'Y' : 'N';
        registProffesor(name.val(),level.val(),classm.val(), profNum, id.val(),pw.val(),telNum,email,addition, stPoint);
    }
    // 교수님 입력값 검사 호출
    $(".signup-btn-div").on("click",".btn-check-pro", function(){
        chkPro();
    });
    // 행정팀 입력값 검사 호출
    $(".signup-btn-div").on("click",".btn-check-co", function(){
        chkCo();
    });
    //체크박스 스타일
    $(".lb-checkbox").bind("click", function(){
        if ( $(this).children("input[type='checkbox']").is(":checked") ){
            $(this).addClass("on");
        } else {
            $(this).removeClass("on");
        }
    });

    // 행정팀 - 담당영역 셀렉트 추가
    $(".signup-div").on("click",".btn-add-co-select",function(){
        var slt1 = $(
            '<div class="addselect-div">' +
            '<select class="co-view-select1" onChange="changeListener(this)">' +
            '<option value="ed">교육</option>' +
            '<option value="re">연구</option>' +
            '<option value="si">교내봉사</option>' +
            '<option value="so">교외봉사</option>' +
            '</select>&nbsp;' +
            '<select class="co-view-select2">' +
            '<option value="ed01">강의시간</option>' +
            '<option value="ed02">강의평가</option>' +
            '<option value="ed03">시험관</option>' +
            '<option value="ed04">문항 및 교육 모듈 개발</option>' +
            '</select>&nbsp;' +
            '<button type="button" class="btn-add-del btn-del-co-select" title="삭제">-</button>' +
            '</div>'
        );
        $(".append-div").append(slt1);
    });
    // 행정팀 - 담당영역 추가된 셀렉트 삭제
    $(".signup-div").on("click",".btn-del-co-select",function(){
        if( confirm("삭제 하시겠습니까?") === true ){
            $(this).parent(".addselect-div").remove();
        }
    });
    // 행정팀 - 담당영역 셀렉트별 옵션 변경
    /*
    $(".signup-div").on("change",".co-view-select1",function(){
    	console.debug("changed =>"+ $(this).val());
        $(this).children("option:selected").each(function(){
            if( $(this).val() === "ed" ){ // 교육
                $(this).parent("select").siblings(".co-view-select2").html(
                    '<option value="01">강의시간</option>' +
                    '<option value="02">강의평가</option>' +
                    '<option value="03">시험관</option>' +
                    '<option value="04">문항 및 교육 모듈 개발</option>'
                );
            } else if( $(this).val() === "si" ){ // 교내봉사
                $(this).parent("select").siblings(".co-view-select2").html(
                    '<option value="01">본부보직</option>' +
                    '<option value="02">교내비 보직 봉사-의전원외</option>' +
                    '<option value="03">의전원 보직자</option>' +
                    '<option value="04">의전원 비보직자</option>'
                );
            } else if( $(this).val() === "re" ){ // 연구
                $(this).parent("select").siblings(".co-view-select2").html(
                    '<option value="01">논문</option>' +
                    '<option value="02">저술</option>' +
                    '<option value="03">기타</option>' +
                    '<option value="04">연구</option>' +
                    '<option value="05">활동</option>' +
                    '<option value="06">활동비</option>'
                );
            } else if( $(this).val() === "so" ){ // 교외봉사
                $(this).parent("select").siblings(".co-view-select2").html(
                    '<option value="01">학술봉사</option>' +
                    '<option value="02">의료봉사</option>' +
                    '<option value="03">기타</option>' +
                    '<option value="04">수상실적</option>'
                );
            }
        });
    });
    */
    
    $(".service-nav a").eq(0).addClass("active");
});
function changeListener(select){
	$(select).children("option:selected").each(function(){
        if( $(this).val() === "ed" ){ // 교육
            $(this).parent("select").siblings(".co-view-select2").html(
                '<option value="ed01">강의시간</option>' +
                '<option value="ed02">강의평가</option>' +
                '<option value="ed03">시험관</option>' +
                '<option value="ed04">문항 및 교육 모듈 개발</option>'
            );
        } else if( $(this).val() === "si" ){ // 교내봉사
            $(this).parent("select").siblings(".co-view-select2").html(
                '<option value="si01">본부보직</option>' +
                '<option value="si02">교내비 보직 봉사-의전원외</option>' +
                '<option value="si03">의전원 보직자</option>' +
                '<option value="si04">의전원 비보직자</option>'
            );
        } else if( $(this).val() === "re" ){ // 연구
            $(this).parent("select").siblings(".co-view-select2").html(
                '<option value="re01">논문</option>' +
                '<option value="re02">저술</option>' +
                '<option value="re03">기타</option>' +
                '<option value="re04">연구</option>' +
                '<option value="re05">활동</option>' +
                '<option value="re06">활동비</option>'
            );
        } else if( $(this).val() === "so" ){ // 교외봉사
            $(this).parent("select").siblings(".co-view-select2").html(
                '<option value="so01">학술봉사</option>' +
                '<option value="so02">의료봉사</option>' +
                '<option value="so03">기타</option>' +
                '<option value="so04">수상실적</option>'
            );
        }
    });
}

</script>
<script>
function registProffesor(userName,jobTitle,subject, profNum,userId,userPass,telNum,email,addition, stPoint){
	console.debug("userName :"+ userName);
	console.debug("jobTitle :"+ jobTitle);
	console.debug("subject :"+ subject);
	console.debug("profNum :"+ profNum);
	console.debug("userId :"+ userId);
	console.debug("userPass :"+ userPass);
	console.debug("telNum :"+ telNum);
	console.debug("email :"+ email);
	console.debug("addition :"+ addition);
	console.debug("stPoint :"+ stPoint);
	
	
	var param = {}
	param.userName = userName;
	param.jobTitle = jobTitle;
	param.subject = subject;
	param.profNum = profNum;
	param.userId = userId;
	param.userPass = userPass;
	param.telNum = telNum;
	param.email = email;
	param.addition = addition;
	param.stPoint = stPoint;
	
	$.ajax({
        type: "POST"
        ,url: "../user/addProfessor"
        ,contentType : 'application/json; charset=UTF-8'
        ,data: JSON.stringify(param)
        ,dataType: "json"
        ,global : false
        ,cache: false
        ,async: false
        ,success: function(json){
           console.debug(json);
           if(json == undefined && json == null){
        	   alert("오류가 발생 하였습니다.");
        	   return;
           }
           if(json.status == "fail"){
        	   alert(json.message);
        	   return;
           }
           alert("성공적으로 등록되었습니다");
           //history.back(-1);
           self.location.reload();
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
            alert("오류가 발생하였습니다");
        }
        ,complete: function() {
        	
        }
    });
}
function registStaff(name,id,pw){
	console.debug("registStaff =>"+ name + " / "+ id + " / "+ pw );
	
	var categoryIds = new Array();
	$(".co-view-select2").each(function(){
		 console.debug($(this).val());
		for(var i=0; i < categoryIds.length; i++){
 			if(categoryIds[i] == $(this).val()){
 				alert("중복선택된 담당영역을 확인하세요");
 				categoryIds = [];
 				return false;
			}
		}
		categoryIds.push($(this).val());			
	});
	if(categoryIds.length == 0){
		return;
	}
	
	var param = {}
	param.userName = name;
	param.userId = id;
	param.userPass = pw;
	param.categoryIds = categoryIds;
	
	$.ajax({
        type: "POST"
        ,url: "../user/addStaff"
        ,contentType : 'application/json; charset=UTF-8'
        ,data: JSON.stringify(param)
        ,dataType: "json"
        ,global : false
        ,cache: false
        ,async: false
        ,success: function(json){
           console.debug(json);
           if(json == undefined && json == null){
        	   alert("오류가 발생 하였습니다.");
        	   return;
           }
           if(json.status == "fail"){
        	   alert(json.message);
        	   return;
           }
           alert("성공적으로 등록되었습니다");
           history.back(-1);
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
            alert("오류가 발생하였습니다");
        }
        ,complete: function() {
        	
        }
    });
}

</script>
<%@ include file="../include/footer.jsp" %>