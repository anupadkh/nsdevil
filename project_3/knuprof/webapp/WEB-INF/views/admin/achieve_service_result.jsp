<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../include/admin_header.jsp" %>
<div id="wrap">
   <%@ include file="../include/top.jsp" %>
    <!-- //.header -->
    
    <div class="container-table">
        <div class="aside">
            <div class="contents-serach-btns-div">
                <input type="text" value="" placeholder="이름" />
                <button type="button" class="btn-search-name"><img src="images/btn-search.png" alt="검색" /></button>
            </div>
            <!-- //.contents-serach-btns-div -->
            
            <div class="count-div">
            	<p class="stay">
                    <span>전체</span>
                    <span>
                        <a href="javascript:getAchieveRegistUserList('');" id="registTotalProfessor" class="btn-done-cnt active"></a>
                    </span>
                </p>
                <p class="">
                    <span>미등록</span>
                    <span>
                        <a href="javascript:getAchieveRegistUserList('N');" id="unregistProfessor" class="btn-done-cnt"></a>
                    </span>
                </p>
                <p>
                    <span>등록완료</span>
                    <span>
                        <a href="javascript:getAchieveRegistUserList('Y');" id="registProfessor"  class="btn-done-cnt"></a>
                    </span>
                </p>
            </div>
            <!-- //.count-div -->
            
            <!-- 교수 리스트 -->
            <ul class="list-pro">
            	<!-- 
                <li class="view-all-pro">
                    <a href="javascript:getAchieveRegistUserList()">전체 확인</a>
                </li>
                -->
                <li id="professorListLayer"></li>
            </ul>
        </div>
        <!-- //.aside -->
        
		<div class="contents">
            <a class="btn-coll"><span>좌측메뉴 영역 접기/펴기 토글</span></a>

            <div class="contents-serach-btns-div">
                <p class="contents-title-name" id="contents-title-name">
                    <!-- <span class="name-pro">정동일</span><span>기생충학</span>-->
                </p>
                <div class="contents-btns-div">
                    <a href="javascript:toggleExcelRegistLayer(true)">엑셀파일 일괄 등록하기 (전체 교수님)</a>
                </div>
            </div>
            <!-- //.contents-serach-btns-div -->

            <div class="list-table-contents">
                    <p class="tab-content-table-title">
                        [교내 봉사]
                    </p>
                    <table class="service-table co-tabs-table">
                        <colgroup>
                            <col width="80" />
                            <col width="110" />
                            <col width="250" />
                            <col width="80" />
                            <col width="156" />
                            <col width="90" />
                            <col width="200" />
                            <col width="80" />
                            <col width="60" />
                        </colgroup>
                        <tbody id="tbody_1">
                            <tr class="auth_category_si01">
                                <th>
                                    업적내용
                                </th>
                                <th colspan="2">
                                    평가항목(평가기간이내)
                                </th>
                                <th>
                                    기준점수
                                </th>
                                <th>
                                    비고
                                </th>
                                <th>
                                    건수(회수)
                                </th>
                                <th>
                                    내용
                                </th>
                                <th>
                                    증빙서류
                                </th>
                                <th>
                                    점수
                                </th>
                            </tr>
                            <tr class="auth_category_si01">
                                <td rowspan="7">
                                    본부보직
                                </td>
                                <td colspan="2" class="text-left">
                                    부총장, 처장, 부처장 등<br />
                                    책임시수 3시간에 준하는 보직
                                </td>
                                <td>
                                    10
                                </td>
                                <td rowspan="2" class="text-left">
                                    경북대학교 수업 관리 지침&lt;보직교원 주당 교수시간 기준표&gt;에 준함
                                </td>
                                <td id="serviceCode_count_1_1">
                                    
                                </td>
                                <td class="text-left" id="serviceCode_content_1_1">
                                
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_1">

                                </td>
                                <td id="serviceCode_point_1_1">
                                    
                                </td>
                            </tr>
                            <tr class="auth_category_si01">
                                <td colspan="2" class="text-left border-l">
                                    입학본부장, 국제교류원장,<br /> 입학본부부본부장, 국제교류원부원장,<br />
                                    인재개발원장, 인재개발부원장 등<br /> 책임시수 3시간에 준하는 보직
                                </td>
                                <td>
                                    10
                                </td>
                                <td id="serviceCode_count_1_2">
                                    
                                </td>
                                <td class="text-left" id="serviceCode_content_1_2">
                                   
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_2">
                                   
                                </td>
                                <td id="serviceCode_point_1_2">
                                    
                                </td>
                            </tr>
                            <tr class="auth_category_si01">
                                <td colspan="2" class="text-left border-l">
                                    신문방송사주간, 정보전산원장,생활관장,<br /> 공동실험실습관장,보건진료소장
                                </td>
                                <td>
                                    2
                                </td>
                                <td>

                                </td>
                                <td id="serviceCode_count_1_3">
                                
                                </td>
                                <td class="text-left" id="serviceCode_content_1_3">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_3">

                                </td>
                                <td id="serviceCode_point_1_3">

                                </td>
                                
                            </tr>
                            <tr class="auth_category_si01">
                                <td colspan="2" class="text-left border-l">
                                    교수회 의장, 부의장, 처장, 부처장
                                </td>
                                <td>
                                    7
                                </td>
                                <td>

                                </td>
                                <td id="serviceCode_count_1_4">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_4">
                                   
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_4">

                                </td>
                                <td id="serviceCode_point_1_4">

                                </td>
                            </tr>
                            <tr class="auth_category_si01">
                                <td colspan="2" class="text-left border-l">
                                    기숙사 담당(명의관 포함), 부설연구소장
                                </td>
                                <td>
                                    2
                                </td>
                                <td>

                                </td>
                                <td id="serviceCode_count_1_5">

                                </td>
								<td class="text-left" id="serviceCode_content_1_5">
                                   
                                </td>
                                
                                <td class="service-table-up-td" id="serviceCode_file_1_5">

                                </td>
                                <td id="serviceCode_point_1_5">

                                </td>
                            </tr>
                            <tr class="auth_category_si01">
                                <td colspan="2" class="text-left border-l">
                                    특별학부장 (자율전공,글로벌인재)
                                </td>
                                <td>
                                    2
                                </td>
                                <td>

                                </td>
                                <td id="serviceCode_count_1_6">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_6">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_6">

                                </td>
                                <td id="serviceCode_point_1_6">

                                </td>
                            </tr>
                            <tr class="auth_category_si01">
                                <td colspan="2" class="text-left border-l">
                                    발전기금 담당 책임자
                                </td>
                                <td>
                                    3
                                </td>
                                <td>

                                </td>
                                <td id="serviceCode_count_1_7">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_7">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_7">

                                </td>
                                <td id="serviceCode_point_1_7">

                                </td>
                            </tr>
                            <tr class="auth_category_si02">
                                <td rowspan="6">
                                    교내 비보직 봉사-의전원외
                                </td>
                                <td rowspan="3">
                                    학생지도
                                </td>
                                <td class="text-left">
                                    학생단체(동아리)지도교수
                                </td>
                                <td>
                                    1
                                </td>
                                <td>

                                </td>
                                <td id="serviceCode_count_1_8">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_8">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_8">

                                </td>
                                <td id="serviceCode_point_1_8">

                                </td>
                            </tr>
                            <tr class="auth_category_si02">
                                <td class="text-left border-l">
                                    학생상담자원봉사교수
                                </td>
                                <td>
                                    1
                                </td>
                                <td>

                                </td>
                                <td id="serviceCode_count_1_9">

                                </td>
								<td class="text-left" id="serviceCode_content_1_9">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_9">

                                </td>
                                <td id="serviceCode_point_1_9">

                                </td>
                            </tr>
                            <tr class="auth_category_si02">
                                <td class="text-left border-l">
                                    학생관련 특수 프로그램 지도 교수
                                </td>
                                <td>
                                    1
                                </td>
                                <td>

                                </td>
                                <td id="serviceCode_count_1_10">

                                </td>
                               <td class="text-left" id="serviceCode_content_1_10">
                                   
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_10">

                                </td>
                                <td id="serviceCode_point_1_10">

                                </td>
                            </tr>
                            <tr class="auth_category_si02">
                                <td rowspan="3" class="border-l">
                                    국제협력봉사
                                </td>
                                <td class="text-left">
                                    해외대학/연구소 등과의 학술협력 주도
                                </td>
                                <td>
                                    0.5
                                </td>
                                <td rowspan="3" class="text-left">
                                    1. 국제교류원장이 정하는 활동만을 대상으로 한다.<br />
                                    2. 연간 최대 2점을 초과하지 않는다.
                                </td>
                                <td id="serviceCode_count_1_11">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_11">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_11">

                                </td>
                                <td id="serviceCode_point_1_11">

                                </td>
                            </tr>
                            <tr class="auth_category_si02">
                                <td class="text-left border-l">
                                    학생인솔 해외 프로그램 운영
                                </td>
                                <td>
                                    1
                                </td>
                                <td id="serviceCode_count_1_12">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_12">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_12">

                                </td>
                                <td id="serviceCode_point_1_12">

                                </td>
                            </tr>
                            <tr class="auth_category_si02">
                                <td class="text-left border-l">
                                    기타 국제협력활동
                                </td>
                                <td>
                                    0.2
                                </td>
                                <td id="serviceCode_count_1_13">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_13">
                                   
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_13">

                                </td>
                                <td id="serviceCode_point_1_13">

                                </td>
                            </tr>
                            <tr class="auth_category_si03">
                                <td rowspan="3">
                                    의전원<br />
                                    보직자
                                </td>
                                <td colspan="2" class="text-left">
                                    원장, 병원장 등 책임시수 3시간에 준하는 보직.
                                </td>
                                <td>
                                    20점 / 년
                                </td>
                                <td class="text-left">
                                    경북대학교 수업 관리 지침&lt;보직교원 주당 교수시간 기준표&gt;에 준함
                                </td>
                                <td id="serviceCode_count_1_14">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_14">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_14">

                                </td>
                                <td id="serviceCode_point_1_14">

                                </td>
                            </tr>
                            <tr  class="auth_category_si03">
                                <td colspan="2" class="text-left border-l">
                                    부원장(교무,학생,연구,교육) 및 부원장보실장<br />(기획, 입학, 임상수기, 대외협력),<br />
                                    기타(BK사업단장 등), 교수회의장
                                </td>
                                <td>
                                    12점 / 년
                                </td>
                                <td class="text-left">
                                    부원장보는 가중치 0.5
                                </td>
                                <td id="serviceCode_count_1_15">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_15">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_15">

                                </td>
                                <td id="serviceCode_point_1_15">

                                </td>
                            </tr>
                            <tr  class="auth_category_si03">
                                <td colspan="2" class="text-left border-l">
                                    대학원 학과장 (역학 및 건강증진학과장, 보건관리학과장,<br /> 보건학과장,
                                    법정의학과장, 과학수사학과장, 법의간호학과장,<br /> 의학과장, 의과학과장, 의용생체공학과장, 의료정보학과장),<br /> 국가 지정연구센터장, 교수회 부의장, 간사교실주임,<br /> BK사업단부단장/팀장, 학년담임 등
                                </td>
                                <td>
                                    6점 / 년
                                </td>
                                <td class="text-left">
                                    다수인 경우 인원수로 나눔
                                </td>
                                <td id="serviceCode_count_1_16">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_16">

                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_16">

                                </td>
                                <td id="serviceCode_point_1_16">

                                </td>
                            </tr>
                            <tr  class="auth_category_si04">
                                <td rowspan="5">
                                    의전원<br />
                                    비보직자
                                </td>
                                <td colspan="2" class="text-left">
                                    의전원장이 인정한 교내 봉사<br />
                                    -회의개최 의전원 위원회 위원에 대한 평점
                                </td>
                                <td>
                                    2점 / 년
                                </td>
                                <td class="text-left">
                                    위원장 / 간사는 가중치 2<br />
                                    &lt;주 3,4&gt; 참조
                                </td>
                                <td id="serviceCode_count_1_17">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_17">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_17">

                                </td>
                                <td id="serviceCode_point_1_17">

                                </td>
                            </tr>
                            <tr class="auth_category_si04">
                                <td colspan="2" class="text-left border-l">
                                    의학과장이 인정한 교내 봉사<br />
                                    -교내 활동 참여(입학시험면접, 출제 등), 학생지도 활동<br />(학생체육행사, 졸업여행 인솔, MMPI검사, 신입생OT/새터 등),<br /> 학교 행사 참여(신입생 환영회, 국외 대학 교류활동 등)
                                </td>
                                <td>
                                    1점 / 회
                                </td>
                                <td class="text-left">
                                    비 보직자만 해당함, 2시간미만은 가중치 0.5
                                </td>
                                <td id="serviceCode_count_1_18">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_18">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_18">

                                </td>
                                <td id="serviceCode_point_1_18">

                                </td>
                            </tr>
                            <tr class="auth_category_si04">
                                <td rowspan="3" class="border-l">
                                    학교발전기여도
                                </td>
                                <td class="text-left">
                                    대학발전기금 및 장학기금(현금 또는) 물품
                                </td>
                                <td>

                                </td>
                                <td class="text-left">
                                    3천만 원 이상 (9점)<br />
                                    2천만 원 이상 (7점)<br />
                                    1천만 원 이상 (5점)<br />
                                    5백만 원 이상 (3점)<br />
                                    100만 원 이상 (1점)
                                </td>
                                <td id="serviceCode_count_1_19">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_19">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_19">

                                </td>
                                <td id="serviceCode_point_1_19">

                                </td>
                            </tr>
                            <tr class="auth_category_si04">
                                <td class="text-left border-l">
                                    연구프로젝트 신청건수
                                </td>
                                <td>
                                    1점 / 건
                                </td>
                                <td class="text-left">
                                    5천만 원 이상 가중치 2점
                                </td>
                                <td id="serviceCode_count_1_20">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_20">
                                    
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_20">

                                </td>
                                <td id="serviceCode_point_1_20">

                                </td>
                            </tr>
                            <tr class="auth_category_si04">
                                <td class="text-left border-l">
                                    대학의 신규 주요사업(국책사업 및 대학종합평가 포함) 계획서 및 보고서 작성)
                                </td>
                                <td>
                                    2점 / 건
                                </td>
                                <td>

                                </td>
                                <td id="serviceCode_count_1_21">

                                </td>
                                <td class="text-left" id="serviceCode_content_1_21">
                                   
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_1_21">

                                </td>
                                <td  id="serviceCode_point_1_21">

                                </td>
                            </tr>
                            <tr class="total-score-tr">
                                <td colspan="4">
                                    소계
                                </td>
                                <td colspan="6" id="serviceCode_point_1_sum">
                                    0 점
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- //.service-table -->

                    <p class="tab-content-table-title">
                        [교외 봉사]
                    </p>
                    <table class="service-table co-tabs-table">
                        <colgroup>
                            <col width="80" />
                            <col width="" />
                            <col width="80" />
                            <col width="156" />
                            <col width="90" />
                            <col width="200" />
                            <col width="80" />
                            <col width="50" />
                        </colgroup>
                        <tbody>
                            <tr>
                                <th>
                                    업적내용
                                </th>
                                <th>
                                    평가항목(평가기간이내)
                                </th>
                                <th>
                                    기준점수
                                </th>
                                <th>
                                    비고
                                </th>
                                <th>
                                    건수(회수)
                                </th>
                                <th>
                                    내용
                                </th>
                                <th>
                                    증빙서류
                                </th>
                                <th>
                                    점수
                                </th>
                            </tr>
                            <tr class="auth_category_so01">
                                <td>
                                    학술봉사
                                </td>
                                <td class="text-left">
                                    국내외 학회 이사장, 회장, 국제학술대회 조직위원장/이사
                                    국외 SCI, SCIE, SSCI, AHCI 등재 학술지의 편집(부)위원장/위원
                                    한국연구재단 등재 학술지 편집위원(0.5점)
                                </td>
                                <td>
                                    6점 / 년
                                </td>
                                <td rowspan="2" class="text-left">
                                    3건만 인정함.
                                    초 세부학회 (대한 의학회 명시된 학회만 인정)는 가중치(0.5) 부여함.
                                    연구재단 등재지이상 발간 학회는 초세부학회에 준함.
                                    연구평가위원은 출장서류로 증빙함. 고시위원은 업적평가위원회에서 인정한 경우에 한함. 편집위원은 가중치 0.5
                                </td>
                                <td id="serviceCode_count_2_22">

                                </td>
                                <td  id="serviceCode_content_2_22">
                                	<!-- 
                                    <div class="service-table-content-div"
                                    	<!-- 
                                        <p>
                                            07.01 ~ 12.31
                                        </p>
                                        <p>
                                            입학본부장
                                        </p>
                                    </div>
                                    -->
                                </td>
                                <td class="service-table-up-td" id="serviceCode_file_2_22">
                                	<!-- 
                                    <p>
                                        <span class="text-bold"><a>3건</a></span>
                                    </p>
                                    <a class="btn-down">다운로드</a>
                                     -->
                                </td>
                                <td  id="serviceCode_point_2_22">
                                    
                                </td>
                            </tr>
                            <tr class="auth_category_so01">
                                <td>

                                </td>
                                <td class="text-left">
                                    연구평가위원(한국연구재단, 보건산업진흥원, 학력평가, MEET, 의학관련 고시위원
                                </td>
                                <td>
                                    1점 / 건
                                </td>
                               <td id="serviceCode_count_2_23">

                                </td>
                                <td  id="serviceCode_content_2_23">

                                </td>
                                <td class="service-table-up-td"  id="serviceCode_file_2_23">
  
                                </td>
                                <td id="serviceCode_point_2_23">

                                </td>
            	            </tr>
                            <tr class="auth_category_so02">
                                <td>
                                    의료봉사
                                </td>
                                <td class="text-left">
                                    국내외 의료봉사
                                </td>
                                <td>
                                    1점 / 건
                                </td>
                                <td class="text-left">
                                    1일 이상인 경우 인정.
                                </td>
                                <td  id="serviceCode_count_2_24">

                                </td>
                                <td  id="serviceCode_content_2_24">

                                </td>
                                <td class="service-table-up-td"  id="serviceCode_file_2_24">

                                </td>
                                <td id="serviceCode_point_2_24">

                                </td>
                            </tr>
                            <tr class="auth_category_so03">
                                <td rowspan="4">
                                    기타
                                </td>
                                <td class="text-left">
                                    전공 관련 국제기구 위원
                                </td>
                                <td>

                                </td>
                                <td class="text-left">
                                    UN, UNICEF, WHO급
                                </td>
                                <td id="serviceCode_count_2_25">

                                </td>
                                <td  id="serviceCode_content_2_25">

                                </td>
                                <td class="service-table-up-td"  id="serviceCode_file_2_25">

                                </td>
                                <td id="serviceCode_point_2_25">

                                </td>
                            </tr>
                            <tr class="auth_category_so03">
                                <td class="border-l text-left">
                                    정부기간 (가중치=1), 지자체(가중치=0.4), 국영기업체 및 이에 준하는 기관(가중치=0.2)의 각종 위원
                                </td>
                                <td>
                                    1점 / 년
                                </td>
                                <td class="text-left">
                                    &lt;주 4&gt; 참조, 3건만 인정함.
                                </td>
                                <td  id="serviceCode_count_2_26">

                                </td>
                                <td  id="serviceCode_content_2_26">

                                </td>
                                <td class="service-table-up-td"  id="serviceCode_file_2_26">

                                </td>
                                <td id="serviceCode_point_2_26">

                                </td>
                            </tr>
                            <tr class="auth_category_so03">
                                <td class="border-l text-left">
                                    특별강연 (정부기관, 지자체, 국영기업체 및 이에 준하는 기관이 주관)
                                </td>
                                <td>
                                    1점 / 건
                                </td>
                                <td class="text-left">
                                    3건만 인정함.
                                </td>
                                <td  id="serviceCode_count_2_27">

                                </td>
                                <td  id="serviceCode_content_2_27">

                                </td>
                                <td class="service-table-up-td"  id="serviceCode_file_2_27">

                                </td>
                                <td id="serviceCode_point_2_27">

                                </td>
                            </tr>
                            <tr class="auth_category_so03">
                                <td class="border-l text-left">
                                    동창회 및 의사회 (부)회장, 상임이사
                                </td>
                                <td>
                                    1점 / 년
                                </td>
                                <td  id="serviceCode_count_2_28">

                                </td>
                                <td>

                                </td>
                                <td  id="serviceCode_content_2_28">

                                </td>
                                <td class="service-table-up-td"  id="serviceCode_file_2_28">

                                </td>
                                <td id="serviceCode_point_2_28">

                                </td>
                            </tr>
                            <tr class="auth_category_so04">
                                <td rowspan="3">
                                    수상실적
                                </td>
                                <td class="text-left">
                                    전공 관련 국제기구 위원
                                </td>
                                <td>
                                    4점 / 건
                                </td>
                                <td rowspan="3" class="text-left">
                                    UN, UNICEF, WHO급
                                </td>
                                <td id="serviceCode_count_2_29">

                                </td>
                                <td id="serviceCode_content_2_29">

                                </td>
                                <td class="service-table-up-td"  id="serviceCode_file_2_29">

                                </td>
                                <td id="serviceCode_point_2_29">

                                </td>
                            </tr>
                            <tr class="auth_category_so04">
                                <td class="border-l text-left">
                                    특별강연 (정부기관, 지자체, 국영기업체 및 이에 준하는 기관이 주관)
                                </td>
                                <td>
                                    4점 / 건
                                </td>
                                <td  id="serviceCode_count_2_30">

                                </td>
                                <td  id="serviceCode_content_2_30">

                                </td>
                                <td class="service-table-up-td"  id="serviceCode_file_2_30">

                                </td>
                                <td  id="serviceCode_point_2_30">

                                </td>
                            </tr>
                            <tr class="auth_category_so04">
                                <td class="border-l text-left">
                                    동창회 및 의사회 (부)회장, 상임이사
                                </td>
                                <td>
                                    2점 / 년
                                </td>
                                <td  id="serviceCode_count_2_31">

                                </td>
                                <td  id="serviceCode_content_2_31">

                                </td>
                                <td class="service-table-up-td"  id="serviceCode_file_2_31">

                                </td>
                                <td id="serviceCode_point_2_31">

                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- //.co-tabs-table -->

                    <p class="tab-content-table-title" id="totalPointTitleLayer">
                        [총점]
                    </p>
                    <table class="service-table co-tabs-table" id="totalPointLayer">
                        <colgroup>
                            <col width="" />
                            <col width="" />
                            <col width="" />
                            <col width="" />
                            <col width="" />
                        </colgroup>
                        <tbody>
                            <tr>
                                <th>
                                    구분
                                </th>
                                <th>
                                    교내봉사
                                </th>
                                <th>
                                    교외봉사
                                </th>
                                <th>
                                    합계
                                </th>
                                <th>
                                    비고
                                </th>
                            </tr>
                            <tr class="total-score-tr">
                                <td>
                                    점수
                                </td>
                                <td id="serviceCode_point_1_sum_sum">
                                   0
                                </td>
                                <td id="serviceCode_point_2_sum_sum">
                                   0
                                </td>
                                <td id="serviceCode_point_total">
                                   0 점
                                </td>
                                <td>

                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="guide-txt-table-div">
                        <p>
                            &lt;주&gt;
                        </p>
                        <p>
                            1. [봉사영역 항목별 평가표]의 교내봉사 부분 중 교내 공문으로 발령 받은 경우는 의전원 행정실에서 작성한다. 단 교내 공문으로 발령 받았더라도 비고란에 조건이 있는 사항과 공문으로 발령 받지 않은 봉사내역은 내용과 함께 근거 자료를 제출하여야 한다. 교외봉사업적은 봉사영역 평가 항목별 평점표를 작성하고 근거자료를 제출 한다.
                        </p>
                        <p class="color-red">
                            2. 동일기간 보직을 겸직하는 경우 점수가 상위인 보직의 점수를 부여한다. 교실주임의 경우 주임교수회의를 1/3 참석하지 않은 경우 가점의 1/2만 인정한다.
                        </p>

                        <p>
                            3. 상임위원회, 운영위원회, 상임소위원회 위원, 기타 지원시설(공동기기실, 실험동물실, 의대도서관분관 등)의 장을 포함한다. 위원회 위원은 1/2 이상 회의 참석한 경우에만 가점을 부여하고, 참석여부는 회의록으로 확인한다.
                        </p>
                        <p>
                            4. 기간이 년 단위인 경우 1년 미만인 경우에는 기간을 반올림한 개월 수에 비례하여 환산한 점수를 부여한다.
                        </p>
                    </div>
                    <!-- //.guide-txt-table-div -->
                </div>
            
            
            <!-- 등록 버튼 영역 -->
            <div class="signup-btn-div text-right">
                <a href="javascript:updateAchieve()" class="btn-signup">수정</a>
            </div>
            <!-- //.signup-btn-div -->
        </div>
        <!-- //.contents -->
    </div>
    <!-- //.container-table -->
    <%@ include file="../include/bottom.jsp" %>
</div>
<!-- //#wrap -->
<div id="permissionhiddenLayer" class="hidden">

        <div class="date_not">
            <p><img src="images/not.png"></p>
            <p class="date_memo_title">
                <span id="categoryName">교육</span> 등록 권한이 없습니다.<br />
                관리자에게 문의해 주세요.
            <p>
            <p class="date_memo_text"></p>
            <p class="red_bt"><a href="javascript:void(0)">확인</a></p>
        </div>
</div>
	<div id="toggleExcelRegistLayer" style="display:none;">
			<div class="btn-down-div">
                <h2>
                    해당 되는 <ins>단체등록 양식을 다운로드</ins> 받으신 뒤 내용을 기입하시고,<br />
                    <ins>엑셀 파일을 아래 등록</ins>하시면 모든 교수님의 봉사등록이 일괄 완료됩니다.
                </h2>
                <div class="btn-execl-down-div">
                    <p>
                        봉사 일괄 등록양식<br />
						<a href="<c:url value="/download?path=file&fileName=achieve_service.xlsx"/>" target="_blank">
                            <img src="images/btn-exel-down.png" alt="엑셀다운로드" />
                        </a>
                    </p>
                </div>
            </div>
            <!-- //.btn-down-exel-div -->

            <!-- 이용자 등록 / 수정 영역 -->
            <form id='uploadForm' action='' onSubmit="return false;" enctype='multipart/form-data' method='post'>
            <div class="signup-div">
                <fieldset>
                    <legend>연구 일괄 등록 필드</legend>
                    <div>
                        <label>파일</label>
                        <p>
                            <input type="text" class="input-upload-txt" value="" readonly />
                            <label class="btn-input-file">
                                등록
                                <input type="file" class="input-upload" id="uploadInputFile" name="uploadInputFile" value="" />
                            </label>
                        </p>
                    </div>
                </fieldset>
            </div>
            </form>
            <!-- //.signup-div -->

            <div class="signup-btn-div" style="display:block">
                <a class="btn-signup check-addfile" href="javascript:uploadExcelFile();">봉사 일괄 등록</a>
                <a href="javascript:toggleExcelRegistLayer(false)" class="btn-cancel">취소</a>
            </div>
            <!-- //.signup-btn-div -->
	</div>
    <div class="wrap-popup wrap-plan-popup hidden popup-thesis" id="uploadFileLayer">
    	<input type="hidden" name="uploadServiceCode" />
        <div class="popup-guide">
            <div class="popup-guide-header">
                <h2>
                    증빙서류
                </h2>
                <a class="btn-close-giude-popup" title="close" onclick="javascript:$('#uploadFileLayer').fadeOut(300);">
                    X
                </a>
            </div>
            <div class="popup-guide-content">
                <fieldset>
                	<div class="popup-input-file-up-div">
                        <div id="uploadFileList"></div>
                        <div class="btn-submit-div" onclick="javascript:$('#uploadFileLayer').fadeOut(300);">
                            <a>완료</a>
                        </div>
                    </div>
                    <!-- //.popup-input-file-up-div -->
                </fieldset>
            </div>
        </div>
    </div>
   
    <form id="updateAchieve" method="post" action="<c:url value="/admin/updateAchieve"/>">
    	<input type="hidden" name="requestUserCode" value=""/>
    	<input type="hidden" name="category" value="${category}"/>
    	<input type="hidden" name="subject" value=""/>
    	<input type="hidden" name="userName" value=""/>
    </form>
	
	<div id="listTableContentsHiddenLayer" style="display:none;">
	</div>
<script type="text/javascript" src="<c:url value="/js/achieve_service_result.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery/jquery.blockUI.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery/jquery.form.min.js"/>"></script>
<script>
$(document).ready(function(){
	$(".view-all-pro a").css("border-bottom", "solid 0px #da2127");
    // 좌측메뉴 펼침 / 접기 토글 스크림트
    $(".btn-coll").bind("click", function(){
        $(".btn-coll").toggleClass("move-trigger");
        $(".aside").toggleClass("hidden-important");
        $(".contents").toggleClass("width-100");
    });
    // 이용자 이름 검색값 검사
    $(".btn-search-name").bind("click", function(){
        var input = $(this).siblings("input[type='text']");
        if ( input.val() == 0 ){
            alert("검색할 이름을 입력해주세요.");
            input.focus().val("");
        }
        findProfessor(input.val().trim());
        
    });
    // 재입력
    $(".btn-reset").bind("click", function(){
        $(".score-input-td input").val("");
        $(".score-input-td input:first").focus();
    });
    // textarea 자동 높이 맞춤
    $(".autoheight").on("keyup", "textarea", function (e){
        $(this).css("height", "auto");
        $(this).height( this.scrollHeight );
    });
    $(".autoheight").find("textarea").keyup();
    // textarea 자동 포커스
    $(".autoheight td").click(function (){
        $(this).children("textarea").focus();
    });
    // 미등록, 등록완료 숫자 버튼
    $(".aside .count-div p span a").each(function(){
        $(".aside .count-div p span a").bind("click",function(){
            var btnCount = $(".aside .count-div p span a");
            $(btnCount).removeClass("active");
            $(this).addClass("active");
        });
    });
    
 	// 등록 파일명 표시 & 확장자 검사
    
    
    
    $(".contents-serach-btns-div input").keydown(function (key) {
        if(key.keyCode == 13){//키가 13이면 실행 (엔터는 13)
        	 $(".btn-search-name").click();
        }
 
    });
    
    

    $(".service-nav a").removeClass("active");
        switch(category){
        case "e" :
        	$(".service-nav a").eq(2).addClass("active");
        	$("#categoryName").html("교육");
        	break;
        case "r" :
        	$(".service-nav a").eq(3).addClass("active");
        	$("#categoryName").html("연구");
        	break;
        case "s" : 
        	$(".service-nav a").eq(4).addClass("active");
        	$("#categoryName").html("봉사");
        	break;
    }
        
    //getCategoryHtmlData();
    getProfessorList();
    var userLevel = "${sessionScope.userlevel}";
    if(userLevel == 1){
		//$("#contents").html($("#permissionhiddenLayer").html());
    	
    	$(".total-score-tr").hide();
		$("#totalPointTitleLayer").hide();
    	$("#totalPointLayer").hide();
    	
    	//$("[class^='auth_category_']").hide();
    	try{
    		authInfo = ${json.authInfo};	
    	}catch(e){
    		console.error("authInfo is null or empty");
    	}
    	
    	hideUnPermissionLayer();
    }
    
    
});
function toggleExcelRegistLayer(isShow){
	console.debug("showExcelRegistLayer()");
	if(isShow){
		$(".contents-btns-div").hide();
		$(".signup-btn-div").hide();
		$(".list-table-contents").hide();
		$(".contents").append($("#toggleExcelRegistLayer")[0].outerHTML);
		
		$("#toggleExcelRegistLayer").show();
		$("#toggleExcelRegistLayer .signup-btn-div").show();
		$(".input-upload").each(function(){
	        $(this).change(function(){
	            if( $(this).val() != "" ){
	                var value = $(this).val();
	                var chk = $(this).val().split('.').pop().toLowerCase();
	                if($.inArray(chk, ["xlsx","xls"]) == -1){
	                    alert("엑셀 파일만 등록해주세요.");
	                    $(this).val("");
	                    return true;
	                } else {
	                    $(this).parent().siblings("input[type='text']").val(value);
	                }
	            }
	        });
	    });
	}else{
		$(".contents-btns-div").show();
		$(".signup-btn-div").show();
		$(".list-table-contents").show();
		$("#toggleExcelRegistLayer").remove();
		
	}
	
}
function uploadExcelFile(){
	var addFile = $(".input-upload").val();
    if ( addFile == 0 ){
        alert("엑셀 파일을 등록해주세요.");
        return true;
    }
    
    $("#uploadForm").ajaxForm({
		type: "POST",
		url:  '<c:url value="/admin/achieveExcelFile"/>',
		data: {
			"achieveType":"s"
		},
		dataType: "json",
		success: function(data, status){
			if (data.status == "success") {
				console.debug("data.resultCount =>"+ data.resultCount);
				alert("총 "+ data.resultCount + "건의 봉사 등록을 완료하였습니다.");
        	} else {
        		alert("오류가 발생했습니다");
        	}
		},
		error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        },
		beforeSend:function() {
			$.blockUI();
		},
		complete:function() {
			$.unblockUI();
		}
	}).submit();
}
var authInfo = null;
var contentsHtml = $(".contents").html();
var category = "${category}";
var userLevel = "${sessionScope.userlevel}";
var userCode = "${sessionScope.userCode}";
var currentRequestUserCode = null;

var requestUserCode = null;
var userName = "${userName}";
var subject = "${subject}";

function hideUnPermissionLayer(){
	if(userLevel == 0){
		return;
	}
	$("[class^='auth_category_']").hide();	
	if(authInfo != null){
		for(var i=0; i < authInfo.length; i++){
			console.debug(authInfo[i]);
			var categoryId = authInfo[i].categoryid;
			$("[class^='auth_category_"+ categoryId +"']").show();
		}
	}else{
		$("#contents").html($("#permissionhiddenLayer").html());
	}
}
function getCategoryHtmlData(){
	var param = {}
	param.category = category;

	$.ajax({
        type: "POST"
        ,url: "./getCategoryHtmlData"
        ,contentType : 'application/json; charset=UTF-8'
        ,data:JSON.stringify(param)
        ,dataType: "json"
        ,global : false
        ,cache: false
        ,async: false
        ,success: function(json){
           console.debug(json);
           if(json != undefined && json != null){
        	   if(json.status == "success"){
        		   if(json.list == undefined && json.list == null){
        			   $("#contents").html($("#permissionhiddenLayer").html());
        		   }
        	   }else{
        		   alert(json.message);
        	   }
           }
           return;
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
        	alert("오류가 발생하였습니다");
        }
        ,complete: function() {
        	
        }
    });
}
function getProfessorList(){
	$.ajax({
        type: "POST"
        ,url: "./getProfessorList"
        ,contentType : 'application/json; charset=UTF-8'
        ,data: ""
        ,dataType: "json"
        ,global : false
        ,cache: false
        ,async: true
        ,success: function(json){
           console.debug(json);
           if(json != undefined && json != null){
        	   if(json.status == "success"){
        		   if(json.list != undefined && json.list != null){
        			   
        			   for(var i=0; i <json.list.length; i++){
        				   var onClass = json.list[i].registered == "Y" ? "regist" : "unregist";
        				   var html = "";
        				   html += "<li id=\"professor_"+ json.list[i].usercode +"\" class=\"" + onClass + "\">";
        				   html +=     "<a href=\"javascript:getProfessorDetailData('"+ json.list[i].usercode + "')\">";
        				   html +=         "<span>"+ json.list[i].username + "</span><span>"+ json.list[i].subject +"</span>";
        				   html +=     "</a>";
        				   html +=     "<input type='hidden' name='userName' value='" + json.list[i].username + "'/>";
        				   html +=     "<input type='hidden' name='subject' value='" + json.list[i].subject + "'/>";
        				   html += "</li>";
        				   $("#professorListLayer").append(html);
        			   }
        		   }
        		   if(json.registInfo != undefined && json.registInfo != null){
        			  	console.error(json.registInfo);
        			  	$("#registTotalProfessor").html(json.registInfo.totalcount);
        			  	$("#unregistProfessor").html(json.registInfo.unregistcount);
        			  	$("#registProfessor").html(json.registInfo.registcount);
        		   }
        		   
        	   }else{
        		   alert(json.message);
        	   }
           }
           return;
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
        	alert("오류가 발생하였습니다");
        }
        ,beforeSend:function() {
        	$.blockUI();
		}
		,complete:function() {
			$.unblockUI();
		}
    });
}

function getProfessorDetailData(userCode){
	
	$(".contents").html(contentsHtml);
	$(".view-all-pro a").css("border-bottom", "solid 0px #da2127");
	$("[id^='professor_']").removeClass("on");
	$("#professor_" + userCode).addClass("on");
	
	var userName = $("#professor_" + userCode).children("input[name='userName']").val();
	var subject = $("#professor_" + userCode).children("input[name='subject']").val();
	
	$("#contents-title-name").html("<span class=\"name-pro\">" + userName + "</span><span>" + subject +"</span>");
	$("#updateAchieve input[name='userName']").val(userName);
	$("#updateAchieve input[name='subject']").val(subject);
	
	currentRequestUserCode = userCode;
	
	$.ajax({
        type: "POST"
        ,url: "./getServiceResult"
        ,data: {
            "userCode" : currentRequestUserCode
        }
        ,dataType: "json"
        ,success: function(data, status) {
        	if (data.status == "success") {        		
        		setServiceResult(data.serviceResult1);
        	}
        }
        ,error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        }
    });
}


function findProfessor(userName){
	var array = new Array();
	$("#professorListLayer input[name^='userName']").each(function(){
		if($(this).val().includes(userName)){
			array.push($(this).parent().attr("id"));
	    }
	});
	console.debug("array =>"+ array);
	var showRegist = "total";
	if($(".count-div p").eq(1).attr("class") == "stay"){
		showRegist = "unregist";
	}else if($(".count-div p").eq(2).attr("class") == "stay"){
		showRegist = "regist";
	}else{
	}
	if(array != null && array.length > 0){
		$("#professorListLayer li").hide();
		for(var i=0; i< array.length; i++){
			
			var professor = $("#" + array[i]); 
			if(showRegist == "unregist"){
				if(professor.attr("class") == "unregist"){
					professor.show();	
				}
			}else if(showRegist == "regist"){
				if(professor.attr("class") == "regist"){
					professor.show();	
				}
			}else{
				$("#" + array[i]).show();	
			}
			
		}
	}else{
		alert("검색결과가 없습니다");		
	}
}
var listTableContents = null;

function getAchieveRegistUserList(registered){
	$("[id^='professor_']").removeClass("on");
	$("#professorListLayer li").show();
	
	$(".count-div p").removeClass("stay");
	$(".contents-serach-btns-div input").val("");
	if(registered == "N"){
		$(".unregist").show();
		$(".regist").hide();
		$("#unregistProfessor").parent().parent().addClass("stay");;
	}else if(registered == "Y"){
		$(".unregist").hide();
		$(".regist").show();
		$("#registProfessor").parent().parent().addClass("stay");;
	}else{
		$(".unregist").show();
		$(".regist").show();
		$("#registTotalProfessor").parent().parent().addClass("stay");;
	}
	/*
	$(".view-all-pro a").css("border-bottom", "solid 1px #da2127");
	var param = {};
	if(registered != undefined && registered != null){
		param = { "registered" : registered};
	}
	
	
	$.ajax({
        type: "POST"
        ,url: "./getAchieveRegistUserList"
        ,data: param
		,cache: false
    	,async: true
        ,dataType: "json"
        ,success: function(data, status) {
        	//console.debug(data);
        	if (data.status == "success") {        		
        		if(data.achieveList != undefined && data.achieveList != null){
        			setAchieveListResult(data.achieveList);
        		}
        	}else{
        		alert("조회 도중 오류가 발생하였습니다");
        	}
        }
        ,error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        }
        ,beforeSend:function() {
        	$.blockUI();
		}
		,complete:function() {
			$.unblockUI();
		}
    });
	*/
	
}

function setAchieveListResult(list){
	$.blockUI();
	console.debug("setAchieveListResult ");
	console.debug(list);
	
	
	$(".contents").empty();


	var htmlTmp = $("<div/>").html(contentsHtml);
	htmlTmp.find(".btn-coll").remove();  //접기
	htmlTmp.find(".contents-serach-btns-div").remove(); // 엑셀일괄다운
	htmlTmp.find(".guide-txt-table-div").remove();//하단 안내문구
	htmlTmp.find(".signup-btn-div").remove();  //등록,수정버튼영역
	
	if(userLevel > 0){
		htmlTmp.find("#totalPointTitleLayer").remove(); // 총점 타이틀영역
		htmlTmp.find("#totalPointLayer").remove(); // 총점 테이블영역	
	}
	

	$(".contents").empty();
	
	var array = new Array();
	
	for(var i=0; i < list.length; i++){
		var data = list[i];
		//if(data.userCode == 294){
			
			$("#listTableContentsHiddenLayer").empty();
			var registDoneId = "regsitListLayer_"+ data.userCode
			var registDone = $("<div/>");
			
			registDone.attr("id", registDoneId);
			
			if(data.registered == "Y" ){
				registDone.addClass("regist-done");	
			}else{
				registDone.addClass("regist-no");
			}
			
			registDone.append(getRegistDoneNameHtml(data.username,data.subject, data.registered));
			registDone.append(htmlTmp.html());
			
			$("#listTableContentsHiddenLayer").append(registDone[0].outerHTML);
			
			jsonData = data.serviceResult;
			setServiceResult(jsonData, data.userCode);
			array.push($("#listTableContentsHiddenLayer").html());
		//}
		
	}	
	for(var i=0; i <array.length; i++){
		$(".contents").append(array[i]);
	}
	$("#listTableContentsHiddenLayer").empty();
	$.unblockUI();
}

var jsonData = null;
function getRegistDoneNameHtml(userName,subject, registered){
	var html = "";
	if(registered == "Y"){
		html += "<div class=\"contents-title-name-div\">";
		html += 	"<span class=\"regist-on\">(등록)</span>";
		html += 	"<p class=\"contents-title-name\">";
		html += 		"<span>" + userName + "</span><span>" + subject + "</span>";
		html += 	"</p>";
		html += "</div>";	
	}else if(registered == "N"){
		html += "<div class=\"contents-title-name-div\">";
		html += 	"<span class=\"regist-off\">(미등록)</span>";
		html += 	"<p class=\"contents-title-name\">";
		html += 		"<span>" + userName + "</span><span>" + subject + "</span>";
		html += 	"</p>";
		html += "</div>";
	}
	return html;
}
</script>
<%@ include file="../include/footer.jsp" %>
