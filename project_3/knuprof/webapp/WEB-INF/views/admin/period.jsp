<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../include/admin_header.jsp" %>
<div id="wrap">
    <%@ include file="../include/top.jsp" %>
    <!-- //.header -->
	<style>
	p.list-table-contents-tit{width:100%; display:inline-block; color:#000; font-size:16px; margin-bottom:2px; height:30px; line-height:30px;}
p.list-table-contents-tit em{display:inline-block; color:#e13838; font-size:13px; margin-right:2px; float:left;}

.tatal-info-text{width:99%; display:block;padding-left:1%; font-size:16px; color:#dc232a;  position:relative; padding-top:12px; border-top:1px dashed #ddd;}
.tatal-info-text input[type="text"]{width:60px;vertical-align:middle; border:solid 1px #ccc; padding:3px 5px; color:#666; font-size:13px; margin:0 4px 2px 4px; text-align:center;}
a.btn-signup-ab{ position:absolute; right:0; padding:8px 40px; border:solid 1px #d10707;color:#fff;background:#d83a3a; top:10px; cursor:pointer;}
	
	input {text-align:center};
	</style>
    <div class="container">

        <!-- 탭 메뉴 영역 -->
        <div class="tabmenu-div">
            <ul class="tabmenu-ul">
                <!-- 선택된 탭메뉴 표시 클래스 "active" -->
                <li >
                    <a href="<c:url value="/admin/"/>">이용자 관리</a>
                </li>
                <li>
                    <a href="<c:url value="/admin/gradeScore"/>">기준 점수</a>
                </li>
                <li class="active">
                    <a href="javascript:void(0)">기간 설정</a>
                </li>
            </ul>
        </div>
        <!-- //.tabmenu-div -->
        
        <!-- 이용자 구분 검색영역 -->
        <div class="contents-serach-btns-div" style="margin-bottom:0;">
            <div class="contents-search-div">
                <p>
                    <label for="search_year">년도 : </label>
                    <select id="searchYear">
                    	<option value="" selected></option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                    </select>
                </p>
                <p>
                    <button type="button" class="btn-search-name" onclick="searchPeriodList()"><img src="images/btn-search.png" alt="검색" /></button>
                </p>
            </div>
            <!-- //.contents-search-div -->
        </div>
        <!-- //.contents-serach-btns-div -->
        
        <!--<div class="guide-top-txt-div">
            ※ 평가년도 : 평가대상 년도를 4자리로 등록합니다 (예: 2016)
        </div>-->
        <!-- //.guide-top-txt-div -->
        
        <!-- 이용자 구분 검색영역 -->
        <div class="contents-serach-btns-div range-up-div">
            <div class="contents-search-div">
                <p>
                    <label>평가 기준 기간 : </label>
                </p>
                <p>
                    <span>
                        <input  id="period_a_start" type="text" class="month-datepicker" value="" />
                        <button type="button" class="btn-monthpick"><img src="images/calendar.png" alt="달력보기" /></button>
                    </span>
                    ~
                    <span>
                        <input  id="period_a_end"type="text" class="month-datepicker" value="" />
                        <button type="button" class="btn-monthpick"><img src="images/calendar.png" alt="달력보기" /></button>
                    </span>
                </p>
                <p class="contents-btns-div" id="addPeriodButton_a">
                    <a class="btn-submit-cou" href="javascript:addPeriod('A')">등록</a>
                </p>
                <!--<div class="guide-txt-div">
                    (안내) 검수기간이 등록되면 해당 기간에는 교수님들의 연구/봉사/산학협력 등록이 제한됩니다. (조회만 가능) 
                </div>-->
            </div>
        </div>
        <div class="contents-serach-btns-div range-up-div">
            <div class="contents-search-div">
                <p>
                    <label>등록 기간: </label>
                   
                </p>
                <p>
                    <span>
                        <input id="period_b_start" type="text" class="month-datepicker" value="" />
                        <button type="button" class="btn-monthpick"><img src="images/calendar.png" alt="달력보기" /></button>
                    </span>
                    ~
                    <span>
                        <input id="period_b_end" type="text" class="month-datepicker" value="" />
                        <button type="button" class="btn-monthpick"><img src="images/calendar.png" alt="달력보기" /></button>
                    </span>
                </p>
                <p class="contents-btns-div" id="addPeriodButton_b">
                    <a class="btn-submit-cou" href="javascript:addPeriod('B')">등록</a>
                </p>
                <!--<div class="guide-txt-div">
                    (안내) 검수기간이 등록되면 해당 기간에는 교수님들의 연구/봉사/산학협력 등록이 제한됩니다. (조회만 가능) 
                </div>-->
            </div>
        </div>
        
        <div class="contents-serach-btns-div range-up-div">
            <div class="contents-search-div">
                <p>
                    <label>조회 공시 기간 : </label>
                </p>
                <p>
                    <span>
                        <input id="period_c_start" type="text" class="month-datepicker" value="" />
                        <button type="button" class="btn-monthpick"><img src="images/calendar.png" alt="달력보기" /></button>
                    </span>
                    ~
                    <span>
                        <input id="period_c_end" type="text" class="month-datepicker" value="" />
                        <button type="button" class="btn-monthpick"><img src="images/calendar.png" alt="달력보기" /></button>
                    </span>
                </p>
                <p class="contents-btns-div" id="addPeriodButton_c">
                    <a class="btn-submit-par" href="javascript:addPeriod('C')">등록</a>
                </p>
                <!--<div class="guide-txt-div">
                    (안내) 평가확인 기간에만 교수님들께 My 메뉴를 통해 평가결과 확인 페이지가 제공됩니다. (기간을 등록하지 않으면 평가결과를 확인할 수 없습니다.)
                </div>-->
            </div>
            <!--<div class="contents-btns-div">
                <a>등록</a>
            </div>-->
        </div>
        <!-- //.contents-serach-btns-div -->
        
		<p class="list-table-contents-tit"><em>■</em>평가 유형 별 총점 계산 기준</p>        
        <!-- 테이블 영역 -->
        <div class="list-table-contents">
            <!-- 페이지당 리스트 최대 6건 노출, 등록일 내림차순 -->
            <table class="service-table">
                <colgroup>
                    <col width="20%" />
                    <col width="20%" />
                    <col width="20%" />
                    <col width="20%" />
                    <col width="20%" />
                </colgroup>
                <tbody>
                    <tr>
                        <th>
                            평가영역
                        </th>
                        <th>
                            표준형
                        </th>
                        <th>
                            교육중심
                        </th>
                        <th>
                            연구중심
                        </th>
                        <th>
                            봉사중심
                        </th>
                    </tr>
                    <tr>
                        <td>
                            교육
                        </td>
                        <td>
                             <input type="text" id="ed_st" name="st" onblur="chkCriterionValid(this,'st')" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="" />%
                        </td>
                        <td>
                            <input type="text" id="ed_ed" name="ed" onblur="chkCriterionValid(this,'ed')" onkeypress='return event.charCode >= 48 && event.charCode <= 57'value=""  />%
                        </td>
                        <td>
                            <input type="text" id="ed_re" name="re" onblur="chkCriterionValid(this,'re')" onkeypress='return event.charCode >= 48 && event.charCode <= 57'value=""  />%
                        </td>
                        <td>
                            <input type="text" id="ed_se" name="se" onblur="chkCriterionValid(this,'se')" onkeypress='return event.charCode >= 48 && event.charCode <= 57'value=""  />%
                        </td>
                    </tr>
                    <tr>
                        <td>
                            연구
                        </td>
                        <td>
                             <input type="text" id="re_st" name="st" onblur="chkCriterionValid(this,'st')" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="" />%
                        </td>
                        <td>
                            <input type="text" id="re_ed"  name="ed" onblur="chkCriterionValid(this,'ed')" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value=""  />%
                        </td>
                        <td>
                            <input type="text" id="re_re"  name="re" onblur="chkCriterionValid(this,'re')" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value=""  />%
                        </td>
                        <td>
                            <input type="text" id="re_se" name="se" onblur="chkCriterionValid(this,'se')" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value=""  />%
                        </td>
                    </tr>
                    <tr>
                        <td>
                            봉사
                        </td>
                        <td>
                             <input type="text" id="se_st"  name="st" onblur="chkCriterionValid(this,'st')" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="" />%
                        </td>
                        <td>
                            <input type="text" id="se_ed"   name="ed" onblur="chkCriterionValid(this,'ed')" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value=""  />%
                        </td>
                        <td>
                            <input type="text" id="se_re"  name="re"  onblur="chkCriterionValid(this,'re')" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value=""  />%
                        </td>
                        <td>
                            <input type="text" id="se_se"  name="se" onblur="chkCriterionValid(this,'se')" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value=""  />%
                        </td>
                    </tr>
                    <tr>
                        <td>
                            합계
                        </td>
                        <td>
                             <input type="text" id="sumst" value="0" disabled />%
                        </td>
                        <td>
                            <input type="text" id="sumed" value="0" disabled />%
                        </td>
                        <td>
                            <input type="text" id="sumre" value="0" disabled />%
                        </td>
                        <td>
                            <input type="text" id="sumse" value="0" disabled />%
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- //.service-table -->
           
        </div>
        <!-- //.list-table-contents -->
        
        <!-- 등록 버튼 영역 -->
           <div class="signup-btn-div text-right">
               <a class="btn-signup" onclick="addCriterion()" >등록</a>
           </div>
       <!-- //signup-btn-div text-right -->  
       
       <p class="list-table-contents-tit"><em>■</em>평가 등급 비중 관리</p>
       
       <!-- 테이블 영역 -->
        <div class="list-table-contents">
            <!-- 페이지당 리스트 최대 6건 노출, 등록일 내림차순 -->
            <table class="service-table">
                <colgroup>
                    <col width="16.666%" />
                    <col width="16.666%" />
                    <col width="16.666%" />
                    <col width="16.666%" />
                    <col width="16.666%" />
                    <col width="16.666%" />
                </colgroup>
                <tbody>
                    <tr>
                        <th>
                            S
                        </th>
                        <th>
                            A
                        </th>
                        <th>
                            B
                        </th>
                        <th>
                            C
                        </th>
                        <th>
                            D
                        </th>
                        <th>
                            합계
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="grade_s" onblur="chkGradeValid(this,'s')" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="" />%
                        </td>
                        <td>
                             <input type="text" id="grade_a"  onblur="chkGradeValid(this,'a')" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value=""  />%
                        </td>
                        <td>
                            <input type="text" id="grade_b" onblur="chkGradeValid(this,'b')" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="" />%
                        </td>
                        <td>
                            <input type="text" id="grade_c" onblur="chkGradeValid(this,'c')" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value=""  />%
                        </td>
                        <td>
                            <input type="text" id="grade_d" onblur="chkGradeValid(this,'d')" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value=""  />%
                        </td>
                        <td>
                            <input type="text" id="gradeSum"value="0" disabled />%
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- //.service-table -->
           
        </div>
        <!-- //.list-table-contents -->
        <!-- 등록 버튼 영역 -->
        <div class="signup-btn-div text-right">
        	<a class="btn-signup" onclick="addGrade()">등록</a>
        </div>
        <!-- //signup-btn-div text-right --> 
       
        <!-- 최소기준합계기준 영역 --> 
        <div class="total-sc">
        
        	<!-- 최소기준합계기준 타이틀 영역 --> 
      		<p class="list-table-contents-tit"><em>■</em>최소 기준 합계 기준 </p> 
       	 	<!-- //list-table-contents-tit --> 
        
        	<!-- 최소기준합계기준 text 영역 --> 
        	<p class="tatal-info-text">
	        	최소 기준의 합계가<input type="text" id="minPointSum" onblur="chkMinSumValid(this)" value="" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 46" style="ime-mode:disabled"/>점 이상일 경우 “가(PASS)" 임
        		<a class="btn-signup-ab" onClick="addMinPoint();">등록</a>
        	</p> 
        	<!-- //total_info  --> 
       
        	<!-- 등록 버튼 영역 -->
        	<div class="signup-btn-div text-right"></div>
        	<!-- //signup-btn-div text-right --> 
        </div> 
         <!-- //total_sc-->    	
    </div>
    <!-- //.container -->

   <%@ include file="../include/bottom.jsp" %>
</div>
<!-- //#wrap -->

<!-- 기간 수정 / 삭제 팝업 -->
<div class="wrap-popup wrap-rage-del-popup hidden">
	<input type="hidden" name="hiddenYear"/>
	<input type="hidden" name="hiddenStartDate"/>
	<input type="hidden" name="hiddenEndDate"/>
    <div class="popup-guide">
        <div class="popup-guide-header">
            <h2>
                <span name="title">2016 조회공시 기간 </span>수정
            </h2>
            <a class="btn-close-rage-popup" title="close">
                X
            </a>
        </div>
        <div class="popup-guide-content">
            <h3>
                 <span name="title">2016 조회공시 기간</span>
            </h3>
            <p class="popup-input-date-p">
                <label>기간설정 : </label>
                <span>
                    <input type="text" name="startDate" id="startDateId" class="month-datepicker" value="" />
                    <button type="button" class="btn-monthpick"><img src="images/calendar.png" alt="달력보기" /></button>
                </span>
                ~
                <span>
                    <input type="text" name="endDate" class="month-datepicker" value="" />
                    <button type="button" class="btn-monthpick"><img src="images/calendar.png" alt="달력보기" /></button>
                </span>
            </p>
            <!--<div class="textarea-div">
                <textarea cols="30" rows="4" placeholder="(필수등록) 검수기간 삭제사유를 남겨주세요."></textarea>
            </div>-->
            <div class="popup-btns-div">
                <!-- <button type="button">수정</button>-->
                <!--<button type="button">삭제</button>-->
                <!-- <button type="button" class="btn-close-rage-popup">취소</button>-->
            </div>
        </div>
    </div>
</div>
<!-- //.wrap-popup -->

<script src="js/datetimep.js"></script>
<script src="js/monthdatepicker_ui.js"></script>
<script type="text/javascript" src=<c:url value="/js/paging.js"/>></script>
<script>
$(document).ready(function(){
    // 월,일 달력 선택
    $(".month-datepicker").datepicker({
        changeYear:false,
        changeMonth: true,
        dateFormat:'yy-mm-dd',
        showOtherMonths: true,
        selectOtherMonths: true
    });
    $('.btn-monthpick').bind('click',function(){
        $(this).siblings('input').datepicker('show');
    });
    // 년,월,일 달력 선택
    $.datetimepicker.setLocale('kr');
    $('.dateyearpicker-input').datetimepicker({
        timepicker:false,
        datepicker:true,
        format:'y.m.d',
        formatDate:'y.m.d'
    });
    $('.btn-yearmonthpick').bind('click',function(){
        $(this).siblings('input').datetimepicker('show');
    });
    // 테이블 라인 배경
    $(".service-table tr:even").css("background","#fdfdfd");
    // 기간 수정, 삭제 팝업
    /*
    $(".btn-range-re,.btn-range-del").bind("click", function(){
        $("html").css("overflow-y","hidden");
        $(".wrap-rage-del-popup").fadeIn(200);
    });
    */
    $(".btn-close-rage-popup").bind("click", function(){
        $("html").css("overflow-y","auto");
        $(".wrap-rage-del-popup").fadeOut(200);
    });
    // 검수기간등록
    $(".btn-submit-cou,.btn-submit-par").bind("click", function(){
        var year = $(this).parent().siblings().children("input[type='text']");
        var month = $(this).parent().siblings().children().children("input.month-datepicker");
        var num = /[0-9]/g;
        if ( year.val() == 0 ){
            alert("평가년도를 입력해주세요");
            year.focus().val("");
            return false;
        } else if ( year.val().length != 4 || !num.test($.trim($(year).val())) ){
            alert("평가년도는 4자리 숫자로 입력해주세요.");
            year.focus().val("");
            return false;
        } 
        if ( month.val() == 0 ){
            alert("기간을 설정해주세요");
            $(month).first().focus();
            return ;
        }
    });
    
    $(".service-nav a").removeClass("active");
    $(".service-nav a").eq(0).addClass("active");

    //getPeriodList( new Date().getFullYear());
});
</script>
<script>
var userLevel = "${sessionScope.userlevel}";
var searchOption = null;
function searchPeriodList(){
	if(searchOption == null){
		searchOption = new Object();
	}
	searchOption.searchYear = $("#searchYear").val();
	console.debug("searchYear =>"+ searchOption.searchYear);
	getPeriodList();
}
function getPageList(pageNo){
	getPeriodList(pageNo);
	if(searchOption == null){
		searchOption = new Object();
	}
	searchOption.pageNo = pageNo;
}
function getPeriodList(year){
	if(searchOption == null){
		searchOption = new Object();
	}
	
	if(year != null){
		searchOption.searchYear = year;
	}
	
	var param = {};
	if(searchOption != null){
		if(searchOption.searchYear != "undefined"){
			param.searchYear = searchOption.searchYear;
		} 	
	}
	
	$.ajax({
        type: "POST",
        url: "./getPeriodList",
        data: param,
        dataType: "json",
        success: function(data, status) {
        	console.debug(data);
        	if (data.status == "success") {
        		setPeriodList(data.periodList);
        		setPeriodCriterion(data.periodCriterion);
        		setPeriodGrade(data.periodGrade);
        	}else{
        		//alert("검색결과가 없습니다");	
        	}
        	
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
			console.error(error);
        }
    });
}
function setPeriodList(data){
	console.debug("setPeriodList =>"+ data);
	console.debug(data);
	for(var i=0; i< data.length; i++){
		console.debug(data[i]);
		var period = data[i];
		if(period.period_type == "A"){
			$("#period_a_start").datepicker({dateFormat:'yyyy-mm-dd'});
			$("#period_a_start").datepicker('setDate',period.start_date);
			$("#period_a_end").datepicker({dateFormat:'yyyy-mm-dd'});
			$("#period_a_end").datepicker('setDate',period.end_date);
			$("#addPeriodButton_a").html("<a class=\"btn-submit-cou\" href=\"javascript:modifyPeriod('"+ period.period_code+ "','"+ period.year+ "','a','평가 기준 기간')\">수정</a>");
		}
		if(period.period_type == "B"){
			$("#period_b_start").datepicker({dateFormat:'yyyy-mm-dd'});
			$("#period_b_start").datepicker('setDate',period.start_date);
			$("#period_b_end").datepicker({dateFormat:'yyyy-mm-dd'});
			$("#period_b_end").datepicker('setDate',period.end_date);
			$("#addPeriodButton_b").html("<a class=\"btn-submit-cou\" href=\"javascript:modifyPeriod('"+ period.period_code+ "','"+ period.year+ "','b','등록 기간')\">수정</a>");
		}
		if(period.period_type == "C"){
			$("#period_c_start").datepicker({dateFormat:'yyyy-mm-dd'});
			$("#period_c_start").datepicker('setDate',period.start_date);
			$("#period_c_end").datepicker({dateFormat:'yyyy-mm-dd'});
			$("#period_c_end").datepicker('setDate',period.end_date);
			$("#addPeriodButton_c").html("<a class=\"btn-submit-cou\" href=\"javascript:modifyPeriod('"+ period.period_code+ "','"+ period.year+ "','c','조회 공시 기간')\">수정</a>");
		}
		if(period.minimum_point != null && period.minimum_point.length > 0){
			$("#minPointSum").val(period.minimum_point);	
		}
	}
}
function modifyPeriod(periodCode,year,type,title){	
	var startDate = $("#period_"+type + "_start").val() + " 00:00:00";
	var endDate = $("#period_"+type + "_end").val() + " 23:59:59";
	console.debug(startDate  + " _ " + endDate);
    var contents = year+"년 " + title + " " + $("#period_"+type + "_start").val() + " 부터 " +  $("#period_"+type + "_end").val() + " 까지  입니다";
	var type = type.toUpperCase();
	
	if(confirm(title + "을 수정 하시겠습니까?")){
		$.ajax({
	        type: "POST",
	        url: "./updatePeriod",
	        data: {
	        	"useFlag" : "Y"
	        	,"contents" : contents
	        	,"periodCode" : periodCode  
	        	,"startDate" : startDate
	        	,"endDate" : endDate
	        },
	        dataType: "json",
	        success: function(data, status) {
	        	console.debug(data);
	        	if (data.status == "success") {
	        		alert("수정 완료");
	        		getPeriodList(1);
	        	}
	        },
	        error: function(xhr, textStatus) {
				alert("오류가 발생했습니다.");
				console.error(error);
	        }
	    });	
	}
}
function chkCriterionValid(elem,type){
	var id = $(elem).attr("id");
	console.debug(id);
	console.debug(type);
	
	var types = $("[id$='_"+ type + "']");
	var sum = 0;
	$("[id$='_"+ type +"']").each(function(e){
		var point = $(this).val();
		point = parseInt(point,10);
		console.debug(isNaN(point));
		if(!isNaN(point)){
			sum = sum+ point;			
		}
	});
	if(sum > 100){
		alert("합계를 확인하세요.");
		$(elem).val(0);
		$(elem).focus();
	}else{
		$("#sum" + type).val(sum);	
	}	
}
function setPeriodCriterion(data){
	for(var i=0; i <data.length; i++){
		var crit = data[i];
		$("#" + crit.category_code + "_st").val(crit.type_st);
		$("#" + crit.category_code + "_ed").val(crit.type_ed);
		$("#" + crit.category_code + "_re").val(crit.type_re);
		$("#" + crit.category_code + "_se").val(crit.type_se);
	}
	$("#sumst").val(100);
	$("#sumed").val(100);
	$("#sumre").val(100);
	$("#sumse").val(100);
}
function addCriterion(){
	if($("#sumst").val() != 100){
		alert("표준형 영역의 합계를 확인하세요");
		return;
	}
	if($("#sumed").val() != 100){
		alert("교육중심 영역의 합계를 확인하세요");
		return;		
	}
	if($("#sumre").val() != 100){
		alert("연구중심 영역의 합계를 확인하세요");
		return;
	}
	if($("#sumse").val() != 100){
		alert("봉사중심 영역의 합계를 확인하세요");
		return;
	}
	var paramList = {};
	var data ={};
	
	$("[id^='ed_']").each(function(e){
		data.year = searchOption.searchYear;
		data.category_code = "ed";
		if($(this).attr("name") == "st"){
			data.type_st = $(this).val();	
		}
		if($(this).attr("name") == "ed"){
			data.type_ed = $(this).val();	
		}
		if($(this).attr("name") == "re"){
			data.type_re = $(this).val();	
		}
		if($(this).attr("name") == "se"){
			data.type_se = $(this).val();	
		}
	});
	paramList[0] = data;
	
	var data ={};
	$("[id^='re_']").each(function(e){
		data.year = searchOption.searchYear;
		data.category_code = "re";
		if($(this).attr("name") == "st"){
			data.type_st = $(this).val();	
		}
		if($(this).attr("name") == "ed"){
			data.type_ed = $(this).val();	
		}
		if($(this).attr("name") == "re"){
			data.type_re = $(this).val();	
		}
		if($(this).attr("name") == "se"){
			data.type_se = $(this).val();	
		}
	});
	paramList[1] = data;
	
	var data ={};
	$("[id^='se_']").each(function(e){
		data.year = searchOption.searchYear;
		data.category_code = "se";
		if($(this).attr("name") == "st"){
			data.type_st = $(this).val();	
		}
		if($(this).attr("name") == "ed"){
			data.type_ed = $(this).val();	
		}
		if($(this).attr("name") == "re"){
			data.type_re = $(this).val();	
		}
		if($(this).attr("name") == "se"){
			data.type_se = $(this).val();	
		}
	});
	paramList[2] = data;
	
	console.debug(paramList);
	
	var paramData = {};
	paramData.paramList = paramList;
	$.ajax({
        type: "POST",
        url: "./updateCriterion",
        contentType : 'application/json; charset=UTF-8',
        data: JSON.stringify(paramData),
        dataType: "json",
        success: function(data, status) {
        	console.debug(data);
        	if (data.status == "success") {
        		alert("등록되었습니다");
        	}else if(data.status =="fail"){
        		alert("등록에 실패하였습니다.");
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
			console.error(error);
        }
    });
	
}
function setPeriodGrade(data){
	$("#grade_s").val(data.grade_s);
	$("#grade_a").val(data.grade_a);
	$("#grade_b").val(data.grade_b);
	$("#grade_c").val(data.grade_c);
	$("#grade_d").val(data.grade_d);	
	$("#gradeSum").val(100);
}
function chkGradeValid(elem){
	var sum =0;
	$("[id^='grade_']").each(function(e){
		var point = $(this).val();
		point = parseInt(point,10);
		console.debug(isNaN(point));
		if(!isNaN(point)){
			sum = sum+ point;			
		}
	});
	if(sum > 100){
		alert("합계를 확인하세요.");
		$(elem).val(0);
		$(elem).focus();
	}else{
		$("#gradeSum").val(sum);	
	}	
}
function addGrade(){
	if($("#gradeSum").val() != 100){
		alert("평가 유형 별 총점 계산 기준의 합계를 확인하세요");
		return;
	}
	var paramData = {};
	paramData.grade_s = $("#grade_s").val();
	paramData.grade_a = $("#grade_a").val();
	paramData.grade_b = $("#grade_b").val();
	paramData.grade_c = $("#grade_c").val();
	paramData.grade_d = $("#grade_d").val();
	paramData.year = searchOption.searchYear;
	
	$.ajax({
        type: "POST",
        url: "./updatePeriodGrade",
        data: paramData,
        dataType: "json",
        success: function(data, status) {
        	console.debug(data);
        	if (data.status == "success") {
        		alert("등록되었습니다");
        	}else if(data.status =="fail"){
        		alert("등록에 실패하였습니다.");
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
			console.error(error);
        }
    });
	
	
}
function onlyNumDecimalInput(elem){
	var code = window.event.keyCode;
	console.debug(code);
	if ((code >= 48 && code <= 57) || code == 46){
		window.event.returnValue = true;
	   	return;
	}
	window.event.returnValue = false;	
}
function chkMinSumValid(elem){
	console.debug(elem);
	if($(elem).val() > 100){
		alert("최소기준합계기준을 정확히 입력 하세요");
		$(elem).val(0);
		$(elem).focus();
		return;
	}else{
		var value = parseFloat($(elem).val());
		console.debug(value);
		value = value.toFixed(2);
		$(elem).val(value);	
	}
}
function addMinPoint(){
	var paramData = {};
	paramData.minimum_point = $("#minPointSum").val();
	paramData.year = searchOption.searchYear;
	
	$.ajax({
        type: "POST",
        url: "./updateAchievePeriodMinPoint",
        data: paramData,
        dataType: "json",
        success: function(data, status) {
        	console.debug(data);
        	if (data.status == "success") {
        		alert("등록되었습니다");
        	}else if(data.status =="fail"){
        		alert("등록에 실패하였습니다.");
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
			console.error(error);
        }
    });
}
	
function updatePeriod(periodCode,mode,title,year,startDate,endDate){
	switch(mode){
	case "delete":
		if(confirm(title + "를 삭제하시겠습니까?")){
			var contents = title + "이 삭제 되었습니다";
			$.ajax({
		        type: "POST",
		        url: "./updatePeriod",
		        data: {
		        	"useFlag" : "N"
		        	,"periodCode" : periodCode  
		        	,"contents" : contents
		        },
		        dataType: "json",
		        success: function(data, status) {
		        	console.debug(data);
		        	if (data.status == "success") {
		        		getPeriodList(1);
		        	}
		        },
		        error: function(xhr, textStatus) {
					alert("오류가 발생했습니다.");
					console.error(error);
		        }
		    });	
		}
		break;
	case "modify" :
	    $("html").css("overflow-y","hidden");
	    $(".wrap-rage-del-popup span[name='title']").html(title);
	    $(".wrap-rage-del-popup input[name='hiddenYear']").val(year);
	    
		$(".popup-guide input[name='startDate']").val(startDate);
	    $(".popup-guide input[name='endDate']").val(endDate);
	    
	    var html ="<button type=\"button\" onclick=\"updatePeriod("+ periodCode + ",'update','')\">수정</button>";
	    html += "<button type=\"button\" class=\"btn-close-rage-popup\">취소</button>";
	    $(".wrap-rage-del-popup .popup-btns-div").html(html);
        $(".wrap-rage-del-popup").fadeIn(200);
        $(".btn-close-rage-popup").bind("click", function(){
            $("html").css("overflow-y","auto");
            $(".wrap-rage-del-popup").fadeOut(200);
        });
		break;
	case "update":
		var start = $(".wrap-rage-del-popup input[name='startDate']");
		var end = $(".wrap-rage-del-popup input[name='endDate']");
		console.debug(start.val() + " ~ "+ end.val());
		if(start.val() == null || start.val() ==""){
			alert("시작일을 확인하세요");
			start.focus();
			return;
		}
		if(end.val() == null || end.val() ==""){
			alert("종료일을 확인하세요");
			end.focus();
			return;
		}
		var year = $(".wrap-rage-del-popup input[name='hiddenYear']").val();
		var title =  $(".wrap-rage-del-popup span[name='title']").html();
		
	    if(parseInt(year + start.val().replace(".",""),10) > parseInt(year + end.val().replace(".",""),10)){
	    	alert("시작일은 종료일보다 이전 이어야 합니다.")
	    	return;
	    }
	    //var startDate = year + "-" + start.val().replace(".","-") + " 00:00:00";
		//var endDate = year + "-" + end.val().replace(".","-") + " 23:59:59";
		//var contents = title + " 이 " +  year + "-" + start.val().replace(".","-") + "  ~ " + year + "-" + end.val().replace(".","-") + " 로 수정되었습니다";
		var startDate = start.val().replace(".","-") + " 00:00:00";
		var endDate = end.val().replace(".","-") + " 23:59:59";
		var contents = title + " 이 " + start.val().replace(".","-") + "  ~ " + end.val().replace(".","-") + " 로 수정되었습니다";
		$.ajax({
	        type: "POST",
	        url: "./updatePeriod",
	        data: {
	        	"useFlag" : "Y"
	        	,"periodCode" : periodCode  
	        	,"startDate" : startDate
	        	,"endDate" : endDate
	        	,"contents" : contents
	        },
	        dataType: "json",
	        success: function(data, status) {
	        	console.debug(data);
	        	if (data.status == "success") {
	        		getPeriodList(1);
	        	}
	        	 $("html").css("overflow-y","auto");
	             $(".wrap-rage-del-popup").fadeOut(200);
	        },
	        error: function(xhr, textStatus) {
				alert("오류가 발생했습니다.");
				console.error(error);
	        }
	    });	
		break;
	}
}
function addPeriod(type){
	var lType = type.toLowerCase();
	var year = $("#searchYear");
	var start = $("#period_" + lType + "_start");
	var end = $("#period_" + lType + "_end");
	if(year.val() == null || year.val() ==""){
		alert("연도를 확인하세요");
		year.focus();
		return;
	}
	if(start.val() == null || start.val() ==""){
		alert("시작일을 확인하세요");
		start.focus();
		return;
	}
	if(end.val() == null || end.val() ==""){
		alert("종료일을 확인하세요");
		end.focus();
		return;
	}
	if(parseInt(year.val() + start.val().replace(".",""),10) > parseInt(year.val() + end.val().replace(".",""),10)){
    	alert("시작일은 종료일보다 이전 이어야 합니다.")
    	return;
    }
	//var startDate = year.val() + "-" + start.val().replace(".","-") + " 00:00:00";
	//var endDate = year.val() + "-" + end.val().replace(".","-") + " 23:59:59";
	var startDate = start.val().replace(".","-") + " 00:00:00";
	var endDate = end.val().replace(".","-") + " 23:59:59";
	console.debug(startDate + " ~ " + endDate);
	var title = "";
	switch(type){
	case "A":
		title = year.val() + "년 등록 기간";
		break;
	case "B":
		title = year.val() + "년 확인 기간";
		break;
	case "C":
		title = year.val() + "년 평가 확인 기간";
		break;
	}
	//var contents =  year.val() + "-" + start.val().replace(".","-") + " 부터 " +  year.val() + "-" + end.val().replace(".","-") + " 까지 "  + title + " 입니다";
	var contents =  start.val().replace(".","-") + " 부터 " +  end.val().replace(".","-") + " 까지 "  + title + " 입니다";
	console.debug("title =>"+ title);
	$.ajax({
        type: "POST",
        url: "./addPeriod",
        data: {
        	"type": type
        	,"year" : year.val()
        	,"searchYear" : year.val()
        	,"startDate" : startDate
        	,"endDate" : endDate
        	,"title" : title
        	,"contents" : contents
        },
        dataType: "json",
        success: function(data, status) {
        	console.debug(data);
        	if (data.status == "success") {
        		alert(title + "이 등록되었습니다");
        		getPeriodList(1);
        	}else if(data.status =="fail"){
        		if(data.message == "duplicated"){
        			alert("기존에 등록된 ["+ title +"] 이 있습니다.\n기존정보를 먼저 확인바랍니다.");
        		}
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
			console.error(error);
        }
    });
}
</script>
<%@ include file="../include/footer.jsp" %>