<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../include/admin_header.jsp" %>
<div id="wrap">
    <%@ include file="../include/top.jsp" %>
    <!-- //.header -->


    <div class="container">

        <!-- 탭 메뉴 영역 -->
        <div class="tabmenu-div">
            <ul class="tabmenu-ul">
                <!-- 선택된 탭메뉴 표시 클래스 "active" -->
                <li >
                    <a href="<c:url value="/admin/"/>">이용자 관리</a>
                </li>
                <li class="active">
                    <a href="javascript:void(0)">기준 점수</a>
                </li>
                <li >
                    <a href="<c:url value="/admin/period"/>">기간 설정</a>
                </li>
            </ul>
        </div>
        <!-- //.tabmenu-div -->

        <!-- 검색영역 -->
        <div class="contents-serach-btns-div">
            <div class="contents-search-div">
                <p>
                    <select class="search-div-select">
                        <option value="e" selected>교육</option>
                        <option value="r">연구</option>
                        <option value="s">봉사</option>
                    </select>
                    <button type="button" class="btn-search-name" onclick="javascript:getGradeScoreInfo();"><img src="images/btn-search.png" alt="검색" /></button>
                </p>
            </div>
            <!-- //.contents-search-div -->
        </div>
        <!-- //.contents-serach-btns-div -->

        <!-- 테이블 영역 -->
        <div class="list-table-contents">
            <!-- 교육 -->
            <div class="search-select-div search-div-edu">
                <table class="service-table">
                    <colgroup>
                        <col width="140" />
                        <col width="" />
                        <col width="220" />
                        <col width="120" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <th colspan="2">
                                평가 항목(평가기간 이내)
                            </th>
                            <th>
                                기준 점수
                            </th>
                            <th>
                                가중치
                            </th>
                        </tr>
                        <tr>
                            <td rowspan="9">
                                강의시간
                            </td>
                            <td>
                                강의 시간 수<br />
                                (해당학년도 1년간)
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_point_1"/>
                                <select id="achieve_edu_unit_1"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_weight_point_1"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l">
                                기초세미나
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_point_2"/>
                                <select id="achieve_edu_unit_2"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_weight_point_2"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l">
                                PBL 튜터
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_point_3"/>
                                <select id="achieve_edu_unit_3"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_weight_point_3"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l">
                                주소바탕학습
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_point_4"/>
                                <select id="achieve_edu_unit_4"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_weight_point_4"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l">
                                4학년자유실습,<br />
                                2학년 임상술기학
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_point_5"/>
                                <select id="achieve_edu_unit_5"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_weight_point_5"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l">
                                임상실습 중<br />
                                OSCE/CPX 전담교수
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_point_6"/>
                                <select id="achieve_edu_unit_6"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_weight_point_6"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l">
                                강좌책임/간사교수<br />
                                (해당학년도 1년간)
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_point_7"/>
                                <select id="achieve_edu_unit_7"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_weight_point_7"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l">
                                임상실습 교육책임<br />
                                (해당학년도 1년간)
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_point_8"/>
                                <select id="achieve_edu_unit_8"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_weight_point_8"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l color-red">
                                서브인턴 교육책임<br />
                                (해당학년도 1년간)
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_point_9"/>
                                <select id="achieve_edu_unit_9"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_weight_point_9"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                강의 평가
                            </td>
                            <td>
                                평가점수
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_point_10"/>
                                <select id="achieve_edu_unit_10"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_weight_point_10"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                시험관
                            </td>
                            <td>
                                교내CPX/OSCE, 대경모의시험CPX, 국
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_point_11"/>
                                <select id="achieve_edu_unit_11"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_weight_point_11"/>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="3">
                                문항 및 교육모듈 개발
                            </td>
                            <td>
                                모의고사 학력평가 문제 출제 문항수
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_point_12"/>
                                <select id="achieve_edu_unit_12"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_weight_point_12"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l">
                                PBL 사례 신규개발
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_point_13"/>
                                <select id="achieve_edu_unit_13"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_weight_point_13"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l">
                                의학교육학회 세미나/심포지움 참석,<br />
                                자체의학교육세미나(동.하계)
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_point_14"/>
                                <select id="achieve_edu_unit_14"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_edu_weight_point_14"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- //.service-table -->
            </div>
            <!-- //.search-div-edu -->
            
            <div class="search-select-div search-div-research">
                <!-- 연구 -->
                <table class="service-table">
                    <colgroup>
                        <col width="" />
                        <col width="" />
                        <col width="" />
                        <col width="" />
                        <col width="" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>업적내용</th>
                            <th colspan="2">연구 평가 항목</th>
                            <th>기준 점수</th>
                            <th>가중치</th>
                        </tr>
                        <tr>
                            <td rowspan="5">
                                논문
                            </td>
                            <td rowspan="2">
                                SCI급 논문(SCI, SCIE, SCOPUS, SSCI, AHCI 등)
                            </td>
                            <td>
                                Impact factor(IF) ≥ 1.0, X [(IF/2)+1]
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_research_point_1"/>
                                <select id="achieve_edu_unit_1"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_research_weight_point_1"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l">
                                IF＜1.0
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_research_point_2"/>
                                <select id="achieve_edu_unit_2"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_research_weight_point_2"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="border-l">
                                한국연구재단 등재 및 등재후보 학술지 자연 및 인문사회계열
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_research_point_3"/>
                                <select id="achieve_edu_unit_3"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_research_weight_point_3"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="border-l">
                                기타 국제 학술지 자연 및 인문사회계열
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_research_point_4"/>
                                <select id="achieve_edu_unit_4"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_research_weight_point_4"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="border-l">
                                국내 전국규모 학술지 자연 및 인문사회계열
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_research_point_5"/>
                                <select id="achieve_edu_unit_5"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_research_weight_point_5"/>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="4">
                                저술
                            </td>
                            <td rowspan="2">
                                저서, 교재
                            </td>
                            <td>
                                초판
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_research_point_6"/>
                                <select id="achieve_edu_unit_6"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_research_weight_point_6"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l">
                                개정판
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_research_point_7"/>
                                <select id="achieve_edu_unit_7"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_research_weight_point_7"/>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="2" class="border-l">
                                역서, 편저 등
                            </td>
                            <td>
                                초판
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_research_point_8"/>
                                <select id="achieve_edu_unit_8"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_research_weight_point_8"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l">
                                개정판
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_research_point_9"/>
                                <select id="achieve_edu_unit_9"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_research_weight_point_9"/>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="3">
                                기타연구활동
                            </td>
                            <td colspan="2">
                                등록된 국외특허
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_research_point_10"/>
                                <select id="achieve_edu_unit_10"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_research_weight_point_10"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="border-l">
                                등록된 국내특허
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_research_point_11"/>
                                <select id="achieve_edu_unit_11"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_research_weight_point_11"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="border-l">
                                등록된 소프트웨어
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_research_point_12"/>
                                <select id="achieve_edu_unit_12"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_research_weight_point_12"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                연구비
                            </td>
                            <td colspan="2">
                                간접연구비 기준 100만원 미만은 1점, 그 이후 100만원 당 1점씩 가산
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_research_point_13"/>
                                <select id="achieve_edu_unit_13"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_research_weight_point_13"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- //.service-table -->
            </div>
            <!-- //.search-div-research -->
            
            <!-- 봉사 -->
            <div class="search-select-div search-div-service">
                <p class="tab-content-table-title">
                    [교내 봉사]
                </p>
                <table class="service-table co-tabs-table">
                    <colgroup>
                        <col width="140" />
                        <col width="" />
                        <col width="" />
                        <col width="220" />
                        <col width="120" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>
                                업적내용
                            </th>
                            <th colspan="2">
                                평가항목(평가기간이내)
                            </th>
                            <th>
                                기준점수
                            </th>
                            <th>
                                가중치
                            </th>
                        </tr>
                        <tr>
                            <td rowspan="7">
                                본부보직
                            </td>
                            <td colspan="2" class="text-left">
                                부총장, 처장, 부처장 등<br />
                                책임시수 3시간에 준하는 보직
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_1"/>
                                <select id="achieve_service_unit_1_1"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_1"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-left border-l">
                                입학본부장, 국제교류원장,<br /> 입학본부부본부장, 국제교류원부원장,<br />
                                인재개발원장, 인재개발부원장 등<br /> 책임시수 3시간에 준하는 보직
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_2"/>
                                <select id="achieve_service_unit_1_2"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_2"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-left border-l">
                                신문방송사주간, 정보전산원장,생활관장,<br /> 공동실험실습관장,보건진료소장
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_3"/>
                                <select id="achieve_service_unit_1_3"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_3"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-left border-l">
                                교수회 의장, 부의장, 처장, 부처장
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_4"/>
                                <select id="achieve_service_unit_1_4"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_4"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-left border-l">
                                기숙사 담당(명의관 포함), 부설연구소장
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_5"/>
                                <select id="achieve_service_unit_1_5"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_5"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-left border-l">
                                특별학부장 (자율전공,글로벌인재)
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_6"/>
                                <select id="achieve_service_unit_1_6"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_6"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-left border-l">
                                발전기금 담당 책임자
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_7"/>
                                <select id="achieve_service_unit_1_7"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_7"/>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="6">
                                교내 비보직 봉사-의전원외
                            </td>
                            <td rowspan="3">
                                학생지도
                            </td>
                            <td class="text-left">
                                학생단체(동아리)지도교수
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_8"/>
                                <select id="achieve_service_unit_1_8"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_8"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left border-l">
                                학생상담자원봉사교수
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_9"/>
                                <select id="achieve_service_unit_1_9"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_9"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left border-l">
                                학생관련 특수 프로그램 지도 교수
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_10"/>
                                <select id="achieve_service_unit_1_10"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_10"/>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="3" class="border-l">
                                국제협력봉사
                            </td>
                            <td class="text-left">
                                해외대학/연구소 등과의 학술협력 주도
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_11"/>
                                <select id="achieve_service_unit_1_11"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_11"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left border-l">
                                학생인솔 해외 프로그램 운영
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_12"/>
                                <select id="achieve_service_unit_1_12"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_12"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left border-l">
                                기타 국제협력활동
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_13"/>
                                <select id="achieve_service_unit_1_13"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_13"/>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="3">
                                의전원<br />
                                보직자
                            </td>
                            <td colspan="2" class="text-left">
                                원장, 병원장 등 책임시수 3시간에 준하는 보직.
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_14"/>
                                <select id="achieve_service_unit_1_14"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_14"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-left border-l">
                                부원장(교무,학생,연구,교육) 및 부원장보실장<br />(기획, 입학, 임상수기, 대외협력),<br />
                                기타(BK사업단장 등), 교수회의장
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_15"/>
                                <select id="achieve_service_unit_1_15"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_15"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-left border-l">
                                대학원 학과장 (역학 및 건강증진학과장, 보건관리학과장,<br /> 보건학과장,
                                법정의학과장, 과학수사학과장, 법의간호학과장,<br /> 의학과장, 의과학과장, 의용생체공학과장, 의료정보학과장),<br /> 국가 지정연구센터장, 교수회 부의장, 간사교실주임,<br /> BK사업단부단장/팀장, 학년담임 등
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_16"/>
                                <select id="achieve_service_unit_1_16"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_16"/>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="5">
                                의전원<br />
                                비보직자
                            </td>
                            <td colspan="2" class="text-left">
                                의전원장이 인정한 교내 봉사<br />
                                -회의개최 의전원 위원회 위원에 대한 평점
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_17"/>
                                <select id="achieve_service_unit_1_17"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_17"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-left border-l">
                                의학과장이 인정한 교내 봉사<br />
                                -교내 활동 참여(입학시험면접, 출제 등), 학생지도 활동<br />(학생체육행사, 졸업여행 인솔, MMPI검사, 신입생OT/새터 등),<br /> 학교 행사 참여(신입생 환영회, 국외 대학 교류활동 등)
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_18"/>
                                <select id="achieve_service_unit_1_18"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_18"/>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="3" class="border-l">
                                학교발전기여도
                            </td>
                            <td class="text-left">
                                대학발전기금 및 장학기금(현금 또는) 물품
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_19"/>
                                <select id="achieve_service_unit_1_19"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_19"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left border-l">
                                연구프로젝트 신청건수
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_20"/>
                                <select id="achieve_service_unit_1_20"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_20"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left border-l">
                                대학의 신규 주요사업(국책사업 및 대학종합평가 포함) 계획서 및 보고서 작성)
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_1_21"/>
                                <select id="achieve_service_unit_1_21"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_1_21"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- //.service-table -->

                <p class="tab-content-table-title">
                    [교외 봉사]
                </p>
                <table class="service-table co-tabs-table">
                    <colgroup>
                        <col width="140" />
                        <col width="" />
                        <col width="220" />
                        <col width="120" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>
                                업적내용
                            </th>
                            <th>
                                평가항목(평가기간이내)
                            </th>
                            <th>
                                기준 점수
                            </th>
                            <th>
                                가중치
                            </th>
                        </tr>
                        <tr>
                            <td>
                                학술봉사
                            </td>
                            <td class="text-left">
                                국내외 학회 이사장, 회장, 국제학술대회 조직위원장/이사
                                국외 SCI, SCIE, SSCI, AHCI 등재 학술지의 편집(부)위원장/위원
                                한국연구재단 등재 학술지 편집위원(0.5점)
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_2_22"/>
                                <select id="achieve_service_unit_2_22"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_2_22"/>
                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                            <td class="text-left">
                                연구평가위원(한국연구재단, 보건산업진흥원, 학력평가, MEET, 의학관련 고시위원
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_2_23"/>
                                <select id="achieve_service_unit_2_23"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_2_23"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                의료봉사
                            </td>
                            <td class="text-left">
                                국내외 의료봉사
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_2_24"/>
                                <select id="achieve_service_unit_2_24"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_2_24"/>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="4">
                                기타
                            </td>
                            <td class="text-left">
                                전공 관련 국제기구 위원
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_2_25"/>
                                <select id="achieve_service_unit_2_25"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_2_25"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l text-left">
                                정부기간 (가중치=1), 지자체(가중치=0.4), 국영기업체 및 이에 준하는 기관(가중치=0.2)의 각종 위원
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_2_26"/>
                                <select id="achieve_service_unit_2_26"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_2_26"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l text-left">
                                특별강연 (정부기관, 지자체, 국영기업체 및 이에 준하는 기관이 주관)
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_2_27"/>
                                <select id="achieve_service_unit_2_27"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_2_27"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l text-left">
                                동창회 및 의사회 (부)회장, 상임이사
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_2_28"/>
                                <select id="achieve_service_unit_2_28"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_2_28"/>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="3">
                                수상실적
                            </td>
                            <td class="text-left">
                                전공 관련 국제기구 위원
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_2_29"/>
                                <select id="achieve_service_unit_2_29"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_2_29"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l text-left">
                                특별강연 (정부기관, 지자체, 국영기업체 및 이에 준하는 기관이 주관)
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_2_30"/>
                                <select id="achieve_service_unit_2_30"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_2_30"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="border-l text-left">
                                동창회 및 의사회 (부)회장, 상임이사
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_point_2_31"/>
                                <select id="achieve_service_unit_2_31"/>
                                    <option value="건">건(회)</option>
                                    <option value="년">년</option>
                                    <option value="시간">시간</option>
                                    <option value="분">분</option>
                                </select>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_service_weight_point_2_31"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- //.co-tabs-table -->
            </div>
            <!-- //.search-div-service -->
        </div>
        <!-- //.list-table-contents -->
        
        <!-- 버튼 영역 -->
        <div class="table-bottom-btns-div">
            <a class="btn-cons">저장</a>
            <a class="btn-cancel-cons">취소</a>
        </div>
    </div>
    <!-- //.container -->

    <footer id="footer">
        <div class="footer_con">
            <div class="footer_address">
                <p>
                    경북대학교 의과대학 · 의과전문대학원 ADD. (41944)대구광역시 중구 국채보상로 680
                </p>
                <p>
                    TEL. 053-420-4910(교무), 4906(서무) FAX. 053-422-9195(교무), 420-4925(서무) E-mail. meddean@knu.ac.kr
                </p>
                <p>
                    Copyright(c)2014 경북대학교 의과대학·의과전문대학원 All rights reserved.
                </p>
            </div>
        </div>
    </footer>
    <!-- //.footer -->
</div>
<!-- //#wrap -->

<!-- 이용자 삭제 팝업 -->
<div class="wrap-popup-nonebg wrap-del-popup hidden">
    <div class="popup-guide">
        <div class="popup-guide-header">
            <h2>
                이용자 삭제
            </h2>
            <a class="btn-close-del-popup" title="close">
                X
            </a>
        </div>
        <div class="popup-guide-content">
            <span class="popname"></span>
            <span class="popposition"></span>님을 삭제 하시겠습니까?
            <div class="popup-btns-div">
                <button type="button">확인</button>
                <button type="button" class="btn-close-del-popup">취소</button>
            </div>
        </div>
    </div>
</div>
<!-- //.wrap-popup -->

<script>
$(document).ready(function(){
    // 이름 검색창 체크
    $(".btn-search-name").bind("click", function(){
        var input = $(this).siblings("input[type='text']");
        if ( input.val() == 0 ){
            alert("검색할 이름을 입력해주세요.");
            input.focus();
        }
    });

    // 테이블 라인 배경
    $(".service-table tr:even").css("background","#fdfdfd");

    // 이용자 삭제 팝업
    $(".btn-del").bind("click", function(){
        var tableName = $(this).parent().siblings().children(".tablename").text();
        var tablePosition = $(this).parent().siblings().children(".tableposition").text();
        $(".wrap-del-popup").fadeIn(200);
        $(".popup-guide-content span.popname").text("'" + tableName + "'");
        $(".popup-guide-content span.popposition").text(tablePosition);
    });
    $(".btn-close-del-popup").bind("click", function(){
        $(".wrap-del-popup").fadeOut(200);
    });
    // 교육 , 연구 , 봉사 기준점수 입력표 노출
    $(".search-select-div:nth-of-type(2),.search-select-div:nth-of-type(3)").hide();
    /*
    $(".contents-search-div").on("change","select.search-div-select",function(){
        if ( $(this).children("option:selected").val() == 0 ){
            $(".btn-search-name").bind("click",function(){
                $(".search-select-div").hide();
                $(".search-div-edu").show();
            });
        }
        if ( $(this).children("option:selected").val() == 1 ){
            $(".btn-search-name").bind("click",function(){
                $(".search-select-div").hide();
                $(".search-div-research").show();
            });
        }
        if ( $(this).children("option:selected").val() == 2 ){
            $(".btn-search-name").bind("click",function(){
                $(".search-select-div").hide();
                $(".search-div-service").show();
            });
        }
    });
    */
    $(".service-nav a").removeClass("active");
    $(".service-nav a").eq(0).addClass("active");
    
    getGradeScoreInfo();
});
</script>
<script>
var userLevel = "${sessionScope.userlevel}";
var requestUserCode = "${requestUserCode}";
var userName = "${userName}";
function getGradeScoreInfo(){
	var achieveType = $(".search-div-select").val();
	$(".search-select-div").hide();
	if(achieveType == "e"){
		$(".search-div-edu").show();
	} else if(achieveType == "r"){
		$(".search-div-research").show();
	} else if(achieveType == "s"){
		$(".search-div-service").show();
	}
	var param = {};
	param.achieveType = achieveType;
	
	$.ajax({
        type: "POST",
        url: "./getGradeScoreInfo",
        data: param,
        dataType: "json",
        success: function(data, status) {
        	console.debug(data);
        	if (data.status == "success") {
		        setGradeSscoreInfo(data.result, achieveType);
        	}else{
        		
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
			console.error(error);
        }
    });
}
function setGradeSscoreInfo(result, achieveType){
	if(achieveType == "e"){
		for(var i=0; i < result.length; i++){
			var data = result[i];
			$("#achieve_edu_point_" + data.education_item_num ).val(data.point);
			$("#achieve_edu_unit_" + data.education_item_num ).val(data.unit);
			$("#achieve_edu_weight_point_" + data.education_item_num ).val(data.weight_point);
		}		
	}else if(achieveType == "r"){
		for(var i=0; i < result.length; i++){
			var data = result[i];
			$("#achieve_research_point_" + data.research_item_num ).val(data.point);
			$("#achieve_research_unit_" + data.research_item_num ).val(data.unit);
			$("#achieve_research_weight_point_" + data.research_item_num ).val(data.weight_point);
		}		
	}else if(achieveType == "s"){
		for(var i=0; i < result.length; i++){
			var data = result[i];
			$("#achieve_research_point_" + data.research_item_num ).val(data.point);
			$("#achieve_research_unit_" + data.research_item_num ).val(data.unit);
			$("#achieve_research_weight_point_" + data.research_item_num ).val(data.weight_point);
		}		
	}
}
</script>
</body>
</html>