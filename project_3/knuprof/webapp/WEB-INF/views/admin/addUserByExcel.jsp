<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../include/admin_header.jsp" %>

<div id="wrap">
     <%@ include file="../include/top.jsp" %>
    
    <div class="container">

        <!-- 탭 메뉴 영역 -->
        <div class="tabmenu-div">
            <ul class="tabmenu-ul">
                <!-- 선택된 탭메뉴 표시 클래스 "active" -->
                <li class="active">
                    <a href="<c:url value="/admin/"/>">이용자 관리</a>
                </li>
                <li>
                    <a href="javascript:void(0)">기준 점수</a>
                </li>
                <li>
                    <a href="javascript:void(0)">기간 설정</a>
                </li>
            </ul>
        </div>
        <!-- //.tabmenu-div -->
        
        <!-- 엑셀파일 다운로드 영역 -->
        <div class="btn-down-div">
            <h2>
                해당 되는 <ins>단체등록 양식을 다운로드</ins> 받으신 뒤 내용을 기입하시고,<br />
                <ins>엑셀 파일을 아래 등록</ins>하시면 단체등록이 완료됩니다.
            </h2>
            <div class="btn-execl-down-div">
                <p>
                    교수님 단체등록 양식<br />
                    <a href="<c:url value="/download?path=file&fileName=professor.xlsx"/>" target="_blank">
                        <img src="images/btn-exel-down.png" alt="엑셀다운로드"/>
                    </a>
                </p>
                <p>
                    행정팀 단체등록 양식<br />
                    <a href="<c:url value="/download?path=file&fileName=staff.xlsx"/>" target="_blank">
                        <img src="images/btn-exel-down.png" alt="엑셀다운로드" />
                    </a>
                </p>
            </div>
        </div>
        <!-- //.btn-down-exel-div -->
        
        <!-- 이용자 등록 / 수정 영역 -->
        <div class="signup-div">
            <fieldset>
                <legend>단체 등록 필드</legend>
                <div>
                    <label>구분</label>
                    <p>
                        <!-- 선택표시 클래스 "on" -->
                        <button type="button" class="btn-pro-reg on" onclick="resetForm('2')">교수님</button>
                        <button type="button" class="btn-co-reg"  onclick="resetForm('1')">행정팀</button>
                    </p>
                </div>
                <form id='uploadForm' action='' onSubmit="return false;" enctype='multipart/form-data' method='post'>
                <div>
                    <label>파일</label>
                    <p>
               		
                        <input type="text" class="input-upload-txt" value="" readonly />
                        <label class="btn-input-file">
                            등록
                            
                            <input type="file" class="input-upload" id="uploadInputFile" name="uploadInputFile" value="" />
                            
                        </label>
                        &nbsp;
                        * 파일은 한번에 1개만 등록 가능합니다.
                    </p>
                </div>
                <input type="hidden" id="uploadFileType" name="uploadFileType" value="2"/>
                </form>
            </fieldset>
        </div>
        <!-- //.signup-div -->

        <div class="signup-btn-div">
            <!-- submit button -->
            <a class="btn-signup check-addfile">단체 등록</a>
            
            <!-- cancel : go to before page -->
            <a href="javascript:history.back(-1)" class="btn-cancel">취소</a>
        </div>
        <!-- //.signup-btn-div -->
    </div>
    <!-- //.container -->
  <%@ include file="../include/bottom.jsp" %>
</div>
<!-- //#wrap -->
		
<script type="text/javascript" src="<c:url value="/js/jquery/jquery.form.min.js"/>"></script>
<script>
$(document).ready(function(){
    // 구분 탭 메뉴 스타일
    $(".signup-div button").bind("click", function(){
        $(".signup-div button").removeClass("on");
        $(this).addClass("on");
    });
    // 등록 파일명 표시 & 확장자 검사
    $(".input-upload").each(function(){
        $(this).change(function(){
            if( $(this).val() != "" ){
                var value = $(this).val();
                var chk = $(this).val().split('.').pop().toLowerCase();
                  if($.inArray(chk, ["xlsx","xlsm","xlsb","xltx","xls","xlt","xml","xlam","xla","xlw"]) == -1){
                    alert("엑셀 파일만 등록해주세요.");
                    $(this).val("");
                    return true;
              } else {
                  $(this).parent().siblings("input[type='text']").val(value);
              }
            }
        });
    });
    // 단체등록시 파일첨부 검사
    $(".check-addfile").bind("click", function(){
        var addFile = $(".input-upload").val();
        if ( addFile == 0 ){
            alert("엑셀 파일을 등록해주세요.");
            return true;
        }
        $("#uploadForm").ajaxForm({
        	type: "POST",
    	    url:  '<c:url value="/user/userDataUpload"/>',
   			async: true,
    		beforeSend : function(){
    			console.log("beforeSubmit");
    		},
    		success : function(json){
    			console.debug(json);
    	        if(json == undefined && json == null){
    	       		alert("오류가 발생 하였습니다.");
    	       		return;
    	        }
    	        var json = $.parseJSON(json);
    	        if(json.status == "fail"){
    	        	alert(json.message);
    	       	 	return;
    	        }
    	        if(json.uploadFileType == 2){
    	        	alert("총 " + json.count + "명의 교수님이 등록되었습니다");	
    	        }else{
    	        	alert("총 " + json.count + "명의 행정팀 직원이 등록되었습니다");	
    	        }
    	        
    	        
    	        history.back(-1);
   			},
    		error : function(data){
    			console.log('error');
    			alert("오류가 발생 하였습니다.");
    		}
    	}).submit();
    	
    });
    $(".service-nav a").eq(0).addClass("active");
});
function resetForm(userType){
	$('#uploadFileType').val(userType);
	$('#uploadInputFile').val('');
	$(".input-upload-txt").val("");
}
function excelDownload(fileName){
	
}
</script>
<%@ include file="../include/footer.jsp" %>