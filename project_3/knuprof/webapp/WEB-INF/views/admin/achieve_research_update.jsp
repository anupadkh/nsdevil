<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../include/admin_header.jsp" %>
<div id="wrap">
    <%@ include file="../include/top.jsp" %>
    <!-- //.header -->

    <div class="container-table">
         <div class="aside">
            <div class="contents-serach-btns-div">
                <input type="text" value="" placeholder="이름" />
                <button type="button" class="btn-search-name"><img src="images/btn-search.png" alt="검색" /></button>
            </div>
            <!-- //.contents-serach-btns-div -->
            
            <div class="count-div">
            	<p class="stay">
                    <span>전체</span>
                    <span>
                        <a href="javascript:getAchieveRegistUserList('');" id="registTotalProfessor" class="btn-done-cnt active"></a>
                    </span>
                </p>
                <p class="">
                    <span>미등록</span>
                    <span>
                        <a href="javascript:getAchieveRegistUserList('N');" id="unregistProfessor" class="btn-done-cnt"></a>
                    </span>
                </p>
                <p>
                    <span>등록완료</span>
                    <span>
                        <a href="javascript:getAchieveRegistUserList('Y');" id="registProfessor"  class="btn-done-cnt"></a>
                    </span>
                </p>
            </div>
            <!-- //.count-div -->
            
            <!-- 교수 리스트 -->
            <ul class="list-pro">
            	<!-- 
                <li class="view-all-pro">
                    <a href="javascript:void(0)">전체 확인</a>
                </li>
                 -->
                <li id="professorListLayer"></li>
            </ul>
        </div>
        <!-- //.aside -->
        

        <div class="contents" id="contents">
            <a class="btn-coll"><span>좌측메뉴 영역 접기/펴기 토글</span></a>

            <div class="contents-serach-btns-div">
                <p class="contents-title-name" id="contents-title-name">
                    <!-- <span class="name-pro">정동일</span><span>기생충학</span> -->
                </p>
                <div class="contents-btns-div">
                    <a href="javascript:toggleExcelRegistLayer(true)">엑셀파일 일괄 등록하기 (전체 교수님)</a>
                </div>
            </div>
            <!-- //.contents-serach-btns-div -->

            <!-- 테이블 영역 -->
            <div class="list-table-contents">
           		<table class="service-table co-tabs-table autoheight">
                    <colgroup>
                        <col width="80" />
                        <col width="100" />
                        <col width="180" />
                        <col width="80" />
                        <col width="150" />
                        <col width="100" />
                        <col width="" />
                        <col width="" />
                        <col width="50" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <th>
                                업적내용
                            </th>
                            <th colspan="2">
                                연구 평가 항목
                            </th>
                            <th>
                                기준점수
                            </th>
                            <th>
                                비고
                            </th>
                            <th>
                                건수
                            </th>
                            <th>
                                내용
                            </th>
                            <th>
                                증빙서류
                            </th>
                            <th>
                                점수
                            </th>
                        </tr>
                        
                        <tr class="auth_category_re01">
                            <td rowspan="5">
                                논문
                            </td>
                            <td rowspan="2">
                                SCI급 논문(SCI, SCIE, SCOPUS, SSCI, AHCI 등)
                            </td>
                            <td class="text-left">
                                Impact factor(IF) ≥ 1.0, X<br />[(IF/2)+1]
                            </td>
                            <td>
                                30점
                            </td>
                            <td class="text-left">
                                &lt;주 3> 참조
                            </td>
                            <td class="score-input-td">
                                <input type="text" value="" id="achieve_re_unit_1"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="논문 명"  id="achieve_re_contents_1"  ></textarea>
                            </td>
                            <td class="service-table-up-td" id="achieve_file_1">
                            	<!--
                                <p>
                                    <span class="text-bold"><a>3건</a></span>
                                </p>
                                 <a class="btn-down">다운로드</a>
                                <button type="button" class="btn-service-popup">등록</button>
                                -->
								<button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 1);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td id="achieve_re_point_1">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_re01">
                            <td class="border-l text-left">
                                IF＜1.0
                            </td>
                            <td>
                                30점
                            </td>
                            <td class="text-left">
                                &lt;주 4> 참조
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_2"/>
                            </td>
                            <td class="textarea-td autoheight">
                                <textarea cols="20" rows="1"  id="achieve_re_contents_2"></textarea>
                            </td>
                            <td class="service-table-up-td"  id="achieve_file_2">
                            	<button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 2);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td  id="achieve_re_point_2">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_re01">
                            <td colspan="2" class="border-l text-left">
                                한국연구재단 등재 및 등재후보 학술지 자연 및 인문사회계열
                            </td>
                            <td>
                                15점
                            </td>
                            <td>
                                
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_3"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1"  id="achieve_re_contents_3"></textarea>
                            </td>
                            <td class="service-table-up-td"  id="achieve_file_3">
                            	<button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 3);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td id="achieve_re_point_3">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_re01">
                            <td colspan="2" class="border-l text-left">
                                기타 국제 학술지 자연 및 인문사회계열
                            </td>
                            <td>
                                10점
                            </td>
                            <td>

                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_4"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1"  id="achieve_re_contents_4"></textarea>
                            </td>
                            <td class="service-table-up-td"  id="achieve_file_4">
                            	<button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 4);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td id="achieve_re_point_4">

                            </td>
                        </tr>
                        <tr class="auth_category_re01">
                            <td colspan="2" class="border-l text-left">
                                국내 전국규모 학술지 자연 및 인문사회계열
                            </td>
                            <td>
                                5점
                            </td>
                            <td>

                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_5"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1"  id="achieve_re_contents_5"></textarea>
                            </td>
                            <td class="service-table-up-td"  id="achieve_file_5">
                            	<button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 5);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td id="achieve_re_point_5">

                            </td>
                        </tr>
                        <tr class="auth_category_re02">
                            <td rowspan="4">
                                저술
                            </td>
                            <td rowspan="2">
                                저서, 교재
                            </td>
                            <td class="text-left">
                                초판
                            </td>
                            <td>
                                30점
                            </td>
                            <td rowspan="2" class="text-left">
                                국내는 가중치 0.5
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_6"/>
                            </td>
                            <td rowspan="2" class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="저서 명" id="achieve_re_contents_6"></textarea>
                            </td>
                            <td rowspan="2" class="service-table-up-td" id="achieve_file_6">
                            	<button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 6);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td id="achieve_re_point_6">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_re02">
                            <td class="text-left border-l">
                                개정판
                            </td>
                            <td>
                                10점
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_7"/>
                                 <textarea cols="1" rows="1" placeholder="unit7" id="achieve_re_contents_7" style="display:none" readonly></textarea>
                            </td>
                            <td id="achieve_re_point_7">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_re02">
                            <td rowspan="2" class="border-l">
                                역서, 편저 등
                            </td>
                            <td class="text-left">
                                초판
                            </td>
                            <td>
                                10점
                            </td>
                            <td rowspan="2">

                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_8"/>
                            </td>
                            <td rowspan="2" class="textarea-td">
                                <textarea cols="20" rows="1"  id="achieve_re_contents_8"></textarea>
                            </td>
                            <td rowspan="2" class="service-table-up-td" id="achieve_file_8">
                            	<button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 8);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td id="achieve_re_point_8">

                            </td>
                        </tr>
                        <tr class="auth_category_re02">
                            <td class="text-left border-l">
                                개정판
                            </td>
                            <td>
                                5점
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_9"/>
                                <textarea cols="1" rows="1" placeholder="unit9" id="achieve_re_contents_9" style="display:none" readonly></textarea>
                            </td>
                            <td id="achieve_re_point_9">

                            </td>
                        </tr>
                        <tr class="auth_category_re03">
                            <td>
                                기타
                            </td>
                            <td colspan="2" class="text-left">
                                등록된 국외특허
                            </td>
                            <td>
                                10점
                            </td>
                            <td rowspan="3" class="text-left">
                                기타 연구활동 총합이 20점을 초과하지 못함.<br />
                                <span class="color-red">등록일 기준, 등록증 제출함.</span>
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_10"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="특허번호, 특허명 기입" id="achieve_re_contents_10"></textarea>
                            </td>
                            <td class="service-table-up-td"  id="achieve_file_10">
                                <button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 10);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td  id="achieve_re_point_10">
                                
                            </td>
                        </tr>
                        <tr class="auth_category_re04">
                            <td>
                                연구
                            </td>
                            <td colspan="2" class="text-left">
                                등록된 국내특허
                            </td>
                            <td>
                                5점
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_11"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1"  id="achieve_re_contents_11"></textarea>
                            </td>
                            <td class="service-table-up-td"  id="achieve_file_11">
                                <button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 11);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td  id="achieve_re_point_11">

                            </td>
                        </tr>
                        <tr class="auth_category_re05">
                            <td>
                                활동
                            </td>
                            <td colspan="2" class="text-left">
                                등록된 소프트웨어
                            </td>
                            <td>
                                5점
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_12"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1"  id="achieve_re_contents_12"></textarea>
                            </td>
                            <td class="service-table-up-td"  id="achieve_file_12">
                                <button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 12);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td id="achieve_re_point_12">
								
                            </td>
                        </tr>
                        <tr class="auth_category_re06">
                            <td>
                                연구비
                            </td>
                            <td colspan="2" class="text-left">
                                간접연구비 기준 100만원 미만은 1점, 그 이후 100만원당 1점씩 가산
                            </td>
                            <td>
                                -
                            </td>
                            <td class="text-left">
                                간접연구비가 의학전문대학원으로 배정된 것
                            </td>
                            <td class="score-input-td">
                                <input type="text" value=""  id="achieve_re_unit_13"/>
                            </td>
                            <td class="textarea-td">
                                <textarea cols="20" rows="1" placeholder="연구명" id="achieve_re_contents_13"></textarea>
                            </td>
                            <td class="service-table-up-td"  id="achieve_file_13">
                                <button type="button" class="btn-service-popup hidden" onClick="javascript:uploadFile($(this), 13);">등록</button>
                            	<div class="fileCountDiv"></div>
                            </td>
                            <td  id="achieve_re_point_13">
                                
                            </td>
                        </tr>
                        <tr class="total-score-tr">
                            <td colspan="4">
                                총점
                            </td>
                            <td colspan="5" id="totalResultPoint">
                                
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                
            	<div class="guide-txt-table-div">
                    <p>
                        &lt;주&gt;
                    </p>
                    <p>
                        1. 통합정보시스템(yes.knu.ac.kr)에 등록/승인된 연구업적에 한하여 인정한다. 제 1저자, 교신저자, SCI급 여부 및 발간일의 연월일 까지 정확하게 입력하여야 한다. 미승인으로 인한 불이익을 방지하기 위하여 수시입력/승인요청을 권장한다. 중복 입력된 업적이 없도록 확인한다.
                    </p>
                    <p>
                        2. 공동저자 인정기준은 다음과 같다.
                    </p>
                    <table class="service-table">
                        <caption style="display:table-caption;">
                            &lt;발표2&gt; 연구실적을 공(동)저자 인정 기준표
                        </caption>
                        <colgroup>
                            <col />
                            <col />
                            <col />
                            <col />
                            <col width="70" />
                        </colgroup>
                        <tbody>
                            <tr>
                                <th rowspan="2">
                                    총저자수<br />
                                    (n)
                                </th>
                                <th colspan="3">
                                    인정비율(%)
                                </th>
                                <th rowspan="2">
                                    비고
                                </th>
                            </tr>
                            <tr>
                                <th class="border-l">
                                    주저자 (제1저자, 교신저자)
                                </th>
                                <th>
                                    공동저자
                                </th>
                                <th>
                                    공저자
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    1인
                                </td>
                                <td>
                                    100%
                                </td>
                                <td>
                                    -
                                </td>
                                <td>

                                </td>
                                <td>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    2인 이상
                                </td>
                                <td>
                                    {200/(n+1)}%
                                </td>
                                <td>
                                    {100/(n+1)}%
                                </td>
                                <td>
                                    {100/(n+10)}%
                                </td>
                                <td>

                                </td>
                            </tr>
                        </tbody>
                    </table>

                    ※ 논문과 저술영역에서 주저자인 경우에는 총저자수가 4인 이상이라도 n을 4로 계산함.<br />

                    ※ 공동저자는 주저자의 표기가 있는 경우에 나머지 저자를 일컫는다.<br />

                    ※ 주저자의 표기가 없는 연구실적물의 저자는 공저자로 간주한다.

                    <p>
                        3. Impact factor(IF) 기준은 업적자료제출마감일 기준으로 산출 한다. SCI급 IF가 1.0 이상인 잡지에 출판된 논문의 경우 다음과 같이 계산한다. 
                        점수 = 기준점수 x (&lt;별표2&gt;의 공동저자 인정 비율) x [(IF/2)+1]
                    </p>
                    <p>
                        4. 이외의 SCI급(IF가 1.0 미만인 잡지에 출판된) 논문, 저술, 학술발표, 기타 연구 활동의 경우 다음과 같이 계산한다.점수 = 기준점수 * (&lt;별표2&gt;의 공동저자 인정 비율)로 계산한다.
                    </p>
                    <p>
                        5. 연구비 평가항목에서 간접연구비 계산식은 1+(A/100만원)=B이며, 이 경우 A는 개인 간접연구비, B의 소수점 이하는 0으로 처리한다.
                    </p>
                    <p>
                        6. 연구비 평가항목에서 기간 중에 의학전문대학원으로 들어온 간접연구비 총액을 기준으로 평가하고, 공동연구과제는 분담액으로 하되, 분담액을 정하지 않은 경우는 연구 보고서를 기준으로 &lt;위 별표의 공동저자 인정비율&gt;에 따라 산정한 금액으로 한다.
                    </p>
                    <p class="color-red">
                        7. 특허는 등록일 기준으로 하며, 산·단 승인한 자료만 평가대상으로 인정한다. 참고로 산·단 출원 특허는 자동으로 승인절차가 진행되지만 타 기관을 통한 특허는 반드시 산단에 등록하고 승인절차를 받아야 한다. 등록증을 증빙자료로 제출함.
                    </p>
                    <p class="color-red">
                        8. 특허의 인정비율은 YES 시스템상으로 확인할 수 없어 등록인원으로 인정비율을 정하되 연구실적물 주저자 기준으로 산출한다.
                    </p>
                    <p class="color-red">
                        9. 저술활동의 업적의 인정비율은 연구실적물 주저자 기준으로 산출한다.
                    </p>
                </div>
                <!-- //.guide-txt-table-div -->
            </div>
            <!-- //.list-table-contents-->
            <!-- 등록 버튼 영역-->
            <div class="signup-btn-div text-right">
                <a class="btn-signup achieveDataSubmit" href="javascript:updateAchieveData();">등록</a>
                <a  href="javascript:emptyAchieveData();" class="btn-cancel">취소</a>
            </div>
            
            <!-- //.signup-btn-div -->
        </div>
        <!-- //.contents -->
    </div>
    <!-- //.container-table -->

    <footer id="footer">
        <div class="footer_con">
            <div class="footer_address">
                <p>
                    경북대학교 의과대학 · 의과전문대학원 ADD. (41944)대구광역시 중구 국채보상로 680
                </p>
                <p>
                    TEL. 053-420-4910(교무), 4906(서무) FAX. 053-422-9195(교무), 420-4925(서무) E-mail. meddean@knu.ac.kr
                </p>
                <p>
                    Copyright(c)2014 경북대학교 의과대학·의과전문대학원 All rights reserved.
                </p>
            </div>
        </div>
    </footer>
    <!-- //#footer -->
</div>
<!-- //#wrap -->

    <!-- 증빙서류 등록 팝업 영역 -->
    <div class="wrap-popup wrap-plan-popup hidden popup-thesis">
    	<input type="hidden" name="uploadServiceCode" />
        <div class="popup-guide">
            <div class="popup-guide-header">
                <h2>
                    증빙서류
                </h2>
                <a class="btn-close-giude-popup" title="close">
                    X
                </a>
            </div>
            <div class="popup-guide-content">
                <fieldset>
                    <div class="popup-input-file-up-div">
                        <p class="popup-fd-title">
                            파일 추가를 누르시면 파일을 여러 개 등록할 수 있습니다.
                        </p>
                        <button type="button" class="btn-add-file addFormButton" onclick="javascript:addFileForm();">
                            + 파일 추가
                        </button>
                    </div>
                    <!-- //.popup-input-file-up-div -->

                    <div class="popup-input-file-up-div">
                        <!-- <p class="popup-fd-title">
                            <span class="color-blue">
                                (<span class="word-cut">
                                SCI급 논문
                                </span>)
                            </span> 증빙서류 첨부내역
                        </p>
                         -->
                        <div id="uploadFileList"></div>
                        <div class="btn-submit-div">
                            <a>완료</a>
                        </div>
                    </div>
                    <!-- //.popup-input-file-up-div -->
                </fieldset>
            </div>
        </div>
    </div>
<!-- //.wrap-popup -->
<div id="permissionhiddenLayer" class="hidden">

        <div class="date_not">
            <p><img src="images/not.png"></p>
            <p class="date_memo_title">
                <span id="categoryName">연구</span> 등록 권한이 없습니다.<br />
                관리자에게 문의해 주세요.
            <p>
            <p class="date_memo_text"></p>
            <p class="red_bt"><a href="javascript:void(0)">확인</a></p>
        </div>
</div>


<div id="toggleExcelRegistLayer" style="display:none;">
			<div class="btn-down-div">
                <h2>
                    해당 되는 <ins>단체등록 양식을 다운로드</ins> 받으신 뒤 내용을 기입하시고,<br />
                    <ins>엑셀 파일을 아래 등록</ins>하시면 모든 교수님의 연구등록이 일괄 완료됩니다.
                </h2>
                <div class="btn-execl-down-div">
                    <p>
                        연구 일괄 등록양식<br />
						<a href="<c:url value="/download?path=file&fileName=achieve_research.xlsx"/>" target="_blank">
                            <img src="images/btn-exel-down.png" alt="엑셀다운로드" />
                        </a>
                    </p>
                </div>
            </div>
            <!-- //.btn-down-exel-div -->

            <!-- 이용자 등록 / 수정 영역 -->
            <form id='uploadForm' action='' onSubmit="return false;" enctype='multipart/form-data' method='post'>
            <div class="signup-div">
                <fieldset>
                    <legend>연구 일괄 등록 필드</legend>
                    <div>
                        <label>파일</label>
                        <p>
                            <input type="text" class="input-upload-txt" value="" readonly />
                            <label class="btn-input-file">
                                등록
                                <input type="file" class="input-upload" id="uploadInputFile" name="uploadInputFile" value="" />
                            </label>
                        </p>
                    </div>
                </fieldset>
            </div>
            </form>
            <!-- //.signup-div -->

            <div class="signup-btn-div" style="display:block">
                <a class="btn-signup check-addfile" href="javascript:uploadExcelFile();">연구 일괄 등록</a>
                <a href="javascript:toggleExcelRegistLayer(false)" class="btn-cancel">취소</a>
            </div>
            <!-- //.signup-btn-div -->
	</div>

<script type="text/javascript" src="<c:url value="/js/jquery/jquery.form.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/achieve_service_research.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery/jquery.blockUI.js"/>"></script>
<script src="js/datetimep.js"></script>
<script>
var contentsHtml = $(".contents").html();
var userLevel = "${sessionScope.userlevel}";
var requestUserCode = "${requestUserCode}";
var userName = "${userName}";
var subject = "${subject}";
var category = "${category}";
var achievePointInfo = null;
var authInfo = null;
var isReady = false;
var periodDisableMode = null;
$(document).ready(function(){
	   // 이용자 이름 검색값 검사
 $(".btn-search-name").bind("click", function(){
     var input = $(this).siblings("input[type='text']");
     if ( input.val() == 0 ){
         alert("검색할 이름을 입력해주세요.");
         input.focus().val("");
     }
     findProfessor(input.val().trim());
 });
 $(".contents-serach-btns-div input").keydown(function (key) {
     if(key.keyCode == 13){//키가 13이면 실행 (엔터는 13)
     	 $(".btn-search-name").click();
     }
 });
 
 $(".btn-close-giude-popup").bind("click", function(){
     $(".wrap-plan-popup").fadeOut(300);
     if(requestUserCode != null && requestUserCode > 0){
    	 getFileCountByResearchCode(requestUserCode);
     }
 });
 
 $(".btn-submit-div a").bind("click", function(){
     $(".wrap-plan-popup").fadeOut(300);
     //getServiceResult();
     if(requestUserCode != null && requestUserCode > 0){
    	 getFileCountByResearchCode(requestUserCode);
     }
 });
 
 
 // 파일 등록폼 추가
 $(".wrap-popup").on("click",".addFormButton", function(){
 	//console.debug("등록폼추가``");
     //addFileForm();
 });
 
 //$(".btn-service-popup").hide();
 initalize();
});
function initalize(){
	// 좌측메뉴 펼침 / 접기 토글 스크림트
    $(".btn-coll").bind("click", function(){
        $(".btn-coll").toggleClass("move-trigger");
        $(".aside").toggleClass("hidden-important");
        $(".contents").toggleClass("width-100");
    });
    //탭메뉴
    $(".tab-content").hide();
    $(".tab-content:first").show();
    $(".tabs li").bind("click", function(){
        $(".tabs li").removeClass("active");
        $(this).addClass("active");
        $(".tab-content").hide();
        var activeTab = $(this).attr("data-to");
        $("." + activeTab).show();
    });
   
    // 미등록, 등록완료 숫자 버튼
    $(".aside .count-div p span a").each(function(){
        $(".aside .count-div p span a").bind("click",function(){
            var btnCount = $(".aside .count-div p span a");
            $(btnCount).removeClass("active");
            $(this).addClass("active");
        });
    });
    // textarea 자동 높이 맞춤
    $(".autoheight").on("keyup", "textarea", function (e){
        $(this).css("height", "auto");
        $(this).height( this.scrollHeight );
    });
    $(".autoheight").find("textarea").keyup();
    // textarea 자동 포커스
    $(".autoheight td").click(function (){
        $(this).children("textarea").focus();
    });
    
    $('.btn-monthpick').bind('click',function(){
        $(this).siblings('input').datetimepicker('show');
    });

    $("textarea[id='achieve_re_contents_6']").keyup(function(event){
    	$("#achieve_re_contents_7").val($(this).val());
    });
	$("textarea[id='achieve_re_contents_8']").keyup(function(event){
		$("#achieve_re_contents_9").val($(this).val());
    });
	
    $("input[id^='achieve_re_unit_']").blur(function(event){
 		var point = $(this).val();
 		console.debug("unit blur~ =>"+ point  + " / " + isNaN(point));;
 		if(isNaN(point) || point.length == 0){
 			$(this).val("");
 		}
 		var itemNum = $(this).attr("id").replace("achieve_re_unit_","");
		var calculatedPoint = calculatePoint(itemNum,$(this).val());
		console.debug("calculatedPoint   =>"+ calculatedPoint + " _ itemNum =>"+ itemNum);
		$("td[id='achieve_re_point_" + itemNum + "']").html(ToFloat(calculatedPoint));
		
		
		if(itemNum == 6 || itemNum == 7){
			var calculatedPoint_6 = calculatePoint(6,$("input[id='achieve_re_unit_6']").val());
			var calculatedPoint_7 = calculatePoint(7,$("input[id='achieve_re_unit_7']").val());
			var sum = parseFloat(calculatedPoint_6) + parseFloat(calculatedPoint_7);
			 $("td[id='achieve_re_point_6']").html(ToFloat(sum));
		}
		if(itemNum == 8 || itemNum == 9){
			var calculatedPoint_8 = calculatePoint(8,$("input[id='achieve_re_unit_8']").val());
			var calculatedPoint_9 = calculatePoint(9,$("input[id='achieve_re_unit_9']").val());
			var sum = parseFloat(calculatedPoint_8) + parseFloat(calculatedPoint_9);
			 $("td[id='achieve_re_point_8']").html(ToFloat(sum));
		}
 		calculateTotalPoint();
 	});
        
    $(".service-nav a").removeClass("active");
    switch(category){
    case "e" :
    	$(".service-nav a").eq(2).addClass("active");
    	$("#categoryName").html("교육");
    	break;
    case "r" :
    	$(".service-nav a").eq(3).addClass("active");
    	$("#categoryName").html("연구");
    	break;
    case "s" : 
    	$(".service-nav a").eq(4).addClass("active");
    	$("#categoryName").html("봉사");
    	break;
	}
    
    
    $("#contents-title-name").html("<span class=\"name-pro\">" + userName + "</span><span>" + subject +"</span>");
    
	//getServiceResult();
    

    if(userLevel == 1){
		//$("#contents").html($("#permissionhiddenLayer").html());
    	
    	$(".total-score-tr").hide();
		$("#totalPointTitleLayer").hide();
    	$("#totalPointLayer").hide();
    	
		$("#service1List_totalPoint").hide();
		
    	
    	$("[class^='auth_category_']").hide();
    	
    	try{
    		authInfo = ${json.authInfo};	
    	}catch(e){
    		console.error("authInfo is null or empty");
    	}
    	
    	if(authInfo != null){
    		for(var i=0; i < authInfo.length; i++){
    			console.debug(authInfo[i]);
    			var categoryId = authInfo[i].categoryid;
    			$("[class^='auth_category_"+ categoryId +"']").show();
    		}
    	}else{
    		$("#contents").html($("#permissionhiddenLayer").html());
    	}
    	
    }
    
    try{
    	achievePointInfo = ${json.achievePointInfo};
    	console.debug("====================achievePointInfo");
    	console.debug(achievePointInfo);
    }catch(e){
    	console.error("achievePointInfo is null or empty");
    }
    
    hideUnPermissionLayer();
    if(!isReady){
    	getProfessorList();
    	isReady = true;
    }
    $("[id^='achieve_re_unit_']").attr("readonly",true);
	$("[id^='achieve_re_contents_']").attr("readonly",true);
	
	if(periodDisableMode != null && periodDisableMode == "C"){
	  	$(".contents-btns-div").remove();
	   	$(".signup-btn-div").remove();
	}
}
function hideUnPermissionLayer(){
	if(userLevel == 0){
		return;
	}
	if(authInfo != null){
		var isFind = false;
		for(var i=0; i < authInfo.length; i++){
			console.debug(authInfo[i]);
			var categoryCode = authInfo[i].categorycode;
			console.debug("===> categorycode :"+ categoryCode);
			if(categoryCode == "re"){
				isFind =true;
			}
		}
		if(!isFind){
			$("#contents").html($("#permissionhiddenLayer").html());	
		}
	}
	if(authInfo != null){
		var cnt = 0;
		for(var i=0; i < authInfo.length; i++){
			console.debug(authInfo[i]);
			var categoryId = authInfo[i].categoryid;
			$("[class^='auth_category_"+ categoryId +"']").show();
		}
	}else{
		$("#contents").html($("#permissionhiddenLayer").html());
	}
}
function getPeriodInfo() {
	function getToday(){
        var date = new Date();
        var year  = date.getFullYear();
        var month = date.getMonth() + 1; // 0부터 시작하므로 1더함 더함
        var day   = date.getDate();
    
        if (("" + month).length == 1) { month = "0" + month; }
        if (("" + day).length   == 1) { day   = "0" + day;   }
		return ("" + year + month + day)	       
    }
	function getDate(str){
		str = str.toString();
		return str.substring(0,4) + "-"+ str.substring(4,6) + "-"+str.substring(6,9);
	}
	if(userLevel == 0){
		console.debug("관리자 ");
		return;
	}else{
		$.ajax({
	        type: "POST",
	        url: '<c:url value="/pro/getPeriodInfo"/>',
	        data: {},
	        dataType: "json",
	        success: function(data, status) {
	        	console.debug("getPeriodInfo ============");
	        	console.debug(data);
	        	if (data.status == "success") {
	        		if(data.periodInfo == undefined || data.periodInfo == null){
	        			//alert("조회기간 정보를 찾을 수 없습니다.\n 관리자에게 문의하세요");
	        			blockResultData();
	        			return;
	        		}
	        		var startDate_a = 0;
	        		var endDate_a = 0;
	        		var startDate_b = 0;
	        		var endDate_b = 0;
	        		var startDate_c = 0;
	        		var endDate_c = 0;
	        		for(var i=0; i < data.periodInfo.length; i++){
	        			var period = data.periodInfo[i];
	        			if(period.period_type == "A"){
	        				startDate_a = parseInt(period.start_date,10);
	        				endDate_a = parseInt(period.end_date,10);
	        			}
	        			if(period.period_type == "B"){
	        				startDate_b = parseInt(period.start_date,10);
	        				endDate_b = parseInt(period.end_date,10);
	        			}
	        			if(period.period_type == "C"){
	        				startDate_c = parseInt(period.start_date,10);
	        				endDate_c = parseInt(period.end_date,10);
	        			}
	        		}
	        		if(startDate_a == 0 || startDate_a ==0){
	        			//alert("조회기간 정보를 찾을 수 없습니다.\n 관리자에게 문의하세요");
	        			//blockResultData();
	        			return;
	        		}
	        		
	        		var rightNow = new Date();
	        		var today = getToday(); //rightNow.toLocaleString().slice(0,10).replace(/-/g,"");
	        		today = parseInt(today,10);
	        		console.log(today);
	        		console.log("a:"+ startDate_a + " / " + endDate_a);
	        		console.log("b:"+ startDate_b + " / " + endDate_b);
	        		console.log("c:"+ startDate_c + " / " + endDate_c);
	        		if(endDate_a >= today){
	        			console.debug("등록기간에 걸림");
	        			//periodDisableMode="A";
	        			//var msg = getDate(startDate_a)+ " 부터" + getDate(endDate_a) + "까지는 교수님 봉사 등록 기간입니다\n";
	        			//msg += "행정팀은 조회공시기간 " + getDate(startDate_b) + " 부터 "+ getDate(endDate_b) + " 까지 수정가능합니다";
	        			//alert(msg);
	        			//return;
	        		}
	        		if(today >= startDate_c && today <= endDate_c){
	        			console.debug("확정조회공시 기간에 걸림");
	        			disableElements();
	        			periodDisableMode="C";
	        			var msg = getDate(startDate_c)+ " 부터" + getDate(endDate_c) + "까지는 교수님 최종 조회 기간입니다\n";
	        			msg += "조회만 가능합니다";
	        			alert(msg);
	        			return;
	        		}
	        	} else {
	        		alert("오류가 발생했습니다.");
	        	}
	        },
	        error: function(xhr, textStatus) {
				alert("오류가 발생했습니다.");
	        }
	    });
	}
}
function disableElements(){
	/*
	$("button").each(function(index){
		if($(this).attr("class") == "btn-service-popup"){
			//$(this).attr("onclick","javascript:disableButtons(true)");
		}else{
			$("button").attr("disabled",true);		
		}
	});
	*/
	$("#contents button").attr("disabled",true);	
	$("#contents input").attr("disabled",true);
	$("#contents select").attr("disabled",true);
	$("#contents textarea").attr("disabled",true);
	$(".signup-btn-div").hide();
	$(".contents-btns-div").hide();
	
	$("#contents input").css("border","0px");
	$("#contents textarea").css("border","0px");
}
function calculateTotalPoint(){
	var totalPoint = 0.0;
	$("td[id^=achieve_re_point_]").each(function(e){
		var point = $.trim($(this).html());
		if(point.length > 0){
    		if(!isNaN(point)){
				totalPoint = parseFloat(totalPoint) + parseFloat(point);
				console.debug("totalPoint =>"+ totalPoint );
    		}			
		}
	});
	console.debug("totalPoint =>"+ totalPoint);
	//totalPoint = parseFloat(totalPoint).toFixed(2);
	$("#totalResultPoint").html(ToFloat(totalPoint.toFixed(4)) + " 점");
}
function calculatePoint(itemNum, point){
	console.debug("==========================calculatePoint " + itemNum + "  / " + point);
	var resultPoint = 0;
	console.debug(achievePointInfo);
	for(var i=0; i < achievePointInfo.length;i++){
		var info = achievePointInfo[i];
		console.debug("info.research_item_num =>"+ info.research_item_num);
		if(itemNum == info.research_item_num){
			resultPoint = point * info.point;
		}
	}
	return resultPoint;
}
function getProfessorList(){
	$.ajax({
        type: "POST"
        ,url: "./getProfessorList"
        ,data: { "achieveType": category}
        ,dataType: "json"
        ,async: true
        ,success: function(json){
           console.debug(json);
           if(json != undefined && json != null){
        	   if(json.status == "success"){
        		   if(json.list != undefined && json.list != null){
        			   $("#professorListLayer").empty();
        			   for(var i=0; i <json.list.length; i++){
        				   var addClass = "";
        				   var addClass = json.list[i].registered == "Y" ? "regist" : "unregist";
        				   //console.debug("[" + requestUserCode + "]:"+ json.list[i].usercode + "|"+ json.list[i].username + "|" + json.list[i].registered);
        				   if(json.list[i].usercode == requestUserCode){
        					   addClass = addClass + " on";
        				   }        				           				   
        				   var html = "";
        				   html += "<li id=\"professor_"+ json.list[i].usercode +"\" class=\""+ addClass + "\">";
        				   html +=     "<a href=\"javascript:getProfessorDetailData('"+ json.list[i].usercode + "')\">";
        				   html +=         "<span>"+ json.list[i].username + "</span><span>"+ json.list[i].subject +"</span>";
        				   html +=     "</a>";
        				   html +=     "<input type='hidden' name='userName' value='" + json.list[i].username + "'/>";
        				   html +=     "<input type='hidden' name='subject' value='" + json.list[i].subject + "'/>";
        				   html += "</li>";
        				   $("#professorListLayer").append(html);
        			   }
        			   
        			   if(json.registInfo != undefined && json.registInfo != null){
	           			  	console.debug(json.registInfo);
	           			  	$("#registTotalProfessor").html(json.registInfo.totalcount);
	           			  	$("#unregistProfessor").html(json.registInfo.unregistcount);
	           			  	$("#registProfessor").html(json.registInfo.registcount);
           		   		}
        		   }
        	   }else{
        		   alert(json.message);
        	   }
           }
           return;
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
        	alert("오류가 발생하였습니다");
        }
        ,beforeSend:function() {
        	$.blockUI();
		}
		,complete:function() {
			$.unblockUI();
			getPeriodInfo();
		}
    });
	
}
function getAchieveRegistUserList(registered){
	$("[id^='professor_']").removeClass("on");
	$("#professorListLayer li").show();
	
	$(".count-div p").removeClass("stay");
	$(".contents-serach-btns-div input").val("");
	if(registered == "N"){
		$(".unregist").show();
		$(".regist").hide();
		$("#unregistProfessor").parent().parent().addClass("stay");;
	}else if(registered == "Y"){
		$(".unregist").hide();
		$(".regist").show();
		$("#registProfessor").parent().parent().addClass("stay");;
	}else{
		$(".unregist").show();
		$(".regist").show();
		$("#registTotalProfessor").parent().parent().addClass("stay");;
	}
	
	requestUserCode = null;
	$(".contents").html(contentsHtml);
	initalize();
}
function getProfessorDetailData(userCode){
	requestUserCode = userCode;
	$(".contents").html(contentsHtml);
	$(".btn-service-popup").show();
	initalize();
	$(".view-all-pro a").css("border-bottom", "solid 0px #da2127");
	$("[id^='professor_']").removeClass("on");
	$("#professor_" + userCode).addClass("on");
	
	var userName = $("#professor_" + userCode).children("input[name='userName']").val();
	var subject = $("#professor_" + userCode).children("input[name='subject']").val();
	
	$("#contents-title-name").html("<span class=\"name-pro\">" + userName + "</span><span>" + subject +"</span>");
	$("#updateAchieve input[name='userName']").val(userName);
	$("#updateAchieve input[name='subject']").val(subject);
	
	if(periodDisableMode != null){
		if(periodDisableMode == "A" || periodDisableMode == "C"){
			disableElements();
		}
	}
	getAchieveProfessorResult(userCode);
}
function getAchieveProfessorResult(userCode) {
	$.ajax({
        type: "POST",
        url: "./getAchieveProfessorResult",
        data: {
        	"userCode": userCode
        	,"achieveType": "r"
        },
        dataType: "json",
        success: function(data, status) {
        	console.debug("================================getAchieveProfessorResult");
        	console.debug(data);
        	if (data.status == "success") {
				for(var i=0; i< data.result.length; i++){
					var row = data.result[i];
					var itemNum = row.research_item_num;
					//achieve_edu_unit_14  achieve_edu_point_14  achieve_edu_contents_14
					$("#achieve_re_unit_" + itemNum).val(row.result_unit == -1 ? "" : row.result_unit);
					$("#achieve_re_contents_" + itemNum).val(row.result_contents);
					$("#achieve_re_point_" + itemNum).html(row.result_point == -1 ? "" : ToFloat(row.result_point));
					if(row.filecount > -1){
						var html = "<a href=\"javascript:uploadFile($(this), "+ row.research_item_num +");\">"+ row.filecount + "건</a>";
						$("#achieve_file_"+ itemNum).find(".fileCountDiv").html(html);
					}
					$(".achieveDataSubmit").html("수정");
				}
				/*
				$("[id^='achieve_re_contents_']").each(function(){
				    while($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
				        $(this).height($(this).height()+1);
				    };
				});
				*/
				$("textarea").each(function(){
				    $(this).css('height', 'auto' );
				    $(this).height( this.scrollHeight );
				});
				$("textarea").css("border","solid 1px #ccc");
				$(".score-input-td input").css("border","solid 1px #ccc");
				
				$("[id^='achieve_re_unit_']").attr("readonly",false);
				$("[id^='achieve_re_contents_']").attr("readonly",false);
				calculateTotalPoint();
        	}
        },
        error: function(xhr, textStatus) {
			//alert("오류가 발생했습니다.");
			console.error(error);
        }
        ,complete:function() {
        	console.debug("periodDisableMode =>"+ periodDisableMode);
			if(periodDisableMode != null){
				if(periodDisableMode == "A" || periodDisableMode == "C"){
					disableElements();
				}
			}
		}
    });
}
function getFileCountByResearchCode(userCode){
	if(userCode == undefined || userCode == null || userCode == ""){
		userCode = requestUserCode;
	}
	$.ajax({
        type: "POST",
        url: "./getFileCountByResearchCode",
        data: {
        	"userCode": userCode
        	,"achieveType": "r"
        },
        dataType: "json",
        success: function(data, status) {
        	console.debug("================================getFileCountByResearchCode");
        	if (data.status == "success") {
				for(var i=0; i < data.fileCount.length; i++){
					var row = data.fileCount[i];
					console.debug(row);
					var html ="";
					if(row.filecount > 0){
						html = "<a href=\"javascript:uploadFile($(this), "+ row.research_item_num +");\">"+ row.filecount + "건</a>";
					}
					console.debug(html);
					var already = $("#achieve_file_"+ row.research_item_num).find(".fileCountDiv").html();
					console.debug("already =>"+ row.research_item_num + " =>"+ already);
					$("#achieve_file_"+ row.research_item_num).find(".fileCountDiv").html(html);
				}
        	}
        },
        error: function(xhr, textStatus) {
			//alert("오류가 발생했습니다.");
			console.error(error);
        }
    });
}
function updateAchieveData(){
	console.debug("updateAchieveData()");
	var paramObject = {};
	var paramList ={};
	var size = $("[id^='achieve_re_unit_']").length;
	var cnt = 0;
	for(var num = 1; num <= size; num++){
		var unit = $.trim($("#achieve_re_unit_" + num).val());
		var contents = $.trim($("#achieve_re_contents_" + num).val());
		var point = $.trim($("#achieve_re_point_" + num).html());
		console.debug("[" + num +" ]unit =>"+ unit);
		console.debug("[" + num +" ]contents =>"+ contents);
		console.debug("[" + num +" ]point =>"+ point);
		
		if(unit != undefined && unit != ""){
			if(unit < 0){
				alert("잘못된 값이 입력되었습니다");
				$("#achieve_re_unit_" + num).focus();
				return;
			}
			if(contents != undefined && contents == ""){
				if(num == 7){
					$("#achieve_re_contents_6").focus();
					return;
				}
				if(num == 9){
					$("#achieve_re_contents_8").focus();
					return;
				}
				
				$("#achieve_re_contents_" + num).focus();
				console.debug(num + "번째 컨텐츠가비어잇슴");
				return;
			}
		}
		if(unit.length > 0 || contents.length > 0){
			if(num == 7 || num == 9){
				point = 0;
			}
			var param = {
					"userCode" : requestUserCode
					,"researchItemNum": num
					,"unit": unit
					,"contents" : contents
					,"point" : point
					,"useFlag" : 'Y'
			}
				
			paramList[num] = param;	
			cnt++;
		}		
	}
	console.debug(paramList);

	paramObject.achieveType = "r";
	paramObject.requestUserCode = requestUserCode;
	paramObject.paramList = paramList;
	console.debug(paramObject);
	console.debug(JSON.stringify(paramObject));
	console.debug("cnt =>"+ cnt);
	if(cnt == 0 || requestUserCode == "undefined" || requestUserCode == null || requestUserCode ==""){
		return;
	}

	$.ajax({
        type: "POST",
        url: "./updateAchieveData",
        contentType : 'application/json; charset=UTF-8',
        data: JSON.stringify(paramObject),
        dataType: "json",
        success: function(data, status) {
        	console.debug(data);
        	if(data.status == "success"){
        		alert("등록되었습니다");
        		getProfessorList();
        	}else{
        		alert("오류가 발생했습니다");
        	}
        },
        error: function(xhr, textStatus) {
			//alert("오류가 발생했습니다.");
			alert(error);
        }
    });
	
}
function emptyAchieveData(){
	if(confirm("취소하시겠습니까?")){
		$("[id^='achieve_re_unit_']").attr("readonly",true);
		$("[id^='achieve_re_contents_']").attr("readonly",true);
		
		$("[id^='achieve_re_unit_']").val("");
		$("[id^='achieve_re_contents_']").val("");
		$("[id^='achieve_re_point_']").html("");
		
		$("[id^='professor_']").removeClass("on");
		
		$("#contents-title-name").html("<span class=\"name-pro\">" +  "</span><span>" + "</span>");
		$("#updateAchieve input[name='userName']").val("");
		$("#updateAchieve input[name='subject']").val("");
		
		$("textarea").css("border","solid 0px #ccc");
		$(".score-input-td input").css("border","solid 0px #ccc");
		$(".achieveDataSubmit").html("등록");		
	}
}

function findProfessor(userName){
	var array = new Array();
	$("#professorListLayer input[name^='userName']").each(function(){
		if($(this).val().includes(userName)){
			array.push($(this).parent().attr("id"));
	    }
	});
	console.debug("array =>"+ array);
	var showRegist = "total";
	if($(".count-div p").eq(1).attr("class") == "stay"){
		showRegist = "unregist";
	}else if($(".count-div p").eq(2).attr("class") == "stay"){
		showRegist = "regist";
	}else{
	}
	if(array != null && array.length > 0){
		$("#professorListLayer li").hide();
		for(var i=0; i< array.length; i++){
			
			var professor = $("#" + array[i]); 
			if(showRegist == "unregist"){
				if(professor.attr("class") == "unregist"){
					professor.show();	
				}
			}else if(showRegist == "regist"){
				if(professor.attr("class") == "regist"){
					professor.show();	
				}
			}else{
				$("#" + array[i]).show();	
			}
			
		}
	}else{
		alert("검색결과가 없습니다");		
	}
}

function toggleExcelRegistLayer(isShow){
	console.debug("showExcelRegistLayer()");
	if(isShow){
		$(".contents-btns-div").hide();
		$(".signup-btn-div").hide();
		$(".list-table-contents").hide();
		$(".guide-txt-table-div").hide();
		$(".contents").append($("#toggleExcelRegistLayer")[0].outerHTML);
		
		$("#toggleExcelRegistLayer").show();
		$("#toggleExcelRegistLayer .signup-btn-div").show();
		$(".input-upload").each(function(){
	        $(this).change(function(){
	            if( $(this).val() != "" ){
	                var value = $(this).val();
	                var chk = $(this).val().split('.').pop().toLowerCase();
	                if($.inArray(chk, ["xlsx","xls"]) == -1){
	                    alert("엑셀 파일만 등록해주세요.");
	                    $(this).val("");
	                    return true;
	                } else {
	                    $(this).parent().siblings("input[type='text']").val(value);
	                }
	            }
	        });
	    });
	}else{
		$(".contents-btns-div").show();
		$(".signup-btn-div").show();
		$(".list-table-contents").show();
		$(".guide-txt-table-div").show();
		$("#toggleExcelRegistLayer").remove();
		
	}
	
}

function uploadExcelFile(){
	var addFile = $(".input-upload").val();
    if ( addFile == 0 ){
        alert("엑셀 파일을 등록해주세요.");
        return true;
    }
    
    $("#uploadForm").ajaxForm({
		type: "POST",
		url:  '<c:url value="/admin/achieveExcelFile"/>',
		data: {
			"achieveType":"r"
		},
		dataType: "json",
		success: function(data, status){
			if (data.status == "success") {
				console.debug("data.resultCount =>"+ data.resultCount);
				alert("총 "+ data.resultCount + "건의 연구 등록을 완료하였습니다.");
				getProfessorList();
        	} else {
        		alert("오류가 발생했습니다");
        	}
		},
		error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
        },
		beforeSend:function() {
			$.blockUI();
		},
		complete:function() {
			$.unblockUI();
		}
	}).submit();
}



function ToFloat(number){
    var tmp = number + "";
    if(tmp.indexOf(".") != -1){
        number = Math.floor(number*100)/100;
//        number = number.replace(/(0+$)/, "");
    }

    return number;
}
</script>
</body>
</html>