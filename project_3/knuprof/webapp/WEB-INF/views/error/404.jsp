<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<style type="text/css">
body,div, dl, dt, dd, ol, ul, li, h1, h2,h3,h4,form,fieldset,p,button {margin:0; padding:0;}
html, body {height:100%;}
body,input,select,textarea{color:#333; font-family:dotum,'돋움',gulim,'굴림','맑은 고딕', Helvetica,sans-serif; font-size:.75em; color:#555;}
.copy {width:100%; text-align: center; font-size:.9em; color:#909090; padding:10px 0; clear:both; border-top:1px solid #c8c8d0; letter-spacing:1px; background:#FFF; z-index:1}
</style>
</head>

<body>

<table style="width:100%;height:95%;text-align:center;">
<tr>
<td><h1>페이지가 없습니다.</h1></td>
</tr>
<tr>
<td><a href="javascript:history.back(-1);"><h3>뒤로가기</h3></a></td>
</tr>
</table>
         
<div class="copy">Copyright(c)2014 경북대학교 의과대학·의과전문대학원 All rights reserved.</div>  

</body>
</html>