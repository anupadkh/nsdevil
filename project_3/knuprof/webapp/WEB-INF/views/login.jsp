<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="include/pro_header.jsp" %>
<link rel="stylesheet" type="text/css" href="admin/css/login.css" />

<div id="wrap">
    <header class="header">
       <div class="logo-div">
           <img src="pro/images/logo.png" alt="경북대학교 교수 업적 평가 관리 시스템" />
       </div>
    </header>
    <div class="container">
       <div class="form-contents">
           <h1>교수 업적 평가 관리 시스템</h1>

            <form class="login-form" action="" method="" onSubmit="javascript:return false;">
                <fieldset>
                   <div class="inputbutton-div">
                       <div class="section-radio-div">
                            <span></span>
                            <label class="label-radio">
                                <input type="radio" name="lv_radio" value="1" />행정
                            </label>
                            <label class="label-radio">
                                <input type="radio" name="lv_radio" value="2" />교수
                            </label>
                        </div>
                        <!-- 아이디 & 비밀번호 입력창 -->
                        <div class="login-input-div">
                            <p>
                                <label for="input_id">ID</label>
                                <input type="text" id="input_id" value="" required="required" placeholder="" />
                            </p>
                            <p>
                                <label for="input_pw">Password</label>
                                <input type="password" id="input_pw" required="required" placeholder="" />
                            </p>
                        </div>
                        <!-- //.login-input-div -->

                        <div class="login-button-div">
                            <button type="submit" onClick="javascript:loginSubmit();">LOGIN</button>
                        </div>
                        <!-- //.login-button-div -->
                   </div>
                   <!-- //.inputbutton -->

                    <!-- 아이디 저장 체크박스 / 비번찾기버튼 -->
                    <div class="login-chceck-div">
                        <p>
                            <label class="lb-checkbox"><input type="checkbox" class="check-id" name="id_save" checked />아이디 저장</label>
                        </p>
                        <!--
                        <p>
                            <a class="btn-find-pw">비밀번호 찾기</a>
                        </p>
                        -->
                    </div>
                    <!-- //.login-chceck-div -->
                </fieldset>
            </form>
            <p class="guide-call">
                <img src="pro/images/callnumber.png" alt="전화 1661-1878" /> 상담가능 시간 : 평일 오전 9시 30분 - 오후 6시(주말, 공휴일 휴무) 점심시간 12~1시
            </p>
            
            <div class="version_text">Ver. <spring:eval expression="@config['version']"/></div>
            
       </div>
       <!-- //.form-contents -->
       
    </div>
    <!-- //.container -->
	
    <footer id="footer">
        <div class="footer-contents">
            <div class="footer-address">
                <p>
                    경북대학교 의과대학 · 의과전문대학원 ADD. (41944)대구광역시 중구 국채보상로 680
                </p>
                <p>
                    TEL. 053-420-4910(교무), 4906(서무) FAX. 053-422-9195(교무), 420-4925(서무) E-mail. meddean@knu.ac.kr
                </p>
                <p>
                    Copyright(c)2014 경북대학교 의과대학·의과전문대학원 All rights reserved.
                </p>
            </div>
        </div>
    </footer>
    <!-- //#footer -->
</div>
<!-- //#wrap -->

<!-- 비번찾기 레이어 팝업 영역 -->
<div class="pop-up hidden">
    <div class="form-div">
       <p class="popup-form-div-title">비밀번호 찾기</p>
        <form method="">
            <fieldset>
                <p>
                    <label for="popup_input_name">이름</label>
                    <input type="text" id="popup_input_name" placeholder="성명" autofocus="autofocus" />
                </p>
                <p>
                    <label for="popup_input_id">아이디</label>
                    <input type="text" id="popup_input_id" placeholder="등록된 ID" />
                </p>
                <p>
                    <label for="popup_input_email">E-mail</label>
                    <input type="text" id="popup_input_email" placeholder="등록된 E-mail" />
                </p>
                <p class="guide-find">아래 확인 버튼을 클릭하시면, 등록된 e-mail로 변경된 PW가 전송됩니다.</p>

                <div class="form-btn-div">
                    <button type="button" class="btn-find-cancle">취소</button>
                    <button type="button">확인</button>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<!-- //.pop-up : 비번찾기 팝업 영역 -->

<script src="admin/js/radiocheckbox.js"></script>
<script>
$(function(){
    //로그인 체크박스 스타일
    $(".lb-checkbox").bind("click", function(){
        if ( $(this).children("input[type='checkbox']").is(":checked") ){
            $(this).addClass("on");
        } else {
            $(this).removeClass("on");
        }
    });
    //비번찾기 팝업
    $(".btn-find-pw").bind("click", function(){
        $("html").css("overflow-y","hidden");
        $(".pop-up").fadeIn();
    });
    $(".btn-find-cancle").bind("click", function(){
        $("html").css("overflow-y","auto");
        $(".pop-up").fadeOut();
    });
    
    $(":input[name='lv_radio'][value='" + getCookie("mem_level") + "']").prop("checked", true);
    $("#input_id").val(getCookie("saveId"));
    if ($("#input_id").val() != "") {
    	$(":input[name='id_save']").prop("checked", true); 
    	$(".lb-checkbox").addClass("on");
    }
});

function loginSubmit() {
	if ($(":input[name='lv_radio']:checked").length == 0) {
		alert("로그인 할 관리자 유형을 선택해 주세요.");
		 $(":input[name='lv_radio']").focus();
		return;
	}
	
	if ($(":input[name='id_save']").is(":checked")) {
		setCookie("mem_level", $(":input[name='lv_radio']:checked").val(), 30);
		setCookie("saveId", $("#input_id").val(), 30);
	} else {
		setCookie("mem_level", "", -1);
		setCookie("saveId", "", -1);
	}
	if($('#input_id').val() == "") {
		alert("아이디를 입력해주세요");
		$('#input_id').focus();
		return;
	} else if($('#input_pw').val() == "") {
		alert("비밀번호를 입력해주세요");
		$('#input_pw').focus();
		return;
	}
	
	$.ajax({
        type: "POST",
        url: "ajax/main/loginProcess",
        data: {
        	"input_id" : $('#input_id').val(),
        	"input_pw" : $('#input_pw').val(),
        	"lv_radio" : $(":input[name='lv_radio']:checked").val()
        },
        dataType: "json",
        success: function(data, status) {
        	if (data.status == "success") {
        		if ($(":input[name='lv_radio']:checked").val() == 1) {
        			if(data.user_level == 0){
        				location.href="admin/";	
        			}else if(data.user_level == 1){
        				location.href='<c:url value="/board/"/>';	
        			}
				} else {
					location.href="pro/notice";
				}
        	} else {
        		alert("잘못된 ID 또는 비밀번호 입니다.");
        	}
        },
        error: function(xhr, textStatus) {
			alert("오류가 발생했습니다.");
			//document.write(xhr.responseText);
        }
    });
}

</script>
<%@ include file="include/footer.jsp" %>