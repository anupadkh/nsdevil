<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="imageHost"><spring:eval expression="@config['imageHost']"/></c:set>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="author" content="NS Devilⓒ" />
<meta name="format-detection" content="telephone=no" />
<title>경북대학교 의학전문대학원 교수 업적 평가 관리 시스템</title>
<link rel="stylesheet" href="<c:url value="/css/dev.css"/>" type="text/css" />
<link rel="stylesheet" type="text/css" href="<c:url value="css/datetime.css"/>" />
<link rel="stylesheet" type="text/css" href="<c:url value="css/main.css"/>" />
<script type="text/javascript" src="<c:url value="/js/jquery/jquery-1.11.0.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery/jquery.tmpl.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery/jquery.i18n.properties-min-1.0.9.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery/jquery.blockUI.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/jquery/jquery.form.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/common.js"/>"></script>
<script type="text/javascript" src="<c:url value="js/jq.js"/>"></script>
<script type="text/javascript" src="<c:url value="/js/map.js"/>"></script>
</head>



<script type="text/javascript">

//파일 load용 전역 변수
var imageHost = "${imageHost}";

//AJAX 오류코드가 901일 경우 로그인화면으로 이동
!function($) {
    $.ajaxSetup({
        statusCode: {
            901: function(){
            	location.href="<c:url value="/"/>";
            }
        }
    });
}(window.jQuery);

//메세지 프로퍼티
//jQuery.i18n.properties({
//	name:"message",
//	path:"<c:url value="/messages"/>/",
//	mode:"both",
//	language:"${pageContext.response.locale}",
//	callback: function() {
//	}
//});

</script>
<body>

<!--[if lte IE 9]>
<script src="admin/js/ph.js"></script>
<![endif]-->
<!--[if lte IE 8]>
<script src="admin/js/el.js"></script>
<script src="admin/js/es5.js"></script>
<![endif]-->
<!--[if lte IE 7]>
<script>
$(document).ready(function(){
$(".pop-up-ie7").removeClass("hidden");
$(".btn-popup-close").bind("click",function(){
$(".pop-up-ie7").addClass("hidden");
});
});
</script>
<![endif]-->

<!-- 익스7 이하 버젼 경고팝업 -->
<div class="pop-up-ie7 hidden">
    <div class="popup-ie7">
        <div class="popup-ie7-header">
            <p>Internet Explorer 버젼 체크 알림</p>
            <p><button type="button" class="btn-popup-close">X</button></p>
        </div>
        <div class="popup-ie7-content">
            <p>이 홈페이지는 <span class="color-red">Internet Explorer 7이하 버젼</span>을 지원하지 않습니다. 구버젼 익스플로러 사용 시 기능 및 보안에 문제가 발생할 수 있습니다. 원활한 사용을 위하여 최신 익스플로러 버젼으로 업데이트 하시기 바랍니다.
            </p>
        </div>
        <div class="popup-ie7-button-div">
            <button type="button" class="btn-popup-close">CLOSE</button>
        </div>
    </div>
</div>
<!-- //.pop-up-ie7 -->

<noscript title="스크립트 미작동 안내">
    <p>
        본 사이트는 자바스크립트가 작동되어야 편리하게 이용하실 수 있습니다. 현재 스크립트가 정상적으로 작동되지 않습니다.
    </p>
</noscript>