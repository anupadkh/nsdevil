<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

	<header class="header">
        <div class="logo-div">
            <img src="images/logo.png" alt="경북대학교" />
            <div class="log-div">
                <!-- 사용자 로그 정보 및 로그아웃 버튼-->
                <p>
                	<c:choose>
                	<c:when test="${sessionScope.userlevel == 0}">
                	<span class="top-ip"><span class="logname-admin">Super Admin</span>님 (${sessionScope.loginDate} 접속)</span>
                	</c:when>
                	<c:when test="${sessionScope.userlevel == 1}">
                	<span class="top-ip"><span class="logname-admin">${sessionScope.adminName}</span>님 (${sessionScope.loginDate} 접속)</span>
                	</c:when>
                	<c:when test="${sessionScope.userlevel == 2}">
                	<span class="top-ip">${sessionScope.adminName} 교수님(${sessionScope.loginDate} 접속)</span>
                	</c:when>
                	<c:otherwise></c:otherwise>
                	</c:choose>
                    
                    <c:if test="${sessionScope.userlevel == 0}">
                    </c:if>
                    <c:if test="${sessionScope.userlevel == 1}">
                    </c:if>
                    <c:if test="${sessionScore.userlevel == 2}">
                    </c:if>
                    
                    <span class="top-ip"><a href="<c:url value="/pro/info"/>">비밀번호변경</a></span>
                    <a href="<c:url value="/logout"/>">로그아웃</a>
                </p>

                <nav class="service-nav">
                    <ul>
                    	<c:choose>
                    	<c:when test="${sessionScope.userlevel == 0}">
                    	<li><a href="<c:url value="/admin/"/>">관리</a></li>
                        <li><a href="javascript:void(0)">평가결과</a></li>
                        <li><a href="<c:url value="/admin/updateAchieve?category=e"/>">교육</a></li>
                        <li><a href="<c:url value="/admin/updateAchieve?category=r"/>">연구</a></li>
                        <li><a href="<c:url value="/admin/updateAchieve?category=s"/>">봉사</a></li>

                        <li class="last-li-top"><a href="<c:url value="/board/"/>">게시판</a></li>
	                	</c:when>
    	            	<c:when test="${sessionScope.userlevel == 1}">
    	            	<li><a></a></li>
                        <li><a></a></li>
    	            	<li><a href="<c:url value="/admin/updateAchieve?category=e"/>">교육</a></li> <!-- category=e ,r ,s -->
                        <li><a href="<c:url value="/admin/updateAchieve?category=r"/>">연구</a></li>
                        <li><a href="<c:url value="/admin/updateAchieve?category=s"/>">봉사</a></li>
                        <li class="last-li-top"><a href="<c:url value="/board/"/>">게시판</a></li>
        	        	</c:when>
            	    	<c:when test="${sessionScope.userlevel == 2}">
            	    	<!-- 현재페이지 a태그에 클래스 "active" 추가 -->
                        <li><a href="<c:url value="/pro/notice"/>" >MY</a></li>
                        <li><a href="<c:url value="/pro/service"/>">봉사</a></li>
                        <li class="last-li-top"><a href="<c:url value="/board/"/>">게시판</a></li>
                		</c:when>
                		<c:otherwise></c:otherwise>
                    	</c:choose>
                    	
                    	<c:if test="${sessionScope.userlevel == 0}">
                        
                        </c:if>
                        <c:if test="${sessionScope.userlevel == 1}">
                        </c:if>
                    	<c:if test="${sessionScope.userlevel == 2}">
                        
                        </c:if>
                    </ul>
                    
                </nav>
                
            </div>
        </div>
    </header>
    <!-- //.header -->