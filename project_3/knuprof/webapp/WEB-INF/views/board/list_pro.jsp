<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../include/pro_header.jsp" %>

<div id="wrap">
   <%@ include file="../include/top.jsp" %>

    <div class="container">
        <div class="left_bomenu">
            <ul class="board_ul">
                <li class=""><a href="javascript:toggleAddFormLayer(true)">문의하기</a></li>
           		<li style="display:none;" class=""><a href="javascript:toggleAddFormLayer(false)">게시판 리스트</a></li>
            </ul>    
        </div>
        <!-- //.leftmenu -->

        <div class="content">
            <div class="board_b"  id="boardLayer">
                <table class="Notice">
                    <colgroup>
                        <col width="25%" />
                        <col width="75%" />
                    </colgroup>
                    <tbody id="boardListLayer">
                    	<!-- 
                        <tr class="title_bg">
                            <td>
                                등록일
                            </td>
                            <td>
                                알림내용
                            </td>
                        </tr>
                        <tr>
                            <td>
                                2016.02.01
                            </td>
                            <td class="t_left">
                            	<span class="list_blue">공지</span><a href="">봉사업적 등록을 시작하실 수 있습니다.</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                2015.12.03
                            </td>
                            <td class="t_left">
								<a href="">평가결과 확인은 언제 할 수 있는 건가요?</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                2015.12.03
                            </td>
                            <td class="t_left">
                            	└&nbsp; <span class="list_orange">행정</span><a href="">아직 결정되지 않았습니다..</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                2015.12.03
                            </td>
                            <td class="t_left">
                            	 └&nbsp; <span class="list_gray">나</span><a href="">추가로 봉사 등록을 하고 싶은데 가능한지 여부를....</a>
                            </td>
                        </tr>
                        <!--<tr>
                            <td>
                                2015.12.03
                            </td>
                            <td class="t_left">
                                <span class="notice_ok">승인</span>봉사 등록내역이 최종 승인되었습니다.
                            </td>
                        </tr>-->
                    </tbody>
                </table>       
            </div>
            <div class="table-list-num"  id="pagingLayer"></div>
            
            
            <div id="addFormLayer" class="board_c" style="display:none;">
                <table class="wr_square">
                    <colgroup>
                        <col width="6%" />
                        <col width="94%" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <td>
                                제목:
                            </td>
                            <td>
                                 <input type="text" id="questionWriteTitle" class="tit_a" style="background-color:white;border:solid 1px #999;"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                문의 내용:
                            </td>

                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea id="questionWriteMemo" class="in_memo textAreaCustomStyle" ></textarea>
                            </td>

                        </tr>

                        <tr>
                            <td colspan="2" class="wri_data">
								<a href="javascript:addBoardQna('new')"><span class="bt_red_a">등록</span></a>
								<a href="javascript:toggleAddFormLayer(false)"><span class="bt_gray_a">취소</span></a>     
                            </td>
                        </tr>

                    </tbody>
                </table>             
            </div>
            
            <div id="detailLayer" style="display:none;"> 
	            <div class="board_c">
	                <table class="wr_square">
	                    <colgroup>
	                        <col width="6%" />
	                        <col width="94%" />
	                    </colgroup>
	                    <tbody>
	                        <tr>
	                            <td>
	                                제목:
	                            </td>
	                            <td>
	                                 <input type="text" class="tit_a bbbb" disabled style="background-color:white;border:solid 1px #999"/>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td colspan="2">
	                            </td>
	                        </tr>
	                        <tr>
	                            <td colspan="2">
	                                문의 내용:
	                            </td>
	
	                        </tr>
	                        <tr>
	                            <td colspan="2">
	                                <textarea class="in_memo textAreaCustomStyle cccc" disabled></textarea>
	                            </td>
	
	                        </tr>
	
	                        <tr>
	                            <td colspan="2" class="wri_data">
									<span class="date"><!-- 작성일: 2016-02-01 16:42 --></span>
									<span class="write"><!-- 작성자: 나 --></span>
									<a href="javascript:boardDataModify()"><span class="bt_gray">수정</span></a>
									<a href="javascript:boardDataDelete()"><span class="bt_gray">삭제</span></a> 
	                            </td>
	                        </tr>
	
	                    </tbody>
	                </table>             
	            </div>
	            <!-- //.board_c -->
	            <div class="bt_bottom">
	            	<a href=""><span class="bt_gray_left">< 이전 글</span></a>
	                <a href="javascript:addReplyForm()"><span class="bt_red_center">문의 댓글 남기기</span></a>
	                <a href=""><span class="bt_gray_right">다음 글 ></span></a>
	            </div>
	            <div class="widthbox">
	            	<!-- 
	                <div class="box_orange">
	                    <p><span class="reply_orange">└&nbsp;행정답변&nbsp;</span> <span>2016.02.02.10:12</span>
	                    </p>
	                    <p><textarea class="re_box"></textarea>
	                    </p>
	                </div>
	                <div class="box_gray">
	                    <p><span class="reply_gray">└&nbsp;나의답변&nbsp;</span> <span>2016.02.02.10:12</span>
	                    <a href=""><span class="bt_gray">수정</span></a><a href=""><span class="bt_gray">삭제</span></a>
	                    </p>
	                    <p><textarea class="re_box"></textarea>
	                    </p>
	                </div>
	                 -->
	            </div>
            </div>
            
            
             <div id="noticeLayer" class="board_c" style="display:none;">
                <table class="wr_square">
                    <colgroup>
                        <col width="6%" />
                        <col width="94%" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <td>
                                제목:
                            </td>
                            <td>
                                 <input type="text" id="noticeTitle" class="tit_a" readOnly/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                공지 내용:
                            </td>

                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea id="noticeContents" class="in_memo" readOnly></textarea>
                            </td>

                        </tr>

                        <tr></tr>

                    </tbody>
                </table>   
                <div class="bt_bottom">
	            	<a href=""><span class="bt_gray_left">< 이전 글</span></a>
	                <a href=""><span class="bt_gray_right">다음 글 ></span></a>
	         	</div>          
            </div>
             
        </div>
        <!-- //.content -->
    </div>
    <!-- //.container -->

	<%@ include file="../include/bottom.jsp" %>
</div>
<!-- //#wrap -->
<script type="text/javascript" src=<c:url value="/js/paging.js"/>></script>
<script>
$(".service-nav a").removeClass("active");
$(".service-nav a").eq(2).addClass("active");
var userLevel = "${sessionScope.userlevel}";
var userName = "${userName}";
var userCode = "${sessionScope.userCode}";
var boardList = ${json.boardList};
var pagingInfo = ${json.pagingInfo};
if(boardList != undefined && boardList != null && boardList !=""){
	setBoardList(boardList);	
	setPagingData(pagingInfo);
}

var boardDetailObject = null;
var searchOption = null;
$(document).ready(function(){

});
function getPageList(pageNo){
	getBoardList(pageNo);
	if(searchOption == null){
		searchOption = new Object();
	}
	searchOption.pageNo = pageNo;
}
function getBoardList(pageNo){
	var param = {};
	param.pageNo = pageNo;
	param.rowCount = 10;
	
	/*
	if(searchOption != null){
		if(typeof searchOption.searchUserName != "undefined"){
			param.searchUserName = searchOption.searchUserName;
		}
		if(typeof searchOption.searchYear != "undefined"){
			param.searchYear = searchOption.searchYear;
		}
		if(typeof searchOption.searchUserLevel != "undefined"){
			param.searchUserLevel = searchOption.searchUserLevel;
		}
	}
	*/
	$.ajax({
        type: "POST"
        ,url: "./getBoardList/"
        ,data: param
        ,dataType: "json"
        ,async: true
        ,success: function(json){
        	console.debug(json);
            if(json == undefined && json == null){
           		return;
            }
            if(json.boardList != undefined && json.boardList != null){
            	setBoardList(json.boardList);
            	setPagingData(json.pagingInfo);
            }
            if(json.status == "fail"){
            	alert("오류가 발생 하였습니다.");
            	return;
            }
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
            alert("오류가 발생하였습니다");
        }
        ,complete: function() {
            	
        }
    });
}
function setBoardList(json){
	console.debug("setBoardList ============");
	console.debug(json);
	
	function sLength(string){
		return string.length > 40 ? (string.substring(0,40)+ "..."): string;
	}
	$("#boardListLayer").empty();
	var html = "<tr class=\"title_bg\"><td>등록일</td><td>알림내용</td></tr>";
	for(var i=0; i < json.length; i++){
		
		var data = json[i];
		html += "<tr>";
		html += 	"<td>";
		html += 		data.regdate
		html += 	"</td>";
		html += 	"<td class=\"t_left\">";
		
		if(data.boardtype == "notice"){
			html += 	"<span class=\"list_blue\">공지</span>";
			html +=			"<a href=\"javascript:goToDetailContents('"+ data.boardtype +"','"+ data.boardcode + "')\">";
			html +=			  stripTag(sLength(data.title));
			html +=			 "</a>";	
		}else if(data.boardtype =="qna"){
			if(data.userlevel == 0){
					html +=	"└&nbsp; <span class=\"list_orange\">행정</span><a href=\"javascript:goToDetailContents('"+ data.boardtype +"','"+ data.maingroupboardcode + "')\">"+ stripTag(sLength(data.contents))+"</a>";
			}else if(data.userlevel == 1){ //data.writerusername
					html +=	"└&nbsp; <span class=\"list_orange\">"+"행정"+"</span><a href=\"javascript:goToDetailContents('"+ data.boardtype +"','"+ data.maingroupboardcode + "')\">"+ stripTag(sLength(data.contents))+"</a>";
			}else if(data.userlevel == 2){
				if(data.replyorder == 0 ){
					html +=	"<a href=\"javascript:goToDetailContents('"+ data.boardtype +"','"+ data.maingroupboardcode + "')\">"+ stripTag(sLength(data.title)) +"</a>";	
				}else{
					html +=	"└&nbsp; <span class=\"list_gray\">나</span>";
					html += "<a href=\"javascript:goToDetailContents('"+ data.boardtype +"','"+ data.maingroupboardcode + "')\">";
					html += stripTag(sLength(data.contents)); 
					html += "</a>";
				}
			}
		}
		html += 	"</td>";
		html += "</tr>";
	}
	$("#boardListLayer").append(html);
	
}
function goToDetailContents(boardType,boardCode){
	console.debug("goToDetailContents =>" + boardType + "/" + boardCode);
	
	$.ajax({
        type: "POST"
        ,url: "./getDetailInfo/"
        ,data: {
           	"boardType" : boardType
           	,"boardCode" : boardCode
        }
        ,dataType: "json"
        ,async: true
        ,success: function(json){
        	console.debug(json);
            if(json == undefined && json == null){
           		return;
            }
            if(json.status =="success"){
            	setBoardDetailData(json);
            }
            if(json.status == "fail"){
            	alert("오류가 발생 하였습니다.");
            	return;
            }
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
            alert("오류가 발생하였습니다");
        }
        ,complete: function() {
            	
        }
    });
}
function toggleAddFormLayer(isShow){
	if(isShow){
		$("#boardLayer").hide();
		$("#pagingLayer").hide();
		$("#addFormLayer").fadeIn(100);	
		$(".board_ul li").eq(0).hide();
		$(".board_ul li").eq(1).show();
	}else{
		$("#boardLayer").show();
		$("#pagingLayer").show();
		$("#addFormLayer").hide();
		$(".board_ul li").eq(0).show();
		$(".board_ul li").eq(1).hide();
		$("#questionWriteTitle").val('');
		$("#questionWriteMemo").val('');
		
		
		
		$("#detailLayer").hide();
		$("#detailLayer .widthbox").empty();
		
		$("#noticeLayer").hide();
	}
	boardDetailObject = null;
	
}
function boardDataUpdate(isReply,mode, boardCode,groupId,replyOrder){
	console.debug("boardDataUpdate =>"+ isReply +" / mode:" + mode + " /bordCode:" + boardCode + " / groupId:"+ groupId + " / replyOrder:" + replyOrder);
	
	if(isReply){
		if(mode == "modify"){
				var param = {}
				param.boardCode = boardCode;
				param.title = "";
				param.contents = $("#reply_" + replyOrder).val();
				//param.replyOrder = replyOrder;
				$("#reply_textArea_" + replyOrder).attr("disabled",false);
				$("#reply_textArea_" + replyOrder).focus();
				
				$("#reply_button_"+ boardCode + " a").eq(0).attr("href","javascript:boardDataUpdate(true,'update','"+ boardCode + "','',"+ replyOrder + ")");
		}
		if(mode == "update"){
			if(confirm("문의댓글 내용을 수정하시겠습니까?")){
				var param = {}
				param.boardCode = boardCode;
				param.title = "";
				param.contents = $("#reply_" + replyOrder).val();
				//param.replyOrder = replyOrder;
				updateBoardQna(param);
			}
		}
		
		if(mode == "delete"){
			if(confirm("문의댓글을 삭제하시겠습니까?")){
				console.debug(boardCode + " / " + groupId);
				var param = {}
				param.boardCode = boardCode;
				param.replyOrder = replyOrder;
				param.use_flag = 'N'
				updateBoardQna(param);
			}
		}
	}else{	
		if(mode == "modify"){
				$("#detailLayer .tit_a").focus();
				$("#detailLayer .in_memo").focus();
				$("#detailLayer .tit_a").attr("disabled",false);
				$("#detailLayer .in_memo").attr("disabled",false);
				$("#detailLayer .wri_data a").eq(0).attr("href","javascript:boardDataUpdate(false,'update','"+ boardCode + "')");
		}
		if(mode == "update"){
			if(confirm("문의사항을 수정하시겠습니까?")){
				var title = $("#detailLayer .tit_a").val();
				var contents = $("#detailLayer .in_memo").val();
				var param = {}
				param.boardCode = boardCode;
				param.title = title;
				param.contents = contents;
				param.replyOrder = replyOrder;
				updateBoardQna(param);
			}
		}
		if(mode == "delete"){
			if(confirm("문의사항을 삭제하시겠습니까?")){
				console.debug(boardCode + " / " + groupId);
				var param = {}
				param.groupId = groupId;
				param.use_flag = 'N'
				updateBoardQna(param);
			}
			
		}
	}
}
function updateBoardQna(param){
	$.ajax({
        type: "POST"
        ,url: "./updateBoardQna"
        ,data: param
        ,dataType: "json"
        ,async: true
        ,success: function(json){
        	console.debug(json);
            if(json == undefined && json == null){
           		return;
            }
            if(json.status =="success"){
            	getBoardList(1);
            	toggleAddFormLayer(false);
            	
            }
            if(json.status == "fail"){
            	alert("오류가 발생 하였습니다.");
            	return;
            }
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
            alert("오류가 발생하였습니다");
        }
        ,complete: function() {
            	
        }
    });
}
function addBoardQna(mode){
	function convert(str){
		str = str.replace(/</g,"&lt;");
	 	str = str.replace(/>/g,"&gt;");
	 	str = str.replace(/\"/g,"&quot;");
	 	str = str.replace(/\'/g,"&#39;");
	 	str = str.replace(/\n/g,"<br />");
		return str;
	}
	
	var param = {};
	if(mode == "new"){
		param.title = stripTag($("#questionWriteTitle").val());
		param.contents = stripTag($("#questionWriteMemo").val());
		param.answered = "N";
		param.userCode = userCode;
		param.askUserCode = userCode;
	}else if(mode =="reply"){
		param.groupId = boardDetailObject.groupId;
		param.title = ""
		param.contents = stripTag($("#detailLayer .widthbox .re_box").last().val());
		param.answered = "N";
		param.userCode = userCode;
		param.askUserCode = userCode;
	}
	console.debug(param);
	$.ajax({
        type: "POST"
        ,url: "./addBoardQna"
        ,data: param
        ,dataType: "json"
        ,async: true
        ,success: function(json){
        	console.debug(json);
            if(json == undefined && json == null){
           		return;
            }
            if(json.status =="success"){
            	getBoardList(1);
            	toggleAddFormLayer(false);
            }
            if(json.status == "fail"){
            	alert("오류가 발생 하였습니다.");
            	return;
            }
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
            alert("오류가 발생하였습니다");
        }
        ,complete: function() {
            	
        }
    });
	
}
function setBoardDetailData(json){
	console.debug("setBoardDetailData ==================");
	console.debug(json);
	$(".board_ul li").eq(0).hide();
	$(".board_ul li").eq(1).show();
	
	$("#boardLayer").hide();
	$("#pagingLayer").hide();
	
	if(json.notice == undefined ){
		$("#detailLayer").fadeIn(300);
		$("#detailLayer .widthbox").empty();
		$("#detailLayer .wri_data").show();
		
		$("#detailLayer .tit_a").attr("disabled",false);
		$("#detailLayer .in_memo").attr("disabled",false);
		
		boardDetailObject = new Object();
		
		for(var i=0; i < json.qna.length; i++){
			var qna = json.qna[i];
			
			boardDetailObject.groupId = typeof boardDetailObject.groupId == "undefined" ? qna.groupid : boardDetailObject.groupId ;
			boardDetailObject.boardCode = typeof boardDetailObject.boardCode == "undefined" ? qna.boardcode : boardDetailObject.boardCode;
			
			if(qna.replyorder == 0){
				$("#detailLayer .tit_a").val(qna.title);
				$("#detailLayer .in_memo").val(qna.contents);
				$("#detailLayer .wri_data .date").html("작성일: "+ qna.reg_date);
				$("#detailLayer .wri_data .write").html("작성자: "+ qna.user_name);
				
				$("#detailLayer .wri_data a").eq(0).attr("href","javascript:boardDataUpdate(false,'modify','"+ qna.boardcode + "')");;
				$("#detailLayer .wri_data a").eq(1).attr("href","javascript:boardDataUpdate(false,'delete','"+ qna.boardcode + "','"+ qna.groupid +"','"+ qna.replyorder +"')");;
				
			}else{
				var html = "";
				if(qna.askusercode != qna.writerusercode){
					var name = "행정팀";
					//if(qna.user_level ==1 ){ name = qna.user_name}
					html += "<div class=\"box_orange\">";
	                html += 	"<p><span class=\"reply_orange\">└&nbsp;"+ name + "&nbsp;</span> <span>"+ qna.reg_date +"</span></p>";
	                html += 	"<p><textarea class=\"re_box textAreaCustomStyle \" disabled=\"disabled\">" + qna.contents + "</textarea></p>";
	            	html += "</div>";
				}else{					
					html += "<div class=\"box_gray\">";
					html += 	"<p id=\"reply_button_" + qna.boardcode + "\">";
					html +=         "<span class=\"reply_gray\">└&nbsp;나의답변&nbsp;</span> <span>" + qna.reg_date + "</span>";
	                html += 		"<a href=\"javascript:boardDataUpdate(true,'modify','"+ qna.boardcode + "','','"+ qna.replyorder+"')\"><span class=\"bt_gray\">수정</span></a>";
	                html +=     	"<a href=\"javascript:boardDataUpdate(true,'delete','"+ qna.boardcode + "')\"><span class=\"bt_gray\">삭제</span></a>";
	                html += 	"</p>";
	                html +=     "<p><textarea class=\"re_box textAreaCustomStyle \" disabled id=\"reply_textArea_"+ qna.replyorder+ "\">"+ qna.contents + "</textarea></p>";
	            	html += "</div>";
				}
				$("#detailLayer .widthbox").append(html);
			}
		}
	}else if(json.qna == undefined && json.notice != undefined && json.notice != null){
		console.debug(json.notice.title + " "+ json.notice.contents);
		boardDetailObject = new Object();
		boardDetailObject.groupId = -1;
		boardDetailObject.boardCode = json.notice.board_notice_code;
		boardDetailObject.boardType = "notice";
		console.debug("json.notice.groupid =>"+ json.notice.groupid );
		console.debug("json.notice.boardcode =>"+ json.notice.boardcode  );
		$("#detailLayer").fadeIn(300);
		$("#detailLayer .widthbox").empty();
		$("#detailLayer .wri_data").hide();
		
		$("#detailLayer .tit_a").val(json.notice.title);
		$("#detailLayer .in_memo").val(json.notice.contents);
		$("#detailLayer .tit_a").attr("disabled",true);
		$("#detailLayer .in_memo").attr("disabled",true);
		
	}
	
	$("#detailLayer .tit_a").attr("disabled",true);
	$("#detailLayer .in_memo").attr("disabled",true);
	
	console.debug("json.assignInfo====================================");
	console.debug(json.assignInfo);
	if(json.assignInfo != undefined && json.assignInfo != null){
		var prevHref = "javascript:goToDetailContents('"+ json.assignInfo.prevBoardType +"','"+ json.assignInfo.prevBoardCode + "')";
		var nextHref= "javascript:goToDetailContents('"+ json.assignInfo.nextBoardType +"','"+ json.assignInfo.nextBoardCode + "')"
		if(json.assignInfo.prevBoardCode == 0){
			prevHref = "javascript:alert('제일 처음 글 입니다')";
		}
		if(json.assignInfo.nextBoardCode == 0){
			nextHref = "javascript:alert('제일 마지막 글 입니다')";
		}
		
		var html = "";
		html += "<a href=\""+ prevHref + "\"><span class=\"bt_gray_left\">< 이전 글</span></a>";
		html += "<a href=\"javascript:addReplyForm()\"><span class=\"bt_red_center\">문의 댓글 남기기</span></a>";
		html += "<a href=\""+ nextHref + "\"><span class=\"bt_gray_right\">다음 글 ></span></a>";
		$("#detailLayer .bt_bottom").html(html);
	}
	
	adjustTextAreaSize($(".textAreaCustomStyle"));
}
function addReplyForm(){
	var html ="";
	html += "<div class=\"box_gray\" name=\"newReplyForm\">";
	html += 	"<p><span class=\"reply_gray\">└&nbsp;나의답변&nbsp;</span> <span></span>";
    html += 	"<a href=\"javascript:$('#detailLayer .box_gray').last().remove();\"><span class=\"bt_gray\">취소</span></a>";
    html +=     "<a href=\"javascript:addBoardQna('reply');\"><span class=\"bt_gray\">등록</span></a>";
    html += 	"</p><p><textarea class=\"re_box\" value=''></textarea></p>";
	html += "</div>";
	if($("div[name='newReplyForm']").length > 0){
		$("div[name='newReplyForm']").find(".re_box").focus();
	}else{
		$("#detailLayer .widthbox").append(html);
		$("#detailLayer .box_gray").last().find(".re_box").focus();
	}
	$("body").scrollTop($(document).height());
	
}
function stripTag (str){
    return str.replace(/(<([^>]+)>)/ig,"");
}
function adjustTextAreaSize(object){
	if(object != undefined){
		object.each(function(){
		    while($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
		        $(this).height($(this).height()+1);
		    };
		});	
	}else{
		$("textArea").each(function(){
		    while($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
		        $(this).height($(this).height()+1);
		    };
		});
	}
}
</script>
<style>
.textAreaCustomStyle{
	resize :none;
	background-color:white;
	border:solid 1px #999;
}
</style>
<%@ include file="../include/footer.jsp" %>