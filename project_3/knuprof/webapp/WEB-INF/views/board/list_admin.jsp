<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="../include/pro_header.jsp" %>

<div id="wrap">
   <%@ include file="../include/top.jsp" %>

<div class="container" >

       
        <!-- 이용자 구분 검색영역 -->
        <div class="contents-serach-btns-div">
            <div class="contents-search-div">
                <p>
                    <label for="search_year">년도 : </label>
                    <select id="searchYear">
                    	<option value="" selected></option>
                        <option value="2016">2016</option>
                        <option value="2017">2017</option>
                        <option value="2018" >2018</option>
                    </select>
                </p>
                <p>
                    <label>구분 : </label>
                    <select id="searchUserType">
                        <option value="a">전체</option>
                        <option value="s">행정</option>
                        <option value="p">교수</option>
                    </select>
                </p>
                <p>
                    <label for="search_name">성명 : </label>
                    <input id="search_name" type="text" value="" placeholder="성명 입력" />
                    <button type="button" class="btn-search-name"><img src="images/btn-search.png" alt="검색" /></button>
                </p>
            </div>
            <div class="contents-btns-div">
                <a href="javascript:toggleLayer('noticeWriteLayer')">공지등록</a>
            </div>
        </div>

        <div class="content" style="float:none;width:auto;">
        	<div id="boardLayer" class="innerLayerInContent" style="display:block;">
				<p class="topBoard" style="padding:10px">
					<input type="checkbox" class="answeredCheckBox" id="answeredTotal"> 전체
					<input type="checkbox" class="answeredCheckBox" id="answeredNotice"> 공지 
					<input type="checkbox" class="answeredCheckBox" id="answeredYet"> 미등록 
					<input type="checkbox" class="answeredCheckBox" id="answeredDone"> 완료 
				</p>
					<div class="board_a">
		                <table class="Notice">
		                    <colgroup>
		                        <col width="10%" />
		                        <col width="20%" />
		                        <col width="10%" />
		                        <col width="60%" />
		                    </colgroup>
		                    <tbody id="boardListLayer">
		                    	<!-- 
		                        <tr class="title_bg">
		                            <td>
		                                답변여부
		                            </td>
		                            <td>
		                             등록일
		                            </td>
		                            <td>
		                                등록자
		                            </td>
		                            <td>
		                                문의내용
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                                <span class="notice_return">공지</span>
		                            </td>
		                            <td>
		                                2016.02.01
		                            </td>
		                            <td>
		                                행정팀
		                            </td>
		                            <td class="t_left">
		                                <a href="sub_6_1_rep.html">봉사기간이 만료되었습니다.</a>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                                <span class="color-red">[미등록]</span>
		                            </td>
		                            <td>
		                                2016.02.01
		                            </td>
		                            <td>
		                                <span class="notice_ok">행정</span>
		                            </td>
		                            <td class="t_left">
		                                <a href="sub_6_1_rep.html">봉사기간이 만료되었습니다.</a>
		                            </td>
		                        </tr>
		                        -->
		                    </tbody>
		                </table>             
	            	</div>
	            <div class="table-list-num" id="pagingLayer"></div>
            </div>
            
            <div id="noticeWriteLayer" class="innerLayerInContent" style="display:none;">
            	<div class="inLeft">
	            	<p class="redBtn"><a href="javascript:toggleLayer('boardLayer')">리스트보기</a><p>
				</div>

			    <div class="inView">
					<ul>
						<li class="fLine">
							제 목
							<input type="text" class="txTitle" />
						</li>
						<li class="wrtCnts">
							<textarea class="wrtNotice textAreaCustomStyle"></textarea> 
						</li>
					</ul>
				    <div class="wrtBtns">
						<p class="pd10">
							<a href="javascript:addNotice();" class="wrtMod mgn10">등록하기</a>
							<a href="javascript:toggleLayer('boardLayer')" class="wrtDel">취소</a>
						</p>
					</div>
				</div>
            </div>
            
            <div id="noticeViewLayer" class="innerLayerInContent" style="display:none;">
            	<div class="inLeft">       
            		<p class="redBtn"><a href="javascript:toggleLayer('boardLayer')">리스트보기</a><p>
				</div>
				<div class="inView">
					<ul class="titleContentsLayer">
						<li class="fLine">공지제목 <span class="color-gray"> | </span>평가기간이 언제부터인가요?</li>
						<li class="sLine">평가를 준비해야하는데 기간이 언제부터인지 모르겠어요. </li>
					</ul>
					<div class="infoCnt">
						<ul>
						<li><a href="#" class="smBtn"><span class="redFt">&lt;</span> 이전글</a></li>
						<li class="noticeViewLayer_regDate">작성일 : 2016-02-01 11:20</li>
						<li class="noticeViewLayer_writer">작성자 : 홍길동 교수</li>
						<li><a href="#" class="smBtn">다음글 <span class="redFt">&gt;</span></a></li>
						</ul>
					</div>
				    <div class="bdTop">
						<p class="pd10"><a href="javascript:void(0)" class="wrtMod mgn10">수정하기</a><a href="javascript:void(0)" class="wrtDel">삭제하기</a></p>
					</div>
            	</div>
            </div>
			
			<div id="detailLayer" class="innerLayerInContent" style="display:none;">
				<div class="inLeft">       
	            	<p class="redBtn"><a href="javascript:toggleLayer('boardLayer')">리스트보기</a><p>
					<p class="redBtn"><a href="javascript:toggleLayer('noticeWriteLayer')">공지등록</a><p>
				</div>
				<div class="inView">
					<ul>
						<li class="fLine">
							제 목 
							<span class="color-gray"> | </span>
							<span  id="detailLayerTitle">평가기간이 언제부터인가요?</span>
						</li>
						<li class="sLine">
							<textarea id="detailLayerContents" class="textAreaCustomStyle" disabled style="margin: 0px;width: 850px;resize: none;background-color:white;border:solid 0px;"> 
							평가를 준비해야하는데 기간이 언제부터인지 모르겠어요.
							</textarea> 
						</li>
					</ul>
					<div class="infoCnt">
					</div>
					<!-- infoCnt 
					<div class="infoCnt">
						<ul>
						<li><a href="#" class="smBtn"><span class="redFt">&lt;</span> 이전글</a></li>
						<li class="date">작성일 : 2016-02-01 11:20</li>
						<li class="write">작성자 : 홍길동 교수</li>
						<li><a href="#" class="smBtn">다음글 <span class="redFt">&gt;</span></a></li>
						</ul>
					</div>
	
				    <div class="inRep">
						<p class="rpView">
							아직 결정되지 않았습니다. 결정되면 My - 알림 메뉴에서 평가기간을 확인하실 수 있도록 안내 드릴 예정입니다. 아직 결정되지 않았습니다. 결정되면 My - 알림 메뉴에서 평가기간을 확인하실 수 있도록 안내 드릴 예정입니다.아직 결정되지 않았습니다. 결정되면 My - 알림 메뉴에서 평가기간을 확인하실 수 있도록 안내 드릴 예정입니다.아직 결정되지 않았습니다. 결정되면 My - 알림 메뉴에서 평가기간을 확인하실 수 있도록 안내 드릴 예정입니다.아직 결정되지 않았습니다. 결정되면 My - 알림 메뉴에서 평가기간을 확인하실 수 있도록 안내 드릴 예정입니다.아직 결정되지 않았습니다. 결정되면 My - 알림 메뉴에서 평가기간을 확인하실 수 있도록 안내 드릴 예정입니다. 		
						</p>
						<p class="rpWriter">행정1님 답변등록일 : 2016-02-02 12:20</p>
						<p class="pd10">
							<a href="sub_6_1_view.html" class="rpMod mgn10"> 수정</a>
							<a href="sub_6_1_view.html" class="rpDel"> 삭제</a>
						</p>
					</div>
					<div class="inRep">
						<p class="rpView">
							질문이예요 		
						</p>
						<p class="rpWriter">순실이 답변등록일 : 2016-02-02 12:20</p>
					</div>
					<div class="inRep">
						<p class="pd10"><textarea style="background-color: #f3f3f3" disabled></textarea></p>
						<p class="pd10"><a href="sub_6_1_view.html" class="smtWrite"> 답변등록</a></p>
					</div>
					-->
				</div>
			</div>
			
        </div>
        <!-- //.content -->
    </div>
    <!-- //.container -->
	<%@ include file="../include/bottom.jsp" %>
</div>
<!-- //#wrap -->
<link rel="stylesheet" type="text/css" href="css/datetime.css"> 
<link rel="stylesheet" type="text/css" href="<c:url value="/board/css/main.css"/>" />
<script type="text/javascript" src=<c:url value="/js/paging.js"/>></script>
<script>
$(document).ready(function(){
	 $("#search_name").keydown(function (key) {
	     if(key.keyCode == 13){//키가 13이면 실행 (엔터는 13)
	     	 $(".btn-search-name").click();
	     }
	 });
    // 이름 검색창 체크
    $(".btn-search-name").bind("click", function(){
        var input = $(this).siblings("input[type='text']");
        if ( input.val() == 0 ){
            alert("검색할 이름을 입력해주세요.");
            input.focus();
        }else{
        	if(searchOption == null){
        		searchOption = new Object();
        	}
        	searchOption.userName = input.val();
			var userType = $("#searchUserType").val();
			if(userType == "p"){
				searchOption.userLevel = 2;	
			}else if(userType == "s"){
				searchOption.userLevel = 1;
			}else{
				searchOption.userLevel = null;
			}
			//console.debug("==================find it");
			//console.debug(searchOption);
			getBoardList(1);
        }
        
    });
    
    // 테이블 라인 배경
    $(".service-table tr:even").css("background","#fdfdfd");
    
    // 이용자 삭제 팝업
    $(".btn-del").bind("click", function(){
        var tableName = $(this).parent().siblings().children(".tablename").text();
        var tablePosition = $(this).parent().siblings().children(".tableposition").text();
        $(".wrap-del-popup").fadeIn(200);
        $(".popup-guide-content span.popname").text("'" + tableName + "'");
        $(".popup-guide-content span.popposition").text(tablePosition);
    });
    $(".btn-close-del-popup").bind("click", function(){
        $(".wrap-del-popup").fadeOut(200);
    });
    
    $(".autoheight").on("keyup", "textarea", function (e){
        $(this).css("height", "auto");
        $(this).height( this.scrollHeight );
    });

    $("#answeredTotal").change(function(e){
    	if($(this).is(":checked")){
    		$(".answeredCheckBox").attr("checked",true);
    		$(".answeredCheckBox").prop('checked', true)
    		
    	}else{
    		$(this).attr("checked", false);
    	}
    	changeCheckOption("all");
    });
    $("#answeredNotice").change(function(e){
		if($(this).is(":checked")){
			$(this).attr("checked", true);
    	}else{
    		$(this).attr("checked", false);
    		$("#answeredTotal").attr("checked",false);
    		$("#answeredTotal").prop("checked",false);
    	}
		changeCheckOption("all");
    });
	$("#answeredYet").change(function(e){
		if($(this).is(":checked")){
			$(this).attr("checked", true);
    	}else{
    		$(this).attr("checked", false);
    		$("#answeredTotal").attr("checked",false);
    		$("#answeredTotal").prop("checked",false);
    	}
		changeCheckOption("all");
    });
	$("#answeredDone").change(function(e){
		if($(this).is(":checked")){
			$(this).attr("checked", true);
    	}else{
    		$(this).attr("checked", false);
    		$("#answeredTotal").attr("checked",false);
    		$("#answeredTotal").prop("checked",false);
    	}
		changeCheckOption("all");
	});
    
});

$(".service-nav a").removeClass("active");
$(".service-nav a").eq(5).addClass("active");
var userLevel = "${sessionScope.userlevel}";
var userName = "${userName}";
var userCode = "${sessionScope.userCode}";
var boardList = ${json.boardList};
var pagingInfo = ${json.pagingInfo};
if(boardList != undefined && boardList != null && boardList !=""){
	setBoardList(boardList);	
	setPagingData(pagingInfo);
}

var boardDetailObject = null;
var searchOption = null;
function toggleLayer(layerId){
	$(".innerLayerInContent").hide();
	$(".contents-serach-btns-div").hide();
	switch(layerId){
	case "noticeWriteLayer" :
		$("#noticeWriteLayer").fadeIn(300);
		break;
	case "noticeViewLayer" : 
		$("#noticeViewLayer").fadeIn(300);
		break;
	case "detailLayer" :
		$("#detailLayer").fadeIn(300);
		break;
	case "boardLayer" :
		$("#boardLayer").fadeIn(300);
		$(".contents-serach-btns-div").show();
		break;
	}
}
function getPageList(pageNo){
	getBoardList(pageNo);
	if(searchOption == null){
		searchOption = new Object();
	}
	searchOption.pageNo = pageNo;
}
function changeCheckOption(mode){
	var notice = $("#answeredNotice").is(":checked");
	var yet = $("#answeredYet").is(":checked");
	var done = $("#answeredDone").is(":checked");
	console.debug("notice  / yet  /  done \n" + notice + "  " + yet +"  "+ done );
	if(!notice && !yet && !done ){
		return;
	}
	var pageNo = 1;
	if(searchOption == null){
		searchOption = new Object();
		searchOption.pageNo = 1;
	}
	if(notice && yet && done ){
		searchOption.optionString = null;
		console.debug("alll~~~~~~~~~");
		searchOption.optionString = null;
		searchOption.userName = null;
		searchOption.userLevel = null;
		$("#search_name").val("");
		$("#searchUserType").val("a");
		getBoardList(1);		
		return;
	}
	
	if(notice){
		searchOption.optionString = " board.boardType= 'notice'";
		if(done && !yet){
			searchOption.optionString = " board.boardType= 'notice' OR board.answered = 'Y'";
			//return;
		}
		if(yet && !done){
			searchOption.optionString = " board.boardType= 'notice' OR board.answered = 'N'";
			//return;
		}
	}
	
	if(!notice){
		searchOption.optionString = "boardType = 'qna'";
		if(done && !yet){
			searchOption.optionString = " board.boardType= 'qna' AND board.answered = 'Y'";
			//return;
		}
		if(yet && !done){
			searchOption.optionString = " board.boardType= 'qna' AND board.answered = 'N'";
			//return;
		}
	}
	console.debug(searchOption.optionString);	
	getBoardList(1);
}

function getBoardList(pageNo){
	var param = {};
	param.pageNo = pageNo;
	param.rowCount = 10;
	
	if(searchOption != null){
		if(searchOption.optionString != null && searchOption.optionString.length > 10){
			param.optionString = searchOption.optionString;	
		}
		if(searchOption.userLevel != null && searchOption.userLevel > 0){
			param.userLevel = searchOption.userLevel;
		}
		if(searchOption.userName != null && searchOption.userName.length > 0){
			param.userName = searchOption.userName;
		}
		var searchYear = parseInt($("#searchYear").val(),10);
		if(searchYear > 0){
			param.searchYear = searchYear;
			alert("!!!! searchYear =>"+ param.searchYear);
		}
		
			
	}
	console.debug("=======================getBoardList param");
	console.debug(param);

	$.ajax({
        type: "POST"
        ,url: "./getBoardList/"
        ,data: param
        ,dataType: "json"
        ,async: true
        ,success: function(json){
        	console.debug(json);
            if(json == undefined && json == null){
           		return;
            }
            if(json.boardList != undefined && json.boardList != null){
            	setBoardList(json.boardList);
            	setPagingData(json.pagingInfo);
            }
            if(json.status == "fail"){
            	alert(json.message);
            	return;
            }
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
            alert("오류가 발생하였습니다");
        }
        ,complete: function() {
            	
        }
    });
}
function setBoardList(json){
	console.debug(json);
	
	function sLength(string){
		return string.length > 100 ? (string.substring(0,40)+ "..."): string;
	}
	
	$("#boardListLayer").empty();
	var html = "<tr class=\"title_bg\"><td>답변여부</td><td>등록일</td><td>등록자</td><td>문의내용</td></tr>";
	for(var i=0; i < json.length; i++){
		var data = json[i];
		if(data.boardtype == "notice"){
			var userName = data.writerusername;
			if(data.userlevel == 0){userName = "행정팀";}
			html += "<tr>";
			html += "<td>";
			html += 	"<span class=\"notice_return\">공지</span>";
			html += "</td>";
			html += "<td>";
			html +=		data.regdate;
			html += "</td>";
			html += "<td>";
			html +=		"<span class=\"notice_ok\">"+ userName + "</span>";
			html += "</td>";
			html += "<td class=\"t_left\">";
			html +=		"<a href=\"javascript:goToDetailContents('"+ data.boardtype +"','"+ data.boardcode + "')\">";
			html +=			  stripTag(sLength(data.title));
			html +=		 "</a>";	
			html += "</td>";
			html += "</tr>";			
		}else if(data.boardtype =="qna"){
			html += "<tr>";
			html += 	"<td>";
			if(data.answered == "N"){
				html += 	"<span class=\"color-red\">[미등록]</span>";	
			}else{
				html += 	"<span class=\"\">[완료]</span>";
			}
			html += 	"</td>";
			html += 	"<td>";
			html += 		data.regdate;
			html += 	"</td>";

			if(data.writerusercode != undefined && data.writerusercode > 0 && data.userlevel == 1){
				html += 	"<td>";
				html += 		"<span class=\"notice_ok\">"+ data.writerusername +"</span>";
				html += 	"</td>"
			}else if(data.writerusercode != undefined && data.writerusercode > 0 && data.userlevel == 0){
				html += 	"<td>";
				html += 		"<span class=\"notice_ok\">행정팀</span>";
				html += 	"</td>"
			}else if(data.userlevel == 2){
				html += 	"<td>";
				html += 		"<span class=\"\">"+ data.writerusername +"</span>";
				html += 	"</td>";
			}
			
			if(data.replyorder == 0){
				html += 	"<td class=\"t_left\">";	
				html +=			"<a href=\"javascript:goToDetailContents('"+ data.boardtype +"','"+ data.boardcode + "')\">";
				html +=				  stripTag(sLength(data.contents));
				html +=			 "</a>";	
				html += 	"</td>";
				
			}else{
				html += 	"<td class=\"t_left\" style=\"padding-left:30px\">";
				html +=			"<a href=\"javascript:goToDetailContents('"+ data.boardtype +"','"+ data.maingroupboardcode + "')\">";
				html +=				  "<span style='font-size:20px;'>└&nbsp;</span> " +stripTag(sLength(data.contents));
				html +=			 "</a>";	
				html += 	"</td>";
			}
			
			html += "</tr>";
		}
	}
	$("#boardListLayer").append(html);
	
}
function toggleSearchBar(isShow){
	if(isShow){
		$(".contents-serach-btns-div").show();
	}else{
		$(".contents-serach-btns-div").hide();	
	}
}
function toggleNoticeWriteLayer(isShow){
	if(isShow){
		toggleSearchBar(false);
		$("#boardLayer").hide();
		$("#noticeWriteLayer").show();		
	}else{
		toggleSearchBar(true);
		$("#boardLayer").show();
		$("#noticeWriteLayer").hide();
	}
}
function goToDetailContents(boardType,boardCode){
	console.debug("goToDetailContents =>" + boardType + "/" + boardCode);
	
	$.ajax({
        type: "POST"
        ,url: "./getDetailInfo/"
        ,data: {
           	"boardType" : boardType
           	,"boardCode" : boardCode
        }
        ,dataType: "json"
        ,async: true
        ,success: function(json){
        	console.debug(json);
            if(json == undefined && json == null){
           		return;
            }
            if(json.status =="success"){
            	setBoardDetailData(json);
            }
            if(json.status == "fail"){
            	alert("오류가 발생 하였습니다.");
            	return;
            }
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
            alert("오류가 발생하였습니다");
        }
        ,complete: function() {
            	
        }
    });
}

function boardDataUpdate(isReply,mode, boardCode,groupId,replyOrder){
	console.debug("boardDataUpdate =>"+ isReply +" / mode:" + mode + " /bordCode:" + boardCode + " / groupId:"+ groupId + " / replyOrder:" + replyOrder);
	
	if(isReply){
		if(mode == "modify"){
			if(confirm("작성한 댓글 내용을 수정하시겠습니까?")){
				var param = {}
				param.boardCode = boardCode;
				param.title = "";
				param.contents = $("#reply_" + replyOrder).val();
				//param.replyOrder = replyOrder;
				updateBoardQna(param);
			}
		}
		if(mode == "delete"){
			if(confirm("작성한 댓글을 삭제하시겠습니까?")){
				console.debug(boardCode + " / " + groupId);
				var param = {}
				param.boardCode = boardCode;
				param.replyOrder = replyOrder;
				param.use_flag = 'N'
				updateBoardQna(param);
			}
		}
	}else{	
		if(mode == "modify"){
			if(confirm("문의사항을 수정하시겠습니까?")){
				var title = $("#detailLayer .tit_a").val();
				var contents = $("#detailLayer .in_memo").val();
				var param = {}
				param.boardCode = boardCode;
				param.title = title;
				param.contents = contents;
				param.replyOrder = replyOrder;
				updateBoardQna(param);
			}
		}
		if(mode == "delete"){
			if(confirm("문의사항을 삭제하시겠습니까?")){
				console.debug(boardCode + " / " + groupId);
				var param = {}
				param.groupId = groupId;
				param.use_flag = 'N'
				updateBoardQna(param);
			}
			
		}
	}
}
function updateBoardQna(param){
	$.ajax({
        type: "POST"
        ,url: "./updateBoardQna"
        ,data: param
        ,dataType: "json"
        ,async: true
        ,success: function(json){
        	console.debug(json);
            if(json == undefined && json == null){
           		return;
            }
            if(json.status =="success"){
            	getBoardList(1);
            	goToDetailContents("qna",boardDetailObject.boardCode);
            }
            if(json.status == "fail"){
            	alert("오류가 발생 하였습니다.");
            	return;
            }
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
            alert("오류가 발생하였습니다");
        }
        ,complete: function() {
            	
        }
    });
}
function addBoardQna(mode,object){
	
	var param = {};
	if(mode =="reply"){
		param.groupId = boardDetailObject.groupId;
		param.title = ""
		param.contents = stripTag($("#newReplyTextArea").val());
		param.answered = "Y";
		param.userCode = userCode;
		param.askUserCode = boardDetailObject.askUserCode;
	}
	console.debug("userCode =>"+ param.userCode);
	console.debug("askUserCode =>"+ param.askUserCode);
	
	$.ajax({
        type: "POST"
        ,url: "./addBoardQna"
        ,data: param
        ,dataType: "json"
        ,async: true
        ,success: function(json){
        	console.debug(json);
            if(json == undefined && json == null){
           		return;
            }
            if(json.status =="success"){
            	getBoardList(1);
            	goToDetailContents("qna",boardDetailObject.boardCode);
            }
            if(json.status == "fail"){
            	alert("오류가 발생 하였습니다.");
            	return;
            }
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
            alert("오류가 발생하였습니다");
        }
        ,complete: function() {
            	
        }
    });
	
}
function setBoardDetailData(json){
	console.debug("setBoardDetailData============");
	console.debug(json);
	
	
	var writerName = null;
	var regDate = null
	if(json.notice == undefined ){
		toggleLayer("detailLayer");
		//$("#detailLayer").fadeIn(300);
		$("#detailLayer .widthbox").empty();
		$("#detailLayer .wri_data").show();
		
		
		boardDetailObject = new Object();
		$("#detailLayer .inRep").remove();
		var writerName = null;
		var regDate = null;
		for(var i=0; i < json.qna.length; i++){
			var qna = json.qna[i];
			
			boardDetailObject.groupId = typeof boardDetailObject.groupId == "undefined" ? qna.groupid : boardDetailObject.groupId ;
			boardDetailObject.boardCode = typeof boardDetailObject.boardCode == "undefined" ? qna.boardcode : boardDetailObject.boardCode;
			boardDetailObject.askUserCode =  typeof boardDetailObject.askusercode == "undefined" ? qna.askusercode : boardDetailObject.askUserCode;
			
			if(qna.replyorder == 0){
				$("#detailLayerTitle").html(qna.title);
				$("#detailLayerContents").val(qna.contents);
				writerName = "작성자: "+ qna.user_name;
				regDate = "작성일: "+ qna.reg_date;
				$("#detailLayer .date").html();
				$("#detailLayer .write").html(writerName);
			}else{
				var html = "";
				if(qna.askusercode != qna.writerusercode && qna.user_level < 2){  //관리자인경우
					var userName = qna.user_name;
					if(qna.user_level == 0){
						userName = "행정팀";
					}
					
					html += "<div class=\"inRep\" id=\"boardReply_" + qna.boardcode +"\">";
					html += "	<p class=\"rpView\">"; //#f3f3f3
					html += 		"<textarea style=\"background-color:#f3f3f3 ;height:20px;min-height:20px;resize:none;border:solid 0px white;\" disabled >" + qna.contents+"</textarea>"
					html += "	</p>";
					html +=	"<p class=\"rpWriter\">"+ userName + "님 답변등록일 : " + qna.reg_date +"</p>";
					console.debug("qna.user_level:"+ qna.user_level);
					//if(qna.answerusercode == userCode){  //본인이거나 슈퍼관리자만 수정삭제가능
						html +=	"<p class=\"pd10 replybuttonLayer\">";
						html +=		"<a href=\"javascript:updateReply('"+ qna.boardcode +"','modify')\" class=\"rpMod mgn10\"> 수정</a>";
						html +=		"<a href=\"javascript:boardDataUpdate(true,'delete','"+ qna.boardcode + "','"+ qna.groupid +"','"+ qna.replyorder +"')\" class=\"rpDel\"> 삭제</a>";
						html +=	"</p>";	
					//}
					html += "</div>"
				}else{					//교수인경우
					html += "<div class=\"inRep\" id=\"boardReply_" + qna.boardcode +"\">";
					html += "	<p class=\"rpView\">";
					html += 		"<textarea style=\"background-color: #f3f3f3 ;height:20px;min-height:20px;resize:none;border:solid 0px white;\" disabled >" + qna.contents+"</textarea>" 		
					html += 	"</p>";
					html += 	"<p class=\"rpWriter\" >"+ qna.user_name + " 답변등록일 : " + qna.reg_date + "</p>";
					if(i == json.qna.length){
					html += 	"<p class=\"pd10\" id=\"pd10_" + qna.boardcode + "\"><a href=\"javascript:addReplyForm('"+qna.boardcode+ "')\" class=\"smtWrite\"> 답변등록</a></p>";	
					}
					html += "</div>";
				}
				$("#detailLayer .inView").append(html);
			}
		}
		
		var html ="";
		html += "<div class=\"inRep\">";
		html += "<p class=\"pd10\"><textarea id=\"newReplyTextArea\" class=\"textAreaCustomStyle\" style=\"border: solid 1px #999\"></textarea></p>";
		html += "<p class=\"pd10\"><a href=\"javascript:addBoardQna('reply')\" class=\"smtWrite\"> 답변등록</a></p>";
		html += "</div>";
		
		$("#detailLayer .inView").append(html);
		
		if(json.assignInfo != undefined && json.assignInfo != null){
			var prevHref = "javascript:goToDetailContents('"+ json.assignInfo.prevBoardType +"','"+ json.assignInfo.prevBoardCode + "')";
			var nextHref= "javascript:goToDetailContents('"+ json.assignInfo.nextBoardType +"','"+ json.assignInfo.nextBoardCode + "')"
			if(json.assignInfo.prevBoardCode == 0){
				prevHref = "javascript:alert('제일 처음 글 입니다')";
			}
			if(json.assignInfo.nextBoardCode == 0){
				nextHref = "javascript:alert('제일 마지막 글 입니다')";
			}
			var html = "";
			html += 	"<ul>";
			html += 		"<li>";
			html +=				"<a class=\"smBtn\" href=\""+ prevHref + "\">"; 
			html += 				"<span class=\"redFt\">&lt;</span> 이전글";
			html += 			"</a>";
			html += 		"</li>";
			html += 		"<li class=\"date\">"+ "작성일 : "+ regDate +"</li>";
			html += 		"<li class=\"write\">" + writerName + "</li>";
			html += 		"<li>";
			html +=				"<a class=\"smBtn\" href=\""+ nextHref + "\">";
			html +=					"다음글 <span class=\"redFt\">&gt;</span>";
			html +=				"</a>";
			html +=			"</li>";
			html += 	"</ul>";
			
			$("#detailLayer .infoCnt").html(html);
		}
	}else if(json.qna == undefined && json.notice != undefined && json.notice != null){
		console.debug(json.notice.title + " "+ json.notice.contents);
		boardDetailObject = new Object();
		boardDetailObject.groupId = -1;
		boardDetailObject.boardCode = json.notice.board_notice_code;
		boardDetailObject.boardType = "notice";
		console.debug("json.notice.groupid =>"+ json.notice.groupid );
		console.debug("json.notice.boardcode =>"+ json.notice.boardcode  );
		var html ="";
		html += "	<li class=\"fLine\">공지제목 <span class=\"color-gray\"> | </span><span class=\"noticeTitle\">" + json.notice.title + "</span></li>";
		html += "	<li class=\"sLine\">";
		html += "       <textarea id=\"\" disabled style=\"margin: 0px;width: 850px;resize: none;background-color:white;border:solid 0px;\">";
		html += 			json.notice.contents;
		html += "		</textarea>"; 
		html += "	</li>";
		$("#noticeViewLayer .titleContentsLayer").html(html);
		if(json.notice.user_level == 0){
			writerName = "작성자 : 행정팀";
		}else{
			writerName = "작성자 : "+ json.notice.user_name;
		}
		
		if(json.assignInfo != undefined && json.assignInfo != null){
			var prevHref = "javascript:goToDetailContents('"+ json.assignInfo.prevBoardType +"','"+ json.assignInfo.prevBoardCode + "')";
			var nextHref= "javascript:goToDetailContents('"+ json.assignInfo.nextBoardType +"','"+ json.assignInfo.nextBoardCode + "')"
			if(json.assignInfo.prevBoardCode == 0){
				prevHref = "javascript:alert('제일 처음 글 입니다')";
			}
			if(json.assignInfo.nextBoardCode == 0){
				nextHref = "javascript:alert('제일 마지막 글 입니다')";
			}
			var html = "";
			html += 	"<ul>";
			html += 		"<li>";
			html +=				"<a class=\"smBtn\" href=\""+ prevHref + "\">"; 
			html += 				"<span class=\"redFt\">&lt;</span> 이전글";
			html += 			"</a>";
			html += 		"</li>";
			html += 		"<li class=\"date\">"+ "작성일 : "+ json.notice.reg_date +"</li>";
			html += 		"<li class=\"write\">" + writerName + "</li>";
			html += 		"<li>";
			html +=				"<a class=\"smBtn\" href=\""+ nextHref + "\">";
			html +=					"다음글 <span class=\"redFt\">&gt;</span>";
			html +=				"</a>";
			html +=			"</li>";
			html += 	"</ul>";
			
			$("#noticeViewLayer .infoCnt").html(html);
		}
		
		var html = "";
		html += "<p class=\"pd10\">";
		html += "	<a href=\"javascript:updateNotice("+ json.notice.board_notice_code + ",'modify')\" class=\"wrtMod mgn10\">수정하기</a>";
		html += "	<a href=\"javascript:updateNotice("+ json.notice.board_notice_code + ",'delete')\" class=\"wrtDel\">삭제하기</a>";
		html += "</p>";
		$("#noticeViewLayer .bdTop").html(html);
		
		toggleLayer("noticeViewLayer");
	}
	adjustTextAreaSize();
	
}


var replyOriginalValue = null;
function updateReply(boardCode,mode){
	console.debug("updateReply =>"+ boardCode + " "+ mode);
	
	if(boardCode == undefined || boardCode < 0){
		return;
	}
	if(mode == "modify"){
		replyOriginalValue = $("#boardReply_" + boardCode + "").html();
		var html = "";
		html +=		"<a href=\"javascript:updateReply("+ boardCode +",'update')\" class=\"rpMod mgn10\">등록</a>";
		html +=		"<a href=\"javascript:updateReply("+ boardCode + ",'cancel')\" class=\"rpDel\">취소</a>";
		$("#boardReply_" + boardCode + " .replybuttonLayer").html(html);
		
		$("#boardReply_" + boardCode + " textarea").attr("disabled",false);
		$("#boardReply_" + boardCode + " textarea").css({
			"min-height" : "130px"
			,"overflow-x" : "hidden"
			,"overflow-y" : "auto"
			,"font-family" : "NanumGothic"
			,"padding" : "5px"
			,"background-color":"white"
			
		});		
		$("#boardReply_" + boardCode + " textarea").focus();
	}else if(mode == "cancel"){
		$("#boardReply_"+ boardCode).html(replyOriginalValue);
		replyOriginalValue= null;
	}else if(mode == "update"){
		var contents = $("#boardReply_" + boardCode + " textarea").val();
		var param = {};
		param.contents = contents;
		param.boardCode = boardCode;
		param.groupId = boardDetailObject.groupId;
		param.title ="";
		
		$.ajax({
	        type: "POST"
	        ,url: "./updateBoardQna"
	        ,data: param
	        ,dataType: "json"
	        ,async: true
	        ,success: function(json){
	        	console.debug(json);
	            if(json == undefined && json == null){
	           		return;
	            }
	            if(json.status == "fail"){
	            	alert("오류가 발생 하였습니다.");
	            	return;
	            }
	            if(json.status =="success"){
	            	goToDetailContents("qna",boardDetailObject.boardCode);
	            }
	        }
	        ,error:function (xhr, ajaxOptions, thrownError){
	        	console.error(xhr);
	            console.error(ajaxOptions);
	            console.error(thrownError);
	            alert("오류가 발생하였습니다");
	        }
	        ,complete: function() {
	            	
	        }
	    });
	}	
}
function addNotice(){
	var title =$("#noticeWriteLayer .txTitle").val();
	var contents =$("#noticeWriteLayer .wrtNotice").val();
	
	console.debug(title + " / "+ contents);
	if(title.length == 0){
		alert("공지 제목을 입력하세요");
		return;
	}
	if(contents.length == 0){
		alert("공지 내용을 입력하세요");
		return;
	}
	title = convert(title);
	//contents = convert(contents);
	$.ajax({
        type: "POST"
        ,url: "./addNotice/"
        ,data: { "userCode":userCode, "title": title , "contents" : contents}
        ,dataType: "json"
        ,async: true
        ,success: function(json){
        	console.debug(json);
            if(json == undefined && json == null){
           		return;
            }
            if(json.status == "fail"){
            	alert("오류가 발생 하였습니다.");
            	return;
            }
            if(json.status =="success" && json.result > 0){
            	goToDetailContents("notice",json.result);	
            }
            getBoardList(1);
        }
        ,error:function (xhr, ajaxOptions, thrownError){
        	console.error(xhr);
            console.error(ajaxOptions);
            console.error(thrownError);
            alert("오류가 발생하였습니다");
        }
        ,complete: function() {
            	
        }
    });	
}


var noticeOrignalHtml = null;
function updateNotice(boardCode, mode){
	console.debug("boardCode =>"+ boardCode +  mode);
	if(boardCode > 0  && mode == "delete"){
		if(confirm("공지사항을 삭제 하시겠습니까?")){
			var param = {};
			param.boardCode = boardCode;
			param.useFlag = "N";
			
			$.ajax({
		        type: "POST"
		        ,url: "./updateNotice/"
		        ,data: param
		        ,dataType: "json"
		        ,async: true
		        ,success: function(json){
		        	console.debug(json);
		            if(json == undefined && json == null){
		           		return;
		            }
		            if(json.status == "fail"){
		            	alert("오류가 발생 하였습니다.");
		            	return;
		            }
		            getBoardList(1);
		            toggleLayer("boardLayer");
		        }
		        ,error:function (xhr, ajaxOptions, thrownError){
		        	console.error(xhr);
		            console.error(ajaxOptions);
		            console.error(thrownError);
		            alert("오류가 발생하였습니다");
		        }
		        ,complete: function() {
		            	
		        }
		    });	
		}
	}else if(boardCode > 0 && mode =="modify"){
		noticeOrignalHtml = $("#noticeViewLayer .inView").html();
		
		var title = $("#noticeViewLayer .noticeTitle").html();
		var contents = $("#noticeViewLayer textArea").val();
		
		console.debug(title + "\n"+ contents);
		$("#noticeViewLayer .inView .fLine").html("제 목<input type=\"text\" class=\"txTitle\" value=\""+ title + "\" />");
		$("#noticeViewLayer .inView .sLine").html("<textarea class=\"wrtNotice\">" + contents + "</textarea>");
		
		var html = "";
		html += "<a href=\"javascript:updateNotice("+boardCode+",'update')\" class=\"wrtMod mgn10\">수정하기</a>";
		html += "<a href=\"javascript:goToDetailContents('notice'," + boardCode +");\" class=\"wrtDel\">취소하기</a>";
		$("#noticeViewLayer .inView .pd10").html(html);
		
	}else if(boardCode > 0 && mode =="update"){
		var title = $("#noticeViewLayer .inView .fLine input").val();
		var contents = $("#noticeViewLayer .inView .sLine textArea").val();
		console.debug(title + "\n"+ contents);
		
		var param = {};
		param.boardCode = boardCode;
		param.title = title;
		param.contents = contents;
		console.debug(param);
		$.ajax({
	        type: "POST"
	        ,url: "./updateNotice/"
	        ,data: param
	        ,dataType: "json"
	        ,async: true
	        ,success: function(json){
	        	console.debug(json);
	            if(json == undefined && json == null){
	           		return;
	            }
	            if(json.status == "fail"){
	            	alert("오류가 발생 하였습니다.");
	            	return;
	            }
	            goToDetailContents("notice",boardCode);
	        }
	        ,error:function (xhr, ajaxOptions, thrownError){
	        	console.error(xhr);
	            console.error(ajaxOptions);
	            console.error(thrownError);
	            alert("오류가 발생하였습니다");
	        }
	        ,complete: function() {
	            	
	        }
		});
	}
}
function addReplyForm(boardCode){
	if(boardCode > 0){
		$("#pd10_"+ boardCode).hide();
		var html ="";
		html += "<div class=\"inRep\" id=\"newReplyForm\">";
		html += 	"<p class=\"pd10\">";
		html +=         "<textarea style=\"resize: none;\"></textarea>";
		html +=     "</p>";
		html +=	 	"<p class=\"pd10\">";
		html +=			"<a href=\"javascript:addBoardQna('reply')\" class=\"smtWrite\"> 답변등록</a>&nbsp;&nbsp;";
		html += 		"<a href=\"javascript:$('#newReplyForm').remove();$('#pd10_"+ boardCode + "').show();\" class=\"smtWrite\" style=\"background:#666\"> 취소</a>";
		html += 	"</p>";
		html += "</div>";
		$("#detailLayer .inView").append(html);
		$("body").scrollTop($(document).height());
		$("#detailLayer .inView textarea").last().focus();	
	}	
}
function stripTag (str){
    return str.replace(/(<([^>]+)>)/ig,"");
}
function convert(str){
	 str = str.replace(/</g,"&lt;");
	 str = str.replace(/>/g,"&gt;");
	 str = str.replace(/\"/g,"&quot;");
	 str = str.replace(/\'/g,"&#39;");
	 str = str.replace(/\n/g,"<br />");
	 return str;
	}
function adjustTextAreaSize(object){
	if(object != undefined){
		object.each(function(){
		    while($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
		        $(this).height($(this).height()+1);
		    };
		});	
	}else{
		$("textArea").each(function(){
		    while($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
		        $(this).height($(this).height()+1);
		    };
		});
	}
}

</script>

<style>
.textAreaCustomStyle{
	resize :none;
	background-color:white;
	border:solid 1px #999;
}
</style>
<%@ include file="../include/footer.jsp" %>