<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="author" content="NS Devilⓒ" />
<meta name="format-detection" content="telephone=no" />
<title>경북대학교 의학전문대학원 교수 업적 평가 관리 시스템</title>
<link rel="stylesheet" type="text/css" href="css/datetime.css"> 
<link rel="stylesheet" type="text/css" href="css/main.css" />
<script src="js/jq.js"></script>
<!--[if lte IE 9]>
<link rel="stylesheet" type="text/css" href="css/ie9.css">
<script src="js/ph.js"></script>
<![endif]-->
<!--[if lte IE 8]>
<link rel="stylesheet" type="text/css" href="css/ie8.css"> 
<script src="js/el.js"></script>
<script src="js/es5.js"></script>
<![endif]-->
<!--[if lte IE 7]>
<script>
$(document).ready(function(){
$(".pop-up-ie7").removeClass("hidden");
$(".btn-popup-close").bind("click",function(){
$(".pop-up-ie7").addClass("hidden");
});
});
</script>
<![endif]-->
</head>
<body>
<!-- 익스7 이하 버젼 경고팝업 -->
<div class="pop-up-ie7 hidden">
    <div class="popup-ie7">
        <div class="popup-ie7-header">
            <p>Internet Explorer 버젼 체크 알림</p>
            <p><button type="button" class="btn-popup-close">X</button></p>
        </div>
        <div class="popup-ie7-content">
            <p>이 홈페이지는 <span class="color-red">Internet Explorer 7이하 버젼</span>을 지원하지 않습니다. 구버젼 익스플로러 사용 시 기능 및 보안에 문제가 발생할 수 있습니다. 원활한 사용을 위하여 최신 익스플로러 버젼으로 업데이트 하시기 바랍니다.
            </p>
        </div>
        <div class="popup-ie7-button-div">
            <button type="button" class="btn-popup-close">CLOSE</button>
        </div>
    </div>
</div>
<!-- //.pop-up-ie7 -->

<noscript title="스크립트 미작동 안내">
    <p>
        본 사이트는 자바스크립트가 작동되어야 편리하게 이용하실 수 있습니다. 현재 스크립트가 정상적으로 작동되지 않습니다.
    </p>
</noscript>

<div id="wrap">
    <header class="header">
        <div class="logo-div">
            <img src="images/logo.png" alt="경북대학교" />
            <div class="log-div">
                <!-- 사용자 로그 정보 및 로그아웃 버튼-->
                <p>
                    <span class="top-ip">홍길동 교수님(2015.02.11. 09:24:45 접속)</span>
                    <span class="top-ip"><a href="#">비밀번호변경</a></span>
                    <a href="#">로그아웃</a>
                </p>

                <nav class="service-nav">
                    <ul>
                        <!-- 현재페이지 a태그에 클래스 "active" 추가 -->
                        <li><a href="notice.html" class="">MY</a></li>
                        <li><a href="service.html">봉사</a></li>
                        <li class="last-li-top"><a href="board.html" class="active">게시판</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <!-- //.header -->

    <div class="container">
        <div class="left_bomenu">
            <ul class="board_ul">
                <li class=""><a href="">문의하기</a></li>
				<li class=""><a href="">게시판 리스트</a></li> 
            </ul>    
        </div>
        <!-- //.leftmenu -->

        <div class="content">
            <div class="board_c">
                <table class="wr_square">
                    <colgroup>
                        <col width="6%" />
                        <col width="94%" />
                    </colgroup>
                    <tbody>
                        <tr>
                            <td>
                                제목:
                            </td>
                            <td>
                                 <input type="text" class="tit_a" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                문의 내용:
                            </td>

                        </tr>
                        <tr>
                            <td colspan="2">
                                <textarea class="in_memo" ></textarea>
                            </td>

                        </tr>

                        <tr>
                            <td colspan="2" class="wri_data">
								<span class="date">작성일: 2016-02-01 16:42</span>
								<span class="write">작성자: 나</span>
								<a href=""><span class="bt_gray">수정</span></a>
								<a href=""><span class="bt_gray">삭제</span></a> 
                            </td>
                        </tr>

                    </tbody>
                </table>             
            </div>
            <!-- //.board_c -->
            <div class="bt_bottom">
            	<a href=""><span class="bt_gray_left">< 이전 글</span></a>
                <a href=""><span class="bt_red_center">문의 댓글 남기기</span></a>
                <a href=""><span class="bt_gray_right">다음 글 ></span></a>
            </div>
            <div class="widthbox">
                <div class="box_orange">
                    <p><span class="reply_orange">└&nbsp;행정답변&nbsp;</span> <span>2016.02.02.10:12</span>
                    </p>
                    <p><textarea class="re_box"></textarea>
                    </p>
                </div>
                <div class="box_gray">
                    <p><span class="reply_gray">└&nbsp;나의답변&nbsp;</span> <span>2016.02.02.10:12</span>
                    <a href=""><span class="bt_gray">수정</span></a><a href=""><span class="bt_gray">삭제</span></a>
                    </p>
                    <p><textarea class="re_box"></textarea>
                    </p>
                </div>
            </div>
           

        </div>
        <!-- //.content -->
    </div>
    <!-- //.container -->

    <footer id="footer">
        <div class="footer_con">
            <div class="footer_address">
                <p>
                    경북대학교 의과대학 · 의과전문대학원 ADD. (41944)대구광역시 중구 국채보상로 680
                </p>
                <p>
                    TEL. 053-420-4910(교무), 4906(서무) FAX. 053-422-9195(교무), 420-4925(서무) E-mail. meddean@knu.ac.kr
                </p>
                <p>
                    Copyright(c)2014 경북대학교 의과대학·의과전문대학원 All rights reserved.
                </p>
            </div>
        </div>
    </footer>
    <!-- //.footer -->
</div>
<!-- //#wrap -->

</body>
</html>