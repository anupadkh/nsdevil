import input_sheet as ips
import functions as fn

import pandas as pd
import numpy as np

file = ('input.xlsx')
writer = pd.ExcelWriter('output.xlsx')

answer_sheet = ips.sheet()

df = pd.read_excel(file, header=[0,1,2,3], skiprows=[4], sheet_name=answer_sheet.sheetname)

correct = pd.read_excel(file, header=[0,1,2,3], nrows=1, sheet_name=answer_sheet.sheetname) # Get the correct answers

# Error in column names
institute = 'Institue'
# Subject Records
rf = pd.read_excel(file,skiprows=[1,2,3,4], sheet_name = answer_sheet.sheetname, usecols=list(range(0,8)))

# Students Records
# 1,048,576 rows and 16,384 columns in Excel Sheets
cols_to_use = list(range(8, 1000))
cols_to_use.insert(0,0)
mf = pd.read_excel(file,header=list(range(0,5)), sheet_name = answer_sheet.sheetname, usecols=cols_to_use)
mf['Examinee No.']=df.index


pdf = rf.loc[rf['Attendance'] == 'O']
del pdf['Attendance']
pdf.to_excel(writer,'Appeared Applicants')

rf['Count'] = 1
pdf = rf.loc[rf['Attendance'] == 'O']
del pdf['Attendance']
appeared = pd.pivot_table(pdf, values='Count', index=['Exam Region', 'Exam Site'], columns=['Year'], aggfunc=np.sum)

# Calculating sub-totals
tab_tots = appeared.groupby(level='Exam Region').sum()
tab_tots.index = [tab_tots.index, ['Total'] * len(tab_tots)]

appeared = pd.concat(
    [appeared, tab_tots]
).sort_index().append(
    appeared.sum().rename(('Grand', 'Total'))
)
appeared.to_excel(writer, 'Appeared Summary')



# rf['Year'] = rf['Year'].str.strip()
applicants = pd.pivot_table(rf, values='Count', index=['Exam Region', 'Exam Site'], columns=['Year'], aggfunc=np.sum)
applicants.to_excel(writer,'Applicants Summary')

applicants_institute = pd.pivot_table(rf, values='Count', index=[institute], columns=['Year'], aggfunc=np.sum)
applicants_institute.to_excel(writer,'Institution Applicants')

appeared_institute = pd.pivot_table(pdf, values='Count', index=[institute], columns=['Year'], aggfunc=np.sum)
applicants_institute.to_excel(writer,'Institution Appeared')

del rf['Count']

xyz = correct
all_headers = list(xyz)
all = [x[0] for x in all_headers]
subjects = list(set(all[all.index('Category') + 1:]))
subjects.sort(key=fn.natural_keys)
sf = df.copy()
sf['Total'] = 0
total_subjects = []
for one in subjects:
    sf[one] = (sf[one].values == correct[one].values)
    sf['Total'] = sf[one].sum(axis=1) + sf['Total']
    total_subjects.append(len(sf[one].columns))
    # sf[one].replace([True, False],['1','0'])
# sf['Total'] = sf.sum(axis = 1)
sf['subjects'] = sum(total_subjects)
sf['Percentage'] = sf['Total']/sum(total_subjects) * 100
sf['Rank'] = sf['Total'].rank(method = 'min', ascending = False)
sf['Percentage of Rank'] = sf['Rank']/len(sf)
# sf['Test'] = sf['Total'].rank(method = 'min', ascending = False,pct=True)
sf.to_excel(writer, "Score")

km = pd.DataFrame(df.index, columns = ["Examinee No."])
km = rf
km.index = df.index
for one in subjects:
    km[one,'Scored Marks'] = sf[one].sum(axis=1)
    km[one,'Rank in total'] = km[one,'Scored Marks'].rank(method = 'min', ascending = False)
    km[one,'Rank in Institute'] = km.filter([(one,'Scored Marks'),institute]).groupby([institute]).rank(method = 'min', ascending=False)
    km[one,'Rank in Same Year'] = km.filter([(one,'Scored Marks'),'Year']).groupby(['Year']).rank(method = 'min', ascending=False)

subjects_tuple = []
for a in subjects:
    subjects_tuple.append((a,'Scored Marks'))
r=[]
tup_type = type((1,2))
for a in km.columns:
  if type(a) != tup_type:
    r.append((a,' '),)
  else:
    r.append(a)

s = pd.DataFrame(np.transpose(km.values), index = pd.MultiIndex.from_tuples(r))
km = s.T.copy()
km['Total','Obtained Score'] = km.filter(subjects_tuple).sum(axis=1)
km['Total','Rank in total'] = km['Total','Obtained Score'].rank(method = 'min', ascending = False)
# km['Total','Rank in  Institute'] = km.filter([('Total','Obtained Score'),institute]).groupby([institute]).rank(method = 'min', ascending=False)
# km['Total','Rank in Same Year'] = km.filter([('Total','Obtained Score'),'Year']).groupby(['Year']).rank(method = 'min', ascending=False)

km.to_excel(writer, "Standing in Total")

# Question Analysis
m = correct.filter(like='Subject').T
# Dropping the indices (For column index use columns, For row index use index)
m['Level of Difficulty','Winners'] = sf.filter(like='Subject').sum(axis=0)
m['Level of Difficulty','Total Examinees'] = len(sf)
m['Level of Difficulty','Correct Answer Rate'] = m['Level of Difficulty','Winners']/m['Level of Difficulty','Total Examinees']

m['Answer Discrimination Rate','Winners in Top 27%'] = ((sf['Percentage of Rank'] <= .27) & sf.filter(like='Subject').T).sum(axis=1)
m['Answer Discrimination Rate','Examinees in Top 27%'] = (sf['Percentage of Rank'] <= .27).sum()
m['Answer Discrimination Rate','Winners in Low 27%'] = ((sf['Percentage of Rank'] >= .73) & sf.filter(like='Subject').T).sum(axis=1)
m['Answer Discrimination Rate','Examinees in Low 27%'] = (sf['Percentage of Rank'] >= .73).sum()
m['Answer Discrimination Rate','Distinguishable Rage'] = m['Answer Discrimination Rate','Winners in Top 27%']/m['Answer Discrimination Rate','Examinees in Top 27%'] - m['Answer Discrimination Rate','Winners in Low 27%']/m['Answer Discrimination Rate','Examinees in Low 27%']

for i in [1,2,3,4,5,0]:
    m['Number of Respondent',i] = (df.filter(like='Subject') == i).sum()
for i in [1,2,3,4,5,0]:
    m['Answer Response Rate', i] = (df.filter(like='Subject') == i).sum()/len(sf)

r=[]
tup_type = type((1,2))
for a in m.columns:
  if type(a) != tup_type:
    r.append((a,' '),)
  else:
    r.append(a)


m = pd.DataFrame(np.transpose(m.values), index = pd.MultiIndex.from_tuples(r)).copy()
m.columns = correct.filter(like='Subject').columns
m.columns = m.columns.droplevel([2,3])
m.to_excel(writer,"Question Analysis")
# f = y.filter(['Subject 1','Year']).groupby('Year').rank(method="min", ascending=False)

# df['Auction_Rank'] = df.groupby('Auction_ID')['Bid_Price'].rank(method='min', ascending=False)



# fx = pd.merge(rf,mf, on=['Examinee No.'])


writer.save()
'''
# print ('Index' + str(df.index) + '\n\n')

p = df.loc[62010004]['Subject 1']['No. 1'] # Get Series
# print (type(p))
# print(p.data)
#
# print (p[0])
print(p)

# for header in (df.columns.levels):
#     print(header)

# for y in a:
#     print(y)
#     m = df[y].columns
#     v = df[y]
#     for r in m:
#         x = v[r]
#         print (str(y) + ':' + str(r))
#         print (list(x))

# Processing in dataframes
d = (df['Subject 1'].values == correct['Subject 1'].values) # Is a 45 x 569 Matrix

# Choosing equals
# df.loc[df['column_name'] == some_value]
df.loc[df['Attendance'] == 'O']

# Choosing partialdataframes
mf = pd.read_excel(file,header=list(range(0,5)), sheet_name = answer_sheet.sheetname, usecols=list(range(8,100)), index=[0])
mf['Category']=mf.index
mf.index=df.index


# Choosing partialdataframes students
rf = pd.read_excel(file,skiprows=[1,2,3,4], sheet_name = answer_sheet.sheetname, usecols=list(range(0,8)))

# Merging two partialdataframes
result = pd.merge(left, right, on=['key1', 'key2'])
# left and right are names of dataframes. Both have keys key1, key2

# Get unique values
pd.Series([2, 1, 3, 3], name='A').unique()

# Count unique values
pd.Series([2, 1, 3, 3], name='A').value_counts()

# Sort values
m = x.value_counts()
m.sort_values()
m.sort_index()

# Saving into excel files
writer = pd.ExcelWriter('output.xlsx')
df1.to_excel(writer,'Sheet1')
df2.to_excel(writer,'Sheet2')
writer.save()

# Not writing the index
movies.to_excel('output.xlsx', index=False)

# Improve Pandas Output to ExcelWriter
# http://pbpython.com/improve-pandas-excel-output.html

PIVOTS
>>> df = pd.DataFrame({"A": ["foo", "foo", "foo", "foo", "foo",
...                          "bar", "bar", "bar", "bar"],
...                    "B": ["one", "one", "one", "two", "two",
...                          "one", "one", "two", "two"],
...                    "C": ["small", "large", "large", "small",
...                          "small", "large", "small", "small",
...                          "large"],
...                    "D": [1, 2, 2, 3, 3, 4, 5, 6, 7]})
>>> df
     A    B      C  D
0  foo  one  small  1
1  foo  one  large  2
2  foo  one  large  2
3  foo  two  small  3
4  foo  two  small  3
5  bar  one  large  4
6  bar  one  small  5
7  bar  two  small  6
8  bar  two  large  7
>>> table = pivot_table(df, values='D', index=['A', 'B'],
...                     columns=['C'], aggfunc=np.sum)
>>> table
C        large  small
A   B
bar one    4.0    5.0
    two    7.0    6.0
foo one    4.0    1.0
    two    NaN    6.0

# Counting the Universities with year_wise
rf['Count'] = 1
table = pd.pivot_table(rf, index=[institute,'Year'], aggfunc=np.sum, value='Count')

# Region_Wise Year-Wise Count
table = pd.pivot_table(rf, index=['Exam Site','Exam Region','Year'], aggfunc=np.sum, values='Count')

# Calculating Ranks
In [68]: df['Auction_Rank'] = df.groupby('Auction_ID')['Bid_Price'].rank(ascending=False)

In [69]: df
Out[69]:
   Auction_ID  Bid_Price  Auction_Rank
0         123          9             1
1         123          7             2
2         123          6             3
3         123          2             4
4         124          3             1
5         124          2             2
6         124          1             3
7         125          1             1

Series.rank(axis=0, method='average', numeric_only=None, na_option='keep', ascending=True, pct=False)[source]
Compute numerical data ranks (1 through n) along axis. Equal values are assigned a rank that is the average of the ranks of those values

Parameters:
axis : {0 or ‘index’, 1 or ‘columns’}, default 0
index to direct ranking
method : {‘average’, ‘min’, ‘max’, ‘first’, ‘dense’}
average: average rank of group
min: lowest rank in group
max: highest rank in group
first: ranks assigned in order they appear in the array
dense: like ‘min’, but rank always increases by 1 between groups
numeric_only : boolean, default None
Include only float, int, boolean data. Valid only for DataFrame or Panel objects
na_option : {‘keep’, ‘top’, ‘bottom’}
keep: leave NA values where they are
top: smallest rank if ascending
bottom: smallest rank if descending
ascending : boolean, default True
False for ranks by high (1) to low (N)
pct : boolean, default False
Computes percentage rank of data
Returns:
ranks : same type as caller
'''
