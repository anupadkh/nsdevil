import input_sheet as ips

def present_students(all_students):
    a = []
    for one in all_students:
        if one.attended():
            a.append(one)
    return a

def region_wise(all_students, region):
    a =[]
    for examinee in all_students:
        if examinee.region == region:
            a.append(examinee)
    return a

def site_wise(all_students,site):
    a = []
    for examinee in all_students:
        if examinee.examCenter == site:
            a.append(examinee)
    return a

def year_wise(all_students, year):
    a = []
    for examinee in all_students:
        if examinee.year == year:
            a.append(examinee)
    return a

def institute_wise(all_students,institute):
    a = []
    for examinee in all_students:
        if examinee.institute == institute:
            a.append(examinee)
    return a


'''

present_students(region_wise(all_students, region))
Natural Processing -- Sorting

import re

def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):

    # alist.sort(key=natural_keys) sorts in human order
    # http://nedbatchelder.com/blog/200712/human_sorting.html
    # (See Toothy's implementation in the comments)

    return [ atoi(c) for c in re.split('(\d+)', text) ]

alist=[
    "something1",
    "something12",
    "something17",
    "something2",
    "something25",
    "something29"]

alist.sort(key=natural_keys)
print(alist)
'''
import re

def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    return [ atoi(c) for c in re.split('(\d+)', text) ]
