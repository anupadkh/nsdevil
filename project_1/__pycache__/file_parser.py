import input_class as ic
import functions as fn
import pandas as pd
import numpy as np

#### 0. Parsing the input data from the excel file as needed ####

# excel-file and excel-sheet name
file_name = 'input.xlsx'
sheet_name = 'Answer Sheet(1st input)'

writer = pd.ExcelWriter('output.xlsx')

# Loading the input excel file into dataframe without correct answer row
df = pd.read_excel(file_name, header=[0, 1, 2, 3], skiprows=[4], sheet_name=sheet_name)
# Creating a new column for index
df = df.rename_axis(['Examinee No.']).reset_index()
df.columns.names = ['Index', '', ' ', '  ']

# Getting only correct answer row data
correct = pd.read_excel(file_name, header=[0, 1, 2, 3], nrows=1, sheet_name=sheet_name)
correct = correct.iloc[:,8:]
# Creating a new column for index
correct = correct.rename_axis(['Examinee No.']).reset_index()
correct.columns.names = ['Index', '', ' ', '  ']
correct = correct.iloc[:,1:]

# Copying the first 9 columns which only contains student info to new dataframe
df_student = df.iloc[:,0:9]
df_student.columns = ['Examinee No.','Name','License No.','Institute','Year','Exam Region','Exam Site','Attendance','Q. Type']

# Copying all columns except first 9 columns to another dataframe
df_answers = df.iloc[:,9:]


# Headers of all columns of correct df
headers = correct.columns.values
# Retrieving only subjects,questions no,questions bank id,questions type
allsub = [x[0] for x in headers]
sub = fn.unique(allsub)
allqno = [x[1] for x in headers]
allqbid = [x[2] for x in headers]
allqt = [x[3] for x in headers]

# Number of students,subjects,questions
ns = len(df.values)
nsub = len(sub)
qn = len(allqno)

# Calculating the starting and ending questions for each subjects
l = len(headers)
sname = headers[0][0]
qstart = [] * nsub
qend = [] * nsub
qstart.append(headers[0][1])
for i in range(0,l):
    tname = headers[i][0]
    if(sname != tname):
        qend.append(headers[i-1][1])
        qstart.append(headers[i][1])
        sname = tname
qend.append(headers[i][1])


# Creating object for each subject with total questions number
subject = []
for i in range(0, nsub):
    name = sub[i]
    sq = qstart[i]
    eq = qend[i]
    tqno = allsub.count(sub[i])
    subject.append(ic.Subjects(name, sq, eq, tqno))

# Creating question object for each questions
question = []
for i in range(0, qn):
    qno = allqno[i]
    qbid = allqbid[i]
    qtype = allqt[i]
    cans = correct.iloc[0][i]
    question.append(ic.Questions(qno, qbid, qtype, cans))




#### 1. Creating "Final Examinee List" output ####

# Only presented students in exam List will be returned
fel_output = fn.present_students(df_student)
# Droping the Attendance Column
fel_output = fel_output.drop(['Attendance'], axis=1)

# Outputing the final dataframe to excel file
startcol=0 # Column start 
fel_output.index = fel_output.index + 1
fel_output.to_excel(writer,sheet_name ='Applicants Summary',startrow=1, startcol=startcol)
startcol += len(fel_output.columns) + 2


#### 3. Creating "Applicant for each Exam Site" Output ####

# Passing Arguments to the function which sum the numbers of students as required
# (By each Exam Site and also by each year with Exam Region included)
df1 = df_student.loc[:,'Exam Region':'Exam Site']
df2 = df_student.loc[:,'Year':'Exam Site']
colg1 = ['Exam Region','Exam Site']
colg2 = ['Exam Site','Year']
cn = 'Exam Site'

afes_output = fn.dfcol_count(df1,df2,colg1,colg2,cn)

# Calculate the new row total
afes_output.loc['Total',2:] = afes_output.sum()

# Calculation of sub-total

# Passing the dataframe to insert subtotal in between the Exam Site
final_output = fn.subtotal(afes_output)

# Assigning the claculated total row at the end of the table
final_output.loc['Total'] = afes_output.loc['Total']

# Outputing the final dataframe to excel file
final_output.to_excel(writer,sheet_name ='Applicants Summary',startrow=1, startcol=startcol, index = False)
# startcol += len(final_output.columns) + 2

#### 2. Creating "Applicant for each Institute" Output ####

# Passing Arguments to the function which sum the numbers of students as required
# (By each Institute and also by each year)
df1 = df_student.loc[:,'Institute':'Institute']
df2 = df_student.loc[:,'Institute':'Year']
colg1 = ['Institute']
colg2 = ['Institute','Year']
cn = 'Institute'

afi_output = fn.dfcol_count(df1,df2,colg1,colg2,cn)

# Outputing the final dataframe to excel file
afi_output.index = afi_output.index + 1
afi_output.to_excel(writer,sheet_name ='Applicants Summary',startrow=1+ len(final_output) + 2, startcol=startcol)
startcol += len(afi_output.columns) + 2


#### 5. Creating "Attendance for each Exam Site" Output ####

# Only presented students in exam List will be returned
present = fn.present_students(df_student)

# Passing Arguments to the function which sum the numbers of students as required
# (By each Exam Site and also by each year with Exam Region included)
df1 = present.loc[:,'Exam Region':'Exam Site']
df2 = present.loc[:,'Year':'Exam Site']
colg1 = ['Exam Region','Exam Site']
colg2 = ['Exam Site','Year']
cn = 'Exam Site'

atfes_output = fn.dfcol_count(df1,df2,colg1,colg2,cn)

# Calculate the new row total
atfes_output.loc['Total',2:] = atfes_output.sum()

# Calculation of sub-total

# Passing the dataframe to insert subtotal in between the Exam Site
final_output = fn.subtotal(atfes_output)

# Assigning the claculated total row at the end of the table
final_output.loc['Total'] = atfes_output.loc['Total']

# Outputing the final dataframe to excel file
final_output.to_excel(writer,sheet_name ='Applicants Summary',startrow=1, startcol=startcol, index = False)
# startcol += len(final_output.columns) + 2



#### 4. Creating "Attendance for each Institute" Output ####

# Only presented students in exam List will be returned
present = fn.present_students(df_student)

# Passing Arguments to the function which sum the numbers of students as required
# (By each Institute and also by each year)
df1 = present.loc[:,'Institute':'Institute']
df2 = present.loc[:,'Institute':'Year']
colg1 = ['Institute']
colg2 = ['Institute','Year']
cn = 'Institute'

atfi_output = fn.dfcol_count(df1,df2,colg1,colg2,cn)

# Outputing the final dataframe to excel file
atfi_output.index = atfi_output.index + 1
atfi_output.to_excel(writer,sheet_name ='Applicants Summary',startrow=1 + len(final_output) + 2 , startcol=startcol)
startcol += len(atfi_output.columns) + 2


#### 6. Creating output for "Question List" ####

# Appending array of objects to list then converting it into dataframe
list = []
for i in range(0,nsub):
    list.append(vars(subject[i]))
op = pd.DataFrame(list,  columns=['name','startq','endq','tqno'], index=range(1,nsub+1))
op.columns = ['Category','Start Q. No.','End Q. No.','Number of Q.']

# Adding new column Notation in the final result
list = []
for i in range(1,nsub+1):
    list.append('Sub'+ str(i))
op['Notation in the Final result'] = list

# Calculate the new row total
op.loc['Total','Number of Q.'] = op['Number of Q.'].sum()

# Outputing the final dataframe to excel file
# op.index = op.index + 1
op.to_excel(writer,sheet_name ='Applicants Summary',startrow=1, startcol=startcol)
startcol += len(op.columns) + 2

# Anup Codes

institute = 'Institute'

xyz = correct
sf = df_answers.copy()
sf['Total'] = 0
total_subjects = []
for one in sub:
    sf[one] = (sf[one].values == correct[one].values)
    sf['Total'] = sf[one].sum(axis=1) + sf['Total']
    total_subjects.append(len(sf[one].columns))
    # sf[one].replace([True, False],['1','0'])
# sf['Total'] = sf.sum(axis = 1)
sf['subjects'] = sum(total_subjects)
sf['Percentage'] = sf['Total']/sum(total_subjects) * 100
sf['Rank'] = sf['Total'].rank(method = 'min', ascending = False)
sf['Percentage of Rank'] = sf['Rank']/len(sf)
# sf['Test'] = sf['Total'].rank(method = 'min', ascending = False,pct=True)

fx = pd.merge(df_student,sf, left_index=True, right_index=True)
f = fn.get_tuples(fx)
# from pprint import pprint
# pprint(f)
fx.columns = pd.MultiIndex.from_tuples(f)
fx.to_excel(writer, "Score")

rf = df_student
km = pd.DataFrame(df.index, columns = ["Examinee No."])
km = rf
km.index = df.index
for one in sub:
    km[one,'Scored Marks'] = sf[one].sum(axis=1)
    km[one,'Rank in total'] = km[one,'Scored Marks'].rank(method = 'min', ascending = False)
    km[one,'Rank in Institute'] = km.filter([(one,'Scored Marks'),institute]).groupby([institute]).rank(method = 'min', ascending=False)
    km[one,'Rank in Same Year'] = km.filter([(one,'Scored Marks'),'Year']).groupby(['Year']).rank(method = 'min', ascending=False)

subjects_tuple = []
for a in sub:
    subjects_tuple.append((a,'Scored Marks'))

r = fn.get_tuples(km)
s = pd.DataFrame(np.transpose(km.values), index = pd.MultiIndex.from_tuples(r))
km = s.T.copy()
km['Total','Obtained Score'] = km.filter(subjects_tuple).sum(axis=1)
km['Total','Rank in total'] = km['Total','Obtained Score'].rank(method = 'min', ascending = False)
# km['Total','Rank in  Institute'] = km.filter([('Total','Obtained Score'),institute]).groupby([institute]).rank(method = 'min', ascending=False)
# km['Total','Rank in Same Year'] = km.filter([('Total','Obtained Score'),'Year']).groupby(['Year']).rank(method = 'min', ascending=False)

km.to_excel(writer, "Total")

# Question Analysis
m = correct.filter(like='Subject').T
m['Level of Difficulty','Winners'] = sf.filter(like='Subject').sum(axis=0)
m['Level of Difficulty','Total Examinees'] = len(sf)
m['Level of Difficulty','Correct Answer Rate'] = m['Level of Difficulty','Winners']/m['Level of Difficulty','Total Examinees']

m['Answer Discrimination Rate','Winners in Top 27%'] = ((sf['Percentage of Rank'] <= .27) & sf.filter(like='Subject').T).sum(axis=1)
m['Answer Discrimination Rate','Examinees in Top 27%'] = (sf['Percentage of Rank'] <= .27).sum()
m['Answer Discrimination Rate','Winners in Low 27%'] = ((sf['Percentage of Rank'] >= .73) & sf.filter(like='Subject').T).sum(axis=1)
m['Answer Discrimination Rate','Examinees in Low 27%'] = (sf['Percentage of Rank'] >= .73).sum()
m['Answer Discrimination Rate','Distinguishable Rage'] = m['Answer Discrimination Rate','Winners in Top 27%']/m['Answer Discrimination Rate','Examinees in Top 27%'] - m['Answer Discrimination Rate','Winners in Low 27%']/m['Answer Discrimination Rate','Examinees in Low 27%']

for i in [1,2,3,4,5,0]:
    m['Number of Respondent',i] = (df.filter(like='Subject') == i).sum()
for i in [1,2,3,4,5,0]:
    m['Answer Response Rate', i] = (df.filter(like='Subject') == i).sum()/len(sf)

r=[]
tup_type = type((1,2))
for a in m.columns:
  if type(a) != tup_type:
    r.append((a,' '),)
  else:
    r.append(a)


m = pd.DataFrame(np.transpose(m.values), index = pd.MultiIndex.from_tuples(r)).copy()
m.columns = correct.filter(like='Subject').columns
m.to_excel(writer,"Question Analysis")
# f = y.filter(['Subject 1','Year']).groupby('Year').rank(method="min", ascending=False)

# df['Auction_Rank'] = df.groupby('Auction_ID')['Bid_Price'].rank(method='min', ascending=False)



# fx = pd.merge(rf,mf, on=['Examinee No.'])


# Finish Anup Codes

writer.save()
writer.close()



#### 7. Creating output for "Score Table Sheet" ####
# temp1 = df_answers == correct.values
# # output = pd.concat([correct,temp])
# temp1.columns = temp1.columns.droplevel([2,3])
# # output.insert(0, 'Total Score', output.sum(axis=1))

# # output.insert(0,{correct})
# temp = df_student
# temp = pd.MultiIndex.from_product([[''], temp.columns])
# temp1
# # temp.join(temp1, how='left')
# temp.join(temp1)
# temp = df
# temp.columns = temp.columns.droplevel([2,3])