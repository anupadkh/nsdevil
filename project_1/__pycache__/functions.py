import input_class as ic
import pandas as pd

# Function to count applicant yearwise
# Reveive two dataframe(dfa,dfb),column name(cn),year value(y)
def yearcount(dfa,dfb,cn,y):
    flag=0
    n=len(dfa.index)
    m=len(dfb.index)
    co= [0] * n
    for i in range(0,n):
        iname=dfa[cn][i]
        for j in range(0,m):
            jname=dfb[cn][j]
            jyear=dfb['Year'][j]
            if(iname==jname and y==jyear):
                flag=1
                co[i]=dfb['count'][j]
        if(flag==0):
            co[i]=0
    return co


# Function that return unique element list
def unique(sequence):
    seen = set()
    return [x for x in sequence if not (x in seen or seen.add(x))]


# Function that returns dataframe of only presented students in exam
def present_students(all_students):
    
    all_examinee = all_students.loc[all_students['Attendance'] == 'O']

    # Resetting index so that the index type change into RangeIndex
    all_examinee = all_examinee.reset_index()
    all_examinee = all_examinee.drop(['index'],axis=1)

    return all_examinee


def dfcol_count(df1,df2,colg1,colg2,cn):

    df1['Number of Applicant'] = " "
    df1['Number of Applicant'] = df1.groupby(colg1)[cn].transform('count')
    df1 = df1.drop_duplicates()

    df2['count'] = " "
    df2['count'] = df2.groupby(colg2)[cn].transform('count')

    # Droping previous index column and assign new index
    df1 = df1.reset_index()
    df1 = df1.drop(['index'],axis=1)

    # Creating new columns 
    df1['Year 1'] = yearcount(df1,df2,cn,1)
    df1['Year 2'] = yearcount(df1,df2,cn,2)
    df1['Year 3'] = yearcount(df1,df2,cn,3)
    df1['Year 4'] = yearcount(df1,df2,cn,4)

    return df1


def subtotal(afes_output):
    b = afes_output.groupby('Exam Region').count()>1
    t = b.loc[b['Exam Site'] == True]
    f = b.loc[b['Exam Site'] == False]
    tl=len(t)
    fl=len(f)
    # Appending subtotal in between
    temp = afes_output
    blank = []
    for i in range(0,tl):
        temp = afes_output.loc[afes_output['Exam Region']==t.index[i]]
        temp.loc['Sub Total',2:] = temp.sum() #warning msg
        blank.append(temp)
    for i in range(0,fl):
        temp = afes_output.loc[afes_output['Exam Region']==f.index[i]]
        blank.append(temp)
    final_output = pd.concat(blank)
    final_output.loc['Sub Total','Exam Site']='Sub Total'

    # Droping previous index column and assign new index
    final_output = final_output.reset_index()
    final_output = final_output.drop(['index'],axis=1)

    return final_output


def get_tuples(df):
    r=[]
    tup_type = type((1,2))
    for a in df.columns:
      if type(a) != tup_type:
        r.append((a,''),)
      else:
        r.append(a)
    return r