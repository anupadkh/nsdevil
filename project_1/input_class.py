class Sheet(object):
    def __init__(self):
        self.sheetname = "Answer Sheet(1st input)"


class Students(object):
    def __init__(self, sf, ans):
        self.examineeno = sf[0]
        self.name = sf[1]
        self.licenseno = sf[2]
        self.institute = sf[3]
        self.year = sf[4]
        self.examregion = sf[5]
        self.examsite = sf[6]
        self.attendance = sf[7]
        self.qtype = sf[8]
        self.answer = ans


class Subjects(object):
    def __init__(self, name, tqno):
        self.name = name
        self.tqno = tqno


class Questions(object):
    def __init__(self, qno, qbid, qtype, cans):
        self.qno = qno
        self.qbid = qbid
        self.qtype = qtype
        self.cans = cans
