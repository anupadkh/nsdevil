import input_class as ic
import functions as fn

import pandas as pd

# excel-file and excel-sheet name
file_name = 'input.xlsx'
sheet_name = ic.Sheet().sheetname

# Loading the input excel file into dataframe without correct answer row
df = pd.read_excel(file_name, header=[0, 1, 2, 3], skiprows=[4], sheet_name=sheet_name)

# Getting only correct answer row data
correct = pd.read_excel(file_name, header=[0, 1, 2, 3], index_col=[0,1,2,3,4,5,6,7], nrows=1, sheet_name=sheet_name)

#creating a new column for index
df = df.rename_axis(['Examinee No.']).reset_index()
df.columns.names = ['Index','',' ','  ']

# main header name(heading 1)
h1 = df.columns.levels[0]

#function that return unique element list
def unique(sequence):
    seen = set()
    return [x for x in sequence if not (x in seen or seen.add(x))]
#headers of all columns
all_headers = list(correct)
#retrieving only subjects
allsub = [x[0] for x in all_headers]
allsub.remove('Category')
sub = unique(allsub)
#Counting number of subjects
nsub = len(sub)

#no of students
ns = len(df.values)
student = []
#creating student object for each student
for i in range(0,ns):
    sf = []
    #retriving student information
    for j in range(0,9):
        sf.append(df.iloc[i][j])
    array = []
    #retriving individual answer of each subject
    for k in range(0,nsub):
        ans = []
        nq = len(df[sub[k]].columns)
        for l in range(0,nq):
            ans.append(df[sub[k]].iloc[i][l])
        array.append(ans)
    student.append(ic.Students(sf,array))

#creating subject object for each subject with total questions
subject = []
for i in range(0,nsub):
    name=sub[i]
    tqno=allsub.count(sub[i])
    subject.append(ic.Subjects(name,tqno))

#retrieving only subjects
allqno = [x[1] for x in all_headers]
allqno.remove('Question No.')
#retrieving only subjects
allqbid = [x[2] for x in all_headers]
allqbid.remove('Q. Bank ID')
#retrieving only subjects
allqt = [x[3] for x in all_headers]
allqt.remove('Q. Type')
#no of questions
qn = len(allqno)

# creating question object for each questions
question = []
for i in range(0, qn):
    qno = allqno[i]
    qbid = allqbid[i]
    qtype = allqt[i]
    cans = correct.iloc[0][i + 1]
    question.append(ic.Questions(qno, qbid, qtype, cans))
