from collections import defaultdict

class sheet(object):

    def __init__(self):
        self.sheetname = "Answer Sheet(1st input)"

class Exam(object):
    _all = set() # Set of all exam centers

    def __init__(self, examSite, examRegion):
        self.exam_site = examSite
        self.exam_region = examRegion
        self.__class__._all.add(self)

    # Removing the set duplicacy
    def __repr__(self):
        return "Exam Center (%s, %s)" % (self.exam_site, self.exam_region)
    def __eq__(self, other):
        if isinstance(other, Exam):
            return ((self.exam_site == other.exam_site) and (self.exam_region == other.exam_region))
        else:
            return False
    def __ne__(self, other):
        return (not self.__eq__(other))
    def __hash__(self):
        return hash(self.__repr__())
    # Removing set duplicacy finished

    def register(self):
        # Register only on call
        # This is used to compare the initialized exam sites with each student
        '''
            The general call will be
            a = Exam('Site_name', 'Region_name', 'institute_name')
            if !(a in Exam._all):
                a.register()
        '''
        self.__class__._all.add(self)


class students(object):
    _all = set() # Set of all students
    stud_index = defaultdict(list)

    def __init__(self, no, name, license, year, examCenter,  institute, attendance='O'):
        self.no = no
        self.name = name
        self.license_no = license
        self.year = year
        self.examCenter = examCenter # Exam Object
        self.attendance = attendance
        self.institute = institute
        self.__class__._all.add(self)
        self.__class__.stud_index[no].append(self)

    def attended(self):
        if self.attendance == 'O':
            return True
        else:
            return False

    def __repr__(self):
        return "Student ID (%s, %s)" % (self.name, self.no)
    def __eq__(self, other):
        if isinstance(other, Exam):
            return ((self.name == other.name) and (self.no == other.no))
        else:
            return False
    def __ne__(self, other):
        return (not self.__eq__(other))
    def __hash__(self):
        return hash(self.__repr__())

    def find_student(examinee_no):
        return students.stud_index[examinee_no]


class questions(object):
    _all = set() #set of all questions
    ques_index = defaultdict(list)

    def __init__(self, subject, no, qbank_id, q_type, correct_answer):
        self.subject = subject
        self.no = no
        self.qbank_id = qbank_id
        self.q_type = q_type
        self.correct_answer = correct_answer
        self.__class__._all.add(self)
        self.__class__.ques_index[no].append(self)

    def siblings(self, all_questions): #List of questions with same subject
        a =[]
        for one in all_questions:
            if one.subject == self.subject:
                a.append(one)
        # At the end of siblings it is still the member of same subject
        # But it is already present in all_questions so appending self isn't necessary
        # a.append(self)
        return a

    def full_marks(self,all_questions):
        return len(self.siblings(all_questions))

    def __repr__(self):
        return "Question No (%s, and subject is %s)" % (self.no, self.subject)

    def find_question(q_no):
        return questions.ques_index[q_no]

class answered(object):
    _all =  set() #set of all answers
    answers_index = defaultdict(list)

    def __init__(self, student, question, answer):
        self.student = student.no # Students object
        self.question = question.no # questions object
        self.answer = answer
        self.correct = (self.answer == question.correct_answer)
        self.__class__._all.add(self)
        self.__class__.answers_index[str(question.no)+str(student.no)].append(self)

    def student_answers(self, all_answers):
        # Returns all answers of one student
        a = []
        for item in all_answers:
            if item.student == self.student:
                a.append(item)
        return a

    def find_answer(student_no, q_no):
        return answered.answers_index[str(q_no)+str(student_no)]
