import input_sheet as ips
import functions as fn

import pandas as pd
# import numpy as np

# import os

'''
>>> import importlib
>>> ips = importlib.reload(input_sheet)
>>> answer_sheet = ips.sheet()
>>> answer_sheet.sheetname
'''

file = ('input.xlsx')

answer_sheet = ips.sheet()

df = pd.read_excel(file, header=[0,1,2,3], skiprows=[4], sheet_name=answer_sheet.sheetname)

a = df.columns.levels[0] # Get Headers from Level 1
# matching = [s for s in some_list if "Subject" in s] # Get all the subjects

correct = pd.read_excel(file, header=[0,1,2,3], nrows=1, sheet_name=answer_sheet.sheetname) # Get the correct answers

def all_subjects(xyz):
    all_objects = []
    all_headers = list(xyz)
    all = [x[0] for x in all_headers]
    subjects = list(set(all[all.index('Category') + 1:]))
    subjects.sort(key=fn.natural_keys)
    for one in subjects:
        myques = list(xyz[one])
        for ques in myques:
            all_objects.append(ips.questions(
            subject = one , no = ques[0], qbank_id = ques[1], q_type= ques[2],
            correct_answer= xyz[one][ques[0]].values[0][0]
            ))
    return all_objects



objects_of_questions = (all_subjects(correct))
# print(objects_of_questions[159].subject)
'''
tests:

all = list(ips.questions._all)
all[0].no
all[0].siblings(all)
'''

all_students_objects = []
for student_id in list(df.index):
    p = df.loc[student_id]
    exam = ips.Exam(
        examSite= p['Exam Site'].values[0],
        examRegion = p['Exam Region'].values[0]

    )
    student = ips.students(
        no=student_id,
        name=p['Name'].values[0],
        license=p['License No.'].values[0],
        year = p['Year'].values[0],
        attendance = p['Attendance'].values[0],
        institute=p['Institue'].values[0],
        examCenter = exam
    )
    for question in objects_of_questions:
        answer = ips.answered(
            student=student,
            question=question,
            answer=p[question.subject][question.no].values[0],
        )
    all_students_objects.append(student)

    # print(p)
print(len(ips.Exam._all))


'''
# print ('Index' + str(df.index) + '\n\n')

p = df.loc[62010004]['Subject 1']['No. 1'] # Get Series
# print (type(p))
# print(p.data)
#
# print (p[0])
print(p)

# for header in (df.columns.levels):
#     print(header)

# for y in a:
#     print(y)
#     m = df[y].columns
#     v = df[y]
#     for r in m:
#         x = v[r]
#         print (str(y) + ':' + str(r))
#         print (list(x))

# Processing in dataframes
d = (df['Subject 1'].values == correct['Subject 1'].values) # Is a 45 x 569 Matrix

# Choosing equals
# df.loc[df['column_name'] == some_value]
df.loc[df['Attendance'] == 'O']

# Choosing partialdataframes
mf = pd.read_excel(file,header=list(range(0,5)), sheet_name = answer_sheet.sheetname, usecols=list(range(8,100)), index=[0])
mf['Category']=mf.index
mf.index=df.index


# Choosing partialdataframes students
rf = pd.read_excel(file,skiprows=[1,2,3,4], sheet_name = answer_sheet.sheetname, usecols=list(range(0,8)))

# Merging two partialdataframes
result = pd.merge(left, right, on=['key1', 'key2'])
# left and right are names of dataframes. Both have keys key1, key2

# Get unique values
pd.Series([2, 1, 3, 3], name='A').unique()

# Count unique values
pd.Series([2, 1, 3, 3], name='A').value_counts()

# Sort values
m = x.value_counts()
m.sort_values()
m.sort_index()

# Saving into excel files
writer = pd.ExcelWriter('output.xlsx')
df1.to_excel(writer,'Sheet1')
df2.to_excel(writer,'Sheet2')
writer.save()

# Not writing the index
movies.to_excel('output.xlsx', index=False)

# Improve Pandas Output to ExcelWriter
# http://pbpython.com/improve-pandas-excel-output.html

PIVOTS
>>> df = pd.DataFrame({"A": ["foo", "foo", "foo", "foo", "foo",
...                          "bar", "bar", "bar", "bar"],
...                    "B": ["one", "one", "one", "two", "two",
...                          "one", "one", "two", "two"],
...                    "C": ["small", "large", "large", "small",
...                          "small", "large", "small", "small",
...                          "large"],
...                    "D": [1, 2, 2, 3, 3, 4, 5, 6, 7]})
>>> df
     A    B      C  D
0  foo  one  small  1
1  foo  one  large  2
2  foo  one  large  2
3  foo  two  small  3
4  foo  two  small  3
5  bar  one  large  4
6  bar  one  small  5
7  bar  two  small  6
8  bar  two  large  7
>>> table = pivot_table(df, values='D', index=['A', 'B'],
...                     columns=['C'], aggfunc=np.sum)
>>> table
C        large  small
A   B
bar one    4.0    5.0
    two    7.0    6.0
foo one    4.0    1.0
    two    NaN    6.0

# Counting the Universities with year_wise
rf['Count'] = 1
table = pd.pivot_table(rf, index=['Institue','Year'], aggfunc=np.sum, value='Count')

# Region_Wise Year-Wise Count
table = pd.pivot_table(rf, index=['Exam Site','Exam Region','Year'], aggfunc=np.sum, values='Count')


# Changing report to own style
>>> df
  location  name  Jan-2010  Feb-2010  March-2010
0        A  test        12        20          30
1        B   foo        18        20          25
>>> df2 = pd.melt(df, id_vars=["location", "name"],
                  var_name="Date", value_name="Value")
>>> df2
  location  name        Date  Value
0        A  test    Jan-2010     12
1        B   foo    Jan-2010     18
2        A  test    Feb-2010     20
3        B   foo    Feb-2010     20
4        A  test  March-2010     30
5        B   foo  March-2010     25
>>> df2 = df2.sort(["location", "name"])
>>> df2
  location  name        Date  Value
0        A  test    Jan-2010     12
2        A  test    Feb-2010     20
4        A  test  March-2010     30
1        B   foo    Jan-2010     18
3        B   foo    Feb-2010     20
5        B   foo  March-2010     25

# Multiple Dataframes in Same Sheet
# Creating Excel Writer Object from Pandas
writer = pd.ExcelWriter('test.xlsx',engine='xlsxwriter')
workbook=writer.book
worksheet=workbook.add_worksheet('Validation')
writer.sheets['Validation'] = worksheet
df.to_excel(writer,sheet_name='Validation',startrow=0 , startcol=0)
another_df.to_excel(writer,sheet_name='Validation',startrow=20, startcol=0)

Examples of Filter

>>> df
one  two  three
mouse     1    2      3
rabbit    4    5      6

>>> # select columns by name
>>> df.filter(items=['one', 'three'])
one  three
mouse     1      3
rabbit    4      6

>>> # select columns by regular expression
>>> df.filter(regex='e$', axis=1)
one  three
mouse     1      3
rabbit    4      6

>>> # select rows containing 'bbi'
>>> df.filter(like='bbi', axis=0)
one  two  three
rabbit    4    5      6


'''
